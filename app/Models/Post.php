<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Casts\Attribute;

class Post extends Model
{
    use HasFactory;


    public function scopeActive($query)
    {
        $query->where('active', 1);
    }

    public function scopeHot($query)
    {
        $query->where('hot_topic', 1);
    }

    public function scopeNothot($query)
    {
        $query->where('hot_topic', 0);
    }

    public function scopeFamous($query)
    {
        $query->where('famous', 1);
    }

    public function scopeMontharticle($query)
    {
        $query->where('month_article', 1);
    }

    public function scopeNotmonth($query)
    {
        $query->where('month_article', 0);
    }

    public function scopeArchive($query)
    {
        return $query->where('date','<',date("Y-m-d",strtotime("-1 year")));
    }

    public function scopeNotarchive($query)
    {
        return $query->where('date','>',date("Y-m-d",strtotime("-1 year")));
    }

    public function scopeThisyear($query)
    {
        return $query->whereYear('date',now()->year);
    }

    public function scopeThismonth($query)
    {
        return $query->whereMonth('date',now()->month);
    }

    public function scopeNotthismonth($query)
    {
        return $query->whereMonth('date','!=',now()->month);
    }

    

    public function block()
    {
        return $this->belongsTo(PostBlock::class,'type','post_type');
    }

    public function user_detail()
    {
        return $this->belongsTo(User::class,'user','id');
    }

    protected function link(): Attribute
    {
        return Attribute::make(
            get: function($value){return auth()->check() ? $value : route('login'); }
        );
    }

    public function posts(){
        return $this->belongsTo(Cat::class);
    }

    
}
