<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Course extends Model
{
    use HasFactory;

    protected $fillable =
    [
        'title','description','from','to','price','exam_time','exam_limit','img'
    ];

    public function count_quiz(){
        return $this->hasMany(CourseQues::class)->where('type',0);
    }

    public function count_exam(){
        return $this->hasMany(CourseQues::class)->where('type',1);
    }
}
