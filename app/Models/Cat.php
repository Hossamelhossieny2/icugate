<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Cat extends Model
{
    use HasFactory;

    public function post()
    {
        return $this->hasMany(Post::class);
    }

    public function pricing()
    {
        return $this->belongsToMany(Pricing::class,'cat_pricing');
    }
}
