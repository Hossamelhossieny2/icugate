<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Youtube extends Model
{
    use HasFactory;

    protected $table = 'youtube';

    protected $primaryKey = 'youtube_id';

    protected $fillable = [
        'title','playlist','playlist_id','embed','img','video_id'
    ];
}
