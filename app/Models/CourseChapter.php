<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class CourseChapter extends Model
{
    use HasFactory;

    public function ques()
    {
        return $this->hasMany(CourseOnlineQues::class,'chapter');
    }
}
