<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class PayPalOrder extends Model
{
     public function user(){
        return $this->belongsTo(User::class);
    }
    public function order_type(){
        return $this->belongsTo(PaypalOrderType::class,'order_type_id','id');
    }
}
