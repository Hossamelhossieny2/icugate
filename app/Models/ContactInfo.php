<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ContactInfo extends Model
{
    use HasFactory;

    protected $table="contact_info";

    protected $fillable = [
        'home_job_title', 'home_job_text','phone', 'email', 'paypal_account','facebook','google','twitter','instgram','youtube'
    ];
}
