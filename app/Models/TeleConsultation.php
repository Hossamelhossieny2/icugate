<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class TeleConsultation extends Model
{
    const PayPalType = 3;

    protected $fillable = [
        'title','price'
    ];

    public function getPaymentRoute()
    {
        return route('paypal.payfor.teleConsultaion',$this);
    }

    public function getAfterPaymentRoute()
    {
        return route('home');
    }

    public function getPrePaymentRoute()
    {
        return route('buy.tele',$this);
    }
}
