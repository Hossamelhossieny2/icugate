<?php

namespace App\Models;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use App\Notifications\ResetPassword as ResetPasswordNotification;
use App\Notifications\VerifyEmail;
use Illuminate\Database\Eloquent\Casts\Attribute;
use Laravel\Sanctum\HasApiTokens;

class User extends Authenticatable
{
    use HasApiTokens, HasFactory, Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'f_name', 'l_name','email', 'password', 'hospital', 'profession' , 'mobile', 'country', 'city', 'type'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];
    public function sendPasswordResetNotification($token)
    {
        $this->notify(new ResetPasswordNotification($token));
    }
    public function sendEmailVerificationNotification()
    {
        $this->notify(new VerifyEmail);
    }
    public function user_courses()
    {
        return $this->HasMany('App\UserCourse');
    }
    public function isPaidThisCourse($courseId)
    {
        return $this->user_courses()->where('course_id',$courseId)->wherePayed(1)->first();
    }
    public function isQuizDone($courseId)
    {
        return $this->isPaidThisCourse($courseId)->where('course_id',$courseId)->where('quiz','>','20')->first();
    }
    public function isTestDone($courseId)
    {
        return $this->isQuizDone($courseId)->where('course_id',$courseId)->where('test','>','70')->first();
    }
    public function iscertPrinted($courseId)
    {
        return $this->isTestDone($courseId)->where('course_id',$courseId)->whereNotNull('certificate')->first();
    }

    public function shouldPayPackage()
    {
        return in_array($this->type,[1,2]);
    }
    public function paidHisPackage()
    {
        return $this->paid;
    }

    protected function type(): Attribute
    {
        return new Attribute(
            get: fn ($value) =>  ["0", "1", "2","3"][$value],
        );
    }
}
