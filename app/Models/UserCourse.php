<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;

class UserCourse extends Model
{
    protected $table = 'user_course';

     protected $casts = [
        'payed' 		=> 'boolean',
        'quiz' 			=> 'boolean',
        'certificate' 	=> 'boolean',
    ];


    public function paypal_order()
    {
      return $this->BelongsTo('App\PayPalOrder', 'paypal_order_id');
    }

    public function scopePaid($query,$course_id)
    {
     return $query->where('course_id',$course_id)->where('user_id',Auth::user()->id)->wherePaid('1');
    }

    public function scopeQuiz($query,$course_id)
    {
     return $query->paid($course_id)->whereNotNull('quiz');
    }

    public function scopeTest($query,$course_id)
    {
     return $query->quiz($course_id)->where('test','>','70');
    }

    public function scopeCert($query,$course_id)
    {
     return $query->test($course_id)->whereNotNull('certificate');
    }
}
