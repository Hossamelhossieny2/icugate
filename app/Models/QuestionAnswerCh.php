<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class QuestionAnswerCh extends Model
{
    protected $table='courses_onlineans_opt';
    
    public function question() {
        return $this->belongsTo('App\QuestionCh','q_id')->orderByRaw("RAND()");

    }
}