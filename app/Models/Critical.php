<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Critical extends Model
{
    use HasFactory;

    protected $table="critical";

    protected $fillable = [
        'date_from','date_to','event','country','city','link'
    ];
}
