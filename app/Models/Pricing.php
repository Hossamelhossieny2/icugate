<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Pricing extends Model
{
    use HasFactory;

    protected $table="pricing";

    protected $fillable = [
        'title', 'desc','price', 'course_percent'
    ];
}
