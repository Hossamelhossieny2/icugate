<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ManualPay extends Model
{
    use HasFactory;

    public function user(){
        return $this->belongsTo(User::class);
    }

    public function admin(){
        return $this->belongsTo(User::class,'pay_user','id');
    }

    public function order_type(){
        return $this->belongsTo(PaypalOrderType::class,'payment_type','id');
    }
}
