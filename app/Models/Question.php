<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Question extends Model
{
    protected $table='course_ques';
    
    public function answers() {
        return $this->hasMany('App\QuestionAnswer', 'q_id')->orderByRaw("RAND()");
    }

}