<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class QuestionCh extends Model
{
    protected $table='courses_onlineques';
    
    public function answers() {
        return $this->hasMany('App\QuestionAnswerCh', 'q_id')->orderByRaw("RAND()");
    }

}