<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class JobList extends Model
{
    use HasFactory;

    public function job_cv(){
        return $this->HasMany(Job::class,'job_id','id');
    }
   
}
