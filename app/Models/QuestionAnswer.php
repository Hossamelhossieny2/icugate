<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class QuestionAnswer extends Model
{
    protected $table='course_ans_opts';

    public function question() {
        return $this->belongsTo('App\Question','q_id')->orderByRaw("RAND()");

    }
}