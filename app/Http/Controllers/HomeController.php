<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Models\Post;
use App\Models\Event;
use App\Models\Course;
use App\Models\Pricing;
use App\Models\Team;
use App\Models\Tag;
use App\Models\Cat;
use App\Models\ContactInfo;
use App\Models\Critical;
use App\Models\User;
use App\Models\Page;
use App\Models\Contact;
use App\Models\JobList;
use App\Models\Job;
use App\Models\YoutubeChannel;
use File;

class HomeController extends Controller
{
    public $show = 0;
    
    public function __construct()
    {
        //$this->middleware('auth')->except('index');
    }
    

    // 
    
    public function index()
    {
        $arr['conf'] = $conf = ContactInfo::first();
        $arr['menu'] = 'home';
        if(auth()->user()){
            if(auth()->user()->type == 0){
                $arr['usertype'] = "free";
            }elseif(auth()->user()->type == 1){
                $arr['usertype'] = "silver";
            }elseif(auth()->user()->type == 2){
                $arr['usertype'] = "gold";
            }
        }else{
            $arr['usertype'] = "guest";
        }
        $arr['latest'] = Post::where('famous',1)->where('type','<',2)->orderByDesc('id')->limit('15')->get();
        $arr['guidelines'] = Post::where('cat_id',9)->orderByDesc('id')->limit('15')->get();
        $arr['montharticles'] = Post::where('month_article',1)->orderByDesc('id')->limit('10')->get();
        $arr['tags'] = Tag::with('post')->get();
        $arr['cats'] = Cat::where('shows','1')->withCount('post')->get();
        $arr['hot_topics'] = Post::where('cat_id',12)->orderByDesc('id')->limit(12)->get();
        $arr['pricing'] = Pricing::all();
        $arr['events']   = Event::whereDate('event_date','>=', \Carbon\Carbon::now()->format('Y-m-d'))->get();
        $arr['courses']  = Course::where('to','>=',date("Y-m-d"))->get();
        $arr['teams']    = Team::get();
        $arr['critical'] = Critical::orderByDesc('date_from')->get();
        $arr['yt_ch']       = YoutubeChannel::get();
        $arr['stat_bar']    = [
            'users_count'   => User::count(),
            'courses_count' => Course::count(),
            'posts_count'   => Post::count(),
            'site_visit'   => $conf['site_visit']
        ];
        
        return view('home',$arr);
    }

    public function aboutus()
    {
        $arr['conf'] = $conf = ContactInfo::first();
        $arr['menu'] = 'icugate';
        if(auth()->user()){
            if(auth()->user()->type == 0){
                $arr['usertype'] = "free";
            }elseif(auth()->user()->type == 1){
                $arr['usertype'] = "silver";
            }elseif(auth()->user()->type == 2){
                $arr['usertype'] = "gold";
            }
        }else{
            $arr['usertype'] = "guest";
        }
        $arr['latest'] = Post::where('famous',1)->where('type','<',2)->orderByDesc('id')->limit(5)->get();
        
        $arr['tags'] = Tag::get();
        $arr['cats'] = Cat::where('shows','1')->withCount('post')->get();
        $arr['page'] = Page::where('id','1')->first();
        $arr['teams']    = Team::get();
        $arr['stat_bar']    = [
            'users_count'   => User::count(),
            'courses_count' => Course::count(),
            'posts_count'   => Post::count(),
            'site_visit'   => $conf['site_visit']
        ];
            
        return view('about',$arr);
    }

    public function contactus()
    {
        $arr['conf'] = $conf = ContactInfo::first();
        $arr['menu'] = 'icugate';
        if(auth()->user()){
            if(auth()->user()->type == 0){
                $arr['usertype'] = "free";
            }elseif(auth()->user()->type == 1){
                $arr['usertype'] = "silver";
            }elseif(auth()->user()->type == 2){
                $arr['usertype'] = "gold";
            }
        }else{
            $arr['usertype'] = "guest";
        }
        $arr['latest'] = Post::where('famous',1)->where('type','<',2)->orderByDesc('id')->limit(5)->get();
        
        $arr['tags'] = Tag::get();
        $arr['cats'] = Cat::where('shows','1')->withCount('post')->get();
        $arr['page'] = Page::where('id','1')->first();
        $arr['teams']    = Team::get();
        //dd($arr['page']);
            
        return view('contact',$arr);
    }

    public function contactus_send(Request $request){
        $validated = $request->validate([
	        'form_name' => 'required|max:50',
	        'form_email' => 'required|email',
            'form_phone' => 'required|numeric',
	        'form_subject' => 'required|max:155',
	        'form_message' => 'required|max:255',
	    ]);

        $new_contact = new Contact;
        $new_contact->name = $validated['form_name'];
        $new_contact->email = $validated['form_email'];
        $new_contact->phone = $validated['form_phone'];
        $new_contact->subject = $validated['form_subject'];
        $new_contact->message = $validated['form_message'];
        $new_contact->save();

        return redirect(route('contact_icugate'))->with('status', ['Sent success .. Our professional will contact you soon','success']);
    }

    public function pricing()
    {
        $arr['conf'] = $conf = ContactInfo::first();
        $arr['menu'] = 'icugate';
        if(auth()->user()){
            if(auth()->user()->type == 0){
                $arr['usertype'] = "free";
            }elseif(auth()->user()->type == 1){
                $arr['usertype'] = "silver";
            }elseif(auth()->user()->type == 2){
                $arr['usertype'] = "gold";
            }
        }else{
            $arr['usertype'] = "guest";
        }
        $arr['latest'] = Post::where('famous',1)->where('type','<',2)->orderByDesc('id')->limit(5)->get();
        $arr['tags'] = Tag::with('post')->get();
        $arr['cats'] = Cat::where('shows','1')->withCount('post')->get();
        
        $arr['pricing'] = Pricing::all();
        
        return view('pricing',$arr);
    }

    public function jobs(){
        $arr['conf'] = $conf = ContactInfo::first();
        $arr['menu'] = 'icugate';
        if(auth()->user()){
            if(auth()->user()->type == 0){
                $arr['usertype'] = "free";
            }elseif(auth()->user()->type == 1){
                $arr['usertype'] = "silver";
            }elseif(auth()->user()->type == 2){
                $arr['usertype'] = "gold";
            }
        }else{
            $arr['usertype'] = "guest";
        }
        $arr['latest'] = Post::where('famous',1)->where('type','<',2)->orderByDesc('id')->limit(5)->get();
        
        $arr['tags'] = Tag::get();
        $arr['cats'] = Cat::where('shows','1')->withCount('post')->get();
        
        $arr['jobs']    = JobList::withCount('job_cv')->get();
            
        return view('job',$arr);
    }

    

    public function get_rss(){
        $url = "https://onesearch-rss.nejm.org/api/specialty/rss?context=nejm&specialty=cardiology";
        $xml = simplexml_load_file($url);
        echo $xml->channel->lastBuildDate;
        //dd('xx');
        for($i = 0; $i < 10; $i++){

            $title = $xml->channel->item[$i]->title;
            $link = $xml->channel->item[$i]->guid;
            $description = $xml->channel->item[$i]->description;
            $pubDate = $xml->channel->item[$i]->dc;
            dd($xml->channel->item[$i]);
        }
        dd($pubDate);
    }
}
