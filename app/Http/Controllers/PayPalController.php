<?php

namespace App\Http\Controllers;

use App\Models\TeleConsultation;
use App\Models\UserTeleConsultation;
use Illuminate\Http\Request;
use App\Models\PayPalOrder;
use App\Models\Course;
use App\Models\UserCourse;
use App\Models\ContactInfo;
use DB;
use Auth;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Log;
use Session;

class PayPalController extends Controller
{

    public function create(Course $course)
    {
        $course = Course::find($course['id']);

        dd($course);
        $pr = DB::table('pricing')->where('type', Auth::user()->type)->first();
        $discount = $course->price - (($course->price * $pr->course_percent) / 100);
        $discount = round(intval($discount),2);
        if ($discount < 1) {
            $discount = 1;
        }

        $unit = array('amount' => array('currency_code' => 'USD', 'value' => $discount));
        $Amount = array('intent' => 'CAPTURE', 'purchase_units' => [$unit]);
        if ($getToken = $this->getAccessToken()) {
            if ($getToken['status']) {
                $token = $getToken['token'];
            } else {
                $data['status'] = false;
                return $data;
            }
        }
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, 'https://api.paypal.com/v2/checkout/orders');
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($Amount));

        $headers = array();
        $headers[] = 'Content-Type: application/json';
        $headers[] = 'Authorization: Bearer ' . $token;
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);

        $result = curl_exec($ch);
        if (curl_errno($ch)) {
            $data['status'] = false;
            return $data;
        }
        //return [json_decode($result)];//UnHash this line for testing
        $order = new PayPalOrder;
        $order->user_id = Auth::id();
        $order->item_id = $course['id'];
        $order->total = $discount;
        $order->response = $result;
        $order->response_code = json_decode($result)->status;
        $order->type = 'create';
        $order->paypal_order_id = json_decode($result)->id;
        $order->order_type_id = 1;
        $order->save();
        curl_close($ch);
        return $result;
    }

    public function show($orderId)
    {
        $ch = curl_init();
        $paypalOrder = PayPalOrder::where('paypal_order_id', $orderId)->first();
        curl_setopt($ch, CURLOPT_URL, 'https://api.paypal.com/v2/checkout/orders/' . $orderId . '/capture');
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, "{}");

        $headers = array();
        if ($getToken = $this->getAccessToken()) {
            if ($getToken['status']) {
                $token = $getToken['token'];
            }
        }
        $headers[] = 'Authorization: Bearer ' . $token;
        $headers[] = 'Paypal-Request-Id: 7b92603e-77ed-4896-8e78-5dea2050476a';
        $headers[] = 'Content-Type: application/json';
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);

        $result = curl_exec($ch);
        if (curl_errno($ch)) {
            echo 'Error:' . curl_error($ch);
        }
        curl_close($ch);
        $order = new PayPalOrder;
        $order->user_id = Auth::id();
        $order->item_id = $paypalOrder->item_id;
        $order->total = $paypalOrder->total;
        $order->response = $result;
        $order->response_code = json_decode($result)->status;
        $order->type = 'payment';
        $order->paypal_order_id = json_decode($result)->id;
        $order->order_type_id   = 1;
        $order->save();
        $arr['class'] = 'danger';
        $arr['mssg'] = 'UnSuccessful Payment Please Try Again in a moment!!';
        if ($order->response_code == 'COMPLETED') {
            $paid = new UserCourse;
            $paid->user_id = Auth::id();
            $paid->course_id = $order->item_id;
            $paid->payed = 1;
            $paid->price = $order->total;
            $paid->paypal_order_id = $order->id;
            $paid->save();
            $arr['class'] = 'success';
            $arr['mssg'] = 'Course Payment Done .. Just Complete Your Quiz !!';
        }
        Session::put('paid', $arr);
        return $result;
    }

    public function create2($pkgId)
    {
        $pkg = DB::table('pricing')->where('id', $pkgId)->first();

        $unit = array('amount' => array('currency_code' => 'USD', 'value' => $pkg->price));
        $Amount = array('intent' => 'CAPTURE', 'purchase_units' => [$unit]);

        if ($getToken = $this->getAccessToken()) {
            if ($getToken['status']) {
                $token = $getToken['token'];
            } else {
                $data['status'] = false;
                $data['error'] = $getToken['error'];
                return $data;
            }
        }

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, 'https://api.paypal.com/v2/checkout/orders');
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($Amount));

        $headers = array();
        $headers[] = 'Content-Type: application/json';
        $headers[] = 'Authorization: Bearer ' . $token;
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);

        $result = curl_exec($ch);
        if (curl_errno($ch)) {
            echo 'Error:' . curl_error($ch);
        }
        $order = new PayPalOrder;
        $order->user_id = Auth::id();
        $order->item_id = $pkgId;
        $order->total = $pkg->price;
        $order->response = $result;
        $order->response_code = json_decode($result)->status;
        $order->type = 'create';
        $order->paypal_order_id = json_decode($result)->id;
        $order->order_type_id   = 2;
        $order->save();
        curl_close($ch);
        return $result;
    }

    public function show2($orderId)
    {
        $user = Auth::user();
        $ch = curl_init();
        $paypalOrder = PayPalOrder::where('paypal_order_id', $orderId)->first();
        curl_setopt($ch, CURLOPT_URL, 'https://api.paypal.com/v2/checkout/orders/' . $orderId . '/capture');
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, "{}");

        $headers = array();
        if ($getToken = $this->getAccessToken()) {
            if ($getToken['status']) {
                $token = $getToken['token'];
            }
        }
        $headers[] = 'Authorization: Bearer ' . $token;
        $headers[] = 'Paypal-Request-Id: 7b92603e-77ed-4896-8e78-5dea2050476a';
        $headers[] = 'Content-Type: application/json';
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);

        $result = curl_exec($ch);
        if (curl_errno($ch)) {
            echo 'Error:' . curl_error($ch);
        }
        curl_close($ch);
        $order = new PayPalOrder;
        $order->user_id = Auth::id();
        $order->item_id = $paypalOrder->item_id;
        $order->total = $paypalOrder->total;
        $order->response = $result;
        $order->response_code = json_decode($result)->status;
        $order->type = 'payment';
        $order->paypal_order_id = json_decode($result)->id;
        $order->order_type_id   = 2;
        $order->save();
        $arr['class'] = 'danger';
        $arr['mssg'] = 'UnSuccessful Payment Please Try Again in a moment!!';
        if ($order->response_code == 'COMPLETED') {
            $user->active = 1;
            $user->save();
            $arr['class'] = 'success';
            $arr['mssg'] = 'Payment Successfully!!';
        }
        Session::put('paid', $arr);
        return $result;
    }

    public function create3(TeleConsultation $teleConsultation)
    {
        $response = Http::createPaypalOrder($teleConsultation->generateAmountBody());

        if (!$response->successful()) {

            Log::error('Paypal Create Order',$response->json());

            return [
                'status' => false,
            ];
        }

        $order = new PayPalOrder;
        $order->user_id = auth()->id();
        $order->item_id = $teleConsultation->id;
        $order->total = $teleConsultation->price;
        $order->response = $response;
        $order->response_code = $response->status();
        $order->type = 'create';
        $order->paypal_order_id = $response->json('id');
        $order->order_type_id = TeleConsultation::PayPalType;// = 3 see model
        $order->save();

        return $response->json();
    }

    public function show3($orderId)
    {
        $user = Auth::user();
        $ch = curl_init();
        $paypalOrder = PayPalOrder::where('paypal_order_id', $orderId)->first();
        curl_setopt($ch, CURLOPT_URL, config('paypal.domain') . 'checkout/orders/' . $orderId . '/capture');
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, "{}");

        $headers = array();
        if ($getToken = $this->getAccessToken()) {
            if ($getToken['status']) {
                $token = $getToken['token'];
            }
        }
        $headers[] = 'Authorization: Bearer ' . $token;
        $headers[] = 'Paypal-Request-Id: 7b92603e-77ed-4896-8e78-5dea2050476a';
        $headers[] = 'Content-Type: application/json';
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);

        $result = curl_exec($ch);
        if (curl_errno($ch)) {
            echo 'Error:' . curl_error($ch);
        }
        curl_close($ch);
        $order = new PayPalOrder;
        $order->user_id = Auth::id();
        $order->item_id = $paypalOrder->item_id;
        $order->total = $paypalOrder->total;
        $order->response = $result;
        $order->response_code = json_decode($result)->status;
        $order->type = 'payment';
        $order->paypal_order_id = json_decode($result)->id;
        $order->order_type_id = TeleConsultation::PayPalType;// = 3 see model
        $order->save();
        $arr['class'] = 'danger';
        $arr['mssg'] = 'UnSuccessful Payment Please Try Again in a moment!!';
        if ($order->response_code == 'COMPLETED') {
            $UserTeleConsultation                  = new UserTeleConsultation;
            $UserTeleConsultation->user_id         = Auth::id();
            $UserTeleConsultation->tele_consultation_id = $order->item_id;
            $UserTeleConsultation->paied           = 1;
            $UserTeleConsultation->price           = $order->total;
            $UserTeleConsultation->paypal_order_id = $order->id;
            $UserTeleConsultation->save();
            $arr['class'] = 'success';
            $arr['mssg'] = 'Payment Successfully!!';
        }
        Session::put('paid', $arr);
        return $result;
    }

    public function getAccessToken()
    {
        $getPayPalAccountInfo = ContactInfo::first();
        $clientId = $getPayPalAccountInfo->paypal_client_id;
        $secret = $getPayPalAccountInfo->paypal_secret;

        if (\app()->environment('local')) {
            $clientId = config('paypal.client_id');
            $secret = config('paypal.secret');
        }

        $baseCoder = "access_token\$sandbox\$rhzz26hxvdn8h4q2$421f1ebf555ba2dd1df699f94825e848";

        $curl = curl_init();

        curl_setopt_array($curl, array(
            CURLOPT_URL => "https://api.paypal.com/v1/oauth2/token",
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "POST",
            CURLOPT_POSTFIELDS => "grant_type=client_credentials",
            CURLOPT_HTTPHEADER => array(
                "Authorization: Bearer " . $baseCoder,
                "Content-Type: application/x-www-form-urlencoded"
            ),
        ));

        $result = curl_exec($curl);

        curl_close($curl);
        $json['status'] = false;
        $json['error'] = $result;
        if (empty($result)) {

        } else {
            if (isset(json_decode($result)->access_token)) {
                $json['token'] = json_decode($result)->access_token;
                $json['status'] = true;
            }
        }
        return $json;
    }
}
