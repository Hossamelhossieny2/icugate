<?php

namespace App\Http\Controllers;
use Illuminate\Support\Traits\Macroable;
use Illuminate\Http\Request;
use DB;
use App\Models\User;
use App\Models\Post;
use App\Models\Team;
use App\Models\Tag;
use App\Models\Course;
use App\Models\CourseCat;
use App\Models\CatDoc;
use App\Models\Ccat;
use App\Models\Youtube;
use App\Models\Critical;
use App\Models\ContactInfo;
use App\Models\Contact;
use App\Models\Pricing;
use Illuminate\Support\Facades\Hash;

class ApiController extends Controller
{
    use Macroable;

    public function HomeApp($label) {
        //return $label;
        $limit = 6;
        $cats   = Tag::get();
        $team = Team::where('id','!=','1')->get();
        $team_fixed = Team::where('id','1')->first();
        $famous = Post::orderByDesc('date')->where('cat_id','!=',22)->where('famous','1')->first();
        $tag = Tag::where('title', $label)->first();
        if($label == 'all'){
            $hots   = Post::orderByDesc('date')->where('cat_id','!=',22)->where('cat_id','12')->paginate($limit);
            $news   = Post::orderByDesc('date')->where('cat_id','!=',22)->where('cat_id','!=','12')->paginate($limit);
        }else{
            $hots   = Post::where('tag_id',$tag->id)->orderByDesc('date')->where('cat_id','!=',22)->where('cat_id','12')->paginate($limit);
            $news   = Post::where('tag_id',$tag->id)->orderByDesc('date')->where('cat_id','!=',22)->where('cat_id','!=','12')->paginate($limit);
        }

        return [
            'msg'       => 'done',
            'img_url'   => url('public/up'),
            'data'      =>[
                        'lables'        => $cats,
                        'month_article' => $famous,
                        'hot_topics'    => $hots,
                        'news'          => $news,
                        'team_fixed'    => $team_fixed,
                        'team'          => $team
                          ]
                ];
    }
    
    public function Archive($label) {
        $limit = 6;
        $tag = Tag::where('title', $label)->first();
        if($label == 'all'){
            $news   = Post::orderByDesc('date')->where('cat_id','=',22)->where('cat_id','!=','12')->paginate($limit);
        }else{
            $news   = Post::where('tag_id',$tag)->orderBy('date','desc')->where('cat_id','=',22)->where('cat_id','!=','12')->paginate($limit);
        }
        
        
        return [
            'msg'       => 'done',
            'img_url'   => url('public/up'),
            'data'      =>[
                        'news'          => $news,
                          ]
                ];
    }
    
    function Courses($id=0) {
        $limit = 6;
        $cats = CourseCat::select('name','id')->get();
        if($id == 0){
            $ccat = " ";
            $cat = " ";
            $old_courses = ['data'=>" "];
            $new_courses = ['data'=>" "];

            $docs = ['data'=>" "];
            $vids = ['data'=>" "];
            
            $l1 = '';
            $l2 = '';
            $l3 = '';
        }else{
            $cat = CourseCat::where('id',$id)->select('name')->first();
            $old_courses = Course::where('type',$id)->where('to','<',date("Y-m-d"))->paginate($limit);
            $new_courses = Course::where('type',$id)->where('to','>=',date("Y-m-d"))->paginate($limit);

            $docs = CatDoc::where('cat_id',$id)->paginate($limit);
            //$docs[] = ["image"=>"https://icugate.com/public/assets/images/pdf-doc.png"];
            $vids = Youtube::where('need_change',0)->where('playlist',$cat->name)->paginate($limit);
            $ccat = Ccat::where('id',$id)->first();

            if(count($old_courses) == 0){
                $old_courses = [
                                        'data'=>[]
                                        ];
            }
            if(count($new_courses) == 0){
                $new_courses = [
                                        'data'=>[]
                                        ];
            }

            if(count($docs) == 0){
                $docs = [
                                'data'=>[]

                                        ];
            }

            if(count($vids) == 0){
                $vids = [
                                'data'=>[]
                                        ];
            }


        }
        
        
        return [
            'msg'       => 'done',
            'img_url'   => url('public/up'),
            'data'      =>[
                        'all_cats'          => $cats,
                        'course_cat'        => $cat,
                        'prev_courses'      => $old_courses,
                        'new_courses'       => $new_courses,
                        'ppt_presentation'  => $docs,
                        'videos'            => [
                                                'msg'       => 'done',
                                                'apikey'    => 'AIzaSyCbFWy7rsymYqsMynrKeNsIeVRgRz7yi1c',
                                                'data'      => $vids,
                                                    ],
                        'OngoingResearch'   => $ccat ,
                        'course_link'       => 'https://icugate.com/join_course/',
                        'pdf_image'         => "https://icugate.com/public/assets/images/pdf-doc.png",
                          ]
                ];
    }
    
    function Youtube() {
        $limit =6;
        $youtube = Youtube::where('need_change',0)->paginate($limit);
        return [
            'msg'       => 'done',
            'apikey'    => 'AIzaSyCbFWy7rsymYqsMynrKeNsIeVRgRz7yi1c',
            'data'      =>[
                        'youtube'          => $youtube,
                          ]
                ];
    }
    
    public function Calendar()
    {
        $limit =6;
        $critical = Critical::orderByDesc('date_from')->paginate($limit);
        if(count($critical) == 0){
                $critical = 'no events right now ';
            }
        
        return [
            'msg'       => 'done',
            'data'      =>[
                        'conferences'          => $critical,
                          ]
                ];
    }
    
    public function Config()
    {
        
        $conf = ContactInfo::first();
        $youtube = 'https://www.youtube.com/embed/CF21OwIrS_M';
        
        return [
            'msg'       => 'done',
            'data'      =>[
                        'config'          => $conf,
                        'icu_tube'        => [
                                                'apikey'    => 'AIzaSyCbFWy7rsymYqsMynrKeNsIeVRgRz7yi1c',
                                                'video_id'  => 'CF21OwIrS_M',
                                                'title'     => 'about icugate',
                                                'img'       => 'https://i.ytimg.com/vi/CF21OwIrS_M/hqdefault.jpg',
                                                    ],
                          ]
                ];
    }
    
    public function Search(Request $request) {
        
        $limit =6;
        if ($request->isMethod('post')) {
            $res = Post::where('title','Like', "%{$request->word}%")->paginate($limit);
            $dat = 'done';
        }else{
            $res = "no data ";
            $dat = 'not done';
        }
        return [
            'msg'       => $dat,
            'data'      =>[
                        'result'          => $res,
                          ]
                ];
    }

    public function residents_lecture() {
        $limit = 6;
       
        $rl   = Post::where('cat_id',15)->orderByDesc('date')->paginate($limit);
        
        return [
            'msg'        => 'done',
            'file_url'   => url('public/up'),
            'link_url'   => url('public/'),
            'data'       => $rl
                ];
    }
    
    public function Contact(Request $request) {
        
        if ($request->isMethod('post')) {
            $new_contact = new Contact;
            $new_contact->name = $request['name'];
            $new_contact->email = $request['email'];
            $new_contact->phone = $request['phone'];
            $new_contact->subject = "Sent from mobile App";
            $new_contact->message = $request['message'];
            $new_contact->save();
            $dat = 'true';
        }else{
            $dat = 'false';
        }
        return [
            'msg'       => $dat,
                ];
    }
    
    public function get_search() {
            
                $posts = Post::where('title','Like', "%{$_GET['word']}%")->orderByDesc('date')->paginate(5);
                if(count($posts) > 0){
                    $dat = 'done';
                }else{
                    $dat = 'not done';
                }
                
                return [
                    'msg'       => $dat,
                    'data'      =>[
                                'result'  => $posts,
                                  ]
                        ];
    }

    public function login(Request $request)
    {
         $data = $request->validate([
                'usermail'              => 'required',
                'userpass'               => 'required',
            ]);

        $usr = User::where('email',$data['usermail'])->first();
        if(!empty($usr->id)){
            if(Hash::check($data['userpass'], $usr->password)){
                $user_type = Pricing::where('type',$usr->type)->first();
                $token = $usr->createToken("API TOKEN")->plainTextToken;
                $user = [
                    'name' => $usr->f_name.' '.$usr->l_name,
                    'email'  => $usr->email,
                    'sub'   => $user_type->title,
                    'logo'  => $user_type->img,
                    'type'  => $usr->type,
                    'active' => $usr->active,
                    'token'  => $token,
                ];
                $dat = $user;
                $stat = true;
                
                User::where('id',$usr->id)->update(['app_token'=>$token]);
            }else{
                $dat = 'pass error';
                $stat = false;
                
            }
        }else{
            $dat = 'user not found';
            $stat = false;
            
        }

        return [
                        'user'          => $dat,
                        'status'        => $stat
                                      
                            ];
        }

        public function register(Request $request)
        {
             $data = $request->validate([
                'first_name'         => 'required',
                'last_name'          => 'required',
                'mobile'             => 'required',
                'email'              => 'required',
                'pass'               => 'required',
            ]);

            $usr = User::where('email',$data['email'])->first();
            if(!empty($usr->id)){
                return [
                    'msg' => 'user exist',
                    'token' => 'null'
                ];
            }else{
            
            $new_user = new User;
            $new_user->f_name = $data['first_name'];
            $new_user->l_name = $data['last_name'];
            $new_user->mobile = $data['mobile'];
            $new_user->email = $data['email'];
            $new_user->password = Hash::make($data['pass']);
            $new_user->type = 0;
            $new_user->active = 1;
            $new_user->save();

            $token = $new_user->createToken("API TOKEN")->plainTextToken;
            $new_user->app_token = $token;
            $new_user->save();

            if($new_user['id']){
                $user_type = Pricing::where('type',$new_user->type)->first();
                return [
                    'user'    => [
                            'name'   => $new_user['f_name'].' '.$new_user['l_name'],
                            'email'  => $new_user['email'],
                            'sub'    => $user_type['title'],
                            'logo'   => $user_type['img'],
                            'type'   => $new_user['type'],
                            'active' => $new_user['active'],
                            'token'  => $new_user['app_token']
                            ],
                    'status'  => 'true'
                    
                ];
            }else{
                return [
                    'user' => 'error',
                    'status' => 'false'
                ];
            }
        }

        }

        public function logout(Request $request)
        {

             $data = $request->validate([
                'token'         => 'required',
            ]);

            $usr = User::where('app_token',$data['token'])->first();
            if(!empty($usr->id)){
                User::where('id',$usr->id)->update(['app_token'=>' ']);
                return [
                    'msg' => 'token removed',
                ];
            }else{
                return [
                    'msg' => 'error token',
                ];
            }
        }
    
    
}

