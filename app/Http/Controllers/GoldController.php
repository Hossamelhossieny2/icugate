<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Post;
use App\Models\Cat;
use App\Models\ContactInfo;
use App\Models\Event;
use App\Models\Tag;

class GoldController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function goldarticle(Cat $cat,Tag $tag)
    {   
        $arr['usertype'] = "gold";
        $arr['menu'] = 'article';
        $arr['conf'] = ContactInfo::first();
        $arr['latest'] = Post::where('famous',1)->where('cat_id',$cat->id)->where('tag_id',$tag->id)->orderByDesc('id')->limit(5)->get();
        $arr['montharticle'] = Post::where('month_article',1)->orderByDesc('id')->first();
        
        $arr['cats'] = Cat::where('shows','1')->withCount('post')->get();

        $allTags = Tag::get();
        for($i=0;$i<11;$i++){
            $allTags[$i]['posts'] = Post::where('cat_id',$cat->id)->where('tag_id',$allTags[$i]['id'])->orderByDesc('id')->paginate(10);
            $allTags[$i]['count'] = Post::where('cat_id',$cat->id)->where('tag_id',$allTags[$i]['id'])->count();
        }
        $arr['tags'] = $allTags;
        $arr['posts'] = $allTags[$tag['id']-1]['posts'];
        $arr['events']   = Event::whereDate('event_date','>=', \Carbon\Carbon::now()->format('Y-m-d'))->get();
        $arr['hot_topics'] = Post::where('cat_id',12)->where('tag_id',$tag->id)->orderByDesc('id')->limit(12)->get();
        $arr['cat_tag'] = [
            'cat_id' => $cat['id'],
            'cat_title' => $cat['title'],
            'tag_id' => $tag['id'],
            'tag_title' => $tag['title'],
        ];
        $arr['usertype'] = "gold";

        return view('articles',$arr);
    }

     public function goldcme(Cat $cat,Tag $tag)
    {   // cats id allowed here
        // 13 ICU Scoring Calculators
        // 14 Drug Interactions Checker
        // 17 Members Contribution
        // 18 Video
        // 19 ICU PROCEDURES
        // 20 Ongoing Researches

        $arr['usertype'] = "gold";
        if($cat->id != 13 && $cat->id != 14){
            $arr['menu'] = 'cme';
        }else{
            $arr['menu'] = 'premuim';
        }
        
        $arr['conf'] = ContactInfo::first();
        $arr['latest'] = Post::where('famous',1)->where('cat_id',$cat->id)->where('type','<',2)->orderByDesc('id')->limit(5)->get();
        $arr['montharticle'] = Post::where('month_article',1)->orderByDesc('id')->first();

        $allTags = Tag::get();
        for($i=0;$i<11;$i++){
            $allTags[$i]['posts'] = Post::with('block')->where('cat_id',$cat->id)->where('tag_id',$allTags[$i]['id'])->orderByDesc('id')->paginate(10);
            $allTags[$i]['count'] = Post::where('cat_id',$cat->id)->where('tag_id',$allTags[$i]['id'])->count();
        }
        $arr['tags'] = $allTags;
        $arr['posts'] = $allTags[$tag['id']-1]['posts'];
        // dd($arr['latest']);
        $arr['cats'] = Cat::where('shows','1')->get();
        $arr['hot_topics'] = Post::where('cat_id',12)->orderByDesc('id')->limit(12)->get();
        $arr['events']   = Event::whereDate('event_date','>=', \Carbon\Carbon::now()->format('Y-m-d'))->get();
        $arr['cat_tag'] = [
            'cat_id' => $cat['id'],
            'cat_title' => $cat['title'],
            'tag_id' => $tag['id'],
            'tag_title' => $tag['title']
        ];
        return view('cme',$arr);
    }

    public function premuim(Cat $cat,Tag $tag){
        // cats id allowed here
        // 15 Residents Lecture
        // 16 ICU Club Presentations

        $arr['conf'] = ContactInfo::first();
        if($cat->id != 13 && $cat->id != 14){
            $arr['menu'] = 'cme';
        }else{
            $arr['menu'] = 'premuim';
        }
        $arr['usertype'] = "gold";
        $arr['latest'] = Post::where('famous',1)->where('cat_id',$cat->id)->orderByDesc('id')->where('type','<',2)->limit(5)->get();
        $arr['montharticle'] = Post::where('month_article',1)->orderByDesc('id')->first();
        $arr['events']   = Event::whereDate('event_date','>=', \Carbon\Carbon::now()->format('Y-m-d'))->get();
        $allTags = Tag::get();
        for($i=0;$i<11;$i++){
            $allTags[$i]['posts'] = Post::where('cat_id',$cat->id)->where('tag_id',$allTags[$i]['id'])->orderByDesc('id')->paginate(10);
            $allTags[$i]['count'] = Post::where('cat_id',$cat->id)->where('tag_id',$allTags[$i]['id'])->count();
        }
        $arr['tags'] = $allTags;
        $arr['posts'] = $allTags[$tag['id']-1]['posts'];
        $arr['cats'] = Cat::where('shows','1')->get();
        $arr['hot_topics'] = Post::where('cat_id',12)->orderByDesc('id')->limit(12)->get();
        $arr['cat_tag'] = [
            'cat_id' => $cat['id'],
            'cat_title' => $cat['title'],
            'tag_id' => $tag['id'],
            'tag_title' => $tag['title']
        ];
        
        return view('cme',$arr);
    }

    
}
