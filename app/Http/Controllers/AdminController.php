<?php

namespace App\Http\Controllers;
use App\Http\Requests\PostRequest;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Http\Request;
use App\Http\Requests;
use Illuminate\Support\Facades\Input;
use App\Notifications\VerifyEmail;
use Carbon\Carbon;
use File;
use Auth;
use App\Models\User;
use App\Models\Post;
use App\Models\PostImg;
use App\Models\PostType;
use App\Models\Event;
use App\Models\JobList;
use App\Models\ManualPay;
use App\Models\PayPalOrder;
use DB;


class AdminController extends Controller
{
    private $app = [];

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $data['page'] = 'Admin dashboard';
        $data['menu'] = 'dash';
        $data['title'] = 'Admin | dashboard';
        $data['courses'] = \App\Models\Course::all();
        $query = \App\Models\UserCourse::leftjoin('courses','courses.id','=','user_course.course_id')->leftjoin('users','users.id','=','user_course.user_id')->select('users.f_name as f_name','users.l_name as l_name','users.email as email','users.mobile as mobile','users.profession as profession','courses.title as title','user_course.date as date','user_course.id as pay_id','user_course.paid as paid','user_course.price as price','user_course.id as course_id','user_course.quiz as quiz','user_course.test as test');
        
        if(!empty($_GET['from'])){
            $query->where('user_course.date','>',$_GET['from']);
        }
        if(!empty($_GET['to'])){
            $query->where('user_course.date','<',$_GET['to']);
        }
        if(!empty($_GET['email'])){
            $query->where('users.email',$_GET['email']);
        }
        if(!empty($_GET['course'])){
            $query->where('user_course.id',$_GET['course']);
        }
        if(!empty($_GET['type'])){
            if($_GET['type']  == 1){
                $ser =1;
            }else{
                 $ser =0;
            }
            $query->where('user_course.paid',$ser);
        }
        $data['data'] = $query->get();
        $data['stat'] = [
            'users' => User::count(),
            'users2'=> User::where('active',0)->count(),
            'posts' => Post::count(),
            'posts2' => Post::where('tag_id','')->count(),
            'youtube'=> \App\Models\Youtube::count(),
            'youtube2'=> \App\Models\Youtube::where('need_change','1')->count(),
            'visit' => \App\Models\ContactInfo::first()->site_visit,
            'cv' => \App\Models\Job::count(),
            'cv2'=> \App\Models\Job::where('seen','0')->count(),
            'cor' => \App\Models\Course::where('type','!=','3')->count(),
            'cor2'=> \App\Models\Course::where('type','3')->count()
        ];
        
        return view('admin.dash',$data);
    }

    public function site()
    {
        $data['page'] = 'site info';
        $data['menu'] = 'setting';
        $data['title'] = 'Admin | site info';
        $data['site'] = \App\Models\ContactInfo::first();
        return view('admin.site.view',$data);
    }

    public function site_edit()
    {
        $data['page'] = 'site info';
        $data['menu'] = 'setting';
        $data['title'] = 'Admin | site info';
        $data['site'] = $site = \App\Models\ContactInfo::first();
        if (!empty($_POST)) {
            unset($_POST['_token']);
            $site->update($_POST);
            
            return redirect(route('admin.site'))->with('status', ['success edits modified','success']);
        }
        return view('admin.site.edit',$data);
    }
    
    public function site_social()
    {
        $data['page'] = 'site info';
        $data['menu'] = 'setting';
        $data['title'] = 'Admin | site info';
        $data['site'] = $site = \App\Models\ContactInfo::first();
        if (!empty($_POST)) {
            unset($_POST['_token']);
            $site->update($_POST);
            
            return redirect(route('admin.site'))->with('status', ['success edits modified','success']);
        }
        return view('admin.site.social',$data);
    }
    
     public function pricing()
    {
        $data['page'] = 'pricing info';
        $data['menu'] = 'setting';
        $data['title'] = 'Admin | pricing info';
        $data['data'] = \App\Models\Pricing::all();
        return view('admin.pricing.view',$data);
    }

    public function pricing_edit(\App\Models\Pricing $pricing)
    {
        $data['page'] = 'pricing info';
        $data['menu'] = 'setting';
        $data['title'] = 'Admin | pricing info edit';
        $data['data'] = $one_pricing = \App\Models\Pricing::find($pricing->id);
        //dd($one_pricing);
        if (!empty($_POST)) {
            unset($_POST['_token']);
            $one_pricing->update($_POST);
            
            return redirect(route('admin.pricing'))->with('status', ['success edits modified','success']);
        }
        return view('admin.pricing.edit',$data);
    }
    
     public function tele()
    {
        $data['page'] = 'Consultation info';
        $data['menu'] = 'setting';
        $data['title'] = 'Admin | Consultation info';
        $data['data'] = \App\Models\TeleConsultation::all();
        return view('admin.tele.view',$data);
    }


    public function post()
    {
        $data['page'] = 'post info';
        $data['menu'] = 'post';
        $data['title'] = 'Admin | post info';
        $data['data'] = Post::where('month_article',0)->orderby('id','desc')->paginate(10);
        return view('admin.post.view',$data);
    }

    public function month_article()
    {
        $data['page'] = 'article  of the  month info';
        $data['menu'] = 'post';
        $data['title'] = 'Admin | article  of the  month info';
        $data['data'] = Post::where('month_article',1)->OrderByDesc('id')->paginate(10);

        return view('admin.post.view',$data);
    }

     public function post_mc()
    {
        $data['page'] = 'member post info';
        $data['menu'] = 'post';
        $data['title'] = 'Admin | post info';
        $data['data'] = Post::with('user_detail')->where('user','!=',0)->orderByDesc('id')->get();
        
        return view('admin.post.view_mc',$data);
    }

    public function newpost()
    {
        $data['page'] = 'new post info';
        $data['menu'] = 'post';
        $data['title'] = 'Admin | add new post details';
        
        $data['imgs'] = PostImg::all();
        $data['tags'] = \App\Models\Tag::all();
        $data['cats'] = \App\Models\Cat::orderBy('title','asc')->get();
        $data['post_types'] = PostType::all();

        return view('admin.post.new',$data);
    }

    public function addnewpost(PostRequest $request)
    {
            if(!empty($request['famous']))
            {
               $famous = 1; 
            }else{
                $famous = 0;
            }

            if(!empty($request['journal']))
            {
               $by = $request['journal']; 
            }else{
                $by = 'site admin';
            }
            
            if(!empty($request->file('image'))){
                $file = $request->file('image');
                $path = public_path('/up');
                $file_name = time().rand(1,100).'.'.$file->extension();
                $file->move($path,$file_name);

            }else{
                $file_name = NULL;
            }
            // post type 0 = news / 1 = link on other site / 2 = research / 3 presentation
            $this_post = new Post;
            $this_post->img = $request['img'];
            $this_post->title = $request['post_title'];
            $this_post->cat_id = $request['cat_id'];
            $this_post->tag_id = $request['tag_id'];
            $this_post->link = $request['post_link'];
            $this_post->file = $file_name;
            $this_post->by = 0;
            $this_post->show = 0;
            $this_post->text = $request['post_desc'];
            $this_post->journal = $by;
            $this_post->type = $request['post_type'];
            $this_post->active = $request['post_active'];
            $this_post->date = Carbon::now();
            $this_post->famous = $famous;
            $this_post->user = auth()->user()->id;
            $this_post->save();

            if($request['post_active'] == '0'){
                return redirect(route('admin.post'))->with('status', ['success Post add but still not Active','warning']);
            }else{
                return redirect(route('admin.post'))->with('status', ['success Post add','success']);
            }
        
    }

    public function post_edit(Post $post)
    {
        $data['page'] = 'post info';
        $data['menu'] = 'post';
        $data['title'] = 'Admin | post info edit';
        $data['post'] = $this_post = Post::with('user_detail')->find($post->id);
        $data['imgs'] = PostImg::all();
        $data['tags'] = \App\Models\Tag::all();
        $data['cats'] = \App\Models\Cat::all();
        $data['post_types'] = PostType::all();
        
        return view('admin.post.edit',$data);
    }

    public function post_edit_do(PostRequest $request)
    {
            if(!empty($request['famous']))
            {
               $famous = 1; 
            }else{
                $famous = 0;
            }

            if(!empty($request['journal']))
            {
               $by = $request['journal']; 
            }else{
                $by = 'site admin';
            }
            
            $this_post = Post::find($request['post_id']);

            
            if(!empty($request->file('image'))){
                $file = $request->file('image');
                $path = public_path('/up');
                $file_name = time().rand(1,100).'.'.$file->extension();
                $file->move($path,$file_name);

            }else{
                $file_name = $this_post->file;
            }

            if($this_post->file != $file_name){
                unlink('up/'.$this_post->file);
            }

            $this_post->img = $request['img'];
            $this_post->title = $request['post_title'];
            $this_post->cat_id = $request['cat_id'];
            $this_post->tag_id = $request['tag_id'];
            $this_post->link = $request['post_link'];
            $this_post->file = $file_name;
            $this_post->by = 0;
            $this_post->show = 0;
            $this_post->text = $request['post_desc'];
            $this_post->journal = $by;
            $this_post->type = $request['post_type'];
            $this_post->active = $request['post_active'];
            $this_post->date = Carbon::now();
            $this_post->famous = $famous;
            $this_post->user = auth()->user()->id;
            $this_post->save();

            

            if($request['post_active'] == '0'){
                return redirect(route('admin.post'))->with('status', ['success Post add but still not Active','warning']);
            }else{
                return redirect(route('admin.post'))->with('status', ['success Post add','success']);
            }
    }

    public function del_post(Post $post)
    {
        $this_post = Post::find($post->id);
        unlink('up/'.$this_post->file);
        $this_post->delete();
        return redirect(route('admin.post'))->with('status', ['success Deleted .. ','success']);
    }

    public function event()
    {
        $data['page'] = 'event info';
        $data['menu'] = 'event';
        $data['title'] = 'Admin | event info';
        $data['data'] = Event::orderby('id','desc')->get();
        return view('admin.event.view',$data);
    }

    public function newevent(Request $request)
    {
        $data['page'] = 'new event info';
        $data['menu'] = 'event';
        $data['title'] = 'Admin | post info add';
        
        return view('admin.event.new',$data);
    }

    public function neweventadd(Request $request)
    {
        $validatedData = $request->validate([
                'image' => 'mimes:jpeg,jpg,png,gif|max:10000', // max 10000kb
                'event_title' => 'required|max:255|string',
                'event_link' => 'required|url',
                'event_desc' => 'required|max:255|string',
                'event_by' => 'max:100',
                'event_date' => 'required|date',
            ]);
            
            if(!empty($request->file('image'))){
               $file = $request->file('image');
            
                //Move Uploaded File
                $destinationPath = 'events';
                if(!File::isDirectory($destinationPath)){
                    File::makeDirectory($destinationPath, 0777, true, true);
                }  
                $newname = 'ev_'.time().'_'.$file->getClientOriginalName();
                $file->move($destinationPath,$newname);
                
                $event_image = $newname;
            }else{
                $event_image = "";
            }
            
            if(!empty($validatedData['event_by'])){
                $user_add = $validatedData['event_by'];
            }else{
                $user_add = 'site admin';
            }

            $new_event = new Event;
            $new_event->title = $validatedData['event_title'];
            $new_event->description = $validatedData['event_desc'];
            $new_event->image = $event_image;
            $new_event->link = $validatedData['event_link'];
            $new_event->event_date = $validatedData['event_date'];
            $new_event->author = $user_add;
            $new_event->save();
            
            return redirect(route('admin.event'))->with('status', ['success event added','success']);
        
    }

    public function event_edit(Event $event)
    {
        $data['page'] = 'event info';
        $data['menu'] = 'event';
        $data['title'] = 'Admin | event info edit';
        $data['data'] = $this_event = Event::find($event->id);
       
        return view('admin.event.edit',$data);
    }

    public function do_event_edit(Request $request)
    {
        $validatedData = $request->validate([
                'image' => 'mimes:jpeg,jpg,png,gif|max:10000', // max 10000kb
                'event_title' => 'required|max:255|string',
                'event_link' => 'required|url',
                'event_desc' => 'required|max:255|string',
                'event_by' => 'max:100',
                'event_date' => 'required|date',
            ]);
       
            $old_event = Event::find($request['event_id']);

            if(!empty($request->file('image'))){
                unlink('events/'.$old_event->image);
                
                $file = $request->file('image');
                $destinationPath = 'events';
                $newname = 'ev_'.time().'_'.$file->getClientOriginalName();
                $file->move($destinationPath,$newname);
                $event_image = $newname;
            }else{
                $event_image = $old_event->image;
            }
            
            if(!empty($validatedData['event_by'])){
                $user_add = $validatedData['event_by'];
            }else{
                $user_add = 'site admin';
            }

            $old_event->title = $validatedData['event_title'];
            $old_event->description = $validatedData['event_desc'];
            $old_event->image = $event_image;
            $old_event->link = $validatedData['event_link'];
            $old_event->event_date = $validatedData['event_date'];
            $old_event->author = $user_add;
            $old_event->save();

            return redirect(route('admin.event'))->with('status', ['success event update','success']);
        
    }

    public function del_event(Event $event)
    {
        $this_event = Event::find($event['id']);
        @unlink('events/'.$this_event->image);
        $this_event->delete();

        return redirect(route('admin.event'))->with('status', ['Event success Deleted .. ','success']);
    }

    public function tele_edit(\App\Models\TeleConsultation $teleconsultation)
    {
        $data['page'] = 'Consultation info';
        $data['menu'] = 'setting';
        $data['title'] = 'Admin | Consultation info edit';
        $data['data'] = $telec = \App\Models\TeleConsultation::find($teleconsultation->id);
        if (!empty($_POST)) {
            unset($_POST['_token']);
            $telec->update($_POST);
            
            return redirect(route('admin.tele'))->with('status', ['success edits modified','success']);
        }
        return view('admin.tele.edit',$data);
    }




    

    
    
    
    
    



    

    public function cmepost_edit(Request $request,Post $post,$cat_id)
    {
        $data['page'] = 'CME post info';
        $data['title'] = 'Admin | CME post info edit';
        $data['data'] = $this_post = Post::find($post->id);
        $data['imgs'] = PostImg::all();
        $data['tags'] = \App\Models\Tag::all();
        $data['post_types'] = PostType::all();
        if (!empty($_POST)) {
            unset($_POST['_token']);
            
            $arr_add = [
                'title'=>$request['title'],
                'tag'=>$request['tag'],
                'link'=>$request['link'],
                'text'=>$request['text'],
                'journal'=>$request['journal'],
                'img'=> $request['pic'],
            ];
            
            $this_post->update($arr_add);
            return redirect(route('admin.cmepost',$cat_id))->with('status', ['success edits modified','success']);
        }
        return view('admin.cmepost.edit',$data);
    }
    
    public function cmenewpost(Request $request,\App\Models\Cat $cat)
    {
        $this_cat = \App\Models\Cat::find($cat->id);
        $data['page'] = 'Add CME POST - '.$this_cat->title;
        $data['title'] = 'Admin | CME post info add';
        
        $data['imgs'] = PostImg::all();
        $data['tags'] = \App\Models\Tag::all();
        $data['post_types'] = PostType::all();
        
        if (!empty($_POST)) {
            unset($_POST['_token']);
            //dd($_POST);
            if(!empty($request->file('image'))){
                //Move Uploaded File
                $destinationPath = 'up';
                $file = $request->file('image');
                $newname = 'cme_'.time().'_'.$file->getClientOriginalName();
                $file->move($destinationPath,$newname);
                
                $_POST['file'] = $newname;
                $type = '2';
            }else{
                $newname ="";
                $type = '1';
            }
            
            $arr_add = [
                'title'=>$request['title'],
                'tag'=>$request['tag'],
                'link'=>$request['link'],
                'text'=>$request['text'],
                'journal'=>$request['journal'],
                'img'=> $request['pic'],
                'type'=>$type,
                'category_id'=>$cat->id,
                'file'=> $newname,
                'date'=>Carbon::now(),
            ];
            //dd($arr_add);
            Post::insert($arr_add);
            return redirect(route('admin.cmepost',$cat->id))->with('status', ['success edits modified','success']);
        }
        return view('admin.cmepost.new',$data);
    }
    
    public function del_cmepost(Post $post,$cat_id)
    {
        Post::find($post->id)->delete();
            return redirect(route('admin.cmepost',$cat_id))->with('status', ['success Deleted .. ','success']);
    }


    public function dash_user()
    {
        $data['page'] = 'User activate';
        $data['menu'] = 'user';
        $data['title'] = 'Admin | user activate';
        $data['data'] = User::orderByDesc('id')->where('type','!=','0')->where('active',0)->get();
        return view('admin.user.view',$data);
    }

    public function dash_user_edit(User $user)
    {
        User::find($user->id)->update(['active'=>'1']);
        return redirect(route('admin.user'))->with('status', ['success user activated','success']);
    }

    public function dash_user_deactivate(User $user)
    {
        User::find($user->id)->update(['active'=>'0']);
        return redirect(route('admin.edituser'))->with('status', ['success user deactivated','success']);
    }
    
    public function dash_edituser()
    {
        $data['page'] = 'User activate';
        $data['menu'] = 'user';
        $data['title'] = 'Admin | user activate';
        $data['data'] = User::orderByDesc('id')->where('active','1')->get();
        return view('admin.user.edituser',$data);
    }

    public function dash_edituser_edit(user $user)
    {
        $data['page'] = 'User info';
        $data['menu'] = 'user';
        $data['title'] = 'Admin | User info edit';
        $data['data'] = $this_user = User::find($user->id);
        if (!empty($_POST)) {
            if ($_POST['password'] != "") {
                //dd($_POST['password']);
                
                $pss = bcrypt($_POST['password']);
                //unset($_POST['password']);
                $_POST['password']=$pss;
                
            }
            unset($_POST['_token']);
            $this_user->update($_POST);
            return redirect(route('admin.edituser'))->with('status', ['success edits modified','success']);
        }
        return view('admin.user.edituseredit',$data);
    }
    
    public function del_user(User $user)
    {
            User::find($user->id)->delete();
            return redirect(route('admin.edituser'))->with('status', ['success Deleted .. ','success']);
    }

    public function dash_payuser()
    {
        $data['page'] = 'User Paypal payments';
        $data['menu'] = 'payment';
        $data['title'] = 'Admin | user payments';
        // $data['data'] = DB::table('users')->orderby('id','desc')->where('type','!=','0')->where('active',0)->get();
        //$query = PayPalOrder::leftjoin('users','users.id','=','pay_pal_orders.user_id')->leftjoin('courses','courses.id','=','pay_pal_orders.item_id')->leftjoin('pricing','pricing.type','=','users.type')->select('users.f_name as f_name','users.l_name as l_name','users.email as email','users.mobile as mobile','users.profession as profession','courses.title as title','pay_pal_orders.created_at as date','pay_pal_orders.total as paid','pay_pal_orders.order_type_id as type','pay_pal_orders.paid as status','pay_pal_orders.id as id','pay_pal_orders.total as total','pricing.title as plan')->whereNotNull('pay_pal_orders.user_id');
        $query = PayPalOrder::with(['user','order_type']);
        
        if(!empty($_GET['from'])){
            $query->where('pay_pal_orders.created_at','>',$_GET['from']);
        }
        if(!empty($_GET['to'])){
            $query->where('pay_pal_orders.created_at','<',$_GET['to']);
        }
        if(!empty($_GET['email'])){
            $query->where('users.email',$_GET['email']);
        }
        if(!empty($_GET['status'])){
            $query->where('pay_pal_orders.paid',$_GET['status']);
        }
        if(!empty($_GET['tag'])){
            $query->where('pay_pal_orders.order_type_id',$_GET['tag']);
        }
        $data['data'] = $query->get();
        //dd($data['data']);
        return view('admin.user.pay',$data);
    }
    
    public function user_payment()
    {
        $data['page'] = 'User manual payments';
        $data['menu'] = 'payment';
        $data['title'] = 'Admin | user payments';
        $query = ManualPay::with(['user','admin','order_type']);
        if(!empty($_GET['from'])){
            $query->where('manual_pays.created_at','>',$_GET['from']);
        }
        if(!empty($_GET['to'])){
            $query->where('manual_pays.created_at','<',$_GET['to']);
        }
        if(!empty($_GET['email'])){
            $query->where('users.email',$_GET['email']);
        }
        if(!empty($_GET['payment_type'])){
            $query->where('manual_pays.payment_type',$_GET['payment_type']);
        }
        if(!empty($_GET['amount'])){
            $query->where('manual_pays.amount',$_GET['amount']);
        }
        $data['data'] = $query->get();
        //dd($data['data']);
        return view('admin.user.payreport',$data);
    }

     public function dash_pay_manual(PayPalOrder $paypalorder,$type)
    {
        if($type ==  2){
            $get_course  = PayPalOrder::find($paypalorder->id);
            \App\Models\UserCourse::where('id',$get_course->item_id)->update(['paid'=>1]);
            $cor = \App\Models\Course::where('id',$get_course->item_id)->first();

            $for =  'course-'.$cor->title;
            $user = $get_course->user_id;
            $amount = $get_course->total;
            $item_id = $get_course->item_id;
        }elseif($type ==  1){
            $get_sub  = PayPalOrder::find($paypalorder->id);
            User::where('id',$get_sub->user_id)->update(['paid'=>1,'active'=>1,'type'=>$get_sub->item_id]);
            $for =  'upgrade';
            $user = $get_sub->user_id;
            $amount = $get_sub->total;
            $item_id = $get_sub->item_id;
        }elseif($type ==  3){
            $get_cons  = PayPalOrder::find($paypalorder->id);
            if($get_cons->item_id == 1){
                $for = 'email consultation';
            }else{
                $for = 'zoom consultation';
            }
            $user = $get_cons->user_id;
            $amount = $get_cons->total;
            $item_id = $get_cons->item_id;

            $new_user_tele = new \App\Models\UserTeleConsultation;
            $new_user_tele->user_id = $user;
            $new_user_tele->price = $amount;
            $new_user_tele->paid = 1;
            $new_user_tele->paypal_order_id = $paypalorder->id;
            $new_user_tele->tele_consultation_id = $get_cons->item_i;
            $new_user_tele->save();
            
        }
        $get_this_paypal_order = PayPalOrder::find($paypalorder->id);
        $get_this_paypal_order->response = 'manual';
        $get_this_paypal_order->response_code = 'COMPLETED';
        $get_this_paypal_order->paid = 1;
        $get_this_paypal_order->save();

        $new_payment = new ManualPay;
        $new_payment->user_id = $user;
        $new_payment->pay_user = auth()->id();
        $new_payment->payment_title = $for;
        $new_payment->amount = $amount;
        $new_payment->payment_type = $type;
        $new_payment->item_id = $item_id;
        $new_payment->save();
           
        return redirect(route('admin.payuser'))->with('status', ['success paid .. ','success']);
    }

    // public function dash_paycourse()
    // {
    //     $data['page'] = 'paypal payments';
    //     $data['title'] = 'Admin | user payments';
    //     // $data['data'] = DB::table('users')->orderby('id','desc')->where('type','!=','0')->where('active',0)->get();
    //     $data['data'] = PayPalOrder::where('pay_pal_orders.type','payment')->leftjoin('users','users.id','=','pay_pal_orders.user_id')->leftjoin('courses','courses.id','=','pay_pal_orders.item_id')->select('users.f_name as f_name','users.l_name as l_name','users.email as email','users.mobile as mobile','users.profession as profession','courses.title as title','pay_pal_orders.created_at as date','pay_pal_orders.total as paid','pay_pal_orders.total as paid')->get();
    //     //dd($data['data']);
    //     return view('admin.course.pay',$data);
    // }

    // public function dash_payother()
    // {

    //     $data['page'] = 'other payments';
    //     $data['title'] = 'Admin | user payments';
    //     $data['courses'] = DB::table('courses')->get();
    //     // $data['data'] = DB::table('users')->orderby('id','desc')->where('type','!=','0')->where('active',0)->get();
    //     $query = DB::table('user_course')->leftjoin('courses','courses.id','=','user_course.course_id')->leftjoin('users','users.id','=','user_course.user_id')->select('users.f_name as f_name','users.l_name as l_name','users.email as email','users.mobile as mobile','users.profession as profession','courses.title as title','user_course.date as date','user_course.id as pay_id','user_course.paid as paid','user_course.price as price','user_course.id as course_id','user_course.quiz as quiz','user_course.test as exam');
    //     if(!empty($_GET['from'])){
    //         $query->where('user_course.date','>',$_GET['from']);
    //     }
    //     if(!empty($_GET['to'])){
    //         $query->where('user_course.date','<',$_GET['to']);
    //     }
    //     if(!empty($_GET['email'])){
    //         $query->where('users.email',$_GET['email']);
    //     }

    //     if(!empty($_GET['course'])){
    //         $query->where('user_course.course_id',$_GET['course']);
    //     }

    //     if(!empty($_GET['payment'])){
    //         if($_GET['payment']  == 2){
    //             $ser =  0;
    //         }else{
    //             $ser =  1;
    //         }
    //         $query->where('user_course.paid',$ser);
    //     }
    //     //dd($data['data']);

    //     $data['data'] = $query->get();
    //     return view('admin.course.payother',$data);
    // }

    // public function dash_payother_pay(\App\Models\UserCourse $usercourse)
    // {
    //     \App\Models\UserCourse::find($usercourse->id)->update(['paid'=>1]);
    //         return redirect(route('admin.payother'))->with('status', ['success Deleted .. ','success']);
    // }

    public function dash_course()
    {
        $data['page'] = 'All Cources';
        $data['menu'] = 'course';
        
        $data['title'] = 'Admin | Site courses';
        $data['data'] = \App\Models\Course::with('count_quiz','count_exam')->where('type','!=','3')->orderByDesc('id')->get();
        return view('admin.course.view',$data);
    }

    public function newcourese(Request $request)
    {
        $data['page'] = 'Add new Course';
        $data['menu'] = 'course';
        $data['title'] = 'Admin | Add new Course';
        if (!empty($_POST)) {
            unset($_POST['_token']);
            
            $file = $request->file('image');
            
            //Move Uploaded File
            $destinationPath = 'course';
            if(!File::isDirectory($destinationPath)){
                File::makeDirectory($destinationPath, 0777, true, true);
            }  
            $newname = 'course_'.time().'.'.$file->extension();
            $file->move($destinationPath,$newname);
            
            $_POST['img'] = $newname;
            
            
            \App\Models\Course::insert($_POST);
            return redirect(route('admin.course'))->with('status', ['success added new course','success']);
        }
        return view('admin.course.new',$data);
    }
    
    public function dash_course_edit(Request $request ,\App\Models\Course $course)
    {
        $data['page'] = 'Courses info edit';
        $data['menu'] = 'course';
        $data['title'] = 'Admin | Cources info edit';
        $data['data'] = $this_course = \App\Models\Course::find($course->id);
        if (!empty($_POST)) {
           if(!empty($request->file('image'))){
               $file = $request->file('image');
            
            //Move Uploaded File
            $destinationPath = 'course';
            if(!File::isDirectory($destinationPath)){
                File::makeDirectory($destinationPath, 0777, true, true);
            }  
            $newname = 'course_'.time().'.'.$file->extension();
            $file->move($destinationPath,$newname);
            
            $_POST['img'] = $newname;
           }
            unset($_POST['_token']);
            $this_course->update($_POST);
            return redirect(route('admin.course'))->with('status', ['success edits modified','success']);
        }
        return view('admin.course.edit',$data);
    }
    
    public function del_course(\App\Models\Course $course)
    {
            \App\Models\Course::find($course->id)->delete();
            return redirect(route('admin.course'))->with('status', ['success Deleted .. ','success']);
    }
    
    
    
    public function course_ques($id,$ques=0)
    {
        $data['page'] = 'course Questions info';
        $data['menu'] = 'course';
        $data['title'] = 'Admin | course Questions edit';
        $data['id'] = $id;
        $data['xques'] = $ques;
       
           $data['data'] = \App\Models\CourseQues::where('course_id',$id)->get();
           $course = \App\Models\Course::where('id',$id)->first();
           //dd($course->id);
           if(!empty($course->id)){
               $data['course'] = $course;
           }else{
               $course = new \stdClass() ;
               $course->title = 'not found';
               $data['course'] = $course ;
               
           }
        if($ques  != 0){
            $data['ques'] = \App\Models\CourseQues::where('id',$ques)->first();
            $data['ans'] = \App\Models\CourseAnsOpt::where('q_id',$ques)->get();
        }

        if (!empty($_POST)) {
            if($ques  == 0){
                $i=0;
            unset($_POST['_token']);
            //dd($_POST);
            
            $qid = \App\Models\CourseQues::insertGetId(['text'=>$_POST['ques'],'type'=>$_POST['type'],'course_id'=>$id]);
            for($i=1;$i<5;$i++){
                if($i == 1){
                    $corr = 1;
                }else{
                    $corr = 0;
                }
                $ans = 'ans_'.$i;
                \App\Models\CourseAnsOpt::insert(['text'=>$_POST[$ans],'correct'=>$corr,'q_id'=>$qid]); 
            }
            return redirect(route('admin.course.ques',[$id,0]))->with('status', ['success edits modified','success']);
            
            
        }else{
            // update
            
                $i=0;
            unset($_POST['_token']);
            //dd($_POST);
            
            \App\Models\CourseQues::where('id',$ques)->update(['text'=>$_POST['ques'],'type'=>$_POST['type']]);
            $ans  = DB::table('courses_ans_opt')->where('q_id',$ques)->get();
            
            for($i=0;$i<4;$i++){
                if($i == 0){
                    $corr = 1;
                }else{
                    $corr = 0;
                }
                $h=$i+1;
                $anss = 'ans_'.$h;
                \App\Models\CourseAnsOpt::where('id',$ans[$i]['id'])->update([
                'text'=>$_POST[$anss],
                'correct'=>$corr,
                'q_id'=>$ques]); 
            }
            return redirect(route('admin.course.ques',[$id,$ques]))->with('status', ['success update modified','success']);
            
        }
        }
        
        return view('admin.course.ques',$data);
    }
    
    
     public function course_docs(Request $request,$id)
    {
        $data['page'] = 'course Docs info';
        $data['menu'] = 'course';
        $data['title'] = 'Admin | course Docs edit';
        $data['id'] = $id;
       
           $data['data'] = \App\Models\CourseDoc::orderByDesc('id')->where('course_id',$id)->paginate(10);
           $course = \App\Models\Course::where('id',$id)->first();
           //dd($course->id);
           if(!empty($course->id)){
               $data['course'] = $course;
           }else{
               $course = new \stdClass() ;
               $course->title = 'not found';
               $data['course'] = $course ;
               
           }
           
        if (!empty($_POST)) {
            
            unset($_POST['_token']);
            
            $file = $request->file('image');
            
            //Move Uploaded File
            $destinationPath = 'course';
             $newname = 'doc_'.time().'.'.$file->extension();
            $file->move($destinationPath,$newname);
            
            $_POST['file'] = $newname;
            
            \App\Models\CourseDoc::insert($_POST); 
            
            return redirect(route('admin.course.docs',$id))->with('status', ['success edits modified','success']);
        }
        
        
        return view('admin.course.docs',$data);
    }
    
     public function del_docs(\App\Models\CourseDoc $coursedoc)
    {
        $doc = \App\Models\CourseDoc::find($coursedoc->id);
        unlink('course/'.$doc->file);
        $doc->delete();

        return Redirect::back()->with('status', ['success Deleted .. ','success']);
    }
    
     public function course_out(\App\Models\Course $course)
    {
        $data['page'] = 'course outlines info';
        $data['menu'] = 'course';
        $data['title'] = 'Admin | course outlines edit';
        $data['id'] = $course->id;
       
        $course = \App\Models\Course::find($course->id);
        $data['data'] = \App\Models\CourseList::orderByDesc('id')->where('tut_id',$course->id)->get();
        
        
        if(!empty($course->id)){
            $data['course'] = $course;
        }else{
            $course = new \stdClass() ;
            $course->title = 'not found';
            $data['course'] = $course ;
            
        }
           
        if (!empty($_POST)) {
            
            unset($_POST['_token']);
            
            \App\Models\CourseList::insert($_POST); 
            
            return redirect(route('admin.course.out',$course->id))->with('status', ['success edits modified','success']);
        }
        
        
        return view('admin.course.out',$data);
    }
    
     public function del_out(\App\Models\CourseList $courselist)
    {
        \App\Models\CourseList::find($courselist->id)->delete();
            return Redirect::back()->with('status', ['success Deleted .. ','success']);
    }

    public function dash_course_speak(Request $request,\App\Models\Course $course)
    {
        $data['page'] = 'course Speakers info';
        $data['menu'] = 'course';
        $data['title'] = 'Admin | course Speakers edit';
        $data['id'] = $course->id;
       
        $course = \App\Models\Course::find($course->id);
        $data['data'] = \App\Models\CourseSpeaker::orderByDesc('id')->where('course_id',$course->id)->paginate(10);
        
        //dd($course->id);
        if(!empty($course->id)){
            $data['course'] = $course;
        }else{
            $course = new \stdClass() ;
            $course->title = 'not found';
            $data['course'] = $course ;
            
        }
           
        if (!empty($_POST)) {
            
            unset($_POST['_token']);
            
            $file = $request->file('image');
            
            //Move Uploaded File
            $destinationPath = 'course';
             $newname = 'speaker_'.time().'.'.$file->extension();
            $file->move($destinationPath,$newname);
            
            $_POST['pic'] = $newname;
            
            \App\Models\CourseSpeaker::insert($_POST); 
            
            return redirect(route('admin.course.speak',$course->id))->with('status', ['success edits modified','success']);
        }
        
        
        return view('admin.course.speak',$data);
    }

     public function del_speak(\App\Models\CourseSpeaker $coursespeak)
    {
        $speak = \App\Models\CourseSpeaker::find($coursespeak->id);
        unlink('course/'.$speak->pic);
        $speak->delete();
        
        return Redirect::back()->with('status', ['success Deleted .. ','success']);
    }

     public function dash_course_memo(Request $request,\App\Models\Course $course)
    {
        $data['page'] = 'course Memories info';
        $data['menu'] = 'course';
        $data['title'] = 'Admin | course Memories edit';
        $data['id'] = $course->id;
       
        $course = \App\Models\Course::find($course->id);
        $data['data'] = \App\Models\CourseMemo::orderByDesc('id')->where('course_id',$course->id)->paginate(10);
        
        //dd($course->id);
        if(!empty($course->id)){
            $data['course'] = $course;
        }else{
            $course = new \stdClass() ;
            $course->title = 'not found';
            $data['course'] = $course ;
            
        }
           
        if (!empty($_POST)) {
            
            unset($_POST['_token']);
            
            $file = $request->file('image');
            
            //Move Uploaded File
            $destinationPath = 'course';
             $newname = 'memo_'.time().'.'.$file->extension();
            $file->move($destinationPath,$newname);
            
            $_POST['pic'] = $newname;
            
            \App\Models\CourseMemo::insert($_POST); 
            
            return redirect(route('admin.course.memo',$course->id))->with('status', ['success edits modified','success']);
        }
        
        
        return view('admin.course.memo',$data);
    }

     public function del_memo(\App\Models\CourseMemo $coursememo)
    {
        $memo = \App\Models\CourseMemo::find($coursememo->id);
        unlink('course/'.$memo->pic);
        $memo->delete();

        return Redirect::back()->with('status', ['success Deleted .. ','success']);
    }

     public function dash_team()
    {
        $data['page'] = 'All team';
        $data['menu'] = 'team';
        $data['title'] = 'Admin | Site team';
        $data['data'] = \App\Models\Team::orderbyDesc('id')->get();
        return view('admin.team.view',$data);
    }

    public function dash_team_edit(Request $request ,\App\Models\Team $team)
    {
        $data['page'] = 'team info edit';
        $data['menu'] = 'team';
        $data['title'] = 'Admin | team info edit';
        $data['data'] = $this_team = \App\Models\Team::find($team->id);
        if (!empty($_POST)) {
           if(!empty($request->file('image'))){
               $file = $request->file('image');
            
            //Move Uploaded File
            $destinationPath = 'up';
            if(!File::isDirectory($destinationPath)){
                File::makeDirectory($destinationPath, 0777, true, true);
            } 
            $newname = 'teamimg_'.time().'.'.$file->extension();
            $file->move($destinationPath,$newname);
            
            $_POST['img'] = $newname;
           }
            unset($_POST['_token']);
            $this_team->update($_POST);
            return redirect(route('admin.team'))->with('status', ['success edits modified','success']);
        }
        return view('admin.team.edit',$data);
    }
    
    public function del_team(\App\Models\Team $team)
    {
        \App\Models\Team::find($team->id)->delete();
            return redirect(route('admin.team'))->with('status', ['success Deleted .. ','success']);
    }
    
    public function newteam(Request $request)
    {
        $data['page'] = 'Add new team';
        $data['menu'] = 'team';
        $data['title'] = 'Admin | Add new team';
        
        
        if (!empty($_POST)) {
            unset($_POST['_token']);
            
            $file = $request->file('image');
            
            //Move Uploaded File
            $destinationPath = 'up';
            if(!File::isDirectory($destinationPath)){
                File::makeDirectory($destinationPath, 0777, true, true);
            } 
            $newname = 'teamimg_'.time().'.'.$file->extension();
            $file->move($destinationPath,$newname);
            
            $_POST['img'] = $newname;
            
            
            \App\Models\Team::insert($_POST);
            return redirect(route('admin.team'))->with('status', ['success added new member','success']);
        }
        return view('admin.team.new',$data);
    }
    
    public function dash_confr()
    {
        $data['page'] = 'All Conferences';
        $data['menu'] = 'conf';
        $data['title'] = 'Admin | Site Conferences';
        $data['data'] = \App\Models\Critical::orderByDesc('id')->get();
        return view('admin.confr.view',$data);
    }

    public function dash_confr_edit(Request $request ,\App\Models\Critical $critical)
    {
        $data['page'] = 'Conferences info edit';
        $data['menu'] = 'conf';
        $data['title'] = 'Admin | Conferences info edit';
        $data['data'] = $this_critical = \App\Models\Critical::find($critical->id);
        if (!empty($_POST)) {
            unset($_POST['_token']);
            $this_critical->update($_POST);
            return redirect(route('admin.confr'))->with('status', ['success edits modified','success']);
        }
        return view('admin.confr.edit',$data);
    }
    
    public function del_confr(\App\Models\Critical $critical)
    {
        \App\Models\Critical::find($critical->id)->delete();
            return redirect(route('admin.confr'))->with('status', ['success Deleted .. ','success']);
    }
    
    public function newconfr(Request $request)
    {
        $data['page'] = 'Add new Conference';
        $data['menu'] = 'conf';
        $data['title'] = 'Admin | Add new Conference';
        
        if (!empty($_POST)) {
            unset($_POST['_token']);
            
            DB::table('critical')->insert($_POST);
            return redirect(route('admin.confr'))->with('status', ['success added new course','success']);
        }
        return view('admin.confr.new',$data);
    }
    
     public function ccat_docs(Request $request,$id)
    {
        $data['page'] = 'PPT Docs info';
        $data['menu'] = 'cme';
        $data['title'] = 'Admin | PPT Docs edit';
        
        if($id == 1){
           $data['cname'] = 'critical care educational'; 
        }elseif($id == 2){
           $data['cname'] = 'critical care ultrasound'; 
        }
        $data['id'] = $id;
        
        $data['data'] = \App\Models\CatDoc::orderByDesc('id')->where('cat_id',$id)->get();
           
           
        if (!empty($_POST)) {
            
            unset($_POST['_token']);
            
            $file = $request->file('image');
            
            //Move Uploaded File
            $destinationPath = 'up';
            if(!File::isDirectory($destinationPath)){
                File::makeDirectory($destinationPath, 0777, true, true);
            } 
            $newname = 'ccat_file_'.time().'.'.$file->extension();
            $file->move($destinationPath,$newname);
            
            $_POST['file'] = $newname;
            
            
            \App\Models\CatDoc::insert($_POST); 
            
            return redirect(route('admin.ccat.docs',$id))->with('status', ['success edits modified','success']);
        }
        
        
        return view('admin.ccat.docs',$data);
    }
    
     public function del_ccat_docs(\App\Models\CatDoc $catdoc)
    {
        \App\Models\CatDoc::find($catdoc->id)->delete();
            return Redirect::back()->with('status', ['success Deleted .. ','success']);
    }
    
     public function youtube()
    {
        $data['page'] = 'youtube info';
        $data['menu'] = 'youtube';
        $data['title'] = 'Admin | youtube info';
        $data['data'] = \App\Models\Youtube::orderByDesc('youtube_id')->get();
        return view('admin.youtube.view',$data);
    }
    
    public function addyoutube()
    {
        $data['page'] = 'add youtube video';
        $data['menu'] = 'youtube';
        $data['title'] = 'Admin | youtube info edit';
        
        $data['ccat'] = \App\Models\Ccat::all();
        if (!empty($_POST)) {
            //dd($_POST);
            unset($_POST['_token']);
            $play_list = \App\Models\Ccat::where('id',$_POST['ccat'])->first();
            $arr = [
                'title'=> $_POST['title'],
                'playlist'=>$play_list->name,
                'playlist_id'=>$_POST['ccat'],
                'video_id'=>$_POST['vid_id'],
                'embed'=>'https://www.youtube.com/embed/'.$_POST['vid_id'],
                'img'=>'https://i.ytimg.com/vi/'.$_POST['vid_id'].'/hqdefault.jpg'
            ];
            \App\Models\Youtube::insert($arr);
            return redirect(route('admin.youtube'))->with('status', ['success edits modified','success']);
        }
        return view('admin.youtube.add',$data);
    }
    
    public function youtube_edit(\App\Models\Youtube $youtube)
    {
        $data['page'] = 'youtube info';
        $data['menu'] = 'youtube';
        $data['title'] = 'Admin | youtube info edit';
        $data['data'] = $this_youtube = \App\Models\Youtube::find($youtube->youtube_id);
        $data['ccat'] = \App\Models\Ccat::get();
        if (!empty($_POST)) {
            unset($_POST['_token']);
            //dd($_POST['active']);
            if(isset($_POST['active'])){
                $active = 0;
            }else{
                $active = 1;
            }
            //dd($active);
            $video_id = str_replace(' ', '', $_POST['vid_id']);
            $vid_check = explode('&t=',$video_id);
            if(isset($vid_check[1])){
                $vid_id = $vid_check[0];
            }
            //dd($video_id);
            $arr = [
                'title'=> $_POST['title'],
                'playlist'=>$_POST['ccat'],
                'video_id'=>$video_id,
                'embed'=>'https://www.youtube.com/embed/'.$video_id,
                'img'=>'https://i.ytimg.com/vi/'.$video_id.'/hqdefault.jpg',
                'need_change'=>$active
            ];
            $this_youtube->update($arr);
            return redirect(route('admin.youtube'))->with('status', ['success edits modified','success']);
        }
        return view('admin.youtube.edit',$data);
    }
    
     public function del_youtube(\App\Models\Youtube $youtube)
    {
        \App\Models\Youtube::find($youtube->youtube_id)->delete();
            return Redirect::back()->with('status', ['success Deleted .. ','success']);
    }
    
    public function online_course()
    {
        $data['page'] = 'Online Cources';
        $data['menu'] = 'onlinecourse';
        $data['title'] = 'Admin | Site Onlinecourses';
        $data['data'] = \App\Models\Course::with('count_quiz','count_exam')->where('type','=','3')->orderByDesc('id')->get();
        return view('admin.onlinecourse.view',$data);
    }

    public function online_course_edit(Request $request ,\App\Models\Course $course)
    {
        $data['page'] = 'Courses info edit';
        $data['menu'] = 'onlinecourse';
        $data['title'] = 'Admin | Cources info edit';
        $data['data'] = $this_course = \App\Models\Course::find($course->id);
        //dd($data['data']);
        if (!empty($_POST)) {
           if(!empty($request->file('image'))){
               $file = $request->file('image');
            
            //Move Uploaded File
            $destinationPath = 'course';
            $newname = 'onlineCourse_'.time().'.'.$file->extension();
            $file->move($destinationPath,$newname);
            
            $_POST['img'] = $newname;
           }
            unset($_POST['_token']);
            $this_course->update($_POST);
            return redirect(route('online.course'))->with('status', ['success edits modified','success']);
        }
        return view('admin.onlinecourse.edit',$data);
    }
    
    public function del_onlinecourse(\App\Models\Course $course)
    {
        \App\Models\Course::find($course->id)->delete();
            return redirect(route('online.course'))->with('status', ['success Deleted .. ','success']);
    }
    
    public function newonlinecourese(Request $request)
    {
        $data['page'] = 'Add new Online Course';
        $data['menu'] = 'onlinecourse';
        $data['title'] = 'Admin | Add new Online Course';
        
        
        if (!empty($_POST)) {
            unset($_POST['_token']);
            
            $file = $request->file('image');
            
            //Move Uploaded File
            $destinationPath = 'course';
            $newname = 'onlineCourse_'.time().'.'.$file->extension();
            $file->move($destinationPath,$newname);
            
            $_POST['img'] = $newname;
            
            
            \App\Models\Course::insert($_POST);
            return redirect(route('online.course'))->with('status', ['success added new course','success']);
        }
        return view('admin.onlinecourse.new',$data);
    }
    
    public function onlinecourse_chapter_delete(\App\Models\CourseChapter $coursechapter){
        \App\Models\CourseChapter::find($coursechapter->id)->delete();
        return redirect(route('onine.course.chapter',$coursechapter->course_id))->with('status', ['success chapter deleted','success']);
    }

    public function onlinecourse_ques(\App\Models\Course $course)
    {
        $data['page'] = 'Pre-Quiz';
        $data['menu'] = 'onlinecourse';
        $data['title'] = 'Admin | OnlineCourse Questions edit';
        $data['id'] = $course->id;
       
        $course = \App\Models\Course::find($course->id);
        $data['data'] = \App\Models\CourseOnlineQues::where('course_id',$course->id)->where('type',0)->get();
        
        if(!empty($course->id)){
            $data['course'] = $course;
        }else{
            $course = new \stdClass() ;
            $course->title = 'not found';
            $data['course'] = $course ;
        }
        
        if(!empty($ques_id)){
        $data['ques'] = \App\Models\CourseOnlineQues::where('course_id',$course->id)->get();
        }
        if (!empty($_POST)) {
            $i=0;
            unset($_POST['_token']);
            //dd($_POST);
            
            $qid = \App\Models\CourseOnlineQues::insertGetId(['text'=>$_POST['ques'],'type'=>$_POST['type'],'course_id'=>$course->id]);
            for($i=1;$i<5;$i++){
                if($i == 1){
                    $corr = 1;
                }else{
                    $corr = 0;
                }
                $ans = 'ans_'.$i;
                \App\Models\CourseOnlineansOpt::insert(['text'=>$_POST[$ans],'correct'=>$corr,'q_id'=>$qid]); 
            }
            return redirect(route('online.course.quiz',$course->id))->with('status', ['success edits modified','success']);
        }
        
        
        return view('admin.onlinecourse.quiz',$data);
    }

    public function onlinecourse_chapter_ques(\App\Models\CourseChapter $coursechapter)
    {
        $data['page'] = 'Exam';
        $data['menu'] = 'onlinecourse';
        $data['title'] = 'Admin | OnlineCourse Questions edit';
        $data['id'] = $coursechapter->id;
           $course_chapter = \App\Models\CourseChapter::find($coursechapter->id);

           $data['chapters'] = \App\Models\CourseChapter::where('course_id',$course_chapter->course_id)->get();
           $data['data'] = \App\Models\CourseOnlineQues::where('course_id',$course_chapter->course_id)->orderBy('chapter')->get();
           
           $thiscourse = \App\Models\Course::where('id',$course_chapter->course_id)->first();
           $data['course_id'] = $thiscourse->id;
           //dd($course->id);
           if(!empty($thiscourse->id)){
               $data['course'] = $thiscourse;
           }else{
               $course = new \stdClass() ;
               $course->title = 'not found';
               $data['course'] = $thiscourse ;
               
           }
           
        if (!empty($_POST)) {
            $i=0;
            unset($_POST['_token']);
            //dd($_POST);
            
            $qid = \App\Models\CourseOnlineQues::insertGetId(['text'=>$_POST['ques'],'chapter'=>$_POST['ch'],'course_id'=>$_POST['id']]);
            for($i=1;$i<5;$i++){
                if($i == 1){
                    $corr = 1;
                }else{
                    $corr = 0;
                }
                $ans = 'ans_'.$i;
                \App\Models\CourseOnlineansOpt::insert(['text'=>$_POST[$ans],'correct'=>$corr,'q_id'=>$qid]); 
            }
            return redirect(route('online.course.ques',$coursechapter->id))->with('status', ['success edits modified','success']);
        }
        return view('admin.onlinecourse.ques',$data);
    }

    public function onlinecourse_chapter(\App\Models\Course $course)
    {
        $data['page'] = 'Pre-Quiz';
        $data['menu'] = 'onlinecourse';
        $data['title'] = 'Admin | OnlineCourse Questions edit';
        $data['id'] = $course->id;
       
        $course = \App\Models\Course::find($course->id);
        $data['data'] = \App\Models\CourseChapter::with('ques')->where('course_id',$course->id)->get();
        
        //dd($course->id);
        if(!empty($course->id)){
            $data['course'] = $course;
        }else{
            $course = new \stdClass() ;
            $course->title = 'not found';
            $data['course'] = $course ;
        }
           
        if (!empty($_POST)) {
            $i=0;
            unset($_POST['_token']);
            //dd($_POST);
            
            $qid = \App\Models\CourseChapter::insertGetId(['title'=>$_POST['title'],'course_id'=>$course->id]);
            
            return redirect(route('onine.course.chapter',$course->id))->with('status', ['success ques added','success']);
        }
        
        return view('admin.onlinecourse.chapter',$data);
    }

     public function onlinecourse_docs(Request $request,\App\Models\Course $course)
    {
        $data['page'] = 'course Docs info';
        $data['menu'] = 'onlinecourse';
        $data['title'] = 'Admin | course Docs edit';
        $data['id'] = $course->id;
       
        $course = \App\Models\Course::where('id',$course->id)->first();
        $data['data'] = \App\Models\CourseOnlineDoc::orderByDesc('id')->where('course_id',$course->id)->get();
        $data['chapters'] = \App\Models\CourseChapter::where('course_id',$course->id)->get();
        
        //dd($course->id);
        if(!empty($course->id)){
            $data['course'] = $course;
        }else{
            $course = new \stdClass() ;
            $course->title = 'not found';
            $data['course'] = $course ;
        }
           
        if (!empty($_POST)) {
            
            unset($_POST['_token']);
            
            $file = $request->file('image');
            
            //Move Uploaded File
            $destinationPath = 'course';
            $newname = 'onlineCourseFile_'.time().'.'.$file->extension();
            $file->move($destinationPath,$newname);
            
            $_POST['file'] = $newname;
            
            \App\Models\CourseOnlineDoc::insert($_POST); 
            
            return redirect(route('online.course.docs',$course->id))->with('status', ['success edits modified','success']);
        }
        
        
        return view('admin.onlinecourse.docs',$data);
    }
    
     public function del_onlinedocs(\App\Models\CourseOnlineDoc $courseonlinedoc)
    {
            // $get_fle = DB::table('courses_onlinedocs')->where('id',$id)->first();
            // unlink('/var/www/public_html/public/up/'.$get_fle->file);
            \App\Models\CourseOnlineDoc::find($courseonlinedoc->id)->delete();
            return Redirect::back()->with('status', ['success Deleted .. ','success']);
    }

     public function onlinecourse_youtube(Request $request,\App\Models\Course $course)
    {
        $data['page'] = 'course Youtube info';
        $data['menu'] = 'onlinecourse';
        $data['title'] = 'Admin | course youtube edit';
        $data['id'] = $course->id;
       
        $course = \App\Models\Course::find($course->id);
        $data['data'] = \App\Models\CourseChapterVid::orderByDesc('chapter')->where('course_id',$course->id)->get();
        $data['chapters'] = \App\Models\CourseChapter::where('course_id',$course->id)->get();
        
        //dd($course->id);
        if(!empty($course->id)){
            $data['course'] = $course;
        }else{
            $course = new \stdClass() ;
            $course->title = 'not found';
            $data['course'] = $course ;
            
        }
           
        if (!empty($_POST)) {
            
            unset($_POST['_token']);
            
            $file = $request->file('image');
            $vid_id = str_replace(' ','',$_POST['video_id']);
            $vid_check = explode('&t=',$vid_id);
            if(isset($vid_check[1])){
                $vid_id = $vid_check[0];
            }
            $_POST['video_link'] = 'https://www.youtube.com/watch?v='.$vid_id;
            $_POST['video_id'] = $vid_id;
            
            \App\Models\CourseChapterVid::insert($_POST); 
            
            return redirect(route('online.course.youtube',$course->id))->with('status', ['success edits modified','success']);
        }
        
        
        return view('admin.onlinecourse.youtube',$data);
    }
    
     public function del_onlineyoutube(\App\Models\CourseChapterVid $coursechaptervid)
    {
            \App\Models\CourseChapterVid::find($coursechaptervid->id)->delete();
            return Redirect::back()->with('status', ['success Deleted .. ','success']);
    }

     

    public function cv()
    {
        $data['page'] = 'CV info';
        $data['menu'] = 'job';
        $data['title'] = 'Admin | CV info';
        $data['data'] = $all_jobs = \App\Models\Job::get();
        foreach($all_jobs as $job){
            \App\Models\Job::where('id',$job->id)->update(['seen'=>1]);
        }

        return view('admin.cv.view',$data);
    }

    public function del_cv(\App\Models\Job $job)
    {
        $this_cv = \App\Models\Job::find($job->id);
        unlink('cv/'.$this_cv->cv);
        $this_cv->delete();

        return redirect(route('admin.cv'))->with('status', ['success Deleted .. ','success']);
    }


    public function icujob()
    {
        $data['page'] = 'job info';
        $data['menu'] = 'job';
        $data['title'] = 'Admin | icujob info';
        $data['data'] = JobList::orderByDesc('id')->get();
        return view('admin.icujob.view',$data);
    }
    
    public function addicujob()
    {
        $data['page'] = 'add job';
        $data['menu'] = 'job';
        $data['title'] = 'Admin | icujob info';
        $data['ccat'] = \App\Models\Ccat::get();
        
        return view('admin.icujob.add',$data);
    }
    
    public function icujob_edit(JobList $joblist)
    {
        $data['page'] = 'job info';
        $data['menu'] = 'job';
        $data['title'] = 'Admin | icujob info edit';
        $data['data'] = $this_job = JobList::find($joblist->id);
        
        return view('admin.icujob.edit',$data);
    }

    public function icujob_active_deactive(JobList $joblist)
    {
        $this_job = JobList::find($joblist->id);
        if($this_job['active'] == 0){
            $this_job-> active = 1;
        }else{
            $this_job-> active = 0;
        }
        $this_job->save();
        
        return redirect(route('admin.icujob'))->with('status', ['success edits modified','success']);
    }
    
    public function icujob_add(Request $request)
    {
        if($request['job_id'] == 0){
            $job = new JobList;
        }else{
            $job = JobList::find($request['job_id']);
        }
        $job->job_title = $request['job_title'];
        $job->job_desc = $request['job_desc'];
        $job->specialization = $request['specialization'];
        $job->Qualifications = $request['Qualifications'];
        $job->salary = $request['salary'];
        $job->gender = $request['gender'];
        $job->add_by = auth()->id();
        $job->save();

        return redirect(route('admin.icujob'))->with('status', ['success edits modified','success']);
    }
     public function del_icujob(JobList $joblist)
    {
        JobList::find($joblist->id)->delete();
            return Redirect::back()->with('status', ['success Deleted .. ','success']);
    }

    public function forum()
    {
        $data['page'] = 'forum info';
        $data['title'] = 'Admin | forum info';
        $data['data'] = \App\Models\Forum::get();

        return view('admin.forum.view',$data);
    }

    public function del_forum(\App\Models\ForumComment $forumcomment)
    {
        \App\Models\ForumComment::find($forumcomment->id)->update(['active'=>0]);
            return redirect(route('admin.forum'))->with('status', ['success Deleted .. ','success']);
    }

    public function del_reforum(\App\Models\ForumComment $forumcomment)
    {
        \App\Models\ForumComment::find($forumcomment->id)->update(['active'=>1]);
            return redirect(route('admin.forum'))->with('status', ['success Deleted .. ','success']);
    }

    public function youtubech()
    {
        $data['page'] = 'youtube channels';
        $data['menu'] = 'youtube';
        $data['title'] = 'Admin | youtube info';
        $data['data'] = \App\Models\YoutubeChannel::orderByDesc('id')->get();
        return view('admin.youtubech.view',$data);
    }
    
    public function addyoutubech()
    {
        $data['page'] = 'add youtube channels';
        $data['menu'] = 'youtube';
        $data['title'] = 'Admin | youtube info edit';
        
        if (!empty($_POST)) {
            unset($_POST['_token']);
            $arr = [
                'title'=> $_POST['title'],
                'youtube_id'=>$_POST['vid_id'],
            ];
            \App\Models\YoutubeChannel::insert($arr);
            return redirect(route('admin.youtubech'))->with('status', ['success edits modified','success']);
        }
        return view('admin.youtubech.add',$data);
    }
    
    public function youtube_editch(\App\Models\YoutubeChannel $youtubechannel)
    {
        $data['page'] = 'youtube channel';
        $data['menu'] = 'youtube';
        $data['title'] = 'Admin | youtube channel edit';
        $data['data'] = \App\Models\YoutubeChannel::find($youtubechannel->id);
        if (!empty($_POST)) {
            unset($_POST['_token']);
            $arr = [
                'title'=> $_POST['title'],
                'youtube_id'=>$_POST['vid_id'],
            ];
            \App\Models\YoutubeChannel::where('id',$_POST['id'])->update($arr);
            return redirect(route('admin.youtubech'))->with('status', ['success edits modified','success']);
        }
        return view('admin.youtubech.edit',$data);
    }
    
     public function del_youtubech(\App\Models\YoutubeChannel $youtubechannel)
    {
            \App\Models\YoutubeChannel::find($youtubechannel->id)->delete();
            return Redirect::back()->with('status', ['success Deleted .. ','success']);
    }
    
    public function yt_exists($videoID) {
    $theURL = "http://www.youtube.com/oembed?url=http://www.youtube.com/watch?v=$videoID&format=json";
    $headers = get_headers($theURL);

    return (substr($headers[0], 9, 3) !== "404");
    }

    public function check_youtube() {
        $yts = \App\Models\Youtube::get();
        
        foreach($yts as $yt){
            $id = $yt->video_id; //Video id goes here

            if ($this->yt_exists($id)) {
                //  Yep, video is still up and running :)
            } else {
                \App\Models\Youtube::where('youtube_id',$yt->youtube_id)->update(['need_change'=>1]);
            }
        }
        

    }

}
