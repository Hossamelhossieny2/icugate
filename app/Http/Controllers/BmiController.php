<?php

namespace App\Http\Controllers;

use App\Http\Requests\BmiCalculator;
use Illuminate\Http\Request;
use App\Models\ContactInfo;
use App\Models\Cat;
use App\Models\Tag;

class BmiController extends Controller
{
    public function calculateForm()
    {
        if(auth()->user()->type == 0){
            $arr['usertype'] = "free";
        }elseif(auth()->user()->type == 1){
            $arr['usertype'] = "silver";
        }elseif(auth()->user()->type == 2){
            $arr['usertype'] = "gold";
        }
        $arr['conf'] = ContactInfo::first();
        $arr['menu'] = 'premuim';
        $arr['cats'] = Cat::where('shows','1')->get();
        $arr['tags'] = Tag::get();

        return view('bmi',$arr);
    }
    
    public function calculateBmi(BmiCalculator $request)
    {
        $weight = $request->weight; //Weight in Kg
        $height = $request->height / 100; //Height in M

        # Body Mass Index (BMI):
        $bmi    = $weight / ($height * $height);

        # Ideal Body Weight (IBW):
        $toBeAdd = 50;
        if ($request->gender == 'female') {
            $toBeAdd = 45.5;
        }
        $ibw = $toBeAdd + 0.9 * ($request->height - 152.4);

        # Adjusted Body Weight:

        $abw = $ibw + 0.4 * ($weight - $ibw);

        # Total Caloric Requirement & # Total Protein Requirement
        if ($bmi < 30) {
            $tcr = 25 - (30 / $weight);
            $tpr = 1.2 - (2 / $weight);
        }else{
            $tcr = 25 - (30 / $abw);
            $tpr = 1.2 - (2 / $ibw);
        }
        #Osmolartiy
        if(!empty($request->gm_dextrose)){
            $gmDext = $request->gm_dextrose;
            $gmProt = $request->gm_protein;

            $gmDext = $gmDext * 5;
            $gmProt = $gmProt * 10;

            $soulation = $gmProt + $gmDext; // mOsm/L
            $soulation = $soulation + 300;
        }else{
            $soulation = 0;
        }
        
        $arr = [
            'bmi' => $bmi,
            'ibw' => $ibw,
            'abw' => $abw,
            'tcr' => $tcr,
            'tpr' => $tpr,
            'sol' => $soulation,
            'hgt' => $request->height
        ];

        if(auth()->user()->type == 0){
            $arr['usertype'] = "free";
        }elseif(auth()->user()->type == 1){
            $arr['usertype'] = "silver";
        }elseif(auth()->user()->type == 2){
            $arr['usertype'] = "gold";
        }
        $arr['conf'] = ContactInfo::first();
        $arr['cats'] = Cat::where('shows','1')->get();
        $arr['tags'] = Tag::get();

        //dd($arr);
        return view('bmi_res',$arr);
    }
}
