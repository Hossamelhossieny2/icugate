<?php

namespace App\Http\Controllers;

use App\Models\TeleConsultation;
use Illuminate\Http\Request;

class TeleConsultationController extends Controller
{
    public function preBuyTele(TeleConsultation $teleConsultation)
    {
        $arr['thisTele'] = $teleConsultation;
        return view('tele',$arr);
    }
}
