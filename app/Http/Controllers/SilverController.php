<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\QuestionCh;
use App\Models\Post;
use App\Models\Team;
use App\Models\Cat;
use App\Models\ContactInfo;
use App\Models\HomeSlider;
use App\Models\Tag;
use App\Models\YoutubeChannel;
use App\Models\User;
use App\Models\Event;
use Illuminate\Pagination\Paginator;
use Illuminate\Database\Eloquent\Collection;

class SilverController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function silverarticle(Cat $cat,Tag $tag)
    {   
        $arr['usertype'] = "silver";
        $arr['menu'] = 'article';
        $arr['conf'] = ContactInfo::first();
        $arr['latest'] = Post::where('famous',1)->where('cat_id',$cat->id)->where('tag_id',$tag->id)->where('type','<',2)->orderByDesc('id')->limit(5)->get();
        $arr['montharticle'] = Post::where('month_article',1)->orderByDesc('id')->first();
        
        $arr['cats'] = Cat::where('shows','1')->withCount('post')->get();

        $allTags = Tag::get();
        for($i=0;$i<11;$i++){
            $allTags[$i]['posts'] = Post::where('cat_id',$cat->id)->where('tag_id',$allTags[$i]['id'])->orderByDesc('id')->paginate(10);
            $allTags[$i]['count'] = Post::where('cat_id',$cat->id)->where('tag_id',$allTags[$i]['id'])->count();
        }
        $arr['tags'] = $allTags;
        $arr['posts'] = $allTags[$tag['id']-1]['posts'];

        $arr['hot_topics'] = Post::where('cat_id',12)->where('tag_id',$tag->id)->orderByDesc('id')->limit(12)->get();
        $arr['events']   = Event::whereDate('event_date','>=', \Carbon\Carbon::now()->format('Y-m-d'))->get();
        $arr['cat_tag'] = [
            'cat_id' => $cat['id'],
            'cat_title' => $cat['title'],
            'tag_id' => $tag['id'],
            'tag_title' => $tag['title'],
        ];

        return view('articles',$arr);
    }

    public function silvercme(Cat $cat,Tag $tag)
    {   // cats id allowed here
        // 13 ICU Scoring Calculators
        // 14 Drug Interactions Checker
        // 17 Members Contribution
        // 18 Video
        // 19 ICU PROCEDURES
        // 20 Ongoing Researches

        $arr['usertype'] = "silver";
        if($cat->id != 13 && $cat->id != 14){
            $arr['menu'] = 'cme';
        }else{
            $arr['menu'] = 'premuim';
        }
        $arr['conf'] = ContactInfo::first();
        $arr['latest'] = Post::where('famous',1)->where('cat_id',$cat->id)->where('type','<',2)->orderByDesc('id')->limit(5)->get();
        $arr['montharticle'] = Post::where('month_article',1)->orderByDesc('id')->first();
        

        $allTags = Tag::get();
        for($i=0;$i<11;$i++){
            $allTags[$i]['posts'] = Post::where('cat_id',$cat->id)->where('tag_id',$allTags[$i]['id'])->orderByDesc('id')->paginate(10);
            $allTags[$i]['count'] = Post::where('cat_id',$cat->id)->where('tag_id',$allTags[$i]['id'])->count();
        }
        $arr['tags'] = $allTags;
        $arr['posts'] = $allTags[$tag['id']-1]['posts'];
        
        $arr['cats'] = Cat::where('shows','1')->get();
        $arr['hot_topics'] = Post::where('cat_id',12)->orderByDesc('id')->limit(12)->get();
        $arr['events']   = Event::whereDate('event_date','>=', \Carbon\Carbon::now()->format('Y-m-d'))->get();
        $arr['cat_tag'] = [
            'cat_id' => $cat['id'],
            'cat_title' => $cat['title'],
            'tag_id' => $tag['id'],
            'tag_title' => $tag['title']
        ];
        return view('cme',$arr);
    }

}
