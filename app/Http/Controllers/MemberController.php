<?php

namespace App\Http\Controllers;
use App\Models\PayPalOrder;
use Database\Seeders\TeleConsultaionSeeder;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Models\QuestionAnswer;
use App\Models\Course;
use App\Models\Question;
use App\Models\QuestionAnswerCh;
use App\Models\QuestionCh;
use App\Models\Post;
use App\Models\TeleConsultation;
use App\Models\Cat;
use App\Models\ContactInfo;
use App\Models\Pricing;
use App\Models\Tag;
use App\Models\Ccat;
use App\Models\CatDoc;
use App\Models\Youtube;
use App\Models\Event;
use App\Models\Critical;
use App\Models\JobList;
use App\Models\Job;
use File;
use Session;
use Illuminate\Support\Facades\Validator;

class MemberController extends Controller
{
    
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function freearticle(Cat $cat,Tag $tag)
    {   
        $arr['conf'] = ContactInfo::first();
        $arr['menu'] = 'article';
        if(auth()->user()->type == 0){
            $arr['usertype'] = "free";
        }elseif(auth()->user()->type == 1){
            $arr['usertype'] = "silver";
        }elseif(auth()->user()->type == 2){
            $arr['usertype'] = "gold";
        }
        $arr['latest'] = Post::where('famous',1)->where('type','<',2)->where('cat_id',$cat->id)->where('tag_id',$tag->id)->orderByDesc('id')->limit(5)->get();
        $arr['montharticle'] = Post::where('month_article',1)->orderByDesc('id')->first();
        
        $arr['cats'] = Cat::where('shows','1')->withCount('post')->get();

        $allTags = Tag::get();
        for($i=0;$i<11;$i++){
            $allTags[$i]['posts'] = Post::where('cat_id',$cat->id)->where('tag_id',$allTags[$i]['id'])->orderByDesc('id')->paginate(10);
            $allTags[$i]['count'] = Post::where('cat_id',$cat->id)->where('tag_id',$allTags[$i]['id'])->count();
        }
        $arr['tags'] = $allTags;
        $arr['posts'] = $allTags[$tag['id']-1]['posts'];

        $arr['hot_topics'] = Post::where('cat_id',12)->where('tag_id',$tag->id)->orderByDesc('id')->limit(12)->get();
        $arr['events']   = Event::whereDate('event_date','>=', \Carbon\Carbon::now()->format('Y-m-d'))->get();
        $arr['cat_tag'] = [
            'cat_id' => $cat['id'],
            'cat_title' => $cat['title'],
            'tag_id' => $tag['id'],
            'tag_title' => $tag['title'],
        ];

        return view('articles',$arr);
    }
    
    public function critical_care_conferences(){
        $arr['conf'] = ContactInfo::first();
        $arr['menu'] = 'critical';
        $arr['hot_topics'] = $hottopc = Post::where('cat_id',12)->orderByDesc('id')->limit(12)->get();
        $arr['Latest'] = $hottopc;
        if(auth()->user()->type == 0){
            $arr['usertype'] = "free";
        }elseif(auth()->user()->type == 1){
            $arr['usertype'] = "silver";
        }elseif(auth()->user()->type == 2){
            $arr['usertype'] = "gold";
        }
        
        $arr['cats'] = Cat::where('shows','1')->get();
        $arr['tags'] = Tag::get();
        $arr['events1'] = Critical::get();
        $arr['events']   = Event::whereDate('event_date','>=', \Carbon\Carbon::now()->format('Y-m-d'))->get();
        return view('conferences',$arr);
    }

    public function job_detail(JobList $job){
        $arr['conf'] = $conf = ContactInfo::first();
        $arr['menu'] = 'icugate';
        if(auth()->user()){
            if(auth()->user()->type == 0){
                $arr['usertype'] = "free";
            }elseif(auth()->user()->type == 1){
                $arr['usertype'] = "silver";
            }elseif(auth()->user()->type == 2){
                $arr['usertype'] = "gold";
            }
        }else{
            $arr['usertype'] = "guest";
        }
        $arr['latest'] = Post::where('famous',1)->where('type','<',2)->orderByDesc('id')->limit(5)->get();
        $arr['tags'] = Tag::get();
        $arr['cats'] = Cat::where('shows','1')->withCount('post')->get();

        $arr['job_detail'] = JobList::withCount('job_cv')->find($job['id']);
        $arr['user_send'] = Job::where('user_id',auth()->id())->where('job_id',$job['id'])->first();
            
        return view('job_detail',$arr);
    }

    public function apply_job(Request $request){
        $validated = $request->validate([
	        'form_name' => 'required|max:100',
	        'form_email' => 'required|email',
            'form_phone' => 'required|numeric',
	        'qualifications' => 'required|max:255|string',
	        'specialization' => 'required|max:255|string',
            'form_about' => 'required|max:255|string',
            'form_gender' => 'required',
            'job_id' => 'required',
            'form_attachment' => 'required|mimes:pdf|max:3000',
	    ]);
        $file = $request->file('form_attachment');
        $destinationPath = 'cv';
        if(!File::isDirectory($destinationPath)){
            File::makeDirectory($destinationPath, 0777, true, true);
        }  
        $cvname = 'cv_'.time().'_'.$file->getClientOriginalName();
        $file->move($destinationPath,$cvname);

        $new_contact = new Job;
        $new_contact->job_id = $validated['job_id'];
        $new_contact->name = $validated['form_name'];
        $new_contact->gender = $validated['form_gender'];
        $new_contact->phone = $validated['form_phone'];
        $new_contact->email = $validated['form_email'];
        $new_contact->qualifications = $validated['qualifications'];
        $new_contact->specialization = $validated['specialization'];
        $new_contact->about = $validated['form_about'];
        $new_contact->cv = $cvname;
        $new_contact->user_id = auth()->id();
        $new_contact->save();

        return redirect(route('icugate_jobs'))->with('status', ['Sent success .. Our professional will contact you soon','success']);
    }

    public function criticalcare_ultrasound(){
        $arr['conf'] = ContactInfo::first();
        $arr['tags'] = Tag::get();
        $arr['cats'] = Cat::where('shows','1')->get();
        $arr['ccat'] = Ccat::where('id','2')->first();
        $arr['menu'] = 'critical';
        $arr['usertype'] = "gold";
        $arr['latest'] = Post::where('famous',1)->orderByDesc('id')->where('type','<',2)->limit(5)->get();
        $arr['hot_topics'] = Post::where('cat_id',12)->orderByDesc('id')->limit(12)->get();
        $arr['montharticle'] = Post::where('month_article',1)->orderByDesc('id')->first();
        $arr['docs'] = CatDoc::where('cat_id',2)->get();
        $arr['vids'] = Youtube::where('need_change',0)->where('playlist','Critical Care Ultrasound')->get();
        
        return view('critical_care_view',$arr);
    }

    public function criticalcare_educational(){
        $arr['conf'] = ContactInfo::first();
        $arr['tags'] = Tag::get();
        $arr['cats'] = Cat::where('shows','1')->get();
        $arr['ccat'] = Ccat::where('id','1')->first();
        $arr['menu'] = 'critical';
        $arr['usertype'] = "gold";
        $arr['latest'] = Post::where('famous',1)->orderByDesc('id')->where('type','<',2)->limit(5)->get();
        $arr['hot_topics'] = Post::where('cat_id',12)->orderByDesc('id')->limit(12)->get();
        $arr['montharticle'] = Post::where('month_article',1)->orderByDesc('id')->first();
        $arr['docs'] = CatDoc::where('cat_id',1)->get();
        $arr['vids'] = Youtube::where('need_change',0)->where('playlist','Critical Care Educational Support Professionals')->get();
        $arr['old_courses'] = Course::where('exam_time', '<', date('Y-m-d'))->get();
        $arr['new_courses'] = Course::where('from', '>=', date('Y-m-d'))->get();
        //dd($arr['old_courses']);
        return view('critical_care_view',$arr);
    }

    public function blog(Post $post){
        $arr['conf'] = ContactInfo::first();
        $arr['tags'] = Tag::get();
        $arr['cats'] = Cat::where('shows','1')->get();
        $arr['ccat'] = Ccat::where('id','1')->first();
        $arr['menu'] = 'critical';
        $arr['usertype'] = "gold";
        $arr['latest'] = Post::where('famous',1)->orderByDesc('id')->where('type','<',2)->limit(5)->get();
        $arr['hot_topics'] = Post::where('cat_id',12)->orderByDesc('id')->limit(12)->get();
        $arr['post'] = Post::find($post['id']);

        return view('blog',$arr);
    }

     public function upgrade(Pricing $pricing){
        $arr['menu'] = 'icugate';

        $choosen_price = Pricing::find($pricing->id); 
        $current_price = Pricing::where('type',auth()->user()->type)->first(); 
        if($current_price['id'] > 1){
            $old_price = Pricing::find($pricing->type); 
            $arr['must_pay'] = $must_pay = $choosen_price['price'] - $old_price['price'];
        }else{
            $arr['must_pay'] = $must_pay = $choosen_price['price'];
        }
        $arr['pay_for'] = $pay_for = "Upgrade user account from (".$current_price['title'].") To (".$choosen_price['title'].")";
        
        $check_payment = $this->check_old_payment($pricing->type,1);
        
        if($check_payment == "empty")
        {
            $newOrder = new PayPalOrder;
            $newOrder->order_type_id = 1;
            $newOrder->user_id = auth()->id();
            $newOrder->item_id = $pricing->type;
            $newOrder->total = $must_pay;
            $newOrder->response = "NOT-PAID";
            $newOrder->order_id = 'sub-'.$pricing->type.'/U-'.auth()->id().'/D-'.date('dmy');
            $newOrder->order_title = $pay_for;
            $newOrder->save();
            
        }else{
            
            return redirect(route('paypal.payment', $check_payment));
            
        }
        $arr['order_id'] = $newOrder->id;
        return view('user_paypal_pay',$arr);
    }

    public function preBuyTele(TeleConsultation $teleConsultation){
        $arr['menu'] = 'icugate';
       
        $arr['must_pay'] = $teleConsultation['price'];
        
        $arr['pay_for'] = $pay_for = $teleConsultation['name']." Consultation Online Service ";
        
        $check_payment = $this->check_old_payment($teleConsultation['id'],3);
        
        if($check_payment == "empty")
        {
            $newOrder = new PayPalOrder;
            $newOrder->order_type_id = 3;
            $newOrder->user_id = auth()->id();
            $newOrder->item_id = $teleConsultation['id'];
            $newOrder->total = $teleConsultation['price'];
            $newOrder->response = "NOT-PAID";
            $newOrder->order_id = 'con-'.$teleConsultation['id'].'/U-'.auth()->id().'/D-'.date('dmy');
            $newOrder->order_title = $pay_for;
            $newOrder->save();
            
        }else{
            
            return redirect(route('paypal.payment', $check_payment));
            
        }
        $arr['order_id'] = $newOrder->id;
        return view('user_paypal_pay',$arr);
    }

    public function payCourse(Course $course){
        $arr['menu'] = 'icugate';
        
        $arr['must_pay'] = $course['price'];
        
        $arr['pay_for'] = $pay_for = "Join course (".$course['title'].") <br> in the period : (".date('d M',strtotime($course['from']))." - ".date('d M Y',strtotime($course['to'])).") <br> Exam Time : ".date('d M Y',strtotime($course['exam_time']));
        
        $check_payment = $this->check_old_payment($course['price'],2);
        
        if($check_payment == "empty")
        {
            $newOrder = new PayPalOrder;
            $newOrder->order_type_id = 2;
            $newOrder->user_id = auth()->id();
            $newOrder->item_id = $course['id'];
            $newOrder->total = $course['price'];
            $newOrder->response = "NOT-PAID";
            $newOrder->order_id = 'cor-'.$course['id'].'/U-'.auth()->id().'/D-'.date('dmy');
            $newOrder->order_title = $pay_for;
            $newOrder->save();
            
        }else{
            
            return redirect(route('paypal.payment', $check_payment));
            
        }

        $arr['order_id'] = $newOrder->id;
        return view('user_paypal_pay',$arr);
    }

    public function check_old_payment($item_id,$order_type_id){
        $tranaction = PayPalOrder::where('user_id', auth()->id())->where('item_id', $item_id)->where('order_type_id', $order_type_id)->whereNotNull('paypal_transaction_id')->first();
        if(!empty($tranaction['id'])){
            return $tranaction['id'];
        }else{
            return 'empty';
        }
        
    }

   
}
