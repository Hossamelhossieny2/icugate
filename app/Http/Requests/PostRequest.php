<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class PostRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, mixed>
     */
    public function rules()
    {
        return [
            'img' => 'required',
            'cat_id' => 'required|integer',
            'tag_id' => 'required|integer',
            'post_type' => 'required|integer',
            'post_title' => 'required|string|max:255',
            'post_desc' => 'required|string|max:255',
            //'journal' => 'required|string|max:100',
            'post_active' => 'required|integer'
        ];
    }

    public function messages()
    {
        return [
            'cat_id.required' => 'You Must Choose your POST category',
            'tag_id.required' => 'You must assign Tag to your POST ',
            'post_title.required' => 'Your POST need Title To displayed Correctly',
            'post_desc.required' => 'Your POST need description To be cleared for users',
            'post_link.required' => 'Your POST need right LINK To redirect TO',
            'post_file.required' => 'Your POST need file To displayed Correctly',
            //'journal.required' => 'Your POST need author To have more user trust',
        ] ;
    }
}
