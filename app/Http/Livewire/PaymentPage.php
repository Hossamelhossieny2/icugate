<?php

namespace App\Http\Livewire;

use App\Models\User;
use App\Models\PayPalOrder;
use App\Models\UserTeleConsultation;
use App\Models\UserCourse;
use App\Models\UserUpgrade;
use Livewire\Component;

class PaymentPage extends Component
{
    public $order_id,$order,$pay_for,$total,$payment_mode = NULL , $payment_id = NULL,$pay_time ;

    protected $listeners = [
        'transactionEmit' => 'paidOnlineOrder'
    ];

    

    public function mount(PayPalOrder $paypalorder)
    {
        
        $paypalorderd = json_decode(PayPalOrder::with('kind')->find($paypalorder['id']));

        if(!empty($paypalorderd->paypal_transaction_id))
        {
            $this->payment_mode = "PAID";
            $this->order_id = $paypalorderd->id;
            $this->payment_id = $paypalorderd->paypal_transaction_id;
            $this->order = $paypalorderd->order_id;
            $this->total = $paypalorderd->total;
            $this->pay_for = $paypalorderd->order_title;
            $this->pay_time = $paypalorderd->updated_at;
        }else{
            $this->order_id = $paypalorderd->id;
            $this->order = $paypalorderd->order_id;
            $this->total = $paypalorderd->total;
            $this->pay_for = $paypalorderd->order_title;
            $this->payment_mode = "UNPAID";
        }

    }

    public function paidOnlineOrder($value)
    {
        if($this->payment_mode != "PAID")
        {
            $this->payment_id = $value;
            $this->payment_mode = 'PAID';
            $this_order_payment = PayPalOrder::where('id', $this->order_id)->first();
            $this_order_payment->paypal_transaction_id = $value;
            $this_order_payment->response = 'Paid';
            $this_order_payment->save();

            $this->add_user_payment($this_order_payment->id);
        }
    }
    public function add_user_payment($order_id)
    {
        $this_order_payment = PayPalOrder::where('id',$order_id)->first();

        if($this_order_payment->order_type_id == 1)
        {
            $upgrade = new UserUpgrade;
            $upgrade->user_id = $this_order_payment->user_id;
            $upgrade->paypal_order_id = $this_order_payment->paypal_transaction_id;
            $upgrade->paid = $this_order_payment->total;
            $upgrade->upgrate_to = $this_order_payment->item_id;
            $upgrade->save();

            $get_user = User::find($upgrade->user_id);
            $get_user->type = $upgrade->upgrate_to;
            $get_user->paid = 1;
            $get_user->save();
            
        }
        elseif($this_order_payment->order_type_id == 2)
        {
            $course = new UserCourse;
            $course->course_id = $this_order_payment->item_id;
            $course->user_id = $this_order_payment->user_id;
            $course->price = $this_order_payment->total;
            $course->status = 0;
            $course->paid = 1;
            $course->paypal_order_id = $this_order_payment->paypal_transaction_id;
            $course->save();

        }
        elseif($this_order_payment->order_type_id == 3)
        {
            $tele = new UserTeleConsultation;
            $tele->tele_consultation_id = $this_order_payment->item_id;
            $tele->user_id = $this_order_payment->user_id;
            $tele->price = $this_order_payment->total;
            $tele->paypal_order_id = $this_order_payment->paypal_transaction_id;
            $tele->save();
        }
    }
    public function render()
    {

        return view('livewire.payment-page')
                ->extends('layouts.applivewire',['total'=>$this->total]);;
    }
}
