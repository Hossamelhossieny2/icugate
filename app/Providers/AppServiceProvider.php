<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\Pagination\Paginator;
use Illuminate\Support\Facades\Auth;
use App\Models\ContactInfo;
use App\Models\Post;
use App\Models\Cat;
use App\Models\Tag;
use App\Models\YoutubeChannel;


class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Paginator::useBootstrap();
        view()->share([
            'conf' => ContactInfo::first(),
            'menu' => "",
            'latest' => Post::where('famous',1)->where('type','<',2)->orderByDesc('id')->limit(5)->get(),
            'tags' => Tag::get(),
            'cats' => Cat::where('shows','1')->withCount('post')->get(),
            'yt_ch' => YoutubeChannel::get()
            ]);

        view()->composer('*', function($view) {
            if(auth()->user()){
                if(auth()->user()->type == 0){
                    $usertype = "free";
                }elseif(auth()->user()->type == 1){
                    $usertype = "silver";
                }elseif(auth()->user()->type == 2){
                    $usertype = "gold";
                }
            }else{
                $usertype = "guest";
            }
            
        });
    }
}
