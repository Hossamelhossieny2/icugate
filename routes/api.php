<?php

use Illuminate\Http\Request;
use App\Http\Controllers\BmiController;
use App\Http\Controllers\ApiController;
/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

//Auth::routes();
Route::post('/login', [ApiController::class, 'login']);
Route::post('/register', [ApiController::class, 'register']);
Route::post('/logout', [ApiController::class, 'logout']);

Route::get('/HomeApp/{label}', [ApiController::class, 'HomeApp']);
Route::get('/Archive/{label}', [ApiController::class, 'Archive']);

Route::get('/Courses/{id}', [ApiController::class, 'Courses']);
Route::get('/Youtube', [ApiController::class, 'Youtube']);
Route::get('/Calendar', [ApiController::class, 'Calendar']);
Route::get('/Config', [ApiController::class, 'Config']);
Route::get('/Search', [ApiController::class, 'Search']);
Route::post('/Contact', [ApiController::class, 'Contact']);
Route::get('/residents_lecture', [ApiController::class, 'residents_lecture']);

Route::get('/get_search', [ApiController::class, 'get_search']);
Route::get('/login', [ApiController::class, 'login']);
Route::get('/register', [ApiController::class, 'register']);
Route::get('/logout', [ApiController::class, 'logout']);

