<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\AuthController;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\MemberController;
use App\Http\Controllers\BmiController;
use App\Http\Controllers\AdminController;
use App\Http\Controllers\SilverController;
use App\Http\Controllers\GoldController;
use App\Http\Controllers\Auth\LoginController;
use App\Http\Controllers\Auth\RegisterController;
use App\Http\Controllers\Auth\LogoutController;
use App\Http\Livewire\PaymentPage;

/*------------------------------------------
--------------------------------------------
without Login needed Routes List
--------------------------------------------
--------------------------------------------*/
Auth::routes(['verify' => FALSE,'login' => false,'register' => false,'logout' => false]);
Route::get('signin', [LoginController::class, 'showLoginForm']);
Route::post('signin', [LoginController::class, 'login'])->name('login');
Route::get('create', [RegisterController::class, 'showRegistrationForm']);
Route::post('create', [RegisterController::class, 'register'])->name('register');
Route::post('signout', [LogoutController::class, 'logout'])->name('logout');

Route::get('/', [HomeController::class, 'index'])->name('home');
Route::get('home', [HomeController::class, 'index']);
Route::get('aboutus', [HomeController::class, 'aboutus'])->name('about_icugate');
Route::get('contactUs', [HomeController::class, 'contactus'])->name('contact_icugate');
Route::post('contact_send', [HomeController::class, 'contactus_send'])->name('send.contact');
Route::get('jobs', [HomeController::class, 'jobs'])->name('icugate_jobs');
Route::get('pricing', [HomeController::class, 'pricing'])->name('icugate_pricing');
Route::get('get_rss', [HomeController::class, 'get_rss'])->name('icugate_rss');



/*------------------------------------------
--------------------------------------------
All Logged Users Routes List (type 0 1 2)
--------------------------------------------
--------------------------------------------*/
Route::middleware('auth')->group(function () {
Route::get('/ajax/calculate/bmi',[BmiController::class,'calculateForm'])->name('fcalculate.bmi.orm');
Route::post('/ajax/calculate/bmi',[BmiController::class,'calculateBmi'])->name('calculate.bmi.submit');
Route::get('/critical_care_conferences',[MemberController::class,'critical_care_conferences'])->name('critical.care.conferences');
Route::get('job_detail/{job}', [MemberController::class, 'job_detail'])->name('job.detail');
Route::post('apply_job', [MemberController::class, 'apply_job'])->name('apply.job');
Route::get('cc_ultrasound', [MemberController::class,'criticalcare_ultrasound'])->name('criticalcare.ultrasound');
Route::get('cc_educational', [MemberController::class,'criticalcare_educational'])->name('criticalcare.educational');
Route::get('blog/{post}', [MemberController::class,'blog'])->name('blog.details');
Route::get('upgrade/{pricing}', [MemberController::class, 'upgrade'])->name('icugate_upgrade');
Route::get('teleconsultation/{teleConsultation}',[MemberController::class, 'preBuyTele'])->name('buy.tele');
Route::get('payCourse/{course}',[MemberController::class, 'payCourse'])->name('pay.course');
Route::get('add_payment_transaction',[MemberController::class, 'add_payment_transaction'])->name('add.payment');
Route::get('/paypal_payment/{paypalorder}', PaymentPage::class)->name('paypal.payment');
});
/*------------------------------------------
--------------------------------------------
All free Users Routes List (type 0)
--------------------------------------------
--------------------------------------------*/
Route::middleware(['auth', 'user-access:0'])->group(function () {
  Route::get('/freearticle/{cat}/{tag}', [MemberController::class, 'freearticle'])->name('article.view.free');
  Route::get('/freecme/{cat}/{tag}', function()
      {
          return 'sections.check_permission';
      })->name('cme.free');
  
  });
/*------------------------------------------
--------------------------------------------
All silver Users Routes List (type 1)
--------------------------------------------
--------------------------------------------*/
Route::middleware(['auth', 'user-access:1'])->group(function () {
  Route::get('/silverarticle/{cat}/{tag}', [SilverController::class, 'silverarticle'])->name('article.view.silver');
  Route::get('/silvercme/{cat}/{tag}', [SilverController::class,'silvercme'])->name('cme.silver');
    
});

/*------------------------------------------
--------------------------------------------
All gold Users Routes List (type 2)
--------------------------------------------
--------------------------------------------*/
Route::middleware(['auth', 'user-access:2'])->group(function () {
  Route::get('/goldarticle/{cat}/{tag}', [GoldController::class, 'goldarticle'])->name('article.view.gold');
  Route::get('/goldcme/{cat}/{tag}', [GoldController::class,'goldcme'])->name('cme.gold');
  Route::get('/premuim/{cat}/{tag}', [GoldController::class,'premuim'])->name('premuim');
});

/*------------------------------------------
--------------------------------------------
All admin Users Routes List (type 3)
--------------------------------------------
--------------------------------------------*/
Route::middleware(['auth', 'user-access:3'])->group(function () {
  Route::get('dash', [AdminController::class,'index'])->name('admin.home');
  Route::get('dash_site', [AdminController::class,'site'])->name('admin.site');
  Route::any('dash_site_edit', [AdminController::class,'site_edit'])->name('admin.site.edit');
  Route::any('dash_site_social', [AdminController::class,'site_social'])->name('admin.site.social');

  Route::get('dash_pricing', [AdminController::class,'pricing'])->name('admin.pricing');
  Route::any('dash_pricing_edit/{pricing}', [AdminController::class,'pricing_edit'])->name('admin.pricing.edit');
  Route::get('dash_tele', [AdminController::class,'tele'])->name('admin.tele');
  Route::any('dash_tele_edit/{teleconsultation}', [AdminController::class,'tele_edit'])->name('admin.tele.edit');

  Route::get('dash_post', [AdminController::class,'post'])->name('admin.post');
  Route::get('dash_month_article', [AdminController::class,'month_article'])->name('admin.month_article');
  Route::get('dash_post_mc', [AdminController::class,'post_mc'])->name('admin.post.mc');
  Route::get('newpost', [AdminController::class,'newpost'])->name('post.add.any');

  Route::post('add_newpost', [AdminController::class,'addnewpost'])->name('post.add.new');
  Route::get('dash_post_edit/{post}', [AdminController::class,'post_edit'])->name('admin.post.edit');
  Route::post('post_edit_do', [AdminController::class,'post_edit_do'])->name('post.edit.do');
  Route::get('del_post/{post}', [AdminController::class,'del_post'])->name('admin.post.del');

  Route::get('dash_event', [AdminController::class,'event'])->name('admin.event');
  Route::get('dash_newevent', [AdminController::class,'newevent'])->name('admin.newevent');
  Route::post('add_newevent', [AdminController::class,'neweventadd'])->name('newevent.post.add');
  Route::get('dash_event_edit/{event}', [AdminController::class,'event_edit'])->name('admin.event.edit');
  Route::post('do_event_edit', [AdminController::class,'do_event_edit'])->name('do.event.edit');
  Route::get('del_event/{event}', [AdminController::class,'del_event'])->name('admin.event.del');

  Route::get('dash_user', [AdminController::class,'dash_user'])->name('admin.user');
  Route::get('dash_payuser', [AdminController::class,'dash_payuser'])->name('admin.payuser');
  Route::get('user_payment', [AdminController::class,'user_payment'])->name('user.payment');
  Route::get('dash_paycourse', [AdminController::class,'dash_paycourse'])->name('admin.paycourse');
  Route::get('dash_payother', [AdminController::class,'dash_payother'])->name('admin.payother');
  Route::get('dash_payother_pay/{usercourse}', [AdminController::class,'dash_payother_pay'])->name('admin.payother.pay');
  Route::get('dash_pay_manual/{paypalorder}/{type}', [AdminController::class,'dash_pay_manual'])->name('admin.pay.manual');

  Route::get('dash_edituser', [AdminController::class,'dash_edituser'])->name('admin.edituser');
  Route::any('dash_edituser_edit/{user}', [AdminController::class,'dash_edituser_edit'])->name('admin.edituser.edit');
  Route::get('dash_user_edit/{user}', [AdminController::class,'dash_user_edit'])->name('admin.user.edit');
  Route::get('dash_user_deactivate/{user}', [AdminController::class,'dash_user_deactivate'])->name('admin.user.deactive');
  Route::get('del_user/{user}', [AdminController::class,'del_user'])->name('admin.user.del');

  Route::any('dash_newcourese', [AdminController::class,'newcourese'])->name('admin.newcourese');
  Route::get('dash_course', [AdminController::class,'dash_course'])->name('admin.course');
  Route::any('dash_course_edit/{course}', [AdminController::class,'dash_course_edit'])->name('admin.course.edit');
  Route::get('del_course/{course}', [AdminController::class,'del_course'])->name('admin.course.del');
  Route::any('dash_course_ques/{id}/{ques}', [AdminController::class,'course_ques'])->name('admin.course.ques');
  Route::any('dash_course_speak/{course}', [AdminController::class,'dash_course_speak'])->name('admin.course.speak');
  Route::any('dash_course_memo/{course}', [AdminController::class,'dash_course_memo'])->name('admin.course.memo');
  Route::get('del_speak/{coursespeak}', [AdminController::class,'del_speak'])->name('admin.post.speak');
  Route::get('del_memo/{coursememo}', [AdminController::class,'del_memo'])->name('admin.post.memo');
  Route::any('dash_course_docs/{id}', [AdminController::class,'course_docs'])->name('admin.course.docs');
  Route::get('del_docs/{coursedoc}', [AdminController::class,'del_docs'])->name('admin.course.deldocs');

  Route::get('online_course', [AdminController::class,'online_course'])->name('online.course');
  Route::any('online_newcourese', [AdminController::class,'newonlinecourese'])->name('online.newcourese');
  Route::any('online_course_edit/{course}', [AdminController::class,'online_course_edit'])->name('admin.onlinecourse.edit');
  Route::get('del_onlinecourse/{course}', [AdminController::class,'del_onlinecourse'])->name('online.course.del');
  Route::any('online_course_chapter/{course}', [AdminController::class,'onlinecourse_chapter'])->name('onine.course.chapter');
  Route::any('online_course_quiz/{course}', [AdminController::class,'onlinecourse_ques'])->name('online.course.quiz');
  Route::any('online_course_ques/{coursechapter}', [AdminController::class,'onlinecourse_chapter_ques'])->name('online.course.ques');
  Route::any('online_course_chapter_delete/{coursechapter}', [AdminController::class,'onlinecourse_chapter_delete'])->name('online.course.chapter.delete');
  Route::any('online_course_docs/{course}', [AdminController::class,'onlinecourse_docs'])->name('online.course.docs');
  Route::get('del_onlinedocs/{courseonlinedoc}', [AdminController::class,'del_onlinedocs'])->name('online.course.deldocs');

  Route::any('online_course_youtube/{course}', [AdminController::class,'onlinecourse_youtube'])->name('online.course.youtube');
  Route::get('del_onlineyoutube/{coursechaptervid}', [AdminController::class,'del_onlineyoutube'])->name('online.course.delyoutube');

  Route::any('dash_course_out/{course}', [AdminController::class,'course_out'])->name('admin.course.out');
  Route::get('del_out/{courselist}', [AdminController::class,'del_out'])->name('admin.course.delout');

  Route::get('dash_team', [AdminController::class,'dash_team'])->name('admin.team');
  Route::any('dash_team_edit/{team}', [AdminController::class,'dash_team_edit'])->name('admin.team.edit');
  Route::get('del_team/{team}', [AdminController::class,'del_team'])->name('admin.team.del');
  Route::any('dash_newteam', [AdminController::class,'newteam'])->name('admin.newteam');

  Route::get('dash_confr', [AdminController::class,'dash_confr'])->name('admin.confr');
  Route::any('dash_confr_edit/{critical}', [AdminController::class,'dash_confr_edit'])->name('admin.confr.edit');
  Route::get('del_confr/{critical}', [AdminController::class,'del_confr'])->name('admin.confr.del');
  Route::any('dash_newcofr', [AdminController::class,'newconfr'])->name('admin.newconfr');

  Route::any('dash_ccat_docs/{id}', [AdminController::class,'ccat_docs'])->name('admin.ccat.docs');
  Route::get('del_ccat_docs/{catdoc}', [AdminController::class,'del_ccat_docs'])->name('admin.ccat.deldocs');

  Route::get('dash_youtube', [AdminController::class,'youtube'])->name('admin.youtube');
  Route::any('dash_addyoutube', [AdminController::class,'addyoutube'])->name('admin.newyoutube');
  Route::any('dash_youtube_edit/{youtube}', [AdminController::class,'youtube_edit'])->name('admin.youtube.edit');
  Route::get('del_youtube/{youtube}', [AdminController::class,'del_youtube'])->name('admin.youtube.del');

  Route::get('dash_youtubech', [AdminController::class,'youtubech'])->name('admin.youtubech');
  Route::any('dash_addyoutubech', [AdminController::class,'addyoutubech'])->name('admin.newyoutubech');
  Route::any('dash_youtube_editch/{youtubechannel}', [AdminController::class,'youtube_editch'])->name('admin.youtube.editch');
  Route::get('del_youtubech/{youtubechannel}', [AdminController::class,'del_youtubech'])->name('admin.youtube.delch');

  Route::get('yt_exists/{id}', [AdminController::class,'yt_exists'])->name('admin.youtube.exists');
  Route::get('check_youtube', [AdminController::class,'check_youtube'])->name('admin.youtube.check');

  Route::get('dash_cv', [AdminController::class,'cv'])->name('admin.cv');
  Route::get('del_cv/{job}', [AdminController::class,'del_cv'])->name('admin.delcv');

  Route::get('dash_forum', [AdminController::class,'forum'])->name('admin.forum');
  Route::get('del_forum/{forumcomment}', [AdminController::class,'del_forum'])->name('admin.delforum');
  Route::get('del_reforum/{forumcomment}', [AdminController::class,'del_reforum'])->name('admin.redelforum');

  Route::get('dash_icujob', [AdminController::class,'icujob'])->name('admin.icujob');
  Route::get('dash_addicujob', [AdminController::class,'addicujob'])->name('admin.newicujob');
  Route::get('dash_icujob_edit/{joblist}', [AdminController::class,'icujob_edit'])->name('admin.icujob.edit');
  Route::post('dash_icujob_add', [AdminController::class,'icujob_add'])->name('admin.job.post');
  Route::get('active_icujob/{joblist}', [AdminController::class,'icujob_active_deactive'])->name('admin.icujob.active');
  Route::get('del_icujob/{joblist}', [AdminController::class,'del_icujob'])->name('admin.icujob.del');
    
});



// Route::get('/home/{id?}', [HomeController::class, 'index'])->name('home.links');
// Route::get('/member/{id}', [HomeController::class, 'member'])->name('member');



// Route::get('/archive/{cat}', [MemberController::class, 'archive'])->name('archive');

// Route::get('/ajax/calculate/bmi',[BmiController::class,'calculateForm'])->name('fcalculate.bmi.orm');
// Route::post('/ajax/calculate/bmi',[BmiController::class,'calculateBmi'])->name('calculate.bmi.submit');
 
// Route::get('/blog/{id}', [MemberController::class,'blog'])->name('blog');
// Route::get('/forum/{id}', [MemberController::class,'forum'])->name('forum');
// Route::post('/forum_submit', [MemberController::class,'forum_submit'])->name('forum.submit');
// Route::get('/event/{id}', [MemberController::class,'event'])->name('event');
// Route::get('/icu-conferences', [MemberController::class,'calendar'])->name('icu-conferences');
// Route::get('/cme/{cat}/{kind}', [MemberController::class,'cme'])->name('cme');
// Route::get('/member_contribution', [MemberController::class,'member_contribution'])->name('member.contribution');
// Route::post('/member_contribution_submit', [MemberController::class,'member_contribution_submit'])->name('member.contribution.submit');
// Route::get('/ccf', [MemberController::class,'ccf'])->name('ccf');
// Route::post('/ccf_submit', [MemberController::class,'ccf_submit'])->name('ccf.submit');
// Route::get('/onlinec', [MemberController::class,'online_support'])->name('cme.online');
// Route::get('/archive/{cat}', [MemberController::class,'archive'])->name('archive');
// Route::get('/youtube', [MemberController::class,'youtube'])->name('youtube');
// Route::get('/support/{cat}', [MemberController::class,'support'])->name('support');
// Route::get('/join_course/{id}', [MemberController::class,'join_course'])->name('join_course');

// Route::get('/review_course/{id}', [MemberController::class,'review_course'])->name('review_course');
// Route::get('/add_payment/{id}', [MemberController::class,'add_payment'])->name('payment');
// Route::get('/paypal/{id}', [MemberController::class,'paypal'])->name('paypal');
// Route::get('/do_test/{id}/{type}', [MemberController::class,'do_test'])->name('do_test');
// Route::post('/test_result', [MemberController::class,'test_result'])->name('testres');
// Route::get('certificate', [MemberController::class,'certificate'])->name('certificate');

//////////// admin ///////////////////


///////////////////////////////////////


//   Route::get('start_online_course/{courseid}/{chapter}/{vid}', 'MemberController@start_online_course')->name('online.course.study');

//   Route::get('/do_chapter_test/{id}/{chapter}', 'MemberController@do_chapter_test')->name('test.chapter');
//   Route::post('/test_chapter_result', 'MemberController@test_chapter_result')->name('testres.chapter');

//  Route::get('/about', 'HomeController@pages')->name('about');
//  Route::any('/contact', 'HomeController@contact')->name('contact');
//  Route::any('/jobs', 'HomeController@jobs')->name('jobs');
//  Route::post('/add_visit', 'HomeController@add_visit')->name('add_visit');
//  Route::post('/add_visit_forum', 'HomeController@add_visit_forum')->name('add_visit_forum');

//   Route::get('text-on-image/{course}','ImageController@textOnImage')->name('textOnImage');
//   Route::get('usercertificate/save/{id}',[
//     'as' => 'usercertificate.download', 'uses' => 'ImageController@downloadImage']);
// Route::resource('usercertificate','ImageController');

// Route::get('/join_course/{id}/{token}', 'MemberController@join_course')->name('join_course2');
