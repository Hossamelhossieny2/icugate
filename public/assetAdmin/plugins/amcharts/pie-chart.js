



chart.addListener("init", handleInit);



chart.addListener("rollOverSlice", function(e) {

  handleRollOver(e);

});



function handleInit(){

  chart.legend.addListener("rollOverItem", handleRollOver);

}



function handleRollOver(e){

  var wedge = e.dataItem.wedge.node;

  wedge.parentNode.appendChild(wedge);

}