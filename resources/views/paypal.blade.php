@extends('layouts.app')

@section('content')

<section>
    <div class="container mt-30 mb-30 pt-30 pb-30">
        <div class="row">
            <div class="col-md-12">
                <div class="blog-posts single-post">
                    <article class="post clearfix mb-0">
                        <div class="entry-content">
                            <div class="entry-meta media no-bg no-border mt-15 pb-20">
                                <div class="entry-date media-left text-center flip bg-theme-colored pt-5 pr-15 pb-5 pl-15">
                                    <ul>
                                        <li class="font-16 text-white font-weight-600">Start {{$course['from']}}</li>
                                    </ul>
                                </div>
                                <div class="media-body pl-15">
                                    <div class="event-content flip">
                                        <h3 class="entry-title text-white text-uppercase pt-0 mt-0 text-center"><a href="#">{{$course['title']}}</a></h3>
                                    </div>
                                </div>
                                <div class="entry-date media-left text-center flip bg-theme-colored pt-5 pr-15 pb-5 pl-15">
                                    <ul>
                                        <li class="font-16 text-white font-weight-600">End {{$course['to']}}</li>
                                    </ul>
                                </div>
                            </div>
                            <p class="mb-15">{{$course['description']}}</p>
                        </div>
                    </article>
                </div>
            </div>
        </div>
    </div>
</section>
<section>
      <div class="container">
        <div class="row">
          <div class="col-md-6 col-md-push-3">
            <h4 class="mt-0 pt-5"> Paypal Order Form </h4>
            <img src="{{url('/')}}/public/assets/images/payment-card-logo-sm.png" alt="">
            <hr>
            <h5>COURSE - {{$course['title']}}</h5>
            <h5>TIME ( {{$course['from'].' : '.$course['to']}} )</h5>
            <p>Price ${{$course['price']-($course['price'] * $discount / 100)}} (Qty 1)</p>
            <div id="paypal-button-container" data-id="{{ $course['id'] }}" data-order="" data-url="{{ route('join_course',$course['id']) }}"></div>
          </div>
        </div>
      </div>
    </section>
@endsection
