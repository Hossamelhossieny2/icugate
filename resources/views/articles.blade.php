@extends('layouts.app')

@section('content')
    
<!-- Start main-content -->
  <div class="main-content-area">
    <!-- Section: sliders -->
    <section id="home" class="divider">
      <div class="container-fluid p-0">
      @include('sections.slider')
      </div>
    </section>

    <section>
      <div class="container mt-30 mb-30 pt-30 pb-30">
        <div class="row">

          <!-- Section: main section -->
          <div class="col-md-9 order-lg-2">
            <div class="col-md-12">
              <div class="widget widget_tag_cloud">
                <!-- <h4 class="widget-title widget-title-line-bottom line-bottom-theme-colored1">article Tags</h4> -->
                <div class="tagcloud">
                @foreach($tags as $tag)
                  <a href="@if($tag['count'] == 0) # @else {{ route('article.view.'.$usertype,[$cat_tag['cat_id'],$tag['id']]) }} @endif" class="tag-cloud-link @if($tag['id'] == $cat_tag['tag_id']) bg-primary text-white @endif">{{ $tag['title'] }} <span class="badge @if($tag['id'] != $cat_tag['tag_id']) bg-secondary @else bg-black @endif" @if($tag['count'] == 0) style="color:orange" @endif>{{ $tag['count'] }}</span></a>
                @endforeach
                </div>
            </div>
            </div>
            @include('sections.main_one_cat')
          </div>

          <!-- Section: left panet -->
          <div class="col-md-3 order-lg-1">
            @include('sections.one_side')
          </div>
          
        </div>
      </div>
    </section>
    
  </div>
  <!-- end main-content -->
  
@endsection
