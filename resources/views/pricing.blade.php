@extends('layouts.app')

@section('content')

@push('styles')
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.2.1/css/all.min.css" integrity="sha512-MV7K8+y+gLIBoVD59lQIYicR65iaqukzvf/nwasF0nqhPay5w/9lJmVM2hMDcnK1OnMGCdVK+iQrJ7lzPJQd1w==" crossorigin="anonymous" referrerpolicy="no-referrer" />
@endpush

 <!-- Start main-content -->
  <div class="main-content-area">
    <!-- Section: page title -->
    <section class="page-title layer-overlay overlay-dark-8 section-typo-light bg-img-center" data-tm-bg-img="{{ asset('assets/images/bg/bg10.jpg') }}">
      <div class="container pt-50 pb-50">
        <div class="section-content">
          <div class="row">
            <div class="col-md-12 text-center">
              <h2 class="title">icugate pricing</h2>
            </div>
          </div>
        </div>
      </div>
    </section>

    <!-- Section: about -->
    <section class="">
      <div class="container pb-50">
        <div class="row">
          <div class="row">
            @foreach($pricing as $price)
              <div class="col-sm-4">
                <div class="tm-sc-pricing-table pricing-table-style1 pricing-list-bordered pricing-table-hover-effect pricing-table-box-shadow text-center mb-lg-30 @if(auth()->user() && auth()->user()->type == $price['type']) bg-dark @endif">
                  <div class="pricing-table-inner-wrapper">
                    <div class="pricing-table-thumb">
                      <img src="{{ asset($price['img']) }}" alt="Image">
                    </div>
                    <div class="pricing-table-inner p-0 pt-30">
                      <div class="pricing-table-head">
                        <div class="pricing-table-title-area pt-0 pb-0">
                          <h4 class="pricing-table-title @if(auth()->user() && auth()->user()->type == $price['type']) text-white @endif">{{ $price['title'] }}</h4>
                          <h5 class="pricing-table-subtitle">{{ $price['brief'] }}</h5>
                        </div>
                        <div class="pricing-table-pricing">
                          <span class="pricing-table-prefix">$</span>
                          <span class="pricing-table-price">{{ $price['price'] }}</span>
                          <span class="pricing-table-separator">/</span>
                          <span class="pricing-table-postfix">Year</span>
                        </div>
                      </div>
                      <div class="pricing-table-content">
                        <ul>
                          <li>Browse articles</li>
                          <li>@if($price['videos'] == 1) Browse Videos / Presentations @else No Videos / Presentations @endif</li>
                          <li>@if($price['protocols'] == 1) View ICU procedures and protocols @else No ICU procedures and protocols @endif</li>
                          <li>@if($price['lectures'] == 1) publish articles or presentations @else Can't publish articles or presentations @endif</li>
                          <li>@if($price['lectures'] == 1) View Resident Lectures @else Can't view Resident Lectures @endif</li>
                          <li>CCESP Course {{$price['ccesp_discount']}} % OFF</li>
                        </ul>
                      </div>
                      <div class="pricing-table-footer">
                        @auth
                          @if(auth()->user()->type == $price['type'])
                          <a href="#" target="_self" class="btn btn-theme-colored1 btn-round"> Your current Plan </a>
                          @elseif(auth()->user()->type < $price['type'])
                          <a href="{{route('icugate_upgrade',$price['id'])}}" target="_self" class="btn btn-success btn-round"> upgrade </a>
                          @endif
                        @else
                        <a href="{{route('register')}}" target="_self" class="btn btn-theme-colored1 btn-round"> order Now </a>
                        @endif
                      </div>
                      @if($price['popular'] == 1)
                      <span class="pricing-table-label">Popular</span>
                      @endif
                    </div>
                  </div>
                </div>
              </div>
              @endforeach
              
            </div>
          </div>
        </div>
      </div>
    </section>

@endsection