@extends('layouts.app')

@section('content')

   <!-- Start main-content -->
  <div class="main-content-area">
    <!-- Section: page title -->
    <section class="page-title layer-overlay overlay-dark-8 section-typo-light bg-img-center" data-tm-bg-img="{{ asset('assets/images/bg/bg10.jpg') }}">
      <div class="container pt-50 pb-50">
        <div class="section-content">
          <div class="row">
            <div class="col-md-12 text-center">
              <h2 class="title">icugate Job List</h2>
            </div>
          </div>
        </div>
      </div>
    </section>

    <section>
      <div class="container pb-60">
      @if( Session::has('status') )
          <div class="alert alert-{{session('status')[1]}} alert-dismissible text-center">
            {{ session('status')[0] }}
          </div>
      @endif
      <ul>
        @foreach ($errors->all() as $message)
        <li><span class="help-block text-danger text-center">{{ $message }}</span></li>
        @endforeach
      </ul>
        <div class="row text-center">
        @if(!empty($jobs[0]['id']))
          @foreach($jobs as $job)
          <div class="col-sm-4">
            <div class="icon-box iconbox-border iconbox-theme-colored p-40">
              <a href="#" class="icon icon-gray icon-bordered icon-border-effect effect-flat @if($job['active'] == 0) bg-danger @else bg-success @endif text-white">
                <i class="pe-7s-users"></i>
              </a>
              <h5 class="icon-box-title">{{ $job['job_title'] }}</h5>
              <p class="text-gray">{{ $job['job_desc'] }}</p>
              <p class="text-gray">expected salary : {{ $job['salary'] }}</p>
              <p class="text-gray">applicants count : {{ $job['job_cv_count'] }}</p>
              @if($job['active'] != 0)
              <a class="btn btn-theme-colored2" href="{{ route('job.detail',[$job['id']]) }}">Apply Now</a>
              @else
              <span class="help-block text-danger text-center">Closed on {{ \Carbon\Carbon::parse($job['updated_at'])->format('M d Y') }}</span>
              @endif
            </div>
          </div>
          @endforeach
        @else
        <span class="help-block text-danger text-center">SORRY!! .. no jobs avaliable right now</span>
        @endif
        </div>
      </div>
    </section>

  </div>
  <!-- end main-content -->

@endsection