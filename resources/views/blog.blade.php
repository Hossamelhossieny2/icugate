@extends('layouts.app')

@section('content')

@push('styles')
<!-- <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.2.1/css/all.min.css" integrity="sha512-MV7K8+y+gLIBoVD59lQIYicR65iaqukzvf/nwasF0nqhPay5w/9lJmVM2hMDcnK1OnMGCdVK+iQrJ7lzPJQd1w==" crossorigin="anonymous" referrerpolicy="no-referrer" /> -->

@endpush

 <!-- Start main-content -->
  <div class="main-content-area">
    <!-- Section: page title -->
    <section class="page-title layer-overlay overlay-dark-8 section-typo-light bg-img-center" data-tm-bg-img="{{ asset('assets/images/bg/bg10.jpg') }}">
      <div class="container pt-50 pb-50">
        <div class="section-content">
          <div class="row">
            <div class="col-md-12 text-center">
              <h3 class="title">{{$post['title']}}</h3>
            </div>
          </div>
        </div>
      </div>
    </section>

   <section>
      <div class="container mt-30 mb-30 pt-30 pb-30">
        <div class="row">
          <div class="col-lg-9 order-lg-2">
            <div class="blog-posts single-post">
              <article class="post clearfix mb-0">
                <div class="entry-header mb-30">
                  <div class="post-thumb thumb"> 
                  <?php $file = str_replace(' ','%20',asset($post['file']));?>

                  <!-- <img src="images/shop/portfolio-big1.jpg" alt="images" class="img-responsive img-fullwidth">  -->
                  <iframe src='https://docs.google.com/viewer?url={{$file}}?format%3Dppt&embedded=true' frameborder="0" style="width:100%;height:640px;"></iframe>
                  </div>
                  <h2>{{$post['title']}}</h2>
                  <div class="entry-meta mt-0">
                    <span class="mb-10 text-gray-darkgray mr-10 font-size-13"><i class="far fa-calendar-alt mr-10 text-theme-colored1"></i> {{ $post['created_at']->format('d M Y') }}</span>
                    <!-- <span class="mb-10 text-gray-darkgray mr-10 font-size-13"><i class="far fa-comments mr-10 text-theme-colored1"></i> 214 Comments</span> -->
                    @if(!empty($post['views']))
                    <span class="mb-10 text-gray-darkgray mr-10 font-size-13"><i class="far fa-eye mr-10 text-theme-colored1"></i> {{$post['views']}} views</span>
                    @endif
                  </div>
                </div>
                <div class="entry-content">
                  <!-- <p>text</p> -->

                  <!-- <p>Bring to the table win-win survival strategies to ensure proactive domination. At the end of the day, going forward, a new normal that has evolved from generation X is on the runway heading towards a streamlined cloud solution. User generated content in real-time will have multiple touchpoints for offshoring natoque sem morbi hac nunc ultricies.</p> -->

                  <blockquote class="tm-sc-blockquote blockquote-style6  border-left-theme-colored quote-icon-theme-colored">
                    <p >{!! $post['text'] !!}</p>
                    @if(!empty($post['journal']))
                    <footer><cite >{{$post['journal']}}</cite></footer>
                    @endif
                  </blockquote>
                  <!-- <h5>catego</h5>
                  <p>Habitasse justo, sed justo. Senectus morbi, fermentum magna id tortor. Lacinia sociis morbi erat ultricies dictumst condimentum dictum nascetur? Vitae litora erat penatibus nam lorem. Euismod tempus, mollis leo tempus? Semper est cursus viverra senectus lectus feugiat id! Odio porta nibh dictumst nulla taciti lacus nam est praesent.</p> -->


                  <h5>category / section / tag</h5>
                  <p>this file is related to :</p>
                  <ul>
                    <li>{{\App\Models\Cat::find($post['cat_id'])->title}}</li>
                    @if(!empty($post['tag_id']))
                    <li>{{\App\Models\Tag::find($post['tag_id'])->title}}</li>
                    @endif
                  </ul>
                </div>
              </article>

              <!-- <div class="comment-box mt-30">
                <h3>Leave a Comment</h3>
                <form role="form" id="comment-form">
                <div class="row">
                  <div class="col-6 pt-0 pb-0">
                    <div class="mb-3">
                      <input type="text" class="form-control" required name="contact_name" id="contact_name" placeholder="Enter Name">
                    </div>
                    <div class="mb-3">
                      <input type="text" required class="form-control" name="contact_email2" id="contact_email2" placeholder="Enter Email">
                    </div>
                    <div class="mb-3">
                      <input type="text" placeholder="Enter Website" required class="form-control" name="subject">
                    </div>
                  </div>
                  <div class="col-6">
                    <div class="mb-3">
                      <textarea class="form-control" required name="contact_message2" id="contact_message2"  placeholder="Enter Message" rows="7"></textarea>
                    </div>
                  </div>
                  <div class="col-12">
                    <div class="mb-3">
                      <button type="submit" class="btn btn-theme-colored1 btn-round m-0" data-loading-text="Please wait...">Submit</button>
                    </div>
                  </div>
                </div>
                </form>
              </div> -->
            </div>
          </div>
          <div class="col-md-3">
            @include('sections.left')
          </div>
        </div>
      </div>
    </section>

  </div>

@endsection