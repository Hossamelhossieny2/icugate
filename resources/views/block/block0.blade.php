<div class="isotope-item">
    <div class="isotope-item-inner">
      <div class="tm-sc-blog blog-style-default mb-30">
        <article class="post type-post status-publish format-standard has-post-thumbnail">
          <div class="entry-header">
            <div class="post-thumb lightgallery-lightbox">
              <div class="post-thumb-inner">
                @if(!empty($post['img']))
                <div class="thumb"> <img src="{{ asset('up/XXX'.$post['img']) }}" alt="Image"/></div>
                @else
                <div class="thumb"> <img src="{{ asset('up/icugate_news.jpg') }}" alt="Image"/></div>
                @endif
              </div>
            </div>
            
            @if(!empty($post['link']))
            <a class="link" href="#"><i class="fas fa-link"></i></a>
            <?php $link = $post['link'];?>
            @endif
            
            @if(!empty($post['file']))
            <a class="link" href="#"><i class="fas fa-file-pdf"></i></a>
            <?php 
            $link = route('blog.details',$post['id']);
            $extention = explode('.',$post['file']);
            $file_ext = $extention[1];
            ?>
            @endif
            
          </div>
          <div class="entry-content">
            <h4 class="entry-title"><a target="_blank" href="@if(!empty($link)) {{ $link }} @else # @endif" rel="bookmark">{{ $post['title'] }}</a></h4>
            <div class="entry-meta mt-0 mb-0">
              @if(!empty($post['link']))
              <span class="mb-10 text-gray-darkgray mr-10 font-size-13"> <i class="fas fa-link"></i></span>
              @endif
              @if(!empty($post['file']))
              <span class="badge bg-secondary">{{ $file_ext }}</span>
              <span class="mb-10 text-gray-darkgray mr-10 font-size-13"> <i class="fas fa-file-pdf"></i></span>
              @endif
              @if(!empty($post['img']))
                <span class="mb-10 text-gray-darkgray mr-10 font-size-13"> <i class="fas fa-image"></i></i></span>
              @endif
              <span class="mb-10 text-gray-darkgray mr-10 font-size-13"><i class="far fa-calendar-alt mr-10 text-theme-colored1"></i> {{ $post['created_at']->format('d M Y') }}</span>

              @if(!empty($post['views']))
                <span class="mb-10 text-gray-darkgray mr-10 font-size-13"><i class="far fa-eye mr-10 text-theme-colored1"></i> {{$post['views']}} views</span>
              @endif
            </div>

            <div class="post-excerpt">
              <div class="mascot-post-excerpt">{{$post['text']}}</div>
            </div>
            <div class="post-btn-readmore"> 
            <a target="_blank" href="@if(!empty($link)) {{ $link }} @else # @endif" class="btn btn-plain-text-with-arrow"> View Details </a>
            
            </div>
            <div class="clearfix"></div>
          </div>
        </article>
      </div>
    </div>
  </div>