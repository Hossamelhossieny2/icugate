<article class="post clearfix">
                <div class="entry-header">
                  <div class="post-thumb thumb"> 
                    <img src="{{asset('up/XXX'.$post['img'])}}" alt="" class="img-responsive img-fullwidth" style="border: 2px solid #B1D1E3;"> 
                  </div>
                </div>
                <div class="entry-content border-1px p-20 pr-10">
                  <div class="entry-meta media mt-0 no-bg no-border">
                    <div class="entry-date media-left text-center flip bg-theme-colored pt-5 pr-15 pb-5 pl-15">
                      <ul>
                        <li class="font-16 text-white font-weight-600">{{date('d',strtotime($post['date']))}}</li>
                        <li class="font-12 text-white text-uppercase">{{date('M',strtotime($post['date']))}}</li>
                        <li class="font-12 text-white text-uppercase">{{date('Y',strtotime($post['date']))}}</li>
                      </ul>
                    </div>
                    <div class="media-body pl-15">
                      <div class="event-content pull-left flip">
                          <h4 class="entry-title text-white text-uppercase m-0 mt-5">
                            <!-- internal link -->
                            <a target="_BALNK" href="{{url('blog/'.$post['id'])}}" onclick="addVisitCount({{$post['id']}})">{{$post['title']}}</a>
                           </h4>
                          
                          <span class="mb-10 text-gray-darkgray mr-10 font-13" style="float: right"><i class="fa fa-eye-slash mr-5 text-theme-colored"></i> {{$post['views']}} views</span>                       
                        
                        @if($post['hot_topic'] == 1)
                        <span class="mb-10 text-gray-darkgray mr-10 font-13" style="float: right"><i class="fa fa-fire mr-5 text-danger"></i> Hot Topic</span>
                        @endif
                      </div>
                    </div>
                  </div>
                  <p class="mt-10">{{$post['text']}}</p>
                  <div class="sharethis-inline-share-buttons"></div>
                </div>
              </article>