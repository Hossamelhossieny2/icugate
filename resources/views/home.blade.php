@extends('layouts.app')

@section('content')

@push('styles')

@endpush

<!-- Start main-content -->
  <div class="main-content-area">
    <!-- Section: sliders -->
    <section id="home" class="divider">
      <div class="container-fluid p-0">
      @include('sections.slider')
      </div>
    </section>

    <section>
      <div class="container mt-30 mb-30 pt-30 pb-30">
        <div class="row">

          <!-- Section: main section -->
          <div class="col-md-6 order-lg-2">
            @include('sections.main')
          </div>

          <!-- Section: left panet -->
          <div class="col-md-3 order-lg-1">
            @include('sections.left')
          </div>
          
          <!-- Section: right panel -->
          <div class="col-md-3 order-lg-3">
            @include('sections.right')
          </div>

        </div>
      </div>
    </section>
    @include('sections.plusmain')
    
  </div>
  <!-- end main-content -->

@push('acripts')

@endpush

@endsection
