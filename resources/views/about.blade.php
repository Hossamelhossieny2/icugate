@extends('layouts.app')

@section('content')

@push('styles')
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.2.1/css/all.min.css" integrity="sha512-MV7K8+y+gLIBoVD59lQIYicR65iaqukzvf/nwasF0nqhPay5w/9lJmVM2hMDcnK1OnMGCdVK+iQrJ7lzPJQd1w==" crossorigin="anonymous" referrerpolicy="no-referrer" />
@endpush

 <!-- Start main-content -->
  <div class="main-content-area">
    <!-- Section: page title -->
    <section class="page-title layer-overlay overlay-dark-8 section-typo-light bg-img-center" data-tm-bg-img="{{ asset('assets/images/bg/bg10.jpg') }}">
      <div class="container pt-50 pb-50">
        <div class="section-content">
          <div class="row">
            <div class="col-md-12 text-center">
              <h2 class="title">About icugate</h2>
            </div>
          </div>
        </div>
      </div>
    </section>

    <!-- Section: about -->
    <section class="">
      <div class="container pb-0">
        <div class="row">
          <div class="col-md-8">
            <h4 class="text-uppercase text-theme-colored2 mt-sm-10">Welcome to icugate</h4>
            <h2 class="mt-0">Why icugate ?</h2>
            <?php $content = explode('Thank you.',$page->content); ?>
            <p class="lead">{!! $content[0] !!}</p>
            <p class="lead">Thank you .</p>
            <h3 class="text-theme-colored mb-0">{!! $content[1] !!}</h3>
            <p><span>Consultant Intensivist & Head of Adult ICU Department</span></p>

            <p class="mt-20"><img src="{{ asset('assets/images/signature.png') }}" alt=""></p>
          </div>
          <div class="col-md-4">
            <img src="{{ asset('assets/images/about/dc1.png') }}" alt="">
          </div>
        </div>
      </div>
    </section>

    <!-- Section: Depertment -->
    <section class="divider parallax layer-overlay text-center" data-parallax-ratio="0.7" data-tm-bg-img="{{ asset('assets/images/shop/portfolio-big1.jpg') }}" style="background-image: url({{ asset('assets/images/shop/portfolio-big1.jpg') }}); background-position: 50% 85px;">
      <div class="container">
        <div class="section-title text-center">
          <div class="row justify-content-md-center">
            <div class="col-md-8">
              <h2 class="text-uppercase mt-0 line-height-1 text-white">Articles topic</h2>
              <div class="title-icon">
                <img class="mb-10" src="{{ asset('assets/images/title-icon.png') }}" alt="">
              </div>
            </div>
          </div>
        </div>
        <div class="section-content">
          <div class="row">
            @foreach($tags as $tag)
              @if($tag['display'] == 1)
              <div class="col-xs-12 col-sm-6 col-md-4 mb-30">
                <div class="p-20 bg-white">
                  <img src="{{ asset('assets/images/tags/'.$tag['image']) }}" alt="">
                  <h3 class=""><a href="#">{{ $tag['title'] }}</a></h3>
                  <a href="{{ route('article.view.free',[8,$tag['id']]) }}" class="btn btn-flat btn-theme-colored1 mt-15 text-theme-color-2">Read More</a>                
                </div>
              </div>
              @endif
            @endforeach
          </div>
        </div>
      </div>
    </section>
  
  <!-- end main-content -->

   

    <!-- Divider: Funfact -->
    <section class="divider parallax layer-overlay overlay-theme-colored1-8" data-tm-bg-img="{{ asset('assets/images/bg/bg10.jpg') }}" style="background-image: url(&quot;assets/images/bg/bg10.jpg&quot;); background-position: 50% 36px;">
      <div class="container">
        <div class="row section-typo-light">
          <div class="col-lg-3 col-md-6">
            <div class="tm-sc-funfact funfact text-center">
              <div class="funfact-icon mb-20"><i class="flaticon-medical-family21"></i></div>
              <h2 class="counter"> <span class="animate-number appeared" data-value="{{ $stat_bar['users_count'] }}" data-animation-duration="1500">{{ $stat_bar['users_count'] }}</span><span class="counter-postfix">+</span></h2>
              <h4 class="title">Happy Patients</h4>
            </div>
          </div>
          <div class="col-lg-3 col-md-6">
            <div class="tm-sc-funfact funfact text-center">
              <div class="funfact-icon mb-20"><i class="flaticon-medical-first32"></i></div>
              <h2 class="counter"> <span class="animate-number appeared" data-value="{{ $stat_bar['posts_count'] }}" data-animation-duration="1500">{{ $stat_bar['posts_count'] }}</span><span class="counter-postfix">+</span></h2>
              <h4 class="title">Our Articles</h4>
            </div>
          </div>
          <div class="col-lg-3 col-md-6">
            <div class="tm-sc-funfact funfact text-center">
              <div class="funfact-icon mb-20"><i class="flaticon-medical-medical51"></i></div>
              <h2 class="counter"> <span class="animate-number appeared" data-value="240" data-animation-duration="1500">240</span><span class="counter-postfix">+</span></h2>
              <h4 class="title">Our specialist</h4>
            </div>
          </div>
          <div class="col-lg-3 col-md-6">
            <div class="tm-sc-funfact funfact text-center">
              <div class="funfact-icon mb-20"><i class="flaticon-medical-caduceus3"></i></div>
              <h2 class="counter"> <span class="animate-number appeared" data-value="{{ $stat_bar['site_visit'] }}" data-animation-duration="1500">{{ $stat_bar['site_visit'] }}</span><span class="counter-postfix">+</span></h2>
              <h4 class="title">visits Points</h4>
            </div>
          </div>
        </div>
      </div>
    </section>

     <!-- Section: Services -->
    <section class="bg-silver-light">
      <div class="container">
        <div class="section-title text-center mb-40">
          <div class="row justify-content-md-center">
            <div class="col-md-8">
              <h2 class="text-uppercase mt-0 line-height-1">Website content</h2>
              <div class="title-icon">
                <img class="mb-10" src="{{ asset('assets/images/title-icon.png') }}" alt="">
              </div>
              <p>we have Huge resource of medical content for example</p>
            </div>
          </div>
        </div>
        <div class="section-content">
          <div class="row">
            <div class="col-sm-6 col-lg-4">
              <div class="tm-sc-icon-box icon-box icon-left text-left iconbox-centered-in-responsive iconbox-theme-colored1 animate-icon-on-hover animate-icon-rotate-y mb-40">
                <div class="icon-box-wrapper">
                  <div class="icon-wrapper"> <a class="icon icon-dark icon-circled"> <i class="fa-solid fa-square-rss"></i> </a></div>
                  <div class="icon-text">
                    <h3 class="icon-box-title">Medical articles</h3>
                  </div>
                  <div class="clearfix"></div>
                </div>
              </div>
            </div>
            <div class="col-sm-6 col-lg-4">
              <div class="tm-sc-icon-box icon-box icon-left text-left iconbox-centered-in-responsive iconbox-theme-colored1 animate-icon-on-hover animate-icon-rotate-y mb-40">
                <div class="icon-box-wrapper">
                  <div class="icon-wrapper"> <a class="icon icon-dark icon-circled"> <i class="fa-solid fa-book"></i> </a></div>
                  <div class="icon-text">
                    <h3 class="icon-box-title">Ongoing Researches</h3>
                  </div>
                  <div class="clearfix"></div>
                </div>
              </div>
            </div>
            <div class="col-sm-6 col-lg-4">
              <div class="tm-sc-icon-box icon-box icon-left text-left iconbox-centered-in-responsive iconbox-theme-colored1 animate-icon-on-hover animate-icon-rotate-y mb-40">
                <div class="icon-box-wrapper">
                  <div class="icon-wrapper"> <a class="icon icon-dark icon-circled"> <i class="fa-solid fa-hand-holding-medical"></i> </a></div>
                  <div class="icon-text">
                    <h3 class="icon-box-title">Critical Care Subjects</h3>
                  </div>
                  <div class="clearfix"></div>
                </div>
              </div>
            </div>
            <div class="col-sm-6 col-lg-4">
              <div class="tm-sc-icon-box icon-box icon-left text-left iconbox-centered-in-responsive iconbox-theme-colored1 animate-icon-on-hover animate-icon-rotate-y mb-sm-40">
                <div class="icon-box-wrapper">
                  <div class="icon-wrapper"> <a class="icon icon-dark icon-circled"> <i class="fa-solid fa-photo-film"></i> </a></div>
                  <div class="icon-text">
                    <h3 class="icon-box-title">Presentation & Videos</h3>
                  </div>
                  <div class="clearfix"></div>
                </div>
              </div>
            </div>
            <div class="col-sm-6 col-lg-4">
              <div class="tm-sc-icon-box icon-box icon-left text-left iconbox-centered-in-responsive iconbox-theme-colored1 animate-icon-on-hover animate-icon-rotate-y mb-sm-40">
                <div class="icon-box-wrapper">
                  <div class="icon-wrapper"> <a class="icon icon-dark icon-circled"> <i class="fa-solid fa-graduation-cap"></i> </a></div>
                  <div class="icon-text">
                    <h3 class="icon-box-title">CCESP Courses</h3>
                  </div>
                  <div class="clearfix"></div>
                </div>
              </div>
            </div>
            <div class="col-sm-6 col-lg-4">
              <div class="tm-sc-icon-box icon-box icon-left text-left iconbox-centered-in-responsive iconbox-theme-colored1 animate-icon-on-hover animate-icon-rotate-y">
                <div class="icon-box-wrapper">
                  <div class="icon-wrapper"> <a class="icon icon-dark icon-circled"> <i class="fa-solid fa-school"></i> </a></div>
                  <div class="icon-text">
                    <h3 class="icon-box-title">Educational Support</h3>
                  </div>
                  <div class="clearfix"></div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>

    <!-- Divider: Funfact -->


    
  <section class="text-center" data-tm-bg-img="{{ asset('assets/images/pattern/p10.png') }}" style="background-image: url({{asset('assets/images/pattern/p10.png')}});">
      <div class="container">
        <div class="section-title text-center  mb-40">
          <div class="row justify-content-md-center">
            <div class="col-md-8">
            
              <h2 class=" mt-0 line-height-1">Our <span class="text-theme-colored1">Team</span></h2>
              <div class="title-icon">
                <img class="mb-10" src="{{ asset('assets/images/title-icon.png') }}" alt="">
              </div>
              <p>meet our team of professionals</p>
            </div>
          </div>
        </div>
        <div class="row mtli-row-clearfix">
          <div class="col-md-12">
            <div class="owl-carousel owl-theme tm-owl-carousel-4col">
              @for($i=0;$i<4;$i++)
              <div class="item">
                <div class="team-members border-bottom-theme-color-2px text-center maxwidth400">
                  <div class="team-thumb">
                    <img class="img-fullwidth" alt="" src="{{ asset('up/'.$teams[$i]->img) }}">
                    <div class="team-overlay"></div>
                  </div>
                  <div class="team-details bg-silver-light pt-20 pb-30">
                    <h3 class="mb-0">{{ $teams[$i]->name }}</h3>
                    <h6 class="text-theme-colored1 mb-20">{{ $teams[$i]->jobtitle }}</h6>
                    <ul class="styled-icons icon-dark icon-sm icon-theme-colored1 icon-circled clearfix">
                      <li><a class="social-link" href="#"><i class="fab fa-facebook"></i></a></li>
                      <li><a class="social-link" href="#"><i class="fab fa-instagram"></i></a></li>
                      <li><a class="social-link" href="#"><i class="fab fa-twitter"></i></a></li>
                      <li><a class="social-link" href="#"><i class="fab fa-youtube"></i></a></li>
                    </ul>
                  </div>
                </div>
              </div>
              @endfor
              
            </div>
          </div>
        </div>
      </div>
    </section>

   </div>

@endsection