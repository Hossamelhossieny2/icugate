@extends('layouts.admin')

@section('content')

            <div class="row">
            <div class="col-12">
                <div class="box">
                    <div class="box-header with-border">
                      <h4 class="box-title">{{$page}}</h4>
                      <a class="btn btn-success pull-right" href="{{route('admin.newyoutubech')}}">add channel</a>
                      @if( Session::has('status') )
                      
                           <div class="alert alert-{{session('status')[1]}} alert-dismissible text-center">
                            {{ session('status')[0] }}
                          </div>
                      
                    @endif
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body">
                        <div class="table-responsive">
                          <table id="example1" class="table table-bordered table-striped text-center">
                            <thead>
                                <tr>
                                    <th>channel name</th>
                                    <th>channel id</th>
                                    <th>action</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($data as $d)
                                  
                                    <tr class="bg-dark">
                                        <td>
                                            {{$d['title']}}
                                        </td>
                                        
                                        <td>
                                           {{$d['youtube_id']}} 
                                        </td>
                                        
                                        <td>
                                            <a href="{{route('admin.youtube.editch',$d['id'])}}" class="btn btn-success"><i class="fa fa-pencil"></i></a>
                                            <a href="{{route('admin.youtube.delch',$d['id'])}}" class="btn btn-danger" title="delete" onclick="return confirm('Are you sure?')"><i class="fa fa-trash"></i></a>
                                        </td>
                                    </tr>
                                @endforeach 
                            </tbody>
                            
                          </table>
                        </div>
                        
                    </div>
                    <!-- /.box-body -->
                    
                  </div>
            </div>
        </div>
       
@endsection

