@extends('layouts.admin')

@section('content')

            <div class="row">
                <div class="col-12">
                    <div class="box">
                        <div class="box-header with-border">
                          <h4 class="box-title">{{$page}}</h4>
                           <a class="btn btn-success pull-right" href="{{route('admin.newyoutubech')}}">add channel</a>
                    
                        </div>
                        <div class="box-body">
                          <form method="post" >
                             {{ csrf_field() }}
                             
                            <!-- text input -->
                            <div class="form-group">
                              <label>channel title</label>
                              <input type="text" name="title" class="form-control" value="{{$data['title']}}" />
                            </div>

                            <div class="form-group">
                              <label>channel id</label>
                              <input type="text" name="vid_id" class="form-control" value="{{$data['youtube_id']}} " />
                            </div>

                            
                            <input type="hidden" name="id" value="{{$data['id']}}" />
                            
                            <div class="box-footer">
                                <input type="submit" class="btn btn-info pull-right" value="change">
                              </div>

                          </form>
                        </div>
                        <div class="box-footer">
                          <a>click here </a>if you need to review our terms and conditions .
                        </div>
                    </div>
                </div>
            </div>

@endsection

