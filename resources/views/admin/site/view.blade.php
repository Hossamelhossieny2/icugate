@extends('layouts.admin')

@section('content')

            <div class="row">
            <div class="col-12">
                <div class="box">
                    <div class="box-header with-border">
                      <h4 class="box-title">{{$page}}</h4>
                            @if( Session::has('status') )
                                <div class="alert alert-{{session('status')[1]}} alert-dismissible text-center">
                            {{ session('status')[0] }}
                          </div>
                      
                    @endif
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body">
                        <div class="table-responsive">
                          <table id="example1" class="table table-bordered table-striped text-center">
                            <thead>
                                <tr>
                                    <th>site name</th>
                                    <th>site description</th>
                                    <th>contact phone</th>
                                    <th>contact email</th>
                                    <th>paypal account</th>
                                    <th>action</th>
                                </tr>
                            </thead>
                            <tbody>

                                  
                                    <tr class="bg-dark">
                                        <td>
                                            {{ $site['home_job_title'] }}
                                        </td>
                                        <td>
                                           {{ $site['home_job_text'] }}
                                        </td>
                                        <td>
                                           {{ $site['phone'] }} 
                                        </td>
                                        <td>
                                           {{ $site['email'] }}
                                        </td>
                                        <td>
                                            {{ $site['paypal_account'] }}
                                        </td>
                                        <td>
                                            <a href="{{route('admin.site.edit')}}" class="btn btn-success"><i class="fa fa-pencil"></i></a>
                                        </td>
                                    </tr>
                                       
                            </tbody>
                            
                          </table>
                        </div>
                        <div class="table-responsive">
                          <table id="example1" class="table table-bordered table-striped text-center">
                            <thead>
                                <tr>
                                    <th>facebook</th>
                                    <th>google</th>
                                    <th>twitter</th>
                                    <th>instagram</th>
                                    <th>youtube</th>
                                    <th>action</th>
                                </tr>
                            </thead>
                            <tbody>

                                  
                                    <tr class="bg-dark">
                                        <td>
                                            {{ $site['facebook'] }}
                                        </td>
                                        <td>
                                           {{ $site['google'] }}
                                        </td>
                                        <td>
                                           {{ $site['twitter'] }} 
                                        </td>
                                        <td>
                                           {{ $site['instgram'] }}
                                        </td>
                                        <td>
                                           {{ $site['youtube'] }}
                                        </td>
                                        <td>
                                            <a href="{{route('admin.site.social')}}" class="btn btn-success"><i class="fa fa-pencil"></i></a>
                                        </td>
                                    </tr>
                                       
                            </tbody>
                            
                          </table>
                        </div>
                    </div>
                    <!-- /.box-body -->
                    
                  </div>
            </div>
        </div>

@endsection

