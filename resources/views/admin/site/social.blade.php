@extends('layouts.admin')

@section('content')

            <div class="row">
                <div class="col-12">
                    <div class="box">
                        <div class="box-header with-border">
                          <h4 class="box-title">{{$page}}</h4>
                           
                    
                        </div>
                        <div class="box-body">
                          <form method="post" >
                             {{ csrf_field() }}
                             
                            <!-- text input -->
                            <div class="form-group">
                              <label>facebook</label>
                              <input type="text" name="facebook" class="form-control" value="{{ $site['facebook'] }}" />
                            </div>

                            <div class="form-group">
                              <label>google</label>
                              <input type="text" name="google" class="form-control" value="{{ $site['google'] }}" />
                                        
                            </div>

                            <div class="form-group">
                              <label>twitter</label>
                              <input type="text" name="twitter" class="form-control" value=" {{ $site['twitter'] }} " />
                            </div>

                            <div class="form-group">
                              <label>instgram</label>
                              <input type="text" name="instgram" class="form-control" value=" {{ $site['instgram'] }} " />
                            </div>

                           <div class="form-group">
                              <label>youtube</label>
                              <input type="text" name="youtube" class="form-control" value="{{ $site['youtube'] }}" />
                            </div>
                           

                            <div class="box-footer">
                                <input type="submit" class="btn btn-info pull-right" value="change">
                              </div>

                          </form>
                        </div>
                        <div class="box-footer">
                          <a>click here </a>if you need to review our terms and conditions .
                        </div>
                    </div>
                </div>
            </div>
        
@endsection

