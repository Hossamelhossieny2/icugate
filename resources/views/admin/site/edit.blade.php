@extends('layouts.admin')

@section('content')


            <div class="row">
                <div class="col-12">
                    <div class="box">
                        <div class="box-header with-border">
                          <h4 class="box-title">{{$page}}</h4>
                    
                        </div>
                        <div class="box-body">
                          <form method="post" >
                             {{ csrf_field() }}
                             
                            <!-- text input -->
                            <div class="form-group">
                              <label>site title</label>
                              <input type="text" name="home_job_title" class="form-control" value="{{ $site['home_job_title'] }}" />
                            </div>

                            <div class="form-group">
                              <label>site desc in title</label>
                              <textarea name="home_job_text" class="form-control" >{{ $site['home_job_text'] }}</textarea>
                            </div>

                            <div class="form-group">
                              <label>site keywords in google search</label>
                              <textarea name="site_kw" class="form-control" >{{ $site['site_kw'] }}</textarea>
                            </div>

                            <div class="form-group">
                              <label>site description in google search</label>
                              <textarea name="site_des" class="form-control" >{{ $site['site_des'] }}</textarea>
                            </div>

                            <div class="form-group">
                              <label>contact email</label>
                              <input type="email" name="email" class="form-control" value="{{ $site['email'] }} " />
                            </div>

                            <div class="form-group">
                              <label>contact phone</label>
                              <input type="text" name="phone" class="form-control" value="{{ $site['phone'] }} " maxlength="15"/>
                            </div>

                           <div class="form-group">
                              <label>paypal account</label>
                              <input type="text" name="paypal_account" class="form-control" value="{{ $site['paypal_account'] }}" />
                            </div>
                           

                            <div class="box-footer">
                                <input type="submit" class="btn btn-info pull-right" value="change">
                              </div>

                          </form>
                        </div>
                       
                    </div>
                </div>
            </div>
                

@endsection

