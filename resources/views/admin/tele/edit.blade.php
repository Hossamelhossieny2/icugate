@extends('layouts.admin')

@section('content')

            <div class="row">
                <div class="col-12">
                    <div class="box">
                        <div class="box-header with-border">
                          <h4 class="box-title">{{$page}}</h4>
                           
                    
                        </div>
                        <div class="box-body">
                          <form method="post" >
                             {{ csrf_field() }}
                             
                            <!-- text input -->
                            <div class="form-group">
                              <label>consultation name</label>
                              <input type="text" name="name" class="form-control" value="{{ $data['name'] }}" />
                            </div>

                            <div class="form-group">
                              <label>consultation price</label>
                              <input type="text" name="price" class="form-control" value="{{ $data['price'] }}" />
                            </div>

                            <input type="hidden" name="id" value="{{ $data['id'] }}" />
                            
                            <div class="box-footer">
                                <input type="submit" class="btn btn-info pull-right" value="change">
                              </div>

                          </form>
                        </div>
                       
                    </div>
                </div>
            </div>

@endsection

