@extends('layouts.admin')

@section('content')

<?php $a=1;$a1=1;$total=0;$total1=0;?>

            <div class="row">
            <div class="col-12">
                <div class="box">
                  
                    <div class="box-header with-border">
                      <h4 class="box-title">{{$page}}</h4>
                      @if( Session::has('status') )
                      
                           <div class="alert alert-{{session('status')[1]}} alert-dismissible text-center">
                            {{ session('status')[0] }}
                          </div>
                      
                    @endif
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body">
                      <form method="get">
                      <div class="row">
                        <div class="col-md-2">
                          <label>from</label>
                          <input name="from" type="date" class="form-control" placeholder="from" />
                        </div>
                        <div class="col-md-2">
                          <label>to</label>
                          <input name="to" type="date" class="form-control" placeholder="to" />
                        </div>
                        <div class="col-md-2">
                          <label>User email</label>
                          <input name="email" type="email" class="form-control" placeholder="type user email" />
                        </div>
                        <div class="col-md-2">
                          <label>payment for</label>
                          <select name="payment_type" class="form-control">
                            <option value="">all payments</option>
                            <option value="1">subscription</option>
                            <option value="2">course</option>
                            <option value="3">consulting</option>
                          </select>
                        </div>
                        <div class="col-md-2">
                          <label>amount</label>
                          <input name="amount" type="number" class="form-control" placeholder="type amount" />
                        </div>
                        <div class="col-md-2">
                          <label>search</label>
                          <input type="submit" class="form-control btn btn-warning" value="search now" />
                        </div>
                      </div>
                      </form>
                      <hr>
                        <div class="table-responsive">
                          <table id="example1" class="table table-bordered table-striped text-center">
                            <thead>
                                <tr>
                                    <th>name</th>
                                    <th>user email</th>
                                    <th>user mobile</th>
                                    <th>profession</th>
                                    <th>Paid</th>
                                    <th>for</th>
                                     <th>kind</th>
                                    <th>Payed By</th>
                                    <th>Time</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($data as $d)
                                  
                                    <tr class="bg-dark">
                                        <td>
                                            {{$d['user']['f_name'].' '.$d['user']['l_name']}}
                                        </td>
                                        <td>
                                           {{$d['user']['email']}} 
                                        </td>
                                        <td>
                                           {{$d['user']['mobile']}}
                                        </td>
                                        <td>
                                           {{$d['user']['profession']}}
                                        </td>
                                        <td>
                                           {{$d['amount']}}
                                           <?php $total =  $total+$d->amount;?>
                                        </td>
                                       <td>
                                           {{\App\Models\PaypalOrderType::find($d['payment_type'])->name}}
                                        </td>
                                        <td>
                                        @if($d['payment_type'] == 1)
                                           {{\App\Models\Pricing::find($d['item_id'])->title}}
                                        @elseif($d['payment_type'] == 2)
                                            {{\App\Models\Course::find($d['item_id'])->title}}
                                        @elseif($d['payment_type'] == 3)
                                            {{\App\Models\TeleConsultation::find($d['item_id'])->name}}
                                        @endif
                                        </td>
                                        
                                        <td>
                                           {{$d['admin']['f_name'].' '.$d['admin']['l_name']}}
                                        </td>
                                        <td>
                                           {{$d['created_at']}}
                                        </td>
                                        
                                    </tr>
                                  
                                @endforeach 
                                <tfoot>
                                  <th colspan="4">TOTAL</th>
                                  <th>{{ $total }}</th>
                                  <th colspan="4"></th>
                                </tfoot>
                            </tbody>
                            
                          </table>
                        </div>
                        
                    </div>
                    <!-- /.box-body -->
                    
                    
                  </div>
            </div>
        </div>
              

@endsection

