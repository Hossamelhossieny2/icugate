@extends('layouts.admin')

@section('content')

            <div class="row">
            <div class="col-12">
                <div class="box">
                    <div class="box-header with-border">
                      <h4 class="box-title">{{$page}}</h4>
                      @if( Session::has('status') )
                      
                           <div class="alert alert-{{session('status')[1]}} alert-dismissible text-center">
                            {{ session('status')[0] }}
                          </div>
                      
                    @endif
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body">
                        <div class="table-responsive">
                          <table id="example1" class="table table-bordered table-striped text-center">
                            <thead>
                                <tr>
                                    <th>first name</th>
                                    <th>last name</th>
                                    <th>user email</th>
                                    <th>user mobile</th>
                                    <th>user plan</th>
                                    <th>subscription</th>
                                    <th>action</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($data as $d)
                                  
                                    <tr class="bg-dark">
                                        <td>
                                            {{$d['f_name']}}
                                        </td>
                                        <td>
                                           {{$d['l_name']}}
                                        </td>
                                        <td>
                                           {{$d['email']}} 
                                        </td>
                                        <td>
                                           {{$d['mobile']}}
                                        </td>
                                        <td>
                                           <?php
                                           if($d['type'] == 1){
                                               $plan = 'silver';
                                           }elseif($d['type'] == 2){
                                               $plan = 'gold';
                                           }
                                           echo $plan;
                                           ?>
                                        </td>
                                        <td>
                                           {{$d['created_at']}}
                                        </td>
                                        <td>
                                            <a href="{{route('admin.user.edit',$d['id'])}}" class="btn btn-success">activate</a>
                                        </td>
                                    </tr>
                                @endforeach 
                            </tbody>
                            
                          </table>
                        </div>
                        
                    </div>
                    <!-- /.box-body -->
                    
                    
                  </div>
            </div>
        </div>
       
@endsection

