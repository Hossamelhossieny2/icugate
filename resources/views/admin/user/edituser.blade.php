@extends('layouts.admin')

@section('content')

            <div class="row">
            <div class="col-12">
                <div class="box">
                    <div class="box-header with-border">
                      <h4 class="box-title">{{$page}}</h4>
                      @if( Session::has('status') )
                      
                           <div class="alert alert-{{session('status')[1]}} alert-dismissible text-center">
                            {{ session('status')[0] }}
                          </div>
                      
                    @endif
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body">
                        <div class="table-responsive">
                          <table id="example1" class="table table-bordered table-striped text-center">
                            <thead>
                                <tr>
                                    <th>first name</th>
                                    <th>last name</th>
                                    <th>user email</th>
                                    <th>user mobile</th>
                                    <th>user plan</th>
                                    <th>action</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($data as $d)
                                  
                                    <tr class="bg-dark">
                                        <td>
                                            {{$d['f_name']}}
                                        </td>
                                        <td>
                                           {{$d['l_name']}}
                                        </td>
                                        <td>
                                           {{$d['email']}} 
                                        </td>
                                        <td>
                                           {{$d['mobile']}}
                                        </td>
                                        <td>
                                           <?php
                                           if($d['type'] == 1){
                                               $plan = 'gold';
                                           }elseif($d['type'] == 2){
                                               $plan = 'platinuim';
                                           }elseif($d['type'] == 0){
                                               $plan = 'silver';
                                           }
                                           echo $plan;
                                           ?>
                                        </td>
                                        <td>
                                            <a href="{{route('admin.edituser.edit',$d['id'])}}" class="btn btn-success" title="edit user"><i class="fa fa-pencil"></i></a>
                                            <a href="{{route('admin.user.deactive',$d['id'])}}" class="btn btn-warning" title="deactivate user"><i class="fa fa-unlock-alt"></i></a>
                                            @if($d['is_admin'] == 0)
                                            <a href="{{route('admin.user.del',$d['id'])}}" class="btn btn-danger" title="delete user" onclick="return confirm('Are you sure?')"><i class="fa fa-trash"></i></a>
                                            @else
                                            <a class="btn btn-info" title="site admin user"><i class="fa fa-users"></i></a>
                                            @endif
                                        </td>
                                    </tr>
                                @endforeach 
                            </tbody>
                            
                          </table>
                        </div>
                        
                    </div>
                    <!-- /.box-body -->
                    
                    
                  </div>
            </div>
        </div>
       
@endsection

