@extends('layouts.admin')

@section('content')

            <div class="row">
                <div class="col-12">
                    <div class="box">
                        <div class="box-header with-border">
                          <h4 class="box-title">{{$page}}</h4>
                           
                    
                        </div>
                        <div class="box-body">
                          <form method="post" >
                             {{ csrf_field() }}
                             
                            <!-- text input -->
                            <div class="form-group">
                              <label>first name</label>
                              <input type="text" name="f_name" class="form-control" value="{{$data['f_name']}}" />
                            </div>

                            <div class="form-group">
                              <label>last name</label>
                              <input type="text" name="l_name" class="form-control" value="{{$data['l_name']}}" />
                                        
                            </div>
                            
                            <div class="form-group">
                              <label>user email</label>
                              <input type="email" name="email" class="form-control" value="{{$data['email']}}" />
                                        
                            </div>

                            <div class="form-group">
                              <label>password </label>
                              <input type="text" name="password" class="form-control" value="{{$data['password']}}" placeholder="if you wanna change it .. just leave it blank"/>
                            </div>
                            
                            <div class="form-group">
                              <label>user mobile</label>
                              <input type="text" name="mobile" class="form-control" value="{{$data['mobile']}}" />
                                        
                            </div>
                            
                            <div class="form-group">
                              <label>user country</label>
                              <input type="text" name="country" class="form-control" value="{{$data['country']}}" />
                                        
                            </div>
                            
                            <div class="form-group">
                              <label>user city</label>
                              <input type="text" name="city" class="form-control" value="{{$data['city']}}" />
                                        
                            </div>
                            
                            <div class="form-group">
                              <label>hospital</label>
                              <input type="text" name="hospital" class="form-control" value="{{$data['hospital']}}" />
                                        
                            </div>
                            
                            <div class="form-group">
                              <label>profession</label>
                              <input type="text" name="profession" class="form-control" value="{{$data['profession']}}" />
                                        
                            </div>
                            
                            <div class="form-group">
                              <label>street</label>
                              <input type="text" name="street" class="form-control" value="{{$data['street']}}" />
                                        
                            </div>
                            
                            <div class="form-group">
                              <label>user subscription</label>
                              <select class="form-control" name="type">
                                  <option value="">choose subscription</option>
                                  <option value="0" <?php if($data['type'] == 0)echo 'selected=""';?>>silver</option>
                                  <option value="1" <?php if($data['type'] == 1)echo 'selected=""';?>>gold</option>
                                  <option value="2" <?php if($data['type'] == 2)echo 'selected=""';?>>platinuim</option>
                              </select>
                                        
                            </div>
                            
                            <div class="form-group">
                              <label>facebook </label>
                              <input type="text" name="facebook" class="form-control" value=" {{$data['facebook']}} " />
                            </div>

                            <div class="form-group">
                              <label>twitter</label>
                              <input type="text" name="twitter" class="form-control" value=" {{$data['twitter']}} " />
                            </div>
                            
                            
                            <div class="box-footer">
                                <input type="submit" class="btn btn-info pull-right" value="change">
                              </div>

                          </form>
                        </div>
                       
                    </div>
                </div>
            </div>

@endsection

