@extends('layouts.admin')

@section('content')

            <div class="row">
                <div class="col-12">
                    <div class="box">
                        <div class="box-header with-border">
                          <h4 class="box-title">{{$page}}</h4>
                           
                    
                        </div>
                        <div class="box-body">
                          <form method="post" >
                             {{ csrf_field() }}
                             
                            <!-- text input -->
                            <div class="form-group">
                              <label>subscribtion name</label>
                              <input type="text" name="title" class="form-control" value="{{$data['title']}}" />
                            </div>

                            <div class="form-group">
                              <label>subscribtion description</label>
                              <textarea name="desc" class="form-control" >{{$data['desc']}}</textarea>
                                        
                            </div>

                            <div class="form-group">
                              <label>subscribtion price</label>
                              <input type="text" name="price" class="form-control" value="{{$data['price']}}" />
                            </div>

                            <div class="form-group">
                              <label>sube persentage</label>
                              <input type="text" name="course_percent" class="form-control" value="{{$data['course_percent']}}" />
                            </div>
                            
                            <div class="box-footer">
                                <input type="submit" class="btn btn-info pull-right" value="change">
                              </div>

                          </form>
                        </div>
                    </div>
                </div>
            </div>
                

@endsection

