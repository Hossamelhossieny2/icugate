@extends('layouts.admin')

@section('content')

            <div class="row">
            <div class="col-12">
                <div class="box">
                    <div class="box-header with-border">
                      <h4 class="box-title">{{$page}}</h4>
                      @if( Session::has('status') )
                      
                           <div class="alert alert-{{session('status')[1]}} alert-dismissible text-center">
                            {{ session('status')[0] }}
                          </div>
                      
                    @endif
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body">
                        <div class="table-responsive">
                          <table id="example1" class="table table-bordered table-striped text-center">
                            <thead>
                                <tr>
                                    <th>subscribtion name</th>
                                    <th>subscribtion price</th>
                                    <th>subscribtion description</th>
                                    <th>sube persentage</th>
                                    <th>action</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($data as $d)
                                  
                                    <tr class="bg-dark">
                                        <td>
                                            {{$d['title']}}
                                        </td>
                                        <td>
                                           {{$d['price']}}
                                        </td>
                                        <td>
                                           {{$d['desc']}} 
                                        </td>
                                        <td>
                                           {{$d['course_percent']}}
                                        </td>
                                        <td>
                                            <a href="{{ route('admin.pricing.edit',$d['id']) }}" class="btn btn-success"><i class="fa fa-pencil"></i></a>
                                        </td>
                                    </tr>
                                @endforeach 
                            </tbody>
                            
                          </table>
                        </div>
                        
                    </div>
                    <!-- /.box-body -->
                    
                    
                  </div>
            </div>
        </div>

@endsection

