
@extends('layouts.admin')

@section('content')
       
            <div class="row">
                
                <div class="col-12 col-lg-3">
			<div class="box box-body pull-up bg-hexagons-white">
			  <div class="media align-items-center p-0">
				<div class="text-center">
				  <a href="#"><i class="fa fa-users" title="BTC"></i></a>
				</div>
				<div>
				  <h2 class="no-margin">System Users</h2>
				</div>
			  </div>
			  <div class="flexbox align-items-center mt-25">
				<div>
				  <p class="no-margin">{{$stat['users']}} <span class="text-info">users</span></p>
				</div>
				<div class="text-right">
				  <p class="no-margin"><span class="text-success">{{$stat['users2']}}</span> need activation</p>
				</div>
			  </div>
			</div>
           </div>

		  <div class="col-lg-3 col-12">					
			<div class="box box-body pull-up bg-hexagons-white">
			  <div class="media align-items-center p-0">
				<div class="text-center">
				  <a href="#"><i class="fa fa-send" title="LTC"></i></a>
				</div>
				<div>
				  <h2 class="no-margin">System Posts</h2>
				</div>
			  </div>
			  <div class="flexbox align-items-center mt-25">
                                    
				<div>
				  <p class="no-margin">{{$stat['posts']}} <span class="text-info">posts</span></p>
				</div>
				<div class="text-right">
				  <p class="no-margin"><span class="text-success">{{$stat['posts2']}}</span> need Label</p>
				</div>
			  </div>
			</div>		
		  </div>
		 <div class="col-lg-3 col-12">			
			<div class="box box-body pull-up bg-hexagons-white">
			  <div class="media align-items-center p-0">
				<div class="text-center">
				  <a href="#"><i class="fa fa-youtube" title="NEO"></i></a>
				</div>
				<div>
				  <h2 class="no-margin">Youtube videos</h2>
				</div>
			  </div>
			  <div class="flexbox align-items-center mt-25">
                                   
				<div>
				  <p class="no-margin">{{$stat['youtube']}} <span class="text-info">videos</span></p>
				</div>
				<div class="text-right">
				  <p class="no-margin"><span class="text-success">{{$stat['youtube2']}}</span> need rechange</p>
				</div>
			  </div>
			</div>
		 </div>
		
		 <div class="col-lg-3 col-12">
			<div class="box box-body pull-up bg-hexagons-white">
			  <div class="media align-items-center p-0">
				<div class="text-center">
				  <a href="#"><i class="fa fa-link" title="DASH"></i></a>
				</div>
				<div>
				  <h2 class="no-margin">Website Visits</h2>
				</div>
			  </div>
			  <div class="flexbox align-items-center mt-20">
				<div>
				  <p class="no-margin">{{$stat['visit']}} <span class="text-info">Visit</span></p>
				</div>
<!--				<div class="text-right">
				  <p class="no-margin"><span class="text-danger">-5.35%</span></p>
				</div>-->
			  </div>
			</div>
		 </div>
                
            </div>
            <div class="row">
            	<div class="col-lg-3 col-12">					
					<div class="box box-body pull-up bg-hexagons-white">
					  <div class="media align-items-center p-0">
						<div class="text-center">
						  <a href="#"><i class="fa fa-user-md" title="LTC"></i></a>
						</div>
						<div>
						  <h2 class="no-margin">CV signups</h2>
						</div>
					  </div>
					  <div class="flexbox align-items-center mt-25">
		                                    
						<div>
						  <p class="no-margin">{{$stat['cv']}} <span class="text-info">Jobs</span></p>
						</div>
						<div class="text-right">
						  <p class="no-margin">
						  	@if($stat['cv2'] == 0)
						  		<span class="text-success">
						  			{{$stat['cv2']}}</span> new</p>
						  	@else
								<span class="text-danger">
								( {{$stat['cv2']}} )</span> NEW</p>
						  	@endif
						  	
						</div>
					  </div>
					</div>		
				  </div>
				 
				 <div class="col-lg-3 col-12">					
					<div class="box box-body pull-up bg-hexagons-white">
					  <div class="media align-items-center p-0">
						<div class="text-center">
						  <a href="#"><i class="fa fa-user-md" title="LTC"></i></a>
						</div>
						<div>
		                                    
						  <h2 class="no-margin">Courses </h2>
						</div>
					  </div>
					  <div class="flexbox align-items-center mt-25">

						<div>
						  <p class="no-margin">{{$stat['cor']}} <span class="text-info">offilne</span></p>
						</div>
						<div class="text-right">
						  <p class="no-margin">
						  		<span class="text-success">{{$stat['cor2']}}</span> online</p>
						</div>
					  </div>
					</div>		
				  </div>

				</div>
       
@endsection
