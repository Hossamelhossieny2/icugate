@extends('layouts.admin')

@section('content')

            <div class="row">
            <div class="col-12">
                <div class="box">
                    <!-- /.box-header -->
                    <div class="box-body">
                        <div class="table-responsive">
                          <table id="example1" class="table table-bordered table-striped text-center">
                            <thead>
                                <tr>
                                    <th>event id</th>
                                    <th>event title</th>
                                    <th>event pic</th>
                                    <th>event Description</th>
                                    <th>event date</th>
                                    <th>event author</th>
                                    <th>action</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($data as $d)
                                    <tr class="bg-dark">
                                        <td>
                                           {{ $d['id'] }} 
                                        </td>
                                        <td class="text-left">
                                            {{$d['title']}}
                                        </td>
                                        <td>
                                            @if(!empty($d['image']))
                                            <img src="{{ asset('events/'.$d['image']) }}" width="250"/>
                                            @else
                                            no image
                                            @endif
                                        </td>
                                        <td>
                                           {{ $d['description'] }} 
                                        </td>
                                        <td>
                                           {{ \Carbon\Carbon::parse($d['event_date'])->format('M d Y') }} 
                                        </td>
                                        <td>
                                           {{ $d['author'] }} 
                                        </td>
                                        <td>
                                            <a href="{{route('admin.event.edit',$d['id'])}}" class="btn btn-success"><i class="fa fa-pencil"></i></a>
                                            <a href="{{route('admin.event.del',$d['id'])}}" class="btn btn-danger" onclick="return confirm('Are you sure to delete this event ?')"><i class="fa fa-trash"></i></a>
                                        </td>
                                    </tr>
                                @endforeach 
                            </tbody>
                            
                          </table>
                        </div>
                        
                    </div>
                    <!-- /.box-body -->
                    
                  </div>
            </div>
        </div>

@endsection

