@extends('layouts.admin')

@section('content')

            <div class="row">
                <div class="col-12">
                    <div class="box">
                        <div class="box-header with-border">
                          <h4 class="box-title">{{$page}}</h4>
                           
                    
                        </div>
                        <div class="box-body">
                            @foreach ($errors->all() as $message)
                              <li><span class="help-block text-danger text-center">{{ $message }}</span></li>
                            @endforeach
                          <form method="post" enctype="multipart/form-data" action="{{ route('newevent.post.add') }}">
                             {{ csrf_field() }}
                             
                            <!-- text input -->
                            <div class="form-group">
                              <label>Event image</label>
                              <img src=" {{ asset('assetAdmin/images/events.jpg') }}" width="200"/><br>
                              <input type="file" id="imgFile" name="image" />
                               @if ($errors->has('image')) 
                                  <span class="help-block text-danger text-center">{{$errors->first('image')}}</span>
                              @endif
                            </div>
                                <hr>
                            <div class="form-group">
                              <label>Event title *</label>
                              <input type="text" name="event_title" class="form-control" placeholder ="input event title"  />
                              @if ($errors->has('event_title')) 
                                  <span class="help-block text-danger text-center">{{$errors->first('event_title')}}</span>
                              @endif
                            </div>
                            
                            <div class="form-group">
                              <label>Event LINK</label>
                              <input type="text" name="event_link" class="form-control" placeholder ="input event link"  />
                              @if ($errors->has('event_link')) 
                                  <span class="help-block text-danger text-center">{{$errors->first('event_link')}}</span>
                              @endif
                            </div>
                            
                            <div class="form-group">
                              <label>Event Description *</label>
                              <textarea class="form-control" name="event_desc" ></textarea>
                              @if ($errors->has('event_desc')) 
                                  <span class="help-block text-danger text-center">{{$errors->first('event_desc')}}</span>
                              @endif
                            </div>
                                
                            <div class="form-group">
                              <label>Event author</label>
                              <input type="text" name="event_by" class="form-control" placeholder ="input author name or leave blank for admin"  />
                              @if ($errors->has('event_by')) 
                                  <span class="help-block text-danger text-center">{{$errors->first('event_by')}}</span>
                              @endif
                            </div>
                            
                            <div class="form-group">
                              <label>Event Date *</label>
                              <input type="date" name="event_date" class="form-control" placeholder ="enter pick event Date" />
                               @if ($errors->has('event_date')) 
                                  <span class="help-block text-danger text-center">{{$errors->first('event_date')}}</span>
                              @endif
                            </div>
                            
                            <div class="box-footer">
                                <input type="submit" class="btn btn-info pull-right" value="Add new event">
                              </div>

                          </form>
                        </div>
                    </div>
                </div>
            </div>
                  

@endsection

