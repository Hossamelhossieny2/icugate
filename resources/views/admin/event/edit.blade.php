@extends('layouts.admin')

@section('content')

            <div class="row">
                <div class="col-12">
                    <div class="box">
                        <div class="box-header with-border">
                          <h4 class="box-title">{{$page}}</h4>
                        </div>
                        <div class="box-body">
                            @foreach ($errors->all() as $message)
                              <li><span class="help-block text-danger text-center">{{ $message }}</span></li>
                            @endforeach
                          <form method="post" enctype="multipart/form-data" action="{{ route('do.event.edit') }}">
                             {{ csrf_field() }}
                             
                            <!-- text input -->
                            <div class="form-group">
                              <label>Event image</label>
                              <img src=" {{ asset('events/'.$data['image']) }}" width="200"/><br>
                              <input type="file" id="imgFile" name="image" />
                            </div>
                                <hr>
                            <div class="form-group">
                              <label>Event title</label>
                              <textarea name="event_title" class="form-control" required="">{{ $data['title'] }}</textarea>
                            </div>
                            
                            <div class="form-group">
                              <label>Event LINK</label>
                              <textarea name="event_link" class="form-control" required="">{{ $data['link'] }}</textarea>
                            </div>
                            
                            <div class="form-group">
                              <label>Event Description</label>
                              <textarea class="form-control" name="event_desc">{{ $data['description'] }}</textarea>
                            </div>
                                
                            <div class="form-group">
                              <label>Event author</label>
                              <input type="text" name="event_by" class="form-control" value="{{ $data['author'] }}" />
                            </div>
                            
                            <div class="form-group">
                              <label>Event LINK</label>
                              <input type="date" name="event_date" class="form-control" value="{{ $data['event_date'] }}" />
                            </div>
                            
                            <input type="hidden" name="event_id" value="{{ $data['id'] }}" />
                            
                            <div class="box-footer">
                                <input type="submit" class="btn btn-info pull-right" value="change">
                              </div>

                          </form>
                        </div>
                    </div>
                </div>
            </div>

@endsection

