@extends('layouts.admin')

@section('content')

            <div class="row">
            <div class="col-12">
                <div class="box">
                    <!-- /.box-header -->
                    <div class="box-body">
                        <div class="table-responsive">
                          <table class="table table-bordered table-striped text-center">
                            <thead>
                                <tr>
                                    <th>Id</th>
                                    <th>article title</th>
                                    <th>article pic</th>
                                    <th>cat-tag</th>
                                    <th>article file</th>
                                    <th>article date</th>
                                    <th>action</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($data as $d)
                                    <tr class="bg-dark">
                                        <td>
                                            {{ $d['id'] }}
                                        </td>
                                        <td class="text-left">
                                            {{ $d['title'] }}
                                        </td>
                                        <td>
                                            <img src="{{ asset('up/'.$d['img']) }}" width="250"/>
                                        </td>
                                        <td>
                                           {{ \App\Models\Tag::find($d['tag_id'])->title }}
                                           <br>
                                           {{ \App\Models\Cat::find($d['cat_id'])->title }}
                                        </td>
                                        <td>
                                        <?php if(!empty($d['file'])){
                                            $ext = explode('.',$d['file']);
                                            $file = $ext[1];
                                        }else{
                                            $file = '<i class="fa fa-close"></i>';
                                        }?>
                                           {!! $file !!}
                                        </td>
                                        <td>
                                           {{ \Carbon\Carbon::parse($d['date'])->format('M d Y') }} 
                                        </td>
                                        <td>
                                            <a href="{{ route('admin.post.edit',$d['id']) }}" class="btn btn-success"><i class="fa fa-pencil"></i></a>
                                            <a href="{{ route('admin.post.del',$d['id']) }}" class="btn btn-danger" onclick="return confirm('Are you sure to delete this article ?')"><i class="fa fa-trash"></i></a>
                                            @if($d['active'] == 0)
                                            <a href="{{ route('admin.post.activate',$d['id']) }}" title="activate" class="btn btn-info"><i class="fa fa-recycle"></i></a>
                                            @endif
                                        </td>
                                    </tr>
                                @endforeach 
                            </tbody>
                            
                          </table>
                        </div>
                        
                        {{ $data->links() }}
                        
                    </div>
                    <!-- /.box-body -->
                    
                    
                  </div>
            </div>
        </div>

@endsection

