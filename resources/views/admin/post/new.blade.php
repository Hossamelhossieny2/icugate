@extends('layouts.admin')

@section('content')

            <div class="row">
                <div class="col-12">
                    <div class="box">
                        <div class="box-header with-border">
                          <h4 class="box-title">{{$page}}</h4>
                          
                        </div>
                        <div class="box-body">
                         
                          <form method="post" action="{{ route('post.add.new') }}" enctype="multipart/form-data">
                             {{ csrf_field() }}
                             
                            <!-- text input -->
                            <div class="form-group">
                              <label>post pic</label>
                              <div class="radio">
                              @foreach($imgs as $img)
                              <input name="img" type="radio" id="Option_{{$img['id']}}" value="{{ $img['pic'] }}" @if($img['id'] == 1) checked @endif>
                              <label for="Option_{{$img['id']}}"><img src="{{asset('up/'.$img['pic'])}}" width="100" style="margin-bottom: 50px;" class="img-bordered"/></label>                    
                              @endforeach
                              </div>
                            </div>
                                <hr>

                            <div class="row">
                            <div class="col-4">
                                  <div class="form-group">
                                    <label>Post Category</label>
                                    <select name="cat_id" class="form-control">
                                        <option value="">choose one ...</option> 
                                        @foreach($cats as $cat)
                                          @if($cat['id'] != 22 && $cat['id'] != 18 )
                                          <option value="{{$cat['id']}}" >{{$cat['title']}}</option>
                                          @endif
                                        @endforeach
                                    </select>
                                  </div>
                                  @if ($errors->has('cat_id')) 
                                      <span class="help-block text-danger text-center">{{$errors->first('cat_id')}}</span>
                                  @endif
                                </div>
                              <div class="col-4">
                                  <div class="form-group">
                                    <label>Post Tag</label>
                                    <select name="tag_id" class="form-control">
                                        <option value="">choose one ...</option> 
                                        @foreach($tags as $tag)
                                        <option value="{{ $tag['id'] }}" >{{$tag['title']}}</option>
                                        @endforeach
                                    </select>
                                  </div>
                                  @if ($errors->has('tag_id')) 
                                      <span class="help-block text-danger text-center">{{$errors->first('tag_id')}}</span>
                                  @endif
                                </div>
                                <div class="col-4">  
                                  <div class="form-group">
                                  <label>Post Type</label>
                                    <select name="post_type" class="form-control" id="post_type">
                                        <option value="0">link to other site</option> 
                                        <option value="1">file (PDF/PPT/DOC)</option> 
                                    </select>
                                  </div>
                              </div>
                          </div>

                            <div class="form-group">
                              <label>post title</label>
                              <textarea name="post_title" class="form-control" placeholder="type post title here"></textarea>
                              @if ($errors->has('post_title')) 
                                  <span class="help-block text-danger text-center">{{$errors->first('post_title')}}</span>
                              @endif
                            </div>
                            
              
                            
                            <div class="form-group" id="post_link">
                              <label>post LINK</label>
                              <textarea name="post_link" class="form-control" placeholder="copy and paste full post link"></textarea>
                              <p>* copy and paste whole link from other site</p>
                              @if ($errors->has('post_link')) 
                                  <span class="help-block text-danger text-center">{{$errors->first('post_link')}}</span>
                              @endif
                            </div>

                            <div class="form-group" id="post_file" style="display:none">
                              <label>post file</label>
                              <img src=" {{ asset('assets/images/pdf-doc.png')  }}" width="200"/><br>
                              <input type="file" name="image" class="form-control" />
                              <p>* upload PDF file Or PPT Or image file</p>
                              @if ($errors->has('post_file')) 
                                  <span class="help-block text-danger text-center">{{$errors->first('post_file')}}</span>
                              @endif
                            </div>
                            
                            <div class="form-group">
                              <label>article short description</label>
                              <input type="text" name="post_desc" class="form-control" placeholder="type brief of post description here" />
                              @if ($errors->has('post_desc')) 
                                  <span class="help-block text-danger text-center">{{$errors->first('post_desc')}}</span>
                              @endif
                            </div>
                                
                            <div class="form-group">
                              <label>journal by</label>
                              <input type="text" name="journal" class="form-control" placeholder="type user name of leave blank to choose site admin" />
                              @if ($errors->has('journal')) 
                                  <span class="help-block text-danger text-center">{{$errors->first('journal')}}</span>
                              @endif
                            </div>
                            
                            
                            <div class="row">
                              <div class="col-6">
                                <div class="form-group">
                                  <div class="checkbox">
                                      <input type="checkbox" name="famous" id="Checkbox_10" >
                                      <label for="Checkbox_10">Famous</label>
                                  </div>
                                </div>
                              </div>
                              <div class="col-6">
                                <div class="form-group">
                                    <div class="checkbox">
                                        <input type="checkbox" name="month_article" id="Checkbox_11" >
                                        <label for="Checkbox_11">article of the month</label>
                                    </div>
                                  </div>
                              </div>
                            </div>
               
                            <div class="form-group">
                              <label>Post activation</label>
                                <select name="post_active" class="form-control">
                                  <option value="1">active</option>     
                                  <option value="0">not active</option> 
                                   
                                </select>
                            </div>
                                
                            <div class="box-footer">
                                <input type="submit" class="btn btn-info pull-right" value="add new post">
                              </div>

                          </form>
                        </div>
                    </div>
                </div>
            </div>
         
   
<script>
$('select[name="post_type"]').change(function()
{
    if($(this).val() == '0')
    {
        $("#post_link").show()
        $("#post_file").hide()
    }

    else
    {
        $("#post_file").show()
        $("#post_link").hide()
    }
});
  
</script>  

                   
                

@endsection

