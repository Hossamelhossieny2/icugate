@extends('layouts.admin')

@section('content')

            <div class="row">
                <div class="col-12">
                    <div class="box">
                        <div class="box-header with-border">
                          @if(!empty($post['user_detail']))
                          <?php $this_user = $post['user_detail']['f_name'].' '.$post['user_detail']['l_name'];?>
                            <h3>this Post From User : {{ $this_user }}</h3>
                          @else
                          <?php $this_user = 'ste author' ;?>
                            <h4 class="box-title">{{$page}}</h4>  
                          @endif
                    
                        </div>
                        <div class="box-body">
                        <ul>
                            @foreach ($errors->all() as $message)
                            <li><span class="help-block text-danger text-center">{{ $message }}</span></li>
                            @endforeach
                          </ul>
                          <form method="post" enctype="multipart/form-data" action="{{ route('post.edit.do') }}">
                             {{ csrf_field() }}
                            <input type="hidden" name="post_id" value="{{$post['id']}}" />
                             
                            <!-- text input -->
                            <div class="form-group">
                              <label>post pic</label>
                              <img src="{{ asset('up/'.$post['img']) }}" width="200"/><hr>
                              <div class="radio">
                              @foreach($imgs as $img)
                              <input name="img" type="radio" id="Option_{{$img['id']}}" value="{{$img['pic']}}" <?php if($img['pic'] == $post['img']){echo 'checked';}?>>
                              <label for="Option_{{$img['id']}}"><img src="{{asset('up/'.$img['pic'])}}" width="100" style="margin-bottom: 50px;" class="img-bordered"/></label>                    
                              @endforeach
                              </div>
                            </div>
                                <hr>

                            <div class="row">
                            <div class="col-4">
                                  <div class="form-group">
                                    <label>Post Category</label>
                                    <select name="cat_id" class="form-control" required="">
                                        <option value="">choose one ...</option> 
                                        @foreach($cats as $cat)
                                        <option value="{{$cat['id']}}" <?php if($post['cat_id'] == $cat['id']) {echo 'selected=""';}?>>{{$cat['title']}}</option>
                                        @endforeach
                                    </select>
                                  </div>
                                </div>
                              <div class="col-4">
                                  <div class="form-group">
                                    <label>Post Tag</label>
                                    <select name="tag_id" class="form-control" required="">
                                        <option value="">choose one ...</option> 
                                        @foreach($tags as $tag)
                                        <option value="{{ $tag['id'] }}" <?php if($post->tag_id == $tag->id) {echo 'selected=""';}?>>{{$tag['title']}}</option>
                                        @endforeach
                                    </select>
                                  </div>
                                </div>
                                <div class="col-4">  
                                  <div class="form-group">
                                    <label>Post Type</label>
                                    <select name="post_type" class="form-control" required="" id="post_type">
                                        <option value="0" @if($post['type'] == 0) selected @endif>link to other site</option> 
                                        <option value="1" @if($post['type'] == 1) selected @endif>file (PDF/PPT/DOC)</option>
                                    </select>
                                  </div>
                              </div>
                          </div>

                            <div class="form-group">
                              <label>post title</label>
                              <textarea name="post_title" class="form-control" required="">{{$post['title']}}</textarea>
                            </div>
                            
              
                            
                            <div class="form-group" id="post_link" @if($post['type'] == 2 || $post['type'] == 3) style="display:none" @endif>
                              <label>post LINK</label>
                              <textarea name="post_link" class="form-control">{{$post['link']}}</textarea>
                              <p>* copy and paste whole link from other site</p>
                            </div>

                            <div class="form-group" id="post_file" @if($post['type'] == 0 || $post['type'] == 1) style="display:none" @endif>
                              <label>post file</label>
                              @if($post['type'] == 2)
                                <img src=" {{ asset('assets/images/pdfs.png')  }}" /> {{ $post['file'] }}<br>
                              @else
                                <img src=" {{ asset('assets/images/ppts.png')  }}" /> {{ $post['file'] }}<br>
                              @endif
                              <input type="file" name="image" class="form-control" />
                              <p>* upload PDF file Or PPT .. !! Only !!</p>
                            </div>
                            
                            <div class="form-group">
                              <label>rticle short description</label>
                              <input type="text" name="post_desc" class="form-control" value="{{ $post['text'] }}" />
                            </div>
                                
                            <div class="form-group">
                              <label>journal by</label>
                              <input type="text" name="journal" class="form-control" value="{{ $this_user }}" />
                            </div>
                            
                            
                            <div class="row">
                              <div class="col-6">
                                <div class="form-group">
                                  <div class="checkbox">
                                      <input type="checkbox" name="famous" id="Checkbox_10" <?php if($post['famous'] == 1)echo 'checked=""';?>>
                                      <label for="Checkbox_10">Famous</label>
                                  </div>
                                </div>
                              </div>
                              <div class="col-6">
                                <div class="form-group">
                                    <div class="checkbox">
                                        <input type="checkbox" name="month_article" id="Checkbox_11" <?php if($post['month_article'] == 1)echo 'checked=""';?>>
                                        <label for="Checkbox_11">article of the month</label>
                                    </div>
                                  </div>
                              </div>
                            
                            </div>
               
                            <div class="form-group">
                              <label>Post activation</label>
                                <select name="post_active" class="form-control" required="">
                                  <option value="">select active state</option>     
                                  <option value="1" @if($post['active'] == 1) selected @endif>active</option>
                                  <option value="0" @if($post['active'] == 0) selected @endif>not active</option> 
                                </select>
                            </div>
               
                                @if ($errors->has('terms'))
                                <span class="help-block text-danger text-center">{{ $errors->first('terms') }}</span>
                                @endif
                                
                            <div class="box-footer">
                                <input type="submit" class="btn btn-info pull-right" value="change">
                              </div>

                          </form>
                        </div>
                    </div>
                </div>
            </div>
         

<script>
$('select[name="post_type"]').change(function()
{
    if($(this).val() == '0')
    {
        $("#post_link").show()
        $("#post_file").hide()
    }

    else
    {
        $("#post_file").show()
        $("#post_link").hide()
    }
});
  
</script>           
                

@endsection

