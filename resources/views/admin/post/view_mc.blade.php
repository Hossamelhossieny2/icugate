@extends('layouts.admin')

@section('content')

            <div class="row">
            <div class="col-12">
                <div class="box">
                    <!-- /.box-header -->
                    <div class="box-body">
                        <div class="table-responsive">
                          <table id="example1" class="table table-bordered table-striped text-center">
                            <thead>
                                <tr>
                                    <th>user details</th>
                                    <th>post title</th>
                                    <th>post pic</th>
                                    <th>cat/tag</th>
                                    <th>post date</th>
                                    <th>action</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($data as $d)
                                    <tr class="bg-dark">
                                        <td class="text-left">
                                            {{ $d['user_detail']['f_name'].' '.$d['user_detail']['l_name'] }}
                                            <br>
                                            {{ $d['user_detail']['email'] }}
                                            <br>
                                            {{ $d['user_detail']['profession'] }}
                                            <br>
                                            {{ $d['user_detail']['hospital'] }}
                                        </td>
                                        <td class="text-left">
                                            {{ $d['title'] }}
                                        </td>
                                        <td>
                                            <img src="{{ asset('up/'.$d['img']) }}" width="250"/>
                                        </td>
                                        <td>
                                           {{ \App\Models\Tag::find($d['tag_id'])->title }}
                                           <br>
                                           {{ \App\Models\Cat::find($d['cat_id'])->title }}
                                        </td>
                                        <td>
                                           {{$d['date']}} 
                                        </td>
                                        <td>
                                            <a href="{{ route('admin.post.edit.mc',$d['id']) }}" class="btn btn-success"><i class="fa fa-pencil"></i></a>
                                            <a href="{{ route('admin.post.del',$d['id']) }}" class="btn btn-danger" onclick="return confirm('Are you sure to delete this article ?')"><i class="fa fa-trash"></i></a>
                                            @if($d['active'] == 0)
                                            <a href="{{ route('admin.post.activate',$d['id']) }}" title="activate" class="btn btn-info"><i class="fa fa-recycle"></i></a>
                                            @endif
                                        </td>
                                    </tr>
                                @endforeach 
                            </tbody>
                            
                          </table>
                        </div>
                        
                        

                    </div>
                    <!-- /.box-body -->
                    
                    
                  </div>
            </div>
        </div>

@endsection

