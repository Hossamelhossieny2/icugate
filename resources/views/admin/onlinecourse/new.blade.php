@extends('layouts.admin')

@section('content')

            <div class="row">
                <div class="col-12">
                    <div class="box">
                        <div class="box-header with-border">
                          <h4 class="box-title">{{$page}}</h4>
                        </div>
                        <div class="box-body">
                          <form method="post"   enctype="multipart/form-data">
                             {{ csrf_field() }}
                             
                            <!-- text input -->
                            <div class="form-group">
                              <label>course title</label>
                              <input type="text" name="title" class="form-control" required="" />
                            </div>

                            <div class="form-group">
                              <label>course description</label>
                              <textarea name="description" class="form-control"required="" ></textarea>
                            </div>
                            
                            <div class="form-group">
                                <label for="imgFile">course pic</label>
                              <input type="file" id="imgFile" name="image" required=""/>
                            </div>

                            <div class="form-group">
                              <label>course price </label>
                              <input type="text" name="price" class="form-control" required="" />
                            </div>
                            
                            <div class="form-group">
                              <label>course start</label>
                              <input type="date" name="from" class="form-control" min="{{date('Y-m-d')}}" required="" />
                                        
                            </div>
                            
                            <div class="form-group">
                              <label>course end</label>
                              <input type="date" name="to" class="form-control" min="{{date('Y-m-d')}}" required="" />
                                        
                            </div>
                            
                            <div class="form-group">
                              <label>Exam time</label>
                              <input type="date" name="exam_time" class="form-control" required="" />
                                        
                            </div>

                            <div class="form-group">
                              <label>hours to end</label>
                              <input type="text" name="exam_limit" class="form-control" required="" />
                                        
                            </div>
                           
                            <input type="hidden" name="type" value="3">
<!--                            <div class="form-group">
                              <label>quiz question count</label>
                              <input type="text" name="quiz_count" class="form-control" required="" />
                                        
                            </div>
                            
                            <div class="form-group">
                              <label>Exam question count</label>
                              <input type="text" name="exam_count" class="form-control" required="" />
                                        
                            </div>-->
                            
                            <div class="box-footer">
                                <input type="submit" class="btn btn-info pull-right" value="add">
                              </div>

                          </form>
                        </div>
                        
                    </div>
                </div>
            </div>
                

@endsection

