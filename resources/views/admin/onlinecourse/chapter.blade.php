@extends('layouts.admin')

@section('content')


            <div class="row">
                
                <div class="col-12">
                    
                    <div class="box">
                        
                        <div class="box-body">
                            
                            
                          <form method="post">
                             {{ csrf_field() }}
                             
                            <!-- text input -->
                            
                            <div class="form-group">
                              <label>chapter title</label>
                              <textarea name="title" class="form-control" required=""></textarea>
                            </div>
                            
                            
                            <div class="box-footer">
                                <input type="submit" class="btn btn-info pull-right" value="add">
                              </div>

                          </form>
                        </div>
                        
                        <div class="box-body">
                        <div class="table-responsive">
                          <table id="example1" class="table table-bordered table-striped text-center">
                            <thead>
                                <tr>
                                    <th>Chapter</th>
                                    <th>ques count</th>
                                    <th>action</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($data as $d)
                                  
                                    <tr class="bg-dark">
                                       
                                        <td>
                                           {{$d['title']}}
                                        </td>
                                        <td>
                                          @if(!empty($d['ques']))
                                            {{ count($d['ques']) }}
                                          @else
                                            0
                                          @endif
                                        </td>
                                        <td>
                                          
                                          <a href="{{route('online.course.ques',$d['id'])}}" class="btn btn-primary"  title="add ques"><i class="fa fa-question-circle-o"></i></a>
                                          <a href="{{route('online.course.chapter.delete',$d['id'])}}" class="btn btn-danger"  title="delete chapter"><i class="fa fa-trash"></i></a>
                                            
                                        </td>
                                    </tr>
                                @endforeach 
                            </tbody>
                            
                          </table>
                        </div>
                        
                    </div>
                    </div>
                </div>
            </div>

@endsection

