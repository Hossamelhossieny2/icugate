@extends('layouts.admin')

@section('content')

            <div class="row">
                
                <div class="col-12">
                    
                    <div class="box">
                        <div class="box-header with-border">
                          <h4 class="box-title">{{$page}} for Course : {{$course['title']}}</h4>
                            <a class="btn btn-info pull-right" href="{{route('onine.course.chapter',$course_id)}}"><i class="ti-arrow-left"></i> Back to OnlineCourses chapters</a>
                            @if( Session::has('status') )
                      
                                   <div class="alert alert-{{session('status')[1]}} alert-dismissible text-center">
                                    {{ session('status')[0] }}
                                  </div>
                              
                            @endif
                        </div>
                        
                        <div class="box-body">
                            
                            
                          <form method="post">
                             {{ csrf_field() }}
                             
                            <!-- text input -->
                            <div class="form-group">
                              <label>Chapter</label>
                              <select name="ch" class="form-control" required="">
                                  @foreach($chapters as $ch)
                                        <option value="{{$ch['id']}}" <?php if($ch['id'] == $id)echo 'selected="selected"';?>>{{$ch['title']}}</option>
                                  @endforeach
                              </select>
                            </div>
                           
                            <div class="form-group">
                              <label>ques title</label>
                              <textarea name="ques" class="form-control" required=""></textarea>
                            </div>
                            
                            <div class="form-group">
                              <label>right answer</label>
                              <textarea name="ans_1" class="form-control" required=""></textarea>
                            </div>
                            
                            <div class="form-group">
                              <label>false answer 1</label>
                              <textarea name="ans_2" class="form-control" required=""></textarea>
                            </div>
                            
                            <div class="form-group">
                              <label>false answer 2</label>
                              <textarea name="ans_3" class="form-control" required=""></textarea>
                            </div>
                            
                            <div class="form-group">
                              <label>false answer 3</label>
                              <textarea name="ans_4" class="form-control" required=""></textarea>
                            </div>
                            
                                <input type="hidden" name="id" value="{{$course['id']}}" />
                            
                            <div class="box-footer">
                                <input type="submit" class="btn btn-info pull-right" value="add">
                              </div>

                          </form>
                        </div>
                        
                        <div class="box-body">
                        <div class="table-responsive">
                          <table id="example1" class="table table-bordered table-striped text-center">
                            <thead>
                                <tr>
                                    <th>ques chapter</th>
                                    <th>question</th>
                                    
                                    <th>action</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($data as $d)
                                  
                                    <tr class="bg-dark">
                                        <td>
                                           {{\App\Models\CourseChapter::find($d['chapter'])->title}}
                                        </td>
                                        <td>
                                           {{$d['text']}}
                                        </td>
                                        
                                        <td>
                                              <a href="#" class="btn btn-info">edit ques</a>
                                            <a href="#" class="btn btn-success">edit ans</a>
                                        </td>
                                    </tr>
                                @endforeach 
                            </tbody>
                            
                          </table>
                        </div>
                        
                    </div>
                    </div>
                </div>
            </div>

@endsection

