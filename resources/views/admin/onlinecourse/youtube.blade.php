@extends('layouts.admin')

@section('content')

            <div class="row">
                <div class="col-12">
                    <div class="box">
                        <div class="box-header with-border">
                          <h4 class="box-title">{{$page}} for course : {{$course['title']}}</h4>
                          <a class="btn btn-info pull-right" href="{{route('online.course')}}"><i class="ti-arrow-left"></i> Back to OnlineCourses</a>
                        </div>
                        
                        <div class="box-body">
                            
                            
                          <form method="post" enctype="multipart/form-data">
                             {{ csrf_field() }}
                             
                             <div class="form-group">
                              <label>Chapter</label>
                              <select name="chapter" class="form-control" required="">
                                  @foreach($chapters as $ch)
                                        <option value="{{$ch['id']}}">{{$ch['title']}}</option>
                                  @endforeach
                              </select>
                            </div>

                            <!-- text input -->
                            <div class="form-group">
                              <label>video title</label>
                              <input type="text" class="form-control" name="video_title" required="" />
                            </div>
                            
                             <div class="form-group">
                              <label>video description</label>
                              <textarea name="video_des" class="form-control" ></textarea>
                            </div>

                            <div class="form-group">
                              <label>video id</label>
                              <input type="text" class="form-control" name="video_id" required=""/>
                              <small>inset video id only after ?v= .... link</small>
                            </div>

                            <input type="hidden" name="course_id" value="{{$id}}" />
                            
                            <div class="box-footer">
                                <input type="submit" class="btn btn-info pull-right" value="add">
                              </div>

                          </form>
                        </div>
                        
                        <div class="box-body">
                        <div class="table-responsive">
                          <table id="example1" class="table table-bordered table-striped text-center">
                            <thead>
                                <tr>
                                    <th>chapter</th>
                                    <th>vedio title</th>
                                    <th>vedio desc</th>
                                    <th>action</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($data as $d)
                                  
                                    <tr class="bg-dark">
                                        <td>
                                           Chapter {{$d['chapter']}}
                                        </td>
                                        <td>
                                           {{$d['video_title']}}
                                        </td>
                                        <td>
                                           {{$d['video_des']}}
                                        </td>
                                        <td>
                                          <img class="img-fullwidth" src="https://i.ytimg.com/vi/{{$d->video_id}}/hqdefault.jpg" width="150" alt="{{$d['video_title']}}">

                                        </td>
                                        <td>
                                            <a href="{{route('online.course.delyoutube',$d['id'])}}" class="btn btn-danger" onclick="return confirm('Are you sure?')"><i class="fa fa-trash"></i></a>
                                        </td>
                                    </tr>
                                @endforeach 
                            </tbody>
                            
                          </table>
                        </div>
                        
                    </div>
                    </div>
                </div>
            </div>

@endsection

