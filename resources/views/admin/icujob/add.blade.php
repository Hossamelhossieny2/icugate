@extends('layouts.admin')

@section('content')

            <div class="row">
                <div class="col-12">
                    <div class="box">
                        <div class="box-header with-border">
                          <h4 class="box-title">{{$page}}</h4>
                           
                    
                        </div>
                        <div class="box-body">
                          <form method="post" action="{{ route('admin.job.post') }}">
                             {{ csrf_field() }}
                             
                            <!-- text input -->
                            <div class="form-group">
                              <label>job title</label>
                              <input type="text" name="job_title" class="form-control" required=""/>
                            </div>

                            <div class="form-group">
                              <label>job description</label>
                              <textarea name="job_desc" class="form-control" rows="3"></textarea>
                            </div>

                            <div class="form-group">
                              <label>job salary</label>
                              <input type="text" name="salary" class="form-control" required=""/>
                            </div>

                            <div class="form-group">
                              <label>job specialization</label>
                              <input type="text" name="specialization" class="form-control" required=""/>
                            </div>

                            <div class="form-group">
                              <label>job Qualifications required</label>
                              <input type="text" name="Qualifications" class="form-control" required=""/>
                            </div>

                            <div class="form-group">
                              <label>job gender</label>
                              <select name="gender" class="form-control" required="">
                                <option value="male">male needed</option>
                                <option value="female">female needed</option>
                              </select>
                            </div>

                            <input type="hidden" name="job_id" value="0" />

                            <div class="box-footer">
                                <input type="submit" class="btn btn-info pull-right" value="add">
                              </div>

                          </form>
                        </div>
                        
                    </div>
                </div>
            </div>

@endsection

