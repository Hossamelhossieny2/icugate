@extends('layouts.admin')

@section('content')

            <div class="row">
            <div class="col-12">
                <div class="box">
                    <div class="box-header with-border">
                      <h4 class="box-title">{{$page}}</h4>
                      <a class="btn btn-success pull-right" href="{{route('admin.newicujob')}}">add job</a>
                      @if( Session::has('status') )
                      
                           <div class="alert alert-{{session('status')[1]}} alert-dismissible text-center">
                            {{ session('status')[0] }}
                          </div>
                      
                    @endif
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body">
                        <div class="table-responsive">
                          <table id="example1" class="table table-bordered table-striped text-center">
                            <thead>
                                <tr>
                                    <th>job title</th>
                                    <th>job description</th>
                                    <th>add time</th>
                                    <th>action</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($data as $d)
                                  
                                    <tr class="bg-dark">
                                        <td>
                                            {{$d['job_title']}}
                                        </td>
                                        <td>
                                           {{$d['job_desc']}}
                                        </td>
                                        <td>
                                           {{$d['created_at']}} 
                                        </td>
                                        <td>
                                            <a href="{{route('admin.icujob.edit',$d['id'])}}" class="btn btn-success"><i class="fa fa-pencil"></i></a>
                                            @if($d['active'] == 1)
                                            <a href="{{route('admin.icujob.active',$d['id'])}}" title="de-activate" class="btn btn-warning"><i class="fa fa-close"></i></a>
                                            @else
                                            <a href="{{route('admin.icujob.active',$d['id'])}}" title="re-activate" class="btn btn-success"><i class="fa fa-check"></i></a>
                                            @endif
                                            <a href="{{route('admin.icujob.del',$d['id'])}}" class="btn btn-danger" title="delete" onclick="return confirm('Are you sure?')"><i class="fa fa-trash"></i></a>
                                        </td>
                                    </tr>
                                @endforeach 
                            </tbody>
                            
                          </table>
                        </div>
                        
                    </div>
                    <!-- /.box-body -->
                  </div>
            </div>
        </div>

@endsection

