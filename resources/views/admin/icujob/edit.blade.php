@extends('layouts.admin')

@section('content')

            <div class="row">
                <div class="col-12">
                    <div class="box">
                        <div class="box-header with-border">
                          <h4 class="box-title">{{$page}}</h4>
                           <a class="btn btn-success pull-right" href="{{route('admin.newicujob')}}">add job</a>
                    
                        </div>
                        <div class="box-body">
                          <form method="post" action="{{ route('admin.job.post') }}">
                             {{ csrf_field() }}
                             
                            <!-- text input -->
                            <div class="form-group">
                              <label>job title</label>
                              <input type="text" name="job_title" class="form-control" value="{{$data['job_title']}}" required=""/>
                            </div>

                            <div class="form-group">
                              <div class="form-group">
                              <label>job description</label>
                              <textarea name="job_desc" class="form-control" rows="3">{{$data['job_desc']}}</textarea>
                            </div>
                            </div>

                            <div class="form-group">
                              <label>job salary</label>
                              <input type="text" name="salary" class="form-control" value="{{$data['salary']}}" required=""/>
                            </div>

                            <div class="form-group">
                              <label>job specialization</label>
                              <input type="text" name="specialization" class="form-control" value="{{$data['specialization']}}" required=""/>
                            </div>

                            <div class="form-group">
                              <label>job Qualifications required</label>
                              <input type="text" name="Qualifications" class="form-control" value="{{$data['Qualifications']}}" required=""/>
                            </div>

                            <div class="form-group">
                              <label>job gender</label>
                              <select name="gender" class="form-control" required="">
                                <option value="male" @if($data['gender'] == 'male') selected @endif>male needed</option>
                                <option value="female" @if($data['gender'] == 'fmale') selected @endif>female needed</option>
                              </select>
                            </div>

                            <input type="hidden" name="job_id" value="{{$data['id']}}" />
                            
                            <div class="box-footer">
                                <input type="submit" class="btn btn-info pull-right" value="change">
                              </div>

                          </form>
                        </div>
                        
                    </div>
                </div>
            </div>
       
@endsection

