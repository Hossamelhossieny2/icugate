@extends('layouts.admin')

@section('content')

            <div class="row">
                
                <div class="col-12">
                    
                    <div class="box">
                        <div class="box-header with-border">
                          <h4 class="box-title">{{$page}} for course : {{$course->title}}</h4>
                            <a class="btn btn-info pull-right" href="{{route('admin.course')}}"><i class="ti-arrow-left"></i> Back to Courses</a>
                        </div>
                        
                        <div class="box-body">
                            
                            
                          <form method="post">
                             {{ csrf_field() }}
                             
                            <!-- text input -->
                            <div class="form-group">
                              <label>Ques Type</label>
                              <div class="radio">
                                <input name="type" type="radio" id="Option_1" value="0" required="" <?php if(!empty($ques)){if($ques['type'] == 0)echo 'checked=""';}?>/>
                                <label for="Option_1">Quiz</label>   
                                
                                <input name="type" type="radio" id="Option_2" value="1" required="" <?php if(!empty($ques)){if($ques['type'] == 1)echo 'checked=""';}?>/>
                                <label for="Option_2">Exam</label> 
                              </div>
                            </div>
                           
                            <div class="form-group">
                              <label>ques title</label>
                              <textarea name="ques" class="form-control" required=""><?php if(!empty($ques)){echo $ques['text'];}?></textarea>
                            </div>
                            
                            <div class="form-group">
                              <label>right answer</label>
                              <textarea name="ans_1" class="form-control" required=""><?php if(!empty($ans)){echo $ans[0]->text;}?></textarea>
                            </div>
                            
                            <div class="form-group">
                              <label>false answer 1</label>
                              <textarea name="ans_2" class="form-control" required=""><?php if(!empty($ans)){echo $ans[1]->text;}?></textarea>
                            </div>
                            
                            <div class="form-group">
                              <label>false answer 2</label>
                              <textarea name="ans_3" class="form-control" required=""><?php if(!empty($ans)){echo $ans[2]->text;}?></textarea>
                            </div>
                            
                            <div class="form-group">
                              <label>false answer 3</label>
                              <textarea name="ans_4" class="form-control" required=""><?php if(!empty($ans)){echo $ans[3]->text;}?></textarea>
                            </div>
                            
                                <input type="hidden" name="id" value="{{$id}}" />
                                <input type="hidden" name="qu_id" value="{{$xques}}" />
                            
                            <div class="box-footer">
                                <input type="submit" class="btn btn-info pull-right" value="add">
                              </div>

                          </form>
                        </div>
                        
                        <div class="box-body">
                        <div class="table-responsive">
                          <table id="example1" class="table table-bordered table-striped text-center">
                            <thead>
                                <tr>
                                    <th>ques type</th>
                                    <th>question</th>
                                    <th>action</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($data as $d)
                                  
                                    <tr class="bg-dark">
                                        <td>
                                           <?php
                                           if($d['type'] == 0){
                                               $type = 'quiz';
                                           }elseif($d['type'] == 1){
                                               $type = 'exam';
                                           }
                                           echo $type ;
                                           ?>
                                        </td>
                                        <td>
                                           {{$d['text']}}
                                        </td>
                                        
                                        <td>
                                            <a href="{{route('admin.course.ques',$id,$d['id'])}}" class="btn btn-info">edit ques/ans</a>
                                        </td>
                                    </tr>
                                @endforeach 
                            </tbody>
                            
                          </table>
                        </div>
                        
                    </div>
                    </div>
                </div>
            </div>
        
@endsection

