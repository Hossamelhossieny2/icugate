@extends('layouts.admin')

@section('content')

<?php $a=1;$a1=1;$total=0;$total1=0;?>

            <div class="row">
            <div class="col-12">
                <div class="box">
                    
                    <!-- /.box-header -->
                    <div class="box-body">
                        <div class="table-responsive">
                          <table id="example1" class="table table-bordered table-striped text-center">
                            <thead>
                                <tr>
                                    <th>name</th>
                                    <th>user email</th>
                                    <th>user mobile</th>
                                    <th>profession</th>
                                    <th>amount</th>
                                    <th>Paid</th>
                                    <th>tag</th>
                                    <th>for</th>
                                    <th>Time</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($data as $d)
                                  
                                    <tr class="bg-dark">
                                        <td>
                                            {{$d['f_name'].' '.$d['l_name']}}
                                        </td>
                                        <td>
                                           {{$d['email']}} 
                                        </td>
                                        <td>
                                           {{$d['mobile']}}
                                        </td>
                                        <td>
                                           {{$d['profession']}}
                                        </td>
                                        <td>
                                           {{$d['paid']}}
                                           <?php $total=$total+$d->paid;?>
                                        </td>
                                        <td>
                                           {{$d['paid']}}
                                        </td>
                                        <td>
                                            <a class="btn btn-success">course</a>
                                        </td>
                                        <td>
                                           {{$d['title']}}
                                        </td>
                                        <td>
                                           {{$d['date']}}
                                        </td>
                                        
                                    </tr>
                                @endforeach 
                            </tbody>
                            <tfoot>
                                  <th colspan="4">TOTAL</th>
                                  <th>{{$total}}</th>
                                  <th colspan="4"></th>
                                </tfoot>
                          </table>
                        </div>
                        
                    </div>
                    <!-- /.box-body -->
                    
                    
                  </div>
            </div>
        </div>

@endsection

