@extends('layouts.admin')

@section('content')

            <div class="row">
                
                <div class="col-12">
                    
                    <div class="box">
                        <div class="box-header with-border">
                          <h4 class="box-title">{{$page}} for course : {{$course['title']}}</h4>
                          @if($course['type'] != 3)
                          <a class="btn btn-info pull-right" href="{{route('admin.course')}}"><i class="ti-arrow-left"></i> Back to Courses</a>
                          @else
                          <a class="btn btn-info pull-right" href="{{route('online.course')}}"><i class="ti-arrow-left"></i> Back to OnlineCourses</a>
                          @endif
                        </div>
                        
                        <div class="box-body">
                            
                            
                          <form method="post">
                             {{ csrf_field() }}
                             
                            <!-- text input -->
                            <div class="form-group">
                              <label>outline title</label>
                              <textarea name="text" class="form-control" required=""></textarea>
                            </div>
                            
                            <input type="hidden" name="tut_id" value="{{$id}}" />
                            
                            <div class="box-footer">
                                <input type="submit" class="btn btn-info pull-right" value="add">
                              </div>

                          </form>
                        </div>
                        
                        <div class="box-body">
                        <div class="table-responsive">
                          <table id="example1" class="table table-bordered table-striped text-center">
                            <thead>
                                <tr>
                                    <th>outline title</th>
                                    <th>action</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($data as $d)
                                  
                                    <tr class="bg-dark">
                                        <td>
                                           {{$d['text']}}
                                        </td>
                                        
                                        <td>
                                            <a href="{{route('admin.course.delout',$d['id'])}}" class="btn btn-danger" onclick="return confirm('Are you sure?')"><i class="fa fa-trash"></i></a>
                                        </td>
                                    </tr>
                                @endforeach 
                            </tbody>
                            
                          </table>
                        </div>
                        
                    </div>
                    </div>
                </div>
            </div>

@endsection

