@extends('layouts.admin')

@section('content')

            <div class="row">
                <div class="col-12">
                    
                    <div class="box">
                        <div class="box-header with-border">
                          <h4 class="box-title">{{$page}} for course : {{$course['title']}}</h4>
                           @if($course['type'] != 3)
                          <a class="btn btn-info pull-right" href="{{route('admin.course')}}"><i class="ti-arrow-left"></i> Back to Courses</a>
                          @else
                          <a class="btn btn-info pull-right" href="{{route('online.course')}}"><i class="ti-arrow-left"></i> Back to OnlineCourses</a>
                          @endif
                        </div>
                        
                        <div class="box-body">
                            
                            
                          <form method="post" enctype="multipart/form-data">
                             {{ csrf_field() }}
                             
                            <!-- text input -->
                            <div class="form-group">
                              <label>speaker name</label>
                              <textarea name="name" class="form-control" required=""></textarea>
                            </div>

                            <div class="form-group">
                              <label>speaker title</label>
                              <textarea name="title" class="form-control" required=""></textarea>
                            </div>
                            
                            <div class="form-group">
                              <label>speaker image</label>
                              <input type="file" id="imgFile" name="image" required=""/>
                              <help class="text-danger">Best Display  ( width : 500 ----- hieght : 250 )</help>
                            </div>
                            
                                <input type="hidden" name="course_id" value="{{$id}}" />
                            
                            <div class="box-footer">
                                <input type="submit" class="btn btn-info pull-right" value="add">
                              </div>

                          </form>
                        </div>
                        
                        <div class="box-body">
                        <div class="table-responsive">
                          <table id="example1" class="table table-bordered table-striped text-center">
                            <thead>
                                <tr>
                                    <th>speaker name</th>
                                    <th>speaker title</th>
                                    <th>doc image</th>
                                    <th>action</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($data as $d)
                                  
                                    <tr class="bg-dark">
                                        <td>
                                           {{$d['name']}}
                                        </td>
                                         <td>
                                           {{$d['title']}}
                                        </td>
                                        <td>
                                           <image src="{{ asset('course/'.$d['pic']) }}" width="100"/>
                                        </td>
                                        
                                        <td>
                                            
                                            <a href="{{route('admin.post.speak',$d['id'])}}" class="btn btn-danger" onclick="return confirm('Are you sure?')"><i class="fa fa-trash"></i></a>
                                        </td>
                                    </tr>
                                @endforeach 
                            </tbody>
                            
                          </table>
                        </div>
                        
                    </div>
                    </div>
                </div>
            </div>
                

@endsection

