@extends('layouts.admin')

@section('content')

            <div class="row">
                
                <div class="col-12">
                    
                    <div class="box">
                        <div class="box-header with-border">
                          <h4 class="box-title">{{$page}} for course : {{$course['title']}}</h4>
                          <a class="btn btn-info pull-right" href="{{route('admin.course')}}"><i class="ti-arrow-left"></i> Back to Courses</a>
                        </div>
                        
                        <div class="box-body">
                            
                            
                          <form method="post" enctype="multipart/form-data">
                             {{ csrf_field() }}
                             
                            <!-- text input -->
                            <div class="form-group">
                              <label>document title</label>
                              <textarea name="title" class="form-control" required=""></textarea>
                            </div>
                            
                            <div class="form-group">
                              <label>document file</label>
                              <input type="file" id="imgFile" name="image" required=""/>
                            </div>
                            
                                <input type="hidden" name="course_id" value="{{$id}}" />
                            
                            <div class="box-footer">
                                <input type="submit" class="btn btn-info pull-right" value="add">
                              </div>

                          </form>
                        </div>
                        
                        <div class="box-body">
                        <div class="table-responsive">
                          <table id="example1" class="table table-bordered table-striped text-center">
                            <thead>
                                <tr>
                                    <th>document title</th>
                                    <th>doc file</th>
                                    <th>action</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($data as $d)
                                  
                                    <tr class="bg-dark">
                                        <td>
                                           {{$d['title']}}
                                        </td>
                                        <td>
                                           {{$d['file']}}
                                        </td>
                                        
                                        <td>
                                            <a target="_BLANK" href="{{ asset('course/'.$d['file']) }}" class="btn btn-success"><i class="fa fa-eye"></i></a>
                                            <a href="{{route('admin.course.deldocs',$d['id'])}}" class="btn btn-danger" onclick="return confirm('Are you sure?')"><i class="fa fa-trash"></i></a>
                                        </td>
                                    </tr>
                                @endforeach 
                            </tbody>
                            
                          </table>
                        </div>
                        
                    </div>
                    </div>
                </div>
            </div>
              

@endsection

