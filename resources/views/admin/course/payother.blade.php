@extends('layouts.admin')

@section('content')

<?php $a=1;$a1=1;$total=0;$total1=0;?>

            <div class="row">
            <div class="col-12">
                <div class="box">
                    
                    <!-- /.box-header -->
                    <div class="box-body">
                      <form method="get">
                      <div class="row">
                        <div class="col-md-2">
                          <label>from</label>
                          <input name="from" type="date" class="form-control" placeholder="from" />
                        </div>
                        <div class="col-md-2">
                          <label>to</label>
                          <input name="to" type="date" class="form-control" placeholder="to" />
                        </div>
                        <div class="col-md-2">
                          <label>User email</label>
                          <input name="email" type="email" class="form-control" placeholder="type user email" />
                        </div>
                        <div class="col-md-2">
                          <label>course</label>
                          <select name="course" class="form-control">
                            <option value="">any course</option>
                            @foreach($courses as $course)
                              <option value="{{$course['id']}}">{{$course['title']}}</option>
                            @endforeach
                          </select>
                        </div>
                        <div class="col-md-2">
                          <label>payment</label>
                          <select name="payment" class="form-control">
                            <option value="">payment type</option>
                            <option value="1">paid</option>
                            <option value="2">unpaid</option>
                          </select>
                        </div>
                        <div class="col-md-2">
                          <label>search</label>
                          <input type="submit" class="form-control btn btn-warning" value="search now" />
                        </div>
                      </div>
                      </form>
                      <hr>
                        <div class="table-responsive">
                          <table id="example1" class="table table-bordered table-striped text-center">
                            <thead>
                                <tr>
                                    <th>name</th>
                                    <th>user email</th>
                                    <th>user mobile</th>
                                    <th>profession</th>
                                    <th>amount</th>
                                    <th>for</th>
                                    <th>Time</th>
                                    <th>quiz</th>
                                    <th>exam</th>
                                    <th>status</th>
                                    <th>action</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($data as $d)
                                  
                                    <tr class="bg-dark">
                                        <td>
                                            {{$d['f_name'].' '.$d['l_name']}}
                                        </td>
                                        <td>
                                           {{$d['email']}} 
                                        </td>
                                        <td>
                                           {{$d['mobile']}}
                                        </td>
                                        <td>
                                           {{$d['profession']}}
                                        </td>
                                        <td>
                                           {{$d['price']}}
                                           <?php $total =  $total+$d->price;?>
                                        </td>
                                        <td>
                                           {{$d['title']}}
                                        </td>
                                        <td>
                                           {{$d['date']}}
                                        </td>
                                        <td>
                                           {{$d['quiz']}}
                                        </td>
                                        <td>
                                           {{$d['exam']}}
                                        </td>
                                        <td>
                                          @if($d['payed'] ==1)
                                            <span class="badge badge-success">paid</span>
                                          @else
                                            <span class="badge badge-danger">pending</span>
                                          @endif
                                        </td>
                                        <td>
                                           @if($d['payed'] ==1)
                                            <span class="badge">done</span>
                                          @else
                                            <a href="{{route('admin.payother.pay',$d['pay_id'])}}" class="btn btn-success" title="manual payment" onclick="return confirm('Are you sure?')"><i class="fa fa-money"></i></a>
                                          @endif
                                         
                                        </td>
                                    </tr>
                                @endforeach 
                            </tbody>
                            <tfoot>
                                  <th colspan="4">TOTAL</th>
                                  <th>{{ number_format($total,2) }}</th>
                                  <th colspan="4"></th>
                                </tfoot>
                          </table>
                        </div>
                        
                    </div>
                    <!-- /.box-body -->
                    
                    
                  </div>
            </div>
        </div>
       
@endsection

