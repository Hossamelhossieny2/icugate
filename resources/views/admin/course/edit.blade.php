@extends('layouts.admin')

@section('content')

            <div class="row">
                <div class="col-12">
                    <div class="box">
                        <div class="box-body">
                          <form method="post" enctype="multipart/form-data">
                             {{ csrf_field() }}
                             
                            <!-- text input -->
                            <div class="form-group">
                              <label>course title</label>
                              <input type="text" name="title" class="form-control" value="{{$data['title']}}" />
                            </div>

                            <div class="form-group">
                              <label>course description</label>
                              <textarea name="description" class="form-control" >{{$data['description']}}</textarea>
                            </div>
                            
                            <div class="form-group">
                              <label>course main image</label>
                              <img src=" {{url('course/'.$data['img'])}}" width="200"/><br>
                              <input type="file" id="imgFile" name="image" />
                            </div>

                            <div class="form-group">
                              <label>course price </label>
                              <input type="text" name="price" class="form-control" value="{{$data['price']}}" />
                            </div>
                            
                            <div class="form-group">
                              <label>course start</label>
                              <input type="date" name="from" class="form-control" value="{{$data['from']}}" />
                                        
                            </div>
                            
                            <div class="form-group">
                              <label>course end</label>
                              <input type="date" name="to" class="form-control" value="{{$data['to']}}" />
                                        
                            </div>

                            <div class="form-group">
                              <label>Exam Time</label>
                              <input type="datetime" name="exam_time" class="form-control" value="{{$data['exam_time']}}" />
                                        
                            </div>

                            <div class="form-group">
                              <label>hours to End</label>
                              <input type="text" name="exam_limit" class="form-control" value="{{$data['exam_limit']}}" />
                                        
                            </div>
                            
                            <div class="form-group">
                              <label>course section</label>
                              <select class="form-control" name="type">
                                  <option value="">choose type</option>
                                  <option value="2" <?php if($data->type == 2)echo 'selected=""';?>>Critical Care Ultrasound</option>
                                  <option value="1" <?php if($data->type == 1)echo 'selected=""';?>>Critical Care Educational</option>
                                  <!-- <option value="3" <?php if($data->type == 3)echo 'selected=""';?>>Online Course</option> -->
                              </select>
                                        
                            </div>
                            
<!--                            <div class="form-group">
                              <label>quiz question count</label>
                              <input type="text" name="quiz_count" class="form-control" value="{{$data['quiz_count']}}" />
                                        
                            </div>
                            
                            <div class="form-group">
                              <label>Exam question count</label>
                              <input type="text" name="exam_count" class="form-control" value="{{$data['exam_count']}}" />
                                        
                            </div>-->
                            
                            <div class="box-footer">
                                <input type="submit" class="btn btn-info pull-right" value="change">
                              </div>

                          </form>
                        </div>
                    </div>
                </div>
            </div>
       
@endsection

