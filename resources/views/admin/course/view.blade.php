@extends('layouts.admin')

@section('content')

            <div class="row">
            <div class="col-12">
                <div class="box">
                    
                    <!-- /.box-header -->
                    <div class="box-body">
                        <div class="table-responsive">
                          <table id="example1" class="table table-bordered table-striped text-center">
                            <thead>
                                <tr>
                                    <th>Course image</th>
                                    <th>Course type</th>
                                    <th>Course title</th>
                                    <th>Course price</th>
                                    <th>Course time</th>
                                    <th>quiz/exam</th>
                                    <th>action</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($data as $d)
                                    <tr class="bg-dark">
                                        <td>
                                            <img src="{{ asset('course/'.$d['img']) }}" width="250"/>
                                        </td>
                                        <td>
                                           <?php
                                           if($d['type'] == 2){
                                               $type = 'Critical Care Ultrasound';
                                           }elseif($d['type'] == 1){
                                               $type = 'Critical Care Educational';
                                           }elseif($d['type'] == 3){
                                               $type = 'Online Course';
                                           }
                                           echo $type ;
                                           ?>
                                        </td>
                                        <td>
                                           {{$d['title']}}
                                        </td>
                                        <td>
                                           {{$d['price']}}
                                        </td>
                                        <td>
                                           {{$d['from'] .' - '.$d['to']}}
                                        </td>
                                        <td>
                                            QUIZ (
                                            @if(!empty($d['count_quiz']))
                                            {{ count($d['count_quiz']) }}
                                            @else
                                            0
                                            @endif
                                            )
                                            <br>EXAM (
                                            @if(!empty($d['count_exam']))
                                            {{ count($d['count_exam']) }}
                                            @else
                                            0
                                            @endif
                                            )
                                        </td>
                                        <td>
                                            <a href="{{route('admin.course.edit',$d['id'])}}" class="btn btn-success" title="edit"><i class="fa fa-pencil"></i></a>
                                            <a href="{{route('admin.course.del',$d['id'])}}" class="btn btn-danger" title="delete" onclick="return confirm('Are you sure?')"><i class="fa fa-trash"></i></a>
                                            <a href="{{route('admin.course.out',$d['id'])}}" class="btn btn-info"  title="outlines"><i class="fa fa-tasks"></i></a>
                                            <a href="{{route('admin.course.ques',[$d['id'],0])}}" class="btn btn-primary"  title="questions"><i class="fa fa-question-circle-o"></i></a>
                                            <a href="{{route('admin.course.speak',$d['id'])}}" class="btn btn-success" title="speakers"><i class="fa fa-user"></i></a>
                                            <a href="{{route('admin.course.memo',$d['id'])}}" class="btn btn-info"  title="memories"><i class="fa fa-image"></i></a>
                                            <a href="{{route('admin.course.docs',$d['id'])}}" class="btn btn-warning" title="documents"><i class="fa fa-book"></i></a>
                                        </td>
                                    </tr>
                                @endforeach 
                            </tbody>
                            
                          </table>
                        </div>
                        
                    </div>
                    <!-- /.box-body -->
                    
                    
                  </div>
            </div>
        </div>
       
@endsection

