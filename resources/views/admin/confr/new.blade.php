@extends('layouts.admin')

@section('content')

            <div class="row">
                <div class="col-12">
                    <div class="box">
                        <div class="box-header with-border">
                          <h4 class="box-title">{{$page}}</h4>
                           
                    
                        </div>
                        <div class="box-body">
                          <form method="post">
                             {{ csrf_field() }}
                             
                            <!-- text input -->
                            <div class="form-group">
                              <label>Conference name</label>
                              <input type="text" name="event" class="form-control" required="" />
                            </div>

                            <div class="form-group">
                              <label>Conference start</label>
                              <input type="date" name="date_from" class="form-control" min="{{date('Y-m-d')}}" required="" />
                                        
                            </div>
                            
                            <div class="form-group">
                              <label>Conference end</label>
                              <input type="date" name="date_to" class="form-control" min="{{date('Y-m-d')}}" required="" />
                                        
                            </div>
                            
                            <div class="form-group">
                              <label>Conference country</label>
                              <input type="text" name="country" class="form-control" required="" />
                            </div>
                            
                             <div class="form-group">
                              <label>Conference city</label>
                              <input type="text" name="city" class="form-control" required="" />
                            </div>
                            
                             <div class="form-group">
                              <label>Conference LINK</label>
                              <input type="text" name="link" class="form-control" required="" />
                            </div>
                            
                            <div class="box-footer">
                                <input type="submit" class="btn btn-info pull-right" value="add">
                              </div>

                          </form>
                        </div>
                    </div>
                </div>
            </div>

@endsection

