@extends('layouts.admin')

@section('content')

            <div class="row">
                <div class="col-12">
                    <div class="box">
                        <div class="box-body">
                          <form method="post">
                             {{ csrf_field() }}
                             
                            <!-- text input -->
                            <div class="form-group">
                              <label>Conference name</label>
                              <input type="text" name="event" class="form-control"  value="{{ $data['event'] }}" />
                            </div>

                            <div class="form-group">
                              <label>Conference start</label>
                              <input type="date" name="date_from" class="form-control" value="{{ $data['date_from'] }}"/>
                                        
                            </div>
                            
                            <div class="form-group">
                              <label>Conference end</label>
                              <input type="date" name="date_to" class="form-control"  value="{{ $data['date_to'] }}" />
                                        
                            </div>
                            
                            <div class="form-group">
                              <label>Conference country</label>
                              <input type="text" name="country" class="form-control"  value="{{ $data['country'] }}" />
                            </div>
                            
                             <div class="form-group">
                              <label>Conference city</label>
                              <input type="text" name="city" class="form-control"  value="{{ $data['city'] }}" />
                            </div>
                            
                             <div class="form-group">
                              <label>Conference LINK</label>
                              <input type="text" name="link" class="form-control"  value="{{ $data['link'] }}" />
                            </div>
                            
                            <div class="box-footer">
                                <input type="submit" class="btn btn-info pull-right" value="add">
                              </div>

                          </form>
                        </div>
                    </div>
                </div>
            </div>

@endsection

