@extends('layouts.admin')

@section('content')

            <div class="row">
            <div class="col-12">
                <div class="box">
                    <div class="box-header with-border">
                      <h4 class="box-title">{{$page}}</h4>
                      @if( Session::has('status') )
                      
                           <div class="alert alert-{{session('status')[1]}} alert-dismissible text-center">
                            {{ session('status')[0] }}
                          </div>
                      
                    @endif
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body">
                        <div class="table-responsive">
                          <table id="example1" class="table table-bordered table-striped text-center">
                            <thead>
                                <tr>
                                    <th>forum title</th>
                                    <th>forum text</th>
                                    <th>comments  / actions</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($data as $d)
                                  <?php $comments = DB::table('forum_comments')->where('forum_id',$d->id)->get();?>
                                    <tr class="bg-dark">
                                        
                                        <td>
                                           {{$d['title']}}
                                        </td>
                                        <td>
                                           {{$d['des']}}
                                        </td>
                                        
                                        <td>
                                          <table>
                                          @foreach($comments as $comment)
                                            <tr>
                                              <td width="90%" class="text-left">{{$comment['text']}}</td>
                                              <td> 
                                                @if($comment->active == 1)
                                                  <span class="label label-success">active</span>
                                                @else
                                                  <span class="label label-danger">deleted</span>
                                                @endif
                                              </td>
                                              <td>
                                                @if($comment->active == 1)
                                                  <a href="{{route('admin.delforum',$comment['id'])}}" class="btn btn-danger" title="delete" onclick="return confirm('Are you sure?')"><i class="fa fa-trash"></i></a>
                                                @else
                                                  <a href="{{route('admin.redelforum',$comment['id'])}}" class="btn btn-success" title="reactivate" onclick="return confirm('Are you sure?')"><i class="fa fa-refresh"></i></a>
                                                @endif
                                                
                                              </td>
                                            
                                            </tr>
                                          @endforeach
                                          </table>
                                        </td>
                                       
                                    </tr>
                                @endforeach 
                            </tbody>
                            
                          </table>
                        </div>
                        
                    </div>
                    <!-- /.box-body -->
                    
                  </div>
            </div>
        </div>

@endsection

