@extends('layouts.admin')

@section('content')

            <div class="row">
                <div class="col-12">
                    <div class="box">
                        <div class="box-body">
                          <form method="post" enctype="multipart/form-data">
                             {{ csrf_field() }}
                             
                            <!-- text input -->
                            <div class="form-group">
                              <label>team image</label>
                              <img src=" {{ asset('up/'.$data['img']) }}" width="200"/><br>
                              <input type="file" id="imgFile" name="image" />
                            </div>
                            
                            <div class="form-group">
                              <label>Full name</label>
                              <input type="text" name="name" class="form-control" value="{{ $data['name'] }}" />
                            </div>

                            <div class="form-group">
                              <label>Email</label>
                              <input type="email" name="email" class="form-control" value="{{ $data['email'] }}" />
                            </div>
                            
                             <div class="form-group">
                              <label>Job Title</label>
                              <input type="text" name="jobtitle" class="form-control" value="{{ $data['jobtitle'] }}" />
                            </div>
                            
                            <div class="form-group">
                              <label>Facebook</label>
                              <input type="text" name="fb" class="form-control" value="{{ $data['fb'] }}" />
                            </div>
                            
                            <div class="form-group">
                              <label>Twitter</label>
                              <input type="text" name="tw" class="form-control" value="{{ $data['tw'] }}" />
                            </div>
                            
                            <div class="form-group">
                              <label>instagram</label>
                              <input type="text" name="ins" class="form-control" value="{{ $data['ins'] }}" />
                            </div>
                            
                            <div class="form-group">
                              <label>Skype</label>
                              <input type="text" name="skype" class="form-control" value="{{ $data['skype'] }}" />
                            </div>
                            
                            
                            <div class="box-footer">
                                <input type="submit" class="btn btn-info pull-right" value="change">
                              </div>

                          </form>
                        </div>
                        
                    </div>
                </div>
            </div>

@endsection

