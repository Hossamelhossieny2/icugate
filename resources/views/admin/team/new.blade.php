@extends('layouts.admin')

@section('content')

            <div class="row">
                <div class="col-12">
                    <div class="box">
                        <div class="box-header with-border">
                          <h4 class="box-title">{{$page}}</h4>
                           
                    
                        </div>
                        <div class="box-body">
                          <form method="post" enctype="multipart/form-data">
                             {{ csrf_field() }}
                             
                            <!-- text input -->
                            <div class="form-group">
                              <label>team image</label>
                              <input type="file" id="imgFile" name="image" required=""/>
                            </div>
                            
                            <div class="form-group">
                              <label>Full name</label>
                              <input type="text" name="name" class="form-control" required="" />
                            </div>

                            <div class="form-group">
                              <label>Email</label>
                              <input type="email" name="email" class="form-control" required="" />
                            </div>
                            
                             <div class="form-group">
                              <label>Job Title</label>
                              <input type="text" name="jobtitle" class="form-control" required="" />
                            </div>
                            
                            <div class="form-group">
                              <label>Facebook</label>
                              <input type="text" name="fb" class="form-control"  />
                            </div>
                            
                            <div class="form-group">
                              <label>Twitter</label>
                              <input type="text" name="tw" class="form-control"  />
                            </div>
                            
                            <div class="form-group">
                              <label>instagram</label>
                              <input type="text" name="ins" class="form-control"  />
                            </div>
                            
                            <div class="form-group">
                              <label>Skype</label>
                              <input type="text" name="skype" class="form-control"  />
                            </div>
                            
                            
                            <div class="box-footer">
                                <input type="submit" class="btn btn-info pull-right" value="change">
                              </div>

                          </form>
                        </div>
                        
                    </div>
                </div>
            </div>

@endsection

