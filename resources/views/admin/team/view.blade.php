@extends('layouts.admin')

@section('content')

            <div class="row">
            <div class="col-12">
                <div class="box">
                    <div class="box-header with-border">
                      <h4 class="box-title">{{$page}}</h4>
                      <a class="btn btn-success pull-right" href="{{route('admin.newteam')}}">add Member</a>
                      @if( Session::has('status') )
                      
                           <div class="alert alert-{{session('status')[1]}} alert-dismissible text-center">
                            {{ session('status')[0] }}
                          </div>
                      
                    @endif
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body">
                        <div class="table-responsive">
                          <table id="example1" class="table table-bordered table-striped text-center">
                            <thead>
                                <tr>
                                    <th>team image</th>
                                    <th>team name</th>
                                    <th>team email</th>
                                    <th>job title</th>
                                    <th>action</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($data as $d)
                                  
                                    <tr class="bg-dark">
                                        <td>
                                            <img src="{{ asset('up/'.$d['img']) }}" width="250"/>
                                        </td>
                                        <td>
                                           {{ $d['name'] }}
                                        </td>
                                        <td>
                                           {{ $d['email'] }}
                                        </td>
                                        <td>
                                           {{ $d['jobtitle'] }}
                                        </td>
                                        <td>
                                            <a href="{{ route('admin.team.edit',$d['id']) }}" class="btn btn-success" title="edit"><i class="fa fa-pencil"></i></a>
                                            <a href="{{ route('admin.team.del',$d['id']) }}" class="btn btn-danger" title="delete" onclick="return confirm('Are you sure to delete this person?')"><i class="fa fa-trash"></i></a>
                                        </td>
                                    </tr>
                                @endforeach 
                            </tbody>
                            
                          </table>
                        </div>
                        
                    </div>
                    <!-- /.box-body -->
                    
                    
                  </div>
            </div>
        </div>

@endsection

