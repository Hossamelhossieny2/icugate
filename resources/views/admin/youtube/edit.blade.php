@extends('layouts.admin')

@section('content')

            <div class="row">
                <div class="col-12">
                    <div class="box">
                        <div class="box-header with-border">
                          <h4 class="box-title">{{$page}}</h4>
                           <a class="btn btn-success pull-right" href="{{route('admin.newyoutube')}}">add video</a>
                    
                        </div>
                        <div class="box-body">
                          <form method="post" >
                             {{ csrf_field() }}
                             
                            <!-- text input -->
                            <div class="form-group">
                              <label>video title</label>
                              <input type="text" name="title" class="form-control" value="{{$data['title']}}" />
                            </div>

                            <div class="form-group">
                              <label>video section</label>
                              <select name="ccat" class="form-control">
                                  <option value="uploads" <?php if($data->playlist == "uploads")echo 'selected=""';?>>show for all videos</option>
                                  @foreach($ccat as $cc)
                                  <option value="{{$cc['name']}}" <?php if($data['playlist'] == $cc['name'])echo 'selected=""';?>>{{$cc['name']}}</option>
                                  @endforeach
                              </select>
                                        
                            </div>

                            <div class="form-group">
                              <label>video id</label>
                              <input type="text" name="vid_id" class="form-control" value="{{$data['video_id']}}" />
                            </div>

                            <div class="form-group">
                              <div class="checkbox">
                                  <input type="checkbox" name="active" id="Checkbox_10" <?php if($data['need_change'] == 0)echo "checked=''";?>/>
                                  <label for="Checkbox_10">activate this video</label>
                              </div>
                                @if ($errors->has('terms'))
                                <span class="help-block text-danger text-center">{{ $errors->first('terms') }}</span>
                                @endif
                            </div>
                            
                            <input type="hidden" name="id" value="{{$data['youtube_id']}}" />
                            
                            <div class="box-footer">
                                <input type="submit" class="btn btn-info pull-right" value="change">
                              </div>

                          </form>
                        </div>
                       
                    </div>
                </div>
            </div>
        
@endsection

