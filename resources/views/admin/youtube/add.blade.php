@extends('layouts.admin')

@section('content')

            <div class="row">
                <div class="col-12">
                    <div class="box">
                        <div class="box-header with-border">
                          <h4 class="box-title">{{$page}}</h4>
                           
                    
                        </div>
                        <div class="box-body">
                          <form method="post" >
                             {{ csrf_field() }}
                             
                            <!-- text input -->
                            <div class="form-group">
                              <label>video title</label>
                              <input type="text" name="title" class="form-control" required=""/>
                            </div>

                            <div class="form-group">
                              <label>video section</label>
                              <select name="ccat" class="form-control" required="">
                                  <option value="">add video section</option>
                                  <option value="uploads">show for all videos</option>
                                  @foreach($ccat as $cc)
                                  <option value="{{$cc['id']}}">{{$cc['name']}}</option>
                                  @endforeach
                              </select>
                                        
                            </div>

                            <div class="form-group">
                              <label>video id</label>
                              <input type="text" name="vid_id" class="form-control" required="" />
                            </div>

                            <div class="box-footer">
                                <input type="submit" class="btn btn-info pull-right" value="add">
                              </div>

                          </form>
                        </div>
                       
                    </div>
                </div>
            </div>
       
@endsection

