@extends('layouts.admin')

@section('content')

            <div class="row">
            <div class="col-12">
                <div class="box">
                    <div class="box-header with-border">
                      <h4 class="box-title">{{$page}}</h4>
                      <a class="btn btn-success pull-right" href="{{route('admin.newyoutube')}}">add video</a>
                      @if( Session::has('status') )
                      
                           <div class="alert alert-{{session('status')[1]}} alert-dismissible text-center">
                            {{ session('status')[0] }}
                          </div>
                      
                    @endif
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body">
                        <div class="table-responsive">
                          <table id="example1" class="table table-bordered table-striped text-center">
                            <thead>
                                <tr>
                                    <th>video name</th>
                                    <th>playlist</th>
                                    <th>video id</th>
                                    <th>video img</th>
                                    <th>need change</th>
                                    <th>action</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($data as $d)
                                  
                                    <tr class="bg-dark">
                                        <td>
                                            {{$d['title']}}
                                        </td>
                                        <td>
                                           {{$d['playlist']}}
                                        </td>
                                        <td>
                                           {{$d['video_id']}} 
                                        </td>
                                        <td>
                                            @if($d['need_change'] == 1)
                                                <span class="label label-danger">change</span>
                                            @else
                                                <span class="label label-success">good</span>
                                            @endif
                                        </td>
                                        <td>
                                            <img src="{{$d['img']}}" width="100"/>
                                        </td>
                                        <td>
                                            <a href="{{route('admin.youtube.edit',$d['youtube_id'])}}" class="btn btn-success"><i class="fa fa-pencil"></i></a>
                                            <a href="{{route('admin.youtube.del',$d['youtube_id'])}}" class="btn btn-danger" title="delete" onclick="return confirm('Are you sure?')"><i class="fa fa-trash"></i></a>
                                        </td>
                                    </tr>
                                @endforeach 
                            </tbody>
                            
                          </table>
                        </div>
                        
                    </div>
                    <!-- /.box-body -->
                  </div>
            </div>
        </div>
       
@endsection

