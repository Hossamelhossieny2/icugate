@extends('layouts.app')

@section('content')

@push('styles')
  <script src="{{ asset('assets/js/fullcalendar/fullcalendar.min.js') }}"></script>
  <link href="{{ asset('assets/js/fullcalendar/fullcalendar.min.css') }}" rel="stylesheet" type="text/css">
  <script>
    document.addEventListener('DOMContentLoaded', function() {
      var calendarEl = document.getElementById('full-event-calendar');
      var calendar = new FullCalendar.Calendar(calendarEl, {
        themeSystem: 'bootstrap',
        initialDate: '2022-11-23',
        initialView: 'dayGridMonth',
        headerToolbar: {
          left: 'prev,next today',
          center: 'title',
          right: 'dayGridMonth,timeGridWeek,timeGridDay,listWeek'
        },
        height: 'auto',
        navLinks: true, // can click day/week names to navigate views
        editable: true,
        selectable: true,
        selectMirror: true,
        nowIndicator: true,
        events: [
          @foreach($events1 as $event1)
          {
            title: '{{ $event1["event"] }}',
            url: '{{ $event1["link"] }}',
            start: '{{ $event1["date_from"] }}',
            end: '{{ $event1["date_to"] }}',
            color: '#1983B4',
          },
          @endforeach
          
          @foreach($events as $event)
          {
            title: '{{ $event["title"] }}',
            url: '{{ $event["link"] }}',
            start: '{{ $event["event_date"] }}',
            color: '#0B5ED7',
          },
          @endforeach
        ]
      });

      calendar.render();
    });
  </script>
@endpush

<!-- Start main-content -->
  <div class="main-content-area">
    <!-- Section: sliders -->
    <section id="home" class="divider">
      <div class="container-fluid p-0">
      @include('sections.slider')
      </div>
    </section>

    <section>
      <div class="container mt-30 mb-30 pt-30 pb-30">
        <div class="row">

          <!-- Section: main section -->
          <div class="col-md-9 order-lg-2">
            <div id='loading'>loading...</div>
              <div id='full-event-calendar'></div>

@if(!empty($events1))
    <div class="container">
        <div class="section-title text-center">
              <h2 class=" mt-0 line-height-1">CRITICAL CARE <span class="text-primary">CONFERENCES</span></h2>
              <div class="title-icon">
                <img class="mb-10" src="{{ asset('assets/images/title-icon.png') }}" alt="">
              </div>
        </div>
            <div class="section-content">
              <div class="row">
                <div class="col-md-12 col-lg-12">
                  <table class="table table-striped table-schedule">
                    <thead>
                      <tr class="bg-theme-colored1 text-white">
                          <th>From</th>
                          <th>To</th>
                          <th>CRITICAL CARE (INTENSIVE CARE) CONFERENCES</th>
                          <th>City</th>
                          <th>Link</th>
                      </tr>
                    </thead>
                      <tbody>
                      @foreach($events1 as $conf)
                          <tr>
                            <td>{{ $conf['date_from'] }}</td>
                            <td>{{ $conf['date_to'] }}</td>
                            <td><strong>{{ $conf['event'] }}</strong></td>
                            <td>{{ $conf['city'] }}</td>
                            <td><a target="_BLANK" href="{{ $conf['link'] }}" class="btn btn-primary btn-circled">More</a></td>
                          </tr>
                      @endforeach
                      </tbody>
                    </table>
                </div>
              </div>
            </div>
            </div>
    @endif
    @if(!empty($events1))
    <div class="container">
        <div class="section-title text-center">
              <h2 class=" mt-0 line-height-1">icugate <span class="text-primary">Events</span></h2>
              <div class="title-icon">
                <img class="mb-10" src="{{ asset('assets/images/title-icon.png') }}" alt="">
              </div>
        </div>
            <div class="section-content">
              <div class="row">
                <div class="col-md-12 col-lg-12">
                  <table class="table table-striped table-schedule">
                    <thead>
                      <tr class="bg-theme-colored1 text-white">
                          <th>date</th>
                          <th>image</th>
                          <th>Title</th>
                          <th>Description</th>
                          <th>Link</th>
                      </tr>
                    </thead>
                      <tbody>
                      @foreach($events as $conf)
                          <tr>
                            <td>{{ $conf['event_date'] }}</td>
                            <td>{{ $conf['image'] }}</td>
                            <td><strong>{{ $conf['title'] }}</strong></td>
                            <td>{{ $conf['description'] }}</td>
                            <td><a target="_BLANK" href="{{ $conf['link'] }}" class="btn btn-primary btn-circled">More</a></td>
                          </tr>
                      @endforeach
                      </tbody>
                    </table>
                </div>
              </div>
            </div>
          </div>
          @endif
          </div>

          <!-- Section: left panet -->
          <div class="col-md-3 order-lg-1">
            @include('sections.left')
          </div>
          
        </div>
      </div>
    </section>
    
    

  </div>
  <!-- end main-content -->
  
@endsection
