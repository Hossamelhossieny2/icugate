@extends('layouts.app')

@section('content')

@push('styles')
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.2.1/css/all.min.css" integrity="sha512-MV7K8+y+gLIBoVD59lQIYicR65iaqukzvf/nwasF0nqhPay5w/9lJmVM2hMDcnK1OnMGCdVK+iQrJ7lzPJQd1w==" crossorigin="anonymous" referrerpolicy="no-referrer" />
@endpush

   <!-- Start main-content -->
  <div class="main-content-area">
    <!-- Section: page title -->
    <section class="page-title layer-overlay overlay-dark-8 section-typo-light bg-img-center" data-tm-bg-img="{{ asset('assets/images/bg/bg10.jpg') }}">
      <div class="container pt-50 pb-50">
        <div class="section-content">
          <div class="row">
            <div class="col-md-12 text-center">
              <h2 class="title">Job Details </h2>
              
            </div>
          </div>
        </div>
      </div>
    </section>

    <section class="text-center" data-tm-bg-img="{{ asset('assets/images/pattern/p10.png') }}" style="background-image: url({{asset('assets/images/pattern/p10.png')}});">
      <div class="container">
        <div class="row">
          <div class="col-md-9">
          @if(!empty($user_send['id']))
            <span class="alert alert-danger text-center">You applied this Job Before on {{$user_send['created_at']}} .. wait our replay</span>
          @endif
            <div class="p-30 bg-white-fa border-1px">
              <div class="row">
                <div class="col-md-4">
                  <div class="icon-box icon-left iconbox-centered-in-responsive iconbox-theme-colored1 animate-icon-on-hover animate-icon-rotate mb-30">
                    <div class="icon-box-wrapper">
                      <div class="icon-wrapper">
                        <a class="icon icon-type-font-icon"><i class="far fa-calendar-alt"></i></a>
                      </div>
                      <div class="icon-text">
                        <h5 class="icon-box-title mt-0 mb-0">Date Posted</h5>
                        <div class="content mt-0"><a href="tel:{{$conf['phone']}}">{{ \Carbon\Carbon::parse($job_detail['created_at'])->format('M d Y') }}</a></div>
                      </div>
                      <div class="clearfix"></div>
                    </div>
                  </div>
                </div>
                <div class="col-md-4">
                  <div class="icon-box icon-left iconbox-centered-in-responsive iconbox-theme-colored1 animate-icon-on-hover animate-icon-rotate mb-30">
                    <div class="icon-box-wrapper">
                      <div class="icon-wrapper">
                        <a class="icon icon-type-font-icon"><i class="far fa-money-bill-alt"></i></a>
                      </div>
                      <div class="icon-text">
                        <h5 class="icon-box-title mt-0 mb-0">Offered Salary</h5>
                        <div class="content mt-0"><a href="tel:{{$conf['phone']}}">{{$job_detail['salary']}}</a></div>
                      </div>
                      <div class="clearfix"></div>
                    </div>
                  </div>
                </div>
                <div class="col-md-4">
                  <div class="icon-box icon-left iconbox-centered-in-responsive iconbox-theme-colored1 animate-icon-on-hover animate-icon-rotate mb-30">
                    <div class="icon-box-wrapper">
                      <div class="icon-wrapper">
                        <a class="icon icon-type-font-icon"><i class="fas fa-briefcase"></i></a>
                      </div>
                      <div class="icon-text">
                        <h5 class="icon-box-title mt-0 mb-0">Job Title</h5>
                        <div class="content mt-0"><a href="tel:{{$conf['phone']}}">{{$job_detail['job_title']}}</a></div>
                      </div>
                      <div class="clearfix"></div>
                    </div>
                  </div>
                </div>
                <div class="col-md-4">
                  <div class="icon-box icon-left iconbox-centered-in-responsive iconbox-theme-colored1 animate-icon-on-hover animate-icon-rotate mb-30">
                    <div class="icon-box-wrapper">
                      <div class="icon-wrapper">
                        <a class="icon icon-type-font-icon"><i class="far fa-building"></i></a>
                      </div>
                      <div class="icon-text">
                        <h5 class="icon-box-title mt-0 mb-0">Specialization</h5>
                        <div class="content mt-0"><a href="tel:{{$conf['phone']}}">{{$job_detail['specialization']}}</a></div>
                      </div>
                      <div class="clearfix"></div>
                    </div>
                  </div>
                </div>
                <div class="col-md-4">
                  <div class="icon-box icon-left iconbox-centered-in-responsive iconbox-theme-colored1 animate-icon-on-hover animate-icon-rotate mb-30">
                    <div class="icon-box-wrapper">
                      <div class="icon-wrapper">
                        <a class="icon icon-type-font-icon"><i class="fas fa-user-graduate"></i></a>
                      </div>
                      <div class="icon-text">
                        <h5 class="icon-box-title mt-0 mb-0">Qualifications</h5>
                        <div class="content mt-0"><a href="tel:{{$conf['phone']}}">{{$job_detail['qualifications']}}</a></div>
                      </div>
                      <div class="clearfix"></div>
                    </div>
                  </div>
                </div>
                <div class="col-md-4">
                  <div class="icon-box icon-left iconbox-centered-in-responsive iconbox-theme-colored1 animate-icon-on-hover animate-icon-rotate mb-30">
                    <div class="icon-box-wrapper">
                      <div class="icon-wrapper">
                        <a class="icon icon-type-font-icon"><i class="far fa-user"></i></a>
                      </div>
                      <div class="icon-text">
                        <h5 class="icon-box-title mt-0 mb-0">Gender</h5>
                        <div class="content mt-0"><a href="tel:{{$conf['phone']}}">{{$job_detail['gender']}}</a></div>
                      </div>
                      <div class="clearfix"></div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <hr>
            <h3>{{$job_detail['job_title']}}</h3>
            <p>{{$job_detail['job_desc']}}</p>
            <h5 class="mt-30">Requirments:</h5>
            <div class="tm-sc-unordered-list list-style2">
              <ul>
                <li>Create an Attractive Product</a></li>
              </ul>
            </div>
          </div>
          <div class="col-md-3">
            <div class="text-center">
            @if(!empty($user_send['id']))
              <a class="btn btn-danger btn-round mb-30" href="#apply_job">You Applied this job before</a>
            @else
              <a class="btn btn-theme-colored1 btn-round mb-30" href="#apply_job">Apply for the job</a>
            @endif
              <p class="">An easy way to apply for this job. Use the following social media.</p>
              <div class="row mb-30">
                <div class="col"><a data-tm-bg-color="#3b5998" class="btn btn-theme-colored1 btn-round btn-sm" target="_blank" href="{{ $conf['facebook'] }}">Facebook</a></div>
                <div class="col"><a data-tm-bg-color="#007ab9" class="btn btn-theme-colored1 btn-round btn-sm" target="_blank" href="{{ $conf['twitter'] }}">Twitter</a></div>
              </div>
              <div class="widget widget_text">
                <div class="textwidget">
                  <div class="section-typo-light bg-theme-colored1 text-center mb-md-40 p-30 pt-40 pb-40"> <img class="size-full wp-image-800 aligncenter" src="{{ asset('assets/images/headphone-128.png') }}" alt="images" width="128" height="128" />
                  <h4 style="text-align: center;">Online Help!</h4>
                  <h5 style="text-align: center;">{{$conf['phone']}}</h5>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>

@if(!empty($user_send['id']))
  <span class="h4 text-danger">You applied this job on {{$user_send['created_at']}}</span>
@else
    <!-- Section: Job Apply Form -->
    <section class="divider parallax layer-overlay overlay-white-9" data-tm-bg-img="{{ asset('assets/images/bg/bg10.jpg') }}" id="apply_job">
      <div class="container-fluid">
        <div class="row">
          <div class="col-md-6 offset-md-3 bg-lightest-transparent p-30 pt-10">
            <h3 class="text-center text-theme-colored1 mb-20">Apply Now</h3>
            <form method="post" action="{{ route('apply.job') }}" enctype="multipart/form-data" >
            @csrf
            <input type="hidden" name="job_id" value="{{$job_detail['id']}}" />
              <div class="row">
                <div class="col-sm-6">
                  <div class="mb-3">
                    <label>Name <small>*</small></label>
                    <input name="form_name" type="text" placeholder="Enter Name" class="form-control" value="{{old('form_name')}}" />
                  </div>
                  @if ($errors->has('form_name')) 
                      <span class="alert alert-danger text-center">{{$errors->first('form_name')}}</span>
                  @endif
                </div>
                <div class="col-sm-6">
                  <div class="mb-3">
                    <label for="form_gender">Sex <small>*</small></label>
                    <select name="form_gender" class="form-control required">
                      <option value="Male">Male</option>
                      <option value="Female">Female</option>
                    </select>
                  </div>
                  @if ($errors->has('form_gender')) 
                      <span class="alert alert-danger text-center">{{$errors->first('form_gender')}}</span>
                  @endif
                </div>
              </div>
              <div class="row">
                <div class="col-sm-6">
                  <div class="mb-3">
                    <label>Phone <small>*</small></label>
                    <input name="form_phone" type="text" placeholder="Enter phone" class="form-control" value="{{old('form_phone')}}" />
                  </div>
                  @if ($errors->has('form_phone')) 
                      <span class="alert alert-danger text-center">{{$errors->first('form_phone')}}</span>
                  @endif
                </div>
                <div class="col-sm-6">
                  <div class="mb-3">
                    <label for="form_email">Email <small>*</small></label>
                    <input name="form_email" class="form-control required email" type="email" placeholder="Enter Email" value="{{old('form_email')}}" />
                  </div>
                  @if ($errors->has('form_email')) 
                      <span class="alert alert-danger text-center">{{$errors->first('form_email')}}</span>
                  @endif
                </div>
              </div>
              <div class="row">
                <div class="col-sm-6">
                  <div class="mb-3">
                    <label>qualifications <small>*</small></label>
                    <input name="qualifications" type="text" placeholder="Enter qualifications" class="form-control" value="{{old('qualifications')}}" />
                  </div>
                  @if ($errors->has('qualifications')) 
                      <span class="alert alert-danger text-center">{{$errors->first('qualifications')}}</span>
                  @endif
                </div>
                <div class="col-sm-6">
                  <div class="mb-3">
                    <label>specialization <small>*</small></label>
                    <input name="specialization" type="text" placeholder="Enter specialization" class="form-control" value="{{old('specialization')}}" />
                  </div>
                  @if ($errors->has('specialization')) 
                      <span class="alert alert-danger text-center">{{$errors->first('specialization')}}</span>
                  @endif
                </div>
              </div>
              <div class="mb-3">
                <label>about <small>*</small></label>
                <textarea id="form_about" name="form_about" class="form-control required" rows="3" placeholder="Your cover letter/message sent to the employer">{{old('form_about')}}</textarea>
                @if ($errors->has('form_about')) 
                    <span class="alert alert-danger text-center">{{$errors->first('form_about')}}</span>
                @endif
              </div>
              <div class="mb-3">
                <label>C/V Upload</label>
                <input name="form_attachment" class="file" type="file" />
                <small>Maximum upload file size: 3 MB</small>
                @if ($errors->has('form_attachment')) 
                    <span class="alert alert-danger text-center">{{$errors->first('form_attachment')}}</span>
                @endif
              </div>
              <div class="mb-3">
                <button type="submit" class="btn btn-block btn-theme-colored1 mt-20 pt-10 pb-10" data-loading-text="Please wait...">Apply Now</button>
              </div>
            </form>
            
          </div>
        </div>
      </div>
    </section>
  @endif
  </div>
  <!-- end main-content -->

@endsection