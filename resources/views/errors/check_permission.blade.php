<!DOCTYPE html>
<html dir="ltr" lang="en">
<head>

<!-- Meta Tags -->
<meta name="viewport" content="width=device-width,initial-scale=1.0"/>
<meta http-equiv="content-type" content="text/html; charset=UTF-8"/>
<meta name="description" content="icugate"/>
<meta name="keywords" content="medical, clinic, icugate, news, feeds, error, medical news, doctor, health, hospital,"/>
<meta name="author" content="hossamelhossieny"/>

<!-- Page Title -->
<title>{{ config('app.name', 'Laravel') }} | Live 24 Medical News</title>

<!-- Favicon and Touch Icons -->
<link href="{{ asset('assets/images/favicon.png') }}" rel="shortcut icon" type="image/png">
<link href="{{ asset('assets/images/apple-touch-icon.png') }}" rel="apple-touch-icon">
<link href="{{ asset('assets/images/apple-touch-icon-72x72.png') }}" rel="apple-touch-icon" sizes="72x72">
<link href="{{ asset('assets/images/apple-touch-icon-114x114.png') }}" rel="apple-touch-icon" sizes="114x114">
<link href="{{ asset('assets/images/apple-touch-icon-144x144.png') }}" rel="apple-touch-icon" sizes="144x144">

<!-- Stylesheet -->
<link href="{{ asset('assets/css/bootstrap.min.css') }}" rel="stylesheet" type="text/css">
<link href="{{ asset('assets/css/animate.min.css') }}" rel="stylesheet" type="text/css">
<link href="{{ asset('assets/css/javascript-plugins-bundle.css') }}" rel="stylesheet"/>

<!-- CSS | menuzord megamenu skins -->
<link href="{{ asset('assets/js/menuzord/css/menuzord.css') }}" rel="stylesheet"/>

<!-- CSS | Main style file -->
<link href="{{ asset('assets/css/style-main.css') }}" rel="stylesheet" type="text/css">
<link id="menuzord-menu-skins" href="{{ asset('assets/css/menuzord-skins/menuzord-rounded-boxed.css') }}" rel="stylesheet"/>

<!-- CSS | Responsive media queries -->
<link href="{{ asset('assets/css/responsive.css') }}" rel="stylesheet" type="text/css">
<!-- CSS | Style css. This is the file where you can place your own custom css code. Just uncomment it and use it. -->

<!-- CSS | Theme Color -->
<link href="{{ asset('assets/css/colors/theme-skin-color-set1.css') }}" rel="stylesheet" type="text/css">

<!-- external javascripts -->
<script src="{{ asset('assets/js/jquery.js') }}"></script>
<script src="{{ asset('assets/js/popper.min.js') }}"></script>
<script src="{{ asset('assets/js/bootstrap.min.js') }}"></script>
<script src="{{ asset('assets/js/javascript-plugins-bundle.js') }}"></script>
<script src="{{ asset('assets/js/menuzord/js/menuzord.js') }}"></script>

<!-- <link href="css/style.css" rel="stylesheet" type="text/css"> -->


<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
<![endif]-->
</head>
<body class="tm-container-1300px">
<div id="wrapper" class="clearfix">

  <!-- Start main-content -->
  <div class="main-content-area">

    <!-- Section: home -->
    <section id="home" class="fullscreen bg-lightest">
      <div class="display-table text-center">
        <div class="display-table-cell">
          <div class="container pt-0 pb-0">
            <div class="row">
              <div class="col"></div>
              <div class="col-lg-8">
                <h1 class="font-size-150 text-theme-colored1 mt-0 mb-0"><i class="fa fa-ban text-gray-silver"></i>403!</h1>
                <h2 class="mt-0">Oops! Seems You need upgrade your plan</h2>
                <p>You do not have permission to access for this page.</p>
                <a class="btn btn-theme-colored1 btn-round btn-circled" href="{{ route('home') }}">Return Home</a>
                <a class="btn btn-theme-colored1 btn-round btn-circled" href="{{ route('pricing') }}">see our pricing plans</a>
              </div>
              <div class="col"></div>
            </div>
          </div>
        </div>
      </div>
    </section>
  </div>
  <!-- end main-content -->

  <!-- Footer -->
  <footer id="footer" class="footer">
    <div class="container p-20">
      <div class="row">
        <div class="col-lg-12 text-center">
          <p class="mb-0">© 2012-2023 Triangle4it. All Rights Reserved.</p>
        </div>
      </div>
    </div>
  </footer>
  <a class="scrollToTop" href="#"><i class="fa fa-angle-up"></i></a>
</div>
<!-- end wrapper -->

<!-- Footer Scripts -->
<!-- JS | Custom script for all pages -->
<script src="{{ asset('assets/js/custom.js') }}"></script>

</body>
</html>