@extends('layouts.app')

@section('content')


<!-- Start main-content -->
  <div class="main-content-area">

<!-- Section: page title -->
    <section class="page-title layer-overlay overlay-dark-8 section-typo-light bg-img-center" data-tm-bg-img="{{ asset('assets/images/bg/bg10.jpg') }}">
      <div class="container pt-50 pb-50">
        <div class="section-content">
          <div class="row">
            <div class="col-md-12 text-center">
              <h2 class="title"><i class="fa fa-lock"></i> Paypal Payment</h2>
            </div>
          </div>
        </div>
      </div>
    </section>

    <section class="text-center" data-tm-bg-img="{{ asset('assets/images/pattern/p10.png') }}" style="background-image: url({{ asset('assets/images/pattern/p10.png') }});">
      <div class="container">
        <div class="row">
          
          <div class="col-md-6 offset-md-3 bg-white pb-50 pt-20">
          
            <h4 class="mt-0 pt-10">Confirm Paypal Order </h4>
            <hr>
            <p class="text-danger">please note that you about to GoTo paypal pay Form</p>
            <h5>{!! $pay_for !!}</h5>
            <p>Price {{$must_pay}} USD</p>
            
            
            <a class="btn btn-primary" href="{{ route('paypal.payment',$order_id) }}">Ok .. I agree</a>
            <a class="btn btn-secondary" href="{{ url()->previous() }}">Cancel & Back </a>
          </div>
        </div>
      </div>
      <img src="{{ asset('assets/images/secure-paypal-payments.png') }}" width="100"/>
    </section>
</div>

    @endsection
