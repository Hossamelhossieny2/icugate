<section class="divider parallax layer-overlay overlay-theme-colored-8" data-parallax-ratio="0.1" data-bg-img="{{asset('assets/images/course1.jpg')}}">
      <div class="container">
        <div class="section-title text-center">
          <div class="row">
            <div class="col-md-8 col-md-offset-2">
              <h2 class="text-uppercase text-white mt-0 line-height-1">Critical Care Conferences</h2>
              <div class="title-icon">
                <img class="mb-10" src="{{ asset('assets/images/title-icon-white.png')}}" alt="">
              </div>
            </div>
          </div>
        </div>
        <div class="section-content">
          <div class="row">
            <div class="col-md-12">
              
                
              @if($agent!= 1)
                <table class="table table-striped table-schedule">
                <thead>
                  <tr class="bg-theme-colored2">
                      <th>From</th>
                      <th>To</th>
                      <th>CRITICAL CARE (INTENSIVE CARE) CONFERENCES</th>
                      <th>City</th>
                      <th>Link</th>
                  </tr>
                </thead>
                <tbody>
                   @if(!empty($critical))
                      @foreach($critical as $cr)
                      <tr>
                        <td>{{ date('dM',strtotime($cr['date_from'])) }}</td>
                        <td>{{ date('dM',strtotime($cr['date_to'])) }}</td>
                        <td><strong>{{$cr['event']}}</strong></td>
                        <td>{{$cr['country'].' / '.$cr['city']}}</td>
                        <td><a target="_BLANK" href="{{$cr['link']}}" class="btn btn-primary btn-circled">More</a></td>
                      </tr>
                      @endforeach
                     @else
                    <tr>
                      <td colspan="5" style="text-align: center;color:red">No avaliable conferences</td>
                    </tr>
                  @endif
                </tbody>
                </table>
              @else
                <table class="table table-striped table-schedule">
                <thead>
                  <tr class="bg-theme-colored2">
                      <th>From/to</th>
                      <th>CRITICAL CARE (INTENSIVE CARE) CONFERENCES</th>
                      <th>City</th>
                  </tr>
                </thead>
                <tbody>
                   @if(!empty($critical))
                    <tr>
                      <td colspan="5" style="color:red;text-align: center">
                        ** this is mobile view only. open  in browser to see more **
                      </td>
                    </tr>
                      @foreach($critical as $cr)
                      <tr>
                        <td>{{ $cr['date_from'].' To '.$cr['date_to'] }}</td>
                        <td><strong>{{$cr['event']}}</strong></td>
                        <td><a target="_BLANK" href="{{$cr['link']}}" class="btn btn-primary btn-circled">{{$cr['country']}}</a></td>
                      </tr>
                      @endforeach
                     @else
                    <tr>
                      <td colspan="5" style="text-align: center;color:red">No avaliable conferences</td>
                    </tr>
                  @endif
                </tbody>
                </table>
              @endif

             
            </div>
          </div>
        </div> 

        <div class="section-content">
          <div class="row">
            
            <div class="col-md-12 col-lg-12">
              <table class="table table-striped table-schedule bg-white">
                <thead>
                  <tr class="bg-theme-colored1 text-white">
                      <th>From</th>
                      <th>To</th>
                      <th>CRITICAL CARE (INTENSIVE CARE) CONFERENCES</th>
                      <th>City</th>
                      <th>Link</th>
                  </tr>
                </thead>
                  <tbody>
                  @foreach($critical as $conf)
                      <tr>
                        <td>{{ $conf['date_from'] }}</td>
                        <td>{{ $conf['date_to'] }}</td>
                        <td><strong>{{ $conf['event'] }}</strong></td>
                        <td>{{ $conf['city'] }}</td>
                        <td><a target="_BLANK" href="{{ $conf['link'] }}" class="btn btn-primary btn-circled">More</a></td>
                      </tr>
                  @endforeach
                  </tbody>
                </table>
            </div>
          </div>
        </div>
        
      </div>
    </section>