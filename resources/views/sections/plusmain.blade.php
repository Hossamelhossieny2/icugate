<section class="divider parallax layer-overlay overlay-theme-colored-8" data-parallax-ratio="0.1" data-bg-img="assets/images/bg/bg3.jpg') }}" style="background-image: url(assets/images/bg/bg3.jpg); background-position: 50% 181px;">
    <div class="container">
        <div class="section-title text-center">
          <div class="row justify-content-md-center">
            <div class="col-md-8">
              <h2 class=" mt-0 line-height-1">CRITICAL CARE <span class="text-white">CONFERENCES</span></h2>
              <div class="title-icon">
                <img class="mb-10" src="{{ asset('assets/images/title-icon.png') }}" alt="">
              </div>
            </div>
          </div>
        </div>
        <div class="section-content">
          <div class="row">
            
            <div class="col-md-12 col-lg-12">
              <table class="table table-striped table-schedule bg-white">
                <thead>
                  <tr class="bg-theme-colored1 text-white">
                      <th>From</th>
                      <th>To</th>
                      <th>CRITICAL CARE (INTENSIVE CARE) CONFERENCES</th>
                      <th>City</th>
                      <th>Link</th>
                  </tr>
                </thead>
                  <tbody>
                  @foreach($critical as $conf)
                      <tr>
                        <td>{{ $conf['date_from'] }}</td>
                        <td>{{ $conf['date_to'] }}</td>
                        <td><strong>{{ $conf['event'] }}</strong></td>
                        <td>{{ $conf['city'] }}</td>
                        <td><a target="_BLANK" href="{{ $conf['link'] }}" class="btn btn-primary btn-circled">More</a></td>
                      </tr>
                  @endforeach
                  </tbody>
                </table>
            </div>
          </div>
        </div>
      </div>
      
    </section>
    
     <!-- Divider: Certificates -->
    <section class="bg-silver-light">
      <div class="container">
        <div class="row">
          <div class="col-md-12">
            <div class="owl-carousel owl-theme tm-owl-carousel-4col">
              <div class="item">
                <a href="#" data-lightbox-gallery="certificates" title="Certificate"><img src="{{ asset('assets/images/certificates/s1.jpg') }}" alt=""></a>
              </div>
              <div class="item">
                <a href="#" data-lightbox-gallery="certificates" title="Certificate"><img src="{{ asset('assets/images/certificates/s2.jpg') }}" alt=""></a>
              </div>
              <div class="item">
                <a href="#" data-lightbox-gallery="certificates" title="Certificate"><img src="{{ asset('assets/images/certificates/s3.jpg') }}" alt=""></a>
              </div>
              <div class="item">
                <a href="#" data-lightbox-gallery="certificates" title="Certificate"><img src="{{ asset('assets/images/certificates/s4.jpg') }}" alt=""></a>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>

<section class="text-center" data-tm-bg-img="{{ asset('assets/images/pattern/p10.png') }}" style="background-image: url({{ asset('assets/images/pattern/p10.png') }});">
      <div class="container">
        <div class="row mt-10">
          <div class="col-sm-12">
            <div class="section-title text-center  mb-40">
          <div class="row justify-content-md-center">
            <div class="col-md-8">
              <h2 class=" mt-0 line-height-1">Our <span class="text-theme-colored1">Pricing</span></h2>
              <div class="title-icon">
                <img class="mb-10" src="{{ asset('assets/images/title-icon.png') }}" alt="">
              </div>
              <p>Choose your desire paln which suite your needs</p>
            </div>
          </div>
        </div>
            <div class="row">
            @foreach($pricing as $price)
              <div class="col-sm-4">
                <div class="tm-sc-pricing-table pricing-table-style1 pricing-list-bordered pricing-table-hover-effect pricing-table-box-shadow text-center mb-lg-30 @if(auth()->user() && auth()->user()->type == $price['type']) bg-dark @endif">
                  <div class="pricing-table-inner-wrapper">
                    <div class="pricing-table-thumb">
                      <img src="{{ asset($price['img']) }}" alt="Image">
                    </div>
                    <div class="pricing-table-inner p-0 pt-30">
                      <div class="pricing-table-head">
                        <div class="pricing-table-title-area pt-0 pb-0">
                          <h4 class="pricing-table-title">{{ $price['title'] }}</h4>
                          <h5 class="pricing-table-subtitle">{{ $price['brief'] }}</h5>
                        </div>
                        <div class="pricing-table-pricing">
                          <span class="pricing-table-prefix">$</span>
                          <span class="pricing-table-price">{{ $price['price'] }}</span>
                          <span class="pricing-table-separator">/</span>
                          <span class="pricing-table-postfix">Year</span>
                        </div>
                      </div>
                      <div class="pricing-table-content">
                        <ul>
                          <li>Browse articles</li>
                          <li>@if($price['videos'] == 1) Browse Videos / Presentations @else No Videos / Presentations @endif</li>
                          <li>@if($price['protocols'] == 1) View ICU procedures and protocols @else No ICU procedures and protocols @endif</li>
                          <li>@if($price['lectures'] == 1) publish articles or presentations @else Can't publish articles or presentations @endif</li>
                          <li>@if($price['lectures'] == 1) View Resident Lectures @else Can't view Resident Lectures @endif</li>
                          <li>CCESP Course {{$price['ccesp_discount']}} % OFF</li>
                        </ul>
                      </div>
                      <div class="pricing-table-footer">
                        @auth
                          @if(auth()->user()->type == $price['type'])
                          <a href="#" target="_self" class="btn btn-theme-colored1 btn-round"> Your current Plan </a>
                          @elseif(auth()->user()->type < $price['type'])
                          <a href="{{route('icugate_upgrade',$price['id'])}}" target="_self" class="btn btn-success btn-round"> upgrade </a>
                          @endif
                        @else
                        <a href="{{route('register')}}" target="_self" class="btn btn-theme-colored1 btn-round"> order Now </a>
                        @endif
                      </div>
                      @if($price['popular'] == 1)
                      <span class="pricing-table-label">Popular</span>
                      @endif
                    </div>
                  </div>
                </div>
              </div>
              @endforeach
              
            </div>
          </div>
        </div>
      </div>
    </scetion>

    <section class="bg-silver-light">
      <div class="container">
        <div class="section-title text-center  mb-40">
          <div class="row justify-content-md-center">
            <div class="col-md-8">
              <h2 class=" mt-0 line-height-1">Our <span class="text-theme-colored1">Team</span></h2>
              <div class="title-icon">
                <img class="mb-10" src="{{ asset('assets/images/title-icon.png') }}" alt="">
              </div>
              <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Rem autem<br> voluptatem obcaecati!</p>
            </div>
          </div>
        </div>
        <div class="row mtli-row-clearfix">
          <div class="col-md-3">
            <div class="team-members border-bottom-theme-color-2px text-center maxwidth400">
                <div class="team-thumb">
                    <img class="img-fullwidth" alt="" src="{{ asset('up/'.$teams[0]->img) }}">
                    <div class="team-overlay"></div>
                </div>
                <div class="team-details bg-silver-light pt-20 pb-30">
                  <h3 class="mb-0">{{$teams[0]->name}}</h3>
                  <h6 class="text-theme-colored1 mb-20">{{$teams[0]->jobtitle}}</h6>
                  <ul class="styled-icons icon-dark icon-sm icon-theme-colored1 icon-circled clearfix">
                    <li><a class="social-link" href="#"><i class="fab fa-facebook"></i></a></li>
                    <li><a class="social-link" href="#"><i class="fab fa-instagram"></i></a></li>
                    <li><a class="social-link" href="#"><i class="fab fa-twitter"></i></a></li>
                    <li><a class="social-link" href="#"><i class="fab fa-youtube"></i></a></li>
                  </ul>
                </div>
              </div>
          </div>
          <div class="col-md-9">
            <div class="owl-carousel owl-theme tm-owl-carousel-3col">
              @for($i=1;$i<4;$i++)
              <div class="item">
                <div class="team-members border-bottom-theme-color-2px text-center maxwidth400">
                  <div class="team-thumb">
                    <img class="img-fullwidth" alt="" src="{{ asset('up/'.$teams[$i]->img) }}">
                    <div class="team-overlay"></div>
                  </div>
                  <div class="team-details bg-silver-light pt-20 pb-30">
                    <h3 class="mb-0">{{ $teams[$i]->name }}</h3>
                    <h6 class="text-theme-colored1 mb-20">{{ $teams[$i]->jobtitle }}</h6>
                    <ul class="styled-icons icon-dark icon-sm icon-theme-colored1 icon-circled clearfix">
                      <li><a class="social-link" href="#"><i class="fab fa-facebook"></i></a></li>
                      <li><a class="social-link" href="#"><i class="fab fa-instagram"></i></a></li>
                      <li><a class="social-link" href="#"><i class="fab fa-twitter"></i></a></li>
                      <li><a class="social-link" href="#"><i class="fab fa-youtube"></i></a></li>
                    </ul>
                  </div>
                </div>
              </div>
              @endfor
              
            </div>
          </div>
        </div>
      </div>
    </section>

    <section class="divider parallax layer-overlay overlay-theme-colored1-8" data-tm-bg-img="{{ asset('assets/images/bg/bg10.jpg') }}" style="background-image: url(&quot;assets/images/bg/bg10.jpg&quot;); background-position: 50% 36px;">
      <div class="container">
        <div class="row section-typo-light">
          <div class="col-lg-3 col-md-6">
            <div class="tm-sc-funfact funfact text-center">
              <div class="funfact-icon mb-20"><i class="flaticon-medical-family21"></i></div>
              <h2 class="counter"> <span class="animate-number appeared" data-value="{{ $stat_bar['users_count'] }}" data-animation-duration="1500">{{ $stat_bar['users_count'] }}</span><span class="counter-postfix">+</span></h2>
              <h4 class="title">Happy Patients</h4>
            </div>
          </div>
          <div class="col-lg-3 col-md-6">
            <div class="tm-sc-funfact funfact text-center">
              <div class="funfact-icon mb-20"><i class="flaticon-medical-first32"></i></div>
              <h2 class="counter"> <span class="animate-number appeared" data-value="{{ $stat_bar['posts_count'] }}" data-animation-duration="1500">{{ $stat_bar['posts_count'] }}</span><span class="counter-postfix">+</span></h2>
              <h4 class="title">Our Articles</h4>
            </div>
          </div>
          <div class="col-lg-3 col-md-6">
            <div class="tm-sc-funfact funfact text-center">
              <div class="funfact-icon mb-20"><i class="flaticon-medical-medical51"></i></div>
              <h2 class="counter"> <span class="animate-number appeared" data-value="240" data-animation-duration="1500">240</span><span class="counter-postfix">+</span></h2>
              <h4 class="title">Our specialist</h4>
            </div>
          </div>
          <div class="col-lg-3 col-md-6">
            <div class="tm-sc-funfact funfact text-center">
              <div class="funfact-icon mb-20"><i class="flaticon-medical-caduceus3"></i></div>
              <h2 class="counter"> <span class="animate-number appeared" data-value="{{ $stat_bar['site_visit'] }}" data-animation-duration="1500">{{ $stat_bar['site_visit'] }}</span><span class="counter-postfix">+</span></h2>
              <h4 class="title">visits Points</h4>
            </div>
          </div>
        </div>
      </div>
    </section>