<!-- sidebar menu-->
<ul class="sidebar-menu" data-widget="tree">
          
          <li class="header nav-small-cap">ADMIN LINKS</li>
          
          <li  @if($menu == 'dash') class="active" @endif>
            <a href="{{route('admin.home')}}">
              <i class="ti-dashboard"></i>
              Dashboard
            </a>
            
          </li>
          <li class="treeview @if($menu == 'setting') active @endif">
            <a href="#">
              <i class="ti-settings"></i>
              <span>Setting</span>
              <span class="pull-right-container">
                <i class="fa fa-angle-right pull-right"></i>
              </span>
            </a>
            <ul class="treeview-menu">
              <li><a href="{{route('admin.site')}}"><i class="ti-more"></i>site info</a></li>
              <li><a href="{{route('admin.pricing')}}"><i class="ti-more"></i>sub price</a></li>
              <li><a href="{{route('admin.tele')}}"><i class="ti-more"></i>Consultation</a></li>
            </ul>
          </li> 
          
          <li class="treeview @if($menu == 'post') active @endif">
            <a href="#">
              <i class="ti-location-arrow"></i>
              <span>article (s)</span>
              <span class="pull-right-container">
                <i class="fa fa-angle-right pull-right"></i>
              </span>
            </a>
            <ul class="treeview-menu">
              <li><a href="{{ route('post.add.any') }}"><i class="ti-more"></i> new article</a></li>
              <li><a href="{{ route('admin.post') }}"><i class="ti-more"></i>all article(s)</a></li>
              <li><a href="{{ route('admin.month_article') }}"><i class="ti-more"></i>month article</a></li>
              <li><a href="{{ route('admin.post.mc') }}"><i class="ti-more"></i>Contribution</a></li>
            </ul>
          </li> 
  
           <li class="treeview @if($menu == 'conf') active @endif">
            <a href="#">
              <i class="ti-world"></i>
              <span>Conferences</span>
              <span class="pull-right-container">
                <i class="fa fa-angle-right pull-right"></i>
              </span>
            </a>
            <ul class="treeview-menu">
              <li><a href="{{route('admin.confr')}}"><i class="ti-more"></i>all Conf.</a></li>
              <li><a href="{{route('admin.newconfr')}}"><i class="ti-more"></i>new conference</a></li>
            </ul>
          </li> 

          <li class="treeview @if($menu == 'event') active @endif">
            <a href="#">
              <i class="ti-world"></i>
              <span>icugate events</span>
              <span class="pull-right-container">
                <i class="fa fa-angle-right pull-right"></i>
              </span>
            </a>
            <ul class="treeview-menu">
              <li><a href="{{route('admin.event')}}"><i class="ti-more"></i>icugate Events</a></li>
              <li><a href="{{route('admin.newevent')}}"><i class="ti-more"></i>new Event</a></li>
            </ul>
          </li> 
  
          <li class="treeview @if($menu == 'user') active @endif">
            <a href="#">
              <i class="ti-stamp"></i>
              <span>System User</span>
              <span class="pull-right-container">
                <i class="fa fa-angle-right pull-right"></i>
              </span>
            </a>
            <ul class="treeview-menu">
            <li><a href="{{route('admin.edituser')}}"><i class="ti-more"></i>Edit / delete</a></li>
              <li><a href="{{route('admin.user')}}"><i class="ti-more"></i>activate user</a></li>
            </ul>
          </li> 

          <li class="treeview @if($menu == 'payment') active @endif">
            <a href="#">
              <i class="ti-stamp"></i>
              <span>User Payments</span>
              <span class="pull-right-container">
                <i class="fa fa-angle-right pull-right"></i>
              </span>
            </a>
            <ul class="treeview-menu">
              <li><a href="{{route('admin.payuser')}}"><i class="ti-more"></i>paypal payment</a></li>
              <li><a href="{{route('user.payment')}}"><i class="ti-more"></i>manual payment</a></li>
            </ul>
          </li> 
          
          <li class="treeview @if($menu == 'course') active @endif">
            <a href="#">
              <i class="ti-agenda"></i>
              <span>Courses</span>
              <span class="pull-right-container">
                <i class="fa fa-angle-right pull-right"></i>
              </span>
            </a>
            <ul class="treeview-menu">
              <li><a href="{{route('admin.newcourese')}}"><i class="ti-more"></i>New Courses</a></li>
              <li><a href="{{route('admin.course')}}"><i class="ti-more"></i>all Courses</a></li>
            </ul>
          </li> 

          <li class="treeview @if($menu == 'onlinecourse') active @endif">
            <a href="#">
              <i class="ti-agenda"></i>
              <span>Online courses</span>
              <span class="pull-right-container">
                <i class="fa fa-angle-right pull-right"></i>
              </span>
            </a>
            <ul class="treeview-menu">
              <li><a href="{{route('online.newcourese')}}"><i class="ti-more"></i>New Courses</a></li>
              <li><a href="{{route('online.course')}}"><i class="ti-more"></i>all Courses</a></li>
            </ul>
          </li> 
          
          <li class="treeview @if($menu == 'cme') active @endif">
            <a href="#">
              <i class="ti-pulse"></i>
              <span>critical care PPT</span>
              <span class="pull-right-container">
                <i class="fa fa-angle-right pull-right"></i>
              </span>
            </a>
            <ul class="treeview-menu">
              <li><a href="{{route('admin.ccat.docs',1)}}"><i class="ti-more"></i>CC educational</a></li>
              <li><a href="{{route('admin.ccat.docs',2)}}"><i class="ti-more"></i>CC ultrasound</a></li>
            </ul>
          </li> 
          
          <li class="treeview @if($menu == 'youtube') active @endif">
            <a href="#">
              <i class="ti-youtube"></i>
              <span>Youtube</span>
              <span class="pull-right-container">
                <i class="fa fa-angle-right pull-right"></i>
              </span>
            </a>
            <ul class="treeview-menu">
              <li><a href="{{route('admin.youtube')}}"><i class="ti-more"></i>youtube videos</a></li>
              <li><a href="{{route('admin.youtubech')}}"><i class="ti-more"></i>youtube channels</a></li>
            </ul>
          </li> 
          
          <li class="treeview @if($menu == 'team') active @endif">
            <a href="#">
              <i class="ti-crown"></i>
              <span>Team</span>
              <span class="pull-right-container">
                <i class="fa fa-angle-right pull-right"></i>
              </span>
            </a>
            <ul class="treeview-menu">
              <li><a href="{{route('admin.team')}}"><i class="ti-more"></i>Our team</a></li>
            </ul>
          </li> 
          <li class="treeview @if($menu == 'job') active @endif">
            <a href="#">
              <i class="ti-pencil-alt"></i>
              <span>Jobs</span>
              <span class="pull-right-container">
                <i class="fa fa-angle-right pull-right"></i>
              </span>
            </a>
            <ul class="treeview-menu">
              <li><a href="{{route('admin.icujob')}}"><i class="ti-more"></i>icugate Jobs</a></li>
              <li><a href="{{route('admin.cv')}}"><i class="ti-more"></i>deliverd CV</a></li>
            </ul>
  
          </li> 
  
          <!-- <li class="treeview">
            <a href="#">
              <i class="ti-headphone-alt"></i>
              <span>Forum</span>
              <span class="pull-right-container">
                <i class="fa fa-angle-right pull-right"></i>
              </span>
            </a>
            <ul class="treeview-menu">
              <li><a href="{{route('admin.forum')}}"><i class="ti-more"></i>Comments</a></li>
            </ul>
            
          </li> -->
  
          <li>
              <a class="dropdown-item" href="{{ route('logout',[app()->getlocale()]) }}"
                        onclick="event.preventDefault();
                                      document.getElementById('logout-form').submit();">
                        <i class="ti-power-off"></i> {{ __('Logout') }}
                    </a>

                    <form id="logout-form" action="{{ route('logout',[app()->getlocale()]) }}" method="POST" style="display: none;">
                        @csrf
                    </form>
  
          </li> 
          
        </ul>