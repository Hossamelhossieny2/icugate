          
          <h4 class="widget-title title-dots"><span>{{$cat_tag['cat_title']}} ({{$cat_tag['tag_title']}})</span></h4>
        <div class="tm-sc-gallery tm-sc-gallery-grid gallery-style1-basic">
          <!-- Isotope Gallery Grid -->
          <div class="isotope-layout masonry grid-2 gutter-15 clearfix">
            <div class="isotope-layout-inner">
              <div class="isotope-item isotope-item-sizer"></div>
              @foreach($posts as $post)
              <!-- Isotope Item Start -->
              
              @include('block/block0')

              <!-- Isotope Item End -->
              @endforeach
            </div>             
          </div>
        </div>
          <!-- End Isotope Gallery Grid -->
        @if($posts instanceof \Illuminate\Pagination\LengthAwarePaginator )
        <div class="col-md-12">
            <nav>
              <ul class="pagination theme-colored">
                 {{ $posts->links() }}
              </ul>
            </nav>
          </div>
          @endif