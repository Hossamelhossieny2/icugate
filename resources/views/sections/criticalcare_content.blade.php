        @if($ccat['id'] != 2)
        <h4 class="widget-title title-dots"><span>UpComming Courses</span></h4>
        <div class="tm-sc-gallery tm-sc-gallery-grid gallery-style1-basic">
          <!-- Isotope Gallery Grid -->
          
          <div class="isotope-layout masonry grid-3 gutter-15 clearfix">
            <div class="isotope-layout-inner">
                <div class="isotope-item isotope-item-sizer"></div>
                @if(!empty($new_cources))
                @foreach($new_cources as $course)
                <div class="isotope-item">
                <div class="isotope-item-inner">
                  <div class="tm-sc-blog blog-style-default mb-30">
                    <article class="post type-post status-publish format-standard has-post-thumbnail">
                      <div class="entry-content">
                        <h4 class="entry-title"><a target="_blank" href="{{ $ccat['link1'] }}" rel="bookmark">Research (1)</a></h4>
                        <div class="post-btn-readmore"> <a target="_blank" href="{{ $ccat['link1'] }}" class="btn btn-plain-text-with-arrow"> View Details </a></div>
                        <div class="clearfix"></div>
                      </div>
                    </article>
                  </div>
                </div>
              </div>
              @endforeach
              @else
              <div class="isotope-item">
                <div class="isotope-item-inner">
                  <div class="tm-sc-blog blog-style-default mb-30">
                    <span class="alert text-danger">no Courses avaliable right now .</span>
                  </div>
                </div>
              </div>
              @endif
            </div>   
          </div>
        </div>
          <!-- End Isotope Gallery Grid -->

        <h4 class="widget-title title-dots"><span>Previous Courses</span></h4>
        <div class="tm-sc-gallery tm-sc-gallery-grid gallery-style1-basic">
          <!-- Isotope Gallery Grid -->
          
          <div class="row">
            <div class="col-md-12">
              <div class="tm-owl-carousel-3col" data-nav="true" data-margin="5">
                @foreach($old_courses as $course)
                <div class="item font-9">
                  <div class="featured-news">
                    <img src="{{ asset('up/'.$course['img']) }}" alt="images">
                    <div class="featured-news-details">
                      @if($course['type'] == 3)
                        <span class="badge bg-secondary">Online</span>
                      @else
                        <span class="badge bg-warning text-dark">Offilne</span>
                      @endif
                      <p class="text-white">{{ $course['title'] }}</p>
                      <a href="#" class="btn btn-plain-text-with-arrow text-white">Read more</a>
                    </div>
                  </div>
                </div>
                @endforeach
              </div>
            </div>
          </div>
          
        </div>
          <!-- End Isotope Gallery Grid -->

        @endif

        <h4 class="widget-title title-dots"><span>Ongoing Researches</span></h4>
        <div class="tm-sc-gallery tm-sc-gallery-grid gallery-style1-basic">
          <!-- Isotope Gallery Grid -->
          <div class="isotope-layout masonry grid-3 gutter-15 clearfix">
            <div class="isotope-layout-inner">
                <div class="isotope-item isotope-item-sizer"></div>

                <div class="isotope-item">
                <div class="isotope-item-inner">
                  <div class="tm-sc-blog blog-style-default mb-30">
                    <article class="post type-post status-publish format-standard has-post-thumbnail">
                      <div class="entry-content">
                        <h4 class="entry-title"><a target="_blank" href="{{ $ccat['link1'] }}" rel="bookmark">Research (1)</a></h4>
                        <div class="post-btn-readmore"> <a target="_blank" href="{{ $ccat['link1'] }}" class="btn btn-plain-text-with-arrow"> View Details </a></div>
                        <div class="clearfix"></div>
                      </div>
                    </article>
                  </div>
                </div>
              </div>

              <div class="isotope-item">
                <div class="isotope-item-inner">
                  <div class="tm-sc-blog blog-style-default mb-30">
                    <article class="post type-post status-publish format-standard has-post-thumbnail">
                      <div class="entry-content">
                        <h4 class="entry-title"><a target="_blank" href="{{ $ccat['link2'] }}" rel="bookmark">Research (2)</a></h4>
                        <div class="post-btn-readmore"> <a target="_blank" href="{{ $ccat['link2'] }}" class="btn btn-plain-text-with-arrow"> View Details </a></div>
                        <div class="clearfix"></div>
                      </div>
                    </article>
                  </div>
                </div>
              </div>

              <div class="isotope-item">
                <div class="isotope-item-inner">
                  <div class="tm-sc-blog blog-style-default mb-30">
                    <article class="post type-post status-publish format-standard has-post-thumbnail">
                      <div class="entry-content">
                        <h4 class="entry-title"><a target="_blank" href="{{ $ccat['link3'] }}" rel="bookmark">Research (3)</a></h4>
                        <div class="post-btn-readmore"> <a target="_blank" href="{{ $ccat['link3'] }}" class="btn btn-plain-text-with-arrow"> View Details </a></div>
                        <div class="clearfix"></div>
                      </div>
                    </article>
                  </div>
                </div>
              </div>

            </div>   
            
                      
          </div>
        </div>
          <!-- End Isotope Gallery Grid -->

          <h4 class="widget-title title-dots"><span>Power Point Presentation</span></h4>
        <div class="tm-sc-gallery tm-sc-gallery-grid gallery-style1-basic">
          <!-- Isotope Gallery Grid -->
          <div class="isotope-layout masonry grid-4 gutter-15 clearfix">
            <div class="isotope-layout-inner">
                <div class="isotope-item isotope-item-sizer"></div>
                @foreach($docs as $post)
                  @include('block.block0')
                @endforeach
            </div>   
            
                      
          </div>
        </div>
          <!-- End Isotope Gallery Grid -->

          <h4 class="widget-title title-dots"><span>videos</span></h4>
        <div class="tm-sc-gallery tm-sc-gallery-grid gallery-style1-basic">
          <!-- Isotope Gallery Grid -->
          <div class="isotope-layout masonry grid-2 gutter-15 clearfix">
            <div class="isotope-layout-inner">
                <div class="isotope-item isotope-item-sizer"></div>
                @foreach($vids as $vid)
                <div class="isotope-item">
                    <div class="isotope-item-inner">
                        <div class="tm-sc-blog blog-style-default mb-30">
                            <article class="post type-post status-publish format-standard has-post-thumbnail">
                                <div class="entry-header">
                                    <div class="post-thumb lightgallery-lightbox">
                                    <div class="post-thumb-inner">
                                        <div class="thumb"> <img src="{{ $vid['img'] }}" alt="Image"/></div>
                                    </div>
                                    </div>
                                    <a class="link popup-youtube" href="http://www.youtube.com/watch?v={{$vid['video_id']}}"><i class="fas fa-play-circle"></i></a>
                                </div>
                                <div class="entry-content">
                                    <h4 class="entry-title"><a target="_blank" href="#" rel="bookmark">{{$vid['title']}}</a></h4>
                                    <div class="entry-meta mt-0 mb-0">
                                        <span class="mb-10 text-gray-darkgray mr-10 font-size-13"> <i class="fab fa-youtube"></i></span>
                                        <span class="mb-10 text-gray-darkgray mr-10 font-size-13"><i class="far fa-calendar-alt mr-10 text-theme-colored1"></i>{{ \Carbon\Carbon::parse($post['date'])->format('M d Y') }}</span>
                                    </div>
                                    
                                    <div class="clearfix"></div>
                                </div>
                            </article>
                        </div>
                    </div>
                </div>
                @endforeach
            </div>   
            
                      
          </div>
        </div>
          <!-- End Isotope Gallery Grid -->
