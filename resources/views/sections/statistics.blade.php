<!-- Divider: Funfact -->
<section class="divider parallax layer-overlay overlay-theme-colored-gradient-9" data-bg-img="{{asset('assets/images/pattern/p4.png')}}" data-parallax-ratio="0.7">
      <div class="container">
        <div class="row">
          <div class="col-xs-12 col-sm-6 col-md-3 mb-md-50">
            <div class="funfact text-center">
              <i class="pe-7s-smile mt-5 text-white"></i>
              <h2 data-animation-duration="2000" data-value="{{$stat_bar['users_count']}}" class="animate-number text-white font-42 font-weight-500">0</h2>
              <h4 class="text-white text-uppercase font-weight-600">Happy Members</h4>
            </div>
          </div>
          <div class="col-xs-12 col-sm-6 col-md-3 mb-md-50">
            <div class="funfact text-center">
              <i class="pe-7s-rocket mt-5 text-white"></i>
              <h2 data-animation-duration="2000" data-value="{{ $stat_bar['posts_count'] }}" class="animate-number text-white font-42 font-weight-500">0</h2>
              <h4 class="text-white text-uppercase font-weight-600">Our Articles</h4>
            </div>
          </div>
          <div class="col-xs-12 col-sm-6 col-md-3 mb-md-50">
            <div class="funfact text-center">
              <i class="pe-7s-add-user mt-5 text-white"></i>
              <h2 data-animation-duration="2000" data-value="{{$stat_bar['courses_count']}}" class="animate-number text-white font-42 font-weight-500">0</h2>
              <h4 class="text-white text-uppercase font-weight-600">Our Cources</h4>
            </div>
          </div>
          <div class="col-xs-12 col-sm-6 col-md-3 mb-md-50">
            <div class="funfact text-center">
              <i class="pe-7s-global mt-5 text-white"></i>
              <h2 data-animation-duration="2000" data-value="{{$conf['site_visit']}}" class="animate-number text-white font-42 font-weight-500">0</h2>
              <h4 class="text-white text-uppercase font-weight-600">Online Visits</h4>
            </div>
          </div>
        </div>
      </div>
    </section>