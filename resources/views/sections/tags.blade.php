<div class="widget widget_tag_cloud">
    <!-- <h4 class="widget-title widget-title-line-bottom line-bottom-theme-colored1">CME Tags</h4> -->
    <div class="tagcloud">
    @foreach($tags as $tag)
      <a href="@if($tag['count'] == 0) # @else {{ route('cme.'.$usertype,[$cat_tag['cat_id'],$tag['id']]) }} @endif" class="tag-cloud-link @if($tag['id'] == $cat_tag['tag_id']) bg-primary text-white @endif">{{ $tag['title'] }} <span class="badge @if($tag['id'] != $cat_tag['tag_id']) bg-secondary @else bg-black @endif" @if($tag['count'] == 0) style="color:orange" @endif>{{ $tag['count'] }}</span></a>
    @endforeach
    </div>
</div>