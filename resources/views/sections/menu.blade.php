<nav id="menuzord-right" class="menuzord blue no-bg"><a class="menuzord-brand pull-left flip mb-15" href="{{url('/')}}"><img src="{{ asset('assets/images/logo2.png')}}" alt=""></a>
              <ul class="menuzord-menu">
              <li class="active"><a href="{{ route('home.links','all') }}">Home</a>
              </li>
              <li><a href="javascript:void(0)">Resources</a>
                <div class="megamenu megamenu-bg-img">
                  <div class="megamenu-row">
                    <div class="col3">
                      <h4 class="megamenu-col-title">Articles:</h4>
                      <div class="widget">
                        <div class="latest-posts">
                          <article class="post media-post clearfix pb-0 mb-10">
                            <a href="#" class="post-thumb"><img alt="" src="{{ asset('assets/images/arc.jpg') }}"></a>
                            <div class="post-right">
                              <h5 class="post-title mt-0 mb-5"><a href="{{url('home/archive')}}">Our archive</a></h5>
                              <p class="post-date mb-0 font-12">Before, {{date('Y',strtotime('-1 year'))}}</p>
                            </div>
                          </article>
                          <article class="post media-post clearfix pb-0 mb-10">
                            <a href="#" class="post-thumb"><img alt="" src="{{ asset('assets/images/fam.jpg') }}"></a>
                            <div class="post-right">
                              <h5 class="post-title mt-0 mb-5"><a href="{{ route('home.links','famous') }}">Famous</a></h5>
                              <p class="post-date mb-0 font-12">on, {{date('Y')}}</p>
                            </div>
                          </article>
                          <article class="post media-post clearfix pb-0 mb-10">
                            <a href="#" class="post-thumb"><img alt="" src="{{ asset('assets/images/rec.jpg') }}"></a>
                            <div class="post-right">
                              <h5 class="post-title mt-0 mb-5"><a href="{{ route('home.links','all') }}">Recent</a></h5>
                              <p class="post-date mb-0 font-12">Last, {{date('Y')}}</p>
                            </div>
                          </article>
                          <article class="post media-post clearfix pb-0 mb-10">
                            <a href="#" class="post-thumb"><img alt="" src="{{ asset('assets/images/hot.jpg') }}"></a>
                            <div class="post-right">
                              <h5 class="post-title mt-0 mb-5"><a href="#">Hot Topic</a></h5>
                              <p class="post-date mb-0 font-12">on, {{date('Y')}}</p>
                            </div>
                          </article>
                        </div>
                      </div>
                    </div>
                    @isset($famouss[0])
                    <div class="col3">
                      <h4 class="megamenu-col-title"><strong>Article of the month:</strong></h4>
                      <article class="post clearfix">
                        <div class="entry-header">
                          <div class="post-thumb"> <img class="img-responsive" src="{{ asset('up/'.$famouss[0]->img)}}" alt="{{$famouss[0]->title}}" style="border: 1px solid #4F4D4E"> </div>
                        </div>
                        <div class="entry-content">
                          <p class="h4">{{$famouss[0]->title}}</p>
                            <p class="">{{$famouss[0]->text}}</p>
                          <a class="btn btn-dark btn-theme-colored" target="_BALNK" href="{{$famouss[0]->link}}">read more..</a> </div>
                      </article>
                    </div>
                    @endif
                    <div class="col3">
                      <h4 class="megamenu-col-title">ICU Scoring Calculators:</h4>
                      <img src="{{ asset('assets/images/score.jpg') }}" alt="">
                        <a class="btn btn-dark pull-right" href="{{url('/cme/ICU-Scoring-Calculators/all')}}">GoTo</a>
                      <hr>
                        <h4 class="megamenu-col-title">Drug Interactions Checker:</h4>
                      <img src="{{ asset('assets/images/fda.jpg') }}" alt="">
                        <a class="btn btn-dark" href="{{url('/cme/Drug-Interactions-Checker/all')}}">GoTo</a>
                    </div>
                    <div class="col3">
                      <h4 class="megamenu-col-title">Quick Links:</h4>
                      <ul class="list-dashed list-icon">
                        <li><a href="{{url('/cme/Guidelines/all')}}">Guidelines</a></li>
                        <li><a href="{{url('/cme/Protocols/all')}}">Protocols</a></li>
                        <li><a href="{{url('/youtube')}}">YouTube Channel</a></li>
                      </ul>
                    </div>
                  </div>
                </div>
              </li>
              <li><a href="#">CME</a>
                <ul class="dropdown">
                  <li><a href="{{url('/cme/Residents-Lecture/all')}}">Residents Lectures</a></li>
                  <li><a href="{{url('/cme/Ongoing-Researches/all')}}">Ongoing Researches</a></li>
                  <li><a href="{{url('/cme/ICU-Club-Presentations/all')}}">ICU Club Presentations</a></li>
                  <li><a href="{{url('/member_contribution')}}">Members Contribution</a></li>
                  <li><a href="{{url('/cme/Video/all')}}">Video</a></li>
                  <li><a href="{{url('/cme/ICU-PROCEDURES/all')}}">ICU PROCEDURES</a></li>
                </ul>
              </li>
              <li><a href="{{url('/jobs')}}">Critical Care Jobs</a></li>
              <li><a href="{{url('/ccf')}}">Critical Care Forum</a></li>
              <li><a href="{{url('/icu-conferences')}}">Critical Care Conferences</a></li>

              <li><a href="#">About Us</a>
                <ul class="dropdown">
                  <li><a href="{{url('about')}}"><i class="fa fa-question-circle"></i> Why ICUGate</a>
                  </li>

                  </li>
                  <li><a href="{{url('/contact')}}"><i class="fa fa-envelope"></i> Contact Us</a>
                  </li>

                  @if(!empty(Auth::user()->id))
                    <li><a href="{{ route('certificate') }}"><i class="fa fa-user"></i> My Certificates</a></li>
                  @endif
                </ul>
              </li>

            </ul>
      

          </nav>