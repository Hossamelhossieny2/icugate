<section id="doctors" data-bg-img="{{asset('assets/images/pattern/p3.png')}}">
      <div class="container pb-10">
        <div class="section-title text-center">
          <div class="row">
            <div class="col-md-8 col-md-offset-2">
              <h2 class=" mt-0 line-height-1">Our <span class="text-theme-colored">Team</span></h2>
              <div class="title-icon">
                  <img class="mb-10" src="{{ asset('assets/images/title-icon.png') }}" alt="">
              </div>
              We are trusted group of senior intensivists from all over the world committed to make new changes in critical care ultrasonagraphy
            </div>
          </div>
        </div>
        <div class="row mtli-row-clearfix">
          <div class="col-md-3">
                    <div class="item"  style="border: 1px solid #4FBBE7">
                      <div class="team-members border-bottom-theme-color-2px text-center maxwidth400">
                        <div class="team-thumb">
                          <img  class="img-fullwidth" alt="{{$teams[2]->name}}" title="{{$teams[2]->name}}" src="{{asset('up/'.$teams[2]->img)}}"  width="260" height="320" />
                          <div class="team-overlay"></div>
                        </div>
                        <div class="team-details bg-silver-light pt-10 pb-10">
                          <h4 class="text-uppercase font-weight-600 m-5">{{$teams[2]->name}}</h4>
                          <h6 class="text-theme-colored font-15 font-weight-400 mt-0">{{$teams[2]->jobtitle}}</h6>
                          <ul class="styled-icons icon-theme-colored icon-dark icon-circled icon-sm">
                            <li><a target="_BLANK" href="{{$teams[2]->fb}}"><i class="fa fa-facebook"></i></a></li>
                            <li><a target="_BLANK" href="{{$teams[2]->tw}}"><i class="fa fa-twitter"></i></a></li>
                            <li><a target="_BLANK" href="{{$teams[2]->ins}}"><i class="fa fa-instagram"></i></a></li>
                            <li><a target="_BLANK" href="{{$teams[2]->skype}}"><i class="fa fa-skype"></i></a></li>
                          </ul>
                        </div>
                      </div>
                    </div>
          </div>
          <div class="col-md-9">
            <div class="owl-carousel-3col">
                @foreach($teams as $team)
                @if($team->id != 3)
                    <div class="item" style="border: 1px solid #4FBBE7">
                      <div class="team-members border-bottom-theme-color-2px text-center maxwidth400">
                        <div class="team-thumb">
                          <img class="img-fullwidth" alt="" src="{{ asset('up/'.$team['img']) }}" width="260" height="320">
                          <div class="team-overlay"></div>
                        </div>
                        <div class="team-details bg-silver-light pt-10 pb-10">
                          <h4 class="text-uppercase font-weight-600 m-5">{{$team['name']}}</h4>
                          <h6 class="text-theme-colored font-15 font-weight-400 mt-0">{{$team['jobtitle']}}</h6>
                          <ul class="styled-icons icon-theme-colored icon-dark icon-circled icon-sm">
                            <li><a target="_BLANK" href="{{$team['fb']}}"><i class="fa fa-facebook"></i></a></li>
                            <li><a target="_BLANK" href="{{$team['tw']}}"><i class="fa fa-twitter"></i></a></li>
                            <li><a target="_BLANK" href="{{$team['ins']}}"><i class="fa fa-instagram"></i></a></li>
                            <li><a target="_BLANK" href="{{$team['skype']}}"><i class="fa fa-skype"></i></a></li>
                          </ul>
                        </div>
                      </div>
                    </div>
                @endif
              @endforeach
              
            </div>
          </div>
            
        </div>
      </div>
    </section>