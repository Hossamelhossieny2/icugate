<div class="sidebar sidebar-left mt-sm-30">
  <div class="widget">
    <h5 class="widget-title widget-title-line-bottom line-bottom-theme-colored1">B M I</h5>
    <a href="{{ route('fcalculate.bmi.orm') }}">
    <img src="{{ asset('assets/images/bmi.jpg') }}" alt="" title="" />
    </a>
  </div>
  <div class="widget">
      <a href="{{ route('criticalcare.ultrasound') }}">
      <img src="{{ asset('assets/images/cc_ultrasound.png') }}" alt="Critical Care Ultrasound" title="Critical Care Ultrasound" />
      </a>
  </div>
  <div class="widget">
      <a href="{{ route('criticalcare.educational') }}">
      <img src="{{ asset('assets/images/cc_educational.png') }}" alt="Critical Care Educational Support Professionals" title="Critical Care Educational Support Professionals" />
      </a>
  </div>
  <div class="widget widget_categories">
    <h4 class="widget-title widget-title-line-bottom line-bottom-theme-colored1">New Courses</h4>
    <ul>
      <li class="cat-item"><span class="help-block text-danger text-center">no Courses avaliable right now</span></li>
    </ul>
  </div>
  <div class="widget widget_meta">
    <h4 class="widget-title widget-title-line-bottom line-bottom-theme-colored1">New Events</h4>
    @if(!empty($events))
    <ul>
      @foreach($events as $event)
        <li class="cat-item text-primary">{{ $event['title'] }}</li>
      @endforeach
    </ul>
    <a href="{{ route('critical.care.conferences') }}" class="btn btn-plain-text-with-arrow">Read more</a>
    @else
      <span class="help-block text-danger text-center">no Events avaliable right now</span>
    @endif
  </div>
  <div class="widget">
  <h5 class="widget-title widget-title-line-bottom line-bottom-theme-colored1">Consultation</h5>
    <a href="{{ route('buy.tele',2) }}">
    <img src="{{ asset('assets/images/zoom.jpg') }}" alt="" title="" />
    </a>
  </div>
  <div class="widget">
    <a href="{{ route('buy.tele',1) }}">
    <img src="{{ asset('assets/images/email.jpg') }}" alt="" title="" />
    </a>
  </div>
  <div class="widget">
      <ul>
        <li class="pb-20"><img src="{{ asset('assets/images/youtubechannel.jpg') }}" alt="icugate youtube channel" title="icugate youtube channel" /></li>
        <li><a target="_blank" href="https://www.youtube.com/watch?v=CF21OwIrS_M" class="btn btn-plain-text-with-arrow">icugate history Video</a></li>
        @foreach($yt_ch as $yc)
        <li><a target="_blank" href="https://www.youtube.com/channel/{{ $yc['youtube_id'] }}" class="btn btn-plain-text-with-arrow">{{ substr($yc['title'],0,26) }}</a></li>
        @endforeach
      </ul>
  </div>
  <!-- <div class="widget">
    <h5 class="widget-title widget-title-line-bottom line-bottom-theme-colored1">Youtube Video</h5>
    <iframe width="286" height="205" src="https://www.youtube.com/embed/CF21OwIrS_M" title="ICU HISTORY - Intensive and Critical Care History" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
  </div> -->
</div>