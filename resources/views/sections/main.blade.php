           <div class="row">
              <div class="col-md-12">
                <h4 class="widget-title title-dots"><span>Last Guidelines</span></h4>
              </div>
              <div class="col-xs-12 col-sm-6 col-md-7">
                @for($i=0;$i<2;$i++)
                <article class="post clearfix mb-30 border-1px">
                  <div class="row">
                    <div class="col">
                    <div class="entry-header">
                        <div class="post-thumb thumb">
                          <img src="{{ asset('up/'.$guidelines[$i]['img']) }}" alt="images" class="img-responsive img-fullwidth">
                        </div>
                      </div>
                      <div class="entry-content p-10">
                        <h5 class="entry-title mt-0 mb-0"><a href="#">{{ $guidelines[$i]['title'] }}</a></h5>
                        <div class="entry-meta">
                          <span class="mb-10 text-gray-darkgray mr-10 font-size-13"><i class="far fa-calendar-alt mr-10 text-theme-colored1"></i> {{ $guidelines[$i]['created_at']->format('d M Y') }}</span>
                          <span class="mb-10 text-gray-darkgray mr-10 font-size-13"><i class="far fa-user mr-10 text-theme-colored1"></i> {{$guidelines[$i]['journal']}}</span>
                        </div>
                        <p class="mt-10">{{ $guidelines[$i]['text'] }}</p>
                        <a href="{{ $guidelines[$i]['link'] }}" class="btn btn-plain-text-with-arrow">Read more</a>
                        <div class="clearfix"></div>
                      </div>
                    </div>
                  </div>
                </article>
               @endfor
              </div>
              <div class="col-xs-12 col-sm-6 col-md-5">
                @for($i=2;$i<6;$i++)
                <blockquote class="tm-sc-blockquote blockquote-style3">
                    <p>{{ $guidelines[$i]['title'] }}</p>
                    <footer><cite>more ...</cite></footer>
                  </blockquote>
                @endfor
              </div>
            </div>

            <hr>
            

            <div class="row">
              <div class="col-md-12">
                <h4 class="widget-title title-dots mt-30"><span>Famous Articles</span></h4>
              </div>
              <div class="tagcloud pb-20">
                @foreach($tags as $tag)
                <a class="badge bg-secondary" href="{{ route('login') }}">{{ $tag['title'] }}</a>
                @endforeach
              </div>
              <div class="owl-carousel owl-theme tm-owl-carousel-2col owl-loaded owl-drag" data-nav="false" data-dots="false" data-duration="3000">
              @foreach($tags as $article)
              @if($article['id'] != 11)
              <div class="col-xs-12 col-sm-6 col-md-12">    
                <article class="post clearfix mb-30 border-1px">
                  <div class="row">
                    <div class="col">
                    <div class="entry-header">
                        <div class="post-thumb thumb">
                          <img src="{{ asset('up/'.$article['post'][0]['img']) }}" alt="images" class="img-responsive img-fullwidth">
                        </div>
                      </div>
                      <div class="entry-content p-10">
                        <h5 class="entry-title mt-0 mb-0"><a href="{{ $article['post'][0]['link'] }}">{{ $article['post'][0]['title'] }}</a></h5>
                        <div class="entry-meta">
                          <span class="mb-10 text-gray-darkgray mr-10 font-size-13"><i class="far fa-calendar-alt mr-10 text-theme-colored1"></i> {{ $article['post'][0]['created_at']->format('d M Y') }}</span>
                          <span class="mb-10 text-gray-darkgray mr-10 font-size-13"><i class="far fa-user mr-10 text-theme-colored1"></i> {{$article['post'][0]['journal']}}</span>
                          <span class="badge bg-danger"><i class="far fa-list-alt mr-10 text-white"></i> {{ $article['title'] }}</span>
                        </div>
                        <p class="mt-10">{{ $article['post'][0]['text'] }}</p>
                        <a href="{{ $article['post'][0]['link'] }}" class="btn btn-plain-text-with-arrow">Read more</a>
                        <div class="clearfix"></div>
                      </div>
                    </div>
                  </div>
                </article>
              </div>
              @endif
              @endforeach
            </div>
          </div>
            