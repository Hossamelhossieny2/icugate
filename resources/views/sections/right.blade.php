    <div class="sidebar sidebar-right mt-sm-30">

        <div class="widget">
          <h4 class="widget-title widget-title-line-bottom line-bottom-theme-colored1">article of the month</h4>
          <div class="owl-carousel owl-theme tm-owl-carousel-1col owl-loaded owl-drag" data-nav="false" data-dots="false" data-duration="3000">
            @foreach($montharticles as $montharticle)
            <div class="owl-item">
              <img src="{{ asset('up/'.$montharticle['img']) }}" alt="images" class="img-responsive img-fullwidth">
              <div class="entry-content border-1px p-20 pr-10" style="background:#dedede;min-height:460px;">
                <div class="entry-meta mt-0">
                  <span class="mb-10 text-gray-darkgray mr-10 font-size-13"><i class="far fa-calendar-alt mr-10 text-theme-colored1"></i> {{ \Carbon\Carbon::parse($montharticle['date'])->format('M d, Y') }}</span>
                </div>
                <h4 class="entry-title"><a href="{{ $montharticle['link'] }}">{{ $montharticle['title'] }}</a></h4>
                <p class="mt-10">{{ $montharticle['text'] }}</p>
                <a href="{{ $montharticle['link'] }}" class="btn btn-plain-text-with-arrow">Read more</a>
                <div class="clearfix"></div>
              </div>
            </div>
            @endforeach
          </div>
          
        </div>
        <div class="widget">
          <h4 class="widget-title widget-title-line-bottom line-bottom-theme-colored1">News Update</h4>
          @for($i=0;$i<9;$i+=3)
          <div class="owl-carousel owl-theme tm-owl-carousel-1col owl-loaded owl-drag" data-nav="false" data-dots="false" data-duration="5000">
            
            <div class="owl-item">
              <h5 class="post-title mt-0"><a href="#">{{ $latest[$i]['title'] }}</a></h5>
              <span class="post-date">
                <time class="entry-date" datetime="{{$latest[$i]['date']}}">{{ $latest[$i]['created_at']->format('M d, Y') }}</time>
              </span>
            </div>
            <div class="owl-item">
              <h5 class="post-title mt-0"><a href="#">{{ $latest[$i+1]['title'] }}</a></h5>
              <span class="post-date">
                <time class="entry-date" datetime="{{$latest[$i+1]['date']}}">{{ $latest[$i+1]['created_at']->format('M d, Y') }}</time>
              </span>
            </div>
            <div class="owl-item">
              <h5 class="post-title mt-0"><a href="#">{{ $latest[$i+2]['title'] }}</a></h5>
              <span class="post-date">
                <time class="entry-date" datetime="{{$latest[$i+2]['date']}}">{{ $latest[$i+2]['created_at']->format('M d, Y') }}</time>
              </span>
            </div>
            
          </div>
          @endfor
        </div>

        <div class="widget widget_meta">
          <h4 class="widget-title widget-title-line-bottom line-bottom-theme-colored1">Categories</h4>
          <ul>
            @foreach($cats as $cat)
            <li><a href="{{ route('login') }}">{{ $cat['title'] }}</a> <span class="badge bg-secondary">{{ $cat['post_count'] }}</span></li>
            @endforeach
          </ul>
        </div>

        <div class="widget">
          <h4 class="widget-title widget-title-line-bottom line-bottom-theme-colored1">Tags</h4>
          <div class="tagcloud">
            @foreach($tags as $tag)
            <a class="badge bg-primary" href="{{ route('login') }}">{{ $tag['title'] }}</a>
            @endforeach
          </div>
        </div>

    </div>
