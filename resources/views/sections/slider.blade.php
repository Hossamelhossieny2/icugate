<div class="row">
  <div class="col-md-12">
    <div class="tm-owl-carousel-3col" data-nav="true" data-margin="5">
      @foreach($hot_topics as $news)
      <div class="item font-9">
        <div class="featured-news">
          <img src="{{ asset('up/'.$news['img']) }}" alt="images">
          <div class="featured-news-details">
            <h4 class="text-white mb-5">{{ substr($news['title'],0,80) }} ... -</h4>
            <p class="text-white"><a class="badge bg-secondary">hot topic</a><br> {{ $news['text'] }}</p>
            <a href="{{ $news['link'] }}" class="btn btn-plain-text-with-arrow text-white">Read more</a>
          </div>
        </div>
      </div>
      @endforeach
    </div>
  </div>
</div>