@extends('layouts.app')

@section('content')
    <section>
        <div class="container mt-30 mb-30 pt-30 pb-30">
            <div class="row">
                <div class="col-md-12">
                    <div class="blog-posts single-post">
                        <article class="post clearfix mb-0">
                            <div class="entry-content">
                                <div class="entry-meta media no-bg no-border mt-15 pb-20">
                                    <div class="entry-date media-left text-center flip bg-theme-colored2 pt-5 pr-15 pb-5 pl-15">
                                        <ul>
                                            <li class="font-16 text-white font-weight-600">🔒</li>
                                        </ul>
                                    </div>
                                    <div class="media-body pl-15">
                                        <div class="event-content flip">
                                            <h3 class="entry-title text-white text-uppercase pt-0 mt-0 text-center"><a href="#">{{$thisTele['name']}} Teleconsultation Fees</a></h3>
                                        </div>
                                    </div>
                                    <div class="entry-date media-left text-center flip bg-theme-colored2 pt-5 pr-15 pb-5 pl-15">
                                        <ul>
                                            <li class="font-16 text-white font-weight-600">🔒</li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </article>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section>
        <div class="container">
            <div class="row">
                <div class="col-md-6 col-md-push-3">
                    <h4 class="mt-0 pt-5"> Paypal Order Form </h4>
                    <img src="{{ asset('assets/images/payment-card-logo-sm.png') }}" alt="">
                    <hr>
                    <h5>Teleconsultation - By {{$thisTele['name']}}</h5>
                    <h5>TIME ( {{$thisTele['created_at']}} )</h5>
                    <p>Price ${{$thisTele['price']}} (Qty 1)</p>
                    <div id="paypal-button-container3" data-id="{{ $thisTele['id'] }}" data-order="" data-url="{{ $thisTele->getAfterPaymentRoute() }}" data-route="{{ $thisTele->getPaymentRoute() }}"></div>
                </div>
            </div>
        </div>
    </section>
    @push('scripts')
        <script>
            const payPalContainerTele = $("#paypal-button-container3");
            if (payPalContainerTele.length) {
                var teleConsultationId = payPalContainerTele.data('id');
                var paymentRoute       = payPalContainerTele.data('route');
                paypal.Buttons({
                    createOrder: function (data, actions) {
                        return fetch(paymentRoute, {
                            method: 'POST'
                        }).then(function(res) {
                            console.log(res)
                            Swal.fire({
                                icon: 'warning',
                                title: 'Wait',
                                text: 'Please Wait while the system will redirect you in a moment',
                            })
                            return res.json();
                        }).then(function(data) {
                            payPalContainerTele.data('order',data.id);
                            Swal.fire({
                                icon: 'warning',
                                title: 'Wait',
                                text: 'Please Wait while the system will redirect you in a moment',
                            })
                            return data.id;
                        });
                    },
                    onApprove: function (data, actions) {
                        var orderId = payPalContainerTele.data('order');
                        return fetch(APP_URL + '/api/paypal/consultation/catch-order/' + orderId, {
                            method: 'POST'
                        }).then(function(res) {
                            location.href = payPalContainerTele.data('url')
                        }).then(function(data) {
                            location.href = payPalContainerTele.data('url');
                        });
                    }
                }).render('#paypal-button-container3');
            }
        </script>
    @endpush
@endsection
