@extends('layouts.app')

@section('content')
    
<!-- Start main-content -->
  <div class="main-content-area">
    <!-- Section: sliders -->
    <section id="home" class="divider">
      <div class="container-fluid p-0">
      @include('sections.slider')
      </div>
    </section>

    <section>
      <div class="container mt-30 mb-30 pt-30 pb-30">
        <div class="row">

          <!-- Section: main section -->
          <div class="col-md-9 order-lg-2">
            <div class="col-md-12">
              @include('sections.tags')
            </div>

            @include('sections.main_one_cat')
          </div>

          <!-- Section: left panet -->
          <div class="col-md-3 order-lg-1">
            @include('sections.left')
          </div>
          
        </div>
      </div>
    </section>
    
  </div>
  <!-- end main-content -->
  
@endsection
