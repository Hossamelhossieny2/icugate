@extends('layouts.app')

@section('content')

@push('styles')
  <script src="{{ asset('assets/js/odometer/odometer.min.js') }}"></script>
  <link href="{{ asset('assets/js/odometer/themes/odometer-theme-car.css') }}" rel="stylesheet"/>
    <style type="text/css">
      .odometer-theme-car .odometer-inside .odometer-digit {
          font-size: 3em;
      }
    </style>
@endpush

<section>
      <div class="container">
        <div class="row">
         
          <div class="col-md-6" style="border-right: 1px dashed #53bdff">
            <div class="row">
              
              <div class="col-md-6">
                <div class="odometer-theme-car bmi font-64">0</div>
              </div>
            <!-- <script>
            var odometer = new Odometer({ el: $('.odometer-theme-car')[0], value: 123, theme: 'car' });
            odometer.update({{$bmi}});
            odometer.render();
            </script> -->
              <script>
                (function($) {
                  setTimeout(function(){
                    var $select = $('.odometer-theme-car');
                    if( $select.length > 0 ) {
                      var el = document.querySelector('.bmi');
                      od = new Odometer({
                        auto: false,
                        el: el,
                        value: 11.11,
                        theme: 'car'
                      });
                      od.update({{$bmi}});
                    }
                  }, 1000);
                })(jQuery);
              </script>
              <div class="col-md-6">
                <h3><i class="fa fa-arrow-right"></i> BMI</h3>
                <p>Body Mass Index</p>
              </div>
            </div>
            <div class="separator" style="margin:0">
              <i class="fa fa-cog fa-spin"></i>
            </div>
            <div class="row">
              
              <div class="col-md-6">
                <div class="odometer-theme-car ibw font-64">0</div>
              </div>
          
             <script>
                (function($) {
                  setTimeout(function(){
                    var $select = $('.odometer-theme-car');
                    if( $select.length > 0 ) {
                      var el = document.querySelector('.ibw');
                      od = new Odometer({
                        el: el,
                        value: 11.11,
                        theme: 'car'
                      });
                      od.update({{$ibw}});
                    }
                  }, 1000);
                  od.render();
                })(jQuery);
              </script>
              <div class="col-md-6">
                <h3><i class="fa fa-arrow-right"></i> IBW</h3>
                <p>Ideal Body Weight</p>
              </div>
            </div>
            <div class="separator" style="margin:0">
              <i class="fa fa-cog fa-spin"></i>
            </div>
            <div class="row">
              
              <div class="col-md-6">
                <div class="odometer-theme-car abw font-64">0</div>
              </div>
          
             <script>
                (function($) {
                  setTimeout(function(){
                    var $select = $('.odometer-theme-car');
                    if( $select.length > 0 ) {
                      var el = document.querySelector('.abw');
                      od = new Odometer({
                        el: el,
                        value: 11.11,
                        theme: 'car'
                      });
                      od.update({{$abw}});
                    }
                  }, 1000);
                  od.render();
                })(jQuery);
              </script>
              <div class="col-md-6">
                <h3><i class="fa fa-arrow-right"></i> ABW</h3>
                <p>Adjusted Body Weight</p>
              </div>
            </div>
            <div class="separator" style="margin:0">
              <i class="fa fa-cog fa-spin"></i>
            </div>
            <div class="row">
              
              <div class="col-md-6">
                <div class="odometer-theme-car tcr font-64">0</div>
              </div>
          
             <script>
                (function($) {
                  setTimeout(function(){
                    var $select = $('.odometer-theme-car');
                    if( $select.length > 0 ) {
                      var el = document.querySelector('.tcr');
                      od = new Odometer({
                        el: el,
                        value: 11.11,
                        theme: 'car'
                      });
                      od.update({{$tcr}});
                    }
                  }, 1000);
                  od.render();
                })(jQuery);
              </script>
              <div class="col-md-6">
                <h3><i class="fa fa-arrow-right"></i> TCR</h3>
                <p>Total Caloric Requirement</p>
              </div>
            </div>
            <div class="separator" style="margin:0">
              <i class="fa fa-cog fa-spin"></i>
            </div>
            <div class="row">
              
              <div class="col-md-6">
                <div class="odometer-theme-car tpr font-64">0</div>
              </div>
          
             <script>
                (function($) {
                  setTimeout(function(){
                    var $select = $('.odometer-theme-car');
                    if( $select.length > 0 ) {
                      var el = document.querySelector('.tpr');
                      od = new Odometer({
                        el: el,
                        value: 11.11,
                        theme: 'car'
                      });
                      od.update({{$tpr}});
                    }
                  }, 1000);
                  od.render();
                })(jQuery);
              </script>
              <div class="col-md-6">
                <h3><i class="fa fa-arrow-right"></i> TPR</h3>
                <p>Total Protein Requirement</p>
              </div>
            </div>
            @if($sol != 0)
            <div class="separator" style="margin:0">
              <i class="fa fa-cog fa-spin"></i>
            </div>
            <div class="row">
              
              <div class="col-md-6">
                <div class="odometer-theme-car sol font-64">0</div>
              </div>
          
             <script>
                (function($) {
                  setTimeout(function(){
                    var $select = $('.odometer-theme-car');
                    if( $select.length > 0 ) {
                      var el = document.querySelector('.sol');
                      od = new Odometer({
                        el: el,
                        value: 11.11,
                        theme: 'car'
                      });
                      od.update({{$sol}});
                    }
                  }, 1000);
                  od.render();
                })(jQuery);
              </script>
              <div class="col-md-6">
                <h3><i class="fa fa-hand-o-right"></i> soulation</h3>
                <p>Calculation of Solution Osmolarity</p>
              </div>
            </div>
            @endif
          </div>
          
          <div class="col-md-6">
               
                @if($bmi<18.5)
                  <button type="button" class="btn btn-warning"><strong>person is underweight </strong></button>
                  <hr>
                  <div class="tm-sc-progress-bar progress-bar-default" data-percent="15" data-bar-height="22">
                    <div class="progress-holder bg-theme-colored4">
                      <div class="progress-content bg-theme-colored1">
                        <div class="progress-title-holder text-right">
                          <span class="percent"><span class="symbol-left"></span><span class="value">15</span><span class="symbol-right">%</span></span>
                        </div>
                      </div>
                    </div>
                  </div>
                  <ul class="list theme-colored angle-double-right h3">
                    <li style="padding-bottom: 20px;">This BMI is considered underweight.</li>
                    <li style="padding-bottom: 20px;">Being underweight may pose certain health risks, including nutrient deficiencies and hormonal changes.</li>
                    <li style="padding-bottom: 20px;">Waist-to-hip ratio, waist-to-height ratio, and body fat percentage measurements can provide a more complete picture of any health risks.</li>
                    <li style="padding-bottom: 20px;">A person should consult with their healthcare provider and consider making lifestyle changes through healthy eating and fitness to improve their health indicators.</li>
                    <li style="padding-bottom: 20px;">For this height {{$hgt}} cm, a weight range of {{$ibw}} kg is a normal BMI.</li>
                  </ul>
                  
                @elseif($bmi>=18.5 && $bmi<24.9)
                <button type="button" class="btn btn-success"><strong>person is ideal </strong></button>
                  <hr>
                  <div class="tm-sc-progress-bar progress-bar-default" data-percent="85" data-bar-height="22">
                    <div class="progress-holder bg-theme-colored4">
                      <div class="progress-content bg-theme-colored1">
                        <div class="progress-title-holder text-right">
                          <span class="percent"><span class="symbol-left"></span><span class="value">85</span><span class="symbol-right">%</span></span>
                        </div>
                      </div>
                    </div>
                  </div>
                  
                  <ul class="list theme-colored angle-double-right h3">
                    <li style="padding-bottom: 20px;">This BMI is considered normal. </li>
                    <li style="padding-bottom: 20px;">Maintaining a healthy weight may lower the risk of developing certain health conditions, including cardiovascular disease and type 2 diabetes.</li>
                    <li style="padding-bottom: 20px;"> Waist-to-hip ratio, waist-to-height ratio, and body fat percentage measurements can provide a more complete picture of any health risks.</li>
                    <li style="padding-bottom: 20px;">For this height {{$hgt}} cm, a weight range of {{$ibw}} kg is a normal BMI.</li>
                  </ul>
                @elseif($bmi>=25 && $bmi<29.9)
                  <button type="button" class="btn btn-warning"><strong>person is overweight </strong></button>
                  <hr>
                  <div class="tm-sc-progress-bar progress-bar-default" data-percent="35" data-bar-height="22">
                    <div class="progress-holder bg-theme-colored4">
                      <div class="progress-content bg-theme-colored1">
                        <div class="progress-title-holder text-right">
                          <span class="percent"><span class="symbol-left"></span><span class="value">35</span><span class="symbol-right">%</span></span>
                        </div>
                      </div>
                    </div>
                  </div>
                  <ul class="list theme-colored angle-double-right h3">
                    <li style="padding-bottom: 20px;">This BMI is considered overweight.</li>
                    <li style="padding-bottom: 20px;">Being overweight may increase the risk of certain health conditions, including cardiovascular disease, high blood pressure, and type 2 diabetes.</li>
                    <li style="padding-bottom: 20px;">Waist-to-hip ratio, waist-to-height ratio, and body fat percentage measurements can provide a more complete picture of any health risks.</li>
                    <li style="padding-bottom: 20px;">A person should consult with their healthcare provider and consider making lifestyle changes through healthy eating and fitness to improve their health indicators.</li>
                    <li style="padding-bottom: 20px;">For this height {{$hgt}} cm, a weight range of {{$ibw}} kg is a normal BMI.</li>
                  </ul>
                  
                @elseif($bmi>=30)
                  <button type="button" class="btn btn-danger"><strong>obese person </strong></button>
                  <hr>
                 <div class="tm-sc-progress-bar progress-bar-default" data-percent="15" data-bar-height="22">
                    <div class="progress-holder bg-theme-colored4">
                      <div class="progress-content bg-theme-colored1">
                        <div class="progress-title-holder text-right">
                          <span class="percent"><span class="symbol-left"></span><span class="value">15</span><span class="symbol-right">%</span></span>
                        </div>
                      </div>
                    </div>
                  </div>
                  <ul class="list theme-colored angle-double-right h3">
                    <li style="padding-bottom: 20px;">This BMI is considered obese.</li>
                    <li style="padding-bottom: 20px;">People with obesity have increased risk of cardiovascular disease, type 2 diabetes, high blood pressure, and other health conditions.</li>
                    <li style="padding-bottom: 20px;">Waist-to-hip ratio, waist-to-height ratio, and body fat percentage measurements can provide a more complete picture of any health risks.</li>
                    <li style="padding-bottom: 20px;">A person should consult with their healthcare provider and consider making lifestyle changes through healthy eating and fitness to improve their health indicators.</li>
                    <li style="padding-bottom: 20px;">For this height {{$hgt}} cm, a weight range of {{$ibw}} kg is a normal BMI.</li>
                  </ul>
                  
                @endif
      </div>
    </section>

@endsection
