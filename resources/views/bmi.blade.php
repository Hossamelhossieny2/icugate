@extends('layouts.app')

@section('content')

<section class="page-title layer-overlay overlay-dark-9 section-typo-light bg-img-center" data-tm-bg-img="{{ asset('assets/images/bg/bg9.jpg') }}">
      <div class="container pt-50 pb-50">
        <div class="section-content">
          <div class="row">
            <div class="col-md-12 text-center">
              <h2 class="title">BMI calculator</h2>
            </div>
          </div>
        </div>
      </div>
    </section>

<section  data-bg-img="{{url('/')}}/public/assets/images/pattern/p1.png" class="pb-50">
      <div class="container">
        <div class="row">
          <div class="col-md-6" style="border-right: 1px dashed #53bdff">
              <section id="">
                <h2>What is the body mass index (BMI)?</h2>
                  <p>The body mass index&nbsp;(BMI) is a measure that uses your height and weight to work out if your weight is healthy.</p>
              <p>The BMI calculation divides&nbsp;an adult's weight in kilograms by their height in metres squared. For example, A BMI of 25 means 25kg/m2.</p>
                
              </section>

              <section id="">
                  <h2>BMI ranges</h2>
              <p>For most adults, an ideal BMI is in the 18.5 to 24.9 range. </p>
              <p>For children and young people aged&nbsp;2 to 18, the BMI&nbsp;calculation takes into account age and gender as well as height and weight.</p>
              <p>If your BMI is:</p>
              <ul>
              <li>below 18.5&nbsp;–&nbsp;you're in the&nbsp;underweight range </li>
              <li>between 18.5 and 24.9&nbsp;–&nbsp;you're in the healthy weight range </li>
              <li>between 25 and 29.9&nbsp;–&nbsp;you're in the&nbsp;overweight range </li>
              <li>between 30 and 39.9&nbsp;–&nbsp;you're in the&nbsp;obese range&nbsp; </li>
              </ul>
              </section>

              <section id="">
                  <h2>Accuracy of BMI</h2>
              <p>BMI takes into account natural variations in body shape, giving a healthy weight range for a particular height.</p>
              <p>As well as measuring your BMI, healthcare professionals may take other factors into account when assessing if you're a healthy weight.</p>
              <p>Muscle is much denser than fat, so very muscular people, such as heavyweight boxers, weight trainers and athletes, may be a healthy weight even though their BMI is classed as obese.</p>
              <p>Your ethnic group can also affect your risk of some health conditions. For example, adults of Asian origin may have a higher risk of health problems at BMI levels below 25.</p>
              <p>You should not use BMI as a measure if you're pregnant. Get advice from your midwife or GP if you're concerned about your weight.</p>
              </section>
          </div>


          <div class="col-md-4 col-md-push-1">
            <form action="{{ route('calculate.bmi.submit') }}" method="POST">
        @csrf
        <div class="form-group">
            <label for="exampleFormControlSelect2">Gender *</label>
            <select class="form-control" id="exampleFormControlSelect2" name="gender">
                <option value="male">Male</option>
                <option value="female">Female</option>
            </select>
        </div>
        <div class="form-group">
            <label for="height">Height *</label>
            <input type="number" class="form-control" name="height" id="weight" placeholder="177.5 Cm" required="">
        </div>
        <div class="form-group">
            <label for="weight">Weight *</label>
            <input type="number" class="form-control" name="weight" id="weight" placeholder="90 Kg" required="">
        </div>
        <div class="form-group">
            <label for="weight">Grams of Dextrose</label>
            <input type="number" class="form-control" name="gm_dextrose" placeholder="90 G">
        </div>
        <div class="form-group">
            <label for="weight">Grams of Protein</label>
            <input type="number" class="form-control" name="gm_protein" placeholder="40 G">
        </div>
        <div class="action">
            <button class="pull-right" type="submit">calculate</button>
              </div>
            </form>
          </div>

        </div>
      </div>
    </section>

@endsection
