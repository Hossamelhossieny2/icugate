<!DOCTYPE html>
<html dir="ltr" lang="en">
<head>

<!-- Meta Tags -->
<meta name="viewport" content="width=device-width,initial-scale=1.0"/>
<meta http-equiv="content-type" content="text/html; charset=UTF-8"/>
<meta name="description" content="{{ $conf['site_des'] }}"/>
<meta name="keywords" content="{{ $conf['site_kw'] }}"/>
<meta name="author" content="triangle4it"/>

<!-- Page Title -->
<title>{{ config('app.name', 'Laravel') }} | Live 24 Medical News</title>

<!-- Favicon and Touch Icons -->
<link href="{{ asset('assets/images/favicon.png') }}" rel="shortcut icon" type="image/png">
<link href="{{ asset('assets/images/apple-touch-icon.png') }}" rel="apple-touch-icon">
<link href="{{ asset('assets/images/apple-touch-icon-72x72.png') }}" rel="apple-touch-icon" sizes="72x72">
<link href="{{ asset('assets/images/apple-touch-icon-114x114.png') }}" rel="apple-touch-icon" sizes="114x114">
<link href="{{ asset('assets/images/apple-touch-icon-144x144.png') }}" rel="apple-touch-icon" sizes="144x144">

<!-- Stylesheet -->
<link href="{{ asset('assets/css/bootstrap.min.css') }}" rel="stylesheet" type="text/css">
<link href="{{ asset('assets/css/animate.min.css') }}" rel="stylesheet" type="text/css">
<link href="{{ asset('assets/css/javascript-plugins-bundle.css') }}" rel="stylesheet"/>
<!-- <link href="{{ asset('assets/css/css-plugin-collections.css') }}" rel="stylesheet"/> -->

<!-- CSS | menuzord megamenu skins -->
<link href="{{ asset('assets/js/menuzord/css/menuzord.css') }}" rel="stylesheet"/>

<!-- CSS | Main style file -->
<link href="{{ asset('assets/css/style-main.css') }}" rel="stylesheet" type="text/css">
<link id="menuzord-menu-skins" href="{{ asset('assets/css/menuzord-skins/menuzord-rounded-boxed.css') }}" rel="stylesheet"/>

<!-- CSS | Responsive media queries -->
<link href="{{ asset('assets/css/responsive.css') }}" rel="stylesheet" type="text/css">
<!-- CSS | Style css. This is the file where you can place your own custom css code. Just uncomment it and use it. -->

<!-- CSS | Theme Color -->
<link href="{{ asset('assets/css/colors/theme-skin-color-set1.css') }}" rel="stylesheet" type="text/css">

<!-- external javascripts -->
<script src="{{ asset('assets/js/jquery.js') }}"></script>
<script src="{{ asset('assets/js/popper.min.js') }}"></script>
<script src="{{ asset('assets/js/bootstrap.min.js') }}"></script>
<script src="{{ asset('assets/js/javascript-plugins-bundle.js') }}"></script>
<script src="{{ asset('assets/js/menuzord/js/menuzord.js') }}"></script>
<!-- <script src="{{ asset('assets/js/jquery-plugin-collection.js') }}"></script> -->
<script src="https://www.paypal.com/sdk/js?client-id=ATIXWmueNSzaPnysgY7-gxta-ay2zfDnMTGJ_X7VQhHIQoWGEXM6omE6cZZrZSyoVXu65-x1g0BcNTTj&currency=USD"></script>
<!-- <link href="css/style.css" rel="stylesheet" type="text/css"> -->

@livewireStyles
<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
<![endif]-->
</head>
<body class="tm-container-1300px has-side-panel side-panel-right">
<!-- preloader -->
<div id="preloader">
  <div id="spinner">
    <img src="{{ asset('assets/images/preloaders/123.gif') }}" alt="">
  </div>
  <div id="disable-preloader" class="btn btn-default btn-sm">Disable Preloader</div>
</div>
<div class="side-panel-body-overlay"></div>

<div id="side-panel-container" class="dark" data-tm-bg-img="{{ asset('assets/images/side-push-bg.jpg') }}">
  <div class="side-panel-wrap">
    <div id="side-panel-trigger-close" class="side-panel-trigger"><a href="#"><i class="fa fa-times side-panel-trigger-icon"></i></a></div>
    <img class="logo mb-50" src="{{ asset('assets/images/logo-wide.png') }}" alt="Logo">
    <p>{{ $conf['site_des'] }}</p>
    <div class="widget">
      <h4 class="widget-title widget-title-line-bottom line-bottom-theme-colored1">Latest News</h4>
      <div class="latest-posts">
      @if(!empty($latest[0]['link']))
        @for($i=0;$i<2;$i++)
        <article class="post clearfix pb-0 mb-10">
          <a class="post-thumb" href="{{ $latest[$i]['link']}}"><img src="{{ asset('up/QQQ'. $latest[$i]['img']) }}" alt="{{ $latest[$i]['title']}}" title="{{ $latest[$i]['title']}}"></a>
          <div class="post-right">
            <h5 class="post-title mt-0"><a href="{{ $latest[$i]['link']}}">{{ $latest[$i]['title']}}</a></h5>
            <a href="{{ $latest[$i]['link']}}">read more .....</a>
          </div>
        </article>
        @endfor
      @else
        <h5 class="post-title mt-0 text-danger">Not Avaliable for this category</h5>
      @endif
      </div>
    </div>

    <div class="widget">
      <h5 class="widget-title widget-title-line-bottom line-bottom-theme-colored1">Contact Info</h5>
      <div class="tm-widget-contact-info contact-info-style1 contact-icon-theme-colored1">
        <ul>
          <li class="contact-name">
            <div class="icon"><i class="flaticon-contact-037-address"></i></div>
            <div class="text">{{ $conf['home_job_title'] }}</div>
          </li>
          <li class="contact-phone">
            <div class="icon"><i class="flaticon-contact-042-phone-1"></i></div>
            <div class="text"><a href="tel:{{ $conf['phone'] }}">{{ $conf['phone'] }}</a></div>
          </li>
          <li class="contact-email">
            <div class="icon"><i class="flaticon-contact-043-email-1"></i></div>
            <div class="text"><a href="mailto:{{ $conf['site_email'] }}">{{ $conf['site_email'] }}</a></div>
          </li>
          <li class="contact-address">
            <div class="icon"><i class="flaticon-contact-047-location"></i></div>
            <div class="text">{{ $conf['address'] }}</div>
          </li>
        </ul>
      </div>
    </div>
  </div>
</div>
<div id="wrapper" class="clearfix">
  <!-- Header -->
  <header id="header" class="header header-layout-type-header-2rows">
    <div class="header-top">
      <div class="container">
        <div class="row">
          <div class="col-xl-auto header-top-left align-self-center text-center text-xl-start">
            <ul class="element contact-info">
              <li class="contact-phone"><i class="fa fa-phone font-icon sm-display-block"></i> Tel: {{ $conf['phone'] }}</li>
              <li class="contact-email"><i class="fa fa-envelope font-icon sm-display-block"></i> {{ $conf['site_email'] }}</li>
            </ul>
          </div>
          <div class="col-xl-auto ms-xl-auto header-top-right align-self-center text-center text-xl-end">
            <div class="element pt-0 pb-0">
              <ul class="styled-icons icon-dark icon-theme-colored1 icon-circled clearfix">
                <li><a class="social-link" target="_blank" href="{{ $conf['facebook'] }}" ><i class="fab fa-facebook"></i></a></li>
                <li><a class="social-link" target="_blank" href="{{ $conf['twitter'] }}" ><i class="fab fa-twitter"></i></a></li>
                <li><a class="social-link" target="_blank" href="{{ $conf['instgram'] }}" ><i class="fab fa-instagram"></i></a></li>
                <li><a class="social-link" target="_blank" href="{{ $conf['youtube'] }}" ><i class="fab fa-youtube"></i></a></li>
                <li><a class="social-link" target="_blank" href="{{ $conf['android'] }}" ><i class="fab fa-android"></i></a></li>
                <li><a class="social-link" target="_blank" href="{{ $conf['apple'] }}" ><i class="fab fa-apple"></i></a></li>
              </ul>
            </div>
            <div class="element pt-0 pt-lg-10 pb-0">
              @auth()
              <a class="btn btn-theme-colored1 text-white btn-sm" href="{{ route('logout',app()->getLocale()) }}" onclick="event.preventDefault(); document.getElementById('frm-logout').submit();">
              <i class="la la-sign-out"></i>{{__('logout')}}</a>
                  <form id="frm-logout" action="{{ route('logout',app()->getLocale()) }}" method="POST" style="display: none;">
                  @csrf
                </form>
              @else
                <a href="{{ route('login') }}" class="btn btn-theme-colored1 text-white btn-sm">Login</a> | 
                <a href="{{ route('register') }}" class="btn btn-theme-colored1 text-white btn-sm">Register</a>
              @endif
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="header-nav tm-enable-navbar-hide-on-scroll">
      <div class="header-nav-wrapper navbar-scrolltofixed">
        <div class="menuzord-container header-nav-container">
          <div class="container position-relative">
            <div class="row header-nav-col-row">
              <div class="col-sm-auto align-self-center">
                <a class="menuzord-brand site-brand" href="#">
                  <img class="logo-default logo-1x" src="{{ asset('assets/images/logo-wide.png') }}" alt="Logo">
                  <img class="logo-default logo-2x retina" src="{{ asset('assets/images/logo-wide@2x.png') }}" alt="Logo">
                </a>
              </div>
              <div class="col-sm-auto ms-auto pr-0 align-self-center">
                <nav id="top-primary-nav" class="menuzord theme-color2" data-effect="fade" data-animation="none" data-align="right">
                  <ul id="main-nav" class="menuzord-menu">
                    <li class="@if($menu == 'home') active @endif menu-item">
                      <a href="{{ route('home') }}">Home</a>
                      
                    </li>
                    
                    <li class="@if($menu == 'article') active @endif menu-item"><a href="#">Article</a>
                      <ul class="dropdown">
                      @auth()
                       <?php if(!isset($usertype)) {
                          $usertype = 'free';
                    } ?>
                        <li><a href="{{ route('article.view.'.$usertype,[12,1]) }}">hot Topics</a></li>
                        <li><a href="#">by <span class="badge bg-success">Category</span></a>
                              <ul class="dropdown">
                                @foreach($cats as $cat)
                                <li><a href="{{ route('article.view.'.$usertype,[$cat['id'],1]) }}">{{$cat['title']}}</a></li>
                                @endforeach
                              </ul>
                            </li>
                            <li><a href="#">by <span class="badge bg-danger">Tag</span></a>
                              <ul class="dropdown">
                                @foreach($tags as $tag)
                                <li><a href="{{ route('article.view.'.$usertype,[8,$tag['id']]) }}">{{$tag['title']}}</a></li>
                                @endforeach
                              </ul>
                            </li>
                        <li><a href="{{ route('article.view.'.$usertype,[22,1]) }}">our archive</a></li>
                        @else
                         <li><a href="{{ route('login') }}">hot Topics</a></li>
                        <li><a href="#">by  <span class="badge bg-danger">Category</span></a>
                              <ul class="dropdown">
                                @foreach($cats as $cat)
                                <li><a href="{{ route('login') }}">{{$cat['title']}}</a></li>
                                @endforeach
                              </ul>
                            </li>
                            <li><a href="#">by  <span class="badge bg-danger">Tag</span></a>
                              <ul class="dropdown">
                                @foreach($tags as $tag)
                                <li><a href="{{ route('login') }}">{{$tag['title']}}</a></li>
                                @endforeach
                              </ul>
                            </li>
                        <li><a href="{{ route('login') }}">our archive</a></li>
                        @endauth
                      </ul>
                    </li>
                  @auth()
                   
                     
                    <li class="@if($menu == 'cme') active @endif menu-item"><a href="#">C M E</a>
                      <ul class="dropdown">
                        <li><a href="{{ route('premuim',[15,6]) }}">Resident Lectures</a></li>
                        <li><a href="{{ route('premuim',[16,5]) }}">ICU Club Presentations</a></li>
                        <li><a href="{{ route('cme.'.$usertype,[20,4]) }}">Ongoing Researches</a></li>
                        <li><a href="{{ route('cme.'.$usertype,[17,1]) }}">Members Contribution</a></li>
                        <li><a href="{{ route('cme.'.$usertype,[19,1]) }}">ICU PROCEDURES</a></li>
                        <li><a href="{{ route('cme.'.$usertype,[9,1]) }}">Guidelines</a></li>
                        <li><a href="{{ route('cme.'.$usertype,[18,1]) }}">Video</a></li>
                      </ul>
                    </li>
                    <li class="@if($menu == 'critical') active @endif menu-item"><a href="#">Critical Care</a>
                      <ul class="dropdown">
                        <li><a href="{{ route('critical.care.conferences') }}">Critical Care Conferences</a></li>
                        <li><a href="#">Critical Forum </a></li>
                        <li><a href="{{ route('criticalcare.educational') }}">Critical Care Educational Support Professionals</a></li>
                        <li><a href="{{ route('criticalcare.ultrasound') }}">Critical Care Ultrasound</a></li>
                      </ul>
                    </li>
                    <li class="@if($menu == 'premuim') active @endif menu-item"><a href="#">Premuim</a>
                      <ul class="dropdown">
                        <li><a href="{{ route('cme.'.$usertype,[13,1]) }}">ICU score calculator</a></li>
                        <li><a href="{{ route('cme.'.$usertype,[14,11]) }}">Drug Interactions Checker</a></li>
                        <li><a href="{{ route('fcalculate.bmi.orm') }}">BMI calculator</a></li>
                      </ul>
                    </li>
                  @else
                    <li class="menu-item"><a href="#">C M E</a>
                      <ul class="dropdown">
                        <li><a href="{{ route('login') }}">Residents Lectures</a></li>
                        <li><a href="{{ route('login') }}">ICU Club Presentations</a></li>
                        <li><a href="{{ route('login') }}">Ongoing Researches</a></li>
                        <li><a href="{{ route('login') }}">Members Contribution</a></li>
                        <li><a href="{{ route('login') }}">ICU PROCEDURES</a></li>
                        <li><a href="{{ route('login') }}">Guidelines</a></li>
                        <li><a href="{{ route('login') }}">Video</a></li>
                      </ul>
                    </li>
                    <li class="menu-item"><a href="#">Critical Care</a>
                      <ul class="dropdown">
                        <li><a href="{{ route('login') }}">Critical Care Conferences</a></li>
                        <li><a href="{{ route('login') }}">Critical Forum </a></li>
                        <li><a href="{{ route('login') }}">Critical Care Educational Support Professionals</a></li>
                        <li><a href="{{ route('login') }}">Critical Care Ultrasound</a></li>
                      </ul>
                    </li>
                    <li class="menu-item"><a href="#">Premuim</a>
                      <ul class="dropdown">
                        <li><a href="{{ route('login') }}">ICU score calculator</a></li>
                        <li><a href="{{ route('login') }}">Drug Interactions Checker</a></li>
                        <li><a href="{{ route('login') }}">BMI calculator</a></li>
                      </ul>
                    </li>
                  @endauth
                    <li class="@if($menu == 'icugate') active @endif menu-item"><a href="#">ICUGATE</a>
                      <ul class="dropdown">
                        <li><a href="#">Our Courses</a></li>
                        <li><a href="{{ route('icugate_jobs') }}">Our Jobs</a></li>
                        <li><a href="{{ route('icugate_pricing') }}">Our pricing</a></li>
                        <li><a href="{{ route('about_icugate') }}">about icugate</a></li>
                        <li><a href="{{ route('contact_icugate') }}">Contact our Professionals</a></li>
                      </ul>
                    </li>
                    
                  </ul>
                </nav>
              </div>
              <div class="col-sm-auto align-self-center nav-side-icon-parent">
                <ul class="list-inline nav-side-icon-list">
                  <li class="hidden-mobile-mode">
                    <div id="side-panel-trigger" class="side-panel-trigger">
                      <a href="#">
                        <div class="hamburger-box">
                          <div class="hamburger-inner"></div>
                        </div>
                        </a>
                    </div>
                  </li>
                </ul>
                <div id="top-nav-search-form" class="clearfix">
                  <form action="#" method="GET">
                    <input type="text" name="s" value="" placeholder="Type and Press Enter..." autocomplete="off" />
                  </form>
                  <a href="#" id="close-search-btn"><i class="fa fa-times"></i></a>
                </div>
              </div>
            </div>
            <div class="row header-nav-clone-col-row d-block d-xl-none">
               <div class="col-12">
                <nav id="top-primary-nav-clone" class="menuzord d-block d-xl-none default menuzord-color-default menuzord-border-boxed menuzord-responsive" data-effect="slide" data-animation="none" data-align="right">
                 <ul id="main-nav-clone" class="menuzord-menu menuzord-right menuzord-indented scrollable">
                 </ul>
                </nav>
               </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </header>
    <!-- end header -->

  @yield('content')

   <!-- Footer -->
  
  <a class="scrollToTop" href="#"><i class="fa fa-angle-up"></i></a>
</div>
<!-- end wrapper -->

<!-- Footer Scripts -->
<!-- JS | Custom script for all pages -->
<script src="{{ asset('assets/js/custom.js') }}"></script>
<script>
    paypal.Buttons({
      // Sets up the transaction when a payment button is clicked
      createOrder: (data, actions) => {
        return actions.order.create({
          purchase_units: [{
            amount: {
              value: '{{$total}}' // Can also reference a variable or function
            }
          }]
        });
      },
      // Finalize the transaction after payer approval
      onApprove: (data, actions) => {
        return actions.order.capture().then(function(orderData) {
          // Successful capture! For dev/demo purposes:
          console.log('Capture result', orderData, JSON.stringify(orderData, null, 2));
          const transaction = orderData.purchase_units[0].payments.captures[0];
          if(transaction.status == "COMPLETED"){
            Livewire.emit('transactionEmit',transaction.id);
          }
          //alert(`Transaction ${transaction.status}: ${transaction.id}\n\nSee console for all available details`);
          // When ready to go live, remove the alert and show a success message within this page. For example:
          // const element = document.getElementById('paypal-button-container');
          // element.innerHTML = '<h3>Thank you for your payment!</h3>';
          // Or go to another URL:  actions.redirect('thank_you.html');
        });
      }
    }).render('#paypal-button-container');

    $(document).ready(function() {
        setTimeout(function() {
            $('#alert').fadeOut('fast');
        }, 3000); // <-- time in milliseconds
        setTimeout(function() {
            $("#pay-details").show();
        }, 3200); // <-- time in milliseconds
            
        // var element = $(".alert");
        //   element.fadeToggle(10000, function() {
        //     setTimeout(function() {
        //       element.css('color', '#F00');
        //     }, 8000)
        //   });
      });
  </script>
@livewireScripts
</body>
</html>