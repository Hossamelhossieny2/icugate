<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="icugate admin panel">
    <meta name="author" content="hossamelhossieny">
    <link rel="icon" href="{{ asset('assets/images/favicon.png') }}">

    <title>icugate | Admin panel</title>
    
    <!-- Bootstrap 4.0-->
    <link rel="stylesheet" href="{{ asset('assetAdmin/vendor_components/bootstrap/dist/css/bootstrap.css') }}">
    
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.2.1/css/all.min.css" integrity="sha512-MV7K8+y+gLIBoVD59lQIYicR65iaqukzvf/nwasF0nqhPay5w/9lJmVM2hMDcnK1OnMGCdVK+iQrJ7lzPJQd1w==" crossorigin="anonymous" referrerpolicy="no-referrer" />
      
    <!-- Data Table-->
    <link rel="stylesheet" type="text/css" href="{{ asset('assetAdmin/vendor_components/datatable/datatables.min.css') }}"/>
    
    <!-- Morris charts -->
    <link rel="stylesheet" href="{{ asset('assetAdmin/vendor_components/morris.js/morris.css') }}">
    
    <!-- theme style -->
    <link rel="stylesheet" href="{{ asset('assetAdmin/css/style.css') }}">
    
    <!-- Crypto Admin skins -->
    <link rel="stylesheet" href="{{ asset('assetAdmin/css/skin_color.css') }}">


    <!-- Bootstrap-extend -->
    <link rel="stylesheet" href="{{ asset('assetAdmin/css/bootstrap-extend.css') }}">
    
    <!-- theme style -->
    <link rel="stylesheet" href="{{ asset('assetAdmin/css/master_style.css') }}">
    
    <!-- Crypto_Admin skins -->
    <link rel="stylesheet" href="{{ asset('assetAdmin/css/skins/_all-skins.css') }}">

    <link rel="stylesheet" type="text/css" href="{{ asset('assetAdmin/css/multi-select.css') }}">
   
   <!-- jQuery 3 -->
    <script src="{{ asset('assetAdmin/vendor_components/jquery-3.3.1/jquery-3.3.1.js') }}"></script>
    
  </head>

<body class="hold-transition light-skin sidebar-mini theme-classic">
    
<div class="wrapper">

  <header class="main-header">
    <!-- Logo -->
    <a href="#" class="logo">
      <!-- mini logo -->
      <div class="logo-mini">
          <span class="light-logo"><img src="{{ asset('assets/images/logo-wide-white.png') }}" alt="logo"></span>
          <span class="dark-logo"><img src="{{ asset('assets/images/logo-wide.png') }}" alt="logo"></span>
      </div>
      <!-- logo-->
      <div class="logo-lg">
          <span class="light-logo"><img src="{{ asset('assets/images/logo-wide-white@2x.png') }}" alt="logo"></span>
          <span class="dark-logo"><img src="{{ asset('assets/images/logo-wide@2x.png') }}" alt="logo"></span>
      </div>
    </a>
    <!-- Header Navbar -->
    <nav class="navbar navbar-static-top">
      <!-- Sidebar toggle button-->
      <div>
          <a href="#" class="sidebar-toggle" data-toggle="push-menu" role="button" id="menu_toggle">
            <i class="ti-align-left"></i>
          </a>
          
          <a href="#" data-provide="fullscreen" class="sidebar-toggle" title="Full Screen">
            <i class="mdi mdi-crop-free"></i>
          </a> 
        
      </div>
        
    </nav>
  </header>
  
  <!-- Left side column. contains the logo and sidebar -->
  <aside class="main-sidebar">
    <!-- sidebar-->
    <section class="sidebar">   
        
        <div class="user-profile">
            <div class="ulogo">
                 <a href="#">
                  <!-- logo for regular state and mobile devices -->
                  <h3><b>Icu</b>gate</h3>
                </a>
            </div>
            <div class="profile-pic">
                <!-- <img src="{{ asset('assets/images/user5-128x128.jpg') }}" alt="user">  -->
                <img src="{{ asset('assets/images/blog/author.png') }}" alt="icugate admin" style="border:1px solid #E5E5E5">  
                    <div class="profile-info">
                      <h5 class="mt-15">{{Auth::user()->f_name.' '.Auth::user()->l_name}}</h5>
                    </div>
            </div>
        </div>
      
      @include('sections.admin_menu')
    </section>
  </aside>

<!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper" data-tm-bg-img="{{ asset('assets/images/pattern/p10.png') }}" style="background-image: url({{ asset('assets/images/pattern/p10.png') }});">
        <!-- Main content -->
        <section class="content" data-tm-bg-img="{{ asset('assets/images/pattern/p10.png') }}" style="background-image: url({{ asset('assets/images/pattern/p10.png') }});">

        <div class="row">
            <div class="col-12">
                <div class="box">
                    
                      @if( Session::has('status') )
                      <div class="box-header with-border">
                        <div class="alert alert-{{session('status')[1]}} alert-dismissible text-center">
                          {{ session('status')[0] }}
                        </div>
                      </div>
                      @endif

                      @if( $errors->all() )
                      <div class="box-header with-border">
                      <ul>
                          @foreach ($errors->all() as $message)
                          <li><span class="help-block text-danger text-center">{{ $message }}</span></li>
                          @endforeach
                      </ul>
                      </div>
                      @endif
                        
                    
                  </div>
              </div>
          </div>

      @yield('content')

        </section>
        <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->


 <!-- /.content-wrapper -->
  <footer class="main-footer">
    <div class="pull-right d-none d-sm-inline-block">
        <ul class="nav nav-primary nav-dotted nav-dot-separated justify-content-center justify-content-md-end">
          <li class="nav-item">
            <a class="nav-link" href="javascript:void(0)">FAQ</a>
          </li>
          
        </ul>
    </div>
      &copy; 2020-2023 <a href="https::/icugate.com">icugate</a>. All Rights Reserved.
  </footer>

  
  <!-- /.control-sidebar -->
  
  <!-- Add the sidebar's background. This div must be placed immediately after the control sidebar -->
  <div class="control-sidebar-bg"></div>
  
</div>
<!-- ./wrapper -->
    
     
    
    
    
    <!-- fullscreen -->
    <script src="{{ asset('assetAdmin/vendor_components/screenfull/screenfull.js') }}"></script>
    
    <!-- jQuery UI 1.11.4 -->
    <script src="{{ asset('assetAdmin/vendor_components/jquery-ui/jquery-ui.js') }}"></script>
    
    <!-- popper -->
    <script src="{{ asset('assetAdmin/vendor_components/popper/dist/popper.min.js') }}"></script>
    
    <!-- Bootstrap 4.0-->
    <script src="{{ asset('assetAdmin/vendor_components/bootstrap/dist/js/bootstrap.js') }}"></script>  
    
    <!-- Slimscroll -->
    <script src="{{ asset('assetAdmin/vendor_components/jquery-slimscroll/jquery.slimscroll.js') }}"></script>
    
    <!-- FastClick -->
    <script src="{{ asset('assetAdmin/vendor_components/fastclick/lib/fastclick.js') }}"></script>
    
    <!-- Bootstrap WYSIHTML5 -->
    <script src="{{ asset('assetAdmin/vendor_plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.js') }}"></script>
    
    <!-- Morris.js charts -->
    <script src="{{ asset('assetAdmin/vendor_components/raphael/raphael.min.js') }}"></script>
    <script src="{{ asset('assetAdmin/vendor_components/morris.js/morris.min.js') }}"></script>
    
    <!-- ChartJS -->
    <script src="{{ asset('assetAdmin/vendor_components/chart.js-master/Chart.min.js') }}"></script>
    
    
    
    <!-- webticker -->
    <script src="{{ asset('assetAdmin/vendor_components/Web-Ticker-master/jquery.webticker.min.js') }}"></script>
    
    <!-- EChartJS JavaScript -->
    <script src="{{ asset('assetAdmin/vendor_components/echarts-master/dist/echarts-en.min.js') }}"></script>
    <script src="{{ asset('assetAdmin/vendor_components/echarts-liquidfill-master/dist/echarts-liquidfill.min.js') }}"></script>
    
    <!-- This is data table -->
    <script src="{{ asset('assetAdmin/vendor_components/datatable/datatables.min.js') }}"></script>
    
    <!-- Crypto Admin App -->
    <script src="{{ asset('assetAdmin/js/template.js') }}"></script>
    
    <!-- Crypto Admin dashboard demo (This is only for demo purposes) -->
    <script src="{{ asset('assetAdmin/js/pages/dashboard.js') }}"></script>
    <script src="{{ asset('assetAdmin/js/pages/dashboard-chart.js') }}"></script>
    
    <!-- Crypto Admin for demo purposes -->
    <script src="{{ asset('assetAdmin/js/demo.js') }}"></script>

    <!-- Crypto Admin for Data Table -->
  <script src="{{ asset('assetAdmin/js/pages/data-table.js') }}"></script>
 <script type="text/javascript">
      $('#example1').dataTable( {
        "ordering": false
      } );      

      $(document).ready(function() {
        var element = $(".alert");
          element.fadeToggle(3000, function() {
            setTimeout(function() {
              //your code here
              element.css('color', '#F00');
            }, 15000)

          });
        //this does not make sense
        $('input[type="checkbox"]').change(function(){
            this.value = (Number(this.checked));
        });
      });
  </script>

</body>
</html>

