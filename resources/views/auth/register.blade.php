<!DOCTYPE html>
<html dir="ltr" lang="en">
<head>

<!-- Meta Tags -->
<meta name="viewport" content="width=device-width,initial-scale=1.0"/>
<meta http-equiv="content-type" content="text/html; charset=UTF-8"/>
<meta name="description" content="ICU Gate is a Specialized Website In Medical Researches ,Live Medical News ,Medical cources ,More Training Hours ... amd discover More !!" />
<meta name="keywords" content="Icugate,Medical,cources,News,Training" />
<meta name="author" content="Hossamelhossieny" />

<!-- Page Title -->
<title>{{ config('app.name', 'Laravel') }} | Live 24 Medical News</title>

<!-- Favicon and Touch Icons -->
<link href="{{ asset('assets') }}/images/favicon.png" rel="shortcut icon" type="image/png">
<link href="{{ asset('assets') }}/images/apple-touch-icon.png" rel="apple-touch-icon">
<link href="{{ asset('assets') }}/images/apple-touch-icon-72x72.png" rel="apple-touch-icon" sizes="72x72">
<link href="{{ asset('assets') }}/images/apple-touch-icon-114x114.png" rel="apple-touch-icon" sizes="114x114">
<link href="{{ asset('assets') }}/images/apple-touch-icon-144x144.png" rel="apple-touch-icon" sizes="144x144">

<!-- Stylesheet -->
<link href="{{ asset('assets/css/bootstrap.min.css') }}" rel="stylesheet" type="text/css">
<link href="{{ asset('assets/css/jquery-ui.min.css') }}" rel="stylesheet" type="text/css">
<link href="{{ asset('assets/css/animate.css') }}" rel="stylesheet" type="text/css">
<link href="{{ asset('assets/css/css-plugin-collections.css') }}" rel="stylesheet"/>
<!-- CSS | menuzord megamenu skins -->
<link href="{{ asset('assets/css/menuzord-megamenu.css') }}" rel="stylesheet"/>
<link id="menuzord-menu-skins" href="{{ asset('assets/css/menuzord-skins/menuzord-boxed.css') }}" rel="stylesheet"/>
<!-- CSS | Main style file -->
<link href="{{ asset('assets/css/style-main.css') }}" rel="stylesheet" type="text/css">
<!-- CSS | Preloader Styles -->
<link href="{{ asset('assets/css/preloader.css') }}" rel="stylesheet" type="text/css">
<!-- CSS | Custom Margin Padding Collection -->
<link href="{{ asset('assets/css/custom-bootstrap-margin-padding.css') }}" rel="stylesheet" type="text/css">
<!-- CSS | Responsive media queries -->
<link href="{{ asset('assets/css/responsive.css') }}" rel="stylesheet" type="text/css">
<!-- CSS | Style css. This is the file where you can place your own custom css code. Just uncomment it and use it. -->
<!-- <link href="css/style.css" rel="stylesheet" type="text/css"> -->

<!-- CSS | Theme Color -->
<link href="{{ asset('assets/css/colors/theme-skin-color-set1.css') }}" rel="stylesheet" type="text/css">

<!-- external javascripts -->
<script src="{{ asset('assets/js/jquery-2.2.4.min.js') }}"></script>
<script src="{{ asset('assets/js/jquery-ui.min.js') }}"></script>
<script src="{{ asset('assets/js/bootstrap.min.js') }}"></script>
<!-- JS | jquery plugin collection for this theme -->
<script src="{{ asset('assets/js/jquery-plugin-collection.js') }}"></script>

<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
<![endif]-->
</head>
<body class="tm-container-1300px">
<div id="wrapper" class="clearfix">

  <!-- Start main-content -->
  <div class="main-content-area">
    <!-- Section: home -->
    <section id="home" class="divider parallax fullscreen layer-overlay overlay-dark-9 section-typo-light" data-tm-bg-img="images/bg/bg9.jpg">
      <div class="display-table">
        <div class="display-table-cell">
          <div class="container">
            <div class="row">
              <div class="col-md-9 offset-md-2">
                <div class="text-center mb-60"><a href="#" class=""><img alt="images" src="{{ asset('assets/images/logo-wide-white.png') }}"></a></div>
                 <form name="reg-form" class="register-form" method="post" action="{{ route('register') }}">
                  {{ csrf_field() }}
                  <div class="icon-box mb-0 p-0">
                    <a href="#" class="icon icon-bordered icon-rounded icon-sm float-start mt-10 mb-0 mr-10">
                      <i class="pe-7s-users"></i>
                    </a>
                    <h4 class="text-gray pt-10 mt-0 mb-30">Don't have an Account? Register Now as free user</h4>
                  </div>
                  <hr>
                  <div class="row">
                    <div class="mb-3 col-md-3">
                      <label>First Name</label>
                      <input name="f_name" class="form-control" type="text" placeholder="Type first name .. " value="{{ old('f_name') }}" required="">
                      @if ($errors->has('f_name'))
                        <span class="help-block">
                            <strong style="color: red">{{ $errors->first('f_name') }}</strong>
                        </span>
                    @endif
                    </div>
                    <div class="mb-3 col-md-3">
                      <label>Last Name</label>
                      <input name="l_name" class="form-control" type="text" placeholder="Type Last name .. " value="{{ old('l_name') }}" required="">
                    @if ($errors->has('l_name'))
                        <span class="help-block">
                            <strong style="color: red">{{ $errors->first('l_name') }}</strong>
                        </span>
                    @endif
                    </div>
                    <div class="form-group col-md-3">
                      <label>Living country</label>
                      <input name="country" class="form-control" type="text" placeholder="Living country .. " value="{{ old('country') }}">
                      @if ($errors->has('country'))
                            <span class="help-block">
                                <strong style="color: red">{{ $errors->first('country') }}</strong>
                            </span>
                        @endif
                    </div>
                    <div class="form-group col-md-3">
                      <label>Living city</label>
                      <input name="city" class="form-control" type="text" placeholder="Living city .. " value="{{ old('city') }}">
                      @if ($errors->has('city'))
                            <span class="help-block">
                                <strong>{{ $errors->first('city') }}</strong>
                            </span>
                        @endif
                    </div>
                  </div>
                  <div class="row">
                    <div class="mb-3 col-md-6">
                      <label for="form_choose_username">Email Address</label>
                      <input name="email" class="form-control" type="email" placeholder="Type your Email To login anytime .. " value="{{ old('email') }}" required="">
                      @if ($errors->has('email'))
                        <span class="help-block">
                            <strong style="color: red">{{ $errors->first('email') }}</strong>
                        </span>
                        @endif
                    </div>
                    <div class="mb-3 col-md-6">
                      <div class="form-group col-md-12">
                        <label for="form_choose_username">Mobile no</label>
                        <input id="form_mobile" name="mobile" class="form-control" type="text" placeholder="Your Mobile no Just 050.... without Country code" value="{{ old('mobile') }}">
                        @if ($errors->has('mobile'))
                              <span class="help-block">
                                  <strong style="color: red">{{ $errors->first('mobile') }}</strong>
                              </span>
                          @endif
                      </div>
                    </div>
                  </div>
                  <div class="row">
                    <div class="mb-3 col-md-6">
                      <label for="form_choose_password">Choose Password</label>
                      <input id="form_choose_password" name="password" class="form-control" type="password" placeholder="choose your password .. " value="{{ old('password') }}" required="">
                   @if ($errors->has('password'))
                        <span class="help-block">
                            <strong style="color: red">{{ $errors->first('password') }}</strong>
                        </span>
                    @endif
                    </div>
                    <div class="mb-3 col-md-6">
                      <label>Re-enter Password</label>
                      <input id="form_re_enter_password" name="password_confirmation"  class="form-control" type="password" placeholder="re-Type password again .. " value="{{ old('password_confirmation') }}" required="">
                   @if ($errors->has('password_confirmation'))
                        <span class="help-block">
                            <strong style="color: red">{{ $errors->first('password_confirmation') }}</strong>
                        </span>
                    @endif
                    </div>
                  </div>
                  <div class="row">
                    
                  </div>
                  <div class="row">
                    <div class="form-group col-md-6">
                      <label>Hospital</label>
                      <input name="hospital" class="form-control" type="text" placeholder="Member of Hospital .. " value="{{ old('hospital') }}">
                      @if ($errors->has('hospital'))
                            <span class="help-block">
                                <strong style="color: red">{{ $errors->first('hospital') }}</strong>
                            </span>
                        @endif
                    </div>
                    <div class="form-group col-md-6">
                      <label>Profession</label>
                      <input name="profession" class="form-control" type="text" placeholder="Your Profession .. " value="{{ old('profession') }}">
                      @if ($errors->has('profession'))
                            <span class="help-block">
                                <strong style="color: red">{{ $errors->first('profession') }}</strong>
                            </span>
                        @endif
                    </div>
                  </div>
                  
                  <div class="mb-3 tm-sc-button">
                    <button class="btn btn-dark btn-theme-colored1 mt-15" type="submit">Register Now</button>
                  </div>
                </form>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>
  </div>
  <!-- end main-content -->

  <!-- Footer -->
  <footer id="footer" class="footer">
    <div class="container p-20">
      <div class="row">
        <div class="col-md-12 text-center">
          <p class="mb-0">© 2012-2023 Triangle4it. All Rights Reserved.</p>
        </div>
      </div>
    </div>
  </footer>
  <a class="scrollToTop" href="#"><i class="fa fa-angle-up"></i></a>
</div>
<!-- end wrapper -->

<!-- Footer Scripts -->
<!-- JS | Custom script for all pages -->
<script src="{{ asset('assets/js/custom.js') }}"></script>

</body>
</html>