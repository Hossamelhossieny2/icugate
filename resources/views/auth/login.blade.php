<!DOCTYPE html>
<html dir="ltr" lang="en">
<head>
<meta name="viewport" content="width=device-width,initial-scale=1.0"/>
<meta http-equiv="content-type" content="text/html; charset=UTF-8"/>
<meta name="description" content="ICU Gate is a Specialized Website In Medical Researches ,Live Medical News ,Medical cources ,More Training Hours ... amd discover More !!" />
<meta name="keywords" content="Icugate,Medical,cources,News,Training" />
<meta name="author" content="Hossamelhossieny" />

<!-- Page Title -->
<title>{{ config('app.name', 'Laravel') }} | Live 24 Medical News</title>

<!-- Page Title -->
<title>{{ config('app.name', 'Laravel') }} | Live 24 Medical News</title>

<!-- Favicon and Touch Icons -->
<link href="{{ asset('assets/images/favicon.png') }}" rel="shortcut icon" type="image/png">
<link href="{{ asset('assets/images/apple-touch-icon.png') }}" rel="apple-touch-icon">
<link href="{{ asset('assets/images/apple-touch-icon-72x72.png') }}" rel="apple-touch-icon" sizes="72x72">
<link href="{{ asset('assets/images/apple-touch-icon-114x114.png') }}" rel="apple-touch-icon" sizes="114x114">
<link href="{{ asset('assets/images/apple-touch-icon-144x144.png') }}" rel="apple-touch-icon" sizes="144x144">

<!-- Stylesheet -->
<link href="{{ asset('assets/css/bootstrap.min.css') }}" rel="stylesheet" type="text/css">
<link href="{{ asset('assets/css/animate.min.css') }}" rel="stylesheet" type="text/css">
<link href="{{ asset('assets/css/javascript-plugins-bundle.css') }}" rel="stylesheet"/>

<!-- CSS | menuzord megamenu skins -->
<link href="{{ asset('assets/js/menuzord/css/menuzord.css') }}" rel="stylesheet"/>

<!-- CSS | Main style file -->
<link href="{{ asset('assets/css/style-main.css') }}" rel="stylesheet" type="text/css">
<link id="menuzord-menu-skins" href="{{ asset('assets/css/menuzord-skins/menuzord-rounded-boxed.css') }}" rel="stylesheet"/>

<!-- CSS | Responsive media queries -->
<link href="{{ asset('assets/css/responsive.css') }}" rel="stylesheet" type="text/css">
<!-- CSS | Style css. This is the file where you can place your own custom css code. Just uncomment it and use it. -->

<!-- CSS | Theme Color -->
<link href="{{ asset('assets/css/colors/theme-skin-color-set1.css') }}" rel="stylesheet" type="text/css">

<!-- external javascripts -->
<script src="{{ asset('assets/js/jquery.js') }}"></script>
<script src="{{ asset('assets/js/popper.min.js') }}"></script>
<script src="{{ asset('assets/js/bootstrap.min.js') }}"></script>
<script src="{{ asset('assets/js/javascript-plugins-bundle.js') }}"></script>
<script src="{{ asset('assets/js/menuzord/js/menuzord.js') }}"></script>

<!-- <link href="css/style.css" rel="stylesheet" type="text/css"> -->


<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
<![endif]-->
</head>
<body class="tm-container-1300px">
<div id="wrapper" class="clearfix">

  <!-- Start main-content -->
  <div class="main-content-area">
    <!-- Section: home -->
    <section id="home" class="divider parallax section-fullscreen layer-overlay overlay-dark-8" data-tm-bg-img="{{ asset('assets/images/bg/bg9.jpg') }}">
      <div class="display-table">
        <div class="display-table-cell">
          <div class="container">
            <div class="row">
              <div class="col-md-6 offset-md-3">
                <div class="text-center mb-60"><a href="#" class=""><img alt="images" src="{{ asset('assets/images/logo-wide-white.png') }}"></a></div>
                <h4 class="text-theme-colored1 mt-0 pt-10"> Login</h4>
                <p class="text-white">Login to your account.</p>
                  @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif
                  @if ($errors->any())
                      <div class="alert alert-danger">
                          <ul>
                              @foreach ($errors->all() as $error)
                                  <li>{{ $error }}</li>
                              @endforeach
                          </ul>
                      </div>
                  @endif
                  @if (session('error'))
                        <div class="alert alert-danger" role="alert">
                            {{ session('error') }}
                        </div>
                    @endif
                <form name="login-form" class="form-login clearfix" method="POST" action="{{ route('login') }}">
                {{ csrf_field() }}
                  <div class="row">
                    <div class="mb-3 col-md-12">
                      <label class="text-white" for="form_username_email">Email</label>
                      <input id="form_username_email" name="email" class="form-control" type="email" value="{{ old('email') }}" required="">
                    @if ($errors->has('email'))
                    <span class="help-block" style="color: red">
                        <strong>{{ $errors->first('email') }}</strong>
                    </span>
                    @endif
                    </div>
                  </div>
                  <div class="row">
                    <div class="mb-3 col-md-12">
                      <label class="text-white" for="form_password">Password</label>
                      <input id="form_password" name="password" class="form-control" type="password" required="">
                    @if ($errors->has('password'))
                      <span class="help-block"  style="color: red">
                          <strong>{{ $errors->first('password') }}</strong>
                      </span>
                      @endif
                    </div>
                  </div>
                  <div class="checkbox mt-15">
                    <label for="form_checkbox">
                      <input id="form_checkbox" name="form_checkbox" type="checkbox" {{ old('remember') ? 'checked' : '' }}>
                      Remember me </label>
                  </div>
                  <div class="mb-3 tm-sc-button mt-10">
                    <button type="submit" class="btn btn-theme-colored1 btn-lg">Login</button>
                  </div>
                  <div class="clearfix pt-15">
                    <a class="text-theme-colored1 font-weight-600 font-size-14" href="{{ route('password.request') }}">Forgot Your Password?</a>
                  </div>
                  <div class="clear text-center pt-10">
                    <a class="text-theme-colored font-weight-600 font-12" href="{{ route('register') }}">Don't have an Account? Register Now </a>
                  </div>
                  <div class="clearfix tm-sc-button pt-10">
                    <a href="#" target="_self" class="btn btn-theme-colored1" data-tm-bg-color="#3b5998"> Login facebook </a>
                    <a href="#" target="_self" class="btn btn-theme-colored1" data-tm-bg-color="#00acee"> Login with twitter </a>
                  </div>
                </form>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>
  </div>
  <!-- end main-content -->

  <!-- Footer -->
  <footer id="footer" class="footer">
    <div class="container p-20">
      <div class="row">
        <div class="col-md-12 text-center">
          <p class="mb-0">© 2012-2023 Triangle4it. All Rights Reserved.</p>
        </div>
      </div>
    </div>
  </footer>
  <a class="scrollToTop" href="#"><i class="fa fa-angle-up"></i></a>
</div>
<!-- end wrapper -->

<!-- Footer Scripts -->
<!-- JS | Custom script for all pages -->
<script src="{{ asset('assets/js/custom.js') }}"></script>

</body>
</html>