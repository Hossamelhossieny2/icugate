@extends('layouts.app')

@section('content')
    
<!-- Start main-content -->
  <div class="main-content-area">
    <!-- Section: page title -->
    <section class="page-title layer-overlay overlay-dark-8 section-typo-light bg-img-center" data-tm-bg-img="{{ asset('assets/images/bg/bg10.jpg') }}">
      <div class="container pt-50 pb-50">
        <div class="section-content">
          <div class="row">
            <div class="col-md-12 text-center">
              <h2 class="title">{{$ccat['name']}}</h2>
            </div>
          </div>
        </div>
      </div>
    </section>

    <section>
      <div class="container mt-30 mb-30 pt-30 pb-30">
        <div class="row">

          <!-- Section: main section -->
          <div class="col-md-9 order-lg-2">
            @include('sections.criticalcare_content')
          </div>

          <!-- Section: left panet -->
          <div class="col-md-3 order-lg-1">
            @include('sections.left')
          </div>
          
        </div>
      </div>
    </section>
    
  </div>
  <!-- end main-content -->
  
@endsection
