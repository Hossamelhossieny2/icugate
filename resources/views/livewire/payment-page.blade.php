<div>
    <!-- Start main-content -->
  <div class="main-content-area">

<!-- Section: page title -->
    <section class="page-title layer-overlay overlay-dark-8 section-typo-light bg-img-center" data-tm-bg-img="{{ asset('assets/images/bg/bg10.jpg') }}">
      <div class="container pt-50 pb-50">
        <div class="section-content">
          <div class="row">
            <div class="col-md-12 text-center">
              <h2 class="title"><i class="fa fa-lock"></i> Paypal Payment</h2>
            </div>
          </div>
        </div>
      </div>
    </section>

    <section class="text-center" data-tm-bg-img="{{ asset('assets/images/pattern/p10.png') }}" style="background-image: url({{ asset('assets/images/pattern/p10.png') }});">
      <div class="container">
        <div class="row">
        @if($payment_mode == "PAID")
          <div class="col-md-6 offset-md-3" id="alert">
            <p class="alert alert-success h4">THANK YOU !! Successful Payment</p>
            <span class="badge badge-secondary text-secondary">if the page Freeze please make refresh</span>
          </div>
          <div class="col-md-6 offset-md-3 bg-white pb-50 pt-10" style="display:none" id="pay-details">
          <h4 class="mt-0 pt-10"><i class="fa fa-lock"></i> Paypal Order  Details</h4>
          <hr>
            <p class="text-left pl-100">icugate Order id: {{ $order }} </p>
            <p class="text-left pl-100">Paypal Order id: {{ $payment_id }} </p>
            <p class="text-left pl-100">Payment For: {{ $pay_for }} </p>
            <p class="text-left pl-100">Payment time: {{ date('d M Y h:i A',strtotime($pay_time)) }} </p>
            <hr>
            <p class="text-primary">Review your payment from your account any time</p>
            <a class="btn btn-lg btn-secondary" href="{{ route('home') }}">Back to Home</a>
          </div>
        @elseif($payment_mode == "UNPAID")
          <div class="col-md-6 offset-md-3 bg-white pb-50 pt-20" style="display:block" id="pay-block">
          
            <h4 class="mt-0 pt-10"><i class="fa fa-lock"></i> Paypal Order Form </h4>
            <p class="text-primary">icugate reference id: {{ $order }} </p>
            <hr>
            <h5>{!! $pay_for !!}</h5>
            <p>Total To pay: {{$total}} USD</p>
            <div wire:ignore>
              <div id="paypal-button-container"></div>
            </div>
            
            <a class="btn btn-lg btn-secondary" href="{{ url()->previous() }}">Please Cancel & Back to previous page</a>
          </div>
          <!-- <div class="col-md-6 offset-md-3" style="display:none" id="success-msg">
            <p class="alert alert-danger h4">UnSuccessful !! please try again Later</p>
          </div>
          <div class="col-md-6 offset-md-3 bg-white pb-50 pt-20" style="display:block" id="pay-block">
            <a class="btn btn-lg btn-secondary" href="{{ url()->previous() }}">Back to previous page</a>
          </div> -->
        @endif
        </div>
      </div>
      <img src="{{ asset('assets/images/secure-paypal-payments.png') }}" width="100"/>
    </section>
</div>
</div>
