@extends('layouts.app')

@section('content')

@push('styles')
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.2.1/css/all.min.css" integrity="sha512-MV7K8+y+gLIBoVD59lQIYicR65iaqukzvf/nwasF0nqhPay5w/9lJmVM2hMDcnK1OnMGCdVK+iQrJ7lzPJQd1w==" crossorigin="anonymous" referrerpolicy="no-referrer" />
@endpush

  <!-- Start main-content -->
  <div class="main-content-area">

  <section class="page-title layer-overlay overlay-dark-8 section-typo-light bg-img-center" data-tm-bg-img="{{ asset('assets/images/bg/bg10.jpg') }}">
      <div class="container pt-50 pb-50">
        <div class="section-content">
          <div class="row">
            <div class="col-md-12 text-center">
              <h2 class="title">Contact our Professionals</h2>
            </div>
          </div>
        </div>
      </div>
    </section>

  <!-- Section: Contact Form -->
    <section  id="contact">
      <div class="container">
        <div class="section-content">
          <div class="row">
            @if( Session::has('status') )
                <div class="alert alert-{{session('status')[1]}} alert-dismissible text-center">
                  {{ session('status')[0] }}
                </div>
            @endif
             <ul>
              @foreach ($errors->all() as $message)
              <li><span class="help-block text-danger text-center">{{ $message }}</span></li>
              @endforeach
            </ul>
            <div class="col-lg-6">
              <h5 class="mb-0 text-gray">Happy to help!</h5>
              <h2 class="mb-30">If you need someone to talk to, we listen. We won’t judge or tell you what to do.</h2>
              <div class="icon-box icon-left iconbox-centered-in-responsive iconbox-theme-colored1 animate-icon-on-hover animate-icon-rotate mb-50">
                <div class="icon-box-wrapper">
                  <div class="icon-wrapper">
                    <a class="icon icon-type-font-icon icon-dark icon-circled"> <i class="flaticon-contact-044-call-1"></i> </a>
                  </div>
                  <div class="icon-text">
                    <h5 class="icon-box-title mt-0">Phone</h5>
                    <div class="content"><a href="tel:{{ $conf['phone'] }}">{{ $conf['phone'] }}</a></div>
                  </div>
                  <div class="clearfix"></div>
                </div>
              </div>
              <div class="icon-box icon-left iconbox-centered-in-responsive iconbox-theme-colored1 animate-icon-on-hover animate-icon-rotate mb-50">
                <div class="icon-box-wrapper">
                  <div class="icon-wrapper">
                    <a class="icon icon-type-font-icon icon-dark icon-circled"> <i class="flaticon-contact-043-email-1"></i> </a>
                  </div>
                  <div class="icon-text">
                    <h5 class="icon-box-title mt-0">Email</h5>
                    <div class="content"><a href="mailto:{{ $conf['site_email'] }}">{{ $conf['site_email'] }}</a></div>
                  </div>
                  <div class="clearfix"></div>
                </div>
              </div>
              <div class="icon-box icon-left iconbox-centered-in-responsive iconbox-theme-colored1 animate-icon-on-hover animate-icon-rotate mb-50">
                <div class="icon-box-wrapper">
                  <div class="icon-wrapper">
                    <a class="icon icon-type-font-icon icon-dark icon-circled"> <i class="flaticon-contact-025-world"></i> </a>
                  </div>
                  <div class="icon-text">
                    <h5 class="icon-box-title mt-0">Address</h5>
                    <div class="content">{{ $conf['address'] }}</div>
                  </div>
                  <div class="clearfix"></div>
                </div>
              </div>
              <ul class="styled-icons icon-dark icon-lg icon-circled mt-30">
                <li><a target="_blank" href="{{ $conf['facebook'] }}" data-tm-bg-color="#3B5998"><i class="fab fa-facebook"></i></a></li>
                <li><a target="_blank" href="{{ $conf['twitter'] }}" data-tm-bg-color="#02B0E8"><i class="fab fa-twitter"></i></a></li>
                <li><a target="_blank" href="{{ $conf['instgram'] }}" data-tm-bg-color="#D9CCB9"><i class="fab fa-instagram"></i></a></li>
                <li><a target="_blank" href="{{ $conf['youtube'] }}" data-tm-bg-color="#D71619"><i class="fab fa-youtube"></i></a></li>
                <li><a target="_blank" href="{{ $conf['android'] }}" data-tm-bg-color="#A4CA39"><i class="fab fa-android"></i></a></li>
                <li><a target="_blank" href="{{ $conf['apple'] }}" data-tm-bg-color="#4C75A3"><i class="fab fa-apple"></i></a></li>
              </ul>
            </div>
            <div class="col-lg-6">
              <h2 class="mt-0 mb-0">Interested in discussing?</h2>
              <p class="font-size-20">Active & Ready to use Contact Form!</p>

              <!-- Google reCAPTCHA -->
              <script src="https://www.google.com/recaptcha/api.js"></script>
              
              <!-- Contact Form -->
              <form id="contact_form" name="contact_form" class="" action="{{ route('send.contact') }}" method="post">
              @csrf
                <div class="row">
                  <div class="col-sm-6">
                    <div class="mb-3">
                      <label>Name <small>*</small></label>
                      <input name="form_name" class="form-control" type="text" @if(auth()->user()) value="{{ auth()->user()->f_name.' '.auth()->user()->l_name }}" @else placeholder="Enter Name" @endif>
                    </div>
                    @if ($errors->has('form_name')) 
                      <div class="alert alert-danger" role="alert">{{$errors->first('form_name')}}</div>
                    @endif
                  </div>
                  <div class="col-sm-6">
                    <div class="mb-3">
                      <label>Email <small>*</small></label>
                      <input name="form_email" class="form-control required email" type="email" @if(auth()->user()) value="{{ auth()->user()->email }}" @else placeholder="Enter Email" @endif>
                    </div>
                    @if ($errors->has('form_email')) 
                      <div class="alert alert-danger" role="alert">{{$errors->first('form_email')}}</div>
                    @endif
                  </div>
                </div>
                <div class="row">
                  <div class="col-sm-6">
                    <div class="mb-3">
                      <label>Subject <small>*</small></label>
                      <input name="form_subject" class="form-control required" type="text" placeholder="Enter Subject">
                    </div>
                    @if ($errors->has('form_subject')) 
                      <div class="alert alert-danger" role="alert">{{$errors->first('form_subject')}}</div>
                    @endif
                  </div>
                  <div class="col-sm-6">
                    <div class="mb-3">
                      <label>Phone</label>
                      <input name="form_phone" class="form-control" type="text" @if(auth()->user()) value="{{ auth()->user()->mobile }}" @else placeholder="Enter Mobile" @endif>
                    </div>
                    @if ($errors->has('form_phone')) 
                      <div class="alert alert-danger" role="alert">{{$errors->first('form_phone')}}</div>
                    @endif
                  </div>
                </div>

                <div class="mb-3">
                  <label>Message</label>
                  <textarea name="form_message" class="form-control required" rows="5" placeholder="Enter Message"></textarea>
                  @if ($errors->has('form_message')) 
                    <div class="alert alert-danger" role="alert">{{$errors->first('form_message')}}</div>
                  @endif
                </div>
                
                <div class="mb-3">
                  <button type="submit" class="btn btn-flat btn-theme-colored1 text-uppercase mt-10 mb-sm-30 border-left-theme-color-2-4px" data-loading-text="Please wait...">Send your message</button>
                  <button type="reset" class="btn btn-flat btn-theme-colored3 text-uppercase mt-10 mb-sm-30 border-left-theme-color-2-4px">Reset</button>
                </div>
              </form>

            </div>
          </div>
        </div>
      </div>
    </section>
    <!-- End Divider -->
</div>

@endsection