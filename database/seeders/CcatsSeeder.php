<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use App\Models\Ccat;

class CcatsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Ccat::create([
            'id' => '1',
            'name' => "Critical Care Educational Support Professionals",
            'playlist' => "Critical+Care+Educational+Support+Professionals",
            'link1' => "http://www.planestories.iata.org",
            'link2' => "http://www.planestories.iata.org",
            'link3' => "http://www.planestories.iata.org",
            'who_we_are' => "&lt;p&gt;
            &lt;span style=&quot;font-size:14px;&quot;&gt;&lt;strong&gt;We are group of senior intensivists with great passion to make new changes in the education of intensivists all over the world.&lt;/strong&gt;&lt;/span&gt;&lt;/p&gt;",
            'what_we_do' => "&lt;p&gt;
            &lt;span style=&quot;font-size:14px;&quot;&gt;&lt;b&gt;We are committed to educate and support intensivists through conducting non-profit courses, on-site teaching, symposiums and scientific researches including all aspects of critical care medicine.&lt;/b&gt;&lt;/span&gt;&lt;/p&gt;",
            'ongoing_researches' => " ",
        ]);

        Ccat::create([
            'id' => '2',
            'name' => "Critical Care Ultrasound",
            'playlist' => "Critical+Care+Ultrasound",
            'link1' => "http://www.planestories.iata.org",
            'link2' => "http://www.planestories.iata.org",
            'link3' => "http://www.planestories.iata.org",
            'who_we_are' => "&lt;p&gt;
            &lt;span style=&quot;font-size:14px;&quot;&gt;We are trusted group of senior intensivists from all over the world committed&amp;nbsp;to make new changes in critical care ultrasonagraphy.&lt;/span&gt;&lt;/p&gt;&lt;p&gt;&amp;nbsp;&lt;/p&gt;",
            'what_we_do' => "&lt;p&gt;&lt;span style=&quot;font-size:14px;&quot;&gt;We are committed to educate and support intensivists through conducting non profit courses, onsite teaching, symposiums and&amp;nbsp;multicenter researches in critical care ultrasound.&lt;/span&gt;&lt;/p&gt;",
            'ongoing_researches' => " ",
        ]);

        Ccat::create([
            'id' => '3',
            'name' => "Online",
            'playlist' => "nlinro",
            'link1' => "http://www.planestories.iata.org",
            'link2' => "http://www.planestories.iata.org",
            'link3' => "http://www.planestories.iata.org",
            'who_we_are' => "&lt;p&gt;
            &lt;span style=&quot;font-size:14px;&quot;&gt;We are trusted group of senior intensivists from all over the world committed&amp;nbsp;to make new changes in critical care ultrasonagraphy.&lt;/span&gt;&lt;/p&gt;&lt;p&gt;&amp;nbsp;&lt;/p&gt;",
            'what_we_do' => "&lt;p&gt;&lt;span style=&quot;font-size:14px;&quot;&gt;We are committed to educate and support intensivists through conducting non profit courses, onsite teaching, symposiums and&amp;nbsp;multicenter researches in critical care ultrasound.&lt;/span&gt;&lt;/p&gt;",
            'ongoing_researches' => " ",
        ]);
    }
}
