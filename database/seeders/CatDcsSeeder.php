<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use App\Models\CatDoc;

class CatDcsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Catdoc::create( [
            'id'=>5,
            'cat_id'=>1,
            'title'=>'CCESP Courses PPT',
            'file'=>'file-1528437090-8W42GK.pdf',
            'date'=>'2016-08-16 22:55:29'
            ] );
                        
            Catdoc::create( [
            'id'=>7,
            'cat_id'=>2,
            'title'=>'Role of US in Acute Kidney Injury PPT',
            'file'=>'file-1474927080-SBPCLB.pdf',
            'date'=>'2016-09-26 21:58:00'
            ] );
                        
            Catdoc::create( [
            'id'=>8,
            'cat_id'=>2,
            'title'=>'The Art of Hemodynamic 1 IVC Assessment',
            'file'=>'file-1474929059-F4VJFB.pdf',
            'date'=>'2016-09-26 22:30:59'
            ] );
                        
            Catdoc::create( [
            'id'=>9,
            'cat_id'=>2,
            'title'=>'The Art of Hemodynamic 2',
            'file'=>'file-1474929125-91DYZT.pdf',
            'date'=>'2016-09-26 22:32:05'
            ] );
                        
            Catdoc::create( [
            'id'=>10,
            'cat_id'=>2,
            'title'=>'The Art of Hemodynamic 3',
            'file'=>'file-1474929156-2M8G72.pdf',
            'date'=>'2016-09-26 22:32:36'
            ] );
                        
            Catdoc::create( [
            'id'=>11,
            'cat_id'=>2,
            'title'=>'The Art of Hemodynamic 4',
            'file'=>'file-1474929184-Q8DTUN.pdf',
            'date'=>'2016-09-26 22:33:04'
            ] );
                        
            Catdoc::create( [
            'id'=>12,
            'cat_id'=>2,
            'title'=>'The Art of Hemodynamic 5 PLRT',
            'file'=>'file-1474929226-3VAPQR.pdf',
            'date'=>'2016-09-26 22:33:46'
            ] );
                        
            Catdoc::create( [
            'id'=>13,
            'cat_id'=>2,
            'title'=>'Rare Causes of Coma',
            'file'=>'file-1474929455-V1PJVC.pdf',
            'date'=>'2016-09-26 22:37:35'
            ] );
                        
            Catdoc::create( [
            'id'=>14,
            'cat_id'=>2,
            'title'=>'Metformin Toxicity',
            'file'=>'file-1474929501-IKSRCR.pdf',
            'date'=>'2016-09-26 22:38:21'
            ] );
                        
            Catdoc::create( [
            'id'=>15,
            'cat_id'=>2,
            'title'=>'ETAMI',
            'file'=>'file-1474929591-47U43N.pdf',
            'date'=>'2016-09-26 22:39:51'
            ] );
                        
            Catdoc::create( [
            'id'=>16,
            'cat_id'=>2,
            'title'=>'H1 N1 in ICU',
            'file'=>'file-1474929673-EXFLZ4.pdf',
            'date'=>'2016-09-26 22:41:13'
            ] );
                        
            Catdoc::create( [
            'id'=>17,
            'cat_id'=>2,
            'title'=>'Critical Illness Myopathy',
            'file'=>'file-1474929759-MTWRXY.pdf',
            'date'=>'2016-09-26 22:42:39'
            ] );
                        
            Catdoc::create( [
            'id'=>18,
            'cat_id'=>2,
            'title'=>'The Complete Story Case 4',
            'file'=>'file-1474929852-74UYPY.pdf',
            'date'=>'2016-09-26 22:44:12'
            ] );
                        
            Catdoc::create( [
            'id'=>19,
            'cat_id'=>2,
            'title'=>'The Complete Story Case 3',
            'file'=>'file-1474929892-R9GPBU.pdf',
            'date'=>'2016-09-26 22:44:52'
            ] );
                        
            Catdoc::create( [
            'id'=>20,
            'cat_id'=>2,
            'title'=>'The Complete Story Case 2',
            'file'=>'file-1474929927-RCMEWB.pdf',
            'date'=>'2016-09-26 22:45:27'
            ] );
                        
            Catdoc::create( [
            'id'=>21,
            'cat_id'=>2,
            'title'=>'The Complete Story Case 1',
            'file'=>'file-1474929954-W8W7YT.pdf',
            'date'=>'2016-09-26 22:45:54'
            ] );
                        
            Catdoc::create( [
            'id'=>22,
            'cat_id'=>2,
            'title'=>'daasdasdsad',
            'file'=>'file-1528438303-PMHAIH.pdf',
            'date'=>'2018-06-08 06:11:43'
            ] );
                        
            Catdoc::create( [
            'id'=>23,
            'cat_id'=>2,
            'title'=>'daasdasdsad',
            'file'=>'file-1528438492-D3T1FY.pdf',
            'date'=>'2018-06-08 06:14:52'
            ] );
                        
            Catdoc::create( [
            'id'=>25,
            'cat_id'=>2,
            'title'=>'admin',
            'file'=>'Admin giftcode reporting.pdf',
            'date'=>'2020-06-19 17:43:10'
            ] );
    }
}
