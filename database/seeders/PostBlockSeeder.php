<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use App\Models\PostBlock;

class PostBlockSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        PostBlock::create([
            'id' => 1,
            'post_type' => 0,
            'title' => "news",
            'block' => "block_type0"
        ]);

        PostBlock::create([
            'id' => 2,
            'post_type' => 1,
            'title' => "link on other site",
            'block' => "block_type1"
        ]);

        PostBlock::create([
            'id' => 3,
            'post_type' => 2,
            'title' => "research",
            'block' => "block_type2"
        ]);

        PostBlock::create([
            'id' => 4,
            'post_type' => 3,
            'title' => "presentation",
            'block' => "block_type3"
        ]);
    }
}
