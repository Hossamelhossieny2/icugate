<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use App\Models\HomeSlider;

class HomeSliderSeeder extends Seeder
{
    public function run()
    {
        HomeSlider::create([
            'id' => 1,
            'title' => "Optimism",
            'text' => "IS THE SECRET OF SUCCESS",
            'img' => "file-1466945762-ZZCU8X.jpg",
            'align' => "left",
        ]);

        HomeSlider::create([
            'id' => 2,
            'title' => "We Care for You",
            'text' => "Nothing goes better with your new summer suit than a sculpted booty and were showing you exactly how to get it The key to lifting toning and shaping your tush is all about completing targeted butt exercises two to three times a week.",
            'img' => "file-1466945777-SV92SM.jpg",
            'align' => "left",
        ]);

        HomeSlider::create([
            'id' => 3,
            'title' => "We Care for You",
            'text' => "Nothing goes better with your new summer suit than a sculpted booty and were showing you exactly how to get it The key to lifting toning and shaping your tush is all about completing targeted butt exercises two to three times a week.",
            'img' => "file-1466945792-FMR8GL.jpg",
            'align' => "right",
        ]);

        HomeSlider::create([
            'id' => 4,
            'title' => "We Care for You",
            'text' => "Nothing goes better with your new summer suit than a sculpted booty and were showing you exactly how to get it The key to lifting toning and shaping your tush is all about completing targeted butt exercises two to three times a week.",
            'img' => "file-1466945807-X2XT1C.jpg",
            'align' => "right",
        ]);
    }
}
