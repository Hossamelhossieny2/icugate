<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    public function run()
    {
        $this->call(HomeSliderSeeder::class);
        $this->call(ContactInfoSeeder::class);
        $this->call(PricingSeeder::class);
        $this->call(CatsSeeder::class);
        $this->call(CcatsSeeder::class);
        $this->call(CatDcsSeeder::class);
        $this->call(CourseCatSeeder::class);
        $this->call(YoutubeSeeder::class);
        $this->call(YoutubeChannelSeeder::class);
        $this->call(PageSeeder::class);
        $this->call(ConfigSeeder::class);
        $this->call(PayPalOrdersTypeSeeder::class);
        $this->call(TagsSeeder::class);
        $this->call(PostBlockSeeder::class);
        $this->call(CriticalSeeder::class);
        $this->call(CoureseSeeder::class);
        $this->call(TeamSeeder::class);
        $this->call(TeleConsultaionSeeder::class);
        $this->call(EventsSeeder::class);
        $this->call(PostImgSeeder::class);
        $this->call(PostTypeSeeder::class);
        $this->call(Posts1Seeder::class);
        $this->call(Posts2Seeder::class);
        $this->call(Posts3Seeder::class);
        $this->call(Posts4Seeder::class);
        $this->call(UserSeeder::class);
    }
}
