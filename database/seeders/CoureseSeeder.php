<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use App\Models\Course;

class CoureseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Course::create( [
            'id'=>12,
            'title'=>'BASIC Intensive Care Course in Luxor',
            'from'=>'2017-01-03',
            'to'=>'2017-01-04',
            'price'=>200.00,
            'exam_time'=>'2017-01-04',
            'exam_limit'=>0,
            'img'=>'file-1474554756-KABIVG.jpg',
            'description'=>'',
            'type'=>1,
            'deleted'=>0,
            'views'=>3223
            ] );


                        
            Course::create( [
            'id'=>21,
            'title'=>'4th Advanced Critical Care Nephrology/CRRT Course',
            'from'=>'2018-02-22',
            'to'=>'2018-02-23',
            'price'=>400.00,
            'exam_time'=>'2018-02-23',
            'exam_limit'=>0,
            'img'=>'file-1516699175-RJRLYA.jpg',
            'description'=>'The course Venue: King Fahd Military Medical Complex - Dhahran\r\nDate: February 22 and 23 (Thursday, Friday)\r\nThe course is fully affiliated with BASIC Collaboration and European society of Intensive Care Medicine (Prof. Ricardo Matos from ESICM will be available as a course facilitator), so international certificate will be issued for candidates who will pass the course exam as well as around15 CME hrs will be issued by the Saudi Commission of Health Specialties.',
            'type'=>1,
            'deleted'=>0,
            'views'=>1314
            ] );


                        
            Course::create( [
            'id'=>23,
            'title'=>'9th BASIC Intensive Care Medicine Course',
            'from'=>'2018-09-21',
            'to'=>'2018-09-22',
            'price'=>300.00,
            'exam_time'=>'2018-09-22',
            'exam_limit'=>0,
            'img'=>'file-1533482969-HHRPRP.jpg',
            'description'=>'The course is very valuable for both intensivists and non-intensivists (G. S. Internists, Cardiologists, anesthesiologists, OB/Gynaecologists) as well as senior nurses. It is considered as a refresher course for intensivists and an introduction to intensive care for non-intensivists.',
            'type'=>1,
            'deleted'=>0,
            'views'=>996
            ] );


                        
            Course::create( [
            'id'=>24,
            'title'=>'4th Advanced Critical Care Nutrition Course',
            'from'=>'2018-11-15',
            'to'=>'2018-11-16',
            'price'=>400.00,
            'exam_time'=>'2018-11-16',
            'exam_limit'=>0,
            'img'=>'file-1533483639-TM68ME.jpg',
            'description'=>'The ACCN course is very unique as it is 4th time to be done in the whole world and the 2nd time in Bahrain. It provides full comprehensive review from the basic to the very advanced level including all aspects of critical care nutrition in adults, paediatrics and neonates through answering 30 questions related to different categories of critically ill patients e.g. medical, surgical, cardiac, renal, oncology (both Enteral and Parenteral).',
            'type'=>1,
            'deleted'=>0,
            'views'=>1190
            ] );


                        
            Course::create( [
            'id'=>25,
            'title'=>'5th Advanced Critical Care Nutrition Course',
            'from'=>'2019-11-28',
            'to'=>'2019-11-30',
            'price'=>550.00,
            'exam_time'=>'2019-11-30',
            'exam_limit'=>0,
            'img'=>'file-1567881999-Q3IVQX.jpg',
            'description'=>'The ACCN course is very unique as it is 5th time to be done in the whole world and the 3rd time in Bahrain. It provides a full comprehensive review from the basic to the very advanced level including all aspects of critical care nutrition in adults, pediatrics and neonates through answering 30 questions related to different categories of critically ill patients e.g. medical, surgical, cardiac, renal, oncology (both Enteral and Parenteral).',
            'type'=>1,
            'deleted'=>0,
            'views'=>2359
            ] );

                        
            // Course::create( [
            // 'id'=>30,
            // 'title'=>'Fundamentals of Critical Care Nutrition EN Course',
            // 'from'=>'2020-09-13',
            // 'to'=>'2020-10-09',
            // 'price'=>1.00,
            // 'exam_time'=>'2020-10-10',
            // 'exam_limit'=>1,
            // 'img'=>'file_1599346749_Nestle Course Final.jpg',
            // 'description'=>'The course takes you through a full journey in critical care enteral nutrition starting from initiation till reaching the calories and protein targets. Also a couple of sessions about perioperative nutrition as well as the importance of fish oil.',
            // 'type'=>3,
            // 'deleted'=>0,
            // 'views'=>0
            // ] );


                        
            Course::create( [
            'id'=>31,
            'title'=>'6th Advanced Critical Care Nutrition Course	',
            'from'=>'2020-11-26',
            'to'=>'2020-11-28',
            'price'=>550.00,
            'exam_time'=>'2020-11-28',
            'exam_limit'=>1,
            'img'=>'file_1599739210_ACCN.jpg',
            'description'=>'The ACCN course is very unique as it is 6th time to be done in the whole world and the 4th time in Bahrain. It provides a full comprehensive review from the basic to the very advanced level including all aspects of critical care nutrition in adults, pediatrics, and neonates through answering 30 questions related to different categories of critically ill patients e.g. medical, surgical, cardiac, renal, oncology (both Enteral and Parenteral).',
            'type'=>1,
            'deleted'=>0,
            'views'=>1
            ] );
    }
}
