<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use App\Models\YoutubeChannel;

class YoutubeChannelSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        YoutubeChannel::create( [
            'id'=>1,
            'title'=>'Critical Care Educational Support Proffessional',
            'youtube_id'=>'UCWnd9BLErb6EuiflehtpWTQ'
            ] );


                        
            YoutubeChannel::create( [
            'id'=>2,
            'title'=>'ICUGATE LIVE Courses',
            'youtube_id'=>'UCxzDjxP8zb_OfEMe_zpI-qw'
            ] );


                        
            YoutubeChannel::create( [
            'id'=>3,
            'title'=>'ICUGATE ULTRASOUND',
            'youtube_id'=>'UCseynn8wtMKrMppABTWnErg'
            ] );
    }
}
