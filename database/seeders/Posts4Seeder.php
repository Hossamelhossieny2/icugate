<?php
namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use App\Models\Post;

class Posts4Seeder extends Seeder
{
   
    public function run()
    {

                                                                                        
        Post::create( [
'id'=>4180,
'cat_id'=>8,
'tag_id'=>6,
'title'=>'Asphyxia by Drowning Induces Massive Bleeding Due To Hyperfibrinolytic Disseminated Intravascular Coagulation',
'text'=>'Schwameis, Michael and others\r\nCritical Care Medicine:\r\nNovember 2015 - Volume 43 - Issue 11 - p 2394â€“2402',
'img'=>'file-1463591752-XK911T.jpg',
'file'=>NULL,
'link'=>'http://journals.lww.com/ccmjournal/Abstract/2015/11000/Asphyxia_by_Drowning_Induces_Massive_Bleeding_Due.16.aspx',
'by'=>0,
'journal'=>'Site Admin',
'type'=>0,
'month_article'=>0,
'famous'=>0,
'views'=>1101,
'show'=>0,
'date'=>'2015-11-01 08:35:49',
'active'=>'1',
'user'=>0,
'created_at'=>'2020-05-15 01:20:06',
'updated_at'=>'2020-06-21 00:08:23',
'deleted_at'=>NULL
] );



Post::create( [
'id'=>4181,
'cat_id'=>8,
'tag_id'=>6,
'title'=>'The Impact of Tracheostomy Timing on Clinical Outcome and Adverse Events in Poor-Grade Subarachnoid Hemorrhage',
'text'=>'Gessler, Florian and others\r\nCritical Care Medicine:\r\nNovember 2015 - Volume 43 - Issue 11 - p 2429â€“2438',
'img'=>'file-1463591752-XK911T.jpg',
'file'=>NULL,
'link'=>'http://journals.lww.com/ccmjournal/Abstract/2015/11000/The_Impact_of_Tracheostomy_Timing_on_Clinical.20.aspx',
'by'=>0,
'journal'=>'Site Admin',
'type'=>0,
'month_article'=>0,
'famous'=>0,
'views'=>1101,
'show'=>0,
'date'=>'2015-11-01 08:37:35',
'active'=>'1',
'user'=>0,
'created_at'=>'2020-05-15 01:20:06',
'updated_at'=>'2020-06-21 00:08:23',
'deleted_at'=>NULL
] );



Post::create( [
'id'=>4182,
'cat_id'=>8,
'tag_id'=>7,
'title'=>'Guidelines for the Appropriate Use of Bedside General and Cardiac Ultrasonography in the Evaluation of Critically Ill Patientsâ€”Part I: General Ultrasonography',
'text'=>'Frankel, Heidi L and others\r\nCritical Care Medicine:\r\nNovember 2015 - Volume 43 - Issue 11 - p 2479â€“2502',
'img'=>'file-1463591681-AGRM7H.jpg',
'file'=>NULL,
'link'=>'http://journals.lww.com/ccmjournal/Abstract/2015/11000/Guidelines_for_the_Appropriate_Use_of_Bedside.26.aspx',
'by'=>0,
'journal'=>'Site Admin',
'type'=>0,
'month_article'=>0,
'famous'=>0,
'views'=>1101,
'show'=>0,
'date'=>'2015-11-01 08:39:52',
'active'=>'1',
'user'=>0,
'created_at'=>'2020-05-15 01:20:06',
'updated_at'=>'2020-06-21 00:08:23',
'deleted_at'=>NULL
] );



Post::create( [
'id'=>4183,
'cat_id'=>12,
'tag_id'=>7,
'title'=>'European Resuscitation Council and European Society of Intensive Care Medicine 2015 guidelines for post-resuscitation care',
'text'=>'Jerry P. Nolan and others\r\nIntensive Care Med. Nov. 20154112:2039 - 2056',
'img'=>'file-1463591650-J14MMU.jpg',
'file'=>NULL,
'link'=>'http://icmjournal.esicm.org/journals/abstract.html?v=41&j=134&i=12&a=4051_10.1007_s00134-015-4051-3&doi=',
'by'=>0,
'journal'=>'Site Admin',
'type'=>0,
'month_article'=>0,
'famous'=>0,
'views'=>1212,
'show'=>0,
'date'=>'0000-00-00 00:00:00',
'active'=>'1',
'user'=>0,
'created_at'=>'2020-05-15 01:20:06',
'updated_at'=>'2020-06-21 00:08:23',
'deleted_at'=>NULL
] );



Post::create( [
'id'=>4184,
'cat_id'=>12,
'tag_id'=>7,
'title'=>'European Resuscitation Council and European Society of Intensive Care Medicine 2015 guidelines for post-resuscitation care',
'text'=>'Jerry P. Nolan and others\r\nIntensive Care Med. December 20154112:2039 - 2056',
'img'=>'file-1463591725-9J88SF.jpg',
'file'=>NULL,
'link'=>'http://icmjournal.esicm.org/journals/abstract.html?v=41&j=134&i=12&a=4051_10.1007_s00134-015-4051-3&doi=',
'by'=>0,
'journal'=>'Site Admin',
'type'=>0,
'month_article'=>0,
'famous'=>0,
'views'=>1212,
'show'=>0,
'date'=>'2015-11-03 09:05:52',
'active'=>'1',
'user'=>0,
'created_at'=>'2020-05-15 01:20:06',
'updated_at'=>'2020-06-21 00:08:23',
'deleted_at'=>NULL
] );



Post::create( [
'id'=>4185,
'cat_id'=>8,
'tag_id'=>1,
'title'=>'ICU-acquired candidemia within selective digestive decontamination studies: a meta-analysis',
'text'=>'James C. Hurley\r\nIntensive care Med. Nov. 2015 4111:1877 - 1885',
'img'=>'file-1463591644-ZGN2DA.jpg',
'file'=>NULL,
'link'=>'http://icmjournal.esicm.org/journals/abstract.html?v=41&j=134&i=11&a=4004_10.1007_s00134-015-4004-x&doi=',
'by'=>0,
'journal'=>'Site Admin',
'type'=>0,
'month_article'=>0,
'famous'=>0,
'views'=>1130,
'show'=>0,
'date'=>'2015-11-02 07:29:12',
'active'=>'1',
'user'=>0,
'created_at'=>'2020-05-15 01:20:06',
'updated_at'=>'2020-06-21 00:08:23',
'deleted_at'=>NULL
] );



Post::create( [
'id'=>4186,
'cat_id'=>8,
'tag_id'=>7,
'title'=>'Hemodynamic assessment of ventilated ICU patients with cardiorespiratory failure using a miniaturized multiplane transesophageal echocardiography probe',
'text'=>'Emmanuelle Begot and others\r\nIntensive care Med. Nov. 2015 4111:1877 - 1885',
'img'=>'file-1463591752-XK911T.jpg',
'file'=>NULL,
'link'=>'http://icmjournal.esicm.org/journals/abstract.html?v=41&j=134&i=11&a=3998_10.1007_s00134-015-3998-4&doi=',
'by'=>0,
'journal'=>'Site Admin',
'type'=>0,
'month_article'=>0,
'famous'=>0,
'views'=>1101,
'show'=>0,
'date'=>'2015-11-02 07:32:21',
'active'=>'1',
'user'=>0,
'created_at'=>'2020-05-15 01:20:06',
'updated_at'=>'2020-06-21 00:08:23',
'deleted_at'=>NULL
] );



Post::create( [
'id'=>4187,
'cat_id'=>8,
'tag_id'=>6,
'title'=>'Acute respiratory distress syndrome in patients with and without diffuse alveolar damage: an autopsy study',
'text'=>'JosÃ© A. Lorente and others\r\nIntensive care Med. Nov. 2015 4111:1877 - 1885',
'img'=>'file-1463591685-DDLMK9.jpg',
'file'=>NULL,
'link'=>'http://icmjournal.esicm.org/journals/abstract.html?v=41&j=134&i=11&a=4046_10.1007_s00134-015-4046-0&doi=',
'by'=>0,
'journal'=>'Site Admin',
'type'=>0,
'month_article'=>0,
'famous'=>0,
'views'=>1101,
'show'=>0,
'date'=>'2015-11-02 07:33:55',
'active'=>'1',
'user'=>0,
'created_at'=>'2020-05-15 01:20:06',
'updated_at'=>'2020-06-21 00:08:23',
'deleted_at'=>NULL
] );



Post::create( [
'id'=>4188,
'cat_id'=>8,
'tag_id'=>1,
'title'=>'Antifungal de-escalation was not associated with adverse outcome in critically ill patients treated for invasive candidiasis: post hoc analyses of the AmarCAND2 study data',
'text'=>'SÃ©bastien Bailly and others\r\nIntensive care Med. Nov. 2015 4111:1877 - 1885',
'img'=>'file-1463591681-AGRM7H.jpg',
'file'=>NULL,
'link'=>'http://icmjournal.esicm.org/journals/abstract.html?v=41&j=134&i=11&a=4053_10.1007_s00134-015-4053-1&doi=',
'by'=>0,
'journal'=>'Site Admin',
'type'=>0,
'month_article'=>0,
'famous'=>0,
'views'=>1130,
'show'=>0,
'date'=>'2015-11-02 07:35:36',
'active'=>'1',
'user'=>0,
'created_at'=>'2020-05-15 01:20:06',
'updated_at'=>'2020-06-21 00:08:23',
'deleted_at'=>NULL
] );



Post::create( [
'id'=>4189,
'cat_id'=>12,
'tag_id'=>1,
'title'=>'Did studies on HFOV fail to improve ARDS survival because they did not decrease VILI On the potential validity of a physiological concept enounced several decades ago',
'text'=>'Didier Dreyfuss, Jean-Damien Ricard, StÃ©phane Gaudry\r\nIntensive Care Med. December 20154112:2076 - 2086',
'img'=>'file-1463591752-XK911T.jpg',
'file'=>NULL,
'link'=>'http://icmjournal.esicm.org/journals/abstract.html?v=41&j=134&i=12&a=4062_10.1007_s00134-015-4062-0&doi=',
'by'=>0,
'journal'=>'Site Admin',
'type'=>0,
'month_article'=>0,
'famous'=>0,
'views'=>1241,
'show'=>0,
'date'=>'2015-11-05 07:43:35',
'active'=>'1',
'user'=>0,
'created_at'=>'2020-05-15 01:20:06',
'updated_at'=>'2020-06-21 00:08:23',
'deleted_at'=>NULL
] );



Post::create( [
'id'=>4190,
'cat_id'=>8,
'tag_id'=>10,
'title'=>'Effects of packed red blood cell storage duration on post-transfusion clinical outcomes: a meta-analysis and systematic review',
'text'=>'Monica Suet Ying Ng and others\r\nIntensive Care Med. December 20154112:2087-2097',
'img'=>'file-1463591650-J14MMU.jpg',
'file'=>NULL,
'link'=>'http://icmjournal.esicm.org/journals/abstract.html?v=41&j=134&i=12&a=4078_10.1007_s00134-015-4078-5&doi=',
'by'=>0,
'journal'=>'Site Admin',
'type'=>0,
'month_article'=>0,
'famous'=>0,
'views'=>1101,
'show'=>0,
'date'=>'2015-12-01 07:45:57',
'active'=>'1',
'user'=>0,
'created_at'=>'2020-05-15 01:20:06',
'updated_at'=>'2020-06-21 00:08:23',
'deleted_at'=>NULL
] );



Post::create( [
'id'=>4191,
'cat_id'=>12,
'tag_id'=>1,
'title'=>'Very high volume hemofiltration with the Cascade system in septic shock patients',
'text'=>'Jean-Pierre Quenot and others\r\nIntensive Care Med. December 20154112:2111-2120',
'img'=>'file-1463591685-DDLMK9.jpg',
'file'=>NULL,
'link'=>'http://icmjournal.esicm.org/journals/abstract.html?v=41&j=134&i=12&a=4056_10.1007_s00134-015-4056-y&doi=',
'by'=>0,
'journal'=>'Site Admin',
'type'=>0,
'month_article'=>0,
'famous'=>0,
'views'=>1241,
'show'=>0,
'date'=>'2015-11-05 07:49:54',
'active'=>'1',
'user'=>0,
'created_at'=>'2020-05-15 01:20:06',
'updated_at'=>'2020-06-21 00:08:23',
'deleted_at'=>NULL
] );



Post::create( [
'id'=>4192,
'cat_id'=>8,
'tag_id'=>8,
'title'=>'Citrate versus heparin anticoagulation for continuous renal replacement therapy: an updated meta-analysis of RCTs',
'text'=>'Ming Bai and others\r\nIntensive Care Med. December 20154112:2111-2120',
'img'=>'file-1463591685-DDLMK9.jpg',
'file'=>NULL,
'link'=>'http://icmjournal.esicm.org/journals/abstract.html?v=41&j=134&i=12&a=4099_10.1007_s00134-015-4099-0&doi=',
'by'=>0,
'journal'=>'Site Admin',
'type'=>0,
'month_article'=>0,
'famous'=>0,
'views'=>1152,
'show'=>0,
'date'=>'2015-11-01 07:56:41',
'active'=>'1',
'user'=>0,
'created_at'=>'2020-05-15 01:20:06',
'updated_at'=>'2020-06-21 00:08:23',
'deleted_at'=>NULL
] );



Post::create( [
'id'=>4193,
'cat_id'=>8,
'tag_id'=>7,
'title'=>'The CAHP Cardiac Arrest Hospital Prognosis score: a tool for risk stratification after out-of-hospital cardiac arrest',
'text'=>'Maupain C and others\r\nEur Heart J. 2015',
'img'=>'file-1463591725-9J88SF.jpg',
'file'=>NULL,
'link'=>'http://www.ntkinstitute.org/news/content.nsf/PaperFrameSet?OpenForm&pp=1&id=DF5950353CA4263E85256FDB00692E0A&refid=4702&specid=106&newsid=B85EEB7DA15FFAEB85257EF3002B982F&locref=ntkwatch&u=http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&db=PubMed&dopt=Abstract&list_uids=26497161',
'by'=>0,
'journal'=>'Site Admin',
'type'=>0,
'month_article'=>0,
'famous'=>0,
'views'=>1101,
'show'=>0,
'date'=>'2015-11-01 08:19:21',
'active'=>'1',
'user'=>0,
'created_at'=>'2020-05-15 01:20:06',
'updated_at'=>'2020-06-21 00:08:23',
'deleted_at'=>NULL
] );



Post::create( [
'id'=>4194,
'cat_id'=>8,
'tag_id'=>8,
'title'=>'Optimizing citrate dose for regional anticoagulation in continuous renal replacement therapy: measuring citrate concentrations instead of ionized calcium',
'text'=>'Honore P, Jacobs R, Hendrickx I, De Waele E, Van Gorp V, Spapen H\r\nCritical Care 2015, 19 :386 6 November 2015',
'img'=>'file-1463591725-9J88SF.jpg',
'file'=>NULL,
'link'=>'http://www.ccforum.com/content/19/1/386/abstract',
'by'=>0,
'journal'=>'Site Admin',
'type'=>0,
'month_article'=>0,
'famous'=>0,
'views'=>1152,
'show'=>0,
'date'=>'2015-11-06 07:15:25',
'active'=>'1',
'user'=>0,
'created_at'=>'2020-05-15 01:20:06',
'updated_at'=>'2020-06-21 00:08:23',
'deleted_at'=>NULL
] );



Post::create( [
'id'=>4195,
'cat_id'=>8,
'tag_id'=>7,
'title'=>'Qualitative real-time analysis by nurses of sublingual microcirculation in intensive care unit: the MICRONURSE study',
'text'=>'Tanaka S, Harrois A, NicolaÃ¯ C, Flores M, Hamada S, Vicaut E, Duranteau J\r\nCritical Care 2015, 19 :388 6 November 2015',
'img'=>'file-1463591639-4YG8JY.jpg',
'file'=>NULL,
'link'=>'http://www.ccforum.com/content/19/1/388/abstract',
'by'=>0,
'journal'=>'Site Admin',
'type'=>0,
'month_article'=>0,
'famous'=>0,
'views'=>1101,
'show'=>0,
'date'=>'2015-11-06 07:16:49',
'active'=>'1',
'user'=>0,
'created_at'=>'2020-05-15 01:20:06',
'updated_at'=>'2020-06-21 00:08:23',
'deleted_at'=>NULL
] );



Post::create( [
'id'=>4196,
'cat_id'=>8,
'tag_id'=>1,
'title'=>'Carbapenems versus alternative antibiotics for the treatment of bloodstream infections caused by Enterobacter, Citrobacter or Serratia species: a systematic review with meta-analysis',
'text'=>'Patrick N. A. Harris and others\r\nJ. Antimicrob. Chemother. published 4 November 2015',
'img'=>'file-1463591681-AGRM7H.jpg',
'file'=>NULL,
'link'=>'http://jac.oxfordjournals.org/content/early/2015/11/04/jac.dkv346.abstract?papetoc',
'by'=>0,
'journal'=>'Site Admin',
'type'=>0,
'month_article'=>0,
'famous'=>0,
'views'=>1130,
'show'=>0,
'date'=>'2015-11-05 07:33:26',
'active'=>'1',
'user'=>0,
'created_at'=>'2020-05-15 01:20:06',
'updated_at'=>'2020-06-21 00:08:23',
'deleted_at'=>NULL
] );



Post::create( [
'id'=>4197,
'cat_id'=>13,
'tag_id'=>3,
'title'=>'NUTRIC Score',
'text'=>'Critical Care Nutrition\r\nMarch 2013',
'img'=>'file-1463591681-AGRM7H.jpg',
'file'=>NULL,
'link'=>'http://www.criticalcarenutrition.com/docs/qi_tools/NUTRIC%20Score%201%20page%20summary_19March2013.pdf',
'by'=>0,
'journal'=>'Site Admin',
'type'=>0,
'month_article'=>0,
'famous'=>0,
'views'=>1101,
'show'=>0,
'date'=>'2013-03-13 06:17:57',
'active'=>'1',
'user'=>0,
'created_at'=>'2020-05-15 01:20:06',
'updated_at'=>'2020-06-21 00:08:23',
'deleted_at'=>NULL
] );



Post::create( [
'id'=>4198,
'cat_id'=>8,
'tag_id'=>7,
'title'=>'Continuous or Interrupted Chest Compressions for Cardiac Arrest',
'text'=>'Rudolph W. Koster, M.D., Ph.D.\r\nN ENGL J Med. November 9, 2015',
'img'=>'file-1463591685-DDLMK9.jpg',
'file'=>NULL,
'link'=>'http://www.nejm.org/doi/full/10.1056/NEJMe1513415',
'by'=>0,
'journal'=>'Site Admin',
'type'=>0,
'month_article'=>0,
'famous'=>0,
'views'=>1101,
'show'=>0,
'date'=>'2015-11-09 10:25:55',
'active'=>'1',
'user'=>0,
'created_at'=>'2020-05-15 01:20:06',
'updated_at'=>'2020-06-21 00:08:23',
'deleted_at'=>NULL
] );



Post::create( [
'id'=>4199,
'cat_id'=>8,
'tag_id'=>3,
'title'=>'Diabetes-specific enteral nutrition formula in hyperglycemic, mechanically ventilated, critically ill patients: a prospective, open-label, blind-randomized, multicenter study',
'text'=>'Mesejo A and others\r\nCritical Care 2015, 19 :390 9 November 2015',
'img'=>'file-1463591650-J14MMU.jpg',
'file'=>NULL,
'link'=>'http://www.ccforum.com/content/19/1/390/abstract',
'by'=>0,
'journal'=>'Site Admin',
'type'=>0,
'month_article'=>0,
'famous'=>0,
'views'=>1101,
'show'=>0,
'date'=>'2015-11-09 10:41:44',
'active'=>'1',
'user'=>0,
'created_at'=>'2020-05-15 01:20:06',
'updated_at'=>'2020-06-21 00:08:23',
'deleted_at'=>NULL
] );



Post::create( [
'id'=>4200,
'cat_id'=>8,
'tag_id'=>10,
'title'=>'Argatroban in the management of heparin-induced thrombocytopenia: a multicenter clinical trial',
'text'=>'Tardy-Poncet B and others\r\nCritical Care 2015, 19 :396 11 November 2015',
'img'=>'file-1463591732-SM39NY.jpg',
'file'=>NULL,
'link'=>'http://www.ccforum.com/content/19/1/396/abstract',
'by'=>0,
'journal'=>'Site Admin',
'type'=>0,
'month_article'=>0,
'famous'=>0,
'views'=>1101,
'show'=>0,
'date'=>'2015-11-11 11:32:30',
'active'=>'1',
'user'=>0,
'created_at'=>'2020-05-15 01:20:06',
'updated_at'=>'2020-06-21 00:08:23',
'deleted_at'=>NULL
] );



Post::create( [
'id'=>4201,
'cat_id'=>8,
'tag_id'=>7,
'title'=>'Considerations for initial therapy in the treatment of acute heart failure',
'text'=>'Peacock W, Cannon C, Singer A, Hiestand B\r\nCritical Care 2015, 19 :399 10 November 2015',
'img'=>'file-1463591746-GEE3E7.jpg',
'file'=>NULL,
'link'=>'http://www.ccforum.com/content/19/1/399',
'by'=>0,
'journal'=>'Site Admin',
'type'=>0,
'month_article'=>0,
'famous'=>1,
'views'=>1196,
'show'=>0,
'date'=>'2015-11-10 11:33:31',
'active'=>'1',
'user'=>0,
'created_at'=>'2020-05-15 01:20:06',
'updated_at'=>'2020-06-21 00:08:23',
'deleted_at'=>NULL
] );



Post::create( [
'id'=>4202,
'cat_id'=>8,
'tag_id'=>7,
'title'=>'Effects of passive leg raising and volume expansion on mean systemic pressure and venous return in shock in humans',
'text'=>'GuÃ©rin L, Teboul J, Persichini R, Dres M, Richard C, Monnet X\r\nCritical Care 2015, 19 :411 23 November 2015',
'img'=>'file-1463591681-AGRM7H.jpg',
'file'=>NULL,
'link'=>'http://www.ccforum.com/content/19/1/411/abstract',
'by'=>0,
'journal'=>'Site Admin',
'type'=>0,
'month_article'=>0,
'famous'=>0,
'views'=>1101,
'show'=>0,
'date'=>'2015-11-23 11:36:24',
'active'=>'1',
'user'=>0,
'created_at'=>'2020-05-15 01:20:06',
'updated_at'=>'2020-06-21 00:08:23',
'deleted_at'=>NULL
] );



Post::create( [
'id'=>4203,
'cat_id'=>9,
'tag_id'=>4,
'title'=>'Clinician Update\r\nFat Embolism Syndrome',
'text'=>'Ethan Kosova, Brian Bergmark, Gregory Piazza\r\nCirculation.Nov. 24,2015131:317-320',
'img'=>'file-1463591732-SM39NY.jpg',
'file'=>NULL,
'link'=>'http://circ.ahajournals.org/content/131/3/317.full',
'by'=>0,
'journal'=>'Site Admin',
'type'=>0,
'month_article'=>0,
'famous'=>0,
'views'=>1198,
'show'=>0,
'date'=>'2015-11-24 06:06:06',
'active'=>'1',
'user'=>0,
'created_at'=>'2020-05-15 01:20:06',
'updated_at'=>'2020-06-21 00:08:23',
'deleted_at'=>NULL
] );



Post::create( [
'id'=>4204,
'cat_id'=>12,
'tag_id'=>4,
'title'=>'Clinician Update Fat Embolism Syndrome',
'text'=>'Ethan Kosova, Brian Bergmark, Gregory Piazza\r\nCirculation.Nov. 24,2015131:317-320',
'img'=>'file-1463591725-9J88SF.jpg',
'file'=>NULL,
'link'=>'http://circ.ahajournals.org/content/131/3/317.full',
'by'=>0,
'journal'=>'Site Admin',
'type'=>0,
'month_article'=>0,
'famous'=>0,
'views'=>1309,
'show'=>0,
'date'=>'2015-11-25 06:07:11',
'active'=>'1',
'user'=>0,
'created_at'=>'2020-05-15 01:20:06',
'updated_at'=>'2020-06-21 00:08:23',
'deleted_at'=>NULL
] );



Post::create( [
'id'=>4205,
'cat_id'=>12,
'tag_id'=>7,
'title'=>'A Randomized Trial of Intensive versus Standard Blood-Pressure Control',
'text'=>'The SPRINT Research Group\r\nN Engl J Med 2015373:2103-2116, Published Online November 9, 2015',
'img'=>'file-1463591738-IZ3XFW.jpg',
'file'=>NULL,
'link'=>'http://www.nejm.org/doi/full/10.1056/NEJMoa1511939',
'by'=>0,
'journal'=>'Site Admin',
'type'=>0,
'month_article'=>0,
'famous'=>0,
'views'=>1212,
'show'=>0,
'date'=>'2015-11-09 08:22:51',
'active'=>'1',
'user'=>0,
'created_at'=>'2020-05-15 01:20:06',
'updated_at'=>'2020-06-21 00:08:23',
'deleted_at'=>NULL
] );



Post::create( [
'id'=>4206,
'cat_id'=>8,
'tag_id'=>7,
'title'=>'Redefining Blood-Pressure Targets â€” SPRINT Starts the Marathon',
'text'=>'V. Perkovic and A. Rodgers\r\nThe SPRINT Research Group\r\nN Engl J Med 2015 373:2175-2178, November 26, 2015',
'img'=>'file-1463591639-4YG8JY.jpg',
'file'=>NULL,
'link'=>'http://www.nejm.org/doi/full/10.1056/NEJMe1513301',
'by'=>0,
'journal'=>'Site Admin',
'type'=>0,
'month_article'=>0,
'famous'=>0,
'views'=>1101,
'show'=>0,
'date'=>'2015-11-26 08:25:37',
'active'=>'1',
'user'=>0,
'created_at'=>'2020-05-15 01:20:06',
'updated_at'=>'2020-06-21 00:08:23',
'deleted_at'=>NULL
] );



Post::create( [
'id'=>4207,
'cat_id'=>8,
'tag_id'=>4,
'title'=>'INTERACTION EFFECTS OF MULTIPLE COMPLICATIONS ON POSTOPERATIVE MORTALITY IN GENERAL SURGERY',
'text'=>'Kim, Minjae Li, Guohua\r\nCritical Care Medicine:\r\nDecember 2015 - Volume 43 - Issue 12 - p 1',
'img'=>'file-1463591644-ZGN2DA.jpg',
'file'=>NULL,
'link'=>'http://journals.lww.com/ccmjournal/Citation/2015/12001/3___INTERACTION_EFFECTS_OF_MULTIPLE_COMPLICATIONS.6.aspx',
'by'=>0,
'journal'=>'Site Admin',
'type'=>0,
'month_article'=>0,
'famous'=>0,
'views'=>1198,
'show'=>0,
'date'=>'2015-12-02 07:31:33',
'active'=>'1',
'user'=>0,
'created_at'=>'2020-05-15 01:20:06',
'updated_at'=>'2020-06-21 00:08:23',
'deleted_at'=>NULL
] );



Post::create( [
'id'=>4208,
'cat_id'=>8,
'tag_id'=>10,
'title'=>'FOUR-FACTOR PROTHROMBIN COMPLEX CONCENTRATE FOR LIFE-THREATENING BLEEDS OR EMERGENT SURGERY',
'text'=>'Sin, Jonathan Berger, Karen Lesch, Christine\r\nCritical Care Medicine:\r\nDecember 2015 - Volume 43 - Issue 12 - p 1â€“2',
'img'=>'file-1463591732-SM39NY.jpg',
'file'=>NULL,
'link'=>'http://journals.lww.com/ccmjournal/Citation/2015/12001/4___FOUR_FACTOR_PROTHROMBIN_COMPLEX_CONCENTRATE.7.aspx',
'by'=>0,
'journal'=>'Site Admin',
'type'=>0,
'month_article'=>0,
'famous'=>0,
'views'=>1101,
'show'=>0,
'date'=>'2015-12-02 07:32:53',
'active'=>'1',
'user'=>0,
'created_at'=>'2020-05-15 01:20:06',
'updated_at'=>'2020-06-21 00:08:23',
'deleted_at'=>NULL
] );



Post::create( [
'id'=>4209,
'cat_id'=>8,
'tag_id'=>7,
'title'=>'Pharmacodynamic Analysis of a Fluid Challenge',
'text'=>'Aya, Hollmann D and others\r\nCritical Care Medicine., Post Author Corrections: December 17, 2015',
'img'=>'file-1463591732-SM39NY.jpg',
'file'=>NULL,
'link'=>'http://journals.lww.com/ccmjournal/Abstract/publishahead/Pharmacodynamic_Analysis_of_a_Fluid_Challenge_.97036.aspx',
'by'=>0,
'journal'=>'Site Admin',
'type'=>0,
'month_article'=>0,
'famous'=>0,
'views'=>1101,
'show'=>0,
'date'=>'2015-12-17 07:35:00',
'active'=>'1',
'user'=>0,
'created_at'=>'2020-05-15 01:20:06',
'updated_at'=>'2020-06-21 00:08:23',
'deleted_at'=>NULL
] );



Post::create( [
'id'=>4210,
'cat_id'=>8,
'tag_id'=>10,
'title'=>'Is Thrombocytopenia an Early Prognostic Marker in Septic Shock',
'text'=>'Thiery-Antier and others\r\nCritical Care Medicine., Post Author Corrections: December 14, 2015',
'img'=>'file-1463591685-DDLMK9.jpg',
'file'=>NULL,
'link'=>'http://journals.lww.com/ccmjournal/Abstract/publishahead/Is_Thrombocytopenia_an_Early_Prognostic_Marker_in.97038.aspx',
'by'=>0,
'journal'=>'Site Admin',
'type'=>0,
'month_article'=>0,
'famous'=>0,
'views'=>1101,
'show'=>0,
'date'=>'2015-12-14 07:36:07',
'active'=>'1',
'user'=>0,
'created_at'=>'2020-05-15 01:20:06',
'updated_at'=>'2020-06-21 00:08:23',
'deleted_at'=>NULL
] );



Post::create( [
'id'=>4211,
'cat_id'=>8,
'tag_id'=>7,
'title'=>'Effect of Perioperative Goal-Directed Hemodynamic Resuscitation Therapy on Outcomes Following Cardiac Surgery: A Randomized Clinical Trial and Systematic Review',
'text'=>'Osawa, Eduardo A and others\r\nCritical Care Medicine., Post Author Corrections: December 7, 2015',
'img'=>'file-1463591746-GEE3E7.jpg',
'file'=>NULL,
'link'=>'http://journals.lww.com/ccmjournal/Abstract/publishahead/Effect_of_Perioperative_Goal_Directed_Hemodynamic.97049.aspx',
'by'=>0,
'journal'=>'Site Admin',
'type'=>0,
'month_article'=>0,
'famous'=>0,
'views'=>1101,
'show'=>0,
'date'=>'2015-12-07 07:37:32',
'active'=>'1',
'user'=>0,
'created_at'=>'2020-05-15 01:20:06',
'updated_at'=>'2020-06-21 00:08:23',
'deleted_at'=>NULL
] );



Post::create( [
'id'=>4212,
'cat_id'=>8,
'tag_id'=>7,
'title'=>'Hemostatic Changes During Extracorporeal Membrane Oxygenation: A Prospective Randomized Clinical Trial Comparing Three Different Extracorporeal Membrane Oxygenation Systems.',
'text'=>'Malfertheiner, Maximilian V and others\r\nCritical Care Medicine., Post Author Corrections: December 9, 2015',
'img'=>'file-1463591732-SM39NY.jpg',
'file'=>NULL,
'link'=>'http://journals.lww.com/ccmjournal/Abstract/publishahead/Hemostatic_Changes_During_Extracorporeal_Membrane.97051.aspx',
'by'=>0,
'journal'=>'Site Admin',
'type'=>0,
'month_article'=>0,
'famous'=>0,
'views'=>1101,
'show'=>0,
'date'=>'2015-12-09 07:38:38',
'active'=>'1',
'user'=>0,
'created_at'=>'2020-05-15 01:20:06',
'updated_at'=>'2020-06-21 00:08:23',
'deleted_at'=>NULL
] );



Post::create( [
'id'=>4213,
'cat_id'=>8,
'tag_id'=>7,
'title'=>'A Selective V1A Receptor Agonist, Selepressin, Is Superior to Arginine Vasopressin and to Norepinephrine in Ovine Septic Shock',
'text'=>'He, Xinrong and others\r\nCritical Care Medicine:\r\nJanuary 2016 - Volume 44 - Issue 1 - p 23â€“31',
'img'=>'file-1463591681-AGRM7H.jpg',
'file'=>NULL,
'link'=>'http://journals.lww.com/ccmjournal/Fulltext/2016/01000/A_Selective_V1A_Receptor_Agonist,_Selepressin,_Is.4.aspx',
'by'=>0,
'journal'=>'Site Admin',
'type'=>0,
'month_article'=>0,
'famous'=>0,
'views'=>1101,
'show'=>0,
'date'=>'2016-01-01 07:43:13',
'active'=>'1',
'user'=>0,
'created_at'=>'2020-05-15 01:20:06',
'updated_at'=>'2020-06-21 00:08:23',
'deleted_at'=>NULL
] );



Post::create( [
'id'=>4214,
'cat_id'=>8,
'tag_id'=>7,
'title'=>'A Selective V1A Receptor Agonist, Selepressin, Is Superior to Arginine Vasopressin and to Norepinephrine in Ovine Septic Shock',
'text'=>'He, Xinrong and others\r\nCritical Care Medicine:\r\nJanuary 2016 - Volume 44 - Issue 1 - p 23â€“31',
'img'=>'file-1463591681-AGRM7H.jpg',
'file'=>NULL,
'link'=>'http://journals.lww.com/ccmjournal/Fulltext/2016/01000/A_Selective_V1A_Receptor_Agonist,_Selepressin,_Is.4.aspx',
'by'=>0,
'journal'=>'Site Admin',
'type'=>0,
'month_article'=>0,
'famous'=>1,
'views'=>1196,
'show'=>0,
'date'=>'2016-01-01 07:49:55',
'active'=>'1',
'user'=>0,
'created_at'=>'2020-05-15 01:20:06',
'updated_at'=>'2020-06-21 00:08:23',
'deleted_at'=>NULL
] );



Post::create( [
'id'=>4215,
'cat_id'=>8,
'tag_id'=>6,
'title'=>'Open Lung Approach for the Acute Respiratory Distress Syndrome: A Pilot, Randomized Controlled Trial',
'text'=>'Kacmarek, Robert M and others\r\nCritical Care Medicine:\r\nJanuary 2016 - Volume 44 - Issue 1 - p 32â€“42',
'img'=>'file-1463591738-IZ3XFW.jpg',
'file'=>NULL,
'link'=>'http://journals.lww.com/ccmjournal/Abstract/2016/01000/Open_Lung_Approach_for_the_Acute_Respiratory.5.aspx',
'by'=>0,
'journal'=>'Site Admin',
'type'=>0,
'month_article'=>0,
'famous'=>0,
'views'=>1101,
'show'=>0,
'date'=>'2016-01-01 07:53:13',
'active'=>'1',
'user'=>0,
'created_at'=>'2020-05-15 01:20:06',
'updated_at'=>'2020-06-21 00:08:23',
'deleted_at'=>NULL
] );



Post::create( [
'id'=>4216,
'cat_id'=>8,
'tag_id'=>11,
'title'=>'Delayed Rapid Response Team Activation Is Associated With Increased Hospital Mortality, Morbidity, and Length of Stay in a Tertiary Care Institution',
'text'=>'Barwise, Amelia and others\r\nCritical Care Medicine:\r\nJanuary 2016 - Volume 44 - Issue 1 - p 54â€“63',
'img'=>'file-1463591650-J14MMU.jpg',
'file'=>NULL,
'link'=>'http://journals.lww.com/ccmjournal/Abstract/2016/01000/Delayed_Rapid_Response_Team_Activation_Is.7.aspx',
'by'=>0,
'journal'=>'Site Admin',
'type'=>0,
'month_article'=>0,
'famous'=>0,
'views'=>1101,
'show'=>0,
'date'=>'2016-01-01 08:01:09',
'active'=>'1',
'user'=>0,
'created_at'=>'2020-05-15 01:20:06',
'updated_at'=>'2020-06-21 00:08:23',
'deleted_at'=>NULL
] );



Post::create( [
'id'=>4217,
'cat_id'=>8,
'tag_id'=>6,
'title'=>'Volume Delivered During Recruitment Maneuver Predicts Lung Stress in Acute Respiratory Distress Syndrome',
'text'=>'Beitler, Jeremy R and others\r\nCritical Care Medicine:\r\nJanuary 2016 - Volume 44 - Issue 1 - p 91â€“99',
'img'=>'file-1463591725-9J88SF.jpg',
'file'=>NULL,
'link'=>'http://journals.lww.com/ccmjournal/Abstract/2016/01000/Volume_Delivered_During_Recruitment_Maneuver.11.aspx',
'by'=>0,
'journal'=>'Site Admin',
'type'=>0,
'month_article'=>0,
'famous'=>0,
'views'=>1101,
'show'=>0,
'date'=>'2016-01-01 08:02:48',
'active'=>'1',
'user'=>0,
'created_at'=>'2020-05-15 01:20:06',
'updated_at'=>'2020-06-21 00:08:23',
'deleted_at'=>NULL
] );



Post::create( [
'id'=>4218,
'cat_id'=>8,
'tag_id'=>6,
'title'=>'Timing of Intubation and Clinical Outcomes in Adults With Acute Respiratory Distress Syndrome',
'text'=>'Kangelaris, Kirsten Neudoerffer and others\r\nCritical Care Medicine:\r\nJanuary 2016 - Volume 44 - Issue 1 - p 120â€“129',
'img'=>'file-1463591685-DDLMK9.jpg',
'file'=>NULL,
'link'=>'http://journals.lww.com/ccmjournal/Abstract/2016/01000/Timing_of_Intubation_and_Clinical_Outcomes_in.14.aspx',
'by'=>0,
'journal'=>'Site Admin',
'type'=>0,
'month_article'=>0,
'famous'=>0,
'views'=>1101,
'show'=>0,
'date'=>'2016-01-01 08:07:08',
'active'=>'1',
'user'=>0,
'created_at'=>'2020-05-15 01:20:06',
'updated_at'=>'2020-06-21 00:08:23',
'deleted_at'=>NULL
] );



Post::create( [
'id'=>4219,
'cat_id'=>8,
'tag_id'=>5,
'title'=>'23.4% Saline Decreases Brain Tissue Volume in Severe Hepatic Encephalopathy as Assessed by a Quantitative CT Marker',
'text'=>'Liotta, Eric M and others\r\nCritical Care Medicine:\r\nJanuary 2016 - Volume 44 - Issue 1 - p 171â€“179',
'img'=>'file-1463591639-4YG8JY.jpg',
'file'=>NULL,
'link'=>'http://journals.lww.com/ccmjournal/Abstract/2016/01000/23_4__Saline_Decreases_Brain_Tissue_Volume_in.20.aspx',
'by'=>0,
'journal'=>'Site Admin',
'type'=>0,
'month_article'=>0,
'famous'=>0,
'views'=>1114,
'show'=>0,
'date'=>'2016-01-01 08:08:32',
'active'=>'1',
'user'=>0,
'created_at'=>'2020-05-15 01:20:06',
'updated_at'=>'2020-06-21 00:08:23',
'deleted_at'=>NULL
] );



Post::create( [
'id'=>4220,
'cat_id'=>8,
'tag_id'=>11,
'title'=>'Excellence in Intensive Care Medicine',
'text'=>'Sprung, Charles L and others\r\nCritical Care Medicine:\r\nJanuary 2016 - Volume 44 - Issue 1 - p 202â€“206',
'img'=>'file-1463591650-J14MMU.jpg',
'file'=>NULL,
'link'=>'http://journals.lww.com/ccmjournal/Abstract/2016/01000/Excellence_in_Intensive_Care_Medicine.23.aspx',
'by'=>0,
'journal'=>'Site Admin',
'type'=>0,
'month_article'=>0,
'famous'=>0,
'views'=>1101,
'show'=>0,
'date'=>'2016-01-01 08:10:48',
'active'=>'1',
'user'=>0,
'created_at'=>'2020-05-15 01:20:06',
'updated_at'=>'2020-06-21 00:08:23',
'deleted_at'=>NULL
] );



Post::create( [
'id'=>4221,
'cat_id'=>8,
'tag_id'=>7,
'title'=>'Extracorporeal membrane oxygenation ECMO for critically ill adults in the emergency department: history, current applications, and future directions',
'text'=>'Mosier J, Kelsey M, Raz Y, Gunnerson K, Meyer R, Hypes C, Malo J, Whitmore S, Spaite D\r\nCritical Care 2015, 19 :431 17 December 2015',
'img'=>'file-1463591639-4YG8JY.jpg',
'file'=>NULL,
'link'=>'http://www.ccforum.com/content/19/1/431',
'by'=>0,
'journal'=>'Site Admin',
'type'=>0,
'month_article'=>0,
'famous'=>1,
'views'=>1196,
'show'=>0,
'date'=>'2015-12-17 08:15:36',
'active'=>'1',
'user'=>0,
'created_at'=>'2020-05-15 01:20:06',
'updated_at'=>'2020-06-21 00:08:23',
'deleted_at'=>NULL
] );



Post::create( [
'id'=>4222,
'cat_id'=>8,
'tag_id'=>6,
'title'=>'Should Reinke edema be considered a contributing factor to post-extubation failure',
'text'=>'Andrea Cortegiani, Vincenzo Russotto, Santi Maurizio Raineri and Antonino Giarratano\r\nCritical Care 2015, 19:430 19 December 2015',
'img'=>'file-1463591732-SM39NY.jpg',
'file'=>NULL,
'link'=>'http://www.ccforum.com/content/19/1/430',
'by'=>0,
'journal'=>'Site Admin',
'type'=>0,
'month_article'=>0,
'famous'=>0,
'views'=>1101,
'show'=>0,
'date'=>'2015-12-19 08:17:11',
'active'=>'1',
'user'=>0,
'created_at'=>'2020-05-15 01:20:06',
'updated_at'=>'2020-06-21 00:08:23',
'deleted_at'=>NULL
] );



Post::create( [
'id'=>4223,
'cat_id'=>8,
'tag_id'=>6,
'title'=>'Elevated soluble thrombomodulin is associated with organ failure and mortality in children with acute respiratory distress syndrome ARDS: a prospective observational cohort study',
'text'=>'Orwoll BE, Spicer AC, Zinter MS, Alkhouli MF, Khemani RG, Flori HR, Neuhaus JM, Calfee CS et al.\r\nCritical Care 2015, 19:435 14 December 2015',
'img'=>'file-1463591752-XK911T.jpg',
'file'=>NULL,
'link'=>'http://www.ccforum.com/content/19/1/435',
'by'=>0,
'journal'=>'Site Admin',
'type'=>0,
'month_article'=>0,
'famous'=>0,
'views'=>1101,
'show'=>0,
'date'=>'0000-00-00 00:00:00',
'active'=>'1',
'user'=>0,
'created_at'=>'2020-05-15 01:20:06',
'updated_at'=>'2020-06-21 00:08:23',
'deleted_at'=>NULL
] );



Post::create( [
'id'=>4224,
'cat_id'=>8,
'tag_id'=>7,
'title'=>'Effects of different types of fluid resuscitation for hemorrhagic shock on splanchnic organ microcirculation and renal reactive oxygen species formation',
'text'=>'Wu CY, Chan KC, Cheng YJ, Yeh YC, Chien CT and on behalf of the NTUH Center of Microcirculation Medical Research NCMMR\r\nCritical Care 2015, 19:434 11 December 2015',
'img'=>'file-1463591732-SM39NY.jpg',
'file'=>NULL,
'link'=>'http://www.ccforum.com/content/19/1/434',
'by'=>0,
'journal'=>'Site Admin',
'type'=>0,
'month_article'=>0,
'famous'=>0,
'views'=>1101,
'show'=>0,
'date'=>'2015-12-11 08:22:52',
'active'=>'1',
'user'=>0,
'created_at'=>'2020-05-15 01:20:06',
'updated_at'=>'2020-06-21 00:08:23',
'deleted_at'=>NULL
] );



Post::create( [
'id'=>4225,
'cat_id'=>8,
'tag_id'=>6,
'title'=>'Timing of tracheotomy in ICU patients: a systematic review of randomized controlled trials',
'text'=>'Koji Hosokawa, Masaji Nishimura, Moritoki Egi, Jean-Louis Vincent\r\nCritical Care 2015, 19:424 4 December 2015',
'img'=>'file-1463591650-J14MMU.jpg',
'file'=>NULL,
'link'=>'http://www.ccforum.com/content/19/1/424',
'by'=>0,
'journal'=>'Site Admin',
'type'=>0,
'month_article'=>0,
'famous'=>0,
'views'=>1101,
'show'=>0,
'date'=>'2015-12-04 08:24:31',
'active'=>'1',
'user'=>0,
'created_at'=>'2020-05-15 01:20:06',
'updated_at'=>'2020-06-21 00:08:23',
'deleted_at'=>NULL
] );



Post::create( [
'id'=>4226,
'cat_id'=>8,
'tag_id'=>7,
'title'=>'The effect of mild induced hypothermia on outcomes of patients after cardiac arrest: a systematic review and meta-analysis of randomised controlled trials',
'text'=>'Xi Zhang, Jian Xie, Jian Chen, Ying Huang, Feng Guo, Yi Yang, Hai Qiu\r\nCritical Care 2015, 19:417 1 December 2015',
'img'=>'file-1463591746-GEE3E7.jpg',
'file'=>NULL,
'link'=>'http://www.ccforum.com/content/19/1/417',
'by'=>0,
'journal'=>'Site Admin',
'type'=>0,
'month_article'=>0,
'famous'=>0,
'views'=>1101,
'show'=>0,
'date'=>'2015-12-01 08:25:57',
'active'=>'1',
'user'=>0,
'created_at'=>'2020-05-15 01:20:06',
'updated_at'=>'2020-06-21 00:08:23',
'deleted_at'=>NULL
] );



Post::create( [
'id'=>4227,
'cat_id'=>8,
'tag_id'=>5,
'title'=>'Accuracy of intracranial pressure monitoring: systematic review and meta-analysis',
'text'=>'Lucia Zacchetti, Sandra Magnoni, Federica Di Corte, Elisa Zanier, Nino Stocchetti\r\nCritical Care 2015, 19:420 2 December 2015',
'img'=>'file-1463591725-9J88SF.jpg',
'file'=>NULL,
'link'=>'http://www.ccforum.com/content/19/1/420',
'by'=>0,
'journal'=>'Site Admin',
'type'=>0,
'month_article'=>0,
'famous'=>0,
'views'=>1114,
'show'=>0,
'date'=>'2015-12-02 08:27:05',
'active'=>'1',
'user'=>0,
'created_at'=>'2020-05-15 01:20:06',
'updated_at'=>'2020-06-21 00:08:23',
'deleted_at'=>NULL
] );



Post::create( [
'id'=>4228,
'cat_id'=>8,
'tag_id'=>6,
'title'=>'Paracetamol for flu Its pointless, say scientists: Popular drug neither reduces fever nor aches and pains',
'text'=>'By STEPHEN ADAMS HEALTH CORRESPONDENT FOR THE MAIL ON SUNDAY\nPUBLISHED: 01:58 GMT, 13 December 2015  UPDATED: 11:41 GMT, 14 December 2015',
'img'=>'file-1463591752-XK911T.jpg',
'file'=>NULL,
'link'=>'http://www.univadis.org/medical-news/53/Paracetamol-may-not-be-a-suitable-treatment-for-influenza?utm_source=newsletter+email&utm_medium=email&utm_campaign=medical+updates+best+of+-+weekly&utm_content=532848&utm_term=automated_bestof_weekly',
'by'=>0,
'journal'=>'Site Admin',
'type'=>0,
'month_article'=>0,
'famous'=>0,
'views'=>1101,
'show'=>0,
'date'=>'2015-12-14 08:31:36',
'active'=>'1',
'user'=>0,
'created_at'=>'2020-05-15 01:20:06',
'updated_at'=>'2020-06-21 00:08:23',
'deleted_at'=>NULL
] );



Post::create( [
'id'=>4229,
'cat_id'=>8,
'tag_id'=>5,
'title'=>'The reliability of the Glasgow Coma Scale: a systematic review',
'text'=>'Florence C. M. Reith and others\r\nIntensive Care Med. Jan 2015421:3 - 15',
'img'=>'file-1463591657-K8BRDF.jpg',
'file'=>NULL,
'link'=>'http://icmjournal.esicm.org/journals/abstract.html?v=42&j=134&i=1&a=4124_10.1007_s00134-015-4124-3&doi=',
'by'=>0,
'journal'=>'Site Admin',
'type'=>0,
'month_article'=>0,
'famous'=>0,
'views'=>1114,
'show'=>0,
'date'=>'2015-01-01 08:35:21',
'active'=>'1',
'user'=>0,
'created_at'=>'2020-05-15 01:20:06',
'updated_at'=>'2020-06-21 00:08:23',
'deleted_at'=>NULL
] );



Post::create( [
'id'=>4230,
'cat_id'=>8,
'tag_id'=>5,
'title'=>'The reliability of the Glasgow Coma Scale: a systematic review',
'text'=>'Florence C. M. Reith and others\r\nIntensive Care Med. Jan 2015421:3 - 15',
'img'=>'file-1463591746-GEE3E7.jpg',
'file'=>NULL,
'link'=>'http://icmjournal.esicm.org/journals/abstract.html?v=42&j=134&i=1&a=4124_10.1007_s00134-015-4124-3&doi=',
'by'=>0,
'journal'=>'Site Admin',
'type'=>0,
'month_article'=>0,
'famous'=>0,
'views'=>1114,
'show'=>0,
'date'=>'2016-01-01 08:35:39',
'active'=>'1',
'user'=>0,
'created_at'=>'2020-05-15 01:20:06',
'updated_at'=>'2020-06-21 00:08:23',
'deleted_at'=>NULL
] );



Post::create( [
'id'=>4231,
'cat_id'=>8,
'tag_id'=>9,
'title'=>'Cholestatic liver dysfunction during sepsis and other critical illnesses',
'text'=>'Marc Jenniskens and others\r\nIntensive Care Med. Jan 2015421:16 - 27',
'img'=>'file-1463591657-K8BRDF.jpg',
'file'=>NULL,
'link'=>'http://icmjournal.esicm.org/journals/abstract.html?v=42&j=134&i=1&a=4054_10.1007_s00134-015-4054-0&doi=',
'by'=>0,
'journal'=>'Site Admin',
'type'=>0,
'month_article'=>0,
'famous'=>0,
'views'=>1101,
'show'=>0,
'date'=>'2016-01-01 08:37:27',
'active'=>'1',
'user'=>0,
'created_at'=>'2020-05-15 01:20:06',
'updated_at'=>'2020-06-21 00:08:23',
'deleted_at'=>NULL
] );



Post::create( [
'id'=>4232,
'cat_id'=>8,
'tag_id'=>6,
'title'=>'Lung ultrasound training: curriculum implementation and learning trajectory among respiratory therapists',
'text'=>'K. C. See and others\r\nIntensive Care Med. Jan 2015421:63 - 71',
'img'=>'file-1463591650-J14MMU.jpg',
'file'=>NULL,
'link'=>'http://icmjournal.esicm.org/journals/abstract.html?v=42&j=134&i=1&a=4102_10.1007_s00134-015-4102-9&doi=',
'by'=>0,
'journal'=>'Site Admin',
'type'=>0,
'month_article'=>0,
'famous'=>0,
'views'=>1101,
'show'=>0,
'date'=>'2016-01-01 08:38:42',
'active'=>'1',
'user'=>0,
'created_at'=>'2020-05-15 01:20:06',
'updated_at'=>'2020-06-21 00:08:23',
'deleted_at'=>NULL
] );



Post::create( [
'id'=>4233,
'cat_id'=>8,
'tag_id'=>6,
'title'=>'Lung ultrasound training: curriculum implementation and learning trajectory among respiratory therapists',
'text'=>'K. C. See and others\r\nIntensive Care Med. Jan 2016421:63 - 71',
'img'=>'file-1463591725-9J88SF.jpg',
'file'=>NULL,
'link'=>'http://icmjournal.esicm.org/journals/abstract.html?v=42&j=134&i=1&a=4102_10.1007_s00134-015-4102-9&doi=',
'by'=>0,
'journal'=>'Site Admin',
'type'=>0,
'month_article'=>0,
'famous'=>0,
'views'=>1101,
'show'=>0,
'date'=>'0000-00-00 00:00:00',
'active'=>'1',
'user'=>0,
'created_at'=>'2020-05-15 01:20:06',
'updated_at'=>'2020-06-21 00:08:23',
'deleted_at'=>NULL
] );



Post::create( [
'id'=>4234,
'cat_id'=>8,
'tag_id'=>5,
'title'=>'Apnea test during brain death assessment in mechanically ventilated and ECMO patients',
'text'=>'Marco Giani and others\r\nIntensive Care Med. Jan 2016421:72 - 81',
'img'=>'file-1463591685-DDLMK9.jpg',
'file'=>NULL,
'link'=>'http://icmjournal.esicm.org/journals/abstract.html?v=42&j=134&i=1&a=4105_10.1007_s00134-015-4105-6&doi=',
'by'=>0,
'journal'=>'Site Admin',
'type'=>0,
'month_article'=>0,
'famous'=>0,
'views'=>1114,
'show'=>0,
'date'=>'2016-01-01 08:40:52',
'active'=>'1',
'user'=>0,
'created_at'=>'2020-05-15 01:20:06',
'updated_at'=>'2020-06-21 00:08:23',
'deleted_at'=>NULL
] );



Post::create( [
'id'=>4235,
'cat_id'=>8,
'tag_id'=>6,
'title'=>'Changing use of noninvasive ventilation in critically ill patients: trends over 15 years in francophone countries',
'text'=>'Alexandre Demoule and others\r\nIntensive Care Med. Jan 2016421:82 - 92',
'img'=>'file-1463591738-IZ3XFW.jpg',
'file'=>NULL,
'link'=>'http://icmjournal.esicm.org/journals/abstract.html?v=42&j=134&i=1&a=4087_10.1007_s00134-015-4087-4&doi=',
'by'=>0,
'journal'=>'Site Admin',
'type'=>0,
'month_article'=>0,
'famous'=>0,
'views'=>1101,
'show'=>0,
'date'=>'2016-01-01 08:42:47',
'active'=>'1',
'user'=>0,
'created_at'=>'2020-05-15 01:20:06',
'updated_at'=>'2020-06-21 00:08:23',
'deleted_at'=>NULL
] );



Post::create( [
'id'=>4236,
'cat_id'=>8,
'tag_id'=>10,
'title'=>'Andexanet Alfa for the Reversal of Factor Xa Inhibitor Activity',
'text'=>'D.M. Siegal and Others \r\nN Engl J Med 2015373:2413-2424',
'img'=>'file-1463591752-XK911T.jpg',
'file'=>NULL,
'link'=>'http://www.nejm.org/doi/full/10.1056/NEJMoa1510991',
'by'=>0,
'journal'=>'Site Admin',
'type'=>0,
'month_article'=>0,
'famous'=>0,
'views'=>1101,
'show'=>0,
'date'=>'2015-12-17 19:14:47',
'active'=>'1',
'user'=>0,
'created_at'=>'2020-05-15 01:20:06',
'updated_at'=>'2020-06-21 00:08:23',
'deleted_at'=>NULL
] );



Post::create( [
'id'=>4237,
'cat_id'=>12,
'tag_id'=>1,
'title'=>'2016 IDSA Guidelines for The Management of Candidiasis',
'text'=>'Peter G. Pappas and others\r\nClin Infect Dis. 2015,First published online: December 16, 2015',
'img'=>'file-1463591644-ZGN2DA.jpg',
'file'=>NULL,
'link'=>'http://cid.oxfordjournals.org/content/early/2015/12/15/cid.civ933.full',
'by'=>0,
'journal'=>'Site Admin',
'type'=>0,
'month_article'=>0,
'famous'=>0,
'views'=>1241,
'show'=>0,
'date'=>'2015-12-14 21:39:03',
'active'=>'1',
'user'=>0,
'created_at'=>'2020-05-15 01:20:06',
'updated_at'=>'2020-06-21 00:08:23',
'deleted_at'=>NULL
] );



Post::create( [
'id'=>4238,
'cat_id'=>12,
'tag_id'=>6,
'title'=>'PEEP titration during prone positioning for acute respiratory distress syndrome',
'text'=>'Beitler J, GuÃ©rin C, Ayzac L, Mancebo J, Bates D, Malhotra A, Talmor D\r\nCritical Care 2015, 19 :436 21 December 2015',
'img'=>'file-1463591685-DDLMK9.jpg',
'file'=>NULL,
'link'=>'http://www.ccforum.com/content/19/1/436',
'by'=>0,
'journal'=>'Site Admin',
'type'=>0,
'month_article'=>0,
'famous'=>0,
'views'=>1212,
'show'=>0,
'date'=>'2015-12-21 12:27:47',
'active'=>'1',
'user'=>0,
'created_at'=>'2020-05-15 01:20:06',
'updated_at'=>'2020-06-21 00:08:23',
'deleted_at'=>NULL
] );



Post::create( [
'id'=>4239,
'cat_id'=>12,
'tag_id'=>1,
'title'=>'New Recommendations Aim to Redefine Definition and Enhance Diagnosis of Sepsis, Septic Shock',
'text'=>'The recommendations are published in the February 2016 issue of JAMA and were recently highlighted for clinicians and media at the SCCM 45th Critical Care Congress in Orlando, Florida.',
'img'=>'file-1463591746-GEE3E7.jpg',
'file'=>NULL,
'link'=>'http://www.sccm.org/Research/Quality/Pages/Sepsis-Definitions.aspx',
'by'=>0,
'journal'=>'Site Admin',
'type'=>0,
'month_article'=>0,
'famous'=>0,
'views'=>1241,
'show'=>0,
'date'=>'2016-02-23 21:26:31',
'active'=>'1',
'user'=>0,
'created_at'=>'2020-05-15 01:20:06',
'updated_at'=>'2020-06-21 00:08:23',
'deleted_at'=>NULL
] );



Post::create( [
'id'=>4240,
'cat_id'=>9,
'tag_id'=>1,
'title'=>'2016 IDSA Guidelines for The Management of Candidiasis',
'text'=>'Peter G. Pappas and others\r\nClin Infect Dis. 2015,First published online: December 16, 2015',
'img'=>'file-1463591738-IZ3XFW.jpg',
'file'=>NULL,
'link'=>'http://cid.oxfordjournals.org/content/early/2015/12/15/cid.civ933.full',
'by'=>0,
'journal'=>'Site Admin',
'type'=>0,
'month_article'=>0,
'famous'=>0,
'views'=>1130,
'show'=>0,
'date'=>'2015-12-16 12:30:37',
'active'=>'1',
'user'=>0,
'created_at'=>'2020-05-15 01:20:06',
'updated_at'=>'2020-06-21 00:08:23',
'deleted_at'=>NULL
] );



Post::create( [
'id'=>4241,
'cat_id'=>8,
'tag_id'=>9,
'title'=>'Molecular adsorbent recirculating system and single-pass albumin dialysis in liver failure â€“ a prospective, randomised crossover study',
'text'=>'Sponholz C, Matthes K, Rupp D, Backaus W, Klammt S, Karailieva D, Bauschke A, Settmacher U, Kohl M, Clemens M, Mitzner S, Bauer M, Kortgen A\r\nCritical Care 2016, 20 :2 4 January 2016',
'img'=>'file-1463591685-DDLMK9.jpg',
'file'=>NULL,
'link'=>'http://www.ccforum.com/content/20/1/2/abstract',
'by'=>0,
'journal'=>'Site Admin',
'type'=>0,
'month_article'=>0,
'famous'=>0,
'views'=>1101,
'show'=>0,
'date'=>'2016-01-05 09:43:27',
'active'=>'1',
'user'=>0,
'created_at'=>'2020-05-15 01:20:06',
'updated_at'=>'2020-06-21 00:08:23',
'deleted_at'=>NULL
] );



Post::create( [
'id'=>4242,
'cat_id'=>12,
'tag_id'=>3,
'title'=>'A commentary on the 2015 Canadian Clinical Practice Guidelines in glutamine supplementation to parenteral nutrition',
'text'=>'Leguina-Ruzzi A\r\nCritical Care 2016, 20 :7 8 January 2016',
'img'=>'file-1463591738-IZ3XFW.jpg',
'file'=>NULL,
'link'=>'http://www.ccforum.com/content/20/1/7',
'by'=>0,
'journal'=>'Site Admin',
'type'=>0,
'month_article'=>0,
'famous'=>0,
'views'=>1212,
'show'=>0,
'date'=>'2016-01-07 21:38:51',
'active'=>'1',
'user'=>0,
'created_at'=>'2020-05-15 01:20:06',
'updated_at'=>'2020-06-21 00:08:23',
'deleted_at'=>NULL
] );



Post::create( [
'id'=>4243,
'cat_id'=>9,
'tag_id'=>3,
'title'=>'Canadian Clinical Practice Guidelines 2015\r\nSummary of Revisions to the Recommendations',
'text'=>'Canadian Critical Care Nutrition\r\nMay 25th 2015',
'img'=>'file-1463591639-4YG8JY.jpg',
'file'=>NULL,
'link'=>'http://www.criticalcarenutrition.com/docs/CPGs%202015/Summary%20CPGs%202015%20vs%202013.pdf',
'by'=>0,
'journal'=>'Site Admin',
'type'=>0,
'month_article'=>0,
'famous'=>0,
'views'=>1101,
'show'=>0,
'date'=>'2015-05-01 06:15:47',
'active'=>'1',
'user'=>0,
'created_at'=>'2020-05-15 01:20:06',
'updated_at'=>'2020-06-21 00:08:23',
'deleted_at'=>NULL
] );



Post::create( [
'id'=>4244,
'cat_id'=>12,
'tag_id'=>3,
'title'=>'Society of Critical Care Medicine SCCM\r\nand American Society for Parenteral and Enteral\r\nNutrition A.S.P.E.N.',
'text'=>'Beth E. Taylor and others February 2016 â€¢ Volume 44 â€¢ Number 2\r\nThe Society of Critical Care Medicine and American Society for Parenteral and Enteral Nutrition',
'img'=>'file-1463591752-XK911T.jpg',
'file'=>NULL,
'link'=>'file:///Users/drsewafie/Downloads/2016%20SCCM-ASPEN%20Guidelines%20for%20the%20Provision%20and%20%20Assessment%20of%20Nutrition%20Support%20Therapy%20in%20the%20Adult%20Critically%20Ill%20%20Patient.pdf',
'by'=>0,
'journal'=>'Site Admin',
'type'=>0,
'month_article'=>0,
'famous'=>0,
'views'=>1212,
'show'=>0,
'date'=>'2016-01-22 14:15:35',
'active'=>'1',
'user'=>0,
'created_at'=>'2020-05-15 01:20:06',
'updated_at'=>'2020-06-21 00:08:23',
'deleted_at'=>NULL
] );



Post::create( [
'id'=>4245,
'cat_id'=>9,
'tag_id'=>3,
'title'=>'Society of Critical Care Medicine SCCM and American Society for Parenteral and Enteral Nutrition A.S.P.E.N.',
'text'=>'Beth E. Taylor and others February 2016 â€¢ Volume 44 â€¢ Number 2\r\nThe Society of Critical Care Medicine and American Society for Parenteral and Enteral Nutrition',
'img'=>'file-1463591657-K8BRDF.jpg',
'file'=>NULL,
'link'=>'http://sccmmedia.sccm.org/documents/LearnICU/Guidelines/Nutrition-SCCM-ASPEN.pdf',
'by'=>0,
'journal'=>'Site Admin',
'type'=>0,
'month_article'=>0,
'famous'=>0,
'views'=>1101,
'show'=>0,
'date'=>'2016-01-22 11:28:53',
'active'=>'1',
'user'=>0,
'created_at'=>'2020-05-15 01:20:06',
'updated_at'=>'2020-06-21 00:08:23',
'deleted_at'=>NULL
] );



Post::create( [
'id'=>4246,
'cat_id'=>9,
'tag_id'=>1,
'title'=>'Antibiotic Guidelines 2015-2016\r\nTreatment Recommendations For Adult Inpatients',
'text'=>'JOHNS HOPKINS Medicine',
'img'=>'file-1463591725-9J88SF.jpg',
'file'=>NULL,
'link'=>'http://www.hopkinsmedicine.org/amp/guidelines/Antibiotic_guidelines.pdf',
'by'=>0,
'journal'=>'Site Admin',
'type'=>0,
'month_article'=>0,
'famous'=>0,
'views'=>1130,
'show'=>0,
'date'=>'2016-03-23 09:03:45',
'active'=>'1',
'user'=>0,
'created_at'=>'2020-05-15 01:20:06',
'updated_at'=>'2020-06-21 00:08:23',
'deleted_at'=>NULL
] );



Post::create( [
'id'=>4247,
'cat_id'=>8,
'tag_id'=>8,
'title'=>'Acute kidney stressâ€”a useful term based on evolution in the understanding of acute kidney injury',
'text'=>'Katz N, Ronco C\r\nCritical Care 2016, 20 :23 22 January 2016',
'img'=>'file-1463591650-J14MMU.jpg',
'file'=>NULL,
'link'=>'http://www.ccforum.com/content/20/1/23',
'by'=>0,
'journal'=>'Site Admin',
'type'=>0,
'month_article'=>0,
'famous'=>1,
'views'=>1247,
'show'=>0,
'date'=>'2016-01-22 08:53:44',
'active'=>'1',
'user'=>0,
'created_at'=>'2020-05-15 01:20:06',
'updated_at'=>'2020-06-21 00:08:23',
'deleted_at'=>NULL
] );



Post::create( [
'id'=>4248,
'cat_id'=>8,
'tag_id'=>11,
'title'=>'Teamwork in the ICUâ€“Do We Practice What We Preach',
'text'=>'Critical Care Medicine:\r\nFebruary 2016 - Volume 44 - Issue 2 - p 254â€“255',
'img'=>'file-1463591746-GEE3E7.jpg',
'file'=>NULL,
'link'=>'http://journals.lww.com/ccmjournal/Fulltext/2016/02000/Teamwork_in_the_ICU_Do_We_Practice_What_We_Preach_.2.aspx',
'by'=>0,
'journal'=>'Site Admin',
'type'=>0,
'month_article'=>0,
'famous'=>0,
'views'=>1101,
'show'=>0,
'date'=>'2016-02-01 17:41:28',
'active'=>'1',
'user'=>0,
'created_at'=>'2020-05-15 01:20:06',
'updated_at'=>'2020-06-21 00:08:23',
'deleted_at'=>NULL
] );



Post::create( [
'id'=>4249,
'cat_id'=>8,
'tag_id'=>1,
'title'=>'Duration of Antimicrobial Treatment for Bacteremia in Canadian Critically Ill Patients',
'text'=>'Daneman, Nick and others\r\nCritical Care Medicine:\r\nFebruary 2016 - Volume 44 - Issue 2 - p 256â€“264',
'img'=>'file-1463591738-IZ3XFW.jpg',
'file'=>NULL,
'link'=>'http://journals.lww.com/ccmjournal/Fulltext/2016/02000/Duration_of_Antimicrobial_Treatment_for_Bacteremia.3.aspx',
'by'=>0,
'journal'=>'Site Admin',
'type'=>0,
'month_article'=>0,
'famous'=>0,
'views'=>1130,
'show'=>0,
'date'=>'2016-02-01 17:42:52',
'active'=>'1',
'user'=>0,
'created_at'=>'2020-05-15 01:20:06',
'updated_at'=>'2020-06-21 00:08:23',
'deleted_at'=>NULL
] );



Post::create( [
'id'=>4250,
'cat_id'=>8,
'tag_id'=>11,
'title'=>'Economic Evaluation of Telemedicine for Patients in ICUs',
'text'=>'Yoo, Byung-Kwang and others\r\nCritical Care Medicine: \r\nFebruary 2016 - Volume 44 - Issue 2 - p 265â€“274',
'img'=>'file-1463591650-J14MMU.jpg',
'file'=>NULL,
'link'=>'http://journals.lww.com/ccmjournal/Abstract/2016/02000/Economic_Evaluation_of_Telemedicine_for_Patients.4.aspx',
'by'=>0,
'journal'=>'Site Admin',
'type'=>0,
'month_article'=>0,
'famous'=>0,
'views'=>1101,
'show'=>0,
'date'=>'2016-02-01 08:56:12',
'active'=>'1',
'user'=>0,
'created_at'=>'2020-05-15 01:20:06',
'updated_at'=>'2020-06-21 00:08:23',
'deleted_at'=>NULL
] );



Post::create( [
'id'=>4251,
'cat_id'=>8,
'tag_id'=>6,
'title'=>'Failure of Noninvasive Ventilation for De Novo Acute Hypoxemic Respiratory Failure: Role of Tidal Volume',
'text'=>'Carteaux, Guillaume and others\r\nCritical Care Medicine: \r\nFebruary 2016 - Volume 44 - Issue 2 - p 282â€“290',
'img'=>'file-1463591738-IZ3XFW.jpg',
'file'=>NULL,
'link'=>'http://journals.lww.com/ccmjournal/Abstract/2016/02000/Failure_of_Noninvasive_Ventilation_for_De_Novo.6.aspx',
'by'=>0,
'journal'=>'Site Admin',
'type'=>0,
'month_article'=>0,
'famous'=>0,
'views'=>1101,
'show'=>0,
'date'=>'2016-02-02 08:58:42',
'active'=>'1',
'user'=>0,
'created_at'=>'2020-05-15 01:20:06',
'updated_at'=>'2020-06-21 00:08:23',
'deleted_at'=>NULL
] );



Post::create( [
'id'=>4252,
'cat_id'=>8,
'tag_id'=>6,
'title'=>'Recruitment Maneuvers and Positive End-Expiratory Pressure Titration in Morbidly Obese ICU Patients',
'text'=>'Pirrone, Massimiliano and others\r\nCritical Care Medicine: \r\nFebruary 2016 - Volume 44 - Issue 2 - p 300â€“307',
'img'=>'file-1463591639-4YG8JY.jpg',
'file'=>NULL,
'link'=>'http://journals.lww.com/ccmjournal/Abstract/2016/02000/Recruitment_Maneuvers_and_Positive_End_Expiratory.8.aspx',
'by'=>0,
'journal'=>'Site Admin',
'type'=>0,
'month_article'=>0,
'famous'=>0,
'views'=>1101,
'show'=>0,
'date'=>'2016-02-02 08:59:50',
'active'=>'1',
'user'=>0,
'created_at'=>'2020-05-15 01:20:06',
'updated_at'=>'2020-06-21 00:08:23',
'deleted_at'=>NULL
] );



Post::create( [
'id'=>4253,
'cat_id'=>8,
'tag_id'=>7,
'title'=>'Bradycardia During Targeted Temperature Management: An Early Marker of Lower Mortality and Favorable Neurologic Outcome in Comatose Out-of-Hospital Cardiac Arrest Patients',
'text'=>'Thomsen, Jakob Hartvig and others\r\nCritical Care Medicine: \r\nFebruary 2016 - Volume 44 - Issue 2 - p 308â€“318',
'img'=>'file-1463591639-4YG8JY.jpg',
'file'=>NULL,
'link'=>'http://journals.lww.com/ccmjournal/Abstract/2016/02000/Bradycardia_During_Targeted_Temperature.9.aspx',
'by'=>0,
'journal'=>'Site Admin',
'type'=>0,
'month_article'=>0,
'famous'=>0,
'views'=>1101,
'show'=>0,
'date'=>'2016-02-02 09:02:41',
'active'=>'1',
'user'=>0,
'created_at'=>'2020-05-15 01:20:06',
'updated_at'=>'2020-06-21 00:08:23',
'deleted_at'=>NULL
] );



Post::create( [
'id'=>4254,
'cat_id'=>8,
'tag_id'=>7,
'title'=>'Pulse Pressure Variation Adjusted by Respiratory Changes in Pleural Pressure, Rather Than by Tidal Volume, Reliably Predicts Fluid Responsiveness in Patients With Acute Respiratory...',
'text'=>'Liu, Yang Wei and others\r\nCritical Care Medicine: \r\nFebruary 2016 - Volume 44 - Issue 2 - p 342â€“351',
'img'=>'file-1463591650-J14MMU.jpg',
'file'=>NULL,
'link'=>'http://journals.lww.com/ccmjournal/Abstract/2016/02000/Pulse_Pressure_Variation_Adjusted_by_Respiratory.13.aspx',
'by'=>0,
'journal'=>'Site Admin',
'type'=>0,
'month_article'=>0,
'famous'=>0,
'views'=>1101,
'show'=>0,
'date'=>'2016-02-02 09:05:15',
'active'=>'1',
'user'=>0,
'created_at'=>'2020-05-15 01:20:06',
'updated_at'=>'2020-06-21 00:08:23',
'deleted_at'=>NULL
] );



Post::create( [
'id'=>4255,
'cat_id'=>8,
'tag_id'=>1,
'title'=>'Randomized, Double-Blind, Placebo-Controlled Trial of Thiamine as a Metabolic Resuscitator in Septic Shock: A Pilot Study',
'text'=>'Donnino, Michael W and others\r\nCritical Care Medicine: \r\nFebruary 2016 - Volume 44 - Issue 2 - p 360â€“367',
'img'=>'file-1463591725-9J88SF.jpg',
'file'=>NULL,
'link'=>'http://journals.lww.com/ccmjournal/Fulltext/2016/02000/Randomized,_Double_Blind,_Placebo_Controlled_Trial.15.aspx',
'by'=>0,
'journal'=>'Site Admin',
'type'=>0,
'month_article'=>0,
'famous'=>0,
'views'=>1130,
'show'=>0,
'date'=>'2016-02-02 09:11:26',
'active'=>'1',
'user'=>0,
'created_at'=>'2020-05-15 01:20:06',
'updated_at'=>'2020-06-21 00:08:23',
'deleted_at'=>NULL
] );



Post::create( [
'id'=>4256,
'cat_id'=>8,
'tag_id'=>11,
'title'=>'Organ donation in adults: a critical care perspective',
'text'=>'Giuseppe Citerio and others\r\nIntensive Care Med. 423:305 - 315, March 2016',
'img'=>'file-1463591650-J14MMU.jpg',
'file'=>NULL,
'link'=>'http://icmjournal.esicm.org/journals/abstract.html?v=42&j=134&i=3&a=4191_10.1007_s00134-015-4191-5&doi=',
'by'=>0,
'journal'=>'Site Admin',
'type'=>0,
'month_article'=>0,
'famous'=>0,
'views'=>1101,
'show'=>0,
'date'=>'2016-03-01 09:15:30',
'active'=>'1',
'user'=>0,
'created_at'=>'2020-05-15 01:20:06',
'updated_at'=>'2020-06-21 00:08:23',
'deleted_at'=>NULL
] );



Post::create( [
'id'=>4257,
'cat_id'=>8,
'tag_id'=>7,
'title'=>'Acute heart failure and cardiogenic shock: a multidisciplinary practical guidance',
'text'=>'A. Mebazaa, H. Tolppanen and others\r\nIntensive Care Med. 423:147 - 163, February 2016',
'img'=>'file-1463591746-GEE3E7.jpg',
'file'=>NULL,
'link'=>'http://icmjournal.esicm.org/journals/abstract.html?v=42&j=134&i=2&a=4041_10.1007_s00134-015-4041-5&doi=',
'by'=>0,
'journal'=>'Site Admin',
'type'=>0,
'month_article'=>0,
'famous'=>0,
'views'=>1101,
'show'=>0,
'date'=>'2016-02-11 09:17:06',
'active'=>'1',
'user'=>0,
'created_at'=>'2020-05-15 01:20:06',
'updated_at'=>'2020-06-21 00:08:23',
'deleted_at'=>NULL
] );



Post::create( [
'id'=>4258,
'cat_id'=>9,
'tag_id'=>7,
'title'=>'Acute heart failure and cardiogenic shock: a multidisciplinary practical guidance',
'text'=>'A. Mebazaa, H. Tolppanen and others\r\nIntensive Care Med. 423:147 - 163, February 2016',
'img'=>'file-1463591725-9J88SF.jpg',
'file'=>NULL,
'link'=>'http://icmjournal.esicm.org/journals/abstract.html?v=42&j=134&i=2&a=4041_10.1007_s00134-015-4041-5&doi=',
'by'=>0,
'journal'=>'Site Admin',
'type'=>0,
'month_article'=>0,
'famous'=>0,
'views'=>1101,
'show'=>0,
'date'=>'2016-02-11 09:18:01',
'active'=>'1',
'user'=>0,
'created_at'=>'2020-05-15 01:20:06',
'updated_at'=>'2020-06-21 00:08:23',
'deleted_at'=>NULL
] );



Post::create( [
'id'=>4259,
'cat_id'=>8,
'tag_id'=>6,
'title'=>'Acute respiratory distress syndrome mimickers lacking common risk factors of the Berlin definition',
'text'=>'Aude Gibelin and others\r\nIntensive Care Med. 423:164 - 172, February 2016',
'img'=>'file-1463591746-GEE3E7.jpg',
'file'=>NULL,
'link'=>'http://icmjournal.esicm.org/journals/abstract.html?v=42&j=134&i=2&a=4064_10.1007_s00134-015-4064-y&doi=',
'by'=>0,
'journal'=>'Site Admin',
'type'=>0,
'month_article'=>0,
'famous'=>0,
'views'=>1101,
'show'=>0,
'date'=>'2016-02-02 09:19:28',
'active'=>'1',
'user'=>0,
'created_at'=>'2020-05-15 01:20:06',
'updated_at'=>'2020-06-21 00:08:23',
'deleted_at'=>NULL
] );



Post::create( [
'id'=>4260,
'cat_id'=>8,
'tag_id'=>7,
'title'=>'Speckle tracking analysis allows sensitive detection of stress cardiomyopathy in severe aneurysmal subarachnoid hemorrhage patients',
'text'=>'RaphaÃ«l Cinottiand others\r\nIntensive Care Med. 423:173 - 182, February 2016',
'img'=>'file-1463591685-DDLMK9.jpg',
'file'=>NULL,
'link'=>'http://icmjournal.esicm.org/journals/abstract.html?v=42&j=134&i=2&a=4106_10.1007_s00134-015-4106-5&doi=',
'by'=>0,
'journal'=>'Site Admin',
'type'=>0,
'month_article'=>0,
'famous'=>0,
'views'=>1101,
'show'=>0,
'date'=>'2016-02-02 09:21:49',
'active'=>'1',
'user'=>0,
'created_at'=>'2020-05-15 01:20:06',
'updated_at'=>'2020-06-21 00:08:23',
'deleted_at'=>NULL
] );



Post::create( [
'id'=>4261,
'cat_id'=>8,
'tag_id'=>2,
'title'=>'A randomized controlled trial of daily sedation interruption in critically ill children',
'text'=>'Nienke J. Vet and others\r\nIntensive Care Med. 423:233 - 244, February 2016',
'img'=>'file-1463591738-IZ3XFW.jpg',
'file'=>NULL,
'link'=>'http://icmjournal.esicm.org/journals/abstract.html?v=42&j=134&i=2&a=4136_10.1007_s00134-015-4136-z&doi=',
'by'=>0,
'journal'=>'Site Admin',
'type'=>0,
'month_article'=>0,
'famous'=>1,
'views'=>1267,
'show'=>0,
'date'=>'2016-02-02 09:24:33',
'active'=>'1',
'user'=>0,
'created_at'=>'2020-05-15 01:20:06',
'updated_at'=>'2020-06-21 00:08:23',
'deleted_at'=>NULL
] );



Post::create( [
'id'=>4262,
'cat_id'=>8,
'tag_id'=>7,
'title'=>'Ten clinical indicators suggesting the need for ICU admission after Rapid Response Team review',
'text'=>'Nienke J. Vet and others\r\nIntensive Care Med. 423:233 - 244, February 2016',
'img'=>'file-1463591644-ZGN2DA.jpg',
'file'=>NULL,
'link'=>'Daryl Jones, Michael DeVita, Stephen Warrillow',
'by'=>0,
'journal'=>'Site Admin',
'type'=>0,
'month_article'=>0,
'famous'=>0,
'views'=>1101,
'show'=>0,
'date'=>'2016-02-01 09:25:39',
'active'=>'1',
'user'=>0,
'created_at'=>'2020-05-15 01:20:06',
'updated_at'=>'2020-06-21 00:08:23',
'deleted_at'=>NULL
] );



Post::create( [
'id'=>4263,
'cat_id'=>8,
'tag_id'=>7,
'title'=>'Ten clinical indicators suggesting the need for ICU admission after Rapid Response Team review',
'text'=>'Nienke J. Vet and others\r\nIntensive Care Med. 423:261 - 263, February 2016',
'img'=>'file-1463591685-DDLMK9.jpg',
'file'=>NULL,
'link'=>'Daryl Jones, Michael DeVita, Stephen Warrillow',
'by'=>0,
'journal'=>'Site Admin',
'type'=>0,
'month_article'=>0,
'famous'=>0,
'views'=>1101,
'show'=>0,
'date'=>'0000-00-00 00:00:00',
'active'=>'1',
'user'=>0,
'created_at'=>'2020-05-15 01:20:06',
'updated_at'=>'2020-06-21 00:08:23',
'deleted_at'=>NULL
] );



Post::create( [
'id'=>4264,
'cat_id'=>8,
'tag_id'=>7,
'title'=>'Ten clinical indicators suggesting the need for ICU admission after Rapid Response Team review',
'text'=>'Nienke J. Vet and others\r\nIntensive Care Med. 423:261 - 263, February 2016',
'img'=>'file-1463591725-9J88SF.jpg',
'file'=>NULL,
'link'=>'Daryl Jones, Michael DeVita, Stephen Warrillow',
'by'=>0,
'journal'=>'Site Admin',
'type'=>0,
'month_article'=>0,
'famous'=>0,
'views'=>1101,
'show'=>0,
'date'=>'2016-02-01 09:26:05',
'active'=>'1',
'user'=>0,
'created_at'=>'2020-05-15 01:20:06',
'updated_at'=>'2020-06-21 00:08:23',
'deleted_at'=>NULL
] );



Post::create( [
'id'=>4265,
'cat_id'=>8,
'tag_id'=>7,
'title'=>'Venoâ€“venoâ€“arterial extracorporeal membrane oxygenation treatment in patients with severe acute respiratory distress syndrome and septic shock',
'text'=>'Yeo H, Jeon D, Kim Y, Cho W, Kim D\r\nCritical Care 2016, 20 :28 10 February 2016',
'img'=>'file-1463591752-XK911T.jpg',
'file'=>NULL,
'link'=>'http://ccforum.biomedcentral.com/articles/10.1186/s13054-016-1205-9',
'by'=>0,
'journal'=>'Site Admin',
'type'=>0,
'month_article'=>0,
'famous'=>0,
'views'=>1101,
'show'=>0,
'date'=>'2016-02-10 09:42:24',
'active'=>'1',
'user'=>0,
'created_at'=>'2020-05-15 01:20:06',
'updated_at'=>'2020-06-21 00:08:23',
'deleted_at'=>NULL
] );



Post::create( [
'id'=>4266,
'cat_id'=>8,
'tag_id'=>6,
'title'=>'Feasibility and safety of low-flow extracorporeal carbon dioxide removal to facilitate ultra-protective ventilation in patients with moderate acute respiratory distress sindrome',
'text'=>'Fanelli V, Ranieri M, Mancebo J, Moerer O, Quintel M, Morley S, Moran I, Parrilla F, Costamagna A, Gaudiosi M, Combes A\r\nCritical Care 2016, 20 :36 10 February 2016',
'img'=>'file-1463591732-SM39NY.jpg',
'file'=>NULL,
'link'=>'http://ccforum.biomedcentral.com/articles/10.1186/s13054-016-1211-y',
'by'=>0,
'journal'=>'Site Admin',
'type'=>0,
'month_article'=>0,
'famous'=>0,
'views'=>1101,
'show'=>0,
'date'=>'2016-02-10 09:43:32',
'active'=>'1',
'user'=>0,
'created_at'=>'2020-05-15 01:20:06',
'updated_at'=>'2020-06-21 00:08:23',
'deleted_at'=>NULL
] );



Post::create( [
'id'=>4267,
'cat_id'=>8,
'tag_id'=>10,
'title'=>'Caplacizumab for Acquired Thrombotic Thrombocytopenic Purpura',
'text'=>'F. Peyvandi and Others\r\nN Engl J Med February 11, 2016374:511-522',
'img'=>'file-1463591657-K8BRDF.jpg',
'file'=>NULL,
'link'=>'http://www.nejm.org/doi/full/10.1056/NEJMoa1505533',
'by'=>0,
'journal'=>'Site Admin',
'type'=>0,
'month_article'=>0,
'famous'=>0,
'views'=>1101,
'show'=>0,
'date'=>'2016-02-11 09:45:25',
'active'=>'1',
'user'=>0,
'created_at'=>'2020-05-15 01:20:06',
'updated_at'=>'2020-06-21 00:08:23',
'deleted_at'=>NULL
] );



Post::create( [
'id'=>4268,
'cat_id'=>8,
'tag_id'=>1,
'title'=>'The Third International Consensus Definitions for Sepsis and Septic Shock Sepsis-3',
'text'=>'Mervyn Singer and others\r\nJAMA. 20163158:801-810',
'img'=>'file-1463591746-GEE3E7.jpg',
'file'=>NULL,
'link'=>'http://jama.jamanetwork.com/article.aspx?articleid=2492881',
'by'=>0,
'journal'=>'Site Admin',
'type'=>0,
'month_article'=>0,
'famous'=>1,
'views'=>1225,
'show'=>0,
'date'=>'2016-03-09 21:28:52',
'active'=>'1',
'user'=>0,
'created_at'=>'2020-05-15 01:20:06',
'updated_at'=>'2020-06-21 00:08:23',
'deleted_at'=>NULL
] );



Post::create( [
'id'=>4269,
'cat_id'=>8,
'tag_id'=>1,
'title'=>'Assessment of Clinical Criteria for Sepsis\r\nFor the Third International Consensus Definitions for Sepsis and Septic Shock Sepsis-3',
'text'=>'Christopher W. Seymour and others\r\nJAMA. Feb 23, 20163158:762-774',
'img'=>'file-1463591732-SM39NY.jpg',
'file'=>NULL,
'link'=>'http://jama.jamanetwork.com/article.aspx?articleid=2492875',
'by'=>0,
'journal'=>'Site Admin',
'type'=>0,
'month_article'=>0,
'famous'=>0,
'views'=>1130,
'show'=>0,
'date'=>'2016-02-22 21:47:04',
'active'=>'1',
'user'=>0,
'created_at'=>'2020-05-15 01:20:06',
'updated_at'=>'2020-06-21 00:08:23',
'deleted_at'=>NULL
] );

Post::create( [
'id'=>4270,
'cat_id'=>8,
'tag_id'=>1,
'title'=>'Developing a New Definition and Assessing New Clinical Criteria for Septic Shock: For the Third International Consensus Definitions for Sepsis and Septic Shock Sepsis-3',
'text'=>'Manu Shankar-Hari, and othesr\r\nJAMA. Feb 23, 20163158:775-787.',
'img'=>'file-1463591738-IZ3XFW.jpg',
'file'=>NULL,
'link'=>'http://jama.jamanetwork.com/article.aspx?articleid=2492876',
'by'=>0,
'journal'=>'Site Admin',
'type'=>0,
'month_article'=>0,
'famous'=>0,
'views'=>1130,
'show'=>0,
'date'=>'2016-02-22 21:49:08',
'active'=>'1',
'user'=>0,
'created_at'=>'2020-05-15 01:20:06',
'updated_at'=>'2020-06-21 00:08:23',
'deleted_at'=>NULL
] );



Post::create( [
'id'=>4271,
'cat_id'=>8,
'tag_id'=>1,
'title'=>'New Definitions for Sepsis and Septic Shock: Continuing Evolution but With Much Still to Be Done',
'text'=>'Edward Abraham, MD\r\nJAMA. Feb 23, 20163158:757-759',
'img'=>'file-1463591725-9J88SF.jpg',
'file'=>NULL,
'link'=>'http://jama.jamanetwork.com/article.aspx?articleid=2492856',
'by'=>0,
'journal'=>'Site Admin',
'type'=>0,
'month_article'=>0,
'famous'=>1,
'views'=>1225,
'show'=>0,
'date'=>'2016-02-22 21:50:48',
'active'=>'1',
'user'=>0,
'created_at'=>'2020-05-15 01:20:06',
'updated_at'=>'2020-06-21 00:08:23',
'deleted_at'=>NULL
] );



Post::create( [
'id'=>4272,
'cat_id'=>8,
'tag_id'=>6,
'title'=>'The Acute Respiratory Distress Syndrome: Dialing in the Evidence',
'text'=>'Brendan J. Clark, MD Marc Moss, MD\r\nJAMA. Feb 23, 20163158:759-761',
'img'=>'file-1463591685-DDLMK9.jpg',
'file'=>NULL,
'link'=>'http://jama.jamanetwork.com/article.aspx?articleid=2492857',
'by'=>0,
'journal'=>'Site Admin',
'type'=>0,
'month_article'=>0,
'famous'=>0,
'views'=>1101,
'show'=>0,
'date'=>'2016-02-22 21:54:05',
'active'=>'1',
'user'=>0,
'created_at'=>'2020-05-15 01:20:06',
'updated_at'=>'2020-06-21 00:08:23',
'deleted_at'=>NULL
] );



Post::create( [
'id'=>4273,
'cat_id'=>8,
'tag_id'=>11,
'title'=>'Communicating While Receiving Mechanical Ventilation: Texting With a Smartphone',
'text'=>'Joseph Shiber, Ayesha Thomas, and Ashley Northcutt\r\nAm J Crit Care March 2016 25:e38-e39',
'img'=>'file-1463591725-9J88SF.jpg',
'file'=>NULL,
'link'=>'http://ajcc.aacnjournals.org/content/25/2/e38.abstract',
'by'=>0,
'journal'=>'Site Admin',
'type'=>0,
'month_article'=>0,
'famous'=>0,
'views'=>1101,
'show'=>0,
'date'=>'2016-03-01 10:01:06',
'active'=>'1',
'user'=>0,
'created_at'=>'2020-05-15 01:20:06',
'updated_at'=>'2020-06-21 00:08:23',
'deleted_at'=>NULL
] );



Post::create( [
'id'=>4274,
'cat_id'=>8,
'tag_id'=>7,
'title'=>'HES Solutions Warrant Additional Box Warning',
'text'=>'The U.S. Food and Drug Administration FDA has found data that indicate an increased risk of mortality and renal injury ...',
'img'=>'file-1463591732-SM39NY.jpg',
'file'=>NULL,
'link'=>'http://www.fda.gov/BiologicsBloodVaccines/SafetyAvailability/ucm358271.htm',
'by'=>0,
'journal'=>'Site Admin',
'type'=>0,
'month_article'=>0,
'famous'=>0,
'views'=>1101,
'show'=>0,
'date'=>'2016-03-06 10:08:56',
'active'=>'1',
'user'=>0,
'created_at'=>'2020-05-15 01:20:06',
'updated_at'=>'2020-06-21 00:08:23',
'deleted_at'=>NULL
] );



Post::create( [
'id'=>4275,
'cat_id'=>8,
'tag_id'=>1,
'title'=>'Critical Care Medicine After the 2014â€“2015 Ebola Outbreak: Are We Ready If It Happens Again',
'text'=>'Chertow, Daniel S. Palmore, Tara N. Masur, Henry\r\nCritical Care Medicine:\r\nMarch 2016 - Volume 44 - Issue 3 - p 457â€“459',
'img'=>'file-1463591685-DDLMK9.jpg',
'file'=>NULL,
'link'=>'http://journals.lww.com/ccmjournal/Fulltext/2016/03000/Critical_Care_Medicine_After_the_2014_2015_Ebola.1.aspx',
'by'=>0,
'journal'=>'Site Admin',
'type'=>0,
'month_article'=>0,
'famous'=>0,
'views'=>1130,
'show'=>0,
'date'=>'2016-03-01 10:12:11',
'active'=>'1',
'user'=>0,
'created_at'=>'2020-05-15 01:20:06',
'updated_at'=>'2020-06-21 00:08:23',
'deleted_at'=>NULL
] );



Post::create( [
'id'=>4276,
'cat_id'=>8,
'tag_id'=>10,
'title'=>'Epidemiology of Massive Transfusion: A Binational Study From Sweden and Denmark',
'text'=>'Halmin, MÃ¤rit and others\r\nCritical Care Medicine:\r\nMarch 2016 - Volume 44 - Issue 3 - p 468â€“477',
'img'=>'file-1463591650-J14MMU.jpg',
'file'=>NULL,
'link'=>'http://journals.lww.com/ccmjournal/Fulltext/2016/03000/Epidemiology_of_Massive_Transfusion___A_Binational.3.aspx',
'by'=>0,
'journal'=>'Site Admin',
'type'=>0,
'month_article'=>0,
'famous'=>0,
'views'=>1101,
'show'=>0,
'date'=>'2016-03-01 10:13:47',
'active'=>'1',
'user'=>0,
'created_at'=>'2020-05-15 01:20:06',
'updated_at'=>'2020-06-21 00:08:23',
'deleted_at'=>NULL
] );



Post::create( [
'id'=>4277,
'cat_id'=>8,
'tag_id'=>6,
'title'=>'Comparison Between Neurally Adjusted Ventilatory Assist and Pressure Support Ventilation Levels in Terms of Respiratory Effort',
'text'=>'Carteaux, Guillaume and others\r\nCritical Care Medicine:\r\nMarch 2016 - Volume 44 - Issue 3 - p 503â€“511',
'img'=>'file-1463591738-IZ3XFW.jpg',
'file'=>NULL,
'link'=>'http://journals.lww.com/ccmjournal/Abstract/2016/03000/Comparison_Between_Neurally_Adjusted_Ventilatory.7.aspx',
'by'=>0,
'journal'=>'Site Admin',
'type'=>0,
'month_article'=>0,
'famous'=>0,
'views'=>1101,
'show'=>0,
'date'=>'2016-03-01 10:15:45',
'active'=>'1',
'user'=>0,
'created_at'=>'2020-05-15 01:20:06',
'updated_at'=>'2020-06-21 00:08:23',
'deleted_at'=>NULL
] );



Post::create( [
'id'=>4278,
'cat_id'=>8,
'tag_id'=>2,
'title'=>'The Effect of Dexamethasone on Symptoms of Posttraumatic Stress Disorder and Depression After Cardiac Surgery and Intensive Care Admission: Longitudinal Follow-Up of a Randomized...',
'text'=>'Kok, Lotte and others\r\nCritical Care Medicine:\r\nMarch 2016 - Volume 44 - Issue 3 - p 512â€“520',
'img'=>'file-1463591685-DDLMK9.jpg',
'file'=>NULL,
'link'=>'http://journals.lww.com/ccmjournal/Abstract/2016/03000/The_Effect_of_Dexamethasone_on_Symptoms_of.8.aspx',
'by'=>0,
'journal'=>'Site Admin',
'type'=>0,
'month_article'=>0,
'famous'=>0,
'views'=>1172,
'show'=>0,
'date'=>'2016-03-01 10:17:34',
'active'=>'1',
'user'=>0,
'created_at'=>'2020-05-15 01:20:06',
'updated_at'=>'2020-06-21 00:08:23',
'deleted_at'=>NULL
] );



Post::create( [
'id'=>4279,
'cat_id'=>8,
'tag_id'=>2,
'title'=>'Preventing ICU Subsyndromal Delirium Conversion to Delirium With Low-Dose IV Haloperidol: A Double-Blind, Placebo-Controlled Pilot Study',
'text'=>'Al-Qadheeb, Nada S and othrs\r\nCritical Care Medicine:\r\nMarch 2016 - Volume 44 - Issue 3 - p 583â€“591',
'img'=>'file-1463591732-SM39NY.jpg',
'file'=>NULL,
'link'=>'http://journals.lww.com/ccmjournal/Abstract/2016/03000/Preventing_ICU_Subsyndromal_Delirium_Conversion_to.16.aspx',
'by'=>0,
'journal'=>'Site Admin',
'type'=>0,
'month_article'=>0,
'famous'=>0,
'views'=>1172,
'show'=>0,
'date'=>'2016-03-01 10:20:00',
'active'=>'1',
'user'=>0,
'created_at'=>'2020-05-15 01:20:06',
'updated_at'=>'2020-06-21 00:08:23',
'deleted_at'=>NULL
] );



Post::create( [
'id'=>4280,
'cat_id'=>8,
'tag_id'=>6,
'title'=>'Long-Term Outcome Following Tracheostomy in Critical Care: A Systematic Review',
'text'=>'Dempsey, Ged A and others\r\nCritical Care Medicine:\r\nMarch 2016 - Volume 44 - Issue 3 - p 617â€“628',
'img'=>'file-1463591746-GEE3E7.jpg',
'file'=>NULL,
'link'=>'http://journals.lww.com/ccmjournal/Abstract/2016/03000/Long_Term_Outcome_Following_Tracheostomy_in.20.aspx',
'by'=>0,
'journal'=>'Site Admin',
'type'=>0,
'month_article'=>0,
'famous'=>0,
'views'=>1101,
'show'=>0,
'date'=>'2016-03-01 10:21:56',
'active'=>'1',
'user'=>0,
'created_at'=>'2020-05-15 01:20:06',
'updated_at'=>'2020-06-21 00:08:23',
'deleted_at'=>NULL
] );



Post::create( [
'id'=>4281,
'cat_id'=>12,
'tag_id'=>11,
'title'=>'Rapid response team and hospital mortality in hospitalized patients',
'text'=>'Boris Jung and others\r\nIntensive Care Med J. April 2016  42,4: 494 - 504',
'img'=>'file-1463591644-ZGN2DA.jpg',
'file'=>NULL,
'link'=>'http://icmjournal.esicm.org/journals/abstract.html?v=42&j=134&i=4&a=4254_10.1007_s00134-016-4254-2&doi=',
'by'=>0,
'journal'=>'Site Admin',
'type'=>0,
'month_article'=>0,
'famous'=>0,
'views'=>1212,
'show'=>0,
'date'=>'2016-03-15 21:11:36',
'active'=>'1',
'user'=>0,
'created_at'=>'2020-05-15 01:20:06',
'updated_at'=>'2020-06-21 00:08:23',
'deleted_at'=>NULL
] );



Post::create( [
'id'=>4282,
'cat_id'=>8,
'tag_id'=>3,
'title'=>'Normocaloric versus hypocaloric feeding on the outcomes of ICU patients: a systematic review and meta-analysis',
'text'=>'Paul E. Marik, Michael H. Hooper\r\nIntensive Care Med J. March 2016  42,3: 316 - 323',
'img'=>'file-1463591657-K8BRDF.jpg',
'file'=>NULL,
'link'=>'http://icmjournal.esicm.org/journals/abstract.html?v=42&j=134&i=3&a=4131_10.1007_s00134-015-4131-4&doi=',
'by'=>0,
'journal'=>'Site Admin',
'type'=>0,
'month_article'=>0,
'famous'=>0,
'views'=>1101,
'show'=>0,
'date'=>'2016-03-06 10:28:58',
'active'=>'1',
'user'=>0,
'created_at'=>'2020-05-15 01:20:06',
'updated_at'=>'2020-06-21 00:08:23',
'deleted_at'=>NULL
] );



Post::create( [
'id'=>4283,
'cat_id'=>8,
'tag_id'=>7,
'title'=>'Systematic review including re-analyses of 1148 individual data sets of central venous pressure as a predictor of fluid responsiveness',
'text'=>'T. G. Eskesen, M. Wetterslev, A. Perner\r\nIntensive Care Med J. March 2016 42,3: 324 - 332',
'img'=>'file-1463591732-SM39NY.jpg',
'file'=>NULL,
'link'=>'http://icmjournal.esicm.org/journals/abstract.html?v=42&j=134&i=3&a=4168_10.1007_s00134-015-4168-4&doi=',
'by'=>0,
'journal'=>'Site Admin',
'type'=>0,
'month_article'=>0,
'famous'=>0,
'views'=>1101,
'show'=>0,
'date'=>'2016-03-06 10:31:54',
'active'=>'1',
'user'=>0,
'created_at'=>'2020-05-15 01:20:06',
'updated_at'=>'2020-06-21 00:08:23',
'deleted_at'=>NULL
] );



Post::create( [
'id'=>4284,
'cat_id'=>8,
'tag_id'=>1,
'title'=>'Cytomegalovirus reactivation and mortality in patients with acute respiratory distress syndrome',
'text'=>'David S. Y. Ong and others\r\nIntensive Care Med J. March 2016 42,3: 333 - 341',
'img'=>'file-1463591639-4YG8JY.jpg',
'file'=>NULL,
'link'=>'http://icmjournal.esicm.org/journals/abstract.html?v=42&j=134&i=3&a=4071_10.1007_s00134-015-4071-z&doi=',
'by'=>0,
'journal'=>'Site Admin',
'type'=>0,
'month_article'=>0,
'famous'=>0,
'views'=>1130,
'show'=>0,
'date'=>'2016-03-06 10:34:15',
'active'=>'1',
'user'=>0,
'created_at'=>'2020-05-15 01:20:06',
'updated_at'=>'2020-06-21 00:08:23',
'deleted_at'=>NULL
] );



Post::create( [
'id'=>4291,
'cat_id'=>8,
'tag_id'=>1,
'title'=>'FDA: Weigh Risks Before Prescribing Fluoroquinolones',
'text'=>'By IDSE News Staff\r\nMAY 13, 2016',
'img'=>'file-1463591732-SM39NY.jpg',
'file'=>NULL,
'link'=>'http://www.idse.net/Bacterial-infections/Article/05-16/FDA-Weigh-Risks-Before-Prescribing-Fluoroquinolones/36324/ses=ogst',
'by'=>0,
'journal'=>'Site Admin',
'type'=>0,
'month_article'=>0,
'famous'=>0,
'views'=>1130,
'show'=>0,
'date'=>'2016-05-13 12:14:28',
'active'=>'1',
'user'=>0,
'created_at'=>'2020-05-15 01:20:06',
'updated_at'=>'2020-06-21 00:08:23',
'deleted_at'=>NULL
] );



Post::create( [
'id'=>4292,
'cat_id'=>8,
'tag_id'=>9,
'title'=>'LiFe: a liver injury score to predict outcome in critically ill patients',
'text'=>'Christin Edmark and others\r\nIntensive Care Med J. March 2016 42,3: 361 - 369',
'img'=>'file-1463591650-J14MMU.jpg',
'file'=>NULL,
'link'=>'http://icmjournal.esicm.org/journals/abstract.html?v=42&j=134&i=3&a=4203_10.1007_s00134-015-4203-5&doi=',
'by'=>0,
'journal'=>'Site Admin',
'type'=>0,
'month_article'=>0,
'famous'=>0,
'views'=>1101,
'show'=>0,
'date'=>'2016-03-06 10:49:32',
'active'=>'1',
'user'=>0,
'created_at'=>'2020-05-15 01:20:06',
'updated_at'=>'2020-06-21 00:08:23',
'deleted_at'=>NULL
] );



Post::create( [
'id'=>4293,
'cat_id'=>8,
'tag_id'=>9,
'title'=>'LiFe: a liver injury score to predict outcome in critically ill patients',
'text'=>'Christin Edmark and others\r\nIntensive Care Med J. March 2016 42,3: 361 - 369',
'img'=>'file-1463591752-XK911T.jpg',
'file'=>NULL,
'link'=>'http://icmjournal.esicm.org/journals/abstract.html?v=42&j=134&i=3&a=4203_10.1007_s00134-015-4203-5&doi=',
'by'=>0,
'journal'=>'Site Admin',
'type'=>0,
'month_article'=>0,
'famous'=>0,
'views'=>1101,
'show'=>0,
'date'=>'0000-00-00 00:00:00',
'active'=>'1',
'user'=>0,
'created_at'=>'2020-05-15 01:20:06',
'updated_at'=>'2020-06-21 00:08:23',
'deleted_at'=>NULL
] );



Post::create( [
'id'=>4294,
'cat_id'=>8,
'tag_id'=>7,
'title'=>'The ENCOURAGE mortality risk score and analysis of long-term outcomes after VA-ECMO for acute myocardial infarction with cardiogenic shock',
'text'=>'GrÃ©goire Muller and others\r\nIntensive Care Med J. March 2016 42,3: 361 - 369',
'img'=>'file-1463591685-DDLMK9.jpg',
'file'=>NULL,
'link'=>'http://icmjournal.esicm.org/journals/abstract.html?v=42&j=134&i=3&a=4223_10.1007_s00134-016-4223-9&doi=',
'by'=>0,
'journal'=>'Site Admin',
'type'=>0,
'month_article'=>0,
'famous'=>0,
'views'=>1101,
'show'=>0,
'date'=>'2016-03-06 10:57:29',
'active'=>'1',
'user'=>0,
'created_at'=>'2020-05-15 01:20:06',
'updated_at'=>'2020-06-21 00:08:23',
'deleted_at'=>NULL
] );



Post::create( [
'id'=>4295,
'cat_id'=>12,
'tag_id'=>1,
'title'=>'Effect of Noninvasive Ventilation on Tracheal Reintubation Among Patients With Hypoxemic Respiratory Failure Following Abdominal Surgery',
'text'=>'Samir Jaber and others for the NIVAS Study Group\r\nJAMA. Published online March 15, 2016',
'img'=>'file-1463591685-DDLMK9.jpg',
'file'=>NULL,
'link'=>'http://jama.jamanetwork.com/article.aspx?articleid=2503470',
'by'=>0,
'journal'=>'Site Admin',
'type'=>0,
'month_article'=>0,
'famous'=>0,
'views'=>1241,
'show'=>0,
'date'=>'2016-03-15 21:14:08',
'active'=>'1',
'user'=>0,
'created_at'=>'2020-05-15 01:20:06',
'updated_at'=>'2020-06-21 00:08:23',
'deleted_at'=>NULL
] );



Post::create( [
'id'=>4296,
'cat_id'=>8,
'tag_id'=>6,
'title'=>'High-Flow Nasal Oxygen or Noninvasive Ventilation for Postextubation Hypoxemia',
'text'=>'Giulia Spoletini, MD1 Erik Garpestad, MD1 Nicholas S. Hill, MD\r\nJAMA. Published online March 15, 2016',
'img'=>'file-1463591657-K8BRDF.jpg',
'file'=>NULL,
'link'=>'http://jama.jamanetwork.com/article.aspx?articleid=2503418',
'by'=>0,
'journal'=>'Site Admin',
'type'=>0,
'month_article'=>0,
'famous'=>0,
'views'=>1101,
'show'=>0,
'date'=>'2016-03-15 21:20:40',
'active'=>'1',
'user'=>0,
'created_at'=>'2020-05-15 01:20:06',
'updated_at'=>'2020-06-21 00:08:23',
'deleted_at'=>NULL
] );



Post::create( [
'id'=>4297,
'cat_id'=>8,
'tag_id'=>7,
'title'=>'Liver function predicts survival in patients undergoing extracorporeal membrane oxygenation following cardiovascular surgery',
'text'=>'Roth C, Schrutka L, Binder C, Kriechbaumer L, Heinz G, Lang I, Maurer G, Koinig H, Steinlechner B, Niessner A, Distelmaier K, Goliasch G\r\nCritical Care 2016, 20 :57 11 March 2016',
'img'=>'file-1463591725-9J88SF.jpg',
'file'=>NULL,
'link'=>'http://ccforum.biomedcentral.com/articles/10.1186/s13054-016-1242-4',
'by'=>0,
'journal'=>'Site Admin',
'type'=>0,
'month_article'=>0,
'famous'=>0,
'views'=>1101,
'show'=>0,
'date'=>'2016-03-11 18:58:18',
'active'=>'1',
'user'=>0,
'created_at'=>'2020-05-15 01:20:06',
'updated_at'=>'2020-06-21 00:08:23',
'deleted_at'=>NULL
] );



Post::create( [
'id'=>4298,
'cat_id'=>8,
'tag_id'=>5,
'title'=>'Randomized Trial of Stent versus Surgery for Asymptomatic Carotid Stenosis',
'text'=>'K. Rosenfield and Others \r\nN Engl J Med 2016374:1011-1020',
'img'=>'file-1463591732-SM39NY.jpg',
'file'=>NULL,
'link'=>'http://www.nejm.org/doi/full/10.1056/NEJMoa1515706',
'by'=>0,
'journal'=>'Site Admin',
'type'=>0,
'month_article'=>0,
'famous'=>0,
'views'=>1114,
'show'=>0,
'date'=>'2016-03-15 21:02:23',
'active'=>'1',
'user'=>0,
'created_at'=>'2020-05-15 01:20:06',
'updated_at'=>'2020-06-21 00:08:23',
'deleted_at'=>NULL
] );



Post::create( [
'id'=>4299,
'cat_id'=>8,
'tag_id'=>3,
'title'=>'Early versus Late Parenteral Nutrition in Critically Ill Children',
'text'=>'T. Fivez and Others\r\nN Eng J Med, March 15, 2016',
'img'=>'file-1463591746-GEE3E7.jpg',
'file'=>NULL,
'link'=>'http://www.nejm.org/doi/full/10.1056/NEJMoa1514762',
'by'=>0,
'journal'=>'Site Admin',
'type'=>0,
'month_article'=>0,
'famous'=>0,
'views'=>1101,
'show'=>0,
'date'=>'2016-03-15 21:04:58',
'active'=>'1',
'user'=>0,
'created_at'=>'2020-05-15 01:20:06',
'updated_at'=>'2020-06-21 00:08:23',
'deleted_at'=>NULL
] );



Post::create( [
'id'=>4300,
'cat_id'=>8,
'tag_id'=>1,
'title'=>'Incidence, Risk Factors, and Attributable Mortality of Secondary Infections in the Intensive Care Unit After Admission for Sepsis',
'text'=>'Lonneke A. van Vught and others for the MARS Consortium\r\nJAMA. Published online March 15, 2016',
'img'=>'file-1463591725-9J88SF.jpg',
'file'=>NULL,
'link'=>'http://jama.jamanetwork.com/article.aspx?articleid=2503469',
'by'=>0,
'journal'=>'Site Admin',
'type'=>0,
'month_article'=>0,
'famous'=>0,
'views'=>1130,
'show'=>0,
'date'=>'2016-03-15 21:08:05',
'active'=>'1',
'user'=>0,
'created_at'=>'2020-05-15 01:20:06',
'updated_at'=>'2020-06-21 00:08:23',
'deleted_at'=>NULL
] );



Post::create( [
'id'=>4301,
'cat_id'=>12,
'tag_id'=>6,
'title'=>'Effect of Postextubation High-Flow Nasal Cannula vs Conventional Oxygen Therapy on Reintubation in Low-Risk Patients',
'text'=>'Gonzalo HernÃ¡ndez and others\r\nJAMA. Published online March 15, 2016',
'img'=>'file-1463591752-XK911T.jpg',
'file'=>NULL,
'link'=>'http://jama.jamanetwork.com/article.aspx?articleid=2503420',
'by'=>0,
'journal'=>'Site Admin',
'type'=>0,
'month_article'=>0,
'famous'=>0,
'views'=>1212,
'show'=>0,
'date'=>'2016-03-15 21:11:04',
'active'=>'1',
'user'=>0,
'created_at'=>'2020-05-15 01:20:06',
'updated_at'=>'2020-06-21 00:08:23',
'deleted_at'=>NULL
] );



Post::create( [
'id'=>4302,
'cat_id'=>9,
'tag_id'=>4,
'title'=>'The European guideline on management of major bleeding and coagulopathy following trauma 2016 : fourth edition',
'text'=>'Rolf Rossaint AND OTHERS\r\nCritical Care 2016- 20:100. 12 April 2016',
'img'=>'file-1463591639-4YG8JY.jpg',
'file'=>NULL,
'link'=>'http://ccforum.biomedcentral.com/articles/10.1186/s13054-016-1265-x',
'by'=>0,
'journal'=>'Site Admin',
'type'=>0,
'month_article'=>0,
'famous'=>0,
'views'=>1198,
'show'=>0,
'date'=>'2016-05-12 05:39:41',
'active'=>'1',
'user'=>0,
'created_at'=>'2020-05-15 01:20:06',
'updated_at'=>'2020-06-21 00:08:23',
'deleted_at'=>NULL
] );



Post::create( [
'id'=>4303,
'cat_id'=>8,
'tag_id'=>1,
'title'=>'Immunosuppression and Secondary Infection in Sepsis\r\nPart, Not All, of the Story',
'text'=>'Derek C. Angus, MD, MPH1,2,3 Steven Opal, MD4\r\nJAMA. Published online March 15, 2016',
'img'=>'file-1463591746-GEE3E7.jpg',
'file'=>NULL,
'link'=>'http://jama.jamanetwork.com/article.aspx?articleid=2503419',
'by'=>0,
'journal'=>'Site Admin',
'type'=>0,
'month_article'=>0,
'famous'=>0,
'views'=>1130,
'show'=>0,
'date'=>'2016-03-15 21:21:57',
'active'=>'1',
'user'=>0,
'created_at'=>'2020-05-15 01:20:06',
'updated_at'=>'2020-06-21 00:08:23',
'deleted_at'=>NULL
] );



Post::create( [
'id'=>4304,
'cat_id'=>8,
'tag_id'=>5,
'title'=>'The Evolving Approach to Brain Dysfunction in Critically Ill Patients',
'text'=>'E. Wesley Ely, MD, MPH1,2,3 Pratik P. Pandharipande, MD, MSCI4\r\nJAMA. Published online March 15, 2016',
'img'=>'file-1463591657-K8BRDF.jpg',
'file'=>NULL,
'link'=>'http://jama.jamanetwork.com/article.aspx?articleid=2503417',
'by'=>0,
'journal'=>'Site Admin',
'type'=>0,
'month_article'=>0,
'famous'=>0,
'views'=>1114,
'show'=>0,
'date'=>'2016-03-15 21:23:28',
'active'=>'1',
'user'=>0,
'created_at'=>'2020-05-15 01:20:06',
'updated_at'=>'2020-06-21 00:08:23',
'deleted_at'=>NULL
] );



Post::create( [
'id'=>4305,
'cat_id'=>8,
'tag_id'=>5,
'title'=>'Effect of Inhaled Xenon on Cerebral White Matter Damage',
'text'=>'Ruut Laitio and others\r\nJAMA. 201631511:1120-1128',
'img'=>'file-1463591639-4YG8JY.jpg',
'file'=>NULL,
'link'=>'http://jama.jamanetwork.com/article.aspx?articleid=2503174',
'by'=>0,
'journal'=>'Site Admin',
'type'=>0,
'month_article'=>0,
'famous'=>0,
'views'=>1114,
'show'=>0,
'date'=>'2016-03-15 21:24:50',
'active'=>'1',
'user'=>0,
'created_at'=>'2020-05-15 01:20:06',
'updated_at'=>'2020-06-21 00:08:23',
'deleted_at'=>NULL
] );



Post::create( [
'id'=>4306,
'cat_id'=>9,
'tag_id'=>1,
'title'=>'Antimicrobial Stewardship Program',
'text'=>'JOHN HOPKINS Medicine',
'img'=>'file-1463591650-J14MMU.jpg',
'file'=>NULL,
'link'=>'http://www.hopkinsmedicine.org/amp/about/',
'by'=>0,
'journal'=>'Site Admin',
'type'=>0,
'month_article'=>0,
'famous'=>0,
'views'=>1130,
'show'=>0,
'date'=>'2016-03-23 09:03:30',
'active'=>'1',
'user'=>0,
'created_at'=>'2020-05-15 01:20:06',
'updated_at'=>'2020-06-21 00:08:23',
'deleted_at'=>NULL
] );



Post::create( [
'id'=>4307,
'cat_id'=>12,
'tag_id'=>1,
'title'=>'Validation of Bedside Ultrasound of Muscle Layer Thickness of the Quadriceps in the Critically Ill Patient VALIDUM Study: A Prospective Multicenter Study',
'text'=>'Paris MT and others\r\nJPEN J Parenter Enteral Nutr. 2016',
'img'=>'file-1463591681-AGRM7H.jpg',
'file'=>NULL,
'link'=>'http://www.ncbi.nlm.nih.gov/pubmed/26962061',
'by'=>0,
'journal'=>'Site Admin',
'type'=>0,
'month_article'=>0,
'famous'=>0,
'views'=>1241,
'show'=>0,
'date'=>'2016-05-23 10:32:08',
'active'=>'1',
'user'=>0,
'created_at'=>'2020-05-15 01:20:06',
'updated_at'=>'2020-06-21 00:08:23',
'deleted_at'=>NULL
] );



Post::create( [
'id'=>4308,
'cat_id'=>8,
'tag_id'=>1,
'title'=>'Glucocorticoid Sensitivity Is Highly Variable in Critically Ill Patients With Septic Shock and Is Associated With Disease Severity',
'text'=>'Cohen, Jeremy and others\r\nCritical Care Medicine:\r\nJune 2016 - Volume 44 - Issue 6 - p 1034â€“1041',
'img'=>'file-1463591732-SM39NY.jpg',
'file'=>NULL,
'link'=>'http://journals.lww.com/ccmjournal/Fulltext/2016/06000/Glucocorticoid_Sensitivity_Is_Highly_Variable_in.3.aspx',
'by'=>0,
'journal'=>'Site Admin',
'type'=>0,
'month_article'=>0,
'famous'=>0,
'views'=>1130,
'show'=>0,
'date'=>'2016-05-31 21:45:33',
'active'=>'1',
'user'=>0,
'created_at'=>'2020-05-15 01:20:06',
'updated_at'=>'2020-06-21 00:08:23',
'deleted_at'=>NULL
] );



Post::create( [
'id'=>4309,
'cat_id'=>8,
'tag_id'=>6,
'title'=>'Return of Voice for Ventilated Tracheostomy Patients in ICU: A Randomized Controlled Trial of Early-Targeted Intervention',
'text'=>'Freeman-Sanderson, Amy L. and others\r\nCritical Care Medicine:\r\nJune 2016 - Volume 44 - Issue 6 - p 1075â€“1081',
'img'=>'file-1463591650-J14MMU.jpg',
'file'=>NULL,
'link'=>'http://journals.lww.com/ccmjournal/Abstract/2016/06000/Return_of_Voice_for_Ventilated_Tracheostomy.8.aspx',
'by'=>0,
'journal'=>'Site Admin',
'type'=>0,
'month_article'=>0,
'famous'=>0,
'views'=>1101,
'show'=>0,
'date'=>'2016-05-31 21:48:00',
'active'=>'1',
'user'=>0,
'created_at'=>'2020-05-15 01:20:06',
'updated_at'=>'2020-06-21 00:08:23',
'deleted_at'=>NULL
] );



Post::create( [
'id'=>4310,
'cat_id'=>8,
'tag_id'=>6,
'title'=>'A Pilot Randomized Trial Comparing Weaning From Mechanical Ventilation on Pressure Support Versus Proportional Assist Ventilation',
'text'=>'Bosma, Karen J. and others\r\nCritical Care Medicine:\r\nJune 2016 - Volume 44 - Issue 6 - p 1098â€“1108',
'img'=>'file-1463591725-9J88SF.jpg',
'file'=>NULL,
'link'=>'http://journals.lww.com/ccmjournal/Abstract/2016/06000/A_Pilot_Randomized_Trial_Comparing_Weaning_From.11.aspx',
'by'=>0,
'journal'=>'Site Admin',
'type'=>0,
'month_article'=>0,
'famous'=>0,
'views'=>1101,
'show'=>0,
'date'=>'2016-05-31 21:50:54',
'active'=>'1',
'user'=>0,
'created_at'=>'2020-05-15 01:20:06',
'updated_at'=>'2020-06-21 00:08:23',
'deleted_at'=>NULL
] );



Post::create( [
'id'=>4311,
'cat_id'=>8,
'tag_id'=>11,
'title'=>'A Binational Multicenter Pilot Feasibility Randomized Controlled Trial of Early Goal-Directed Mobilization in the ICU',
'text'=>'Hodgson, Carol L. and others\r\nCritical Care Medicine:\r\nJune 2016 - Volume 44 - Issue 6 - p 1145â€“1152',
'img'=>'file-1463591746-GEE3E7.jpg',
'file'=>NULL,
'link'=>'http://journals.lww.com/ccmjournal/Abstract/2016/06000/A_Binational_Multicenter_Pilot_Feasibility.16.aspx',
'by'=>0,
'journal'=>'Site Admin',
'type'=>0,
'month_article'=>0,
'famous'=>0,
'views'=>1101,
'show'=>0,
'date'=>'2016-05-31 21:55:01',
'active'=>'1',
'user'=>0,
'created_at'=>'2020-05-15 01:20:06',
'updated_at'=>'2020-06-21 00:08:23',
'deleted_at'=>NULL
] );



Post::create( [
'id'=>4312,
'cat_id'=>8,
'tag_id'=>5,
'title'=>'Autonomic Impairment in Severe Traumatic Brain Injury: A Multimodal Neuromonitoring Study',
'text'=>'Sykora, Marek and others\r\nCritical Care Medicine:\r\nJune 2016 - Volume 44 - Issue 6 - p 1173â€“1181',
'img'=>'file-1463591752-XK911T.jpg',
'file'=>NULL,
'link'=>'http://journals.lww.com/ccmjournal/Abstract/2016/06000/Autonomic_Impairment_in_Severe_Traumatic_Brain.19.aspx',
'by'=>0,
'journal'=>'Site Admin',
'type'=>0,
'month_article'=>0,
'famous'=>0,
'views'=>1114,
'show'=>0,
'date'=>'2016-05-31 21:57:18',
'active'=>'1',
'user'=>0,
'created_at'=>'2020-05-15 01:20:06',
'updated_at'=>'2020-06-21 00:08:23',
'deleted_at'=>NULL
] );



Post::create( [
'id'=>4313,
'cat_id'=>8,
'tag_id'=>6,
'title'=>'Ultrasonography evaluation during the weaning process: the heart, the diaphragm, the pleura and the lung',
'text'=>'P. Mayo, G. Volpicelli, N. Lerolle, A. Schreiber, P. Doelken, A. Vieillard-Baron\r\nIntensive Care Med. J. July 2016: 2471107 - 1117',
'img'=>'file-1463591738-IZ3XFW.jpg',
'file'=>NULL,
'link'=>'http://icmjournal.esicm.org/journals/abstract.html?v=42&j=134&i=7&a=4245_10.1007_s00134-016-4245-3&doi=',
'by'=>0,
'journal'=>'Site Admin',
'type'=>0,
'month_article'=>0,
'famous'=>0,
'views'=>1101,
'show'=>0,
'date'=>'2016-06-06 22:02:24',
'active'=>'1',
'user'=>0,
'created_at'=>'2020-05-15 01:20:06',
'updated_at'=>'2020-06-21 00:08:23',
'deleted_at'=>NULL
] );



Post::create( [
'id'=>4314,
'cat_id'=>12,
'tag_id'=>6,
'title'=>'Strategies for Optimal Lung Ventilation in ECMO for ARDS: The SOLVE ARDS Study SOLVE ARDS',
'text'=>'Information provided by Responsible Party:\r\nEddy Fan, University of Toronto\r\nClinicalTrials.gov processed this record on June 16, 2016',
'img'=>'file-1463591644-ZGN2DA.jpg',
'file'=>NULL,
'link'=>'https://clinicaltrials.gov/ct2/show/NCT01990456?term=ards&rank=2',
'by'=>0,
'journal'=>'Site Admin',
'type'=>0,
'month_article'=>0,
'famous'=>0,
'views'=>1212,
'show'=>0,
'date'=>'2016-06-16 09:17:18',
'active'=>'1',
'user'=>0,
'created_at'=>'2020-05-15 01:20:06',
'updated_at'=>'2020-06-21 00:08:23',
'deleted_at'=>NULL
] );



Post::create( [
'id'=>4315,
'cat_id'=>8,
'tag_id'=>11,
'title'=>'The Fragility Index in Multicenter Randomized Controlled Critical Care Trials',
'text'=>'Ridgeon, Elliott E. and others\r\nCritical Care Medicine:\r\nJuly 2016 - Volume 44 - Issue 7 - p 1278â€“1284',
'img'=>'file-1463591681-AGRM7H.jpg',
'file'=>NULL,
'link'=>'http://journals.lww.com/ccmjournal/Fulltext/2016/07000/The_Fragility_Index_in_Multicenter_Randomized.4.aspx',
'by'=>0,
'journal'=>'Site Admin',
'type'=>0,
'month_article'=>0,
'famous'=>0,
'views'=>1101,
'show'=>0,
'date'=>'2016-07-01 14:34:06',
'active'=>'1',
'user'=>0,
'created_at'=>'2020-05-15 01:20:06',
'updated_at'=>'2020-06-21 00:08:23',
'deleted_at'=>NULL
] );



Post::create( [
'id'=>4316,
'cat_id'=>8,
'tag_id'=>1,
'title'=>'Derivation of Novel Risk Prediction Scores for Community-Acquired Sepsis and Severe Sepsis',
'text'=>'Wang, Henry E and others\r\nCritical Care Medicine:\r\nJuly 2016 - Volume 44 - Issue 7 - p 1285â€“1294',
'img'=>'file-1463591685-DDLMK9.jpg',
'file'=>NULL,
'link'=>'http://journals.lww.com/ccmjournal/Abstract/2016/07000/Derivation_of_Novel_Risk_Prediction_Scores_for.5.aspx',
'by'=>0,
'journal'=>'Site Admin',
'type'=>0,
'month_article'=>0,
'famous'=>0,
'views'=>1130,
'show'=>0,
'date'=>'2016-07-01 14:35:51',
'active'=>'1',
'user'=>0,
'created_at'=>'2020-05-15 01:20:06',
'updated_at'=>'2020-06-21 00:08:23',
'deleted_at'=>NULL
] );



Post::create( [
'id'=>4317,
'cat_id'=>8,
'tag_id'=>2,
'title'=>'Dexmedetomidine for the Treatment of Hyperactive Delirium Refractory to Haloperidol in Nonintubated ICU Patients: A Nonrandomized Controlled Trial',
'text'=>'Carrasco, GenÃ­s and others\r\nCritical Care Medicine:\r\nJuly 2016 - Volume 44 - Issue 7 - p 1295â€“1306',
'img'=>'file-1463591746-GEE3E7.jpg',
'file'=>NULL,
'link'=>'http://journals.lww.com/ccmjournal/Fulltext/2016/07000/Dexmedetomidine_for_the_Treatment_of_Hyperactive.6.aspx',
'by'=>0,
'journal'=>'Site Admin',
'type'=>0,
'month_article'=>0,
'famous'=>0,
'views'=>1172,
'show'=>0,
'date'=>'2016-07-02 14:36:51',
'active'=>'1',
'user'=>0,
'created_at'=>'2020-05-15 01:20:06',
'updated_at'=>'2020-06-21 00:08:23',
'deleted_at'=>NULL
] );



Post::create( [
'id'=>4318,
'cat_id'=>8,
'tag_id'=>7,
'title'=>'Impact of a Sequential Intervention on Albumin Utilization in Critical Care',
'text'=>'Lyu, Peter F and others\r\nCritical Care Medicine:\r\nJuly 2016 - Volume 44 - Issue 7 - p 1307â€“1313',
'img'=>'file-1463591732-SM39NY.jpg',
'file'=>NULL,
'link'=>'http://journals.lww.com/ccmjournal/Fulltext/2016/07000/Impact_of_a_Sequential_Intervention_on_Albumin.7.aspx',
'by'=>0,
'journal'=>'Site Admin',
'type'=>0,
'month_article'=>0,
'famous'=>0,
'views'=>1101,
'show'=>0,
'date'=>'2016-07-01 14:38:59',
'active'=>'1',
'user'=>0,
'created_at'=>'2020-05-15 01:20:06',
'updated_at'=>'2020-06-21 00:08:23',
'deleted_at'=>NULL
] );



Post::create( [
'id'=>4319,
'cat_id'=>8,
'tag_id'=>7,
'title'=>'Comparison Between Doppler-Echocardiography and Uncalibrated Pulse Contour Method for Cardiac Output Measurement: A Multicenter Observational Study',
'text'=>'Scolletta, Sabino and others\r\nCritical Care Medicine:\r\nJuly 2016 - Volume 44 - Issue 7 - p 1370â€“1379',
'img'=>'file-1463591738-IZ3XFW.jpg',
'file'=>NULL,
'link'=>'http://journals.lww.com/ccmjournal/Abstract/2016/07000/Comparison_Between_Doppler_Echocardiography_and.14.aspx',
'by'=>0,
'journal'=>'Site Admin',
'type'=>0,
'month_article'=>0,
'famous'=>0,
'views'=>1101,
'show'=>0,
'date'=>'2016-07-01 14:41:05',
'active'=>'1',
'user'=>0,
'created_at'=>'2020-05-15 01:20:06',
'updated_at'=>'2020-06-21 00:08:23',
'deleted_at'=>NULL
] );



Post::create( [
'id'=>4320,
'cat_id'=>8,
'tag_id'=>5,
'title'=>'Prognostic Significance of Hyponatremia in Acute Intracerebral Hemorrhage: Pooled Analysis of the Intensive Blood Pressure Reduction in Acute Cerebral Hemorrhage Trial Studies',
'text'=>'Carcel, Cheryl and others\r\nCritical Care Medicine:\r\nJuly 2016 - Volume 44 - Issue 7 - p 1388â€“1394',
'img'=>'file-1463591685-DDLMK9.jpg',
'file'=>NULL,
'link'=>'http://journals.lww.com/ccmjournal/Abstract/2016/07000/Prognostic_Significance_of_Hyponatremia_in_Acute.16.aspx',
'by'=>0,
'journal'=>'Site Admin',
'type'=>0,
'month_article'=>0,
'famous'=>0,
'views'=>1114,
'show'=>0,
'date'=>'2016-07-01 14:43:52',
'active'=>'1',
'user'=>0,
'created_at'=>'2020-05-15 01:20:06',
'updated_at'=>'2020-06-21 00:08:23',
'deleted_at'=>NULL
] );



Post::create( [
'id'=>4321,
'cat_id'=>8,
'tag_id'=>9,
'title'=>'Stress Ulcer Prophylaxis',
'text'=>'Barletta, Jeffrey F and others\r\nCritical Care Medicine:\r\nJuly 2016 - Volume 44 - Issue 7 - p 1395â€“1405',
'img'=>'file-1463591644-ZGN2DA.jpg',
'file'=>NULL,
'link'=>'http://journals.lww.com/ccmjournal/Abstract/2016/07000/Stress_Ulcer_Prophylaxis.17.aspx',
'by'=>0,
'journal'=>'Site Admin',
'type'=>0,
'month_article'=>0,
'famous'=>0,
'views'=>1101,
'show'=>0,
'date'=>'2016-07-02 14:46:28',
'active'=>'1',
'user'=>0,
'created_at'=>'2020-05-15 01:20:06',
'updated_at'=>'2020-06-21 00:08:23',
'deleted_at'=>NULL
] );



Post::create( [
'id'=>4322,
'cat_id'=>8,
'tag_id'=>11,
'title'=>'An Official Critical Care Societies Collaborative Statement: Burnout Syndrome in Critical Care Healthcare Professionals: A Call for Action',
'text'=>'Moss, Marc and others\r\nCritical Care Medicine:\r\nJuly 2016 - Volume 44 - Issue 7 - p 1414â€“1421',
'img'=>'file-1463591752-XK911T.jpg',
'file'=>NULL,
'link'=>'http://journals.lww.com/ccmjournal/Abstract/2016/07000/An_Official_Critical_Care_Societies_Collaborative.19.aspx',
'by'=>0,
'journal'=>'Site Admin',
'type'=>0,
'month_article'=>0,
'famous'=>0,
'views'=>1101,
'show'=>0,
'date'=>'2016-07-01 14:48:05',
'active'=>'1',
'user'=>0,
'created_at'=>'2020-05-15 01:20:06',
'updated_at'=>'2020-06-21 00:08:23',
'deleted_at'=>NULL
] );



Post::create( [
'id'=>4323,
'cat_id'=>12,
'tag_id'=>1,
'title'=>'Mechanical Ventilation and Diaphragmatic Atrophy in Critically Ill Patients: An Ultrasound Study',
'text'=>'Zambon, Massimo and others\r\nCritical Care Medicine:\r\nJuly 2016 - Volume 44 - Issue 7 - p 1347â€“1352',
'img'=>'file-1463591732-SM39NY.jpg',
'file'=>NULL,
'link'=>'http://journals.lww.com/ccmjournal/Abstract/2016/07000/Mechanical_Ventilation_and_Diaphragmatic_Atrophy.11.aspx',
'by'=>0,
'journal'=>'Site Admin',
'type'=>0,
'month_article'=>0,
'famous'=>0,
'views'=>1241,
'show'=>0,
'date'=>'2016-07-02 14:55:24',
'active'=>'1',
'user'=>0,
'created_at'=>'2020-05-15 01:20:06',
'updated_at'=>'2020-06-21 00:08:23',
'deleted_at'=>NULL
] );



Post::create( [
'id'=>4324,
'cat_id'=>12,
'tag_id'=>1,
'title'=>'Age, PaO2/FIO2, and Plateau Pressure Score: A Proposal for a Simple Outcome Score in Patients With the Acute Respiratory Distress Syndrome',
'text'=>'Villar, JesÃºs and others\r\nCritical Care Medicine:\r\nJuly 2016 - Volume 44 - Issue 7 - p 1361â€“1369',
'img'=>'file-1463591738-IZ3XFW.jpg',
'file'=>NULL,
'link'=>'http://journals.lww.com/ccmjournal/Abstract/2016/07000/Age,_PaO2_FIO2,_and_Plateau_Pressure_Score___A.13.aspx',
'by'=>0,
'journal'=>'Site Admin',
'type'=>0,
'month_article'=>0,
'famous'=>0,
'views'=>1241,
'show'=>0,
'date'=>'2016-07-02 14:55:02',
'active'=>'1',
'user'=>0,
'created_at'=>'2020-05-15 01:20:06',
'updated_at'=>'2020-06-21 00:08:23',
'deleted_at'=>NULL
] );



Post::create( [
'id'=>4325,
'cat_id'=>12,
'tag_id'=>6,
'title'=>'Ultrasonography evaluation during the weaning process: the heart, the diaphragm, the pleura and the lung',
'text'=>'P. Mayo and others\r\nIntensive Care Med J. July 201642 71107 - 1117',
'img'=>'file-1463591685-DDLMK9.jpg',
'file'=>NULL,
'link'=>'http://icmjournal.esicm.org/journals/abstract.html?v=42&j=134&i=7&a=4245_10.1007_s00134-016-4245-3&doi=',
'by'=>0,
'journal'=>'Site Admin',
'type'=>0,
'month_article'=>0,
'famous'=>0,
'views'=>1212,
'show'=>0,
'date'=>'2016-07-01 15:11:28',
'active'=>'1',
'user'=>0,
'created_at'=>'2020-05-15 01:20:06',
'updated_at'=>'2020-06-21 00:08:23',
'deleted_at'=>NULL
] );



Post::create( [
'id'=>4326,
'cat_id'=>8,
'tag_id'=>11,
'title'=>'Nurses versus physician-led interhospital critical care transport: a randomized non-inferiority trial',
'text'=>'Erik Jan van Lieshout and others\r\nIntensive Care Med J. July 201642 71146 - 1154',
'img'=>'file-1463591746-GEE3E7.jpg',
'file'=>NULL,
'link'=>'http://icmjournal.esicm.org/journals/abstract.html?v=42&j=134&i=7&a=4355_10.1007_s00134-016-4355-y&doi=',
'by'=>0,
'journal'=>'Site Admin',
'type'=>0,
'month_article'=>0,
'famous'=>0,
'views'=>1101,
'show'=>0,
'date'=>'2016-07-01 15:16:29',
'active'=>'1',
'user'=>0,
'created_at'=>'2020-05-15 01:20:06',
'updated_at'=>'2020-06-21 00:08:23',
'deleted_at'=>NULL
] );



Post::create( [
'id'=>4327,
'cat_id'=>12,
'tag_id'=>6,
'title'=>'Effects of dexmedetomidine and propofol on patient-ventilator interaction in difficult-to-wean, mechanically ventilated patients: a prospective, open-label, randomised, multicentre study',
'text'=>'Giorgio Conti and others\r\nCritical Care. July 2016. 20:206',
'img'=>'file-1463591725-9J88SF.jpg',
'file'=>NULL,
'link'=>'https://ccforum.biomedcentral.com/articles/10.1186/s13054-016-1386-2',
'by'=>0,
'journal'=>'Site Admin',
'type'=>0,
'month_article'=>0,
'famous'=>0,
'views'=>1212,
'show'=>0,
'date'=>'2016-07-02 15:24:43',
'active'=>'1',
'user'=>0,
'created_at'=>'2020-05-15 01:20:06',
'updated_at'=>'2020-06-21 00:08:23',
'deleted_at'=>NULL
] );



Post::create( [
'id'=>4328,
'cat_id'=>8,
'tag_id'=>7,
'title'=>'â€œAwakeâ€ extracorporeal membrane oxygenation ECMO: pathophysiology, technical considerations, and clinical pioneering',
'text'=>'Thomas Langer, Alessandro Santini, Nicola Bottino, Stefania Crotti, Andriy I. Batchinsky, Antonio Pesenti and Luciano Gattinoni\r\nCritical Care201620:150',
'img'=>'file-1463591650-J14MMU.jpg',
'file'=>NULL,
'link'=>'https://ccforum.biomedcentral.com/articles/10.1186/s13054-016-1329-y',
'by'=>0,
'journal'=>'Site Admin',
'type'=>0,
'month_article'=>0,
'famous'=>0,
'views'=>1101,
'show'=>0,
'date'=>'2016-07-01 15:27:39',
'active'=>'1',
'user'=>0,
'created_at'=>'2020-05-15 01:20:06',
'updated_at'=>'2020-06-21 00:08:23',
'deleted_at'=>NULL
] );



Post::create( [
'id'=>4329,
'cat_id'=>8,
'tag_id'=>1,
'title'=>'Early goal-directed therapy in severe sepsis and septic shock: insights and comparisons to ProCESS, ProMISe, and ARISE',
'text'=>'H. Bryant Nguyen and others\r\nCritical Care. July 2016. 20:160',
'img'=>'file-1463591685-DDLMK9.jpg',
'file'=>NULL,
'link'=>'https://ccforum.biomedcentral.com/articles/10.1186/s13054-016-1288-3',
'by'=>0,
'journal'=>'Site Admin',
'type'=>0,
'month_article'=>0,
'famous'=>0,
'views'=>1130,
'show'=>0,
'date'=>'2016-07-01 15:34:22',
'active'=>'1',
'user'=>0,
'created_at'=>'2020-05-15 01:20:06',
'updated_at'=>'2020-06-21 00:08:23',
'deleted_at'=>NULL
] );



Post::create( [
'id'=>4330,
'cat_id'=>8,
'tag_id'=>1,
'title'=>'Daily vancomycin dose requirements as a continuous infusion in obese versus non-obese SICU patients',
'text'=>'Hsin Lin, Daniel Dante Yeh and Alexander R. Levine\r\nCritical Care. July 2016. 20:205',
'img'=>'file-1463591725-9J88SF.jpg',
'file'=>NULL,
'link'=>'https://ccforum.biomedcentral.com/articles/10.1186/s13054-016-1363-9',
'by'=>0,
'journal'=>'Site Admin',
'type'=>0,
'month_article'=>0,
'famous'=>0,
'views'=>1130,
'show'=>0,
'date'=>'2016-07-01 15:35:42',
'active'=>'1',
'user'=>0,
'created_at'=>'2020-05-15 01:20:06',
'updated_at'=>'2020-06-21 00:08:23',
'deleted_at'=>NULL
] );



Post::create( [
'id'=>4331,
'cat_id'=>8,
'tag_id'=>8,
'title'=>'Ulinastatin to prevent acute kidney injury after cardiopulmonary bypass surgery: does serum creatinine tell the whole story',
'text'=>'Patrick M. Honore and Herbert D. Spapen\r\nCritical Care July 2016. 20:183',
'img'=>'file-1463591657-K8BRDF.jpg',
'file'=>NULL,
'link'=>'https://ccforum.biomedcentral.com/articles/10.1186/s13054-016-1356-8',
'by'=>0,
'journal'=>'Site Admin',
'type'=>0,
'month_article'=>0,
'famous'=>0,
'views'=>1152,
'show'=>0,
'date'=>'2016-07-01 15:37:07',
'active'=>'1',
'user'=>0,
'created_at'=>'2020-05-15 01:20:06',
'updated_at'=>'2020-06-21 00:08:23',
'deleted_at'=>NULL
] );



Post::create( [
'id'=>4332,
'cat_id'=>8,
'tag_id'=>7,
'title'=>'Is the 77.1 % rate of in-hospital mortality in patients receiving venoarterial extracorporeal membrane oxygenation really that high',
'text'=>'Shimizu K, Ogura H\r\nCritical Care 2016, 20 :202 15 July 2016',
'img'=>'file-1463591752-XK911T.jpg',
'file'=>NULL,
'link'=>'http://ccforum.biomedcentral.com/articles/10.1186/s13054-016-1379-1',
'by'=>0,
'journal'=>'Site Admin',
'type'=>0,
'month_article'=>0,
'famous'=>0,
'views'=>1101,
'show'=>0,
'date'=>'2016-07-15 10:39:57',
'active'=>'1',
'user'=>0,
'created_at'=>'2020-05-15 01:20:06',
'updated_at'=>'2020-06-21 00:08:23',
'deleted_at'=>NULL
] );



Post::create( [
'id'=>4333,
'cat_id'=>8,
'tag_id'=>1,
'title'=>'Multidrug resistance, inappropriate empiric therapy, and hospital mortality in Acinetobacter baumannii pneumonia and sepsis',
'text'=>'Marya D. Zilberberg, Brian H. Nathanson, Kate Sulham, Weihong Fan and Andrew F. Shorr\r\nCritical Care 2016 20:221. Published on: 11 July 2016',
'img'=>'file-1463591725-9J88SF.jpg',
'file'=>NULL,
'link'=>'http://ccforum.biomedcentral.com/articles/10.1186/s13054-016-1392-4',
'by'=>0,
'journal'=>'Site Admin',
'type'=>0,
'month_article'=>0,
'famous'=>0,
'views'=>1130,
'show'=>0,
'date'=>'2016-07-11 10:41:55',
'active'=>'1',
'user'=>0,
'created_at'=>'2020-05-15 01:20:06',
'updated_at'=>'2020-06-21 00:08:23',
'deleted_at'=>NULL
] );



Post::create( [
'id'=>4334,
'cat_id'=>12,
'tag_id'=>1,
'title'=>'Management of Adults With Hospital-acquired and Ventilator-associated Pneumonia: 2016 Clinical Practice Guidelines by IDSA and the ATS',
'text'=>'Andre C. Kalil and others.\r\nClinical Infectious Diseases  14 July 2016  63 : 1 -51',
'img'=>'file-1463591685-DDLMK9.jpg',
'file'=>NULL,
'link'=>'http://cid.oxfordjournals.org/content/early/2016/07/06/cid.ciw353.full',
'by'=>0,
'journal'=>'Site Admin',
'type'=>0,
'month_article'=>0,
'famous'=>0,
'views'=>1241,
'show'=>0,
'date'=>'2016-07-14 11:28:54',
'active'=>'1',
'user'=>0,
'created_at'=>'2020-05-15 01:20:06',
'updated_at'=>'2020-06-21 00:08:23',
'deleted_at'=>NULL
] );



Post::create( [
'id'=>4335,
'cat_id'=>9,
'tag_id'=>1,
'title'=>'Management of Adults With Hospital-acquired and Ventilator-associated Pneumonia: 2016 Clinical Practice Guidelines by IDSA and the ATS',
'text'=>'Andre C. Kalil and others.\r\nClinical Infectious Diseases  14 July 2016  63 : 1 -51',
'img'=>'file-1463591639-4YG8JY.jpg',
'file'=>NULL,
'link'=>'http://cid.oxfordjournals.org/content/early/2016/07/06/cid.ciw353.full',
'by'=>0,
'journal'=>'Site Admin',
'type'=>0,
'month_article'=>0,
'famous'=>0,
'views'=>1130,
'show'=>0,
'date'=>'2016-07-14 11:29:39',
'active'=>'1',
'user'=>0,
'created_at'=>'2020-05-15 01:20:06',
'updated_at'=>'2020-06-21 00:08:23',
'deleted_at'=>NULL
] );



Post::create( [
'id'=>4336,
'cat_id'=>12,
'tag_id'=>1,
'title'=>'Sepsis-3: What is the Meaning of a Definition',
'text'=>'Marshall, John C. MD\r\nCritical Care Medicine:\r\nAugust 2016 - Volume 44 - Issue 8 - p 1459â€“1460',
'img'=>'file-1463591639-4YG8JY.jpg',
'file'=>NULL,
'link'=>'http://journals.lww.com/ccmjournal/Fulltext/2016/08000/Sepsis_3___What_is_the_Meaning_of_a_Definition_.1.aspx',
'by'=>0,
'journal'=>'Site Admin',
'type'=>0,
'month_article'=>0,
'famous'=>0,
'views'=>1241,
'show'=>0,
'date'=>'2016-08-02 06:31:39',
'active'=>'1',
'user'=>0,
'created_at'=>'2020-05-15 01:20:06',
'updated_at'=>'2020-06-21 00:08:23',
'deleted_at'=>NULL
] );



Post::create( [
'id'=>4337,
'cat_id'=>8,
'tag_id'=>1,
'title'=>'Melioidosis Causing Critical Illness: A Review of 24 Years of Experience From the Royal Darwin Hospital ICU',
'text'=>'Stephens, Dianne P and others\r\nCritical Care Medicine:\r\nAugust 2016 - Volume 44 - Issue 8 - p 1500â€“1505',
'img'=>'file-1463591644-ZGN2DA.jpg',
'file'=>NULL,
'link'=>'http://journals.lww.com/ccmjournal/Abstract/2016/08000/Melioidosis_Causing_Critical_Illness___A_Review_of.7.aspx',
'by'=>0,
'journal'=>'Site Admin',
'type'=>0,
'month_article'=>0,
'famous'=>0,
'views'=>1130,
'show'=>0,
'date'=>'0000-00-00 00:00:00',
'active'=>'1',
'user'=>0,
'created_at'=>'2020-05-15 01:20:06',
'updated_at'=>'2020-06-21 00:08:23',
'deleted_at'=>NULL
] );



Post::create( [
'id'=>4338,
'cat_id'=>8,
'tag_id'=>1,
'title'=>'Melioidosis Causing Critical Illness: A Review of 24 Years of Experience From the Royal Darwin Hospital ICU',
'text'=>'Stephens, Dianne P and others\r\nCritical Care Medicine:\r\nAugust 2016 - Volume 44 - Issue 8 - p 1500â€“1505',
'img'=>'file-1463591685-DDLMK9.jpg',
'file'=>NULL,
'link'=>'http://journals.lww.com/ccmjournal/Abstract/2016/08000/Melioidosis_Causing_Critical_Illness___A_Review_of.7.aspx',
'by'=>0,
'journal'=>'Site Admin',
'type'=>0,
'month_article'=>0,
'famous'=>0,
'views'=>1130,
'show'=>0,
'date'=>'2016-08-01 06:36:05',
'active'=>'1',
'user'=>0,
'created_at'=>'2020-05-15 01:20:06',
'updated_at'=>'2020-06-21 00:08:23',
'deleted_at'=>NULL
] );



Post::create( [
'id'=>4339,
'cat_id'=>8,
'tag_id'=>6,
'title'=>'Hospital Variation in Early Tracheostomy in the United States: A Population-Based Study',
'text'=>'Mehta, Anuj B. and others\r\nCritical Care Medicine:\r\nAugust 2016 - Volume 44 - Issue 8 - p 1506â€“1514',
'img'=>'file-1463591725-9J88SF.jpg',
'file'=>NULL,
'link'=>'http://journals.lww.com/ccmjournal/Abstract/2016/08000/Hospital_Variation_in_Early_Tracheostomy_in_the.8.aspx',
'by'=>0,
'journal'=>'Site Admin',
'type'=>0,
'month_article'=>0,
'famous'=>0,
'views'=>1101,
'show'=>0,
'date'=>'2016-08-01 06:37:40',
'active'=>'1',
'user'=>0,
'created_at'=>'2020-05-15 01:20:06',
'updated_at'=>'2020-06-21 00:08:23',
'deleted_at'=>NULL
] );



Post::create( [
'id'=>4340,
'cat_id'=>8,
'tag_id'=>7,
'title'=>'Saline Is the Solution for Crystalloid Resuscitation',
'text'=>'Young, Paul\r\nCritical Care Medicine:\r\nAugust 2016 - Volume 44 - Issue 8 - p 1538â€“1540',
'img'=>'file-1463591746-GEE3E7.jpg',
'file'=>NULL,
'link'=>'http://journals.lww.com/ccmjournal/Citation/2016/08000/Saline_Is_the_Solution_for_Crystalloid.12.aspx',
'by'=>0,
'journal'=>'Site Admin',
'type'=>0,
'month_article'=>0,
'famous'=>0,
'views'=>1101,
'show'=>0,
'date'=>'2016-08-01 06:40:50',
'active'=>'1',
'user'=>0,
'created_at'=>'2020-05-15 01:20:06',
'updated_at'=>'2020-06-21 00:08:23',
'deleted_at'=>NULL
] );



Post::create( [
'id'=>4341,
'cat_id'=>8,
'tag_id'=>7,
'title'=>'Saline Is Not the First Choice for Crystalloid Resuscitation Fluids',
'text'=>'Semler, Matthew W. Rice, Todd W\r\nCritical Care Medicine:\r\nAugust 2016 - Volume 44 - Issue 8 - p 1541â€“1544',
'img'=>'file-1463591657-K8BRDF.jpg',
'file'=>NULL,
'link'=>'http://journals.lww.com/ccmjournal/Citation/2016/08000/Saline_Is_Not_the_First_Choice_for_Crystalloid.13.aspx',
'by'=>0,
'journal'=>'Site Admin',
'type'=>0,
'month_article'=>0,
'famous'=>0,
'views'=>1101,
'show'=>0,
'date'=>'2016-08-01 06:40:32',
'active'=>'1',
'user'=>0,
'created_at'=>'2020-05-15 01:20:06',
'updated_at'=>'2020-06-21 00:08:23',
'deleted_at'=>NULL
] );



Post::create( [
'id'=>4342,
'cat_id'=>8,
'tag_id'=>5,
'title'=>'Unpeeling the Evidence for the Banana Bag: Evidence-Based Recommendations for the Management of Alcohol-Associated Vitamin and Electrolyte Deficiencies in the ICU',
'text'=>'Flannery, Alexander H. Adkins, David A. Cook, Aaron M.\r\nCritical Care Medicine:\r\nAugust 2016 - Volume 44 - Issue 8 - p 1545â€“1552',
'img'=>'file-1463591685-DDLMK9.jpg',
'file'=>NULL,
'link'=>'http://journals.lww.com/ccmjournal/Fulltext/2016/08000/Unpeeling_the_Evidence_for_the_Banana_Bag__.14.aspx',
'by'=>0,
'journal'=>'Site Admin',
'type'=>0,
'month_article'=>0,
'famous'=>0,
'views'=>1114,
'show'=>0,
'date'=>'2016-08-02 06:43:16',
'active'=>'1',
'user'=>0,
'created_at'=>'2020-05-15 01:20:06',
'updated_at'=>'2020-06-21 00:08:23',
'deleted_at'=>NULL
] );



Post::create( [
'id'=>4343,
'cat_id'=>8,
'tag_id'=>11,
'title'=>'ICU Admission, Discharge, and Triage Guidelines: A Framework to Enhance Clinical Operations, Development of Institutional Policies, and Further Research',
'text'=>'Nates, Joseph L and others\r\nCritical Care Medicine:\r\nAugust 2016 - Volume 44 - Issue 8 - p 1553â€“1602',
'img'=>'file-1463591725-9J88SF.jpg',
'file'=>NULL,
'link'=>'http://journals.lww.com/ccmjournal/Abstract/2016/08000/ICU_Admission,_Discharge,_and_Triage_Guidelines__.15.aspx',
'by'=>0,
'journal'=>'Site Admin',
'type'=>0,
'month_article'=>0,
'famous'=>0,
'views'=>1101,
'show'=>0,
'date'=>'2016-08-01 06:45:05',
'active'=>'1',
'user'=>0,
'created_at'=>'2020-05-15 01:20:06',
'updated_at'=>'2020-06-21 00:08:23',
'deleted_at'=>NULL
] );



Post::create( [
'id'=>4344,
'cat_id'=>8,
'tag_id'=>1,
'title'=>'Surviving Severe Sepsis: Is That Enough',
'text'=>'Anderson-Shaw, Lisa \r\nCritical Care Medicine:\r\nAugust 2016 - Volume 44 - Issue 8 - p 1603â€“1604',
'img'=>'file-1463591639-4YG8JY.jpg',
'file'=>NULL,
'link'=>'http://journals.lww.com/ccmjournal/Fulltext/2016/08000/Surviving_Severe_Sepsis___Is_That_Enough__.16.aspx',
'by'=>0,
'journal'=>'Site Admin',
'type'=>0,
'month_article'=>0,
'famous'=>0,
'views'=>1130,
'show'=>0,
'date'=>'2016-08-01 06:46:12',
'active'=>'1',
'user'=>0,
'created_at'=>'2020-05-15 01:20:06',
'updated_at'=>'2020-06-21 00:08:23',
'deleted_at'=>NULL
] );



Post::create( [
'id'=>4345,
'cat_id'=>8,
'tag_id'=>3,
'title'=>'Early versus Late Parenteral Nutrition\r\nin Critically Ill Adults',
'text'=>'Michael P. Casaer and others\r\nN Engl J Med 2011365:506-17',
'img'=>'file-1463591685-DDLMK9.jpg',
'file'=>NULL,
'link'=>'https://www.ccm.pitt.edu/sites/default/files/calendar_event_articles/nejmoa1102662.pdf',
'by'=>0,
'journal'=>'Site Admin',
'type'=>0,
'month_article'=>0,
'famous'=>1,
'views'=>1196,
'show'=>0,
'date'=>'2011-06-27 12:57:06',
'active'=>'1',
'user'=>0,
'created_at'=>'2020-05-15 01:20:06',
'updated_at'=>'2020-06-21 00:08:23',
'deleted_at'=>NULL
] );



Post::create( [
'id'=>4346,
'cat_id'=>8,
'tag_id'=>3,
'title'=>'Optimisation of energy provision with supplemental parenteral nutrition in critically ill patients: a randomised controlled clinical trial',
'text'=>'Claudia Paula Heidegger and others\r\nThe Lancet, December 2012',
'img'=>'file-1463591738-IZ3XFW.jpg',
'file'=>NULL,
'link'=>'http://thelancet.com/journals/lancet/article/PIIS0140-6736(12)61351-8/fulltext',
'by'=>0,
'journal'=>'Site Admin',
'type'=>0,
'month_article'=>0,
'famous'=>1,
'views'=>1196,
'show'=>0,
'date'=>'2012-12-01 13:02:03',
'active'=>'1',
'user'=>0,
'created_at'=>'2020-05-15 01:20:06',
'updated_at'=>'2020-06-21 00:08:23',
'deleted_at'=>NULL
] );



Post::create( [
'id'=>4347,
'cat_id'=>8,
'tag_id'=>3,
'title'=>'Early Parenteral Nutrition in Critically Ill Patients With Short-term Relative Contraindications to Early Enteral Nutrition\r\nA Randomized Controlled Tria',
'text'=>'Gordon S. Doig and others\r\nJAMA. 201330920:2130-2138',
'img'=>'file-1463591746-GEE3E7.jpg',
'file'=>NULL,
'link'=>'http://jama.jamanetwork.com/article.aspx?articleid=1689534',
'by'=>0,
'journal'=>'Site Admin',
'type'=>0,
'month_article'=>0,
'famous'=>1,
'views'=>1196,
'show'=>0,
'date'=>'2013-05-22 13:04:40',
'active'=>'1',
'user'=>0,
'created_at'=>'2020-05-15 01:20:06',
'updated_at'=>'2020-06-21 00:08:23',
'deleted_at'=>NULL
] );



Post::create( [
'id'=>4348,
'cat_id'=>8,
'tag_id'=>3,
'title'=>'Enteral versus parenteral nutrition in critically ill patients: an updated systematic review and meta-analysis of randomized controlled trials.',
'text'=>'Elke G, van Zanten AR, Lemieux M, McCall M, Jeejeebhoy KN, Kott M, Jiang X, Day AG, Heyland DK.\r\nCrit Care. 2016 Apr 29201:117',
'img'=>'file-1463591650-J14MMU.jpg',
'file'=>NULL,
'link'=>'http://ccforum.biomedcentral.com/articles/10.1186/s13054-016-1298-1',
'by'=>0,
'journal'=>'Site Admin',
'type'=>0,
'month_article'=>0,
'famous'=>1,
'views'=>1196,
'show'=>0,
'date'=>'2016-04-29 17:13:03',
'active'=>'1',
'user'=>0,
'created_at'=>'2020-05-15 01:20:06',
'updated_at'=>'2020-06-21 00:08:23',
'deleted_at'=>NULL
] );



Post::create( [
'id'=>4349,
'cat_id'=>8,
'tag_id'=>3,
'title'=>'Intravenous fish oil lipid emulsions in critically ill patients: an updated systematic review and meta-analysis',
'text'=>', Pascal L Langlois, Rupinder Dhaliwal, Margot Lemieux and Daren K Heyland\r\nCritical Care, 16 April 015 19:167',
'img'=>'file-1463591685-DDLMK9.jpg',
'file'=>NULL,
'link'=>'http://ccforum.biomedcentral.com/articles/10.1186/s13054-015-0888-7',
'by'=>0,
'journal'=>'Site Admin',
'type'=>0,
'month_article'=>0,
'famous'=>1,
'views'=>1196,
'show'=>0,
'date'=>'2015-04-16 17:12:03',
'active'=>'1',
'user'=>0,
'created_at'=>'2020-05-15 01:20:06',
'updated_at'=>'2020-06-21 00:08:23',
'deleted_at'=>NULL
] );



Post::create( [
'id'=>4350,
'cat_id'=>8,
'tag_id'=>3,
'title'=>'Trial of the Route of Early Nutritional Support in Critically Ill Adults',
'text'=>'Sheila E. Harvey and others for the CALORIES Trial Investigators\r\nN Engl J Med 371:1673-1684. October 30, 2014',
'img'=>'file-1463591725-9J88SF.jpg',
'file'=>NULL,
'link'=>'http://www.nejm.org/doi/full/10.1056/NEJMoa1409860',
'by'=>0,
'journal'=>'Site Admin',
'type'=>0,
'month_article'=>0,
'famous'=>1,
'views'=>1196,
'show'=>0,
'date'=>'2014-10-30 17:17:22',
'active'=>'1',
'user'=>0,
'created_at'=>'2020-05-15 01:20:06',
'updated_at'=>'2020-06-21 00:08:23',
'deleted_at'=>NULL
] );



Post::create( [
'id'=>4351,
'cat_id'=>9,
'tag_id'=>1,
'title'=>'CDC Issues Updated Zika Recommendations: Interim Guidance for healthcare providers caring for pregnant women with possible exposure to Zika virus Interim Guidance for the prevention of sexually transmitted Zika virus',
'text'=>'CDC \r\nMonday, July 25, 2016, 11:00 a.m. ET',
'img'=>'file-1463591732-SM39NY.jpg',
'file'=>NULL,
'link'=>'http://www.cdc.gov/media/releases/2016/s0725-zika-guidance.html',
'by'=>0,
'journal'=>'Site Admin',
'type'=>0,
'month_article'=>0,
'famous'=>0,
'views'=>1130,
'show'=>0,
'date'=>'2016-07-25 09:18:44',
'active'=>'1',
'user'=>0,
'created_at'=>'2020-05-15 01:20:06',
'updated_at'=>'2020-06-21 00:08:23',
'deleted_at'=>NULL
] );



Post::create( [
'id'=>4352,
'cat_id'=>8,
'tag_id'=>7,
'title'=>'Application of a simplified definition of diastolic function in severe sepsis and septic shock',
'text'=>'Michael J. Lanspa and others\r\nCritical Care. August 4, 2016 20:243',
'img'=>'file-1463591746-GEE3E7.jpg',
'file'=>NULL,
'link'=>'http://ccforum.biomedcentral.com/articles/10.1186/s13054-016-1421-3',
'by'=>0,
'journal'=>'Site Admin',
'type'=>0,
'month_article'=>0,
'famous'=>0,
'views'=>1101,
'show'=>0,
'date'=>'2016-08-04 10:44:07',
'active'=>'1',
'user'=>0,
'created_at'=>'2020-05-15 01:20:06',
'updated_at'=>'2020-06-21 00:08:23',
'deleted_at'=>NULL
] );



Post::create( [
'id'=>4353,
'cat_id'=>12,
'tag_id'=>8,
'title'=>'Acute kidney injury in critically ill cancer patients: an update',
'text'=>'Critical Care. August 2, 2016 20:209',
'img'=>'file-1463591732-SM39NY.jpg',
'file'=>NULL,
'link'=>'http://ccforum.biomedcentral.com/articles/10.1186/s13054-016-1382-6',
'by'=>0,
'journal'=>'Norbert Lameire, Raymond Vanholder, Wim Van Biesen and Dominique Benoit',
'type'=>0,
'month_article'=>0,
'famous'=>0,
'views'=>1263,
'show'=>0,
'date'=>'2016-08-02 11:00:41',
'active'=>'1',
'user'=>0,
'created_at'=>'2020-05-15 01:20:06',
'updated_at'=>'2020-06-21 00:08:23',
'deleted_at'=>NULL
] );



Post::create( [
'id'=>4354,
'cat_id'=>8,
'tag_id'=>7,
'title'=>'Effects of dexmedetomidine and esmolol on systemic hemodynamics and exogenous lactate clearance in early experimental septic shock',
'text'=>'Glenn HernÃ¡ndez and others\r\nCritical Care. August 2, 201620:234',
'img'=>'file-1463591644-ZGN2DA.jpg',
'file'=>NULL,
'link'=>'http://ccforum.biomedcentral.com/articles/10.1186/s13054-016-1419-x',
'by'=>0,
'journal'=>'Site Admin',
'type'=>0,
'month_article'=>0,
'famous'=>0,
'views'=>1101,
'show'=>0,
'date'=>'2016-08-02 11:14:38',
'active'=>'1',
'user'=>0,
'created_at'=>'2020-05-15 01:20:06',
'updated_at'=>'2020-06-21 00:08:23',
'deleted_at'=>NULL
] );



Post::create( [
'id'=>4355,
'cat_id'=>8,
'tag_id'=>6,
'title'=>'Effect of Early vs Late Tracheostomy Placement on Survival in Patients Receiving Mechanical Ventilation\r\nThe TracMan Randomized Trial',
'text'=>'Duncan Young, DM David A. Harrison, PhD Brian H. Cuthbertson, MD Kathy Rowan, DPhil  for the Trackman Collaborators\r\nJAMA. 201330920:2121-2129',
'img'=>'file-1463591746-GEE3E7.jpg',
'file'=>NULL,
'link'=>'http://jama.jamanetwork.com/article.aspx?articleid=1690674',
'by'=>0,
'journal'=>'Site Admin',
'type'=>0,
'month_article'=>0,
'famous'=>1,
'views'=>1196,
'show'=>0,
'date'=>'2013-05-22 13:14:04',
'active'=>'1',
'user'=>0,
'created_at'=>'2020-05-15 01:20:06',
'updated_at'=>'2020-06-21 00:08:23',
'deleted_at'=>NULL
] );



Post::create( [
'id'=>4356,
'cat_id'=>8,
'tag_id'=>6,
'title'=>'Early vs Late Tracheotomy for Prevention of Pneumonia in Mechanically Ventilated Adult ICU Patients\r\nA Randomized Controlled Trial',
'text'=>'Pier Paolo Terragni, MD Massimo Antonelli, MD Roberto Fumagalli, MD Chiara Faggiano, MD Maurizio Berardino, MD Franco Bobbio Pallavicini, MD Antonio Miletto, MD Salvatore Mangione, MD Angelo U. Sinardi, MD Mauro Pastorelli, MD Nicoletta Vivaldi, MD Alberto Pasetto, MD Giorgio Della Rocca, MD Rosario Urbino, MD Claudia Filippini, PhD Eva Pagano, PhD Andrea Evangelista, PhD Gianni Ciccone, MD Luciana Mascia, MD, PhD V. Marco Ranieri,Pier Paolo Terragni, MD Massimo Antonelli, MD Roberto Fumagalli, MD Chiara Faggiano, MD Maurizio Berardino, MD Franco Bobbio Pallavicini, MD Antonio Miletto, MD Salvatore Mangione, MD Angelo U. Sinardi, MD Mauro Pastorelli, MD Nicoletta Vivaldi, MD Alberto Pasetto, MD Giorgio Della Rocca, MD Rosario Urbino, MD Claudia Filippini, PhD Eva Pagano, PhD Andrea Evangelista, PhD Gianni Ciccone, MD Luciana Mascia, MD, PhD V. Marco Ranieri, MD\r\nJAMA. 201030315:1483-1489.',
'img'=>'file-1463591752-XK911T.jpg',
'file'=>NULL,
'link'=>'http://jama.jamanetwork.com/article.aspx?articleid=185688',
'by'=>0,
'journal'=>'Site Admin',
'type'=>0,
'month_article'=>0,
'famous'=>1,
'views'=>1196,
'show'=>0,
'date'=>'2010-04-21 13:24:12',
'active'=>'1',
'user'=>0,
'created_at'=>'2020-05-15 01:20:06',
'updated_at'=>'2020-06-21 00:08:23',
'deleted_at'=>NULL
] );



Post::create( [
'id'=>4357,
'cat_id'=>8,
'tag_id'=>6,
'title'=>'Effect of early versus late or no tracheostomy on mortality and pneumonia of critically ill patients receiving mechanical ventilation: a systematic review and meta-analysis',
'text'=>'Dr Ilias I Siempos, MDcorrespondencePress enter key for correspondence informationemailPress enter key to Email the author, Theodora K Ntaidou, MD, Filippos T Filippidis, MD, Prof Augustine M K Choi, MD\r\nThe Lancet, Volume 3, No. 2, p150â€“158, February 2015',
'img'=>'file-1463591738-IZ3XFW.jpg',
'file'=>NULL,
'link'=>'http://www.thelancet.com/journals/lanres/article/PIIS2213-2600(15)00007-7/fulltext?rss=yes',
'by'=>0,
'journal'=>'Site Admin',
'type'=>0,
'month_article'=>0,
'famous'=>0,
'views'=>1101,
'show'=>0,
'date'=>'2015-02-05 13:27:21',
'active'=>'1',
'user'=>0,
'created_at'=>'2020-05-15 01:20:06',
'updated_at'=>'2020-06-21 00:08:23',
'deleted_at'=>NULL
] );



Post::create( [
'id'=>4358,
'cat_id'=>8,
'tag_id'=>6,
'title'=>'Timing of tracheostomy as a determinant of weaning success in critically ill patients: a retrospective study',
'text'=>'Chia-Lin Hsu, Kuan-Yu ChenEmail author, Chia-Hsuin Chang, Jih-Shuin Jerng, Chong-Jen Yu and Pan-Chyr Yang\r\nCritical Care20049:R46',
'img'=>'file-1463591681-AGRM7H.jpg',
'file'=>NULL,
'link'=>'https://ccforum.biomedcentral.com/articles/10.1186/cc3018',
'by'=>0,
'journal'=>'Site Admin',
'type'=>0,
'month_article'=>0,
'famous'=>0,
'views'=>1101,
'show'=>0,
'date'=>'2006-08-02 13:42:50',
'active'=>'1',
'user'=>0,
'created_at'=>'2020-05-15 01:20:06',
'updated_at'=>'2020-06-21 00:08:23',
'deleted_at'=>NULL
] );



Post::create( [
'id'=>4359,
'cat_id'=>8,
'tag_id'=>6,
'title'=>'Tracheostomy: Epidemiology, Indications, Timing, Technique, and Outcomes',
'text'=>'Nora H Cheung MD and Lena M Napolitano MD\r\nRESPIRATORY CARE â€¢ JUNE 2014 VOL 59 NO 6',
'img'=>'file-1463591725-9J88SF.jpg',
'file'=>NULL,
'link'=>'http://rc.rcjournal.com/content/59/6/895.full.pdf',
'by'=>0,
'journal'=>'Site Admin',
'type'=>0,
'month_article'=>0,
'famous'=>1,
'views'=>1196,
'show'=>0,
'date'=>'2014-06-06 13:44:09',
'active'=>'1',
'user'=>0,
'created_at'=>'2020-05-15 01:20:06',
'updated_at'=>'2020-06-21 00:08:23',
'deleted_at'=>NULL
] );



Post::create( [
'id'=>4366,
'cat_id'=>15,
'tag_id'=>2,
'title'=>'Art of Sedation in ICU',
'text'=>'',
'img'=>'file-1464106363-HWQX6B.jpg',
'file'=>'file-1471266812-RBR41P.pdf',
'link'=>NULL,
'by'=>0,
'journal'=>'Dr. Khaled Sewify',
'type'=>1,
'month_article'=>0,
'famous'=>0,
'views'=>1135,
'show'=>0,
'date'=>'2016-08-15 13:13:32',
'active'=>'1',
'user'=>0,
'created_at'=>'2020-05-15 01:20:06',
'updated_at'=>'2020-06-21 00:08:23',
'deleted_at'=>NULL
] );



Post::create( [
'id'=>4367,
'cat_id'=>12,
'tag_id'=>1,
'title'=>'Update on Zika Virus in the United States',
'text'=>'JAMA. Published online August 08, 2016',
'img'=>'file-1464165509-QNPJY8.jpg',
'file'=>NULL,
'link'=>'http://jama.jamanetwork.com/article.aspx?articleid=2543301',
'by'=>0,
'journal'=>'Thomas R. Frieden Anne Schuchat Lyle R. Petersen',
'type'=>0,
'month_article'=>0,
'famous'=>0,
'views'=>1130,
'show'=>0,
'date'=>'2016-08-15 13:24:00',
'active'=>'1',
'user'=>0,
'created_at'=>'2020-05-15 01:20:06',
'updated_at'=>'2020-06-21 00:08:23',
'deleted_at'=>NULL
] );



Post::create( [
'id'=>4368,
'cat_id'=>8,
'tag_id'=>1,
'title'=>'U.S. declares a Zika public health emergency in Puerto Rico',
'text'=>'Aug 12 2016',
'img'=>'file-1463591639-4YG8JY.jpg',
'file'=>NULL,
'link'=>'http://www.reuters.com/article/us-health-zika-usa-idUSKCN10N2KA',
'by'=>0,
'journal'=>'Reuters Health',
'type'=>0,
'month_article'=>0,
'famous'=>0,
'views'=>1130,
'show'=>0,
'date'=>'2016-08-15 13:30:35',
'active'=>'1',
'user'=>0,
'created_at'=>'2020-05-15 01:20:06',
'updated_at'=>'2020-06-21 00:08:23',
'deleted_at'=>NULL
] );



Post::create( [
'id'=>4369,
'cat_id'=>12,
'tag_id'=>1,
'title'=>'Update on Zika Virus in the United States',
'text'=>'',
'img'=>'file-1464165509-QNPJY8.jpg',
'file'=>NULL,
'link'=>'http://jama.jamanetwork.com/article.aspx?articleid=2543301',
'by'=>0,
'journal'=>'Thomas R. Frieden Anne Schuchat Lyle R. Petersen',
'type'=>0,
'month_article'=>0,
'famous'=>0,
'views'=>1241,
'show'=>0,
'date'=>'2016-08-15 13:32:58',
'active'=>'1',
'user'=>0,
'created_at'=>'2020-05-15 01:20:06',
'updated_at'=>'2020-06-21 00:08:23',
'deleted_at'=>NULL
] );



Post::create( [
'id'=>4370,
'cat_id'=>8,
'tag_id'=>7,
'title'=>'Effect of a Cerebral Protection Device on Brain Lesions Following Transcatheter Aortic Valve Implantation in Patients With Severe Aortic Stenosis: The CLEAN-TAVI Randomized Clinical Tria',
'text'=>'JAMA. 20163166:592-601',
'img'=>'file-1463591644-ZGN2DA.jpg',
'file'=>NULL,
'link'=>'http://jama.jamanetwork.com/article.aspx?articleid=2542633',
'by'=>0,
'journal'=>'Stephan Haussig and others',
'type'=>0,
'month_article'=>0,
'famous'=>1,
'views'=>1196,
'show'=>0,
'date'=>'2016-08-15 13:39:03',
'active'=>'1',
'user'=>0,
'created_at'=>'2020-05-15 01:20:06',
'updated_at'=>'2020-06-21 00:08:23',
'deleted_at'=>NULL
] );



Post::create( [
'id'=>4372,
'cat_id'=>16,
'tag_id'=>11,
'title'=>'Hospital Acquired and Ventilator Associated Pneumonia',
'text'=>'Dr. Wael Gomaa',
'img'=>'file-1463591752-XK911T.jpg',
'file'=>'file-1471765186-KZSHTG.pptx',
'link'=>NULL,
'by'=>0,
'journal'=>'Review of 2016 IDSA Guidelines',
'type'=>1,
'month_article'=>0,
'famous'=>0,
'views'=>1064,
'show'=>0,
'date'=>'2016-08-21 07:39:46',
'active'=>'1',
'user'=>0,
'created_at'=>'2020-05-15 01:20:06',
'updated_at'=>'2020-06-21 00:08:23',
'deleted_at'=>NULL
] );



Post::create( [
'id'=>4373,
'cat_id'=>12,
'tag_id'=>3,
'title'=>'Probiotic and synbiotic therapy in critical illness: a systematic review and meta-analysis',
'text'=>'Critical Care. 19 August 2016',
'img'=>'file-1463591732-SM39NY.jpg',
'file'=>NULL,
'link'=>'https://ccforum.biomedcentral.com/articles/10.1186/s13054-016-1434-y',
'by'=>0,
'journal'=>'William Manzanares, Margot Lemieux, Pascal L. Langlois and Paul E. Wischmeyer',
'type'=>0,
'month_article'=>0,
'famous'=>0,
'views'=>1212,
'show'=>0,
'date'=>'2016-08-25 19:51:12',
'active'=>'1',
'user'=>0,
'created_at'=>'2020-05-15 01:20:06',
'updated_at'=>'2020-06-21 00:08:23',
'deleted_at'=>NULL
] );



Post::create( [
'id'=>4374,
'cat_id'=>12,
'tag_id'=>6,
'title'=>'Ultrasound-guided percutaneous dilational tracheostomy versus bronchoscopy-guided percutaneous dilational tracheostomy in critically ill patients TRACHUS: a randomized noninferiority controlled trial',
'text'=>'Intensive Care Med J. March 2016 42,3: 333 - 341',
'img'=>'file-1463591752-XK911T.jpg',
'file'=>NULL,
'link'=>'http://icmjournal.esicm.org/journals/abstract.html?v=42&j=134&i=3&a=4218_10.1007_s00134-016-4218-6&doi=',
'by'=>0,
'journal'=>'André Luiz Nunes Gobatto and others',
'type'=>0,
'month_article'=>0,
'famous'=>0,
'views'=>1212,
'show'=>0,
'date'=>'2016-08-26 19:12:58',
'active'=>'1',
'user'=>0,
'created_at'=>'2020-05-15 01:20:06',
'updated_at'=>'2020-06-21 00:08:23',
'deleted_at'=>NULL
] );



Post::create( [
'id'=>4375,
'cat_id'=>14,
'tag_id'=>11,
'title'=>'Drug Interactions Checker',
'text'=>'To view content sources and attributions, refer to our editorial policy.Copyright © 2000-2012 Drugs.com. All rights reserved',
'img'=>'covid-7.jpg',
'file'=>NULL,
'link'=>'https://www.drugs.com/drug_interactions.php',
'by'=>0,
'journal'=>'About Drugs.com Drugs.com provides accurate and independent information on more than 24,000 prescription drugs, over-the-counter medicines & natural products. This material is provided for educational purposes only and is not to be used for medical advice',
'type'=>0,
'month_article'=>0,
'famous'=>0,
'views'=>1101,
'show'=>0,
'date'=>'2016-08-26 20:17:16',
'active'=>'1',
'user'=>0,
'created_at'=>'2020-05-15 01:20:06',
'updated_at'=>'2020-07-23 15:31:50',
'deleted_at'=>NULL
] );



Post::create( [
'id'=>4377,
'cat_id'=>8,
'tag_id'=>11,
'title'=>'Effects of dexmedetomidine and esmolol on systemic hemodynamics and exogenous lactate clearance in early experimental septic shock',
'text'=>'Critical Care 2016, 20 :234 2 August 2016',
'img'=>'file-1464106363-HWQX6B.jpg',
'file'=>NULL,
'link'=>'http://ccforum.biomedcentral.com/articles/10.1186/s13054-016-1419-x',
'by'=>0,
'journal'=>'Hernández G and others',
'type'=>0,
'month_article'=>0,
'famous'=>0,
'views'=>1101,
'show'=>0,
'date'=>'2016-08-26 20:58:03',
'active'=>'1',
'user'=>0,
'created_at'=>'2020-05-15 01:20:06',
'updated_at'=>'2020-06-21 00:08:23',
'deleted_at'=>NULL
] );



Post::create( [
'id'=>4380,
'cat_id'=>8,
'tag_id'=>11,
'title'=>'Variation in diurnal sedation in mechanically ventilated patients who are managed with a sedation protocol alone or a sedation protocol and daily interruption',
'text'=>'Critical Care 2016, 20 :233 1 August 2016',
'img'=>'file-1463591752-XK911T.jpg',
'file'=>NULL,
'link'=>'http://ccforum.biomedcentral.com/articles/10.1186/s13054-016-1405-3',
'by'=>0,
'journal'=>'Mehta S and others',
'type'=>0,
'month_article'=>0,
'famous'=>0,
'views'=>1101,
'show'=>0,
'date'=>'2016-08-26 21:19:40',
'active'=>'1',
'user'=>0,
'created_at'=>'2020-05-15 01:20:06',
'updated_at'=>'2020-06-21 00:08:23',
'deleted_at'=>NULL
] );



Post::create( [
'id'=>4381,
'cat_id'=>8,
'tag_id'=>11,
'title'=>'Uncertainty in the Era of Precision Medicine',
'text'=>'N Engl J Med 2016 375:711-713. August 25, 201',
'img'=>'file-1464165480-R231XR.jpg',
'file'=>NULL,
'link'=>'http://www.nejm.org/doi/full/10.1056/NEJMp1608282?query=featured_home',
'by'=>0,
'journal'=>'David J. Hunter, M.B., B.S., Sc.D.',
'type'=>0,
'month_article'=>0,
'famous'=>0,
'views'=>1101,
'show'=>0,
'date'=>'2016-08-26 21:29:21',
'active'=>'1',
'user'=>0,
'created_at'=>'2020-05-15 01:20:06',
'updated_at'=>'2020-06-21 00:08:23',
'deleted_at'=>NULL
] );



Post::create( [
'id'=>4382,
'cat_id'=>15,
'tag_id'=>5,
'title'=>'ICP Monitoring from Bench to Bedside',
'text'=>'',
'img'=>'file-1464106363-HWQX6B.jpg',
'file'=>'file-1472248837-VMRPSP.pdf',
'link'=>NULL,
'by'=>0,
'journal'=>'Dr. Hamza Al Sheikh',
'type'=>1,
'month_article'=>0,
'famous'=>0,
'views'=>1077,
'show'=>0,
'date'=>'2016-08-26 22:00:37',
'active'=>'1',
'user'=>0,
'created_at'=>'2020-05-15 01:20:06',
'updated_at'=>'2020-06-21 00:08:23',
'deleted_at'=>NULL
] );



Post::create( [
'id'=>4383,
'cat_id'=>15,
'tag_id'=>11,
'title'=>'Nutrition and Metabolism during Critical Illness',
'text'=>'',
'img'=>'file-1463591725-9J88SF.jpg',
'file'=>NULL,
'link'=>'https://www.ncbi.nlm.nih.gov/pmc/articles/PMC5070594/',
'by'=>0,
'journal'=>'Dr. Khaled Sewify',
'type'=>0,
'month_article'=>0,
'famous'=>0,
'views'=>1072,
'show'=>0,
'date'=>'2016-08-26 22:10:43',
'active'=>'1',
'user'=>0,
'created_at'=>'2020-05-15 01:20:06',
'updated_at'=>'2020-06-21 00:08:23',
'deleted_at'=>NULL
] );



Post::create( [
'id'=>4384,
'cat_id'=>15,
'tag_id'=>6,
'title'=>'X ray Interpretation in Critically ill Patients',
'text'=>'',
'img'=>'file-1464165509-QNPJY8.jpg',
'file'=>'file-1472249728-WE7YZ4.pdf',
'link'=>NULL,
'by'=>0,
'journal'=>'Dr. Khaled Sewify',
'type'=>1,
'month_article'=>0,
'famous'=>0,
'views'=>1064,
'show'=>0,
'date'=>'2016-08-26 22:15:28',
'active'=>'1',
'user'=>0,
'created_at'=>'2020-05-15 01:20:06',
'updated_at'=>'2020-06-21 00:08:23',
'deleted_at'=>NULL
] );



Post::create( [
'id'=>4385,
'cat_id'=>16,
'tag_id'=>5,
'title'=>'Recent Advances in Stoke Management',
'text'=>'',
'img'=>'file-1464165463-D3CBM9.jpg',
'file'=>'file-1472249928-P7AUBZ.pdf',
'link'=>NULL,
'by'=>0,
'journal'=>'Dr. Khaled Sewify',
'type'=>1,
'month_article'=>0,
'famous'=>0,
'views'=>1077,
'show'=>0,
'date'=>'2016-08-26 22:18:48',
'active'=>'1',
'user'=>0,
'created_at'=>'2020-05-15 01:20:06',
'updated_at'=>'2020-06-21 00:08:23',
'deleted_at'=>NULL
] );



Post::create( [
'id'=>4386,
'cat_id'=>16,
'tag_id'=>6,
'title'=>'Inhalational Poisoning in ICU',
'text'=>'',
'img'=>'file-1464106363-HWQX6B.jpg',
'file'=>'file-1472250087-RXGNEW.pdf',
'link'=>NULL,
'by'=>0,
'journal'=>'Dr. Khaled Sewify',
'type'=>1,
'month_article'=>0,
'famous'=>0,
'views'=>1065,
'show'=>0,
'date'=>'2016-08-26 22:21:27',
'active'=>'1',
'user'=>0,
'created_at'=>'2020-05-15 01:20:06',
'updated_at'=>'2020-06-24 09:29:51',
'deleted_at'=>NULL
] );



Post::create( [
'id'=>4387,
'cat_id'=>16,
'tag_id'=>11,
'title'=>'Rule of Intensivists in Disaster',
'text'=>'',
'img'=>'file-1464165480-R231XR.jpg',
'file'=>'file-1472250195-RC6SQE.pdf',
'link'=>NULL,
'by'=>0,
'journal'=>'Site Admin',
'type'=>1,
'month_article'=>0,
'famous'=>0,
'views'=>1064,
'show'=>0,
'date'=>'2016-08-26 22:23:15',
'active'=>'1',
'user'=>0,
'created_at'=>'2020-05-15 01:20:06',
'updated_at'=>'2020-06-21 00:08:23',
'deleted_at'=>NULL
] );



Post::create( [
'id'=>4388,
'cat_id'=>16,
'tag_id'=>5,
'title'=>'Recent Advances in Hemorrhagic Stoke',
'text'=>'',
'img'=>'file-1463591681-AGRM7H.jpg',
'file'=>'file-1472250301-GE6AFD.pdf',
'link'=>NULL,
'by'=>0,
'journal'=>'Dr. Adham Elsayed',
'type'=>1,
'month_article'=>0,
'famous'=>0,
'views'=>1077,
'show'=>0,
'date'=>'2016-08-26 22:25:01',
'active'=>'1',
'user'=>0,
'created_at'=>'2020-05-15 01:20:06',
'updated_at'=>'2020-06-21 00:08:23',
'deleted_at'=>NULL
] );



Post::create( [
'id'=>4390,
'cat_id'=>16,
'tag_id'=>7,
'title'=>'Summary of CPR Updates in 2015',
'text'=>'',
'img'=>'file-1463591639-4YG8JY.jpg',
'file'=>'file-1472250519-IM8CZR.pdf',
'link'=>NULL,
'by'=>0,
'journal'=>'Dr. Khaled Sewify',
'type'=>1,
'month_article'=>0,
'famous'=>0,
'views'=>1064,
'show'=>0,
'date'=>'2016-08-26 22:28:39',
'active'=>'1',
'user'=>0,
'created_at'=>'2020-05-15 01:20:06',
'updated_at'=>'2020-06-21 00:08:23',
'deleted_at'=>NULL
] );



Post::create( [
'id'=>4391,
'cat_id'=>16,
'tag_id'=>8,
'title'=>'Contrast Induced Nephropathy, Latest Updates',
'text'=>'',
'img'=>'file-1472583069-XNJH8C.png',
'file'=>'file-1472250639-GACD93.pdf',
'link'=>NULL,
'by'=>0,
'journal'=>'Dr. Khaled Sewify',
'type'=>1,
'month_article'=>0,
'famous'=>0,
'views'=>1115,
'show'=>0,
'date'=>'2016-08-26 21:00:00',
'active'=>'1',
'user'=>0,
'created_at'=>'2020-05-15 01:20:06',
'updated_at'=>'2020-06-21 00:08:23',
'deleted_at'=>NULL
] );



Post::create( [
'id'=>4392,
'cat_id'=>8,
'tag_id'=>2,
'title'=>'Clinicians say many ICU patients get poor sleep',
'text'=>'',
'img'=>'file-1464165509-QNPJY8.jpg',
'file'=>NULL,
'link'=>'http://www.physiciansbriefing.com/Article.asp?AID=714186',
'by'=>0,
'journal'=>'FRIDAY, Aug. 26, 2016 HealthDay News',
'type'=>0,
'month_article'=>0,
'famous'=>0,
'views'=>1172,
'show'=>0,
'date'=>'2016-08-25 21:00:00',
'active'=>'1',
'user'=>0,
'created_at'=>'2020-05-15 01:20:06',
'updated_at'=>'2020-06-21 00:08:23',
'deleted_at'=>NULL
] );



Post::create( [
'id'=>4394,
'cat_id'=>8,
'tag_id'=>5,
'title'=>'Cerebral Oximetry During Cardiac Arrest: A Multicenter Study of Neurologic Outcomes and Survival',
'text'=>'Critical Care Medicine:\r\nSeptember 2016 - Volume 44 - Issue 9 - p 1663–1674',
'img'=>'file-1464106363-HWQX6B.jpg',
'file'=>NULL,
'link'=>'http://journals.lww.com/ccmjournal/Abstract/2016/09000/Cerebral_Oximetry_During_Cardiac_Arrest___A.4.aspx',
'by'=>0,
'journal'=>'Parnia, Sam and others',
'type'=>0,
'month_article'=>0,
'famous'=>0,
'views'=>1114,
'show'=>0,
'date'=>'2016-08-31 21:00:00',
'active'=>'1',
'user'=>0,
'created_at'=>'2020-05-15 01:20:06',
'updated_at'=>'2020-06-21 00:08:23',
'deleted_at'=>NULL
] );



Post::create( [
'id'=>4395,
'cat_id'=>8,
'tag_id'=>11,
'title'=>'Liberal Glycemic Control in Critically Ill Patients With Type 2 Diabetes: An Exploratory Study',
'text'=>'Critical Care Medicine:\r\nSeptember 2016 - Volume 44 - Issue 9 - p 1695–1703',
'img'=>'file-1464165463-D3CBM9.jpg',
'file'=>NULL,
'link'=>'http://journals.lww.com/ccmjournal/Abstract/2016/09000/Liberal_Glycemic_Control_in_Critically_Ill.8.aspx',
'by'=>0,
'journal'=>'Kar, Palash and others',
'type'=>0,
'month_article'=>0,
'famous'=>0,
'views'=>1101,
'show'=>0,
'date'=>'2016-08-31 21:00:00',
'active'=>'1',
'user'=>0,
'created_at'=>'2020-05-15 01:20:06',
'updated_at'=>'2020-06-21 00:08:23',
'deleted_at'=>NULL
] );



Post::create( [
'id'=>4396,
'cat_id'=>8,
'tag_id'=>11,
'title'=>'Incidence and Etiology of Potentially Preventable ICU Readmissions',
'text'=>'Critical Care Medicine:\r\nSeptember 2016 - Volume 44 - Issue 9 - p 1704–1709',
'img'=>'file-1470233162-2MW1RX.jpg',
'file'=>NULL,
'link'=>'http://journals.lww.com/ccmjournal/Abstract/2016/09000/Incidence_and_Etiology_of_Potentially_Preventable.9.aspx',
'by'=>0,
'journal'=>'Al-Jaghbeer, Mohammed J. and others',
'type'=>0,
'month_article'=>0,
'famous'=>0,
'views'=>1101,
'show'=>0,
'date'=>'2016-08-31 21:00:00',
'active'=>'1',
'user'=>0,
'created_at'=>'2020-05-15 01:20:06',
'updated_at'=>'2020-06-21 00:08:23',
'deleted_at'=>NULL
] );



Post::create( [
'id'=>4397,
'cat_id'=>8,
'tag_id'=>5,
'title'=>'Depressive Symptoms After Critical Illness: A Systematic Review and Meta-Analysis',
'text'=>'Critical Care Medicine:\r\nSeptember 2016 - Volume 44 - Issue 9 - p 1744–1753',
'img'=>'file-1464165480-R231XR.jpg',
'file'=>NULL,
'link'=>'http://journals.lww.com/ccmjournal/Abstract/2016/09000/Depressive_Symptoms_After_Critical_Illness___A.14.aspx',
'by'=>0,
'journal'=>'Rabiee, Anahita and others',
'type'=>0,
'month_article'=>0,
'famous'=>0,
'views'=>1114,
'show'=>0,
'date'=>'2016-08-31 21:00:00',
'active'=>'1',
'user'=>0,
'created_at'=>'2020-05-15 01:20:06',
'updated_at'=>'2020-06-21 00:08:23',
'deleted_at'=>NULL
] );



Post::create( [
'id'=>4398,
'cat_id'=>8,
'tag_id'=>5,
'title'=>'Predicting Intracranial Pressure and Brain Tissue Oxygen Crises in Patients With Severe Traumatic Brain Injury',
'text'=>'Critical Care Medicine:\r\nSeptember 2016 - Volume 44 - Issue 9 - p 1754–1761',
'img'=>'file-1464106363-HWQX6B.jpg',
'file'=>NULL,
'link'=>'Myers, Risa B and others',
'by'=>0,
'journal'=>'http://journals.lww.com/ccmjournal/Abstract/2016/09000/Predicting_Intracranial_Pressure_and_Brain_Tissue.15.aspx',
'type'=>0,
'month_article'=>0,
'famous'=>0,
'views'=>1114,
'show'=>0,
'date'=>'2016-08-31 21:00:00',
'active'=>'1',
'user'=>0,
'created_at'=>'2020-05-15 01:20:06',
'updated_at'=>'2020-06-21 00:08:23',
'deleted_at'=>NULL
] );



Post::create( [
'id'=>4399,
'cat_id'=>8,
'tag_id'=>5,
'title'=>'Defining Futile and Potentially Inappropriate Interventions: A Policy Statement From the Society of Critical Care Medicine Ethics Committee',
'text'=>'Critical Care Medicine:\r\nSeptember 2016 - Volume 44 - Issue 9 - p 1769–1774',
'img'=>'file-1464165463-D3CBM9.jpg',
'file'=>NULL,
'link'=>'http://journals.lww.com/ccmjournal/Abstract/2016/09000/Defining_Futile_and_Potentially_Inappropriate.17.aspx',
'by'=>0,
'journal'=>'Kon, Alexander A and others',
'type'=>0,
'month_article'=>0,
'famous'=>0,
'views'=>1114,
'show'=>0,
'date'=>'2016-08-31 21:00:00',
'active'=>'1',
'user'=>0,
'created_at'=>'2020-05-15 01:20:06',
'updated_at'=>'2020-06-21 00:08:23',
'deleted_at'=>NULL
] );



Post::create( [
'id'=>4400,
'cat_id'=>8,
'tag_id'=>11,
'title'=>'Precision Medicine for Critical Illness and Injury',
'text'=>'Critical Care Medicine: September 2016 - Volume 44 - Issue 9 - p 1635–1638',
'img'=>'file-1464165463-D3CBM9.jpg',
'file'=>NULL,
'link'=>'http://journals.lww.com/ccmjournal/Citation/2016/09000/Precision_Medicine_for_Critical_Illness_and_Injury.1.aspx',
'by'=>0,
'journal'=>'Buchman, Timothy G and others',
'type'=>0,
'month_article'=>0,
'famous'=>0,
'views'=>1101,
'show'=>0,
'date'=>'2016-08-31 21:00:00',
'active'=>'1',
'user'=>0,
'created_at'=>'2020-05-15 01:20:06',
'updated_at'=>'2020-06-21 00:08:23',
'deleted_at'=>NULL
] );



Post::create( [
'id'=>4401,
'cat_id'=>8,
'tag_id'=>1,
'title'=>'Comparison of alcoholic chlorhexidine and povidone–iodine cutaneous antiseptics for the prevention of central venous catheter-related infection: a cohort and quasi-experimental multicenter study',
'text'=>'Intensive Care Med. 429 1418 - 1426: September 2016',
'img'=>'file-1463591681-AGRM7H.jpg',
'file'=>NULL,
'link'=>'http://icmjournal.esicm.org/journals/abstract.html?v=42&j=134&i=9&a=4406_10.1007_s00134-016-4406-4&doi=',
'by'=>0,
'journal'=>'Justine Pages and others',
'type'=>0,
'month_article'=>0,
'famous'=>0,
'views'=>1130,
'show'=>0,
'date'=>'2016-08-31 21:00:00',
'active'=>'1',
'user'=>0,
'created_at'=>'2020-05-15 01:20:06',
'updated_at'=>'2020-06-21 00:08:23',
'deleted_at'=>NULL
] );



Post::create( [
'id'=>4402,
'cat_id'=>8,
'tag_id'=>11,
'title'=>'Effect of atorvastatin on the incidence of acute kidney injury following valvular heart surgery: a randomized, placebo-controlled trial',
'text'=>'Intensive Care Med. 429 1398 - 1407: September 2016',
'img'=>'file-1463591639-4YG8JY.jpg',
'file'=>NULL,
'link'=>'http://icmjournal.esicm.org/journals/abstract.html?v=42&j=134&i=9&a=4358_10.1007_s00134-016-4358-8&doi=',
'by'=>0,
'journal'=>'Jin Ha Park and others',
'type'=>0,
'month_article'=>0,
'famous'=>0,
'views'=>1101,
'show'=>0,
'date'=>'2016-08-31 21:00:00',
'active'=>'1',
'user'=>0,
'created_at'=>'2020-05-15 01:20:06',
'updated_at'=>'2020-06-21 00:08:23',
'deleted_at'=>NULL
] );



Post::create( [
'id'=>4403,
'cat_id'=>8,
'tag_id'=>11,
'title'=>'Continuous renal replacement therapy versus intermittent hemodialysis in intensive care patients: impact on mortality and renal recovery',
'text'=>'Intensive Care Med. 429 1398 - 1407: September 2016',
'img'=>'file-1463591738-IZ3XFW.jpg',
'file'=>NULL,
'link'=>'http://icmjournal.esicm.org/journals/abstract.html?v=42&j=134&i=9&a=4404_10.1007_s00134-016-4404-6&doi=',
'by'=>0,
'journal'=>'Anne-Sophie Truche and others',
'type'=>0,
'month_article'=>0,
'famous'=>0,
'views'=>1101,
'show'=>0,
'date'=>'2016-08-31 21:00:00',
'active'=>'1',
'user'=>0,
'created_at'=>'2020-05-15 01:20:06',
'updated_at'=>'2020-06-21 00:08:23',
'deleted_at'=>NULL
] );



Post::create( [
'id'=>4404,
'cat_id'=>8,
'tag_id'=>11,
'title'=>'The feasibility and safety of extracorporeal carbon dioxide removal to avoid intubation in patients with COPD unresponsive to noninvasive ventilation for acute hypercapnic respiratory failure ECLAIR study: multicentre case–control study',
'text'=>'Intensive Care Med. 429 1437 - 1444: September 2016',
'img'=>'file-1463591752-XK911T.jpg',
'file'=>NULL,
'link'=>'http://icmjournal.esicm.org/journals/abstract.html?v=42&j=134&i=9&a=4452_10.1007_s00134-016-4452-y&doi=',
'by'=>0,
'journal'=>'Stephan Braune and others',
'type'=>0,
'month_article'=>0,
'famous'=>0,
'views'=>1101,
'show'=>0,
'date'=>'2016-08-31 21:00:00',
'active'=>'1',
'user'=>0,
'created_at'=>'2020-05-15 01:20:06',
'updated_at'=>'2020-06-21 00:08:23',
'deleted_at'=>NULL
] );



Post::create( [
'id'=>4405,
'cat_id'=>8,
'tag_id'=>11,
'title'=>'Fat-free mass at admission predicts 28-day mortality in intensive care unit patients: the international prospective observational study Phase Angle Project',
'text'=>'Intensive Care Med. 4291445 - 1453:September 2016',
'img'=>'file-1464165463-D3CBM9.jpg',
'file'=>NULL,
'link'=>'http://icmjournal.esicm.org/journals/abstract.htmlv42&j134&i9&a4468_10.1007_s00134-016-4468-3&doi',
'by'=>0,
'journal'=>'Ronan Thibault and others',
'type'=>0,
'month_article'=>0,
'famous'=>0,
'views'=>1101,
'show'=>0,
'date'=>'2016-08-31 21:00:00',
'active'=>'1',
'user'=>0,
'created_at'=>'2020-05-15 01:20:06',
'updated_at'=>'2020-06-21 00:08:23',
'deleted_at'=>NULL
] );



Post::create( [
'id'=>4406,
'cat_id'=>8,
'tag_id'=>6,
'title'=>'Use of high-flow nasal cannula oxygenation in ICU adults: a narrative review',
'text'=>'Intensive Care Med. 4291336 - 1349:September 2016',
'img'=>'file-1463591752-XK911T.jpg',
'file'=>NULL,
'link'=>'http://icmjournal.esicm.org/journals/abstract.html?v=42&j=134&i=9&a=4277_10.1007_s00134-016-4277-8&doi=',
'by'=>0,
'journal'=>'Laurent Papazian and others',
'type'=>0,
'month_article'=>0,
'famous'=>0,
'views'=>1101,
'show'=>0,
'date'=>'2016-08-31 21:00:00',
'active'=>'1',
'user'=>0,
'created_at'=>'2020-05-15 01:20:06',
'updated_at'=>'2020-06-21 00:08:23',
'deleted_at'=>NULL
] );



Post::create( [
'id'=>4407,
'cat_id'=>8,
'tag_id'=>6,
'title'=>'Esophageal and transpulmonary pressure in the clinical setting: meaning, usefulness and perspectives',
'text'=>'Intensive Care Med. 4291336 - 1349:September 2016',
'img'=>'file-1463591657-K8BRDF.jpg',
'file'=>NULL,
'link'=>'http://icmjournal.esicm.org/journals/abstract.html?v=42&j=134&i=9&a=4400_10.1007_s00134-016-4400-x&doi=',
'by'=>0,
'journal'=>'Tommaso Mauri, and others',
'type'=>0,
'month_article'=>0,
'famous'=>0,
'views'=>1101,
'show'=>0,
'date'=>'2016-08-31 21:00:00',
'active'=>'1',
'user'=>0,
'created_at'=>'2020-05-15 01:20:06',
'updated_at'=>'2020-06-21 00:08:23',
'deleted_at'=>NULL
] );



Post::create( [
'id'=>4408,
'cat_id'=>8,
'tag_id'=>1,
'title'=>'Catecholamines for inflammatory shock: a Jekyll-and-Hyde conundrum',
'text'=>'Intensive Care Med. 4291387 - 1397:September 2016',
'img'=>'file-1463591639-4YG8JY.jpg',
'file'=>NULL,
'link'=>'http://icmjournal.esicm.org/journals/abstract.html?v=42&j=134&i=9&a=4249_10.1007_s00134-016-4249-z&doi=',
'by'=>0,
'journal'=>'Davide Tommaso Andreis, Mervyn Singer',
'type'=>0,
'month_article'=>0,
'famous'=>0,
'views'=>1130,
'show'=>0,
'date'=>'2016-08-31 21:00:00',
'active'=>'1',
'user'=>0,
'created_at'=>'2020-05-15 01:20:06',
'updated_at'=>'2020-06-21 00:08:23',
'deleted_at'=>NULL
] );



Post::create( [
'id'=>4409,
'cat_id'=>8,
'tag_id'=>11,
'title'=>'Milrinone for cardiac dysfunction in critically ill adult patients: a systematic review of randomised clinical trials with meta-analysis and trial sequential analysis',
'text'=>'Intensive Care Med. 4291387 - 1397:September 2016',
'img'=>'file-1463591644-ZGN2DA.jpg',
'file'=>NULL,
'link'=>'http://icmjournal.esicm.org/journals/abstract.html?v=42&j=134&i=9&a=4449_10.1007_s00134-016-4449-6&doi=',
'by'=>0,
'journal'=>'Geert Koster and others',
'type'=>0,
'month_article'=>0,
'famous'=>0,
'views'=>1101,
'show'=>0,
'date'=>'2016-08-31 21:00:00',
'active'=>'1',
'user'=>0,
'created_at'=>'2020-05-15 01:20:06',
'updated_at'=>'2020-06-21 00:08:23',
'deleted_at'=>NULL
] );



Post::create( [
'id'=>4411,
'cat_id'=>8,
'tag_id'=>1,
'title'=>'Is first-line antimicrobial therapy still adequate to treat MRSA in the ICU A report from a highly endemic country',
'text'=>'Critical Care 2016, 20 :246',
'img'=>'file-1464106363-HWQX6B.jpg',
'file'=>NULL,
'link'=>'http://ccforum.biomedcentral.com/articles/10.1186/s13054-016-1430-2',
'by'=>0,
'journal'=>'Bassetti M and others',
'type'=>0,
'month_article'=>0,
'famous'=>0,
'views'=>1130,
'show'=>0,
'date'=>'2016-08-31 21:00:00',
'active'=>'1',
'user'=>0,
'created_at'=>'2020-05-15 01:20:06',
'updated_at'=>'2020-06-21 00:08:23',
'deleted_at'=>NULL
] );



Post::create( [
'id'=>4412,
'cat_id'=>8,
'tag_id'=>6,
'title'=>'A Multidisciplinary Pulmonary Embolism Response Team:',
'text'=>'Chest. 20161502:384-393.',
'img'=>'file-1463591644-ZGN2DA.jpg',
'file'=>NULL,
'link'=>'http://journal.publications.chestnet.org/article.aspx?articleID=2506757',
'by'=>0,
'journal'=>'Christopher Kabrhel',
'type'=>0,
'month_article'=>0,
'famous'=>0,
'views'=>1101,
'show'=>0,
'date'=>'2016-09-02 21:00:00',
'active'=>'1',
'user'=>0,
'created_at'=>'2020-05-15 01:20:06',
'updated_at'=>'2020-06-21 00:08:23',
'deleted_at'=>NULL
] );



Post::create( [
'id'=>4414,
'cat_id'=>12,
'tag_id'=>11,
'title'=>'The effect of camicinal GSK962040, a motilin agonist, on gastric emptying and glucose absorption in feed-intolerant critically ill patients: a randomized, blinded, placebo-controlled, clinical trial',
'text'=>'Critical Care. August 2016 20:232',
'img'=>'file-1473348294-WU7GEK.jpg',
'file'=>NULL,
'link'=>'http://ccforum.biomedcentral.com/articles/10.1186/s13054-016-1420-4',
'by'=>0,
'journal'=>'Marianne J. Chapman and others',
'type'=>0,
'month_article'=>0,
'famous'=>0,
'views'=>1212,
'show'=>0,
'date'=>'2016-08-31 21:00:00',
'active'=>'1',
'user'=>0,
'created_at'=>'2020-05-15 01:20:06',
'updated_at'=>'2020-06-21 00:08:23',
'deleted_at'=>NULL
] );



Post::create( [
'id'=>4417,
'cat_id'=>12,
'tag_id'=>2,
'title'=>'Dexmedetomidine sedation reduces atrial fibrillation after cardiac surgery compared to propofol: a randomized controlled trial',
'text'=>'Critical Care 2016, 20 :298 21 September 2016',
'img'=>'im2.jpg',
'file'=>NULL,
'link'=>'http://ccforum.biomedcentral.com/articles/10.1186/s13054-016-1480-5',
'by'=>0,
'journal'=>'Liu X, Zhang K, Wang W, Xie G, Fang X',
'type'=>0,
'month_article'=>0,
'famous'=>0,
'views'=>1172,
'show'=>0,
'date'=>'2016-09-20 21:00:00',
'active'=>'1',
'user'=>0,
'created_at'=>'2020-05-15 01:20:06',
'updated_at'=>'2020-06-21 00:08:23',
'deleted_at'=>NULL
] );



Post::create( [
'id'=>4418,
'cat_id'=>8,
'tag_id'=>7,
'title'=>'Increased mortality with the use of adrenaline in shock: the evidence is still limited',
'text'=>'Critical Care 2016, 20 :289 20 September 2016',
'img'=>'file-1463591639-4YG8JY.jpg',
'file'=>NULL,
'link'=>'http://ccforum.biomedcentral.com/articles/10.1186/s13054-016-1465-4',
'by'=>0,
'journal'=>'Ribeiro R, Restelatto L',
'type'=>0,
'month_article'=>0,
'famous'=>0,
'views'=>1101,
'show'=>0,
'date'=>'2016-09-19 21:00:00',
'active'=>'1',
'user'=>0,
'created_at'=>'2020-05-15 01:20:06',
'updated_at'=>'2020-06-21 00:08:23',
'deleted_at'=>NULL
] );



Post::create( [
'id'=>4419,
'cat_id'=>12,
'tag_id'=>5,
'title'=>'The 4th Edition of the Guidelines for Management of Severe Traumatic Brain Injury 2016',
'text'=>'Brain Trauma Foundation',
'img'=>'im2.jpg',
'file'=>NULL,
'link'=>'https://braintrauma.org/guidelines/guidelines-for-the-management-of-severe-tbi-4th-ed#/',
'by'=>0,
'journal'=>'Nancy Carney and others',
'type'=>0,
'month_article'=>0,
'famous'=>0,
'views'=>1225,
'show'=>0,
'date'=>'2016-09-21 21:00:00',
'active'=>'1',
'user'=>0,
'created_at'=>'2020-05-15 01:20:06',
'updated_at'=>'2020-06-21 00:08:23',
'deleted_at'=>NULL
] );



Post::create( [
'id'=>4420,
'cat_id'=>9,
'tag_id'=>10,
'title'=>'2015 WHO Guidelines for Treatement of Malraia, 3rd Edition',
'text'=>'',
'img'=>'file-1463591681-AGRM7H.jpg',
'file'=>NULL,
'link'=>'http://apps.who.int/iris/bitstream/10665/162441/1/9789241549127_eng.pdf',
'by'=>0,
'journal'=>'World Health Organization',
'type'=>0,
'month_article'=>0,
'famous'=>0,
'views'=>1101,
'show'=>0,
'date'=>'2015-09-30 21:00:00',
'active'=>'1',
'user'=>0,
'created_at'=>'2020-05-15 01:20:06',
'updated_at'=>'2020-06-21 00:08:23',
'deleted_at'=>NULL
] );



Post::create( [
'id'=>4421,
'cat_id'=>9,
'tag_id'=>7,
'title'=>'2016 ESC Guidelines for the diagnosis and treatment of acute and chronic heart failure',
'text'=>'Euro heart J. First published online: 20 May 2016',
'img'=>'file-1463591644-ZGN2DA.jpg',
'file'=>NULL,
'link'=>'http://eurheartj.oxfordjournals.org/content/37/27/2129',
'by'=>0,
'journal'=>'Thomas F. Lüscher and others',
'type'=>0,
'month_article'=>0,
'famous'=>0,
'views'=>1101,
'show'=>0,
'date'=>'2016-05-19 21:00:00',
'active'=>'1',
'user'=>0,
'created_at'=>'2020-05-15 01:20:06',
'updated_at'=>'2020-06-21 00:08:23',
'deleted_at'=>NULL
] );



Post::create( [
'id'=>4422,
'cat_id'=>9,
'tag_id'=>7,
'title'=>'CANCER TREATMENTS & CARDIOVASCULAR TOXICITY 2016 POSITION PAPER',
'text'=>'European Heart Journal. Published in: 2016',
'img'=>'file-1463591639-4YG8JY.jpg',
'file'=>NULL,
'link'=>'http://eurheartj.oxfordjournals.org/content/early/2016/08/24/eurheartj.ehw211',
'by'=>0,
'journal'=>'Jose Luis Zamorano and others',
'type'=>0,
'month_article'=>0,
'famous'=>0,
'views'=>1101,
'show'=>0,
'date'=>'2016-08-25 21:00:00',
'active'=>'1',
'user'=>0,
'created_at'=>'2020-05-15 01:20:06',
'updated_at'=>'2020-06-21 00:08:23',
'deleted_at'=>NULL
] );



Post::create( [
'id'=>4424,
'cat_id'=>12,
'tag_id'=>7,
'title'=>'2016 ESC Guidelines for the management of atrial fibrillation developed in collaboration with EACTS',
'text'=>'European Heart Journal. Published 27 August 2016',
'img'=>'file-1463591644-ZGN2DA.jpg',
'file'=>NULL,
'link'=>'http://eurheartj.oxfordjournals.org/content/early/2016/08/26/eurheartj.ehw210',
'by'=>0,
'journal'=>'Paulus Kirchhof and others',
'type'=>0,
'month_article'=>0,
'famous'=>0,
'views'=>1212,
'show'=>0,
'date'=>'2016-08-26 21:00:00',
'active'=>'1',
'user'=>0,
'created_at'=>'2020-05-15 01:20:06',
'updated_at'=>'2020-06-21 00:08:23',
'deleted_at'=>NULL
] );



Post::create( [
'id'=>4425,
'cat_id'=>8,
'tag_id'=>11,
'title'=>'ICU Physicians Use Variety of Techniques to Cope With Fatigue',
'text'=>'',
'img'=>'file-1464165463-D3CBM9.jpg',
'file'=>NULL,
'link'=>'http://www.physiciansbriefing.com/Article.asp?AID=71',
'by'=>0,
'journal'=>'MONDAY, Sept. 26, 2016 HealthDay News',
'type'=>0,
'month_article'=>0,
'famous'=>0,
'views'=>1101,
'show'=>0,
'date'=>'2016-09-26 21:00:00',
'active'=>'1',
'user'=>0,
'created_at'=>'2020-05-15 01:20:06',
'updated_at'=>'2020-06-21 00:08:23',
'deleted_at'=>NULL
] );



Post::create( [
'id'=>4426,
'cat_id'=>9,
'tag_id'=>5,
'title'=>'2016 International consensus guidance for management of Myasthenia Gravis',
'text'=>'2016 American Academy of Neurology.Neurology® 201687:1–7',
'img'=>'im1.jpg',
'file'=>NULL,
'link'=>'http://www.neurology.org/content/early/2016/06/29/WNL.0000000000002790.full.pdf+html',
'by'=>0,
'journal'=>'Donald B. Sanders and others',
'type'=>0,
'month_article'=>0,
'famous'=>0,
'views'=>1114,
'show'=>0,
'date'=>'2015-07-04 21:00:00',
'active'=>'1',
'user'=>0,
'created_at'=>'2020-05-15 01:20:06',
'updated_at'=>'2020-06-21 00:08:23',
'deleted_at'=>NULL
] );



Post::create( [
'id'=>4427,
'cat_id'=>8,
'tag_id'=>1,
'title'=>'Project helps prevent 34,000 harms, saves 288 million',
'text'=>'Hospital and Health Network Magazines. September 27, 2016',
'img'=>'im6.jpg',
'file'=>NULL,
'link'=>'http://www.hhnmag.com/articles/7680-hospital-engagement-network-results-show-34000-harms-prevented',
'by'=>0,
'journal'=>'Paul Barr',
'type'=>0,
'month_article'=>0,
'famous'=>0,
'views'=>1130,
'show'=>0,
'date'=>'2016-09-29 21:00:00',
'active'=>'1',
'user'=>0,
'created_at'=>'2020-05-15 01:20:06',
'updated_at'=>'2020-06-21 00:08:23',
'deleted_at'=>NULL
] );



Post::create( [
'id'=>4428,
'cat_id'=>8,
'tag_id'=>9,
'title'=>'PPIs do not increase risk for C. difficile infection in ICU',
'text'=>'Am J Gastroenterol. 2016doi:10. 2016.343',
'img'=>'im5.jpg',
'file'=>NULL,
'link'=>'http://www.healio.com/gastroenterology/infection/news/online/%7B7976e97a-3a65-49c5-9f07-b5c98aec6011%7D/ppis-do-not-increase-risk-for-c-difficile-infection-in-icu',
'by'=>0,
'journal'=>'Faleck DM, et al.',
'type'=>0,
'month_article'=>0,
'famous'=>0,
'views'=>1101,
'show'=>0,
'date'=>'2016-10-06 21:00:00',
'active'=>'1',
'user'=>0,
'created_at'=>'2020-05-15 01:20:06',
'updated_at'=>'2020-06-21 00:08:23',
'deleted_at'=>NULL
] );



Post::create( [
'id'=>4429,
'cat_id'=>8,
'tag_id'=>7,
'title'=>'Study links lower heart rate to better septic shock survival',
'text'=>'October 05, 2016',
'img'=>'file-1463591639-4YG8JY.jpg',
'file'=>NULL,
'link'=>'http://www.beckershospitalreview.com/quality/lower-heart-rates-reduce-risk-of-death-among-septic-shock-patients-4-insights.html',
'by'=>0,
'journal'=>'Written by Anuja Vaidya',
'type'=>0,
'month_article'=>0,
'famous'=>0,
'views'=>1101,
'show'=>0,
'date'=>'2016-10-04 21:00:00',
'active'=>'1',
'user'=>0,
'created_at'=>'2020-05-15 01:20:06',
'updated_at'=>'2020-06-21 00:08:23',
'deleted_at'=>NULL
] );



Post::create( [
'id'=>4430,
'cat_id'=>8,
'tag_id'=>6,
'title'=>'Fundamentals of aerosol therapy in critical care',
'text'=>'Critical Care 2016, 20 :269 7 October 2016',
'img'=>'im6.jpg',
'file'=>NULL,
'link'=>'http://ccforum.biomedcentral.com/articles/10.1186/s13054-016-1448-5',
'by'=>0,
'journal'=>'Dhanani J, Fraser J, Chan H, Rello J, Cohen J, Roberts J',
'type'=>0,
'month_article'=>0,
'famous'=>0,
'views'=>1101,
'show'=>0,
'date'=>'2016-10-07 21:00:00',
'active'=>'1',
'user'=>0,
'created_at'=>'2020-05-15 01:20:06',
'updated_at'=>'2020-06-21 00:08:23',
'deleted_at'=>NULL
] );



Post::create( [
'id'=>4431,
'cat_id'=>8,
'tag_id'=>11,
'title'=>'Hyperchloremia and moderate increase in serum chloride are associated with acute kidney injury in severe sepsis and septic shock patients',
'text'=>'Critical Care 2016, 20 :315 6 October 2016',
'img'=>'file-1463591738-IZ3XFW.jpg',
'file'=>NULL,
'link'=>'http://ccforum.biomedcentral.com/articles/10.1186/s13054-016-1499-7',
'by'=>0,
'journal'=>'Suetrong B, Pisitsak C, Boyd J, Russell J, Walley K',
'type'=>0,
'month_article'=>0,
'famous'=>0,
'views'=>1101,
'show'=>0,
'date'=>'2016-10-07 21:00:00',
'active'=>'1',
'user'=>0,
'created_at'=>'2020-05-15 01:20:06',
'updated_at'=>'2020-06-21 00:08:23',
'deleted_at'=>NULL
] );



Post::create( [
'id'=>4432,
'cat_id'=>12,
'tag_id'=>11,
'title'=>'Effect of Postextubation High-Flow Nasal Cannula vs Noninvasive Ventilation on Reintubation and Postextubation Respiratory Failure in High-Risk Patients',
'text'=>'JAMA. Published online October 5, 2016',
'img'=>'file-1463591752-XK911T.jpg',
'file'=>NULL,
'link'=>'http://jamanetwork.com/journals/jama/article-abstract/2565304',
'by'=>0,
'journal'=>'Gonzalo Hernández and others',
'type'=>0,
'month_article'=>0,
'famous'=>0,
'views'=>1212,
'show'=>0,
'date'=>'2016-10-14 21:00:00',
'active'=>'1',
'user'=>0,
'created_at'=>'2020-05-15 01:20:06',
'updated_at'=>'2020-06-21 00:08:23',
'deleted_at'=>NULL
] );



Post::create( [
'id'=>4433,
'cat_id'=>9,
'tag_id'=>7,
'title'=>'Levosimendan Does Not Improve Septic Shock Outcomes',
'text'=>'Medscape Medical News, October 10, 2016',
'img'=>'file-1463591639-4YG8JY.jpg',
'file'=>NULL,
'link'=>'http://www.medscape.com/viewarticle/870026',
'by'=>0,
'journal'=>'Diana Phillips',
'type'=>0,
'month_article'=>0,
'famous'=>0,
'views'=>1101,
'show'=>0,
'date'=>'2016-10-14 21:00:00',
'active'=>'1',
'user'=>0,
'created_at'=>'2020-05-15 01:20:06',
'updated_at'=>'2020-06-21 00:08:23',
'deleted_at'=>NULL
] );



Post::create( [
'id'=>4434,
'cat_id'=>8,
'tag_id'=>11,
'title'=>'Mortality Not Up at Nurse-Practitioner-Staffed ICU',
'text'=>'HealthDay News, Oct. 17, 2016',
'img'=>'file-1464165480-R231XR.jpg',
'file'=>NULL,
'link'=>'http://www.physiciansbriefing.com/Article.asp?AID=715660',
'by'=>0,
'journal'=>'Rachel Scherzer and others',
'type'=>0,
'month_article'=>0,
'famous'=>0,
'views'=>1101,
'show'=>0,
'date'=>'2016-10-16 21:00:00',
'active'=>'1',
'user'=>0,
'created_at'=>'2020-05-15 01:20:06',
'updated_at'=>'2020-06-21 00:08:23',
'deleted_at'=>NULL
] );



Post::create( [
'id'=>4435,
'cat_id'=>12,
'tag_id'=>11,
'title'=>'New guidelines published for discontinuing mechanical ventilation in ICU',
'text'=>'',
'img'=>'file-1463591752-XK911T.jpg',
'file'=>NULL,
'link'=>'http://www.chestnet.org/News/Press-Releases/2016/10/New-Guidelines-Published-for-Discontinuing-Mechanical-Ventilation-in-ICU',
'by'=>0,
'journal'=>'Glenview, IL─The American College of Chest Physicians CHEST and the American Thoracic Society ATS October 25, 2016',
'type'=>0,
'month_article'=>0,
'famous'=>0,
'views'=>1212,
'show'=>0,
'date'=>'2016-10-24 21:00:00',
'active'=>'1',
'user'=>0,
'created_at'=>'2020-05-15 01:20:06',
'updated_at'=>'2020-06-21 00:08:23',
'deleted_at'=>NULL
] );



Post::create( [
'id'=>4436,
'cat_id'=>9,
'tag_id'=>11,
'title'=>'New Guidelines Published for Discontinuing Mechanical Ventilation in ICU',
'text'=>'',
'img'=>'im6.jpg',
'file'=>NULL,
'link'=>'http://www.chestnet.org/News/Press-Releases/2016/10/New-Guidelines-Published-for-Discontinuing-Mechanical-Ventilation-in-ICU',
'by'=>0,
'journal'=>'Glenview, IL─The American College of Chest Physicians CHEST and the American Thoracic Society ATS October 25, 2016',
'type'=>0,
'month_article'=>0,
'famous'=>0,
'views'=>1101,
'show'=>0,
'date'=>'2016-10-24 21:00:00',
'active'=>'1',
'user'=>0,
'created_at'=>'2020-05-15 01:20:06',
'updated_at'=>'2020-06-21 00:08:23',
'deleted_at'=>NULL
] );



Post::create( [
'id'=>4437,
'cat_id'=>8,
'tag_id'=>1,
'title'=>'Bacteria may spread from patients to hospital scrubs, study finds',
'text'=>'THURSDAY, Oct. 27, 2016 HealthDay News',
'img'=>'file-1463591681-AGRM7H.jpg',
'file'=>NULL,
'link'=>'https://consumer.healthday.com/infectious-disease-information-21/staph-infection-mrsa-news-766/briefs-emb-10-27-10amet-nurses-scrubs-bacteria-idweek-duke-release-batch-2933-716183.html',
'by'=>0,
'journal'=>'Superbug MRSA, other disease-causing bacteria detected on uniforms in ICU',
'type'=>0,
'month_article'=>0,
'famous'=>0,
'views'=>1130,
'show'=>0,
'date'=>'2016-10-26 21:00:00',
'active'=>'1',
'user'=>0,
'created_at'=>'2020-05-15 01:20:06',
'updated_at'=>'2020-06-21 00:08:23',
'deleted_at'=>NULL
] );



Post::create( [
'id'=>4438,
'cat_id'=>8,
'tag_id'=>1,
'title'=>'Hospital patients on antibiotics may have higher sepsis risk',
'text'=>'',
'img'=>'file-1464106363-HWQX6B.jpg',
'file'=>NULL,
'link'=>'http://www.medpagetoday.com/MeetingCoverage/IDWeek/61073',
'by'=>0,
'journal'=>'Michael Smith  North American Correspondent, MedPage Today October 27, 2016',
'type'=>0,
'month_article'=>0,
'famous'=>0,
'views'=>1130,
'show'=>0,
'date'=>'2016-10-28 21:00:00',
'active'=>'1',
'user'=>0,
'created_at'=>'2020-05-15 01:20:06',
'updated_at'=>'2020-06-21 00:08:23',
'deleted_at'=>NULL
] );



Post::create( [
'id'=>4439,
'cat_id'=>12,
'tag_id'=>11,
'title'=>'High-dose intravenous selenium does not improve clinical outcomes in the critically ill: a systematic review and meta-analysis',
'text'=>'Critical Care, October 28, 201620:356',
'img'=>'file-1463591732-SM39NY.jpg',
'file'=>NULL,
'link'=>'https://ccforum.biomedcentral.com/articles/10.1186/s13054-016-1529-5',
'by'=>0,
'journal'=>'William Manzanares, Margot Lemieux, Gunnar Elke, Pascal L. Langlois, Frank Bloos and Daren K. Heyland',
'type'=>0,
'month_article'=>0,
'famous'=>0,
'views'=>1212,
'show'=>0,
'date'=>'2016-10-25 21:00:00',
'active'=>'1',
'user'=>0,
'created_at'=>'2020-05-15 01:20:06',
'updated_at'=>'2020-06-21 00:08:23',
'deleted_at'=>NULL
] );



Post::create( [
'id'=>4440,
'cat_id'=>12,
'tag_id'=>11,
'title'=>'Subglottic secretion suction for preventing ventilator-associated pneumonia: an updated meta-analysis and trial sequential analysis',
'text'=>'Critical Care. October 28, 2016 20:353',
'img'=>'file-1463591644-ZGN2DA.jpg',
'file'=>NULL,
'link'=>'https://ccforum.biomedcentral.com/articles/10.1186/s13054-016-1527-7',
'by'=>0,
'journal'=>'Zhi Mao, Ling Gao, Guoqi Wang, Chao Liu, Yan Zhao, Wanjie Gu, Hongjun Kang and Feihu Zhou',
'type'=>0,
'month_article'=>0,
'famous'=>0,
'views'=>1212,
'show'=>0,
'date'=>'2016-10-28 21:00:00',
'active'=>'1',
'user'=>0,
'created_at'=>'2020-05-15 01:20:06',
'updated_at'=>'2020-06-21 00:08:23',
'deleted_at'=>NULL
] );



Post::create( [
'id'=>4441,
'cat_id'=>8,
'tag_id'=>11,
'title'=>'Not all β-lactams are equal regarding neurotoxicity',
'text'=>'Critical Care, October 27, 2016 20:350',
'img'=>'file-1464165463-D3CBM9.jpg',
'file'=>NULL,
'link'=>'https://ccforum.biomedcentral.com/articles/10.1186/s13054-016-1522-z',
'by'=>0,
'journal'=>'Khalil Chaïbi, Maïté Chaussard, Sabri Soussi, Matthieu Lafaurie and Matthieu Legrand',
'type'=>0,
'month_article'=>0,
'famous'=>0,
'views'=>1101,
'show'=>0,
'date'=>'2016-10-28 21:00:00',
'active'=>'1',
'user'=>0,
'created_at'=>'2020-05-15 01:20:06',
'updated_at'=>'2020-06-21 00:08:23',
'deleted_at'=>NULL
] );



Post::create( [
'id'=>4442,
'cat_id'=>8,
'tag_id'=>11,
'title'=>'Surviving critical illness: what is next An expert consensus statement on physical rehabilitation after hospital discharge',
'text'=>'Critical Care, October 27, 2016 20:354',
'img'=>'file-1464165480-R231XR.jpg',
'file'=>NULL,
'link'=>'https://ccforum.biomedcentral.com/articles/10.1186/s13054-016-1508-x',
'by'=>0,
'journal'=>'M. E. Major and others',
'type'=>0,
'month_article'=>0,
'famous'=>0,
'views'=>1101,
'show'=>0,
'date'=>'2016-10-28 21:00:00',
'active'=>'1',
'user'=>0,
'created_at'=>'2020-05-15 01:20:06',
'updated_at'=>'2020-06-21 00:08:23',
'deleted_at'=>NULL
] );



Post::create( [
'id'=>4443,
'cat_id'=>8,
'tag_id'=>11,
'title'=>'Lung ultrasound: a promising tool to monitor ventilator-associated pneumonia in critically ill patients',
'text'=>'Critical Care, October 27, 2016 20:320',
'img'=>'file-1463591752-XK911T.jpg',
'file'=>NULL,
'link'=>'https://ccforum.biomedcentral.com/articles/10.1186/s13054-016-1457-4',
'by'=>0,
'journal'=>'Guyi Wang, Xiaoying Ji, Yongshan Xu and Xudong Xiang',
'type'=>0,
'month_article'=>0,
'famous'=>0,
'views'=>1101,
'show'=>0,
'date'=>'2016-10-28 21:00:00',
'active'=>'1',
'user'=>0,
'created_at'=>'2020-05-15 01:20:06',
'updated_at'=>'2020-06-21 00:08:23',
'deleted_at'=>NULL
] );



Post::create( [
'id'=>4444,
'cat_id'=>8,
'tag_id'=>3,
'title'=>'Benefit of prokinetics during enteral nutrition: still searching for a piece of evidence',
'text'=>'Critical Care, October 27, 2016 20:341',
'img'=>'file-1463591657-K8BRDF.jpg',
'file'=>NULL,
'link'=>'https://ccforum.biomedcentral.com/articles/10.1186/s13054-016-1502-3',
'by'=>0,
'journal'=>'Alain Dive',
'type'=>0,
'month_article'=>0,
'famous'=>0,
'views'=>1101,
'show'=>0,
'date'=>'2016-10-28 21:00:00',
'active'=>'1',
'user'=>0,
'created_at'=>'2020-05-15 01:20:06',
'updated_at'=>'2020-06-21 00:08:23',
'deleted_at'=>NULL
] );



Post::create( [
'id'=>4445,
'cat_id'=>8,
'tag_id'=>10,
'title'=>'Stroke, Bleeding, and Mortality Risks in Elderly Medicare Beneficiaries Treated With Dabigatran or Rivaroxaban for Nonvalvular Atrial Fibrillation',
'text'=>'JAMA Intern Med. Published online October 3, 2016',
'img'=>'file-1463591685-DDLMK9.jpg',
'file'=>NULL,
'link'=>'http://jamanetwork.com/journals/jamainternalmedicine/article-abstract/2560376',
'by'=>0,
'journal'=>'David J. Graham and others',
'type'=>0,
'month_article'=>0,
'famous'=>0,
'views'=>1101,
'show'=>0,
'date'=>'2016-10-28 21:00:00',
'active'=>'1',
'user'=>0,
'created_at'=>'2020-05-15 01:20:06',
'updated_at'=>'2020-06-21 00:08:23',
'deleted_at'=>NULL
] );



Post::create( [
'id'=>4446,
'cat_id'=>8,
'tag_id'=>11,
'title'=>'The younger frail critically ill patient: a newly recognised phenomenon in intensive care',
'text'=>'Critical Care .Nov 1, 2016 20:349',
'img'=>'file-1464165480-R231XR.jpg',
'file'=>NULL,
'link'=>'http://ccforum.biomedcentral.com/articles/10.1186/s13054-016-1526-8',
'by'=>0,
'journal'=>'Stephen Bonner and Nazir I. Lone',
'type'=>0,
'month_article'=>0,
'famous'=>0,
'views'=>1101,
'show'=>0,
'date'=>'2016-11-02 21:00:00',
'active'=>'1',
'user'=>0,
'created_at'=>'2020-05-15 01:20:06',
'updated_at'=>'2020-06-21 00:08:23',
'deleted_at'=>NULL
] );



Post::create( [
'id'=>4447,
'cat_id'=>8,
'tag_id'=>7,
'title'=>'The Effect of Intraaortic Balloon Pumping Under Venoarterial Extracorporeal Membrane Oxygenation on Mortality of Cardiogenic Patients: An Analysis Using a Nationwide Inpatient Database',
'text'=>'Critical Care Medicine:\r\nNovember 2016 - Volume 44 - Issue 11 - p 1974–1979',
'img'=>'file-1463591639-4YG8JY.jpg',
'file'=>NULL,
'link'=>'http://journals.lww.com/ccmjournal/Abstract/2016/11000/The_Effect_of_Intraaortic_Balloon_Pumping_Under.3.aspx',
'by'=>0,
'journal'=>'Aso, Shotaro and others',
'type'=>0,
'month_article'=>0,
'famous'=>0,
'views'=>1101,
'show'=>0,
'date'=>'2016-10-31 21:00:00',
'active'=>'1',
'user'=>0,
'created_at'=>'2020-05-15 01:20:06',
'updated_at'=>'2020-06-21 00:08:23',
'deleted_at'=>NULL
] );



Post::create( [
'id'=>4448,
'cat_id'=>8,
'tag_id'=>1,
'title'=>'Elevated Plasma Angiopoietin-2 Levels Are Associated With Fluid Overload, Organ Dysfunction, and Mortality in Human Septic Shock',
'text'=>'Critical Care Medicine:\r\nNovember 2016 - Volume 44 - Issue 11 - p 2018–2027',
'img'=>'file-1463591644-ZGN2DA.jpg',
'file'=>NULL,
'link'=>'http://journals.lww.com/ccmjournal/Abstract/2016/11000/Elevated_Plasma_Angiopoietin_2_Levels_Are.9.aspx',
'by'=>0,
'journal'=>'Fisher, Jane and others',
'type'=>0,
'month_article'=>0,
'famous'=>0,
'views'=>1130,
'show'=>0,
'date'=>'2016-11-02 21:00:00',
'active'=>'1',
'user'=>0,
'created_at'=>'2020-05-15 01:20:06',
'updated_at'=>'2020-06-21 00:08:23',
'deleted_at'=>NULL
] );



Post::create( [
'id'=>4449,
'cat_id'=>8,
'tag_id'=>7,
'title'=>'The Postcardiac Arrest Consult Team: Impact on Hospital Care Processes for Out-of-Hospital Cardiac Arrest Patients',
'text'=>'Skip Navigation LinksHome  Current Issue  The Postcardiac Arrest Consult Team: Impact on Hospital Car...\r\n Previous Abstract  Next Abstract \r\nYou could be reading the full-text of this article now if you...\r\nBecome a subscriber\r\n \r\nPurchase this article\r\nIf you have access to this article through your institution, \r\nyou can view this article in\r\n \r\nCritical Care Medicine:\r\nNovember 2016 - Volume 44 - Issue 11 - p 2037–2044',
'img'=>'im1.jpg',
'file'=>NULL,
'link'=>'http://journals.lww.com/ccmjournal/Abstract/2016/11000/The_Postcardiac_Arrest_Consult_Team___Impact_on.11.aspx',
'by'=>0,
'journal'=>'Brooks, Steven C and others',
'type'=>0,
'month_article'=>0,
'famous'=>0,
'views'=>1101,
'show'=>0,
'date'=>'2016-10-31 21:00:00',
'active'=>'1',
'user'=>0,
'created_at'=>'2020-05-15 01:20:06',
'updated_at'=>'2020-06-21 00:08:23',
'deleted_at'=>NULL
] );



Post::create( [
'id'=>4450,
'cat_id'=>12,
'tag_id'=>11,
'title'=>'Randomized Trial of Video Laryngoscopy for Endotracheal Intubation of Critically Ill Adults',
'text'=>'Skip Navigation LinksHome  Current Issue  Randomized Trial of Video Laryngoscopy for Endotracheal Intu...\r\n Previous Abstract  Next Abstract \r\nYou could be reading the full-text of this article now if you...\r\nBecome a subscriber\r\n \r\nPurchase this article\r\nIf you have access to this article through your institution, \r\nyou can view this article in\r\n \r\nCritical Care Medicine:\r\nNovember 2016 - Volume 44 - Issue 11 - p 1980–1987',
'img'=>'im6.jpg',
'file'=>NULL,
'link'=>'http://journals.lww.com/ccmjournal/Abstract/2016/11000/Randomized_Trial_of_Video_Laryngoscopy_for.4.aspx',
'by'=>0,
'journal'=>'Janz, David R and others',
'type'=>0,
'month_article'=>0,
'famous'=>0,
'views'=>1212,
'show'=>0,
'date'=>'2016-11-02 21:00:00',
'active'=>'1',
'user'=>0,
'created_at'=>'2020-05-15 01:20:06',
'updated_at'=>'2020-06-21 00:08:23',
'deleted_at'=>NULL
] );



Post::create( [
'id'=>4451,
'cat_id'=>12,
'tag_id'=>7,
'title'=>'Focus on veno-venous ECMO in adults with severe ARDS',
'text'=>'Intensive Care Med. Nov. 2016 4211:1655 - 1657',
'img'=>'file-1463591752-XK911T.jpg',
'file'=>NULL,
'link'=>'http://icmjournal.esicm.org/journals/abstract.html?v=42&j=134&i=11&a=4398_10.1007_s00134-016-4398-0&doi=',
'by'=>0,
'journal'=>'Laurent Papazian, Margaret Herridge, Alain Combes',
'type'=>0,
'month_article'=>0,
'famous'=>0,
'views'=>1212,
'show'=>0,
'date'=>'2016-11-05 21:00:00',
'active'=>'1',
'user'=>0,
'created_at'=>'2020-05-15 01:20:06',
'updated_at'=>'2020-06-21 00:08:23',
'deleted_at'=>NULL
] );



Post::create( [
'id'=>4452,
'cat_id'=>8,
'tag_id'=>11,
'title'=>'Associations between ventilator settings during extracorporeal membrane oxygenation for refractory hypoxemia and outcome in patients with acute respiratory distress syndrome: a pooled individual patient data analysis',
'text'=>'Intensive Care Med. Nov. 2016 4211:1672 - 1684',
'img'=>'im4.jpg',
'file'=>NULL,
'link'=>'http://icmjournal.esicm.org/journals/abstract.html?v=42&j=134&i=11&a=4507_10.1007_s00134-016-4507-0&doi=',
'by'=>0,
'journal'=>'Ary Serpa Neto and others',
'type'=>0,
'month_article'=>0,
'famous'=>0,
'views'=>1101,
'show'=>0,
'date'=>'2016-11-05 21:00:00',
'active'=>'1',
'user'=>0,
'created_at'=>'2020-05-15 01:20:06',
'updated_at'=>'2020-06-21 00:08:23',
'deleted_at'=>NULL
] );



Post::create( [
'id'=>4453,
'cat_id'=>8,
'tag_id'=>5,
'title'=>'The rate of brain death and organ donation in patients resuscitated from cardiac arrest: a systematic review and meta-analysis',
'text'=>'Intensive Care Med. Nov. 2016 4211:1661 - 1671',
'img'=>'im2.jpg',
'file'=>NULL,
'link'=>'http://icmjournal.esicm.org/journals/abstract.html?v=42&j=134&i=11&a=4549_10.1007_s00134-016-4549-3&doi=',
'by'=>0,
'journal'=>'Claudio Sandroni and others',
'type'=>0,
'month_article'=>0,
'famous'=>0,
'views'=>1114,
'show'=>0,
'date'=>'2016-11-05 21:00:00',
'active'=>'1',
'user'=>0,
'created_at'=>'2020-05-15 01:20:06',
'updated_at'=>'2020-06-21 00:08:23',
'deleted_at'=>NULL
] );



Post::create( [
'id'=>4454,
'cat_id'=>12,
'tag_id'=>10,
'title'=>'Long-term outcomes in patients with septic shock transfused at a lower versus a higher haemoglobin threshold: the TRISS randomised, multicentre clinical trial',
'text'=>'Intensive Care Med. Nov. 2016 4211:1685 - 1694',
'img'=>'file-1463591681-AGRM7H.jpg',
'file'=>NULL,
'link'=>'http://icmjournal.esicm.org/journals/abstract.htmlv42&j134&i11&a4437_10.1007_s00134-016-4437-x&doi',
'by'=>0,
'journal'=>'Sofie L. Rygård and others',
'type'=>0,
'month_article'=>0,
'famous'=>0,
'views'=>1101,
'show'=>0,
'date'=>'2016-11-05 21:00:00',
'active'=>'1',
'user'=>0,
'created_at'=>'2020-05-15 01:20:06',
'updated_at'=>'2020-06-21 00:08:23',
'deleted_at'=>NULL
] );

Post::create( [
'id'=>4455,
'cat_id'=>8,
'tag_id'=>7,
'title'=>'Restricting volumes of resuscitation fluid in adults with septic shock after initial management: the CLASSIC randomised, parallel-group, multicentre feasibility trial',
'text'=>'Intensive Care Med. Nov. 2016 4211:1685 - 1694',
'img'=>'im3.jpg',
'file'=>NULL,
'link'=>'http://icmjournal.esicm.org/journals/abstract.html?v=42&j=134&i=11&a=4500_10.1007_s00134-016-4500-7&doi=',
'by'=>0,
'journal'=>'Peter B. Hjortrup and others',
'type'=>0,
'month_article'=>0,
'famous'=>0,
'views'=>1101,
'show'=>0,
'date'=>'2016-11-05 21:00:00',
'active'=>'1',
'user'=>0,
'created_at'=>'2020-05-15 01:20:06',
'updated_at'=>'2020-06-21 00:08:23',
'deleted_at'=>NULL
] );



Post::create( [
'id'=>4456,
'cat_id'=>12,
'tag_id'=>11,
'title'=>'Protein C zymogen in severe sepsis: a double-blinded, placebo-controlled, randomized study',
'text'=>'Intensive Care Med. Nov. 2016 4211:1706 - 1714',
'img'=>'im5.jpg',
'file'=>NULL,
'link'=>'http://icmjournal.esicm.org/journals/abstract.html?v=42&j=134&i=11&a=4405_10.1007_s00134-016-4405-5&doi=',
'by'=>0,
'journal'=>'Federico Pappalardo and others',
'type'=>0,
'month_article'=>0,
'famous'=>0,
'views'=>1212,
'show'=>0,
'date'=>'2016-11-05 21:00:00',
'active'=>'1',
'user'=>0,
'created_at'=>'2020-05-15 01:20:06',
'updated_at'=>'2020-06-21 00:08:23',
'deleted_at'=>NULL
] );



Post::create( [
'id'=>4457,
'cat_id'=>8,
'tag_id'=>11,
'title'=>'Intravenous iron or placebo for anaemia in intensive care: the IRONMAN multicentre randomized blinded trial : A randomized trial of IV iron in critical illness',
'text'=>'Intensive Care Med. Nov. 2016 4211:1715 - 1722',
'img'=>'file-1463591685-DDLMK9.jpg',
'file'=>NULL,
'link'=>'http://icmjournal.esicm.org/journals/abstract.htmlv42&j134&i11&a4465_10.1007_s00134-016-4465-6&doi',
'by'=>0,
'journal'=>'Edward Litton and others',
'type'=>0,
'month_article'=>0,
'famous'=>0,
'views'=>1101,
'show'=>0,
'date'=>'2016-11-05 21:00:00',
'active'=>'1',
'user'=>0,
'created_at'=>'2020-05-15 01:20:06',
'updated_at'=>'2020-06-21 00:08:23',
'deleted_at'=>NULL
] );



Post::create( [
'id'=>4458,
'cat_id'=>8,
'tag_id'=>6,
'title'=>'Neurally adjusted ventilatory assist as an alternative to pressure support ventilation in adults: a French multicentre randomized trial',
'text'=>'Intensive Care Med. Nov. 2016 4211:1723 - 1732',
'img'=>'im6.jpg',
'file'=>NULL,
'link'=>'http://icmjournal.esicm.org/journals/abstract.html?v=42&j=134&i=11&a=4447_10.1007_s00134-016-4447-8&doi=',
'by'=>0,
'journal'=>'A. Demoule, M. Clavel and others',
'type'=>0,
'month_article'=>0,
'famous'=>0,
'views'=>1101,
'show'=>0,
'date'=>'2016-11-05 21:00:00',
'active'=>'1',
'user'=>0,
'created_at'=>'2020-05-15 01:20:06',
'updated_at'=>'2020-06-21 00:08:23',
'deleted_at'=>NULL
] );



Post::create( [
'id'=>4459,
'cat_id'=>8,
'tag_id'=>10,
'title'=>'A recovery program to improve quality of life, sense of coherence and psychological health in ICU survivors: a multicenter randomized controlled trial, the RAPIT study',
'text'=>'Intensive Care Med. Nov. 2016 4211:1733 - 1743',
'img'=>'file-1463591639-4YG8JY.jpg',
'file'=>NULL,
'link'=>'http://icmjournal.esicm.org/journals/abstract.html?v=42&j=134&i=11&a=4522_10.1007_s00134-016-4522-1&doi=',
'by'=>0,
'journal'=>'Janet F. Jensen and others',
'type'=>0,
'month_article'=>0,
'famous'=>0,
'views'=>1101,
'show'=>0,
'date'=>'2016-10-31 21:00:00',
'active'=>'1',
'user'=>0,
'created_at'=>'2020-05-15 01:20:06',
'updated_at'=>'2020-06-21 00:08:23',
'deleted_at'=>NULL
] );



Post::create( [
'id'=>4460,
'cat_id'=>8,
'tag_id'=>11,
'title'=>'Vitamin D Deficiency — Is There Really a Pandemic',
'text'=>'N Engl J Med 2016375:1817-1820',
'img'=>'file-1464165509-QNPJY8.jpg',
'file'=>NULL,
'link'=>'http://www.nejm.org/doi/full/10.1056/NEJMp1608005?query=TOC',
'by'=>0,
'journal'=>'J.E. Manson, P.M. Brannon, C.J. Rosen, and C.L. Taylor',
'type'=>0,
'month_article'=>0,
'famous'=>0,
'views'=>1101,
'show'=>0,
'date'=>'2016-11-09 21:00:00',
'active'=>'1',
'user'=>0,
'created_at'=>'2020-05-15 01:20:06',
'updated_at'=>'2020-06-21 00:08:23',
'deleted_at'=>NULL
] );



Post::create( [
'id'=>4461,
'cat_id'=>9,
'tag_id'=>9,
'title'=>'Guidelines for Diagnosis, Treatment, and Prevention of Clostridium diffi cile Infections',
'text'=>'Am J Gastroenterol 2013 108:478–498 published online 26 February 2013',
'img'=>'file-1463591657-K8BRDF.jpg',
'file'=>NULL,
'link'=>'http://gi.org/wp-content/uploads/2013/04/ACG_Guideline_Cdifficile_April_2013.pdf',
'by'=>0,
'journal'=>'Christina M. Surawicz  and others',
'type'=>0,
'month_article'=>0,
'famous'=>0,
'views'=>1101,
'show'=>0,
'date'=>'2013-02-25 21:00:00',
'active'=>'1',
'user'=>0,
'created_at'=>'2020-05-15 01:20:06',
'updated_at'=>'2020-06-21 00:08:23',
'deleted_at'=>NULL
] );



Post::create( [
'id'=>4462,
'cat_id'=>8,
'tag_id'=>7,
'title'=>'Cholesterol, Cardiovascular Risk, Statins, PCSK9 Inhibitors, and the Future of LDL-C Lowering',
'text'=>'free access  JAMA. November 201631619:1969-1970',
'img'=>'file-1463591681-AGRM7H.jpg',
'file'=>NULL,
'link'=>'http://jamanetwork.com/journals/jama/fullarticle/2584062',
'by'=>0,
'journal'=>'Fatima Rodriguez, MD, MPH Robert A. Harrington, M',
'type'=>0,
'month_article'=>0,
'famous'=>0,
'views'=>1101,
'show'=>0,
'date'=>'2016-11-15 21:00:00',
'active'=>'1',
'user'=>0,
'created_at'=>'2020-05-15 01:20:06',
'updated_at'=>'2020-06-21 00:08:23',
'deleted_at'=>NULL
] );



Post::create( [
'id'=>4464,
'cat_id'=>9,
'tag_id'=>7,
'title'=>'Interpretation and Use of Another Statin Guideline',
'text'=>'JAMA. 201631619:1977-1979',
'img'=>'file-1463591644-ZGN2DA.jpg',
'file'=>NULL,
'link'=>'http://jamanetwork.com/journals/jama/fullarticle/2584033',
'by'=>0,
'journal'=>'Philip Greenland Robert O. Bonow',
'type'=>0,
'month_article'=>0,
'famous'=>0,
'views'=>1101,
'show'=>0,
'date'=>'2016-11-15 21:00:00',
'active'=>'1',
'user'=>0,
'created_at'=>'2020-05-15 01:20:06',
'updated_at'=>'2020-06-21 00:08:23',
'deleted_at'=>NULL
] );



Post::create( [
'id'=>4465,
'cat_id'=>8,
'tag_id'=>2,
'title'=>'Effect of Conscious Sedation vs General Anesthesia on Early Neurological Improvement Among Patients With Ischemic Stroke Undergoing Endovascular ThrombectomyA Randomized Clinical Trial',
'text'=>'JAMA. 201631619:1986-1996',
'img'=>'im1.jpg',
'file'=>NULL,
'link'=>'http://jamanetwork.com/journals/jama/article-abstract/2577957',
'by'=>0,
'journal'=>'Silvia Schönenberger Lorenz Uhlmann Werner Hacke',
'type'=>0,
'month_article'=>0,
'famous'=>0,
'views'=>1172,
'show'=>0,
'date'=>'2016-11-15 21:00:00',
'active'=>'1',
'user'=>0,
'created_at'=>'2020-05-15 01:20:06',
'updated_at'=>'2020-06-21 00:08:23',
'deleted_at'=>NULL
] );



Post::create( [
'id'=>4466,
'cat_id'=>12,
'tag_id'=>7,
'title'=>'Statin Use for the Primary Prevention of Cardiovascular Disease in AdultsUS Preventive Services Task Force Recommendation Statement',
'text'=>'JAMA. November 201631619:1997-2007',
'img'=>'file-1463591639-4YG8JY.jpg',
'file'=>NULL,
'link'=>'http://jamanetwork.com/journals/jama/currentissue',
'by'=>0,
'journal'=>'US Preventive Services Task Force Kirsten Bibbins-Domingo David C. Grossman et al',
'type'=>0,
'month_article'=>0,
'famous'=>0,
'views'=>1212,
'show'=>0,
'date'=>'2016-11-15 21:00:00',
'active'=>'1',
'user'=>0,
'created_at'=>'2020-05-15 01:20:06',
'updated_at'=>'2020-06-21 00:08:23',
'deleted_at'=>NULL
] );



Post::create( [
'id'=>4467,
'cat_id'=>12,
'tag_id'=>10,
'title'=>'Clinical Practice Guidelines From the AABBRed Blood Cell Transfusion Thresholds and Storage',
'text'=>'JAMA. November 201631619:2025-2035',
'img'=>'file-1463591685-DDLMK9.jpg',
'file'=>NULL,
'link'=>'http://jamanetwork.com/journals/jama/article-abstract/2569055',
'by'=>0,
'journal'=>'Jeffrey L. Carson Gordon Guyatt Nancy M. Heddle  et al.',
'type'=>0,
'month_article'=>0,
'famous'=>0,
'views'=>1212,
'show'=>0,
'date'=>'2016-11-15 21:00:00',
'active'=>'1',
'user'=>0,
'created_at'=>'2020-05-15 01:20:06',
'updated_at'=>'2020-06-21 00:08:23',
'deleted_at'=>NULL
] );



Post::create( [
'id'=>4468,
'cat_id'=>12,
'tag_id'=>1,
'title'=>'Protocol-Based Resuscitation Bundle to Improve Outcomes in Septic Shock Patients: Evaluation of the Michigan Health and Hospital Association Keystone Sepsis Collaborative',
'text'=>'Critical Care Medicine:\r\nDecember 2016 - Volume 44 - Issue 12 - p 2123–2130',
'img'=>'file-1464165480-R231XR.jpg',
'file'=>NULL,
'link'=>'http://journals.lww.com/ccmjournal/Fulltext/2016/12000/Protocol_Based_Resuscitation_Bundle_to_Improve.1.aspx',
'by'=>0,
'journal'=>'Thompson, Michael P and others',
'type'=>0,
'month_article'=>0,
'famous'=>0,
'views'=>1241,
'show'=>0,
'date'=>'2016-11-30 21:00:00',
'active'=>'1',
'user'=>0,
'created_at'=>'2020-05-15 01:20:06',
'updated_at'=>'2020-06-21 00:08:23',
'deleted_at'=>NULL
] );



Post::create( [
'id'=>4469,
'cat_id'=>8,
'tag_id'=>6,
'title'=>'Long-Term Survival Rate in Patients With Acute Respiratory Failure Treated With Noninvasive Ventilation in Ordinary Wards',
'text'=>'Critical Care Medicine:\r\nDecember 2016 - Volume 44 - Issue 12 - p 2139–2144',
'img'=>'im5.jpg',
'file'=>NULL,
'link'=>'http://journals.lww.com/ccmjournal/Abstract/2016/12000/Long_Term_Survival_Rate_in_Patients_With_Acute.3.aspx',
'by'=>0,
'journal'=>'Cabrini, Luca Landoni',
'type'=>0,
'month_article'=>0,
'famous'=>0,
'views'=>1101,
'show'=>0,
'date'=>'2016-11-30 21:00:00',
'active'=>'1',
'user'=>0,
'created_at'=>'2020-05-15 01:20:06',
'updated_at'=>'2020-06-21 00:08:23',
'deleted_at'=>NULL
] );



Post::create( [
'id'=>4470,
'cat_id'=>9,
'tag_id'=>6,
'title'=>'Clinical Practice Guidelines for Sustained Neuromuscular Blockade in the Adult Critically Ill Patient',
'text'=>'Critical Care Medicine: November 2016 - Volume 44 - Issue 11 - p 2079–2103',
'img'=>'file-1463591644-ZGN2DA.jpg',
'file'=>NULL,
'link'=>'http://journals.lww.com/ccmjournal/Abstract/2016/11000/Clinical_Practice_Guidelines_for_Sustained.16.aspx',
'by'=>0,
'journal'=>'Murray, Michael J and others',
'type'=>0,
'month_article'=>0,
'famous'=>0,
'views'=>1101,
'show'=>0,
'date'=>'2016-11-30 21:00:00',
'active'=>'1',
'user'=>0,
'created_at'=>'2020-05-15 01:20:06',
'updated_at'=>'2020-06-21 00:08:23',
'deleted_at'=>NULL
] );



Post::create( [
'id'=>4471,
'cat_id'=>12,
'tag_id'=>1,
'title'=>'Early Liberal Fluids for Sepsis Patients Are Harmful',
'text'=>'Critical Care Medicine:\r\nDecember 2016 - Volume 44 - Issue 12 - p 2258–2262',
'img'=>'file-1463591639-4YG8JY.jpg',
'file'=>NULL,
'link'=>'http://journals.lww.com/ccmjournal/Fulltext/2016/12000/Early_Liberal_Fluids_for_Sepsis_Patients_Are.17.aspx',
'by'=>0,
'journal'=>'Genga, Kelly MD Russell, James A. MD',
'type'=>0,
'month_article'=>0,
'famous'=>0,
'views'=>1241,
'show'=>0,
'date'=>'2016-11-30 21:00:00',
'active'=>'1',
'user'=>0,
'created_at'=>'2020-05-15 01:20:06',
'updated_at'=>'2020-06-21 00:08:23',
'deleted_at'=>NULL
] );



Post::create( [
'id'=>4472,
'cat_id'=>8,
'tag_id'=>8,
'title'=>'Lactated Ringer Is Associated With Reduced Mortality and Less Acute Kidney Injury in Critically Ill Patients: A Retrospective Cohort Analysis',
'text'=>'Critical Care Medicine:\r\nDecember 2016 - Volume 44 - Issue 12 - p 2163–2170',
'img'=>'file-1463591738-IZ3XFW.jpg',
'file'=>NULL,
'link'=>'http://journals.lww.com/ccmjournal/Abstract/2016/12000/Lactated_Ringer_Is_Associated_With_Reduced.6.aspx',
'by'=>0,
'journal'=>'Zampieri, Fernando G',
'type'=>0,
'month_article'=>0,
'famous'=>0,
'views'=>1152,
'show'=>0,
'date'=>'2016-11-30 21:00:00',
'active'=>'1',
'user'=>0,
'created_at'=>'2020-05-15 01:20:06',
'updated_at'=>'2020-06-21 00:08:23',
'deleted_at'=>NULL
] );



Post::create( [
'id'=>4473,
'cat_id'=>8,
'tag_id'=>6,
'title'=>'Lung Injury Prediction Score in Hospitalized Patients at Risk of Acute Respiratory Distress Syndrome',
'text'=>'Critical Care Medicine:\r\nDecember 2016 - Volume 44 - Issue 12 - p 2182–2191',
'img'=>'im6.jpg',
'file'=>NULL,
'link'=>'http://journals.lww.com/ccmjournal/Abstract/2016/12000/Lung_Injury_Prediction_Score_in_Hospitalized.8.aspx',
'by'=>0,
'journal'=>'Soto, Graciela J. and others',
'type'=>0,
'month_article'=>0,
'famous'=>0,
'views'=>1101,
'show'=>0,
'date'=>'2016-11-30 21:00:00',
'active'=>'1',
'user'=>0,
'created_at'=>'2020-05-15 01:20:06',
'updated_at'=>'2020-06-21 00:08:23',
'deleted_at'=>NULL
] );



Post::create( [
'id'=>4474,
'cat_id'=>8,
'tag_id'=>2,
'title'=>'The Impact of Interventions to Improve Sleep on Delirium in the ICU: A Systematic Review and Research Framework',
'text'=>'Critical Care Medicine: December 2016 - Volume 44 - Issue 12 - p 2231–2240',
'img'=>'file-1463591639-4YG8JY.jpg',
'file'=>NULL,
'link'=>'http://journals.lww.com/ccmjournal/Abstract/2016/12000/The_Impact_of_Interventions_to_Improve_Sleep_on.14.aspx',
'by'=>0,
'journal'=>'Flannery, Alexander H and others',
'type'=>0,
'month_article'=>0,
'famous'=>0,
'views'=>1172,
'show'=>0,
'date'=>'2016-11-30 21:00:00',
'active'=>'1',
'user'=>0,
'created_at'=>'2020-05-15 01:20:06',
'updated_at'=>'2020-06-21 00:08:23',
'deleted_at'=>NULL
] );



Post::create( [
'id'=>4475,
'cat_id'=>12,
'tag_id'=>11,
'title'=>'Guideline for Reversal of Antithrombotics in Intracranial Hemorrhage: Executive Summary. A Statement for Healthcare Professionals From the Neurocritical Care Society and the Society of Critical Care Medicine',
'text'=>'Critical Care Medicine:\r\nDecember 2016 - Volume 44 - Issue 12 - p 2251–2257',
'img'=>'file-1463591644-ZGN2DA.jpg',
'file'=>NULL,
'link'=>'http://journals.lww.com/ccmjournal/Citation/2016/12000/Guideline_for_Reversal_of_Antithrombotics_in.16.aspx',
'by'=>0,
'journal'=>'Frontera, Jennifer A and others',
'type'=>0,
'month_article'=>0,
'famous'=>0,
'views'=>1212,
'show'=>0,
'date'=>'2016-11-30 21:00:00',
'active'=>'1',
'user'=>0,
'created_at'=>'2020-05-15 01:20:06',
'updated_at'=>'2020-06-21 00:08:23',
'deleted_at'=>NULL
] );



Post::create( [
'id'=>4476,
'cat_id'=>8,
'tag_id'=>5,
'title'=>'Guillain-Barré syndrome linked to recent surgery top',
'text'=>'December 2016',
'img'=>'file-1463591639-4YG8JY.jpg',
'file'=>NULL,
'link'=>'http://www.univadis.md/medical-news/596/Guillain-Barre-syndrome-linked-to-recent-surgery?utm_source=newsletter+email&utm_medium=email&utm_campaign=medical+updates+-+weekly&utm_content=1167959&utm_term=automated_weekly',
'by'=>0,
'journal'=>'New study from Mayo Clinic, published in Neurology Clinical Practice',
'type'=>0,
'month_article'=>0,
'famous'=>0,
'views'=>1114,
'show'=>0,
'date'=>'2016-11-30 21:00:00',
'active'=>'1',
'user'=>0,
'created_at'=>'2020-05-15 01:20:06',
'updated_at'=>'2020-06-21 00:08:23',
'deleted_at'=>NULL
] );



Post::create( [
'id'=>4477,
'cat_id'=>8,
'tag_id'=>11,
'title'=>'The final word on vitamin D supplementation - a review of all existing evidence',
'text'=>'',
'img'=>'file-1464106363-HWQX6B.jpg',
'file'=>NULL,
'link'=>'http://www.univadis.md/medical-news/596/The-final-word-on-vitamin-D-supplementation-a-review-of-all-existing-evidence?utm_source=newsletter+email&utm_medium=email&utm_campaign=medical+updates+-+weekly&utm_content=1167959&utm_term=automated_weekly',
'by'=>0,
'journal'=>'BMJ 2016 355 2016 November 23',
'type'=>0,
'month_article'=>0,
'famous'=>0,
'views'=>1101,
'show'=>0,
'date'=>'2016-11-22 21:00:00',
'active'=>'1',
'user'=>0,
'created_at'=>'2020-05-15 01:20:06',
'updated_at'=>'2020-06-21 00:08:23',
'deleted_at'=>NULL
] );



Post::create( [
'id'=>4478,
'cat_id'=>9,
'tag_id'=>6,
'title'=>'Weaning from Mechanical Ventilation',
'text'=>'HOME  Critical Care Compendium',
'img'=>'im5.jpg',
'file'=>NULL,
'link'=>'http://lifeinthefastlane.com/ccc/weaning-from-mechanical-ventilation/',
'by'=>0,
'journal'=>'Life on the fast line',
'type'=>0,
'month_article'=>0,
'famous'=>0,
'views'=>1101,
'show'=>0,
'date'=>'2015-12-31 21:00:00',
'active'=>'1',
'user'=>0,
'created_at'=>'2020-05-15 01:20:06',
'updated_at'=>'2020-06-21 00:08:23',
'deleted_at'=>NULL
] );



Post::create( [
'id'=>4479,
'cat_id'=>9,
'tag_id'=>6,
'title'=>'New Guidelines Published for Discontinuing Mechanical Ventilation in ICU',
'text'=>'',
'img'=>'im6.jpg',
'file'=>NULL,
'link'=>'http://www.chestnet.org/News/Press-Releases/2016/10/New-Guidelines-Published-for-Discontinuing-Mechanical-Ventilation-in-ICU',
'by'=>0,
'journal'=>'Glenview, IL─The American College of Chest Physicians CHEST and the American Thoracic Society ATS October 25, 2016',
'type'=>0,
'month_article'=>0,
'famous'=>0,
'views'=>1101,
'show'=>0,
'date'=>'2016-10-25 21:00:00',
'active'=>'1',
'user'=>0,
'created_at'=>'2020-05-15 01:20:06',
'updated_at'=>'2020-06-21 00:08:23',
'deleted_at'=>NULL
] );



Post::create( [
'id'=>4480,
'cat_id'=>8,
'tag_id'=>10,
'title'=>'Overtreatment of Heparin-Induced Thrombocytopenia in the Surgical ICU',
'text'=>'Critical Care Medicine: January 2017 - Volume 45 - Issue 1 - p 28–34',
'img'=>'file-1463591685-DDLMK9.jpg',
'file'=>NULL,
'link'=>'http://journals.lww.com/ccmjournal/Abstract/2017/01000/Overtreatment_of_Heparin_Induced_Thrombocytopenia.4.aspx',
'by'=>0,
'journal'=>'Harada, Megan Y. BA and others',
'type'=>0,
'month_article'=>0,
'famous'=>0,
'views'=>1101,
'show'=>0,
'date'=>'2017-01-03 21:00:00',
'active'=>'1',
'user'=>0,
'created_at'=>'2020-05-15 01:20:06',
'updated_at'=>'2020-06-21 00:08:23',
'deleted_at'=>NULL
] );



Post::create( [
'id'=>4481,
'cat_id'=>8,
'tag_id'=>11,
'title'=>'Towards precision medicine for sepsis patients',
'text'=>'Critical Care201721:11 12 January 2017',
'img'=>'file-1464165480-R231XR.jpg',
'file'=>NULL,
'link'=>'http://ccforum.biomedcentral.com/articles/10.1186/s13054-016-1583-z',
'by'=>0,
'journal'=>'Peter Pickkers and Matthijs Kox',
'type'=>0,
'month_article'=>0,
'famous'=>0,
'views'=>1101,
'show'=>0,
'date'=>'2017-01-11 21:00:00',
'active'=>'1',
'user'=>0,
'created_at'=>'2020-05-15 01:20:06',
'updated_at'=>'2020-06-21 00:08:23',
'deleted_at'=>NULL
] );



Post::create( [
'id'=>4482,
'cat_id'=>8,
'tag_id'=>5,
'title'=>'Whole-body vibration to prevent intensive care unit-acquired weakness: safety, feasibility, and metabolic response',
'text'=>'Critical Care 2017 21:9 9 January 2017',
'img'=>'file-1464106363-HWQX6B.jpg',
'file'=>NULL,
'link'=>'http://ccforum.biomedcentral.com/articles/10.1186/s13054-016-1576-y',
'by'=>0,
'journal'=>'Tobias Wollersheim and others',
'type'=>0,
'month_article'=>0,
'famous'=>0,
'views'=>1114,
'show'=>0,
'date'=>'2017-01-08 21:00:00',
'active'=>'1',
'user'=>0,
'created_at'=>'2020-05-15 01:20:06',
'updated_at'=>'2020-06-21 00:08:23',
'deleted_at'=>NULL
] );



Post::create( [
'id'=>4483,
'cat_id'=>8,
'tag_id'=>7,
'title'=>'Normal saline versus heparin for patency of central venous catheters in adult patients - a systematic review and meta-analysis',
'text'=>'Critical Care 2017 21:5 8 January 2017',
'img'=>'file-1464165463-D3CBM9.jpg',
'file'=>NULL,
'link'=>'http://ccforum.biomedcentral.com/articles/10.1186/s13054-016-1585-x',
'by'=>0,
'journal'=>'Lei Zhong and others',
'type'=>0,
'month_article'=>0,
'famous'=>0,
'views'=>1101,
'show'=>0,
'date'=>'2017-01-07 21:00:00',
'active'=>'1',
'user'=>0,
'created_at'=>'2020-05-15 01:20:06',
'updated_at'=>'2020-06-21 00:08:23',
'deleted_at'=>NULL
] );



Post::create( [
'id'=>4484,
'cat_id'=>8,
'tag_id'=>6,
'title'=>'Use of noninvasive ventilation in immunocompromised patients with acute respiratory failure: a systematic review and meta-analysis',
'text'=>'Critical Care 2017 21:4 7 January 2017',
'img'=>'im5.jpg',
'file'=>NULL,
'link'=>'http://ccforum.biomedcentral.com/articles/10.1186/s13054-016-1586-9',
'by'=>0,
'journal'=>'Hui-Bin Huang and others',
'type'=>0,
'month_article'=>0,
'famous'=>0,
'views'=>1101,
'show'=>0,
'date'=>'2017-01-06 21:00:00',
'active'=>'1',
'user'=>0,
'created_at'=>'2020-05-15 01:20:06',
'updated_at'=>'2020-06-21 00:08:23',
'deleted_at'=>NULL
] );



Post::create( [
'id'=>4485,
'cat_id'=>12,
'tag_id'=>11,
'title'=>'Is platelet transfusion associated with hospital-acquired infections in critically ill patients',
'text'=>'Critical Care 2017 21:2 6 January 2017',
'img'=>'file-1463591752-XK911T.jpg',
'file'=>NULL,
'link'=>'http://ccforum.biomedcentral.com/articles/10.1186/s13054-016-1593-x',
'by'=>0,
'journal'=>'Cécile Aubron and others',
'type'=>0,
'month_article'=>0,
'famous'=>0,
'views'=>1101,
'show'=>0,
'date'=>'2017-01-17 21:00:00',
'active'=>'1',
'user'=>0,
'created_at'=>'2020-05-15 01:20:06',
'updated_at'=>'2020-06-21 00:08:23',
'deleted_at'=>NULL
] );



Post::create( [
'id'=>4486,
'cat_id'=>8,
'tag_id'=>11,
'title'=>'Effect of perioperative sodium bicarbonate administration on renal function following cardiac surgery for infective endocarditis: a randomized, placebo-controlled trial',
'text'=>'Critical Care 2017 21:3 5 January 2017',
'img'=>'file-1463591738-IZ3XFW.jpg',
'file'=>NULL,
'link'=>'http://ccforum.biomedcentral.com/articles/10.1186/s13054-016-1591-z',
'by'=>0,
'journal'=>'Jin Sun Cho and others',
'type'=>0,
'month_article'=>0,
'famous'=>0,
'views'=>1101,
'show'=>0,
'date'=>'2017-01-05 21:00:00',
'active'=>'1',
'user'=>0,
'created_at'=>'2020-05-15 01:20:06',
'updated_at'=>'2020-06-21 00:08:23',
'deleted_at'=>NULL
] );



Post::create( [
'id'=>4487,
'cat_id'=>8,
'tag_id'=>6,
'title'=>'Effect of driving pressure on mortality in ARDS patients during lung protective mechanical ventilation in two randomized controlled trials',
'text'=>'Critical Care 2016, 20:384 Published: 29 November 2016',
'img'=>'im6.jpg',
'file'=>NULL,
'link'=>'http://ccforum.biomedcentral.com/articles/10.1186/s13054-016-1556-2?utm_campaign=BMC40557B&utm_medium=BMCemail&utm_source=Teradata',
'by'=>0,
'journal'=>'Claude Guérin and others',
'type'=>0,
'month_article'=>0,
'famous'=>0,
'views'=>1101,
'show'=>0,
'date'=>'2016-11-27 21:00:00',
'active'=>'1',
'user'=>0,
'created_at'=>'2020-05-15 01:20:06',
'updated_at'=>'2020-06-21 00:08:23',
'deleted_at'=>NULL
] );



Post::create( [
'id'=>4488,
'cat_id'=>8,
'tag_id'=>11,
'title'=>'Study looks at antibiotic de-escalation with negative MRSA results',
'text'=>'Coverage from the\r\nSociety of Critical Care Medicine SCCM 46th Critical Care Congress',
'img'=>'file-1464165480-R231XR.jpg',
'file'=>NULL,
'link'=>'http://www.medscape.com/viewarticle/874857',
'by'=>0,
'journal'=>'Damian McNamara. Medscape Critical Care, January 24, 2017',
'type'=>0,
'month_article'=>0,
'famous'=>0,
'views'=>1101,
'show'=>0,
'date'=>'2017-01-27 21:00:00',
'active'=>'1',
'user'=>0,
'created_at'=>'2020-05-15 01:20:06',
'updated_at'=>'2020-06-21 00:08:23',
'deleted_at'=>NULL
] );



Post::create( [
'id'=>4489,
'cat_id'=>8,
'tag_id'=>7,
'title'=>'SCCM: Hypothermia No Benefit After Pediatric Cardiac Arrest',
'text'=>'',
'img'=>'file-1464165509-QNPJY8.jpg',
'file'=>NULL,
'link'=>'http://www.physiciansbriefing.com/Article.asp?AID=718924',
'by'=>0,
'journal'=>'WEDNESDAY, Jan. 25, 2017 HealthDay News',
'type'=>0,
'month_article'=>0,
'famous'=>0,
'views'=>1101,
'show'=>0,
'date'=>'2017-01-24 21:00:00',
'active'=>'1',
'user'=>0,
'created_at'=>'2020-05-15 01:20:06',
'updated_at'=>'2020-06-21 00:08:23',
'deleted_at'=>NULL
] );



Post::create( [
'id'=>4491,
'cat_id'=>12,
'tag_id'=>1,
'title'=>'2017 Surviving Sepsis Guidelines',
'text'=>'JAMA. Published online January 19, 2017',
'img'=>'file-1464165480-R231XR.jpg',
'file'=>NULL,
'link'=>'http://jamanetwork.com/journals/jama/fullarticle/2598893',
'by'=>0,
'journal'=>'Daniel De Backer Todd Dorman',
'type'=>0,
'month_article'=>0,
'famous'=>0,
'views'=>1241,
'show'=>0,
'date'=>'2017-01-28 21:00:00',
'active'=>'1',
'user'=>0,
'created_at'=>'2020-05-15 01:20:06',
'updated_at'=>'2020-06-21 00:08:23',
'deleted_at'=>NULL
] );



Post::create( [
'id'=>4492,
'cat_id'=>8,
'tag_id'=>1,
'title'=>'New Guidelines Released to Improve Care of Adults With Sepsis and Septic Shock',
'text'=>'',
'img'=>'file-1463591639-4YG8JY.jpg',
'file'=>NULL,
'link'=>' http://www.anesthesiologynews.com/Web-Only/Article/01-17/New-Guidelines-Released-to-Improve-Care-of-Adults-With-Sepsis-and-Septic-Shock/40156',
'by'=>0,
'journal'=>'Honolulu,  the new guidelines released at the Society of Critical Care Medicines SCCM 46th Critical Care Congress.',
'type'=>0,
'month_article'=>0,
'famous'=>0,
'views'=>1130,
'show'=>0,
'date'=>'2017-01-28 21:00:00',
'active'=>'1',
'user'=>0,
'created_at'=>'2020-05-15 01:20:06',
'updated_at'=>'2020-06-21 00:08:23',
'deleted_at'=>NULL
] );



Post::create( [
'id'=>4493,
'cat_id'=>8,
'tag_id'=>3,
'title'=>'Greater Protein and Energy Intake May Be Associated With Improved Mortality in Higher Risk Critically Ill Patients: A Multicenter, Multinational Observational Study',
'text'=>'Critical Care Medicine . 452:156-163, February 2017.',
'img'=>'file-1463591725-9J88SF.jpg',
'file'=>NULL,
'link'=>'http://journals.lww.com/ccmjournal/Fulltext/2017/02000/Greater_Protein_and_Energy_Intake_May_Be.2.aspx',
'by'=>0,
'journal'=>'Compher, Charlene and others',
'type'=>0,
'month_article'=>0,
'famous'=>0,
'views'=>1101,
'show'=>0,
'date'=>'2017-01-31 21:00:00',
'active'=>'1',
'user'=>0,
'created_at'=>'2020-05-15 01:20:06',
'updated_at'=>'2020-06-21 00:08:23',
'deleted_at'=>NULL
] );



Post::create( [
'id'=>4494,
'cat_id'=>8,
'tag_id'=>7,
'title'=>'Long-Term Survival in Adults Treated With Extracorporeal Membrane Oxygenation for Respiratory Failure and Sepsis',
'text'=>'February 2017 - Volume 45 - Issue 2 - p 164–170',
'img'=>'file-1463591644-ZGN2DA.jpg',
'file'=>NULL,
'link'=>'http://journals.lww.com/ccmjournal/Fulltext/2017/02000/Long_Term_Survival_in_Adults_Treated_With.3.aspx',
'by'=>0,
'journal'=>'von Bahr, Viktor and others',
'type'=>0,
'month_article'=>0,
'famous'=>0,
'views'=>1101,
'show'=>0,
'date'=>'2017-01-31 21:00:00',
'active'=>'1',
'user'=>0,
'created_at'=>'2020-05-15 01:20:06',
'updated_at'=>'2020-06-21 00:08:23',
'deleted_at'=>NULL
] );



Post::create( [
'id'=>4495,
'cat_id'=>8,
'tag_id'=>11,
'title'=>'The importance of diastolic dysfunction in the development of weaning-induced pulmonary oedema',
'text'=>'Critical Care 2017 21:29. Published on: 11 February 2017',
'img'=>'im5.jpg',
'file'=>NULL,
'link'=>'http://ccforum.biomedcentral.com/articles/10.1186/s13054-017-1613-5',
'by'=>0,
'journal'=>'Filippo Sanfilippo, Cristina Santonocito, Gaetano Burgio and Antonio Arcadipane',
'type'=>0,
'month_article'=>0,
'famous'=>0,
'views'=>1101,
'show'=>0,
'date'=>'2017-02-10 21:00:00',
'active'=>'1',
'user'=>0,
'created_at'=>'2020-05-15 01:20:06',
'updated_at'=>'2020-06-21 00:08:23',
'deleted_at'=>NULL
] );



Post::create( [
'id'=>4496,
'cat_id'=>8,
'tag_id'=>1,
'title'=>'Higher Fluid Balance Increases the Risk of Death From Sepsis: Results From a Large International Audit',
'text'=>'Critical Care Medicine . 453:386-394, March 2017',
'img'=>'file-1463591644-ZGN2DA.jpg',
'file'=>NULL,
'link'=>'http://journals.lww.com/ccmjournal/Fulltext/2017/03000/Higher_Fluid_Balance_Increases_the_Risk_of_Death.2.aspx',
'by'=>0,
'journal'=>'Sakr, Yasser and others',
'type'=>0,
'month_article'=>0,
'famous'=>0,
'views'=>1130,
'show'=>0,
'date'=>'2017-03-04 21:00:00',
'active'=>'1',
'user'=>0,
'created_at'=>'2020-05-15 01:20:06',
'updated_at'=>'2020-06-21 00:08:23',
'deleted_at'=>NULL
] );



Post::create( [
'id'=>4497,
'cat_id'=>8,
'tag_id'=>11,
'title'=>'The Changes in Pulse Pressure Variation or Stroke Volume Variation After a “Tidal Volume Challenge” Reliably Predict Fluid Responsiveness During Low Tidal Volume Ventilation',
'text'=>'Critical Care Medicine . 453:415-421, March 2017',
'img'=>'im5.jpg',
'file'=>NULL,
'link'=>'http://journals.lww.com/ccmjournal/Pages/currenttoc.aspx',
'by'=>0,
'journal'=>'Myatra, Sheila Nainan and others',
'type'=>0,
'month_article'=>0,
'famous'=>0,
'views'=>1101,
'show'=>0,
'date'=>'2017-03-04 21:00:00',
'active'=>'1',
'user'=>0,
'created_at'=>'2020-05-15 01:20:06',
'updated_at'=>'2020-06-21 00:08:23',
'deleted_at'=>NULL
] );



Post::create( [
'id'=>4498,
'cat_id'=>8,
'tag_id'=>1,
'title'=>'Prior Use of Calcium Channel Blockers Is Associated With Decreased Mortality in Critically Ill Patients With Sepsis: A Prospective Observational Study',
'text'=>'Critical Care Medicine . 453:454-463, March 2017',
'img'=>'file-1464165463-D3CBM9.jpg',
'file'=>NULL,
'link'=>'http://journals.lww.com/ccmjournal/Fulltext/2017/03000/Prior_Use_of_Calcium_Channel_Blockers_Is.10.aspx',
'by'=>0,
'journal'=>'Wiewel, Maryse A. and others',
'type'=>0,
'month_article'=>0,
'famous'=>0,
'views'=>1130,
'show'=>0,
'date'=>'2017-03-04 21:00:00',
'active'=>'1',
'user'=>0,
'created_at'=>'2020-05-15 01:20:06',
'updated_at'=>'2020-06-21 00:08:23',
'deleted_at'=>NULL
] );



Post::create( [
'id'=>4499,
'cat_id'=>8,
'tag_id'=>5,
'title'=>'Prophylaxis of Venous Thrombosis in Neurocritical Care Patients: An Executive Summary of Evidence-Based Guidelines: A Statement for Healthcare Professionals From the Neurocritical Care Society and Society of Critical Care Medicine',
'text'=>'Critical Care Medicine . 453:476-479, March 2017',
'img'=>'im1.jpg',
'file'=>NULL,
'link'=>'http://journals.lww.com/ccmjournal/Citation/2017/03000/Prophylaxis_of_Venous_Thrombosis_in_Neurocritical.13.aspx',
'by'=>0,
'journal'=>'Nyquist, Paul and others',
'type'=>0,
'month_article'=>0,
'famous'=>0,
'views'=>1114,
'show'=>0,
'date'=>'2017-03-04 21:00:00',
'active'=>'1',
'user'=>0,
'created_at'=>'2020-05-15 01:20:06',
'updated_at'=>'2020-06-21 00:08:23',
'deleted_at'=>NULL
] );



Post::create( [
'id'=>4500,
'cat_id'=>8,
'tag_id'=>1,
'title'=>'A Users Guide to the 2016 Surviving Sepsis Guidelines',
'text'=>'Critical Care Medicine . 453:381-385, March 2017',
'img'=>'file-1463591639-4YG8JY.jpg',
'file'=>NULL,
'link'=>'http://journals.lww.com/ccmjournal/Fulltext/2017/03000/A_Users__Guide_to_the_2016_Surviving_Sepsis.1.aspx',
'by'=>0,
'journal'=>'Dellinger, R. Phillip Schorr, Christa A. Levy, Mitchell M.',
'type'=>0,
'month_article'=>0,
'famous'=>0,
'views'=>1130,
'show'=>0,
'date'=>'2017-03-04 21:00:00',
'active'=>'1',
'user'=>0,
'created_at'=>'2020-05-15 01:20:06',
'updated_at'=>'2020-06-21 00:08:23',
'deleted_at'=>NULL
] );



Post::create( [
'id'=>4501,
'cat_id'=>8,
'tag_id'=>6,
'title'=>'Effects of neuromuscular blockers on transpulmonary pressures in moderate to severe acute respiratory distress syndrome',
'text'=>'Intensive Care Med. March  2017 43 3: 408 - 418',
'img'=>'file-1463591752-XK911T.jpg',
'file'=>NULL,
'link'=>'http://icmjournal.esicm.org/journals/abstract.html?v=43&j=134&i=3&a=4653_10.1007_s00134-016-4653-4&doi=',
'by'=>0,
'journal'=>'Christophe Guervilly and others',
'type'=>0,
'month_article'=>0,
'famous'=>0,
'views'=>1101,
'show'=>0,
'date'=>'2017-03-04 21:00:00',
'active'=>'1',
'user'=>0,
'created_at'=>'2020-05-15 01:20:06',
'updated_at'=>'2020-06-21 00:08:23',
'deleted_at'=>NULL
] );



Post::create( [
'id'=>4502,
'cat_id'=>8,
'tag_id'=>11,
'title'=>'Venous thromboembolic events in critically ill traumatic brain injury patients',
'text'=>'Intensive Care Med. March  2017 43 3: 419 - 428',
'img'=>'file-1463591639-4YG8JY.jpg',
'file'=>NULL,
'link'=>'http://icmjournal.esicm.org/journals/abstract.html?v=43&j=134&i=3&a=4655_10.1007_s00134-016-4655-2&doi=',
'by'=>0,
'journal'=>'Markus B. Skrifvars and others',
'type'=>0,
'month_article'=>0,
'famous'=>0,
'views'=>1101,
'show'=>0,
'date'=>'2017-03-04 21:00:00',
'active'=>'1',
'user'=>0,
'created_at'=>'2020-05-15 01:20:06',
'updated_at'=>'2020-06-21 00:08:23',
'deleted_at'=>NULL
] );



Post::create( [
'id'=>4503,
'cat_id'=>8,
'tag_id'=>11,
'title'=>'Intensive care medicine in 2050: NEWS for hemodynamic monitoring',
'text'=>'Intensive Care Med. March  2017 43 3: 440 - 442',
'img'=>'file-1463591644-ZGN2DA.jpg',
'file'=>NULL,
'link'=>'http://icmjournal.esicm.org/journals/abstract.html?v=43&j=134&i=3&a=4674_10.1007_s00134-016-4674-z&doi=',
'by'=>0,
'journal'=>'Frederic Michard, Michael R. Pinsky, Jean-Louis Vincent',
'type'=>0,
'month_article'=>0,
'famous'=>0,
'views'=>1101,
'show'=>0,
'date'=>'2017-03-04 21:00:00',
'active'=>'1',
'user'=>0,
'created_at'=>'2020-05-15 01:20:06',
'updated_at'=>'2020-06-21 00:08:23',
'deleted_at'=>NULL
] );



Post::create( [
'id'=>4504,
'cat_id'=>8,
'tag_id'=>10,
'title'=>'Antithrombotic Drugs and Risk of Subdural Hematoma',
'text'=>'JAMA.  March 20173178:836-846',
'img'=>'file-1463591639-4YG8JY.jpg',
'file'=>NULL,
'link'=>'http://jamanetwork.com/journals/jama/article-abstract/2605799',
'by'=>0,
'journal'=>'David Gaist, MD, PhD et al',
'type'=>0,
'month_article'=>0,
'famous'=>0,
'views'=>1101,
'show'=>0,
'date'=>'2017-03-04 21:00:00',
'active'=>'1',
'user'=>0,
'created_at'=>'2020-05-15 01:20:06',
'updated_at'=>'2020-06-21 00:08:23',
'deleted_at'=>NULL
] );



Post::create( [
'id'=>4505,
'cat_id'=>8,
'tag_id'=>11,
'title'=>'Ultrasound assessment of volume responsiveness in critically ill surgical patients: Two measurements are better than one',
'text'=>'Journal of Trauma and Acute Care Surgery . 823:505-511, March 2017',
'img'=>'im4.jpg',
'file'=>NULL,
'link'=>'http://journals.lww.com/jtrauma/pages/default.aspx',
'by'=>0,
'journal'=>'Murthi, Sarah B and others',
'type'=>0,
'month_article'=>0,
'famous'=>0,
'views'=>1101,
'show'=>0,
'date'=>'2017-03-04 21:00:00',
'active'=>'1',
'user'=>0,
'created_at'=>'2020-05-15 01:20:06',
'updated_at'=>'2020-06-21 00:08:23',
'deleted_at'=>NULL
] );



Post::create( [
'id'=>4506,
'cat_id'=>8,
'tag_id'=>4,
'title'=>'Damage control resuscitation in patients with severe traumatic hemorrhage: A practice management guideline from the Eastern Association for the Surgery of Trauma',
'text'=>'Journal of Trauma and Acute Care Surgery . 823:605-617, March 2017',
'img'=>'im4.jpg',
'file'=>NULL,
'link'=>'http://journals.lww.com/jtrauma/Fulltext/2017/03000/Damage_control_resuscitation_in_patients_with.24.aspx',
'by'=>0,
'journal'=>'Cannon, Jeremy W and others',
'type'=>0,
'month_article'=>0,
'famous'=>0,
'views'=>1198,
'show'=>0,
'date'=>'2017-03-04 21:00:00',
'active'=>'1',
'user'=>0,
'created_at'=>'2020-05-15 01:20:06',
'updated_at'=>'2020-06-21 00:08:23',
'deleted_at'=>NULL
] );



Post::create( [
'id'=>4507,
'cat_id'=>12,
'tag_id'=>3,
'title'=>'Early enteral nutrition in critically ill patients: ESICM clinical practice guidelines',
'text'=>'Intensive Care Med. March  2017 43 3: 380 - 398',
'img'=>'file-1463591725-9J88SF.jpg',
'file'=>NULL,
'link'=>'http://icmjournal.esicm.org/journals/abstract.html?v=43&j=134&i=3&a=4665_10.1007_s00134-016-4665-0&doi=',
'by'=>0,
'journal'=>'Annika Reintam Blaser and others',
'type'=>0,
'month_article'=>0,
'famous'=>0,
'views'=>1212,
'show'=>0,
'date'=>'2017-03-04 21:00:00',
'active'=>'1',
'user'=>0,
'created_at'=>'2020-05-15 01:20:06',
'updated_at'=>'2020-06-21 00:08:23',
'deleted_at'=>NULL
] );



Post::create( [
'id'=>4508,
'cat_id'=>8,
'tag_id'=>10,
'title'=>'Reduced-Intensity Rivaroxaban for the Prevention of Recurrent Venous Thromboembolism',
'text'=>'N Eng J Med.  March 18, 2017',
'img'=>'file-1463591681-AGRM7H.jpg',
'file'=>NULL,
'link'=>'http://www.nejm.org/doi/full/10.1056/NEJMe1701628?query=OF',
'by'=>0,
'journal'=>'M.A. Crowther and A. Cuker',
'type'=>0,
'month_article'=>0,
'famous'=>0,
'views'=>1101,
'show'=>0,
'date'=>'2017-03-19 22:00:00',
'active'=>'1',
'user'=>0,
'created_at'=>'2020-05-15 01:20:06',
'updated_at'=>'2020-06-21 00:08:23',
'deleted_at'=>NULL
] );



Post::create( [
'id'=>4509,
'cat_id'=>8,
'tag_id'=>7,
'title'=>'Levosimendan in Patients with Left Ventricular Dysfunction Undergoing Cardiac Surgery',
'text'=>'N Eng J Med. March 19, 2017',
'img'=>'file-1463591639-4YG8JY.jpg',
'file'=>NULL,
'link'=>'http://www.nejm.org/doi/full/10.1056/NEJMoa1616218?query=OF',
'by'=>0,
'journal'=>'R.H. Mehta and Others',
'type'=>0,
'month_article'=>0,
'famous'=>0,
'views'=>1101,
'show'=>0,
'date'=>'2017-03-19 22:00:00',
'active'=>'1',
'user'=>0,
'created_at'=>'2020-05-15 01:20:06',
'updated_at'=>'2020-06-21 00:08:23',
'deleted_at'=>NULL
] );



Post::create( [
'id'=>4510,
'cat_id'=>12,
'tag_id'=>1,
'title'=>'Early, Goal-Directed Therapy for Septic Shock — A Patient-Level Meta-Analysis',
'text'=>'N Engl J Med. March 21, 2017',
'img'=>'file-1463591657-K8BRDF.jpg',
'file'=>NULL,
'link'=>'http://www.nejm.org/doi/full/10.1056/NEJMoa1701380?query=featured_home',
'by'=>0,
'journal'=>'The PRISM Investigators',
'type'=>0,
'month_article'=>0,
'famous'=>0,
'views'=>1241,
'show'=>0,
'date'=>'2017-03-20 22:00:00',
'active'=>'1',
'user'=>0,
'created_at'=>'2020-05-15 01:20:06',
'updated_at'=>'2020-06-21 00:08:23',
'deleted_at'=>NULL
] );



Post::create( [
'id'=>4511,
'cat_id'=>8,
'tag_id'=>11,
'title'=>'Dabigatran versus Warfarin for Ablation in AF',
'text'=>'N Engl J Med. March 19, 2017',
'img'=>'file-1463591644-ZGN2DA.jpg',
'file'=>NULL,
'link'=>'http://www.nejm.org/doi/full/10.1056/NEJMoa1701005?query=featured_home',
'by'=>0,
'journal'=>'H. Calkins and Other',
'type'=>0,
'month_article'=>0,
'famous'=>0,
'views'=>1101,
'show'=>0,
'date'=>'2017-03-18 22:00:00',
'active'=>'1',
'user'=>0,
'created_at'=>'2020-05-15 01:20:06',
'updated_at'=>'2020-06-21 00:08:23',
'deleted_at'=>NULL
] );



Post::create( [
'id'=>4512,
'cat_id'=>12,
'tag_id'=>7,
'title'=>'Levosimendan after Cardiac Surgery',
'text'=>'N Engl J Med. March 21, 2017',
'img'=>'file-1463591639-4YG8JY.jpg',
'file'=>NULL,
'link'=>'http://www.nejm.org/doi/full/10.1056/NEJMoa1616325queryfeatured_home',
'by'=>0,
'journal'=>'G. Landoni and Others. CHEETAH Study Group',
'type'=>0,
'month_article'=>0,
'famous'=>0,
'views'=>1212,
'show'=>0,
'date'=>'2017-03-20 22:00:00',
'active'=>'1',
'user'=>0,
'created_at'=>'2020-05-15 01:20:06',
'updated_at'=>'2020-06-21 00:08:23',
'deleted_at'=>NULL
] );



Post::create( [
'id'=>4513,
'cat_id'=>8,
'tag_id'=>11,
'title'=>'Levosimendan in Patients with LV Dysfunction Undergoing Cardiac Surgery',
'text'=>'N Engl J M.',
'img'=>'im3.jpg',
'file'=>NULL,
'link'=>'http://www.nejm.org/doi/full/10.1056/NEJMoa1616218?query=featured_home',
'by'=>0,
'journal'=>'R.H. Mehta and Others, for the LEVO-CTS Investigators',
'type'=>0,
'month_article'=>0,
'famous'=>1,
'views'=>1196,
'show'=>0,
'date'=>'2017-03-20 22:00:00',
'active'=>'1',
'user'=>0,
'created_at'=>'2020-05-15 01:20:06',
'updated_at'=>'2020-06-21 00:08:23',
'deleted_at'=>NULL
] );



Post::create( [
'id'=>4514,
'cat_id'=>12,
'tag_id'=>11,
'title'=>'Effect of Dexmedetomidine on Mortality and Ventilator-Free Days in Patients Requiring Mechanical Ventilation With Sepsis',
'text'=>'JAMA. Published online March 21, 2017',
'img'=>'im2.jpg',
'file'=>NULL,
'link'=>'http://jamanetwork.com/journals/jama/fullarticle/2612911',
'by'=>0,
'journal'=>'Yu Kawazoe and others',
'type'=>0,
'month_article'=>0,
'famous'=>1,
'views'=>1307,
'show'=>0,
'date'=>'2017-03-20 22:00:00',
'active'=>'1',
'user'=>0,
'created_at'=>'2020-05-15 01:20:06',
'updated_at'=>'2020-06-21 00:08:23',
'deleted_at'=>NULL
] );



Post::create( [
'id'=>4515,
'cat_id'=>8,
'tag_id'=>11,
'title'=>'Optimizing the Settings on the Ventilator SettingsHigh PEEP for All',
'text'=>'JAMA. Published online March 21, 2017',
'img'=>'im6.jpg',
'file'=>NULL,
'link'=>'http://jamanetwork.com/journals/jama/fullarticle/2612909',
'by'=>0,
'journal'=>'Ary Serpa Neto  Marcus J. Schultz',
'type'=>0,
'month_article'=>0,
'famous'=>0,
'views'=>1101,
'show'=>0,
'date'=>'2017-03-20 22:00:00',
'active'=>'1',
'user'=>0,
'created_at'=>'2020-05-15 01:20:06',
'updated_at'=>'2020-06-21 00:08:23',
'deleted_at'=>NULL
] );



Post::create( [
'id'=>4516,
'cat_id'=>8,
'tag_id'=>7,
'title'=>'Association Between US Norepinephrine Shortage and Mortality Among Patients With Septic Shock',
'text'=>'JAMA. Published online March 21, 2017',
'img'=>'im5.jpg',
'file'=>NULL,
'link'=>'http://jamanetwork.com/journals/jama/fullarticle/2612912',
'by'=>0,
'journal'=>'Emily Vail and others',
'type'=>0,
'month_article'=>0,
'famous'=>0,
'views'=>1101,
'show'=>0,
'date'=>'2017-03-20 22:00:00',
'active'=>'1',
'user'=>0,
'created_at'=>'2020-05-15 01:20:06',
'updated_at'=>'2020-06-21 00:08:23',
'deleted_at'=>NULL
] );



Post::create( [
'id'=>4517,
'cat_id'=>12,
'tag_id'=>6,
'title'=>'Effect of Intensive vs Moderate Alveolar Recruitment Strategies Added to Lung-Protective Ventilation on Postoperative Pulmonary Complications, A Randomized Clinical Trial',
'text'=>'JAMA. Published online March 21, 2017',
'img'=>'im4.jpg',
'file'=>NULL,
'link'=>'http://jamanetwork.com/journals/jama/fullarticle/2612913',
'by'=>0,
'journal'=>'Alcino Costa Leme and others',
'type'=>0,
'month_article'=>0,
'famous'=>0,
'views'=>1212,
'show'=>0,
'date'=>'2017-03-20 22:00:00',
'active'=>'1',
'user'=>0,
'created_at'=>'2020-05-15 01:20:06',
'updated_at'=>'2020-06-21 00:08:23',
'deleted_at'=>NULL
] );



Post::create( [
'id'=>4518,
'cat_id'=>8,
'tag_id'=>11,
'title'=>'Effect of Hydrocortisone on Development of Shock Among Patients With Severe Sepsis. The HYPRESS Randomized Clinical Trial',
'text'=>'JAMA. 201631617:1775-1785. November 1, 2016',
'img'=>'file-1464165463-D3CBM9.jpg',
'file'=>NULL,
'link'=>'http://jamanetwork.com/journals/jama/article-abstract/2565176?widget=personalizedcontent&previousarticle=2565175',
'by'=>0,
'journal'=>'Didier Keh and others',
'type'=>0,
'month_article'=>0,
'famous'=>1,
'views'=>1196,
'show'=>0,
'date'=>'2016-10-31 22:00:00',
'active'=>'1',
'user'=>0,
'created_at'=>'2020-05-15 01:20:06',
'updated_at'=>'2020-06-21 00:08:23',
'deleted_at'=>NULL
] );



Post::create( [
'id'=>4519,
'cat_id'=>8,
'tag_id'=>11,
'title'=>'Anemia and blood transfusion in the critically ill patient with cardiovascular disease',
'text'=>'Critical Care 2017 21:61. Published on: 21 March 2017',
'img'=>'file-1463591685-DDLMK9.jpg',
'file'=>NULL,
'link'=>'http://ccforum.biomedcentral.com/articles/10.1186/s13054-017-1638-9',
'by'=>0,
'journal'=>'Annemarie B. Docherty and Timothy S. Walsh',
'type'=>0,
'month_article'=>0,
'famous'=>0,
'views'=>1101,
'show'=>0,
'date'=>'2017-03-20 22:00:00',
'active'=>'1',
'user'=>0,
'created_at'=>'2020-05-15 01:20:06',
'updated_at'=>'2020-06-21 00:08:23',
'deleted_at'=>NULL
] );


Post::create( [
'id'=>4521,
'cat_id'=>8,
'tag_id'=>11,
'title'=>'Antiarrhythmic drugs for out-of-hospital cardiac arrest with refractory ventricular fibrillation',
'text'=>'Critical Care201721:59. Published: 21 March 2017',
'img'=>'file-1463591639-4YG8JY.jpg',
'file'=>NULL,
'link'=>'http://ccforum.biomedcentral.com/articles/10.1186/s13054-017-1639-8',
'by'=>0,
'journal'=>'Takashi Tagami, Hideo Yasunaga and Hiroyuki Yokota',
'type'=>0,
'month_article'=>0,
'famous'=>0,
'views'=>1101,
'show'=>0,
'date'=>'2017-03-20 22:00:00',
'active'=>'1',
'user'=>0,
'created_at'=>'2020-05-15 01:20:06',
'updated_at'=>'2020-06-21 00:08:23',
'deleted_at'=>NULL
] );



Post::create( [
'id'=>4522,
'cat_id'=>8,
'tag_id'=>7,
'title'=>'Levosimendan for the Prevention of Acute Organ Dysfunction in Sepsis LEOPARDS Trial',
'text'=>'N Engl J Med 2016 375:1638-1648. October 27, 2016',
'img'=>'file-1463591639-4YG8JY.jpg',
'file'=>NULL,
'link'=>'http://www.nejm.org/doi/full/10.1056/NEJMoa1609409',
'by'=>0,
'journal'=>'Anthony C. Gordon and others',
'type'=>0,
'month_article'=>0,
'famous'=>1,
'views'=>1196,
'show'=>0,
'date'=>'2016-10-26 22:00:00',
'active'=>'1',
'user'=>0,
'created_at'=>'2020-05-15 01:20:06',
'updated_at'=>'2020-06-21 00:08:23',
'deleted_at'=>NULL
] );



Post::create( [
'id'=>4523,
'cat_id'=>20,
'tag_id'=>4,
'title'=>'RETIC Trial: Reversal of Trauma Induced Coagulopathy Using Coagulation Factor Concentrates or Fresh Frozen Plasma',
'text'=>'ClinicalTrials.gov processed this record on March 20, 2017',
'img'=>'file-1464165480-R231XR.jpg',
'file'=>NULL,
'link'=>'https://clinicaltrials.gov/ct2/show/NCT01545635',
'by'=>0,
'journal'=>'Univ.-Doz. Dr. Petra Innerhofer, Medical University Innsbruck',
'type'=>0,
'month_article'=>0,
'famous'=>0,
'views'=>1198,
'show'=>0,
'date'=>'2017-03-20 22:00:00',
'active'=>'1',
'user'=>0,
'created_at'=>'2020-05-15 01:20:06',
'updated_at'=>'2020-06-21 00:08:23',
'deleted_at'=>NULL
] );



Post::create( [
'id'=>4524,
'cat_id'=>20,
'tag_id'=>4,
'title'=>'Fibrinogen Concentrate FGTW in Trauma Patients, Presumed to Bleed FI in TIC',
'text'=>'ClinicalTrials.gov processed this record on March 20, 2017',
'img'=>'im4.jpg',
'file'=>NULL,
'link'=>'https://clinicaltrials.gov/ct2/show/NCT01475344',
'by'=>0,
'journal'=>'Dietmar Fries, M.D., Medical University Innsbruck',
'type'=>0,
'month_article'=>0,
'famous'=>0,
'views'=>1198,
'show'=>0,
'date'=>'2017-03-19 22:00:00',
'active'=>'1',
'user'=>0,
'created_at'=>'2020-05-15 01:20:06',
'updated_at'=>'2020-06-21 00:08:23',
'deleted_at'=>NULL
] );



Post::create( [
'id'=>4525,
'cat_id'=>8,
'tag_id'=>11,
'title'=>'Study: Aggressive treatment may not improve septic shock outcomes',
'text'=>'March 23, 2017',
'img'=>'file-1463591639-4YG8JY.jpg',
'file'=>NULL,
'link'=>'http://www.medpagetoday.com/criticalcare/sepsis/64079',
'by'=>0,
'journal'=>'MedPage Today',
'type'=>0,
'month_article'=>0,
'famous'=>0,
'views'=>1101,
'show'=>0,
'date'=>'2017-03-22 22:00:00',
'active'=>'1',
'user'=>0,
'created_at'=>'2020-05-15 01:20:06',
'updated_at'=>'2020-06-21 00:08:23',
'deleted_at'=>NULL
] );



Post::create( [
'id'=>4526,
'cat_id'=>8,
'tag_id'=>1,
'title'=>'Enterobacteriaceae bacteraemia raises daily risk of death in ICU',
'text'=>'March 21, 2107',
'img'=>'file-1464165463-D3CBM9.jpg',
'file'=>NULL,
'link'=>'http://www.beckershospitalreview.com/quality/antibiotic-therapy-may-not-alter-enterobacteriaceae-bacteraemia-associated-mortality-in-icus-4-insights.html',
'by'=>0,
'journal'=>'Beckers Hospital Review.com',
'type'=>0,
'month_article'=>0,
'famous'=>0,
'views'=>1130,
'show'=>0,
'date'=>'2017-03-24 22:00:00',
'active'=>'1',
'user'=>0,
'created_at'=>'2020-05-15 01:20:06',
'updated_at'=>'2020-06-21 00:08:23',
'deleted_at'=>NULL
] );



Post::create( [
'id'=>4527,
'cat_id'=>8,
'tag_id'=>5,
'title'=>'Meta-Analysis of Therapeutic Hypothermia for Traumatic Brain Injury in Adult and Pediatric Patients',
'text'=>'Critical Care Medicine . 454:575-583, April 2017',
'img'=>'im1.jpg',
'file'=>NULL,
'link'=>'http://journals.lww.com/ccmjournal/Fulltext/2017/04000/Meta_Analysis_of_Therapeutic_Hypothermia_for.2.aspx',
'by'=>0,
'journal'=>'Crompton, Ellie M and others',
'type'=>0,
'month_article'=>0,
'famous'=>0,
'views'=>1114,
'show'=>0,
'date'=>'2017-03-31 22:00:00',
'active'=>'1',
'user'=>0,
'created_at'=>'2020-05-15 01:20:06',
'updated_at'=>'2020-06-21 00:08:23',
'deleted_at'=>NULL
] );



Post::create( [
'id'=>4528,
'cat_id'=>8,
'tag_id'=>11,
'title'=>'Bedside Glucose Monitoring—Is it Safe A New, Regulatory-Compliant Risk Assessment Evaluation Protocol in Critically Ill Patient Care Settings',
'text'=>'Critical Care Medicine . 454:567-574, April 2017',
'img'=>'file-1464106363-HWQX6B.jpg',
'file'=>NULL,
'link'=>'http://journals.lww.com/ccmjournal/Fulltext/2017/04000/Bedside_Glucose_Monitoring_Is_it_Safe__A_New,.1.aspx',
'by'=>0,
'journal'=>'DuBois, Jeffrey Anton and others',
'type'=>0,
'month_article'=>0,
'famous'=>0,
'views'=>1101,
'show'=>0,
'date'=>'2017-03-31 22:00:00',
'active'=>'1',
'user'=>0,
'created_at'=>'2020-05-15 01:20:06',
'updated_at'=>'2020-06-21 00:08:23',
'deleted_at'=>NULL
] );



Post::create( [
'id'=>4529,
'cat_id'=>8,
'tag_id'=>1,
'title'=>'Fever in the Emergency Department Predicts Survival of Patients With Severe Sepsis and Septic Shock Admitted to the ICU',
'text'=>'Critical Care Medicine . 454:591-599, April 2017',
'img'=>'file-1464165463-D3CBM9.jpg',
'file'=>NULL,
'link'=>'http://journals.lww.com/ccmjournal/Abstract/2017/04000/Fever_in_the_Emergency_Department_Predicts.4.aspx',
'by'=>0,
'journal'=>'Sundén-Cullberg and others',
'type'=>0,
'month_article'=>0,
'famous'=>0,
'views'=>1130,
'show'=>0,
'date'=>'2017-03-31 22:00:00',
'active'=>'1',
'user'=>0,
'created_at'=>'2020-05-15 01:20:06',
'updated_at'=>'2020-06-21 00:08:23',
'deleted_at'=>NULL
] );



Post::create( [
'id'=>4530,
'cat_id'=>8,
'tag_id'=>1,
'title'=>'Increased Time to Initial Antimicrobial Administration Is Associated With Progression to Septic Shock in Severe Sepsis Patients',
'text'=>'Critical Care Medicine . 454:623-629, April 2017',
'img'=>'file-1464165480-R231XR.jpg',
'file'=>NULL,
'link'=>'http://journals.lww.com/ccmjournal/Abstract/2017/04000/Increased_Time_to_Initial_Antimicrobial.8.aspx',
'by'=>0,
'journal'=>'Whiles, Bristol B and others',
'type'=>0,
'month_article'=>0,
'famous'=>0,
'views'=>1130,
'show'=>0,
'date'=>'2017-03-31 22:00:00',
'active'=>'1',
'user'=>0,
'created_at'=>'2020-05-15 01:20:06',
'updated_at'=>'2020-06-21 00:08:23',
'deleted_at'=>NULL
] );



Post::create( [
'id'=>4531,
'cat_id'=>8,
'tag_id'=>8,
'title'=>'Sodium Bicarbonate Versus Sodium Chloride for Preventing Contrast-Associated Acute Kidney Injury in Critically Ill Patients: A Randomized Controlled Trial',
'text'=>'Critical Care Medicine . 454:637-644, April 2017',
'img'=>'file-1463591738-IZ3XFW.jpg',
'file'=>NULL,
'link'=>'http://journals.lww.com/ccmjournal/Abstract/2017/04000/Sodium_Bicarbonate_Versus_Sodium_Chloride_for.10.aspx',
'by'=>0,
'journal'=>'Valette, Xavier and others',
'type'=>0,
'month_article'=>0,
'famous'=>0,
'views'=>1152,
'show'=>0,
'date'=>'2017-03-31 22:00:00',
'active'=>'1',
'user'=>0,
'created_at'=>'2020-05-15 01:20:06',
'updated_at'=>'2020-06-21 00:08:23',
'deleted_at'=>NULL
] );



Post::create( [
'id'=>4532,
'cat_id'=>8,
'tag_id'=>5,
'title'=>'RBC Transfusion Improves Cerebral Oxygen Delivery in Subarachnoid Hemorrhage',
'text'=>'Critical Care Medicine . 454:653-659, April 2017',
'img'=>'file-1463591685-DDLMK9.jpg',
'file'=>NULL,
'link'=>'http://journals.lww.com/ccmjournal/Abstract/2017/04000/RBC_Transfusion_Improves_Cerebral_Oxygen_Delivery.12.aspx',
'by'=>0,
'journal'=>'Dhar, Rajat and others',
'type'=>0,
'month_article'=>0,
'famous'=>0,
'views'=>1114,
'show'=>0,
'date'=>'2017-03-31 22:00:00',
'active'=>'1',
'user'=>0,
'created_at'=>'2020-05-15 01:20:06',
'updated_at'=>'2020-06-21 00:08:23',
'deleted_at'=>NULL
] );



Post::create( [
'id'=>4533,
'cat_id'=>8,
'tag_id'=>7,
'title'=>'Monitoring dynamic arterial elastance as a means of decreasing the duration of norepinephrine treatment in vasoplegic syndrome following cardiac surgery: a prospective, randomized trial',
'text'=>'Intensive Care Med. J. 435 643 - 65 643 - 651: May 2017',
'img'=>'file-1463591639-4YG8JY.jpg',
'file'=>NULL,
'link'=>'http://icmjournal.esicm.org/journals/abstract.htmlv43&j134&i5&a4666_10.1007_s00134-016-4666-z&doi',
'by'=>0,
'journal'=>'Pierre-Grégoire Guinot and others',
'type'=>0,
'month_article'=>0,
'famous'=>0,
'views'=>1101,
'show'=>0,
'date'=>'2017-04-30 22:00:00',
'active'=>'1',
'user'=>0,
'created_at'=>'2020-05-15 01:20:06',
'updated_at'=>'2020-06-21 00:08:23',
'deleted_at'=>NULL
] );



Post::create( [
'id'=>4534,
'cat_id'=>8,
'tag_id'=>11,
'title'=>'Opening pressures and atelectrauma in acute respiratory distress syndrome',
'text'=>'Intensive Care Med. J. 435 603 - 611: May 2017',
'img'=>'file-1463591639-4YG8JY.jpg',
'file'=>NULL,
'link'=>'http://icmjournal.esicm.org/journals/abstract.htmlv43&j134&i5&a4754_10.1007_s00134-017-4754-8&doi',
'by'=>0,
'journal'=>'Massimo Cressoni and others',
'type'=>0,
'month_article'=>0,
'famous'=>0,
'views'=>1101,
'show'=>0,
'date'=>'2017-04-30 22:00:00',
'active'=>'1',
'user'=>0,
'created_at'=>'2020-05-15 01:20:06',
'updated_at'=>'2020-06-21 00:08:23',
'deleted_at'=>NULL
] );



Post::create( [
'id'=>4535,
'cat_id'=>8,
'tag_id'=>11,
'title'=>'Effect of a condolence letter on grief symptoms among relatives of patients who died in the ICU: a randomized clinical trial',
'text'=>'Intensive Care Med. J. 43, 4 473 - 484, April 2017',
'img'=>'file-1464165480-R231XR.jpg',
'file'=>NULL,
'link'=>'http://icmjournal.esicm.org/journals/abstract.htmlv43&j134&i4&a4669_10.1007_s00134-016-4669-9&doi',
'by'=>0,
'journal'=>'Nancy Kentish-Barnes and others',
'type'=>0,
'month_article'=>0,
'famous'=>0,
'views'=>1101,
'show'=>0,
'date'=>'2017-04-08 22:00:00',
'active'=>'1',
'user'=>0,
'created_at'=>'2020-05-15 01:20:06',
'updated_at'=>'2020-06-21 00:08:23',
'deleted_at'=>NULL
] );



Post::create( [
'id'=>4536,
'cat_id'=>8,
'tag_id'=>7,
'title'=>'Targeted temperature management after intraoperative cardiac arrest: a multicenter retrospective study',
'text'=>'Intensive Care Med. J. 43, 4 473 - 484, April 2017',
'img'=>'file-1463591639-4YG8JY.jpg',
'file'=>NULL,
'link'=>'http://icmjournal.esicm.org/journals/abstract.htmlv43&j134&i4&a4709_10.1007_s00134-017-4709-0&doi',
'by'=>0,
'journal'=>'Anne-Laure Constant and others',
'type'=>0,
'month_article'=>0,
'famous'=>0,
'views'=>1101,
'show'=>0,
'date'=>'2017-04-08 22:00:00',
'active'=>'1',
'user'=>0,
'created_at'=>'2020-05-15 01:20:06',
'updated_at'=>'2020-06-21 00:08:23',
'deleted_at'=>NULL
] );



Post::create( [
'id'=>4537,
'cat_id'=>8,
'tag_id'=>6,
'title'=>'Extracorporeal carbon dioxide removal ECCO2R in patients with acute respiratory failure',
'text'=>'Intensive Care Med. J. 43, 4 473 - 484, April 2017',
'img'=>'im6.jpg',
'file'=>NULL,
'link'=>'http://icmjournal.esicm.org/journals/abstract.html?v=43&j=134&i=4&a=4673_10.1007_s00134-016-4673-0&doi=',
'by'=>0,
'journal'=>'Andrea Morelli and others',
'type'=>0,
'month_article'=>0,
'famous'=>0,
'views'=>1101,
'show'=>0,
'date'=>'2017-04-08 22:00:00',
'active'=>'1',
'user'=>0,
'created_at'=>'2020-05-15 01:20:06',
'updated_at'=>'2020-06-21 00:08:23',
'deleted_at'=>NULL
] );



Post::create( [
'id'=>4538,
'cat_id'=>12,
'tag_id'=>6,
'title'=>'COPD: new European and US guidelines on managing exacerbations',
'text'=>'Eur Respir J 2017 49: 1600791',
'img'=>'im6.jpg',
'file'=>NULL,
'link'=>'https://www.thoracic.org/statements/resources/copd/mgmt-of-COPD-exacerbations.pdf',
'by'=>0,
'journal'=>'Jadwiga A. Wedzicha ERS co-chair and others',
'type'=>0,
'month_article'=>0,
'famous'=>0,
'views'=>1212,
'show'=>0,
'date'=>'2017-04-08 22:00:00',
'active'=>'1',
'user'=>0,
'created_at'=>'2020-05-15 01:20:06',
'updated_at'=>'2020-06-21 00:08:23',
'deleted_at'=>NULL
] );



Post::create( [
'id'=>4539,
'cat_id'=>8,
'tag_id'=>6,
'title'=>'COPD: new European and US guidelines on managing exacerbations',
'text'=>'',
'img'=>'im5.jpg',
'file'=>NULL,
'link'=>'http://www.univadis.md/medical-news/596/COPD-new-European-and-US-guidelines-on-managing-exacerbationsutm_sourcenewsletteremail&utm_mediumemail&utm_campaignmedicalupdatesbestof-weekly&utm_content1351421&utm_termautomated_bestof_weekly',
'by'=>0,
'journal'=>'UnivadisThursday 06 April 2017',
'type'=>0,
'month_article'=>0,
'famous'=>0,
'views'=>1101,
'show'=>0,
'date'=>'2017-04-08 22:00:00',
'active'=>'1',
'user'=>0,
'created_at'=>'2020-05-15 01:20:06',
'updated_at'=>'2020-06-21 00:08:23',
'deleted_at'=>NULL
] );



Post::create( [
'id'=>4540,
'cat_id'=>8,
'tag_id'=>1,
'title'=>'A. baumannii colonization in ICU increases mortality risks',
'text'=>'Healio Infectious Disease News',
'img'=>'file-1464106363-HWQX6B.jpg',
'file'=>NULL,
'link'=>'http://www.healio.com/infectious-disease/emerging-diseases/news/online/%7Bb8e4e672-f45d-404e-aed6-f10f6e22e96f%7D/a-baumannii-colonization-upon-icu-admission-increases-likelihood-of-infection-mortality',
'by'=>0,
'journal'=>'MEETING NEWS COVERAGE, Society for Healthcare Epidemiology of America',
'type'=>0,
'month_article'=>0,
'famous'=>0,
'views'=>1130,
'show'=>0,
'date'=>'2017-04-08 22:00:00',
'active'=>'1',
'user'=>0,
'created_at'=>'2020-05-15 01:20:06',
'updated_at'=>'2020-06-21 00:08:23',
'deleted_at'=>NULL
] );



Post::create( [
'id'=>4541,
'cat_id'=>8,
'tag_id'=>11,
'title'=>'Hyperbaric oxygen therapy is associated with lower short- and long-term mortality in patients with carbon monoxide poisoning',
'text'=>'Chest. Published online April 20, 2017. 10.1016',
'img'=>'file-1464165463-D3CBM9.jpg',
'file'=>NULL,
'link'=>'http://journal.publications.chestnet.org/article.aspx?articleid=2622868',
'by'=>0,
'journal'=>'Chien-Cheng Huang and others',
'type'=>0,
'month_article'=>0,
'famous'=>0,
'views'=>1101,
'show'=>0,
'date'=>'2017-04-19 22:00:00',
'active'=>'1',
'user'=>0,
'created_at'=>'2020-05-15 01:20:06',
'updated_at'=>'2020-06-21 00:08:23',
'deleted_at'=>NULL
] );



Post::create( [
'id'=>4542,
'cat_id'=>8,
'tag_id'=>10,
'title'=>'Intravenous immunoglobulin for treatment of severe refractory heparin-induced thrombocytopenia',
'text'=>'Chest. Published online April 20, 2017. 10.1016/j.chest.2017.03.050',
'img'=>'file-1463591685-DDLMK9.jpg',
'file'=>NULL,
'link'=>'http://journal.publications.chestnet.org/article.aspx?articleid=2622869',
'by'=>0,
'journal'=>'Anand Padmanabhan and others',
'type'=>0,
'month_article'=>0,
'famous'=>0,
'views'=>1101,
'show'=>0,
'date'=>'2017-04-21 22:00:00',
'active'=>'1',
'user'=>0,
'created_at'=>'2020-05-15 01:20:06',
'updated_at'=>'2020-06-21 00:08:23',
'deleted_at'=>NULL
] );



Post::create( [
'id'=>4543,
'cat_id'=>8,
'tag_id'=>11,
'title'=>'New drugs, new toxicities: severe side effects of modern targeted and immunotherapy of cancer and their management',
'text'=>'Critical Care 2017, 21:89  Published on: 14 April 2017',
'img'=>'file-1464165480-R231XR.jpg',
'file'=>NULL,
'link'=>'https://ccforum.biomedcentral.com/articles/10.1186/s13054-017-1678-1',
'by'=>0,
'journal'=>'Frank Kroschinsky and others',
'type'=>0,
'month_article'=>0,
'famous'=>1,
'views'=>1196,
'show'=>0,
'date'=>'2017-04-13 22:00:00',
'active'=>'1',
'user'=>0,
'created_at'=>'2020-05-15 01:20:06',
'updated_at'=>'2020-06-21 00:08:23',
'deleted_at'=>NULL
] );



Post::create( [
'id'=>4544,
'cat_id'=>8,
'tag_id'=>11,
'title'=>'Can calculation of energy expenditure based on CO2 measurements replace indirect calorimetry',
'text'=>'Critical Care 2017, 21:95  Published on: 12 April 2017',
'img'=>'file-1463591639-4YG8JY.jpg',
'file'=>NULL,
'link'=>'https://ccforum.biomedcentral.com/articles/10.1186/s13054-017-1650-0',
'by'=>0,
'journal'=>'Taku Oshima and others',
'type'=>0,
'month_article'=>0,
'famous'=>0,
'views'=>1101,
'show'=>0,
'date'=>'2017-04-11 22:00:00',
'active'=>'1',
'user'=>0,
'created_at'=>'2020-05-15 01:20:06',
'updated_at'=>'2020-06-21 00:08:23',
'deleted_at'=>NULL
] );



Post::create( [
'id'=>4545,
'cat_id'=>12,
'tag_id'=>11,
'title'=>'The hospital of tomorrow in 10 points',
'text'=>'Critical Care 2017, 21:93  Published on: 11 April 2017',
'img'=>'file-1464165480-R231XR.jpg',
'file'=>NULL,
'link'=>'https://ccforum.biomedcentral.com/articles/10.1186/s13054-017-1664-7',
'by'=>0,
'journal'=>'Jean-Louis Vincent and Jacques Creteur',
'type'=>0,
'month_article'=>0,
'famous'=>0,
'views'=>1212,
'show'=>0,
'date'=>'2017-04-10 22:00:00',
'active'=>'1',
'user'=>0,
'created_at'=>'2020-05-15 01:20:06',
'updated_at'=>'2020-06-21 00:08:23',
'deleted_at'=>NULL
] );



Post::create( [
'id'=>4546,
'cat_id'=>12,
'tag_id'=>8,
'title'=>'The future of critical care: renal support in 2027',
'text'=>'Critical Care 2017, 21:92  Published on: 11 April 2017',
'img'=>'file-1463591746-GEE3E7.jpg',
'file'=>NULL,
'link'=>'https://ccforum.biomedcentral.com/articles/10.1186/s13054-017-1665-6',
'by'=>0,
'journal'=>'William R. Clark and others',
'type'=>0,
'month_article'=>0,
'famous'=>1,
'views'=>1358,
'show'=>0,
'date'=>'2017-04-10 22:00:00',
'active'=>'1',
'user'=>0,
'created_at'=>'2020-05-15 01:20:06',
'updated_at'=>'2020-06-21 00:08:23',
'deleted_at'=>NULL
] );



Post::create( [
'id'=>4547,
'cat_id'=>12,
'tag_id'=>3,
'title'=>'2017 ESPEN guideline: Clinical nutrition in surgery',
'text'=>'',
'img'=>'file-1463591657-K8BRDF.jpg',
'file'=>NULL,
'link'=>'http://www.clinicalnutritionjournal.com/article/S0261-5614(17)30063-8/fulltext',
'by'=>0,
'journal'=>'Arved Weimann, et al. Clinical Nutrition 36 June 2017 623-650',
'type'=>0,
'month_article'=>0,
'famous'=>1,
'views'=>1307,
'show'=>0,
'date'=>'2017-05-18 22:00:00',
'active'=>'1',
'user'=>0,
'created_at'=>'2020-05-15 01:20:06',
'updated_at'=>'2020-06-21 00:08:23',
'deleted_at'=>NULL
] );



Post::create( [
'id'=>4548,
'cat_id'=>12,
'tag_id'=>10,
'title'=>'Liberal Versus Restrictive Transfusion Strategy in Critically Ill Oncologic Patients: The Transfusion Requirements in Critically Ill Oncologic Patients Randomized Controlled Trial',
'text'=>'Critical Care Medicine . 455:766-773, May 2017',
'img'=>'file-1463591685-DDLMK9.jpg',
'file'=>NULL,
'link'=>'http://journals.lww.com/ccmjournal/Fulltext/2017/05000/Liberal_Versus_Restrictive_Transfusion_Strategy_in.3.aspx',
'by'=>0,
'journal'=>'Bergamin, Fabricio S and others',
'type'=>0,
'month_article'=>0,
'famous'=>0,
'views'=>1212,
'show'=>0,
'date'=>'2017-05-20 22:00:00',
'active'=>'1',
'user'=>0,
'created_at'=>'2020-05-15 01:20:06',
'updated_at'=>'2020-06-21 00:08:23',
'deleted_at'=>NULL
] );



Post::create( [
'id'=>4549,
'cat_id'=>8,
'tag_id'=>6,
'title'=>'Preadmission Oral Corticosteroids Are Associated With Reduced Risk of Acute Respiratory Distress Syndrome in Critically Ill Adults With Sepsis',
'text'=>'Critical Care Medicine . 455:774-780, May 2017',
'img'=>'file-1463591752-XK911T.jpg',
'file'=>NULL,
'link'=>'http://journals.lww.com/ccmjournal/Fulltext/2017/05000/Preadmission_Oral_Corticosteroids_Are_Associated.4.aspx',
'by'=>0,
'journal'=>'McKown, Andrew C and others',
'type'=>0,
'month_article'=>0,
'famous'=>0,
'views'=>1101,
'show'=>0,
'date'=>'2017-05-20 22:00:00',
'active'=>'1',
'user'=>0,
'created_at'=>'2020-05-15 01:20:06',
'updated_at'=>'2020-06-21 00:08:23',
'deleted_at'=>NULL
] );



Post::create( [
'id'=>4550,
'cat_id'=>12,
'tag_id'=>1,
'title'=>'Serial Procalcitonin Predicts Mortality in Severe Sepsis Patients: Results From the Multicenter Procalcitonin MOnitoring SEpsis MOSES Study',
'text'=>'Critical Care Medicine . 455:781-789, May 2017',
'img'=>'file-1464106363-HWQX6B.jpg',
'file'=>NULL,
'link'=>'http://journals.lww.com/ccmjournal/Fulltext/2017/05000/Serial_Procalcitonin_Predicts_Mortality_in_Severe.5.aspx',
'by'=>0,
'journal'=>'Schuetz, Philipp and others',
'type'=>0,
'month_article'=>0,
'famous'=>0,
'views'=>1241,
'show'=>0,
'date'=>'2017-05-20 22:00:00',
'active'=>'1',
'user'=>0,
'created_at'=>'2020-05-15 01:20:06',
'updated_at'=>'2020-06-21 00:08:23',
'deleted_at'=>NULL
] );



Post::create( [
'id'=>4551,
'cat_id'=>8,
'tag_id'=>11,
'title'=>'New-Onset Atrial Fibrillation in the Critically Ill',
'text'=>'Critical Care Medicine . 455:798-805, May 2017',
'img'=>'file-1463591639-4YG8JY.jpg',
'file'=>NULL,
'link'=>'http://journals.lww.com/ccmjournal/Fulltext/2017/05000/New_Onset_Atrial_Fibrillation_in_the_Critically.6.aspx',
'by'=>0,
'journal'=>'Moss, Travis Jand others',
'type'=>0,
'month_article'=>0,
'famous'=>0,
'views'=>1101,
'show'=>0,
'date'=>'2017-05-20 22:00:00',
'active'=>'1',
'user'=>0,
'created_at'=>'2020-05-15 01:20:06',
'updated_at'=>'2020-06-21 00:08:23',
'deleted_at'=>NULL
] );



Post::create( [
'id'=>4552,
'cat_id'=>8,
'tag_id'=>6,
'title'=>'Randomized Clinical Trial of a Combination of an Inhaled Corticosteroid and Beta Agonist in Patients at Risk of Developing the Acute Respiratory Distress Syndrome',
'text'=>'Critical Care Medicine . 455:798-805, May 2017',
'img'=>'im5.jpg',
'file'=>NULL,
'link'=>'http://journals.lww.com/ccmjournal/Abstract/2017/05000/Randomized_Clinical_Trial_of_a_Combination_of_an.7.aspx',
'by'=>0,
'journal'=>'Festic, Emir and others',
'type'=>0,
'month_article'=>0,
'famous'=>0,
'views'=>1101,
'show'=>0,
'date'=>'2017-05-20 22:00:00',
'active'=>'1',
'user'=>0,
'created_at'=>'2020-05-15 01:20:06',
'updated_at'=>'2020-06-21 00:08:23',
'deleted_at'=>NULL
] );



Post::create( [
'id'=>4553,
'cat_id'=>8,
'tag_id'=>1,
'title'=>'Antipyretic Therapy in Critically Ill Septic Patients: A Systematic Review and Meta-Analysis',
'text'=>'Critical Care Medicine . 455:806-813, May 2017',
'img'=>'file-1464165463-D3CBM9.jpg',
'file'=>NULL,
'link'=>'http://journals.lww.com/ccmjournal/Fulltext/2017/05000/Antipyretic_Therapy_in_Critically_Ill_Septic.8.aspx',
'by'=>0,
'journal'=>'Drewry, Anne M and others',
'type'=>0,
'month_article'=>0,
'famous'=>0,
'views'=>1130,
'show'=>0,
'date'=>'2017-05-20 22:00:00',
'active'=>'1',
'user'=>0,
'created_at'=>'2020-05-15 01:20:06',
'updated_at'=>'2020-06-21 00:08:23',
'deleted_at'=>NULL
] );



Post::create( [
'id'=>4554,
'cat_id'=>8,
'tag_id'=>7,
'title'=>'Extracorporeal Membrane Oxygenation for Adult Community-Acquired Pneumonia: Outcomes and Predictors of Mortality',
'text'=>'Critical Care Medicine . 455:814-821, May 2017',
'img'=>'file-1463591644-ZGN2DA.jpg',
'file'=>NULL,
'link'=>'http://journals.lww.com/ccmjournal/Abstract/2017/05000/Extracorporeal_Membrane_Oxygenation_for_Adult.9.aspx',
'by'=>0,
'journal'=>'Ramanathan, Kollengode and others',
'type'=>0,
'month_article'=>0,
'famous'=>0,
'views'=>1101,
'show'=>0,
'date'=>'2017-05-20 22:00:00',
'active'=>'1',
'user'=>0,
'created_at'=>'2020-05-15 01:20:06',
'updated_at'=>'2020-06-21 00:08:23',
'deleted_at'=>NULL
] );



Post::create( [
'id'=>4555,
'cat_id'=>8,
'tag_id'=>5,
'title'=>'Guideline covers neuroprotective interventions after CPR',
'text'=>'Medscape Critical Care, May 17, 2017',
'img'=>'im1.jpg',
'file'=>NULL,
'link'=>'http://www.medscape.com/viewarticle/880135',
'by'=>0,
'journal'=>'Pauline Anderson',
'type'=>0,
'month_article'=>0,
'famous'=>0,
'views'=>1114,
'show'=>0,
'date'=>'2017-05-20 22:00:00',
'active'=>'1',
'user'=>0,
'created_at'=>'2020-05-15 01:20:06',
'updated_at'=>'2020-06-21 00:08:23',
'deleted_at'=>NULL
] );



Post::create( [
'id'=>4556,
'cat_id'=>8,
'tag_id'=>5,
'title'=>'Study supports wider window for thrombectomy use after stroke',
'text'=>'Medscape Critical Care, May 17, 2017',
'img'=>'im2.jpg',
'file'=>NULL,
'link'=>'http://www.medscape.com/viewarticle/880144',
'by'=>0,
'journal'=>'Sue Hughes',
'type'=>0,
'month_article'=>0,
'famous'=>0,
'views'=>1114,
'show'=>0,
'date'=>'2017-05-20 22:00:00',
'active'=>'1',
'user'=>0,
'created_at'=>'2020-05-15 01:20:06',
'updated_at'=>'2020-06-21 00:08:23',
'deleted_at'=>NULL
] );



Post::create( [
'id'=>4557,
'cat_id'=>8,
'tag_id'=>1,
'title'=>'The Septic Shock 3.0 Definition and Trials: A Vasopressin and Septic Shock Trial Experience',
'text'=>'Critical Care Medicine. 456:940-948, June 2017',
'img'=>'file-1464165463-D3CBM9.jpg',
'file'=>NULL,
'link'=>'http://journals.lww.com/ccmjournal/Fulltext/2017/06000/The_Septic_Shock_3_0_Definition_and_Trials___A.3.aspx',
'by'=>0,
'journal'=>'Russell, James A and others',
'type'=>0,
'month_article'=>0,
'famous'=>0,
'views'=>1130,
'show'=>0,
'date'=>'2017-06-02 22:00:00',
'active'=>'1',
'user'=>0,
'created_at'=>'2020-05-15 01:20:06',
'updated_at'=>'2020-06-21 00:08:23',
'deleted_at'=>NULL
] );



Post::create( [
'id'=>4558,
'cat_id'=>8,
'tag_id'=>11,
'title'=>'The Association Between Visiting Intensivists and ICU Outcomes',
'text'=>'Critical Care Medicine. 456:949-955, June 2017',
'img'=>'file-1464165480-R231XR.jpg',
'file'=>NULL,
'link'=>'http://journals.lww.com/ccmjournal/Fulltext/2017/06000/The_Association_Between_Visiting_Intensivists_and.4.aspx',
'by'=>0,
'journal'=>'Whitehouse, Tony and others',
'type'=>0,
'month_article'=>0,
'famous'=>0,
'views'=>1101,
'show'=>0,
'date'=>'2017-06-02 22:00:00',
'active'=>'1',
'user'=>0,
'created_at'=>'2020-05-15 01:20:06',
'updated_at'=>'2020-06-21 00:08:23',
'deleted_at'=>NULL
] );



Post::create( [
'id'=>4559,
'cat_id'=>8,
'tag_id'=>7,
'title'=>'Postoperative Complications and Outcomes Associated With a Transition to 24/7 Intensivist Management of Cardiac Surgery Patients',
'text'=>'Critical Care Medicine. 456:993-1000, June 2017',
'img'=>'im3.jpg',
'file'=>NULL,
'link'=>'http://journals.lww.com/ccmjournal/Abstract/2017/06000/Postoperative_Complications_and_Outcomes.10.aspx',
'by'=>0,
'journal'=>'Benoit, Marc A and others',
'type'=>0,
'month_article'=>0,
'famous'=>0,
'views'=>1101,
'show'=>0,
'date'=>'2017-06-02 22:00:00',
'active'=>'1',
'user'=>0,
'created_at'=>'2020-05-15 01:20:06',
'updated_at'=>'2020-06-21 00:08:23',
'deleted_at'=>NULL
] );



Post::create( [
'id'=>4560,
'cat_id'=>8,
'tag_id'=>11,
'title'=>'American College of Critical Care Medicine Clinical Practice Parameters for Hemodynamic Support of Pediatric and Neonatal Septic Shock',
'text'=>'Critical Care Medicine. 456:1061-1093, June 2017',
'img'=>'file-1463591644-ZGN2DA.jpg',
'file'=>NULL,
'link'=>'http://journals.lww.com/ccmjournal/Fulltext/2017/06000/American_College_of_Critical_Care_Medicine.18.aspx',
'by'=>0,
'journal'=>'Davis, Alan L. and others',
'type'=>0,
'month_article'=>0,
'famous'=>0,
'views'=>1101,
'show'=>0,
'date'=>'2017-06-02 22:00:00',
'active'=>'1',
'user'=>0,
'created_at'=>'2020-05-15 01:20:06',
'updated_at'=>'2020-06-21 00:08:23',
'deleted_at'=>NULL
] );



Post::create( [
'id'=>4561,
'cat_id'=>8,
'tag_id'=>1,
'title'=>'Corticosteroids in septic shock: a systematic review and network meta-analysis',
'text'=>'Critical Care 2017, 21:78',
'img'=>'file-1463591639-4YG8JY.jpg',
'file'=>NULL,
'link'=>'https://ccforum.biomedcentral.com/articles/10.1186/s13054-017-1659-4?utm_campaign=BMC40711B&utm_medium=BMCemail&utm_source=Teradata',
'by'=>0,
'journal'=>'Ben Gibbison and others',
'type'=>0,
'month_article'=>0,
'famous'=>0,
'views'=>1130,
'show'=>0,
'date'=>'2017-06-03 22:00:00',
'active'=>'1',
'user'=>0,
'created_at'=>'2020-05-15 01:20:06',
'updated_at'=>'2020-06-21 00:08:23',
'deleted_at'=>NULL
] );



Post::create( [
'id'=>4564,
'cat_id'=>8,
'tag_id'=>6,
'title'=>'Obesity and survival in critically ill patients with acute respiratory distress syndrome: a paradox within the paradox',
'text'=>'Critical Care June  2017, 21:114',
'img'=>'file-1463591752-XK911T.jpg',
'file'=>NULL,
'link'=>'https://ccforum.biomedcentral.com/articles/10.1186/s13054-017-1682-5',
'by'=>0,
'journal'=>'Lorenzo Ball, Ary Serpa Neto and Paolo Pelosi',
'type'=>0,
'month_article'=>0,
'famous'=>0,
'views'=>1101,
'show'=>0,
'date'=>'2017-06-03 22:00:00',
'active'=>'1',
'user'=>0,
'created_at'=>'2020-05-15 01:20:06',
'updated_at'=>'2020-06-21 00:08:23',
'deleted_at'=>NULL
] );



Post::create( [
'id'=>4565,
'cat_id'=>8,
'tag_id'=>6,
'title'=>'Trials directly comparing alternative spontaneous breathing trial techniques: a systematic review and meta-analysis',
'text'=>'Critical Care June, 2017, 21:127',
'img'=>'im6.jpg',
'file'=>NULL,
'link'=>'https://ccforum.biomedcentral.com/articles/10.1186/s13054-017-1698-x',
'by'=>0,
'journal'=>'Karen E. A. Burns and others',
'type'=>0,
'month_article'=>0,
'famous'=>0,
'views'=>1101,
'show'=>0,
'date'=>'2017-06-03 22:00:00',
'active'=>'1',
'user'=>0,
'created_at'=>'2020-05-15 01:20:06',
'updated_at'=>'2020-06-21 00:08:23',
'deleted_at'=>NULL
] );



Post::create( [
'id'=>4566,
'cat_id'=>8,
'tag_id'=>3,
'title'=>'A randomized controlled pilot study to evaluate the effect of an enteral formulation designed to improve gastrointestinal tolerance in the critically ill patient—the SPIRIT trial',
'text'=>'Critical Care 2017, 21:140  Published on: 10 June 2017',
'img'=>'file-1463591732-SM39NY.jpg',
'file'=>NULL,
'link'=>'https://ccforum.biomedcentral.com/articles/10.1186/s13054-017-1730-1',
'by'=>0,
'journal'=>'Stephan M. Jakob, Lukas Bütikofer, David Berger, Michael Coslovsky and Jukka Takala',
'type'=>0,
'month_article'=>0,
'famous'=>0,
'views'=>1101,
'show'=>0,
'date'=>'2017-06-10 22:00:00',
'active'=>'1',
'user'=>0,
'created_at'=>'2020-05-15 01:20:06',
'updated_at'=>'2020-06-21 00:08:23',
'deleted_at'=>NULL
] );



Post::create( [
'id'=>4567,
'cat_id'=>8,
'tag_id'=>3,
'title'=>'Transthoracic echocardiography: an accurate and precise method for estimating cardiac output in the critically ill patient',
'text'=>'Critical Care 2017, 21:136  Published on: 9 June 2017',
'img'=>'file-1463591639-4YG8JY.jpg',
'file'=>NULL,
'link'=>'https://ccforum.biomedcentral.com/articles/10.1186/s13054-017-1737-7',
'by'=>0,
'journal'=>'Pablo Mercado,and others',
'type'=>0,
'month_article'=>0,
'famous'=>0,
'views'=>1101,
'show'=>0,
'date'=>'2017-06-09 22:00:00',
'active'=>'1',
'user'=>0,
'created_at'=>'2020-05-15 01:20:06',
'updated_at'=>'2020-06-21 00:08:23',
'deleted_at'=>NULL
] );



Post::create( [
'id'=>4568,
'cat_id'=>8,
'tag_id'=>11,
'title'=>'A randomized trial of supplemental parenteral nutrition in underweight and overweight critically ill patients: the TOP-UP pilot trial',
'text'=>'Critical Care 2017, 21:142  Published on: 9 June 2017',
'img'=>'file-1463591725-9J88SF.jpg',
'file'=>NULL,
'link'=>'https://ccforum.biomedcentral.com/articles/10.1186/s13054-017-1736-8',
'by'=>0,
'journal'=>'Paul E. Wischmeyer and others',
'type'=>0,
'month_article'=>0,
'famous'=>0,
'views'=>1101,
'show'=>0,
'date'=>'2017-06-09 22:00:00',
'active'=>'1',
'user'=>0,
'created_at'=>'2020-05-15 01:20:06',
'updated_at'=>'2020-06-21 00:08:23',
'deleted_at'=>NULL
] );



Post::create( [
'id'=>4569,
'cat_id'=>8,
'tag_id'=>7,
'title'=>'Antiarrhythmic drugs in out-of-hospital cardiac arrest: is there a place for potassium chloride',
'text'=>'Critical Care 2017, 21:144  Published on: 17 June 2017',
'img'=>'file-1463591639-4YG8JY.jpg',
'file'=>NULL,
'link'=>'https://ccforum.biomedcentral.com/articles/10.1186/s13054-017-1732-z',
'by'=>0,
'journal'=>'Romain Jouffroy and Benoît Vivien',
'type'=>0,
'month_article'=>0,
'famous'=>0,
'views'=>1101,
'show'=>0,
'date'=>'2017-06-16 22:00:00',
'active'=>'1',
'user'=>0,
'created_at'=>'2020-05-15 01:20:06',
'updated_at'=>'2020-06-21 00:08:23',
'deleted_at'=>NULL
] );



Post::create( [
'id'=>4570,
'cat_id'=>8,
'tag_id'=>11,
'title'=>'Early versus late initiation of renal replacement therapy impacts mortality in patients with acute kidney injury post cardiac surgery: a meta-analysis',
'text'=>'Critical Care 2017, 21:150  Published on: 17 June 2017',
'img'=>'file-1463591746-GEE3E7.jpg',
'file'=>NULL,
'link'=>'https://ccforum.biomedcentral.com/articles/10.1186/s13054-017-1707-0',
'by'=>0,
'journal'=>'Honghong Zou, Qianwen Hong and Gaosi XU',
'type'=>0,
'month_article'=>0,
'famous'=>0,
'views'=>1101,
'show'=>0,
'date'=>'2017-06-16 22:00:00',
'active'=>'1',
'user'=>0,
'created_at'=>'2020-05-15 01:20:06',
'updated_at'=>'2020-06-21 00:08:23',
'deleted_at'=>NULL
] );



Post::create( [
'id'=>4571,
'cat_id'=>8,
'tag_id'=>11,
'title'=>'Open-chest versus closed-chest cardiopulmonary resuscitation in blunt trauma: analysis of a nationwide trauma registry',
'text'=>'Critical Care 2017, 21:169  Published on: 3 July 2017',
'img'=>'file-1463591639-4YG8JY.jpg',
'file'=>NULL,
'link'=>'https://ccforum.biomedcentral.com/articles/10.1186/s13054-017-1759-1',
'by'=>0,
'journal'=>'Akira Endo, Atsushi Shiraishi, Yasuhiro Otomo, Makoto Tomita, Hiroki Matsui and Kiyoshi Murata',
'type'=>0,
'month_article'=>0,
'famous'=>0,
'views'=>1101,
'show'=>0,
'date'=>'2017-08-02 22:00:00',
'active'=>'1',
'user'=>0,
'created_at'=>'2020-05-15 01:20:06',
'updated_at'=>'2020-06-21 00:08:23',
'deleted_at'=>NULL
] );



Post::create( [
'id'=>4572,
'cat_id'=>8,
'tag_id'=>11,
'title'=>'Combination therapy may reduce sepsis mortality, study says',
'text'=>'Infection Control & Clinical Quality',
'img'=>'file-1464106363-HWQX6B.jpg',
'file'=>NULL,
'link'=>'http://www.beckershospitalreview.com/quality/drug-cocktail-of-vitamin-b-c-corticosteroids-could-lower-rates-of-sepsis-shock-death.html',
'by'=>0,
'journal'=>'Written by Anuja Vaidy',
'type'=>0,
'month_article'=>0,
'famous'=>0,
'views'=>1101,
'show'=>0,
'date'=>'2017-07-04 22:00:00',
'active'=>'1',
'user'=>0,
'created_at'=>'2020-05-15 01:20:06',
'updated_at'=>'2020-06-21 00:08:23',
'deleted_at'=>NULL
] );



Post::create( [
'id'=>4573,
'cat_id'=>8,
'tag_id'=>11,
'title'=>'CDC data show C. diff infection rates are falling after steady increase',
'text'=>'YOUR HEALTH, June 29, 2017',
'img'=>'file-1463591639-4YG8JY.jpg',
'file'=>NULL,
'link'=>'http://www.npr.org/sections/health-shots/2017/06/29/534870581/c-diff-infections-are-falling-thanks-to-better-cleaning-and-fewer-antibiotics',
'by'=>0,
'journal'=>'ANGUS CHEN',
'type'=>0,
'month_article'=>0,
'famous'=>0,
'views'=>1101,
'show'=>0,
'date'=>'2017-06-28 22:00:00',
'active'=>'1',
'user'=>0,
'created_at'=>'2020-05-15 01:20:06',
'updated_at'=>'2020-06-21 00:08:23',
'deleted_at'=>NULL
] );



Post::create( [
'id'=>4574,
'cat_id'=>12,
'tag_id'=>1,
'title'=>'Double carbapenem as a rescue strategy for the treatment of severe carbapenemase-producing Klebsiella pneumoniae infections: a two-center, matched case–control study',
'text'=>'Critical Care 2017, 21:173  Published on: 5 July 2017',
'img'=>'file-1464106363-HWQX6B.jpg',
'file'=>NULL,
'link'=>'https://ccforum.biomedcentral.com/articles/10.1186/s13054-017-1769-z',
'by'=>0,
'journal'=>'Gennaro De Pascale and others',
'type'=>0,
'month_article'=>0,
'famous'=>0,
'views'=>1241,
'show'=>0,
'date'=>'2017-07-04 22:00:00',
'active'=>'1',
'user'=>0,
'created_at'=>'2020-05-15 01:20:06',
'updated_at'=>'2020-06-21 00:08:23',
'deleted_at'=>NULL
] );



Post::create( [
'id'=>4575,
'cat_id'=>8,
'tag_id'=>9,
'title'=>'Withholding Pantoprazole for Stress Ulcer Prophylaxis in Critically Ill Patients: A Pilot Randomized Clinical Trial and Meta-Analysis',
'text'=>'Critical Care Medicine. 457:1121-1129, July 2017',
'img'=>'file-1463591657-K8BRDF.jpg',
'file'=>NULL,
'link'=>'http://journals.lww.com/ccmjournal/Abstract/2017/07000/Withholding_Pantoprazole_for_Stress_Ulcer.3.aspx',
'by'=>0,
'journal'=>'Alhazzani, Waleed, and others',
'type'=>0,
'month_article'=>0,
'famous'=>0,
'views'=>1101,
'show'=>0,
'date'=>'2017-07-09 22:00:00',
'active'=>'1',
'user'=>0,
'created_at'=>'2020-05-15 01:20:06',
'updated_at'=>'2020-06-21 00:08:23',
'deleted_at'=>NULL
] );



Post::create( [
'id'=>4576,
'cat_id'=>8,
'tag_id'=>11,
'title'=>'Simulation of a Novel Schedule for Intensivist Staffing to Improve Continuity of Patient Care and Reduce Physician Burnout',
'text'=>'Critical Care Medicine. 457:1138-1144, July 2017',
'img'=>'file-1464165463-D3CBM9.jpg',
'file'=>NULL,
'link'=>'http://journals.lww.com/ccmjournal/Abstract/2017/07000/Simulation_of_a_Novel_Schedule_for_Intensivist.5.aspx',
'by'=>0,
'journal'=>'Geva, Alon and others',
'type'=>0,
'month_article'=>0,
'famous'=>0,
'views'=>1101,
'show'=>0,
'date'=>'2017-07-09 22:00:00',
'active'=>'1',
'user'=>0,
'created_at'=>'2020-05-15 01:20:06',
'updated_at'=>'2020-06-21 00:08:23',
'deleted_at'=>NULL
] );



Post::create( [
'id'=>4577,
'cat_id'=>8,
'tag_id'=>7,
'title'=>'Resuscitation With Balanced Fluids Is Associated With Improved Survival in Pediatric Severe Sepsis',
'text'=>'Critical Care Medicine. 457:1177-1183, July 2017',
'img'=>'file-1463591644-ZGN2DA.jpg',
'file'=>NULL,
'link'=>'http://journals.lww.com/ccmjournal/Abstract/2017/07000/Resuscitation_With_Balanced_Fluids_Is_Associated.10.aspx',
'by'=>0,
'journal'=>'Emrath, Elizabeth T and others',
'type'=>0,
'month_article'=>0,
'famous'=>0,
'views'=>1101,
'show'=>0,
'date'=>'2017-07-09 22:00:00',
'active'=>'1',
'user'=>0,
'created_at'=>'2020-05-15 01:20:06',
'updated_at'=>'2020-06-21 00:08:23',
'deleted_at'=>NULL
] );



Post::create( [
'id'=>4578,
'cat_id'=>8,
'tag_id'=>7,
'title'=>'Neuron-Specific Enolase Predicts Poor Outcome After Cardiac Arrest and Targeted Temperature Management: A Multicenter Study on 1,053 Patients',
'text'=>'Critical Care Medicine. 457:1145-1151, July 2017',
'img'=>'file-1463591639-4YG8JY.jpg',
'file'=>NULL,
'link'=>'http://journals.lww.com/ccmjournal/Abstract/2017/07000/Neuron_Specific_Enolase_Predicts_Poor_Outcome.6.aspx',
'by'=>0,
'journal'=>'Streitberger, Kaspar Josche and others',
'type'=>0,
'month_article'=>0,
'famous'=>0,
'views'=>1101,
'show'=>0,
'date'=>'2017-07-09 22:00:00',
'active'=>'1',
'user'=>0,
'created_at'=>'2020-05-15 01:20:06',
'updated_at'=>'2020-06-21 00:08:23',
'deleted_at'=>NULL
] );



Post::create( [
'id'=>4579,
'cat_id'=>8,
'tag_id'=>11,
'title'=>'Effects of IV Acetaminophen on Core Body Temperature and Hemodynamic Responses in Febrile Critically Ill Adults: A Randomized Controlled Trial',
'text'=>'Critical Care Medicine . 457:1199-1207, July 2017.',
'img'=>'file-1463591639-4YG8JY.jpg',
'file'=>NULL,
'link'=>'http://journals.lww.com/ccmjournal/Abstract/2017/07000/Effects_of_IV_Acetaminophen_on_Core_Body.13.aspx',
'by'=>0,
'journal'=>'Schell-Chaple, Hildy M and others',
'type'=>0,
'month_article'=>0,
'famous'=>0,
'views'=>1101,
'show'=>0,
'date'=>'2017-07-09 22:00:00',
'active'=>'1',
'user'=>0,
'created_at'=>'2020-05-15 01:20:06',
'updated_at'=>'2020-06-21 00:08:23',
'deleted_at'=>NULL
] );



Post::create( [
'id'=>4580,
'cat_id'=>8,
'tag_id'=>11,
'title'=>'Pilot Feasibility Study of Therapeutic Hypothermia for Moderate to Severe Acute Respiratory Distress Syndrome',
'text'=>'Critical Care Medicine: July 2017 - Volume 45 - Issue 7 - p 1152–1159',
'img'=>'file-1463591752-XK911T.jpg',
'file'=>NULL,
'link'=>'http://journals.lww.com/ccmjournal/Abstract/2017/07000/Pilot_Feasibility_Study_of_Therapeutic_Hypothermia.7.aspx',
'by'=>0,
'journal'=>'Slack, Donald F and others',
'type'=>0,
'month_article'=>0,
'famous'=>0,
'views'=>1101,
'show'=>0,
'date'=>'2017-07-09 22:00:00',
'active'=>'1',
'user'=>0,
'created_at'=>'2020-05-15 01:20:06',
'updated_at'=>'2020-06-21 00:08:23',
'deleted_at'=>NULL
] );



Post::create( [
'id'=>4581,
'cat_id'=>8,
'tag_id'=>11,
'title'=>'Ultrasound as a Screening Tool for Central Venous Catheter Positioning and Exclusion of Pneumothorax',
'text'=>'Critical Care Medicine. 457:1192-1198, July 2017',
'img'=>'file-1463591639-4YG8JY.jpg',
'file'=>NULL,
'link'=>'http://journals.lww.com/ccmjournal/Abstract/2017/07000/Ultrasound_as_a_Screening_Tool_for_Central_Venous.12.aspx',
'by'=>0,
'journal'=>'Amir, Rabia and others',
'type'=>0,
'month_article'=>0,
'famous'=>0,
'views'=>1101,
'show'=>0,
'date'=>'2017-07-09 22:00:00',
'active'=>'1',
'user'=>0,
'created_at'=>'2020-05-15 01:20:06',
'updated_at'=>'2020-06-21 00:08:23',
'deleted_at'=>NULL
] );



Post::create( [
'id'=>4582,
'cat_id'=>8,
'tag_id'=>7,
'title'=>'The Clinical Picture of Severe Systemic Capillary-Leak Syndrome Episodes Requiring ICU Admission',
'text'=>'Critical Care Medicine. 457:1216-1223, July 2017',
'img'=>'file-1463591644-ZGN2DA.jpg',
'file'=>NULL,
'link'=>'http://journals.lww.com/ccmjournal/Abstract/2017/07000/The_Clinical_Picture_of_Severe_Systemic.15.aspx',
'by'=>0,
'journal'=>'Pineton de Chambrun and others',
'type'=>0,
'month_article'=>0,
'famous'=>0,
'views'=>1101,
'show'=>0,
'date'=>'2017-07-09 22:00:00',
'active'=>'1',
'user'=>0,
'created_at'=>'2020-05-15 01:20:06',
'updated_at'=>'2020-06-21 00:08:23',
'deleted_at'=>NULL
] );



Post::create( [
'id'=>4583,
'cat_id'=>8,
'tag_id'=>11,
'title'=>'How to remove the grey area between ventilator-associated pneumonia and ventilator-associated tracheobronchitis',
'text'=>'Critical Care 2017, 21:165  Published on: 8 July 2017',
'img'=>'im6.jpg',
'file'=>NULL,
'link'=>'https://ccforum.biomedcentral.com/articles/10.1186/s13054-017-1754-6',
'by'=>0,
'journal'=>'Yuetian Yu, Cheng Zhu, Chunyan Liu and Yuan Gao',
'type'=>0,
'month_article'=>0,
'famous'=>0,
'views'=>1101,
'show'=>0,
'date'=>'2017-07-09 22:00:00',
'active'=>'1',
'user'=>0,
'created_at'=>'2020-05-15 01:20:06',
'updated_at'=>'2020-06-21 00:08:23',
'deleted_at'=>NULL
] );



Post::create( [
'id'=>4584,
'cat_id'=>8,
'tag_id'=>6,
'title'=>'New setting of neurally adjusted ventilatory assist for noninvasive ventilation by facial mask: a physiologic study',
'text'=>'Critical Care 2017, 21:170  Published on: 7 July 2017',
'img'=>'im5.jpg',
'file'=>NULL,
'link'=>'https://ccforum.biomedcentral.com/articles/10.1186/s13054-017-1761-7',
'by'=>0,
'journal'=>'Federico Longhini and others',
'type'=>0,
'month_article'=>0,
'famous'=>0,
'views'=>1101,
'show'=>0,
'date'=>'2017-07-09 22:00:00',
'active'=>'1',
'user'=>0,
'created_at'=>'2020-05-15 01:20:06',
'updated_at'=>'2020-06-21 00:08:23',
'deleted_at'=>NULL
] );



Post::create( [
'id'=>4585,
'cat_id'=>8,
'tag_id'=>9,
'title'=>'Is it time to restrict the use of PPIs',
'text'=>'',
'img'=>'file-1463591650-J14MMU.jpg',
'file'=>NULL,
'link'=>'http://www.univadis.md/medical-news/596/Is-it-time-to-restrict-the-use-of-PPIsutm_sourcenewsletteremail&utm_mediumemail&utm_campaignmedicalupdatesbestof-weekly&utm_content1493583&utm_termautomated_bestof_weekly',
'by'=>0,
'journal'=>'Univadis , Tuesday 04 July 2017',
'type'=>0,
'month_article'=>0,
'famous'=>0,
'views'=>1101,
'show'=>0,
'date'=>'2017-07-09 22:00:00',
'active'=>'1',
'user'=>0,
'created_at'=>'2020-05-15 01:20:06',
'updated_at'=>'2020-06-21 00:08:23',
'deleted_at'=>NULL
] );



Post::create( [
'id'=>4586,
'cat_id'=>12,
'tag_id'=>9,
'title'=>'Risk of death among users of Proton Pump Inhibitors: a longitudinal observational cohort study of United States veterans',
'text'=>'BMJ Open 20177:e015735. doi:10.1136/bmjopen-2016-015735',
'img'=>'file-1463591657-K8BRDF.jpg',
'file'=>NULL,
'link'=>'http://bmjopen.bmj.com/content/bmjopen/7/6/e015735.full.pdf',
'by'=>0,
'journal'=>'Yan Xie and others',
'type'=>0,
'month_article'=>0,
'famous'=>0,
'views'=>1212,
'show'=>0,
'date'=>'2017-07-09 22:00:00',
'active'=>'1',
'user'=>0,
'created_at'=>'2020-05-15 01:20:06',
'updated_at'=>'2020-06-21 00:08:23',
'deleted_at'=>NULL
] );



Post::create( [
'id'=>4587,
'cat_id'=>8,
'tag_id'=>11,
'title'=>'Combined use of serum 1,3-β-d-glucan and procalcitonin for the early differential diagnosis between candidaemia and bacteraemia in intensive care units',
'text'=>'Critical Care 2017, 21:176  Published on: 10 July 2017',
'img'=>'file-1464106363-HWQX6B.jpg',
'file'=>NULL,
'link'=>'https://ccforum.biomedcentral.com/articles/10.1186/s13054-017-1763-5',
'by'=>0,
'journal'=>'Daniele Roberto Giacobbe and others',
'type'=>0,
'month_article'=>0,
'famous'=>0,
'views'=>1101,
'show'=>0,
'date'=>'2017-07-11 22:00:00',
'active'=>'1',
'user'=>0,
'created_at'=>'2020-05-15 01:20:06',
'updated_at'=>'2020-06-21 00:08:23',
'deleted_at'=>NULL
] );



Post::create( [
'id'=>4588,
'cat_id'=>8,
'tag_id'=>5,
'title'=>'Subarachnoid Hemorrhage',
'text'=>'N Engl J Med. 20 July 2017377:257-266',
'img'=>'im2.jpg',
'file'=>NULL,
'link'=>'http://www.nejm.org/doi/full/10.1056/NEJMcp1605827queryTOC',
'by'=>0,
'journal'=>'M.T. Lawton and G.E. Vates',
'type'=>0,
'month_article'=>0,
'famous'=>0,
'views'=>1114,
'show'=>0,
'date'=>'2017-07-19 22:00:00',
'active'=>'1',
'user'=>0,
'created_at'=>'2020-05-15 01:20:06',
'updated_at'=>'2020-06-21 00:08:23',
'deleted_at'=>NULL
] );



Post::create( [
'id'=>4589,
'cat_id'=>8,
'tag_id'=>1,
'title'=>'Low-dose immunoglobulin G is not associated with mortality in patients with sepsis and septic shock',
'text'=>'Critical Care 2017, 21:181, Published on: 13 July 2017',
'img'=>'file-1464106363-HWQX6B.jpg',
'file'=>NULL,
'link'=>'https://ccforum.biomedcentral.com/articles/10.1186/s13054-017-1764-4',
'by'=>0,
'journal'=>'Yusuke Iizuka and others',
'type'=>0,
'month_article'=>0,
'famous'=>0,
'views'=>1130,
'show'=>0,
'date'=>'2017-07-19 22:00:00',
'active'=>'1',
'user'=>0,
'created_at'=>'2020-05-15 01:20:06',
'updated_at'=>'2020-06-21 00:08:23',
'deleted_at'=>NULL
] );



Post::create( [
'id'=>4590,
'cat_id'=>8,
'tag_id'=>1,
'title'=>'Association of baseline steroid use with long-term rates of infection and sepsis in the REGARDS cohort',
'text'=>'Critical Care 2017, 21:185, Published on: 13 July 2017',
'img'=>'im5.jpg',
'file'=>NULL,
'link'=>'https://ccforum.biomedcentral.com/articles/10.1186/s13054-017-1767-1',
'by'=>0,
'journal'=>'Ninad S. Chaudhary and others',
'type'=>0,
'month_article'=>0,
'famous'=>0,
'views'=>1130,
'show'=>0,
'date'=>'2017-07-19 22:00:00',
'active'=>'1',
'user'=>0,
'created_at'=>'2020-05-15 01:20:06',
'updated_at'=>'2020-06-21 00:08:23',
'deleted_at'=>NULL
] );



Post::create( [
'id'=>4591,
'cat_id'=>8,
'tag_id'=>7,
'title'=>'Statins: Do the risks outweigh the benefits in over 65s',
'text'=>'',
'img'=>'file-1463591639-4YG8JY.jpg',
'file'=>NULL,
'link'=>'http://www.univadis.md/medical-news/596/Statins-Do-the-risks-outweigh-the-benefits-in-over-65s?utm_source=adhoc+email&utm_medium=email&utm_campaign=adhoc_statin_email_uniannouncement_eng-ae_20170714&utm_content=1502396&utm_term=',
'by'=>0,
'journal'=>'Univadis , 25 May 2017',
'type'=>0,
'month_article'=>0,
'famous'=>0,
'views'=>1101,
'show'=>0,
'date'=>'2017-07-19 22:00:00',
'active'=>'1',
'user'=>0,
'created_at'=>'2020-05-15 01:20:06',
'updated_at'=>'2020-06-21 00:08:23',
'deleted_at'=>NULL
] );



Post::create( [
'id'=>4592,
'cat_id'=>12,
'tag_id'=>7,
'title'=>'Risks of Statin Therapy in Older Adults',
'text'=>'JAMA Intern Med. 20171777:966',
'img'=>'file-1463591639-4YG8JY.jpg',
'file'=>NULL,
'link'=>'http://jamanetwork.com/journals/jamainternalmedicine/fullarticle/2628968',
'by'=>0,
'journal'=>'Gregory Curfman, MD',
'type'=>0,
'month_article'=>0,
'famous'=>0,
'views'=>1212,
'show'=>0,
'date'=>'2017-07-19 22:00:00',
'active'=>'1',
'user'=>0,
'created_at'=>'2020-05-15 01:20:06',
'updated_at'=>'2020-06-21 00:08:23',
'deleted_at'=>NULL
] );



Post::create( [
'id'=>4593,
'cat_id'=>9,
'tag_id'=>1,
'title'=>'Official American Thoracic Society/Centers for Disease Control and Prevention/Infectious Diseases Society of America Clinical Practice Guidelines: Treatment of Drug-Susceptible Tuberculosis',
'text'=>'Published: Clinical Infectious Diseases  2016  63 : 147 -195',
'img'=>'file-1464165480-R231XR.jpg',
'file'=>NULL,
'link'=>'http://www.idsociety.org/Guidelines/Patient_Care/IDSA_Practice_Guidelines/Infections_by_Organism/Treatment_of_Drug-Susceptible_Tuberculosis/',
'by'=>0,
'journal'=>'Payam Nahid and others',
'type'=>0,
'month_article'=>0,
'famous'=>0,
'views'=>1130,
'show'=>0,
'date'=>'2016-11-30 22:00:00',
'active'=>'1',
'user'=>0,
'created_at'=>'2020-05-15 01:20:06',
'updated_at'=>'2020-06-21 00:08:23',
'deleted_at'=>NULL
] );



Post::create( [
'id'=>4594,
'cat_id'=>8,
'tag_id'=>6,
'title'=>'Inhaled AP301 for treatment of pulmonary edema in mechanically ventilated patients with acute respiratory distress syndrome: a phase IIa randomized placebo-controlled trial',
'text'=>'Critical Care 2017, 21:194  Published on: 27 July 2017',
'img'=>'file-1463591752-XK911T.jpg',
'file'=>NULL,
'link'=>'https://ccforum.biomedcentral.com/articles/10.1186/s13054-017-1795-x',
'by'=>0,
'journal'=>'Katharina Krenn and others',
'type'=>0,
'month_article'=>0,
'famous'=>0,
'views'=>1101,
'show'=>0,
'date'=>'2017-07-27 22:00:00',
'active'=>'1',
'user'=>0,
'created_at'=>'2020-05-15 01:20:06',
'updated_at'=>'2020-06-21 00:08:23',
'deleted_at'=>NULL
] );



Post::create( [
'id'=>4595,
'cat_id'=>8,
'tag_id'=>1,
'title'=>'Is Early Goal-Directed Therapy Harmful to Patients With Sepsis and High Disease Severity',
'text'=>'Critical Care Medicine . 458:1265-1267, August 2017',
'img'=>'im4.jpg',
'file'=>NULL,
'link'=>'http://journals.lww.com/ccmjournal/Fulltext/2017/08000/Is_Early_Goal_Directed_Therapy_Harmful_to_Patients.1.aspx',
'by'=>0,
'journal'=>'Kalil, Andre C. Kellum, John A.',
'type'=>0,
'month_article'=>0,
'famous'=>0,
'views'=>1130,
'show'=>0,
'date'=>'2017-08-01 22:00:00',
'active'=>'1',
'user'=>0,
'created_at'=>'2020-05-15 01:20:06',
'updated_at'=>'2020-06-21 00:08:23',
'deleted_at'=>NULL
] );



Post::create( [
'id'=>4596,
'cat_id'=>8,
'tag_id'=>11,
'title'=>'Antibiotic Therapy in Comatose Mechanically Ventilated Patients Following Aspiration: Differentiating Pneumonia From Pneumonitis',
'text'=>'Critical Care Medicine . 458:1268-1275, August 2017',
'img'=>'im6.jpg',
'file'=>NULL,
'link'=>'http://journals.lww.com/ccmjournal/Fulltext/2017/08000/Antibiotic_Therapy_in_Comatose_Mechanically.2.aspx',
'by'=>0,
'journal'=>'Lascarrou, Jean Baptiste and others',
'type'=>0,
'month_article'=>0,
'famous'=>0,
'views'=>1101,
'show'=>0,
'date'=>'2017-08-01 22:00:00',
'active'=>'1',
'user'=>0,
'created_at'=>'2020-05-15 01:20:06',
'updated_at'=>'2020-06-21 00:08:23',
'deleted_at'=>NULL
] );



Post::create( [
'id'=>4597,
'cat_id'=>8,
'tag_id'=>11,
'title'=>'Clinical Experience With IV Angiotensin II Administration: A Systematic Review of Safety',
'text'=>'Critical Care Medicine . 458:1285-1294, August 2017.',
'img'=>'file-1463591639-4YG8JY.jpg',
'file'=>NULL,
'link'=>'http://journals.lww.com/ccmjournal/Fulltext/2017/08000/Clinical_Experience_With_IV_Angiotensin_II.4.aspx',
'by'=>0,
'journal'=>'Busse, Laurence W and others',
'type'=>0,
'month_article'=>0,
'famous'=>0,
'views'=>1101,
'show'=>0,
'date'=>'2017-08-01 22:00:00',
'active'=>'1',
'user'=>0,
'created_at'=>'2020-05-15 01:20:06',
'updated_at'=>'2020-06-21 00:08:23',
'deleted_at'=>NULL
] );



Post::create( [
'id'=>4598,
'cat_id'=>8,
'tag_id'=>7,
'title'=>'Extracorporeal Membrane Oxygenation for Acute Decompensated Heart Failure',
'text'=>'Critical Care Medicine . 458:1359-1366, August 2017',
'img'=>'file-1463591644-ZGN2DA.jpg',
'file'=>NULL,
'link'=>'http://journals.lww.com/ccmjournal/Abstract/2017/08000/Extracorporeal_Membrane_Oxygenation_for_Acute.13.aspx',
'by'=>0,
'journal'=>'Dangers, Laurence and others',
'type'=>0,
'month_article'=>0,
'famous'=>0,
'views'=>1101,
'show'=>0,
'date'=>'2017-08-01 22:00:00',
'active'=>'1',
'user'=>0,
'created_at'=>'2020-05-15 01:20:06',
'updated_at'=>'2020-06-21 00:08:23',
'deleted_at'=>NULL
] );



Post::create( [
'id'=>4599,
'cat_id'=>8,
'tag_id'=>11,
'title'=>'Transpulmonary Pressure Describes Lung Morphology During Decremental Positive End-Expiratory Pressure Trials in Obesity',
'text'=>'Critical Care Medicine: August 2017 - Volume 45 - Issue 8 - p 1374–1381',
'img'=>'im5.jpg',
'file'=>NULL,
'link'=>'http://journals.lww.com/ccmjournal/Abstract/2017/08000/Transpulmonary_Pressure_Describes_Lung_Morphology.15.aspx',
'by'=>0,
'journal'=>'Fumagalli, Jacop and others',
'type'=>0,
'month_article'=>0,
'famous'=>0,
'views'=>1101,
'show'=>0,
'date'=>'2017-08-01 22:00:00',
'active'=>'1',
'user'=>0,
'created_at'=>'2020-05-15 01:20:06',
'updated_at'=>'2020-06-21 00:08:23',
'deleted_at'=>NULL
] );

Post::create( [
'id'=>4600,
'cat_id'=>8,
'tag_id'=>7,
'title'=>'What is the impact of the fluid challenge technique on diagnosis of fluid responsiveness A systematic review and meta-analysis',
'text'=>'Critical Care 2017, 21:207  Published on: 4 August 2017',
'img'=>'file-1463591639-4YG8JY.jpg',
'file'=>NULL,
'link'=>'https://ccforum.biomedcentral.com/articles/10.1186/s13054-017-1796-9',
'by'=>0,
'journal'=>'Laura Toscani and others',
'type'=>0,
'month_article'=>0,
'famous'=>0,
'views'=>1101,
'show'=>0,
'date'=>'2017-08-05 22:00:00',
'active'=>'1',
'user'=>0,
'created_at'=>'2020-05-15 01:20:06',
'updated_at'=>'2020-06-21 00:08:23',
'deleted_at'=>NULL
] );


    
Post::create( [
'id'=>4601,
'cat_id'=>8,
'tag_id'=>6,
'title'=>'Driving pressure: a marker of severity, a safety limit, or a goal for mechanical ventilation',
'text'=>'Critical Care 2017, 21:199  Published on: 4 August 2017',
'img'=>'im6.jpg',
'file'=>NULL,
'link'=>'https://ccforum.biomedcentral.com/articles/10.1186/s13054-017-1779-x',
'by'=>0,
'journal'=>'Guillermo Bugedo, Jaime Retamal and Alejandro Bruhn',
'type'=>0,
'month_article'=>0,
'famous'=>0,
'views'=>1101,
'show'=>0,
'date'=>'2017-08-05 22:00:00',
'active'=>'1',
'user'=>0,
'created_at'=>'2020-05-15 01:20:06',
'updated_at'=>'2020-06-21 00:08:23',
'deleted_at'=>NULL
] );


    
Post::create( [
'id'=>4602,
'cat_id'=>8,
'tag_id'=>11,
'title'=>'Plasma concentrations of caspofungin in a critically ill patient with morbid obesity',
'text'=>'Critical Care 2017, 21:200  Published on: 2 August 2017',
'img'=>'file-1464106363-HWQX6B.jpg',
'file'=>NULL,
'link'=>'https://ccforum.biomedcentral.com/articles/10.1186/s13054-017-1774-2',
'by'=>0,
'journal'=>'Rafael Ferriols-Lisart and others',
'type'=>0,
'month_article'=>0,
'famous'=>0,
'views'=>1101,
'show'=>0,
'date'=>'2017-08-05 22:00:00',
'active'=>'1',
'user'=>0,
'created_at'=>'2020-05-15 01:20:06',
'updated_at'=>'2020-06-21 00:08:23',
'deleted_at'=>NULL
] );


    
Post::create( [
'id'=>4603,
'cat_id'=>14,
'tag_id'=>11,
'title'=>'Critical Care Review Notes',
'text'=>'CHEST, Critical Care Medicine 2014',
'img'=>'file-1464165480-R231XR.jpg',
'file'=>NULL,
'link'=>'https://criticalcarethoughtsdotcom.files.wordpress.com/2015/02/2014-ccm-review-notes.pdf',
'by'=>0,
'journal'=>'Jon-Emile S. Kenny M.D.',
'type'=>0,
'month_article'=>0,
'famous'=>1,
'views'=>1196,
'show'=>0,
'date'=>'2014-10-31 22:00:00',
'active'=>'1',
'user'=>0,
'created_at'=>'2020-05-15 01:20:06',
'updated_at'=>'2020-06-21 00:08:23',
'deleted_at'=>NULL
] );


    
Post::create( [
'id'=>4604,
'cat_id'=>8,
'tag_id'=>11,
'title'=>'Point-of-care ultrasonography: a practical step in the path to precision in critical care',
'text'=>'Critical Care 2017, 21:215  Published on: 16 August 2017',
'img'=>'file-1464106363-HWQX6B.jpg',
'file'=>NULL,
'link'=>'https://ccforum.biomedcentral.com/articles/10.1186/s13054-017-1802-2',
'by'=>0,
'journal'=>'Gentle Sunder Shrestha',
'type'=>0,
'month_article'=>0,
'famous'=>0,
'views'=>1101,
'show'=>0,
'date'=>'2017-08-16 22:00:00',
'active'=>'1',
'user'=>0,
'created_at'=>'2020-05-15 01:20:06',
'updated_at'=>'2020-06-21 00:08:23',
'deleted_at'=>NULL
] );


    
Post::create( [
'id'=>4605,
'cat_id'=>12,
'tag_id'=>7,
'title'=>'Selepressin, a novel selective vasopressin V1A agonist, is an effective substitute for norepinephrine in a phase IIa randomized, placebo-controlled trial in septic shock patients',
'text'=>'Critical Care 2017, 21:213  Published on: 15 August 2017',
'img'=>'file-1463591644-ZGN2DA.jpg',
'file'=>NULL,
'link'=>'https://ccforum.biomedcentral.com/articles/10.1186/s13054-017-1798-7',
'by'=>0,
'journal'=>'James A. Russell, Jean-Louis Vincent, and others',
'type'=>0,
'month_article'=>0,
'famous'=>1,
'views'=>1307,
'show'=>0,
'date'=>'2017-08-16 22:00:00',
'active'=>'1',
'user'=>0,
'created_at'=>'2020-05-15 01:20:06',
'updated_at'=>'2020-06-21 00:08:23',
'deleted_at'=>NULL
] );


    
Post::create( [
'id'=>4606,
'cat_id'=>8,
'tag_id'=>11,
'title'=>'The cost of treating opioid overdose victims is skyrocketing',
'text'=>'',
'img'=>'im1.jpg',
'file'=>NULL,
'link'=>'https://www.statnews.com/2017/08/11/opioid-overdose-costs/',
'by'=>0,
'journal'=>'CASEY ROSS @byCaseyRoss AUGUST 11, 2017',
'type'=>0,
'month_article'=>0,
'famous'=>0,
'views'=>1101,
'show'=>0,
'date'=>'2017-08-10 22:00:00',
'active'=>'1',
'user'=>0,
'created_at'=>'2020-05-15 01:20:06',
'updated_at'=>'2020-06-21 00:08:23',
'deleted_at'=>NULL
] );


    
Post::create( [
'id'=>4607,
'cat_id'=>8,
'tag_id'=>3,
'title'=>'Global Study: Most ICU Patients Underfed',
'text'=>'Clinical Nutrition364:1122–1129. August 2017',
'img'=>'file-1463591650-J14MMU.jpg',
'file'=>NULL,
'link'=>'https://healthmanagement.org/c/icu/news/global-study-most-icu-patients-underfed',
'by'=>0,
'journal'=>'Bendavid, Itai et al.',
'type'=>0,
'month_article'=>0,
'famous'=>0,
'views'=>1101,
'show'=>0,
'date'=>'2017-08-16 22:00:00',
'active'=>'1',
'user'=>0,
'created_at'=>'2020-05-15 01:20:06',
'updated_at'=>'2020-06-21 00:08:23',
'deleted_at'=>NULL
] );


    
Post::create( [
'id'=>4608,
'cat_id'=>8,
'tag_id'=>8,
'title'=>'Circulating levels of soluble Fas sCD95 are associated with risk for development of a nonresolving acute kidney injury subphenotype',
'text'=>'Critical Care 2017, 21:217  Published on: 17 August 2017',
'img'=>'file-1463591746-GEE3E7.jpg',
'file'=>NULL,
'link'=>'https://ccforum.biomedcentral.com/articles/10.1186/s13054-017-1807-x',
'by'=>0,
'journal'=>'Pavan K. Bhatraju and others',
'type'=>0,
'month_article'=>0,
'famous'=>0,
'views'=>1152,
'show'=>0,
'date'=>'2017-08-17 22:00:00',
'active'=>'1',
'user'=>0,
'created_at'=>'2020-05-15 01:20:06',
'updated_at'=>'2020-06-21 00:08:23',
'deleted_at'=>NULL
] );


    
Post::create( [
'id'=>4609,
'cat_id'=>8,
'tag_id'=>1,
'title'=>'Diagnostic Stewardship—Leveraging the Laboratory to Improve Antimicrobial Use',
'text'=>'JAMA. August 20173187:607-608',
'img'=>'file-1464106363-HWQX6B.jpg',
'file'=>NULL,
'link'=>'http://jamanetwork.com/journals/jama/article-abstract/2647071',
'by'=>0,
'journal'=>'Daniel J. Morgan, MD, MS Preeti Malani, MD, MSJ Daniel J. Diekema, MD, MS',
'type'=>0,
'month_article'=>0,
'famous'=>0,
'views'=>1130,
'show'=>0,
'date'=>'2017-08-20 22:00:00',
'active'=>'1',
'user'=>0,
'created_at'=>'2020-05-15 01:20:06',
'updated_at'=>'2020-06-21 00:08:23',
'deleted_at'=>NULL
] );


    
Post::create( [
'id'=>4610,
'cat_id'=>12,
'tag_id'=>7,
'title'=>'Targeted Temperature Management for 48 vs 24 Hours and Neurologic Outcome After Out-of-Hospital Cardiac Arrest. A Randomized Clinical Trial',
'text'=>'JAMA. 20173184:341-350',
'img'=>'im2.jpg',
'file'=>NULL,
'link'=>'http://jamanetwork.com/journals/jama/article-abstract/2645105?widget=personalizedcontent&previousarticle=2647071',
'by'=>0,
'journal'=>'Hans Kirkegaard and others',
'type'=>0,
'month_article'=>0,
'famous'=>0,
'views'=>1212,
'show'=>0,
'date'=>'2017-08-20 22:00:00',
'active'=>'1',
'user'=>0,
'created_at'=>'2020-05-15 01:20:06',
'updated_at'=>'2020-06-21 00:08:23',
'deleted_at'=>NULL
] );


    
Post::create( [
'id'=>4611,
'cat_id'=>12,
'tag_id'=>11,
'title'=>'Antithrombotic Therapy for Venous Thromboembolic Disease',
'text'=>'JAMA. May 201731719:2008-2009',
'img'=>'file-1463591639-4YG8JY.jpg',
'file'=>NULL,
'link'=>'http://jamanetwork.com/journals/jama/article-abstract/2626557?widget=personalizedcontent&previousarticle=2645105',
'by'=>0,
'journal'=>'Atul Jain, MD, MS Adam S. Cifu, MD',
'type'=>0,
'month_article'=>0,
'famous'=>0,
'views'=>1212,
'show'=>0,
'date'=>'2017-05-15 22:00:00',
'active'=>'1',
'user'=>0,
'created_at'=>'2020-05-15 01:20:06',
'updated_at'=>'2020-06-21 00:08:23',
'deleted_at'=>NULL
] );


    
Post::create( [
'id'=>4612,
'cat_id'=>8,
'tag_id'=>11,
'title'=>'Association of Antithrombotic Drug Use With Subdural Hematoma Risk',
'text'=>'JAMA. Feb 20173178:836-846',
'img'=>'im1.jpg',
'file'=>NULL,
'link'=>'http://jamanetwork.com/journals/jama/fullarticle/2605799?widget=personalizedcontent&previousarticle=2626557',
'by'=>0,
'journal'=>'David Gaist and others',
'type'=>0,
'month_article'=>0,
'famous'=>0,
'views'=>1101,
'show'=>0,
'date'=>'2017-02-15 22:00:00',
'active'=>'1',
'user'=>0,
'created_at'=>'2020-05-15 01:20:06',
'updated_at'=>'2020-06-21 00:08:23',
'deleted_at'=>NULL
] );


    
Post::create( [
'id'=>4613,
'cat_id'=>8,
'tag_id'=>11,
'title'=>'IV to subcutaneous insulin transition rate low in ICU',
'text'=>'',
'img'=>'file-1464165480-R231XR.jpg',
'file'=>NULL,
'link'=>'https://www.healio.com/endocrinology/diabetes/news/in-the-journals/%7B45d4fc72-5538-4651-927f-c1af7f747cc6%7D/iv-to-subcutaneous-insulin-transition-rate-low-in-icu',
'by'=>0,
'journal'=>'Zhou K, et al. Diabetes Res Clin Pract. August 18, 2017',
'type'=>0,
'month_article'=>0,
'famous'=>0,
'views'=>1101,
'show'=>0,
'date'=>'2017-08-21 22:00:00',
'active'=>'1',
'user'=>0,
'created_at'=>'2020-05-15 01:20:06',
'updated_at'=>'2020-06-21 00:08:23',
'deleted_at'=>NULL
] );


    
Post::create( [
'id'=>4614,
'cat_id'=>12,
'tag_id'=>11,
'title'=>'The future of mechanical ventilation: lessons from the present and the past',
'text'=>'Critical Care201721:183',
'img'=>'im6.jpg',
'file'=>NULL,
'link'=>'https://ccforum.biomedcentral.com/articles/10.1186/s13054-017-1750-x',
'by'=>0,
'journal'=>'Luciano Gattinoni et al.',
'type'=>0,
'month_article'=>0,
'famous'=>0,
'views'=>1101,
'show'=>0,
'date'=>'2017-08-22 22:00:00',
'active'=>'1',
'user'=>0,
'created_at'=>'2020-05-15 01:20:06',
'updated_at'=>'2020-06-21 00:08:23',
'deleted_at'=>NULL
] );


    
Post::create( [
'id'=>4615,
'cat_id'=>8,
'tag_id'=>6,
'title'=>'Trials directly comparing alternative spontaneous breathing trial techniques: a systematic review and meta-analysis',
'text'=>'Critical Care201721:127',
'img'=>'im5.jpg',
'file'=>NULL,
'link'=>'https://ccforum.biomedcentral.com/articles/10.1186/s13054-017-1698-x',
'by'=>0,
'journal'=>'Karen E. A. Burns and others',
'type'=>0,
'month_article'=>0,
'famous'=>0,
'views'=>1101,
'show'=>0,
'date'=>'2017-01-31 22:00:00',
'active'=>'1',
'user'=>0,
'created_at'=>'2020-05-15 01:20:06',
'updated_at'=>'2020-06-21 00:08:23',
'deleted_at'=>NULL
] );


    
Post::create( [
'id'=>4616,
'cat_id'=>13,
'tag_id'=>5,
'title'=>'Modified NIH Stroke Scale/Score mNIHSS',
'text'=>'',
'img'=>'im1.jpg',
'file'=>NULL,
'link'=>'https://www.mdcalc.com/modified-nih-stroke-scale-score-mnihss',
'by'=>0,
'journal'=>'MD CALC',
'type'=>0,
'month_article'=>0,
'famous'=>0,
'views'=>1114,
'show'=>0,
'date'=>'2016-08-31 22:00:00',
'active'=>'1',
'user'=>0,
'created_at'=>'2020-05-15 01:20:06',
'updated_at'=>'2020-06-21 00:08:23',
'deleted_at'=>NULL
] );


    
Post::create( [
'id'=>4617,
'cat_id'=>13,
'tag_id'=>7,
'title'=>'Corrected QT Interval QTc',
'text'=>'',
'img'=>'file-1463591639-4YG8JY.jpg',
'file'=>NULL,
'link'=>'https://www.mdcalc.com/corrected-qt-interval-qtc',
'by'=>0,
'journal'=>'MD CALC',
'type'=>0,
'month_article'=>0,
'famous'=>0,
'views'=>1101,
'show'=>0,
'date'=>'2017-01-04 22:00:00',
'active'=>'1',
'user'=>0,
'created_at'=>'2020-05-15 01:20:06',
'updated_at'=>'2020-06-21 00:08:23',
'deleted_at'=>NULL
] );


    
Post::create( [
'id'=>4618,
'cat_id'=>13,
'tag_id'=>5,
'title'=>'Modified Rankin Scale for Neurologic Disability',
'text'=>'',
'img'=>'im2.jpg',
'file'=>NULL,
'link'=>'https://www.mdcalc.com/modified-rankin-scale-neurologic-disability',
'by'=>0,
'journal'=>'MD CALC',
'type'=>0,
'month_article'=>0,
'famous'=>0,
'views'=>1114,
'show'=>0,
'date'=>'2016-11-30 22:00:00',
'active'=>'1',
'user'=>0,
'created_at'=>'2020-05-15 01:20:06',
'updated_at'=>'2020-06-21 00:08:23',
'deleted_at'=>NULL
] );


    
Post::create( [
'id'=>4619,
'cat_id'=>13,
'tag_id'=>6,
'title'=>'Geneva Score Revised for Pulmonary Embolism',
'text'=>'',
'img'=>'file-1463591752-XK911T.jpg',
'file'=>NULL,
'link'=>'https://www.mdcalc.com/geneva-score-revised-pulmonary-embolism',
'by'=>0,
'journal'=>'MD CALC',
'type'=>0,
'month_article'=>0,
'famous'=>0,
'views'=>1101,
'show'=>0,
'date'=>'2016-11-30 22:00:00',
'active'=>'1',
'user'=>0,
'created_at'=>'2020-05-15 01:20:06',
'updated_at'=>'2020-06-21 00:08:23',
'deleted_at'=>NULL
] );


    
Post::create( [
'id'=>4620,
'cat_id'=>8,
'tag_id'=>1,
'title'=>'Sepsis-3: Impact on Previously Defined Septic Shock Patients',
'text'=>'Source: Critical Care Medicine',
'img'=>'file-1464165463-D3CBM9.jpg',
'file'=>NULL,
'link'=>'https://healthmanagement.org/c/icu/news/sepsis-3-impact-on-previously-defined-septic-shock-patients',
'by'=>0,
'journal'=>'Health Management Published on : Tue, 22 Aug 2017',
'type'=>0,
'month_article'=>0,
'famous'=>0,
'views'=>1130,
'show'=>0,
'date'=>'2017-08-21 22:00:00',
'active'=>'1',
'user'=>0,
'created_at'=>'2020-05-15 01:20:06',
'updated_at'=>'2020-06-21 00:08:23',
'deleted_at'=>NULL
] );


    
Post::create( [
'id'=>4621,
'cat_id'=>8,
'tag_id'=>11,
'title'=>'Ultrasound-guided central venous catheter placement: a structured review and recommendations for clinical practice',
'text'=>'Critical Care 2017, 21:225  Published on: 28 August 2017',
'img'=>'im4.jpg',
'file'=>NULL,
'link'=>'https://ccforum.biomedcentral.com/articles/10.1186/s13054-017-1814-y',
'by'=>0,
'journal'=>'Bernd Saugel, Thomas W. L. Scheeren and Jean-Louis Teboul',
'type'=>0,
'month_article'=>0,
'famous'=>0,
'views'=>1101,
'show'=>0,
'date'=>'2017-08-28 22:00:00',
'active'=>'1',
'user'=>0,
'created_at'=>'2020-05-15 01:20:06',
'updated_at'=>'2020-06-21 00:08:23',
'deleted_at'=>NULL
] );


    
Post::create( [
'id'=>4622,
'cat_id'=>8,
'tag_id'=>11,
'title'=>'Drinking four coffees a day cuts the risk of early death by two-thirds, research suggests',
'text'=>'Updated: 29th August 2017',
'img'=>'file-1464165480-R231XR.jpg',
'file'=>NULL,
'link'=>'https://www.thesun.co.uk/living/4337210/four-coffees-a-day-will-cut-changes-for-early-death/',
'by'=>0,
'journal'=>'By Nick McDermott, Health Editor',
'type'=>0,
'month_article'=>0,
'famous'=>0,
'views'=>1101,
'show'=>0,
'date'=>'2017-08-28 22:00:00',
'active'=>'1',
'user'=>0,
'created_at'=>'2020-05-15 01:20:06',
'updated_at'=>'2020-06-21 00:08:23',
'deleted_at'=>NULL
] );


    
Post::create( [
'id'=>4623,
'cat_id'=>12,
'tag_id'=>1,
'title'=>'Beta-Lactam Infusion in Severe Sepsis BLISS: a prospective, two-centre, open-labelled randomised controlled trial of continuous versus intermittent beta-lactam infusion in critically ill patients with severe sepsis',
'text'=>'Intensive Care Medicine. October 2016 4210:1535–1545',
'img'=>'file-1464165463-D3CBM9.jpg',
'file'=>NULL,
'link'=>'https://link.springer.com/article/10.1007/s00134-015-4188-0',
'by'=>0,
'journal'=>'Mohd H. Abdul-Aziz and others',
'type'=>0,
'month_article'=>0,
'famous'=>0,
'views'=>1241,
'show'=>0,
'date'=>'2017-08-31 22:00:00',
'active'=>'1',
'user'=>0,
'created_at'=>'2020-05-15 01:20:06',
'updated_at'=>'2020-06-21 00:08:23',
'deleted_at'=>NULL
] );


    
Post::create( [
'id'=>4624,
'cat_id'=>8,
'tag_id'=>6,
'title'=>'The standard of care of patients with ARDS: ventilatory settings and rescue therapies for refractory hypoxemia',
'text'=>'Intensive Care Med 2016 42:699–711',
'img'=>'file-1463591752-XK911T.jpg',
'file'=>NULL,
'link'=>'https://link.springer.com/content/pdf/10.1007%2Fs00134-016-4325-4.pdf',
'by'=>0,
'journal'=>'Thomas Bein and others',
'type'=>0,
'month_article'=>0,
'famous'=>1,
'views'=>1196,
'show'=>0,
'date'=>'2016-05-10 22:00:00',
'active'=>'1',
'user'=>0,
'created_at'=>'2020-05-15 01:20:06',
'updated_at'=>'2020-06-21 00:08:23',
'deleted_at'=>NULL
] );


    
Post::create( [
'id'=>4625,
'cat_id'=>8,
'tag_id'=>2,
'title'=>'Delirium and Benzodiazepines Associated With Prolonged ICU Stay in Critically Ill Infants and Young Children',
'text'=>'Critical Care Medicine . 459:1427-1435, September 2017',
'img'=>'im1.jpg',
'file'=>NULL,
'link'=>'http://journals.lww.com/ccmjournal/Fulltext/2017/09000/Delirium_and_Benzodiazepines_Associated_With.1.aspx',
'by'=>0,
'journal'=>'Smith, Heidi A. B. and others',
'type'=>0,
'month_article'=>0,
'famous'=>0,
'views'=>1172,
'show'=>0,
'date'=>'2017-08-31 22:00:00',
'active'=>'1',
'user'=>0,
'created_at'=>'2020-05-15 01:20:06',
'updated_at'=>'2020-06-21 00:08:23',
'deleted_at'=>NULL
] );


    
Post::create( [
'id'=>4626,
'cat_id'=>8,
'tag_id'=>1,
'title'=>'The Impact of the Sepsis-3 Septic Shock Definition on Previously Defined Septic Shock Patients',
'text'=>'Critical Care Medicine . 459:1436-1442, September 2017',
'img'=>'file-1464165480-R231XR.jpg',
'file'=>NULL,
'link'=>'http://journals.lww.com/ccmjournal/Fulltext/2017/09000/The_Impact_of_the_Sepsis_3_Septic_Shock_Definition.2.aspx',
'by'=>0,
'journal'=>'Sterling, Sarah A. and others',
'type'=>0,
'month_article'=>0,
'famous'=>0,
'views'=>1130,
'show'=>0,
'date'=>'2017-08-31 22:00:00',
'active'=>'1',
'user'=>0,
'created_at'=>'2020-05-15 01:20:06',
'updated_at'=>'2020-06-21 00:08:23',
'deleted_at'=>NULL
] );


    
Post::create( [
'id'=>4627,
'cat_id'=>8,
'tag_id'=>2,
'title'=>'Statin and Its Association With Delirium in the Medical ICU',
'text'=>'Critical Care Medicine . 459:1515-1522, September 2017',
'img'=>'file-1463591639-4YG8JY.jpg',
'file'=>NULL,
'link'=>'http://journals.lww.com/ccmjournal/Abstract/2017/09000/Statin_and_Its_Association_With_Delirium_in_the.12.aspx',
'by'=>0,
'journal'=>'Mather, Jeffrey F. and others',
'type'=>0,
'month_article'=>0,
'famous'=>0,
'views'=>1172,
'show'=>0,
'date'=>'2017-08-31 22:00:00',
'active'=>'1',
'user'=>0,
'created_at'=>'2020-05-15 01:20:06',
'updated_at'=>'2020-06-21 00:08:23',
'deleted_at'=>NULL
] );


    
Post::create( [
'id'=>4628,
'cat_id'=>8,
'tag_id'=>11,
'title'=>'Preventing Harm in the ICU—Building a Culture of Safety and Engaging Patients and Families',
'text'=>'Critical Care Medicine . 459:1531-1537, September 2017.',
'img'=>'file-1463591639-4YG8JY.jpg',
'file'=>NULL,
'link'=>'http://journals.lww.com/ccmjournal/Abstract/2017/09000/Preventing_Harm_in_the_ICU_Building_a_Culture_of.14.aspx',
'by'=>0,
'journal'=>'Thornton, Kevin C and others',
'type'=>0,
'month_article'=>0,
'famous'=>0,
'views'=>1101,
'show'=>0,
'date'=>'2017-08-31 22:00:00',
'active'=>'1',
'user'=>0,
'created_at'=>'2020-05-15 01:20:06',
'updated_at'=>'2020-06-21 00:08:23',
'deleted_at'=>NULL
] );


    
Post::create( [
'id'=>4629,
'cat_id'=>8,
'tag_id'=>7,
'title'=>'Incorporating Dynamic Assessment of Fluid Responsiveness Into Goal-Directed Therapy: A Systematic Review and Meta-Analysis',
'text'=>'Critical Care Medicine . 459:1538-1545, September 2017',
'img'=>'file-1463591644-ZGN2DA.jpg',
'file'=>NULL,
'link'=>'http://journals.lww.com/ccmjournal/Fulltext/2017/09000/Incorporating_Dynamic_Assessment_of_Fluid.15.aspx',
'by'=>0,
'journal'=>'Bednarczyk, Joseph M. and others',
'type'=>0,
'month_article'=>0,
'famous'=>0,
'views'=>1101,
'show'=>0,
'date'=>'2017-08-31 22:00:00',
'active'=>'1',
'user'=>0,
'created_at'=>'2020-05-15 01:20:06',
'updated_at'=>'2020-06-21 00:08:23',
'deleted_at'=>NULL
] );


    
Post::create( [
'id'=>4630,
'cat_id'=>9,
'tag_id'=>11,
'title'=>'Executive Summary: Clinical Practice Guideline: Safe Medication Use in the ICU',
'text'=>'Critical Care Medicine . 459:1546-1551, September 2017',
'img'=>'file-1464106363-HWQX6B.jpg',
'file'=>NULL,
'link'=>'http://journals.lww.com/ccmjournal/Fulltext/2017/09000/Executive_Summary___Clinical_Practice_Guideline__.16.aspx',
'by'=>0,
'journal'=>'Kane-Gill, Sandra L. and others',
'type'=>0,
'month_article'=>0,
'famous'=>0,
'views'=>1101,
'show'=>0,
'date'=>'2017-08-31 22:00:00',
'active'=>'1',
'user'=>0,
'created_at'=>'2020-05-15 01:20:06',
'updated_at'=>'2020-06-21 00:08:23',
'deleted_at'=>NULL
] );


    
Post::create( [
'id'=>4631,
'cat_id'=>9,
'tag_id'=>3,
'title'=>'2017 ESPEN guideline: Clinical nutrition in surgery',
'text'=>'Clinical Nutrition 36 2017 623-650',
'img'=>'file-1463591732-SM39NY.jpg',
'file'=>NULL,
'link'=>'http://www.espen.org/files/ESPEN-guideline_Clinical-nutrition-in-surgery.pdf',
'by'=>0,
'journal'=>'Arved Weimann and others',
'type'=>0,
'month_article'=>0,
'famous'=>0,
'views'=>1101,
'show'=>0,
'date'=>'2017-04-30 22:00:00',
'active'=>'1',
'user'=>0,
'created_at'=>'2020-05-15 01:20:06',
'updated_at'=>'2020-06-21 00:08:23',
'deleted_at'=>NULL
] );


    
Post::create( [
'id'=>4632,
'cat_id'=>9,
'tag_id'=>3,
'title'=>'2017 ESPEN guidelines on nutrition in cancer patients',
'text'=>'Clinical Nutrition 36 2017 11–48',
'img'=>'file-1463591732-SM39NY.jpg',
'file'=>NULL,
'link'=>'http://www.espen.info/wp/wordpress/wp-content/uploads/2016/11/ESPEN-cancer-guidelines-2016-final-published.pdf',
'by'=>0,
'journal'=>'Jann Arends and others',
'type'=>0,
'month_article'=>0,
'famous'=>0,
'views'=>1101,
'show'=>0,
'date'=>'2017-04-30 22:00:00',
'active'=>'1',
'user'=>0,
'created_at'=>'2020-05-15 01:20:06',
'updated_at'=>'2020-06-21 00:08:23',
'deleted_at'=>NULL
] );


    
Post::create( [
'id'=>4633,
'cat_id'=>9,
'tag_id'=>3,
'title'=>'Management of acute intestinal failure: A position paper from the European Society for Clinical Nutrition and Metabolism ESPEN Special Interest Group',
'text'=>'Clinical Nutrition 35 2016, 6, 1209–1218',
'img'=>'file-1463591725-9J88SF.jpg',
'file'=>NULL,
'link'=>'http://www.espen.org/files/PIIS0261561416300267.pdf',
'by'=>0,
'journal'=>'Stanislaw Klek and others',
'type'=>0,
'month_article'=>0,
'famous'=>0,
'views'=>1101,
'show'=>0,
'date'=>'2016-11-30 22:00:00',
'active'=>'1',
'user'=>0,
'created_at'=>'2020-05-15 01:20:06',
'updated_at'=>'2020-06-21 00:08:23',
'deleted_at'=>NULL
] );


    
Post::create( [
'id'=>4634,
'cat_id'=>8,
'tag_id'=>11,
'title'=>'Study finds the need for more attention to survivors post-ICU. Patients can suffer severe PTSD years after hospital stay',
'text'=>'Posted: Aug 31, 2017 10:13 AM MDT. Updated: Sep 01, 2017 11:02 AM MDT',
'img'=>'file-1463591639-4YG8JY.jpg',
'file'=>NULL,
'link'=>'http://www.good4utah.com/news/local-news/study-finds-the-need-for-more-attention-to-survivors-post-icu/802041376',
'by'=>0,
'journal'=>'By: Surae Chinn',
'type'=>0,
'month_article'=>0,
'famous'=>0,
'views'=>1101,
'show'=>0,
'date'=>'2017-08-31 22:00:00',
'active'=>'1',
'user'=>0,
'created_at'=>'2020-05-15 01:20:06',
'updated_at'=>'2020-06-21 00:08:23',
'deleted_at'=>NULL
] );


    
Post::create( [
'id'=>4635,
'cat_id'=>9,
'tag_id'=>7,
'title'=>'http://circ.ahajournals.org/content/126/6/768.figures-only',
'text'=>'Circulation. 2012126:768-773\r\nOriginally published August 6, 2012',
'img'=>'file-1463591644-ZGN2DA.jpg',
'file'=>NULL,
'link'=>'Management of Deep Vein Thrombosis of the Upper Extremity',
'by'=>0,
'journal'=>'Rolf P. Engelberger, Nils Kucher',
'type'=>0,
'month_article'=>0,
'famous'=>0,
'views'=>1101,
'show'=>0,
'date'=>'2016-10-31 22:00:00',
'active'=>'1',
'user'=>0,
'created_at'=>'2020-05-15 01:20:06',
'updated_at'=>'2020-06-21 00:08:23',
'deleted_at'=>NULL
] );


    
Post::create( [
'id'=>4636,
'cat_id'=>8,
'tag_id'=>3,
'title'=>'1st Advanced Critical Care Nutrition Course',
'text'=>'DT News',
'img'=>'file-1463591725-9J88SF.jpg',
'file'=>NULL,
'link'=>'http://www.newsofbahrain.com/viewNews.php?ppId=27243&TYPE=Posts&pid=21&MNU=18&SUB=58',
'by'=>0,
'journal'=>'Hosted by RBH',
'type'=>0,
'month_article'=>0,
'famous'=>0,
'views'=>1101,
'show'=>0,
'date'=>'2017-07-31 22:00:00',
'active'=>'1',
'user'=>0,
'created_at'=>'2020-05-15 01:20:06',
'updated_at'=>'2020-06-21 00:08:23',
'deleted_at'=>NULL
] );


    
Post::create( [
'id'=>4637,
'cat_id'=>8,
'tag_id'=>8,
'title'=>'Renal replacement therapy after cardiac surgery: do not ask “When”, ask “Why”',
'text'=>'Critical Care 2017, 21:231  Published on: 5 September 2017',
'img'=>'file-1463591746-GEE3E7.jpg',
'file'=>NULL,
'link'=>'https://ccforum.biomedcentral.com/articles/10.1186/s13054-017-1818-7',
'by'=>0,
'journal'=>'Stéphane Gaudry, David Hajage and Didier Dreyfuss',
'type'=>0,
'month_article'=>0,
'famous'=>0,
'views'=>1152,
'show'=>0,
'date'=>'2017-09-06 22:00:00',
'active'=>'1',
'user'=>0,
'created_at'=>'2020-05-15 01:20:06',
'updated_at'=>'2020-06-21 00:08:23',
'deleted_at'=>NULL
] );


    
Post::create( [
'id'=>4638,
'cat_id'=>8,
'tag_id'=>8,
'title'=>'Early versus late initiation of renal replacement therapy impacts mortality in patients with acute kidney injury post cardiac surgery: a meta-analysis',
'text'=>'Critical Care201721:150. June 2107',
'img'=>'file-1463591738-IZ3XFW.jpg',
'file'=>NULL,
'link'=>'https://ccforum.biomedcentral.com/articles/10.1186/s13054-017-1707-0',
'by'=>0,
'journal'=>'Honghong Zou,  Qianwen Hong† and Gaosi XU',
'type'=>0,
'month_article'=>0,
'famous'=>1,
'views'=>1247,
'show'=>0,
'date'=>'2017-05-31 22:00:00',
'active'=>'1',
'user'=>0,
'created_at'=>'2020-05-15 01:20:06',
'updated_at'=>'2020-06-21 00:08:23',
'deleted_at'=>NULL
] );


    
Post::create( [
'id'=>4639,
'cat_id'=>8,
'tag_id'=>4,
'title'=>'Goal-directed Haemodynamic Therapy After Abdominal Surgery',
'text'=>'',
'img'=>'im4.jpg',
'file'=>NULL,
'link'=>'https://healthmanagement.org/c/icu/news/goal-directed-haemodynamic-therapy-after-abdominal-surgery',
'by'=>0,
'journal'=>'ICU Management & Practice, Volume 17 - Issue 1, 2017',
'type'=>0,
'month_article'=>0,
'famous'=>0,
'views'=>1198,
'show'=>0,
'date'=>'2017-09-06 22:00:00',
'active'=>'1',
'user'=>0,
'created_at'=>'2020-05-15 01:20:06',
'updated_at'=>'2020-06-21 00:08:23',
'deleted_at'=>NULL
] );


    
Post::create( [
'id'=>4640,
'cat_id'=>8,
'tag_id'=>11,
'title'=>'Antibiotic Resistance in the ICU: Time to Take Things Seriously!',
'text'=>'',
'img'=>'file-1464106363-HWQX6B.jpg',
'file'=>NULL,
'link'=>'https://healthmanagement.org/c/icu/issuearticle/antibiotic-resistance-in-the-icu-time-to-take-things-seriously',
'by'=>0,
'journal'=>'ICU Management & Practice, Volume 17 - Issue 1, 2017',
'type'=>0,
'month_article'=>0,
'famous'=>0,
'views'=>1101,
'show'=>0,
'date'=>'2017-09-06 22:00:00',
'active'=>'1',
'user'=>0,
'created_at'=>'2020-05-15 01:20:06',
'updated_at'=>'2020-06-21 00:08:23',
'deleted_at'=>NULL
] );


    
Post::create( [
'id'=>4641,
'cat_id'=>8,
'tag_id'=>11,
'title'=>'Antibiotic Resistance in the ICU: Time to Take Things Seriously!',
'text'=>'',
'img'=>'file-1464106363-HWQX6B.jpg',
'file'=>NULL,
'link'=>'https://healthmanagement.org/c/icu/issuearticle/antibiotic-resistance-in-the-icu-time-to-take-things-seriously',
'by'=>0,
'journal'=>'ICU Management & Practice, Volume 17 - Issue 1, 2017',
'type'=>0,
'month_article'=>0,
'famous'=>0,
'views'=>1101,
'show'=>0,
'date'=>'2017-09-06 22:00:00',
'active'=>'1',
'user'=>0,
'created_at'=>'2020-05-15 01:20:06',
'updated_at'=>'2020-06-21 00:08:23',
'deleted_at'=>NULL
] );
Post::create( [
'id'=>4643,
'cat_id'=>9,
'tag_id'=>3,
'title'=>'Applying the 2016 ASPEN/ SCCM Critical Care Guidelines to Your Practice',
'text'=>'',
'img'=>'file-1463591725-9J88SF.jpg',
'file'=>NULL,
'link'=>'http://www.eatright-tn.org/sites/default/files/ASPEN%20Critical%20Care-%20Susan%20Brantley.pdf',
'by'=>0,
'journal'=>'Susan Brantley, MS, RD, LDN',
'type'=>0,
'month_article'=>0,
'famous'=>0,
'views'=>1101,
'show'=>0,
'date'=>'2016-08-31 22:00:00',
'active'=>'1',
'user'=>0,
'created_at'=>'2020-05-15 01:20:06',
'updated_at'=>'2020-06-21 00:08:23',
'deleted_at'=>NULL
] );


        
Post::create( [
'id'=>4644,
'cat_id'=>8,
'tag_id'=>8,
'title'=>'Prognostic value of platelet-to-lymphocyte ratios among critically ill patients with acute kidney injury',
'text'=>'Critical Care 2017, 21:238  Published on: 8 September 2017',
'img'=>'file-1463591746-GEE3E7.jpg',
'file'=>NULL,
'link'=>'https://ccforum.biomedcentral.com/articles/10.1186/s13054-017-1821-z',
'by'=>0,
'journal'=>'Chen-Fei Zheng and others',
'type'=>0,
'month_article'=>0,
'famous'=>0,
'views'=>1152,
'show'=>0,
'date'=>'2017-09-08 22:00:00',
'active'=>'1',
'user'=>0,
'created_at'=>'2020-05-15 01:20:06',
'updated_at'=>'2020-06-21 00:08:23',
'deleted_at'=>NULL
] );


        
Post::create( [
'id'=>4645,
'cat_id'=>8,
'tag_id'=>11,
'title'=>'European Medicines Agency recommends removal of modified-release paracetamol from market',
'text'=>'',
'img'=>'file-1464106363-HWQX6B.jpg',
'file'=>NULL,
'link'=>'http://www.univadis.org/medical-news/596/European-Medicines-Agency-recommends-removal-of-modified-release-paracetamol-from-market?utm_source=newsletter+email&utm_medium=email&utm_campaign=medical+updates+best+of+-+week&utm_content=1607336&utm_term=automated_bestof_weekly',
'by'=>0,
'journal'=>'Univadis Medical NewsSep 5, 2017',
'type'=>0,
'month_article'=>0,
'famous'=>0,
'views'=>1101,
'show'=>0,
'date'=>'2017-09-08 22:00:00',
'active'=>'1',
'user'=>0,
'created_at'=>'2020-05-15 01:20:06',
'updated_at'=>'2020-06-21 00:08:23',
'deleted_at'=>NULL
] );


        
Post::create( [
'id'=>4646,
'cat_id'=>8,
'tag_id'=>3,
'title'=>'PURE study: High carbohydrate intake associated with increased risk of death',
'text'=>'',
'img'=>'file-1463591725-9J88SF.jpg',
'file'=>NULL,
'link'=>'http://www.univadis.org/medical-news/596/PURE-study-High-carbohydrate-intake-associated-with-increased-risk-of-death?utm_source=newsletter+email&utm_medium=email&utm_campaign=medical+updates+best+of+-+week&utm_content=1607336&utm_term=automated_bestof_weekly',
'by'=>0,
'journal'=>'Univadis Medical NewsSep 4, 2017',
'type'=>0,
'month_article'=>0,
'famous'=>0,
'views'=>1101,
'show'=>0,
'date'=>'2017-09-03 22:00:00',
'active'=>'1',
'user'=>0,
'created_at'=>'2020-05-15 01:20:06',
'updated_at'=>'2020-06-21 00:08:23',
'deleted_at'=>NULL
] );


        
Post::create( [
'id'=>4647,
'cat_id'=>8,
'tag_id'=>1,
'title'=>'Ceftazidime-avibactam effective against highly drug-resistant TB',
'text'=>'',
'img'=>'file-1463591752-XK911T.jpg',
'file'=>NULL,
'link'=>'http://www.univadis.org/medical-news/596/Ceftazidime-avibactam-effective-against-highly-drug-resistant-TB',
'by'=>0,
'journal'=>'Univadis Medical NewsSep 4, 2017',
'type'=>0,
'month_article'=>0,
'famous'=>0,
'views'=>1130,
'show'=>0,
'date'=>'2017-09-02 22:00:00',
'active'=>'1',
'user'=>0,
'created_at'=>'2020-05-15 01:20:06',
'updated_at'=>'2020-06-21 00:08:23',
'deleted_at'=>NULL
] );


        
Post::create( [
'id'=>4648,
'cat_id'=>9,
'tag_id'=>6,
'title'=>'IDSA Guidelines for Management of CAPCommunity-Acquired Pneumonia in Adults: Projected Publication... Summer 2107',
'text'=>'',
'img'=>'file-1463591752-XK911T.jpg',
'file'=>NULL,
'link'=>'https://academic.oup.com/cid/article-lookup/doi/10.1086/511159',
'by'=>0,
'journal'=>'Clinical Infectious Disease  March 2017',
'type'=>0,
'month_article'=>0,
'famous'=>0,
'views'=>1101,
'show'=>0,
'date'=>'2017-02-28 22:00:00',
'active'=>'1',
'user'=>0,
'created_at'=>'2020-05-15 01:20:06',
'updated_at'=>'2020-06-21 00:08:23',
'deleted_at'=>NULL
] );


        
Post::create( [
'id'=>4649,
'cat_id'=>12,
'tag_id'=>1,
'title'=>'Colistin vs. Ceftazidime-avibactam in the Treatment of Infections due to Carbapenem-Resistant Enterobacteriaceae',
'text'=>'Clinical Infectious Diseases, 04 September 2017',
'img'=>'file-1464106363-HWQX6B.jpg',
'file'=>NULL,
'link'=>'https://academic.oup.com/cid/article-abstract/doi/10.1093/cid/cix783/4103289/Colistin-vs-Ceftazidime-avibactam-in-the-Treatment',
'by'=>0,
'journal'=>'David van Duin and others',
'type'=>0,
'month_article'=>0,
'famous'=>0,
'views'=>1241,
'show'=>0,
'date'=>'2017-09-11 22:00:00',
'active'=>'1',
'user'=>0,
'created_at'=>'2020-05-15 01:20:06',
'updated_at'=>'2020-06-21 00:08:23',
'deleted_at'=>NULL
] );


        
Post::create( [
'id'=>4650,
'cat_id'=>8,
'tag_id'=>1,
'title'=>'Colistin: An Update on the Antibiotic of the 21st Century',
'text'=>'Expert Review of Anti-Infective Therapy\r\nMedscape Critical Care 2102',
'img'=>'file-1463591639-4YG8JY.jpg',
'file'=>NULL,
'link'=>'http://www.medscape.com/viewarticle/772588_7',
'by'=>0,
'journal'=>'Silpak Biswas and others',
'type'=>0,
'month_article'=>0,
'famous'=>1,
'views'=>1225,
'show'=>0,
'date'=>'2016-09-30 22:00:00',
'active'=>'1',
'user'=>0,
'created_at'=>'2020-05-15 01:20:06',
'updated_at'=>'2020-06-21 00:08:23',
'deleted_at'=>NULL
] );


        
Post::create( [
'id'=>4651,
'cat_id'=>8,
'tag_id'=>6,
'title'=>'Respiratory support in patients with acute respiratory distress syndrome: an expert opinion',
'text'=>'Critical Care 2017, 21:240  Published on: 12 September 2017',
'img'=>'im6.jpg',
'file'=>NULL,
'link'=>'https://ccforum.biomedcentral.com/articles/10.1186/s13054-017-1820-0',
'by'=>0,
'journal'=>'Davide Chiumello and others',
'type'=>0,
'month_article'=>0,
'famous'=>1,
'views'=>1196,
'show'=>0,
'date'=>'2017-09-13 22:00:00',
'active'=>'1',
'user'=>0,
'created_at'=>'2020-05-15 01:20:06',
'updated_at'=>'2020-06-21 00:08:23',
'deleted_at'=>NULL
] );


        
Post::create( [
'id'=>4652,
'cat_id'=>8,
'tag_id'=>5,
'title'=>'One-third of patients develop depression after ICU discharge',
'text'=>'Infection Control & Clinical Quality. September 08, 2017',
'img'=>'file-1463591639-4YG8JY.jpg',
'file'=>NULL,
'link'=>'http://www.beckershospitalreview.com/quality/one-third-of-patients-develop-depression-after-icu-discharge.html',
'by'=>0,
'journal'=>'Written by Anuja Vaidya',
'type'=>0,
'month_article'=>0,
'famous'=>0,
'views'=>1114,
'show'=>0,
'date'=>'2017-09-07 22:00:00',
'active'=>'1',
'user'=>0,
'created_at'=>'2020-05-15 01:20:06',
'updated_at'=>'2020-06-21 00:08:23',
'deleted_at'=>NULL
] );


        
Post::create( [
'id'=>4653,
'cat_id'=>12,
'tag_id'=>6,
'title'=>'Lung Recruitment and Titrated PEEP in Moderate to Severe ARDS. Is the Door Closing on the Open Lung',
'text'=>'JAMA. Published online September 27, 2017',
'img'=>'file-1463591752-XK911T.jpg',
'file'=>NULL,
'link'=>'http://jamanetwork.com/journals/jama/fullarticle/2654890',
'by'=>0,
'journal'=>'Sarina K. Sahetya Roy G. Brower,',
'type'=>0,
'month_article'=>0,
'famous'=>1,
'views'=>1307,
'show'=>0,
'date'=>'2017-09-27 22:00:00',
'active'=>'1',
'user'=>0,
'created_at'=>'2020-05-15 01:20:06',
'updated_at'=>'2020-06-21 00:08:23',
'deleted_at'=>NULL
] );


        
Post::create( [
'id'=>4654,
'cat_id'=>8,
'tag_id'=>6,
'title'=>'LIVES 2017: Lung recruitment and PEEP trial reports results',
'text'=>'Published on : Wed, 27 Sep 2017',
'img'=>'im6.jpg',
'file'=>NULL,
'link'=>'https://healthmanagement.org/c/icu/news/lives-2017-lung-recruitment-and-peep-trial-reports-results',
'by'=>0,
'journal'=>'Health Management.org',
'type'=>0,
'month_article'=>0,
'famous'=>0,
'views'=>1101,
'show'=>0,
'date'=>'2017-08-31 22:00:00',
'active'=>'1',
'user'=>0,
'created_at'=>'2020-05-15 01:20:06',
'updated_at'=>'2020-06-21 00:08:23',
'deleted_at'=>NULL
] );


        
Post::create( [
'id'=>4655,
'cat_id'=>8,
'tag_id'=>4,
'title'=>'Perioperative Management of High-Risk Patients Going Beyond “Avoid Hypoxia and Hypotension”',
'text'=>'JAMA. Published online September 27, 2017',
'img'=>'im4.jpg',
'file'=>NULL,
'link'=>'http://jamanetwork.com/journals/jama/fullarticle/2654891',
'by'=>0,
'journal'=>'Solomon Aronson Monty G. Mythen',
'type'=>0,
'month_article'=>0,
'famous'=>0,
'views'=>1198,
'show'=>0,
'date'=>'2017-09-30 22:00:00',
'active'=>'1',
'user'=>0,
'created_at'=>'2020-05-15 01:20:06',
'updated_at'=>'2020-06-21 00:08:23',
'deleted_at'=>NULL
] );


        
Post::create( [
'id'=>4656,
'cat_id'=>8,
'tag_id'=>11,
'title'=>'Admitting Elderly Patients to the Intensive Care Unit—Is it the Right Decision',
'text'=>'JAMA. Published online September 27, 2017',
'img'=>'file-1464165463-D3CBM9.jpg',
'file'=>NULL,
'link'=>'http://jamanetwork.com/journals/jama/fullarticle/2654892',
'by'=>0,
'journal'=>'Derek C. Angus',
'type'=>0,
'month_article'=>0,
'famous'=>0,
'views'=>1101,
'show'=>0,
'date'=>'2017-09-27 22:00:00',
'active'=>'1',
'user'=>0,
'created_at'=>'2020-05-15 01:20:06',
'updated_at'=>'2020-06-21 00:08:23',
'deleted_at'=>NULL
] );


        
Post::create( [
'id'=>4657,
'cat_id'=>8,
'tag_id'=>11,
'title'=>'Effect of Systematic Intensive Care Unit Triage on Long-term Mortality Among Critically Ill Elderly Patients in FranceA Randomized Clinical Trial',
'text'=>'JAMA. Published online September 27, 2017',
'img'=>'file-1464165480-R231XR.jpg',
'file'=>NULL,
'link'=>'http://jamanetwork.com/journals/jama/fullarticle/2654893',
'by'=>0,
'journal'=>'Bertrand Guidet Guillaume Leblanc Tabassome Simon et al.',
'type'=>0,
'month_article'=>0,
'famous'=>0,
'views'=>1101,
'show'=>0,
'date'=>'2017-09-30 22:00:00',
'active'=>'1',
'user'=>0,
'created_at'=>'2020-05-15 01:20:06',
'updated_at'=>'2020-06-21 00:08:23',
'deleted_at'=>NULL
] );


        
Post::create( [
'id'=>4658,
'cat_id'=>8,
'tag_id'=>5,
'title'=>'Effect of Routine Low-Dose Oxygen Supplementation on Death and Disability in Adults With Acute StrokeThe Stroke Oxygen Study Randomized Clinical Trial',
'text'=>'JAMA. 26 Sept. 201731812:1125-1135',
'img'=>'file-1463591639-4YG8JY.jpg',
'file'=>NULL,
'link'=>'http://jamanetwork.com/journals/jama/article-abstract/2654819',
'by'=>0,
'journal'=>'Christine Roffe Tracy Nevatte Julius Sim et al.',
'type'=>0,
'month_article'=>0,
'famous'=>0,
'views'=>1114,
'show'=>0,
'date'=>'2017-09-30 22:00:00',
'active'=>'1',
'user'=>0,
'created_at'=>'2020-05-15 01:20:06',
'updated_at'=>'2020-06-21 00:08:23',
'deleted_at'=>NULL
] );


        
Post::create( [
'id'=>4659,
'cat_id'=>8,
'tag_id'=>11,
'title'=>'Delirium in Older PersonsAdvances in Diagnosis and Treatment',
'text'=>'JAMA. 201731812:1161-1174',
'img'=>'im2.jpg',
'file'=>NULL,
'link'=>'http://jamanetwork.com/journals/jama/article-abstract/2654826',
'by'=>0,
'journal'=>'Esther S. Oh,and others',
'type'=>0,
'month_article'=>0,
'famous'=>0,
'views'=>1101,
'show'=>0,
'date'=>'2017-09-30 22:00:00',
'active'=>'1',
'user'=>0,
'created_at'=>'2020-05-15 01:20:06',
'updated_at'=>'2020-06-21 00:08:23',
'deleted_at'=>NULL
] );


        
Post::create( [
'id'=>4660,
'cat_id'=>8,
'tag_id'=>10,
'title'=>'Age of Red Cells for Transfusion and Outcomes in Critically Ill Adults',
'text'=>'NEJM September 27, 2017',
'img'=>'file-1463591685-DDLMK9.jpg',
'file'=>NULL,
'link'=>'http://www.nejm.org/doi/full/10.1056/NEJMoa1707572queryOF',
'by'=>0,
'journal'=>'D.J. Cooper and Others, for the TRANSFUSE Investigators and the Australian and New Zealand Intensive Care Society Clinical Trials Group',
'type'=>0,
'month_article'=>0,
'famous'=>1,
'views'=>1196,
'show'=>0,
'date'=>'2017-09-30 22:00:00',
'active'=>'1',
'user'=>0,
'created_at'=>'2020-05-15 01:20:06',
'updated_at'=>'2020-06-21 00:08:23',
'deleted_at'=>NULL
] );


        
Post::create( [
'id'=>4661,
'cat_id'=>12,
'tag_id'=>7,
'title'=>'2017 ACC/AHA/HFSA Focused Update Guideline for the Management of Heart Failure',
'text'=>'American Journal of Cardiology 2017',
'img'=>'file-1463591644-ZGN2DA.jpg',
'file'=>NULL,
'link'=>'http://www.acc.org/latest-in-cardiology/ten-points-to-remember/2017/04/27/15/50/2017-acc-aha-hfsa-focused-update-of-hf-guideline',
'by'=>0,
'journal'=>'Yancy CW, Jessup M, Bozkurt B, et al.',
'type'=>0,
'month_article'=>0,
'famous'=>0,
'views'=>1212,
'show'=>0,
'date'=>'2017-08-31 22:00:00',
'active'=>'1',
'user'=>0,
'created_at'=>'2020-05-15 01:20:06',
'updated_at'=>'2020-06-21 00:08:23',
'deleted_at'=>NULL
] );


        
Post::create( [
'id'=>4662,
'cat_id'=>9,
'tag_id'=>7,
'title'=>'2016 ESC Guidelines for the diagnosis and treatment of acute and chronic heart failure',
'text'=>'European Heart Journal 2016 37, 2129–2200',
'img'=>'file-1463591639-4YG8JY.jpg',
'file'=>NULL,
'link'=>'https://watermark.silverchair.com/api/watermark?token=AQECAHi208BE49Ooan9kkhW_Ercy7Dm3ZL_9Cf3qfKAc485ysgAAAeEwggHdBgkqhkiG9w0BBwagggHOMIIBygIBADCCAcMGCSqGSIb3DQEHATAeBglghkgBZQMEAS4wEQQMxo_qsvyGTk1Djc2SAgEQgIIBlOt-LC-u8OiIuDhp1VsTW3FgB87QBqvfMmvJgiw_MK3RrfQhtN2HA5KfK6bC5Q7Y99Ta7Jt5JnQREqVrCPV25rjtwLVEuxHJ7PkCf7czz2UXHspbFwjAgBeQC15_bGksXnHlrVuCdnQDw3psCABy9bt5gXKDBjcgujQCTvBqeE0D0I-t-uA_t-LHWBQAdxNdCJQs00wjwLP5OTm9-wIcGL4K-Y2bdh7GYr_eBmxxMr6YW5PLx0FzUHD0ET5axueRjgVlTaAGHJp5KaXFvKCtQeBhNDsQnTPX-X7TuMlHVtCmVWBdCfprXtAcDzK7u-TefTnoHSkhX0J2uivEXNr0Tepk5tdWWIQ6dc0I6kPF5PoWC0rZrBYtL9WFhq-9i2s0ADMkatPSDt0yn-UI4yXfPrwHZkMfzTFMsmf7mfW2XBS-ZJAAKlVUvVns6R8w8krshScg5A771ochLg9N6DFEuT_9kMjAJV3-B0jPH9JvSdw5xY1C7l5yGXcfq_3Bdi47IUUbF-gdUiHa5g0C0oPMWYK_R2kH',
'by'=>0,
'journal'=>'Piotr Ponikowski and others',
'type'=>0,
'month_article'=>0,
'famous'=>0,
'views'=>1101,
'show'=>0,
'date'=>'2016-09-27 22:00:00',
'active'=>'1',
'user'=>0,
'created_at'=>'2020-05-15 01:20:06',
'updated_at'=>'2020-06-21 00:08:23',
'deleted_at'=>NULL
] );


        
Post::create( [
'id'=>4663,
'cat_id'=>8,
'tag_id'=>11,
'title'=>'Efficacy and Safety of Combination Therapy of Shenfu Injection and Postresuscitation Bundle in Patients With Return of Spontaneous Circulation After In-Hospital Cardiac Arrest: A Randomized, Assessor-Blinded, Controlled Trial',
'text'=>'Critical Care Medicine. 4510:1587-1595, October 2017',
'img'=>'file-1463591644-ZGN2DA.jpg',
'file'=>NULL,
'link'=>'http://journals.lww.com/ccmjournal/Fulltext/2017/10000/Efficacy_and_Safety_of_Combination_Therapy_of.1.aspx',
'by'=>0,
'journal'=>'Zhang, Qian and others',
'type'=>0,
'month_article'=>0,
'famous'=>0,
'views'=>1101,
'show'=>0,
'date'=>'2017-10-03 22:00:00',
'active'=>'1',
'user'=>0,
'created_at'=>'2020-05-15 01:20:06',
'updated_at'=>'2020-06-21 00:08:23',
'deleted_at'=>NULL
] );


        
Post::create( [
'id'=>4664,
'cat_id'=>8,
'tag_id'=>5,
'title'=>'Severe Respiratory Failure, Extracorporeal Membrane Oxygenation, and Intracranial Hemorrhage',
'text'=>'Critical Care Medicine. 4510:1642-1649, October 2017',
'img'=>'im1.jpg',
'file'=>NULL,
'link'=>'http://journals.lww.com/ccmjournal/Abstract/2017/10000/Severe_Respiratory_Failure,_Extracorporeal.7.aspx',
'by'=>0,
'journal'=>'Lockie, Christopher J. A and others',
'type'=>0,
'month_article'=>0,
'famous'=>0,
'views'=>1114,
'show'=>0,
'date'=>'2017-10-03 22:00:00',
'active'=>'1',
'user'=>0,
'created_at'=>'2020-05-15 01:20:06',
'updated_at'=>'2020-06-21 00:08:23',
'deleted_at'=>NULL
] );


        
Post::create( [
'id'=>4665,
'cat_id'=>8,
'tag_id'=>6,
'title'=>'Critically Ill Patients With the Middle East Respiratory Syndrome: A Multicenter Retrospective Cohort Study',
'text'=>'Critical Care Medicine. 4510:1683-1695, October 2017',
'img'=>'file-1463591752-XK911T.jpg',
'file'=>NULL,
'link'=>'http://journals.lww.com/ccmjournal/Fulltext/2017/10000/Critically_Ill_Patients_With_the_Middle_East.12.aspx',
'by'=>0,
'journal'=>'Arabi, Yaseen M. Al-Omari, Awad and others',
'type'=>0,
'month_article'=>0,
'famous'=>0,
'views'=>1101,
'show'=>0,
'date'=>'2017-10-01 22:00:00',
'active'=>'1',
'user'=>0,
'created_at'=>'2020-05-15 01:20:06',
'updated_at'=>'2020-06-21 00:08:23',
'deleted_at'=>NULL
] );


        
Post::create( [
'id'=>4666,
'cat_id'=>8,
'tag_id'=>11,
'title'=>'Is There a Role for Enterohormones in the Gastroparesis of Critically Ill Patients',
'text'=>'Critical Care Medicine. 4510:1696-1701, October 2017',
'img'=>'file-1463591650-J14MMU.jpg',
'file'=>NULL,
'link'=>'http://journals.lww.com/ccmjournal/Abstract/2017/10000/Is_There_a_Role_for_Enterohormones_in_the.13.aspx',
'by'=>0,
'journal'=>'Santacruz, Carlos A and others',
'type'=>0,
'month_article'=>0,
'famous'=>0,
'views'=>1101,
'show'=>0,
'date'=>'2017-10-02 22:00:00',
'active'=>'1',
'user'=>0,
'created_at'=>'2020-05-15 01:20:06',
'updated_at'=>'2020-06-21 00:08:23',
'deleted_at'=>NULL
] );


        
Post::create( [
'id'=>4667,
'cat_id'=>8,
'tag_id'=>1,
'title'=>'Readmissions for Recurrent Sepsis: New or Relapsed Infection',
'text'=>'Critical Care Medicine. 4510:1702-1708, October 2017',
'img'=>'im5.jpg',
'file'=>NULL,
'link'=>'http://journals.lww.com/ccmjournal/Abstract/2017/10000/Readmissions_for_Recurrent_Sepsis___New_or.14.aspx',
'by'=>0,
'journal'=>'DeMerle, Kimberley Marie and others',
'type'=>0,
'month_article'=>0,
'famous'=>0,
'views'=>1130,
'show'=>0,
'date'=>'2017-10-02 22:00:00',
'active'=>'1',
'user'=>0,
'created_at'=>'2020-05-15 01:20:06',
'updated_at'=>'2020-06-21 00:08:23',
'deleted_at'=>NULL
] );


        
Post::create( [
'id'=>4668,
'cat_id'=>8,
'tag_id'=>6,
'title'=>'Prevalence and Prognostic Association of Circulating Troponin in the Acute Respiratory Distress Syndrome',
'text'=>'Critical Care Medicine. 4510:1709-1717, October 2017',
'img'=>'file-1463591639-4YG8JY.jpg',
'file'=>NULL,
'link'=>'http://journals.lww.com/ccmjournal/Abstract/2017/10000/Prevalence_and_Prognostic_Association_of.15.aspx',
'by'=>0,
'journal'=>'Metkus, Thomas S and others',
'type'=>0,
'month_article'=>0,
'famous'=>0,
'views'=>1101,
'show'=>0,
'date'=>'2017-10-03 22:00:00',
'active'=>'1',
'user'=>0,
'created_at'=>'2020-05-15 01:20:06',
'updated_at'=>'2020-06-21 00:08:23',
'deleted_at'=>NULL
] );


        
Post::create( [
'id'=>4669,
'cat_id'=>9,
'tag_id'=>3,
'title'=>'Summary of ASPEN Guidelines 2016',
'text'=>'',
'img'=>'file-1463591650-J14MMU.jpg',
'file'=>NULL,
'link'=>'http://kathrynattema.weebly.com/uploads/2/6/1/9/26190090/aspen_guidelines_2016_cheat_sheet.pdf',
'by'=>0,
'journal'=>'ARRMC 2016 – Attema/Hardwick',
'type'=>0,
'month_article'=>0,
'famous'=>0,
'views'=>1101,
'show'=>0,
'date'=>'2016-11-30 22:00:00',
'active'=>'1',
'user'=>0,
'created_at'=>'2020-05-15 01:20:06',
'updated_at'=>'2020-06-21 00:08:23',
'deleted_at'=>NULL
] );


        
Post::create( [
'id'=>4670,
'cat_id'=>8,
'tag_id'=>3,
'title'=>'Omega-6 may help prevent type 2 diabetes',
'text'=>'The findings are published in The Lancet Diabetes & Endocrinology',
'img'=>'file-1463591732-SM39NY.jpg',
'file'=>NULL,
'link'=>'http://www.univadis.md/medical-news/596/Omega-6-may-help-prevent-type-2-diabetes?utm_source=newsletter+email&utm_medium=email&utm_campaign=medical+updates+-+weekly&utm_content=1688197&utm_term=automated_weekly',
'by'=>0,
'journal'=>'Univadis Medical News. Monday 16 October 2017',
'type'=>0,
'month_article'=>0,
'famous'=>0,
'views'=>1101,
'show'=>0,
'date'=>'2017-10-15 22:00:00',
'active'=>'1',
'user'=>0,
'created_at'=>'2020-05-15 01:20:06',
'updated_at'=>'2020-06-21 00:08:23',
'deleted_at'=>NULL
] );


        
Post::create( [
'id'=>4671,
'cat_id'=>8,
'tag_id'=>7,
'title'=>'Dual Antithrombotic Therapy with Dabigatran after PCI in Atrial Fibrillation',
'text'=>'N Engl J Med 2017377:1513-1524',
'img'=>'file-1463591644-ZGN2DA.jpg',
'file'=>NULL,
'link'=>'http://www.nejm.org/doi/full/10.1056/NEJMoa1708454queryfeatured_home',
'by'=>0,
'journal'=>'C.P. Cannon and Others',
'type'=>0,
'month_article'=>0,
'famous'=>0,
'views'=>1101,
'show'=>0,
'date'=>'2017-10-19 22:00:00',
'active'=>'1',
'user'=>0,
'created_at'=>'2020-05-15 01:20:06',
'updated_at'=>'2020-06-21 00:08:23',
'deleted_at'=>NULL
] );

        
Post::create( [
'id'=>4762,
'cat_id'=>8,
'tag_id'=>11,
'title'=>'Transfers from intensive care unit to hospital ward: a multicentre textual analysis of physician progress notes',
'text'=>'Critical Care 2018, 22:19  Published on: 28 January 2018',
'img'=>'file-1464165480-R231XR.jpg',
'file'=>NULL,
'link'=>'https://ccforum.biomedcentral.com/articles/10.1186/s13054-018-1941-0',
'by'=>0,
'journal'=>'Kyla N. Brown and others',
'type'=>0,
'month_article'=>0,
'famous'=>0,
'views'=>1101,
'show'=>0,
'date'=>'2018-01-29 22:00:00',
'active'=>'1',
'user'=>0,
'created_at'=>'2020-05-15 01:20:06',
'updated_at'=>'2020-06-21 00:08:23',
'deleted_at'=>NULL
] );


        
Post::create( [
'id'=>4763,
'cat_id'=>12,
'tag_id'=>11,
'title'=>'Stress ulcer prophylaxis in intensive care unit patients receiving enteral nutrition: a systematic review and meta-analysis',
'text'=>'Critical Care 2018, 22:20  Published on: 28 January 2018',
'img'=>'file-1463591650-J14MMU.jpg',
'file'=>NULL,
'link'=>'https://ccforum.biomedcentral.com/articles/10.1186/s13054-017-1937-1',
'by'=>0,
'journal'=>'Hui-Bin Huang and others',
'type'=>0,
'month_article'=>0,
'famous'=>0,
'views'=>1212,
'show'=>0,
'date'=>'2018-01-29 22:00:00',
'active'=>'1',
'user'=>0,
'created_at'=>'2020-05-15 01:20:06',
'updated_at'=>'2020-06-21 00:08:23',
'deleted_at'=>NULL
] );


        
Post::create( [
'id'=>4764,
'cat_id'=>9,
'tag_id'=>6,
'title'=>'Respiratory support in patients with acute respiratory distress syndrome: an expert opinion',
'text'=>'Critical Care 2017: 21:240',
'img'=>'file-1463591644-ZGN2DA.jpg',
'file'=>NULL,
'link'=>'https://ccforum.biomedcentral.com/articles/10.1186/s13054-017-1820-0/?utm_source=BMC_mailing&utm_medium=Email_Internal&utm_content=AJs_Health_BMC-MultiJournal-Hybris_dynamic-Multidisciplinary-Usage_driving-Global&utm_campaign=ElaLee-BMC-AJH_USG_BMC_Q4_2017',
'by'=>0,
'journal'=>'Davide Chiumello, Laurent Brochard, John J. Marini et al',
'type'=>0,
'month_article'=>0,
'famous'=>0,
'views'=>1101,
'show'=>0,
'date'=>'2017-09-29 22:00:00',
'active'=>'1',
'user'=>0,
'created_at'=>'2020-05-15 01:20:06',
'updated_at'=>'2020-06-21 00:08:23',
'deleted_at'=>NULL
] );


        
Post::create( [
'id'=>4765,
'cat_id'=>9,
'tag_id'=>10,
'title'=>'The European guideline on management of major bleeding and coagulopathy following trauma: fourth edition',
'text'=>'Critical Care. 2016: 20:100',
'img'=>'file-1463591681-AGRM7H.jpg',
'file'=>NULL,
'link'=>'https://ccforum.biomedcentral.com/articles/10.1186/s13054-016-1265-x/?utm_source=BMC_mailing&utm_medium=Email_Internal&utm_content=AJs_Health_BMC-MultiJournal-Hybris_dynamic-Multidisciplinary-Usage_driving-Global&utm_campaign=ElaLee-BMC-AJH_USG_BMC_Q4_2017',
'by'=>0,
'journal'=>'Rolf Rossaint, Bertil Bouillon, Vladimir Cerny et al.',
'type'=>0,
'month_article'=>0,
'famous'=>0,
'views'=>1101,
'show'=>0,
'date'=>'2016-11-30 22:00:00',
'active'=>'1',
'user'=>0,
'created_at'=>'2020-05-15 01:20:06',
'updated_at'=>'2020-06-21 00:08:23',
'deleted_at'=>NULL
] );


        
Post::create( [
'id'=>4766,
'cat_id'=>12,
'tag_id'=>3,
'title'=>'Enteral vs Parenteral Nutrition in Critical Care Requiring Mechanical Ventilation',
'text'=>'Lancet. 2018391:133-143',
'img'=>'file-1463591732-SM39NY.jpg',
'file'=>NULL,
'link'=>'http://www.thelancet.com/journals/lancet/article/PIIS0140-6736(17)32146-3/fulltext',
'by'=>0,
'journal'=>'Jean Reignie and others',
'type'=>0,
'month_article'=>0,
'famous'=>0,
'views'=>1213,
'show'=>0,
'date'=>'2018-01-30 22:00:00',
'active'=>'1',
'user'=>0,
'created_at'=>'2020-05-15 01:20:06',
'updated_at'=>'2022-09-04 23:04:37',
'deleted_at'=>NULL
] );


        
Post::create( [
'id'=>4767,
'cat_id'=>8,
'tag_id'=>7,
'title'=>'Paradigm Shift for Treatment of Atrial Fibrillation in Heart Failure',
'text'=>'N Engl J Med 2018378:468-469',
'img'=>'file-1463591639-4YG8JY.jpg',
'file'=>NULL,
'link'=>'http://www.nejm.org/doi/full/10.1056/NEJMe1714782?query=TOC',
'by'=>0,
'journal'=>'M.S. Link',
'type'=>0,
'month_article'=>0,
'famous'=>0,
'views'=>1101,
'show'=>0,
'date'=>'2018-01-31 22:00:00',
'active'=>'1',
'user'=>0,
'created_at'=>'2020-05-15 01:20:06',
'updated_at'=>'2020-06-21 00:08:23',
'deleted_at'=>NULL
] );


        
Post::create( [
'id'=>4768,
'cat_id'=>12,
'tag_id'=>6,
'title'=>'Updated Guidelines for Noninvasive Ventilation in Acute Respiratory Failure',
'text'=>'Pulmonology Advisors',
'img'=>'file-1463591657-K8BRDF.jpg',
'file'=>NULL,
'link'=>'https://www.pulmonologyadvisor.com/restrictive-lung-disease/noninvasive-ventilation-guidelines-in-acute-respiratory-failure/article/700022/',
'by'=>0,
'journal'=>'Anna Kitabjian, MSN RN',
'type'=>0,
'month_article'=>0,
'famous'=>0,
'views'=>1212,
'show'=>0,
'date'=>'2018-01-31 22:00:00',
'active'=>'1',
'user'=>0,
'created_at'=>'2020-05-15 01:20:06',
'updated_at'=>'2020-06-21 00:08:23',
'deleted_at'=>NULL
] );


        
Post::create( [
'id'=>4769,
'cat_id'=>9,
'tag_id'=>6,
'title'=>'Official ERS/ATS clinical practice guidelines: noninvasive ventilation for acute respiratory failure',
'text'=>'European Respiratory Journal 2017 50: 1602426',
'img'=>'im6.jpg',
'file'=>NULL,
'link'=>'http://erj.ersjournals.com/content/50/2/1602426.short',
'by'=>0,
'journal'=>'Bram Rochwerg and others',
'type'=>0,
'month_article'=>0,
'famous'=>0,
'views'=>1101,
'show'=>0,
'date'=>'2017-11-30 22:00:00',
'active'=>'1',
'user'=>0,
'created_at'=>'2020-05-15 01:20:06',
'updated_at'=>'2020-06-21 00:08:23',
'deleted_at'=>NULL
] );


        
Post::create( [
'id'=>4770,
'cat_id'=>8,
'tag_id'=>6,
'title'=>'Variation of poorly ventilated lung units silent spaces measured by electrical impedance tomography to dynamically assess recruitment',
'text'=>'Critical Care 2018, 22:26  Published on: 31 January 2018',
'img'=>'im5.jpg',
'file'=>NULL,
'link'=>'https://ccforum.biomedcentral.com/articles/10.1186/s13054-017-1931-7',
'by'=>0,
'journal'=>'Savino Spadaro and others',
'type'=>0,
'month_article'=>0,
'famous'=>0,
'views'=>1101,
'show'=>0,
'date'=>'2018-01-31 22:00:00',
'active'=>'1',
'user'=>0,
'created_at'=>'2020-05-15 01:20:06',
'updated_at'=>'2020-06-21 00:08:23',
'deleted_at'=>NULL
] );


        
Post::create( [
'id'=>4771,
'cat_id'=>8,
'tag_id'=>6,
'title'=>'Prevention of Ventilator-Associated Pneumonia: The Multimodal Approach of the Spanish ICU “Pneumonia Zero” Program',
'text'=>'Critical Care Medicine. 462:181-188, February 2018',
'img'=>'file-1463591752-XK911T.jpg',
'file'=>NULL,
'link'=>'https://journals.lww.com/ccmjournal/Fulltext/2018/02000/Prevention_of_Ventilator_Associated_Pneumonia__.2.aspx',
'by'=>0,
'journal'=>'Álvarez-Lerma and others',
'type'=>0,
'month_article'=>0,
'famous'=>0,
'views'=>1101,
'show'=>0,
'date'=>'2018-01-31 22:00:00',
'active'=>'1',
'user'=>0,
'created_at'=>'2020-05-15 01:20:06',
'updated_at'=>'2020-06-21 00:08:23',
'deleted_at'=>NULL
] );


        
Post::create( [
'id'=>4772,
'cat_id'=>8,
'tag_id'=>6,
'title'=>'Predictors of Intubation in Patients With Acute Hypoxemic Respiratory Failure Treated With a Noninvasive Oxygenation Strategy',
'text'=>'Critical Care Medicine. 462:208-215, February 2018',
'img'=>'im5.jpg',
'file'=>NULL,
'link'=>'https://journals.lww.com/ccmjournal/Abstract/2018/02000/Predictors_of_Intubation_in_Patients_With_Acute.5.aspx',
'by'=>0,
'journal'=>'Frat, Jean-Pierre and others',
'type'=>0,
'month_article'=>0,
'famous'=>0,
'views'=>1101,
'show'=>0,
'date'=>'2018-01-31 22:00:00',
'active'=>'1',
'user'=>0,
'created_at'=>'2020-05-15 01:20:06',
'updated_at'=>'2020-06-21 00:08:23',
'deleted_at'=>NULL
] );


        
Post::create( [
'id'=>4773,
'cat_id'=>8,
'tag_id'=>8,
'title'=>'Renal Decapsulation Prevents Intrinsic Renal Compartment Syndrome in Ischemia-Reperfusion–Induced Acute Kidney Injury: A Physiologic Approach',
'text'=>'Critical Care Medicine. 462:216-222, February 2018',
'img'=>'file-1463591746-GEE3E7.jpg',
'file'=>NULL,
'link'=>'https://journals.lww.com/ccmjournal/Abstract/2018/02000/Renal_Decapsulation_Prevents_Intrinsic_Renal.6.aspx',
'by'=>0,
'journal'=>'Cruces, Pablo and others',
'type'=>0,
'month_article'=>0,
'famous'=>0,
'views'=>1152,
'show'=>0,
'date'=>'2018-01-31 22:00:00',
'active'=>'1',
'user'=>0,
'created_at'=>'2020-05-15 01:20:06',
'updated_at'=>'2020-06-21 00:08:23',
'deleted_at'=>NULL
] );


        
Post::create( [
'id'=>4774,
'cat_id'=>8,
'tag_id'=>10,
'title'=>'Should Transfusion Trigger Thresholds Differ for Critical Care Versus Perioperative Patients A Meta-Analysis of Randomized Trials',
'text'=>'Critical Care Medicine. 462:252-263, February 2018',
'img'=>'file-1463591685-DDLMK9.jpg',
'file'=>NULL,
'link'=>'https://journals.lww.com/ccmjournal/Fulltext/2018/02000/Should_Transfusion_Trigger_Thresholds_Differ_for.11.aspx',
'by'=>0,
'journal'=>'Chong, Matthew A and others',
'type'=>0,
'month_article'=>0,
'famous'=>0,
'views'=>1101,
'show'=>0,
'date'=>'2018-01-31 22:00:00',
'active'=>'1',
'user'=>0,
'created_at'=>'2020-05-15 01:20:06',
'updated_at'=>'2020-06-21 00:08:23',
'deleted_at'=>NULL
] );

Post::create( [
'id'=>4775,
'cat_id'=>8,
'tag_id'=>1,
'title'=>'Prolonged Infusion Piperacillin-Tazobactam Decreases Mortality and Improves Outcomes in Severely Ill Patients: Results of a Systematic Review and Meta-Analysis',
'text'=>'Critical Care Medicine. 462:236-243, February 2018',
'img'=>'file-1464165463-D3CBM9.jpg',
'file'=>NULL,
'link'=>'https://journals.lww.com/ccmjournal/Abstract/2018/02000/Prolonged_Infusion_Piperacillin_Tazobactam.9.aspx',
'by'=>0,
'journal'=>'Rhodes, Nathaniel J and others',
'type'=>0,
'month_article'=>0,
'famous'=>0,
'views'=>1130,
'show'=>0,
'date'=>'2018-01-31 22:00:00',
'active'=>'1',
'user'=>0,
'created_at'=>'2020-05-15 01:20:06',
'updated_at'=>'2020-06-21 00:08:23',
'deleted_at'=>NULL
] );


            
Post::create( [
'id'=>4776,
'cat_id'=>8,
'tag_id'=>11,
'title'=>'Association of Driving Pressure With Mortality Among Ventilated Patients With Acute Respiratory Distress Syndrome: A Systematic Review and Meta-Analysis',
'text'=>'Critical Care Medicine. 462:300-306, February 2018',
'img'=>'im6.jpg',
'file'=>NULL,
'link'=>'https://journals.lww.com/ccmjournal/Abstract/2018/02000/Association_of_Driving_Pressure_With_Mortality.16.aspx',
'by'=>0,
'journal'=>'Aoyama, Hiroko and others',
'type'=>0,
'month_article'=>0,
'famous'=>0,
'views'=>1101,
'show'=>0,
'date'=>'2018-01-31 22:00:00',
'active'=>'1',
'user'=>0,
'created_at'=>'2020-05-15 01:20:06',
'updated_at'=>'2020-06-21 00:08:23',
'deleted_at'=>NULL
] );


            
Post::create( [
'id'=>4777,
'cat_id'=>8,
'tag_id'=>1,
'title'=>'Effects of procalcitonin-guided antibiotic treatment in acute respiratory infections',
'text'=>'Lancet Infect Dis. 2018181:95-107',
'img'=>'file-1463591639-4YG8JY.jpg',
'file'=>NULL,
'link'=>'http://www.thelancet.com/journals/laninf/article/PIIS1473-3099(17)30592-3/fulltext',
'by'=>0,
'journal'=>'Schuetz P and others',
'type'=>0,
'month_article'=>0,
'famous'=>0,
'views'=>1130,
'show'=>0,
'date'=>'2018-01-31 22:00:00',
'active'=>'1',
'user'=>0,
'created_at'=>'2020-05-15 01:20:06',
'updated_at'=>'2020-06-21 00:08:23',
'deleted_at'=>NULL
] );


            
Post::create( [
'id'=>4778,
'cat_id'=>8,
'tag_id'=>1,
'title'=>'Mixing, cycling antibiotics in ICU does not reduce resistance',
'text'=>'Infectious Disease News ITJ Plus, February 2018',
'img'=>'file-1464165463-D3CBM9.jpg',
'file'=>NULL,
'link'=>'https://www.healio.com/infectious-disease/antimicrobials/news/in-the-journals/%7bfcdd50aa-cf4c-4fd7-8fb8-144054ed5c00%7d/antibiotic-mixing-cycling-strategies-fail-to-reduce-resistance-in-icus',
'by'=>0,
'journal'=>'Pleun Joppe van Duijin',
'type'=>0,
'month_article'=>0,
'famous'=>0,
'views'=>1130,
'show'=>0,
'date'=>'2018-01-31 22:00:00',
'active'=>'1',
'user'=>0,
'created_at'=>'2020-05-15 01:20:06',
'updated_at'=>'2020-06-21 00:08:23',
'deleted_at'=>NULL
] );


            
Post::create( [
'id'=>4779,
'cat_id'=>8,
'tag_id'=>1,
'title'=>'CDC reports highest flu hospitalization rate in nearly 10 years',
'text'=>'CNN, Updated 1150 GMT 1950 HKT February 5, 2018',
'img'=>'file-1464165480-R231XR.jpg',
'file'=>NULL,
'link'=>'https://edition.cnn.com/2018/02/02/health/flu-weekly-surveillance-feb-2/index.html',
'by'=>0,
'journal'=>'By Susan Scutti',
'type'=>0,
'month_article'=>0,
'famous'=>0,
'views'=>1130,
'show'=>0,
'date'=>'2018-02-05 22:00:00',
'active'=>'1',
'user'=>0,
'created_at'=>'2020-05-15 01:20:06',
'updated_at'=>'2020-06-21 00:08:23',
'deleted_at'=>NULL
] );


            
Post::create( [
'id'=>4780,
'cat_id'=>12,
'tag_id'=>6,
'title'=>'Tracheal intubation in critical care - expert guidelines',
'text'=>'ICU Management & Practice. Tue, 6 Feb 2018',
'img'=>'file-1463591657-K8BRDF.jpg',
'file'=>NULL,
'link'=>'https://healthmanagement.org/c/icu/news/tracheal-intubation-in-critical-care-expert-guidelines',
'by'=>0,
'journal'=>'Source: British Journal of Anaesthesia',
'type'=>0,
'month_article'=>0,
'famous'=>0,
'views'=>1212,
'show'=>0,
'date'=>'2018-02-10 22:00:00',
'active'=>'1',
'user'=>0,
'created_at'=>'2020-05-15 01:20:06',
'updated_at'=>'2020-06-21 00:08:23',
'deleted_at'=>NULL
] );


            
Post::create( [
'id'=>4781,
'cat_id'=>9,
'tag_id'=>6,
'title'=>'Guidelines for the management of tracheal intubation in critically ill adults',
'text'=>'Br J Anesth. 1202:323-35',
'img'=>'im4.jpg',
'file'=>NULL,
'link'=>'http://bjanaesthesia.org/article/S0007-0912(17)54060-X/fulltext',
'by'=>0,
'journal'=>'Higgs A et al. on behalf of Difficult Airway Society, Intensive Care Society, Faculty of Intensive Care Medicine, Royal College of Anaesthetists 2018',
'type'=>0,
'month_article'=>0,
'famous'=>0,
'views'=>1101,
'show'=>0,
'date'=>'2018-02-10 22:00:00',
'active'=>'1',
'user'=>0,
'created_at'=>'2020-05-15 01:20:06',
'updated_at'=>'2020-06-21 00:08:23',
'deleted_at'=>NULL
] );


            
Post::create( [
'id'=>4782,
'cat_id'=>8,
'tag_id'=>11,
'title'=>'Copeptin levels and commonly used laboratory parameters in hospitalised patients with severe hypernatraemia - the “Co-MED study”',
'text'=>'Critical Care 2018, 22:33  Published on: 9 February 2018',
'img'=>'file-1464165463-D3CBM9.jpg',
'file'=>NULL,
'link'=>'https://ccforum.biomedcentral.com/articles/10.1186/s13054-018-1955-7',
'by'=>0,
'journal'=>'Nicole Nigro and others',
'type'=>0,
'month_article'=>0,
'famous'=>0,
'views'=>1101,
'show'=>0,
'date'=>'2018-02-07 22:00:00',
'active'=>'1',
'user'=>0,
'created_at'=>'2020-05-15 01:20:06',
'updated_at'=>'2020-06-21 00:08:23',
'deleted_at'=>NULL
] );


            
Post::create( [
'id'=>4783,
'cat_id'=>8,
'tag_id'=>7,
'title'=>'Cardiac output monitoring: throw it out… or keep it',
'text'=>'Critical Care 2018, 22:35  Published on: 8 February 2018',
'img'=>'file-1463591644-ZGN2DA.jpg',
'file'=>NULL,
'link'=>'https://ccforum.biomedcentral.com/articles/10.1186/s13054-018-1957-5',
'by'=>0,
'journal'=>'Xavier Monnet and Jean-Louis Teboul',
'type'=>0,
'month_article'=>0,
'famous'=>0,
'views'=>1101,
'show'=>0,
'date'=>'2018-02-10 22:00:00',
'active'=>'1',
'user'=>0,
'created_at'=>'2020-05-15 01:20:06',
'updated_at'=>'2020-06-21 00:08:23',
'deleted_at'=>NULL
] );


            
Post::create( [
'id'=>4785,
'cat_id'=>12,
'tag_id'=>9,
'title'=>'The ten tips to manage critically ill patients with acute-on-chronic liver failure',
'text'=>'Intensive Care Med, February 2018',
'img'=>'file-1463591650-J14MMU.jpg',
'file'=>NULL,
'link'=>'https://link.springer.com/epdf/10.1007/s00134-018-5078-z?shared_access_token=if05Pv12zAA5g33YCZkmTfe4RwlQNchNByi7wbcMAY63FIMr4IeFUPHZTUFKckgGIQeEsXu7k1NhPo-T8ItytjuWlMH7y0kI6IydY2i7VzWux_orHwfDLlWpCB0SsnOr5FHqhmDfPxsUyfoQkWtfXZjqGRMzjAoDsJqcrvrDzM0%3D',
'by'=>0,
'journal'=>'Valentin Fuhrmann and others',
'type'=>0,
'month_article'=>0,
'famous'=>0,
'views'=>1101,
'show'=>0,
'date'=>'2018-02-11 22:00:00',
'active'=>'1',
'user'=>0,
'created_at'=>'2020-05-15 01:20:06',
'updated_at'=>'2020-06-21 00:08:23',
'deleted_at'=>NULL
] );


            
Post::create( [
'id'=>4786,
'cat_id'=>9,
'tag_id'=>6,
'title'=>'Clinical efficacy of high flow nasal cannula oxygenation',
'text'=>'ICU Management and Practice',
'img'=>'file-1464165509-QNPJY8.jpg',
'file'=>NULL,
'link'=>'https://healthmanagement.org/c/icu/news/clinical-efficacy-of-high-flow-nasal-cannula-oxygenation',
'by'=>0,
'journal'=>'Source: Annals of the American Thoracic Society Image Credit: James Heilman, MD',
'type'=>0,
'month_article'=>0,
'famous'=>0,
'views'=>1101,
'show'=>0,
'date'=>'2018-02-14 22:00:00',
'active'=>'1',
'user'=>0,
'created_at'=>'2020-05-15 01:20:06',
'updated_at'=>'2020-06-21 00:08:23',
'deleted_at'=>NULL
] );


            
Post::create( [
'id'=>4787,
'cat_id'=>8,
'tag_id'=>11,
'title'=>'Hypertonic saline infusion for treating intracranial hypertension after severe traumatic brain injury',
'text'=>'Critical Care 2018, 22:37  Published on: 20 February 2018',
'img'=>'im2.jpg',
'file'=>NULL,
'link'=>'https://ccforum.biomedcentral.com/articles/10.1186/s13054-018-1963-7',
'by'=>0,
'journal'=>'Halinder S. Mangat',
'type'=>0,
'month_article'=>0,
'famous'=>0,
'views'=>1101,
'show'=>0,
'date'=>'2018-02-20 22:00:00',
'active'=>'1',
'user'=>0,
'created_at'=>'2020-05-15 01:20:06',
'updated_at'=>'2020-06-21 00:08:23',
'deleted_at'=>NULL
] );


            
Post::create( [
'id'=>4788,
'cat_id'=>8,
'tag_id'=>1,
'title'=>'First human case of avian flu A H7N4 confirmed First human case of avian flu A H7N4 confirmed top',
'text'=>'February 14, 2018',
'img'=>'file-1463591752-XK911T.jpg',
'file'=>NULL,
'link'=>'http://www.info.gov.hk/gia/general/201802/14/P2018021400759.htm',
'by'=>0,
'journal'=>'Press Releases',
'type'=>0,
'month_article'=>0,
'famous'=>0,
'views'=>1130,
'show'=>0,
'date'=>'2018-02-21 22:00:00',
'active'=>'1',
'user'=>0,
'created_at'=>'2020-05-15 01:20:06',
'updated_at'=>'2020-06-21 00:08:23',
'deleted_at'=>NULL
] );


            
Post::create( [
'id'=>4789,
'cat_id'=>8,
'tag_id'=>11,
'title'=>'Balanced Crystalloids in Noncritically Ill Adults',
'text'=>'N Engl J Med. March 2018 378:819-828',
'img'=>'file-1464165463-D3CBM9.jpg',
'file'=>NULL,
'link'=>'http://www.nejm.org/doi/full/10.1056/NEJMoa1711586?query=featured_home',
'by'=>0,
'journal'=>'Wesley H. Self,  et al., for the SALT-ED Investigators',
'type'=>0,
'month_article'=>0,
'famous'=>1,
'views'=>1196,
'show'=>0,
'date'=>'2018-03-05 22:00:00',
'active'=>'1',
'user'=>0,
'created_at'=>'2020-05-15 01:20:06',
'updated_at'=>'2020-06-21 00:08:23',
'deleted_at'=>NULL
] );


            
Post::create( [
'id'=>4790,
'cat_id'=>12,
'tag_id'=>7,
'title'=>'Balanced Crystalloids versus Saline in Critically Ill Adults',
'text'=>'N Engl J Med. March 2018 378:829-839',
'img'=>'file-1464106363-HWQX6B.jpg',
'file'=>NULL,
'link'=>'http://www.nejm.org/doi/full/10.1056/NEJMoa1711584queryfeatured_home',
'by'=>0,
'journal'=>'Matthew W. Semler,  et al., for the SMART Investigators and the Pragmatic Critical Care Research Group',
'type'=>0,
'month_article'=>0,
'famous'=>1,
'views'=>1307,
'show'=>0,
'date'=>'2018-03-05 22:00:00',
'active'=>'1',
'user'=>0,
'created_at'=>'2020-05-15 01:20:06',
'updated_at'=>'2020-06-21 00:08:23',
'deleted_at'=>NULL
] );


            
Post::create( [
'id'=>4792,
'cat_id'=>8,
'tag_id'=>11,
'title'=>'Physicians: End-of-life antimicrobials add to resistance problems',
'text'=>'Clinical Leadership & Infection Control',
'img'=>'file-1463591639-4YG8JY.jpg',
'file'=>NULL,
'link'=>'https://www.beckershospitalreview.com/quality/end-of-life-antimicrobial-use-may-up-resistance-but-85-of-physicians-respect-patient-wishes.html',
'by'=>0,
'journal'=>'Written by Anuja Vaidya',
'type'=>0,
'month_article'=>0,
'famous'=>0,
'views'=>1101,
'show'=>0,
'date'=>'2018-03-05 22:00:00',
'active'=>'1',
'user'=>0,
'created_at'=>'2020-05-15 01:20:06',
'updated_at'=>'2020-06-21 00:08:23',
'deleted_at'=>NULL
] );


            
Post::create( [
'id'=>4793,
'cat_id'=>8,
'tag_id'=>6,
'title'=>'High-frequency percussive ventilation in acute respiratory distress syndrome: knocking at the door but can it be let in',
'text'=>'Critical Care 2018, 22:55  Published on: 2 March 2018',
'img'=>'im6.jpg',
'file'=>NULL,
'link'=>'https://ccforum.biomedcentral.com/articles/10.1186/s13054-018-1982-4',
'by'=>0,
'journal'=>'Herbert Spapen, Jouke De Regt, Viola van Gorp and Patrick M. Honoré',
'type'=>0,
'month_article'=>0,
'famous'=>0,
'views'=>1101,
'show'=>0,
'date'=>'2018-03-05 22:00:00',
'active'=>'1',
'user'=>0,
'created_at'=>'2020-05-15 01:20:06',
'updated_at'=>'2020-06-21 00:08:23',
'deleted_at'=>NULL
] );


            
Post::create( [
'id'=>4794,
'cat_id'=>8,
'tag_id'=>11,
'title'=>'Adjunctive Glucocorticoid Therapy in Patients with Septic Shock',
'text'=>'N Engl J Med 2018378:797-808',
'img'=>'file-1464165480-R231XR.jpg',
'file'=>NULL,
'link'=>'http://www.nejm.org/doi/full/10.1056/NEJMoa1705835?query=TOC',
'by'=>0,
'journal'=>'B. Venkatesh and Others. for the ADRENAL Trial Investigators and the Australian–New Zealand Intensive Care Society Clinical Trials Group',
'type'=>0,
'month_article'=>0,
'famous'=>1,
'views'=>1196,
'show'=>0,
'date'=>'2018-03-05 22:00:00',
'active'=>'1',
'user'=>0,
'created_at'=>'2020-05-15 01:20:06',
'updated_at'=>'2020-06-21 00:08:23',
'deleted_at'=>NULL
] );


            
Post::create( [
'id'=>4795,
'cat_id'=>12,
'tag_id'=>11,
'title'=>'Hydrocortisone plus Fludrocortisone for Adults with Septic Shock',
'text'=>'N Engl J Med 2018378:809-818',
'img'=>'file-1464165463-D3CBM9.jpg',
'file'=>NULL,
'link'=>'http://www.nejm.org/doi/full/10.1056/NEJMoa1705716?query=TOC',
'by'=>0,
'journal'=>'D. Annane and Others, for the CRICS-TRIGGERSEP Network',
'type'=>0,
'month_article'=>0,
'famous'=>1,
'views'=>1307,
'show'=>0,
'date'=>'2018-03-05 22:00:00',
'active'=>'1',
'user'=>0,
'created_at'=>'2020-05-15 01:20:06',
'updated_at'=>'2020-06-21 00:08:23',
'deleted_at'=>NULL
] );


            
Post::create( [
'id'=>4796,
'cat_id'=>8,
'tag_id'=>7,
'title'=>'Vasoplegia treatments: the past, the present, and the future',
'text'=>'Critical Care 2018, 22:52',
'img'=>'file-1463591644-ZGN2DA.jpg',
'file'=>NULL,
'link'=>'https://ccforum.biomedcentral.com/articles/10.1186/s13054-018-1967-3',
'by'=>0,
'journal'=>'Bruno Levy, Caroline Fritz, Elsa Tahon, Audrey Jacquot, Thomas Auchet and Antoine Kimmou',
'type'=>0,
'month_article'=>0,
'famous'=>0,
'views'=>1101,
'show'=>0,
'date'=>'2018-02-28 22:00:00',
'active'=>'1',
'user'=>0,
'created_at'=>'2020-05-15 01:20:06',
'updated_at'=>'2020-06-21 00:08:23',
'deleted_at'=>NULL
] );


            
Post::create( [
'id'=>4797,
'cat_id'=>8,
'tag_id'=>11,
'title'=>'Tight glycemic control in critically ill pediatric patients: a systematic review and meta-analysis',
'text'=>'Critical Care 2018, 22:57  Published on: 4 March 2018',
'img'=>'file-1464165509-QNPJY8.jpg',
'file'=>NULL,
'link'=>'https://ccforum.biomedcentral.com/articles/10.1186/s13054-018-1976-2',
'by'=>0,
'journal'=>'Lvlin Chen, Tiangui Li, Fang Fang, Yu Zhang and Andrew Faramand',
'type'=>0,
'month_article'=>0,
'famous'=>0,
'views'=>1101,
'show'=>0,
'date'=>'2018-03-05 22:00:00',
'active'=>'1',
'user'=>0,
'created_at'=>'2020-05-15 01:20:06',
'updated_at'=>'2020-06-21 00:08:23',
'deleted_at'=>NULL
] );


            
Post::create( [
'id'=>4798,
'cat_id'=>12,
'tag_id'=>2,
'title'=>'Low-dose Nocturnal Dexmedetomidine Prevents ICU Delirium: A Randomized, Placebo-controlled Trial',
'text'=>'AJRCM, Published Online: March 02, 2018',
'img'=>'im2.jpg',
'file'=>NULL,
'link'=>'https://www.atsjournals.org/doi/abs/10.1164/rccm.201710-1995OC?journalCode=ajrccm',
'by'=>0,
'journal'=>'Yoanna Skrobik and others',
'type'=>0,
'month_article'=>0,
'famous'=>0,
'views'=>1283,
'show'=>0,
'date'=>'2018-03-05 22:00:00',
'active'=>'1',
'user'=>0,
'created_at'=>'2020-05-15 01:20:06',
'updated_at'=>'2020-06-21 00:08:23',
'deleted_at'=>NULL
] );


            
Post::create( [
'id'=>4799,
'cat_id'=>8,
'tag_id'=>11,
'title'=>'How to Run Successful Rounds in the Intensive Care Unit',
'text'=>'',
'img'=>'file-1464165480-R231XR.jpg',
'file'=>NULL,
'link'=>'https://healthmanagement.org/c/icu/issuearticle/how-to-run-successful-rounds-in-the-intensive-care-unit',
'by'=>0,
'journal'=>'ICU Management & Practice, Volume 17 - Issue 2, 2017',
'type'=>0,
'month_article'=>0,
'famous'=>0,
'views'=>1101,
'show'=>0,
'date'=>'2018-03-06 22:00:00',
'active'=>'1',
'user'=>0,
'created_at'=>'2020-05-15 01:20:06',
'updated_at'=>'2020-06-21 00:08:23',
'deleted_at'=>NULL
] );


            
Post::create( [
'id'=>4800,
'cat_id'=>8,
'tag_id'=>6,
'title'=>'Effect of On-Demand vs Routine Nebulization of Acetylcysteine With Salbutamol on Ventilator-Free Days in Intensive Care Unit Patients Receiving Invasive Ventilation A Randomized Clinical Trial',
'text'=>'JAMA. Published online February 27, 2018',
'img'=>'im5.jpg',
'file'=>NULL,
'link'=>'https://jamanetwork.com/journals/jama/fullarticle/2673505',
'by'=>0,
'journal'=>'David M. P. van Meenen and others',
'type'=>0,
'month_article'=>0,
'famous'=>1,
'views'=>1196,
'show'=>0,
'date'=>'2018-03-06 22:00:00',
'active'=>'1',
'user'=>0,
'created_at'=>'2020-05-15 01:20:06',
'updated_at'=>'2020-06-21 00:08:23',
'deleted_at'=>NULL
] );


            
Post::create( [
'id'=>4801,
'cat_id'=>8,
'tag_id'=>3,
'title'=>'A novel method of blind bedside placement of postpyloric tubes',
'text'=>'Critical Care 2018, 22:62  Published on: 9 March 2018',
'img'=>'file-1463591732-SM39NY.jpg',
'file'=>NULL,
'link'=>'https://ccforum.biomedcentral.com/articles/10.1186/s13054-018-1986-0',
'by'=>0,
'journal'=>'Jia-Kui Sun, Xiang Wang and Shou-Tao Yuan',
'type'=>0,
'month_article'=>0,
'famous'=>1,
'views'=>1196,
'show'=>0,
'date'=>'2018-03-09 22:00:00',
'active'=>'1',
'user'=>0,
'created_at'=>'2020-05-15 01:20:06',
'updated_at'=>'2020-06-21 00:08:23',
'deleted_at'=>NULL
] );


            
Post::create( [
'id'=>4802,
'cat_id'=>8,
'tag_id'=>1,
'title'=>'The long sepsis journey in low- and middle-income countries begins with a first step...but on which road',
'text'=>'Critical Care 2018, 22:64  Published on: 9 March 2018',
'img'=>'file-1464106363-HWQX6B.jpg',
'file'=>NULL,
'link'=>'https://ccforum.biomedcentral.com/articles/10.1186/s13054-018-1987-z',
'by'=>0,
'journal'=>'Arthur Kwizera and others',
'type'=>0,
'month_article'=>0,
'famous'=>0,
'views'=>1130,
'show'=>0,
'date'=>'2018-03-08 22:00:00',
'active'=>'1',
'user'=>0,
'created_at'=>'2020-05-15 01:20:06',
'updated_at'=>'2020-06-21 00:08:23',
'deleted_at'=>NULL
] );


            
Post::create( [
'id'=>4803,
'cat_id'=>8,
'tag_id'=>6,
'title'=>'The preventive effect of antiplatelet therapy in acute respiratory distress syndrome: a meta-analysis',
'text'=>'Critical Care 2018, 22:60  Published on: 8 March 2018',
'img'=>'file-1463591752-XK911T.jpg',
'file'=>NULL,
'link'=>'https://ccforum.biomedcentral.com/articles/10.1186/s13054-018-1988-y',
'by'=>0,
'journal'=>'Yingqin Wang, Ming Zhong, Zhichao Wang, Jieqiong Song, Wei Wu and Duming Zhu',
'type'=>0,
'month_article'=>0,
'famous'=>0,
'views'=>1101,
'show'=>0,
'date'=>'2018-03-10 22:00:00',
'active'=>'1',
'user'=>0,
'created_at'=>'2020-05-15 01:20:06',
'updated_at'=>'2020-06-21 00:08:23',
'deleted_at'=>NULL
] );


            
Post::create( [
'id'=>4804,
'cat_id'=>8,
'tag_id'=>1,
'title'=>'Dr. Paul E. Marik proclaims end to corticosteroid monotherapy for sepsis',
'text'=>'Frontline Medical News. Publish date: March 8, 2018 By',
'img'=>'file-1463591639-4YG8JY.jpg',
'file'=>NULL,
'link'=>'https://www.mdedge.com/chestphysician/article/160410/critical-care/dr-paul-e-marik-proclaims-end-corticosteroid-monotherapy?channel=201&utm_campaign=PANTHEON_STRIPPED&utm_source=PANTHEON_STRIPPED&utm_medium=PANTHEON_STRIPPED&utm_content=PANTHEON_STRIPPED&_hsenc=p2ANqtz-8PRPCEVshAh5LmMXm3YTYsjidNsZDDEQ_qRCdV1IaTQwlP9WYRaspBqKt6HOB_aSFgSPfxAD7i5Vx_kMD8sPhF-lpW-Q&_hsmi=61359533',
'by'=>0,
'journal'=>'Andrew D.',
'type'=>0,
'month_article'=>0,
'famous'=>0,
'views'=>1130,
'show'=>0,
'date'=>'2018-03-14 22:00:00',
'active'=>'1',
'user'=>0,
'created_at'=>'2020-05-15 01:20:06',
'updated_at'=>'2020-06-21 00:08:23',
'deleted_at'=>NULL
] );


            
Post::create( [
'id'=>4805,
'cat_id'=>8,
'tag_id'=>11,
'title'=>'PPIs could increase the risk of major depressive disorder top',
'text'=>'Psychotherapy and Psychosomatics',
'img'=>'file-1464106363-HWQX6B.jpg',
'file'=>NULL,
'link'=>'https://www.karger.com/Article/Abstract/485190',
'by'=>0,
'journal'=>'Huang W.-S and others',
'type'=>0,
'month_article'=>0,
'famous'=>0,
'views'=>1101,
'show'=>0,
'date'=>'2018-03-14 22:00:00',
'active'=>'1',
'user'=>0,
'created_at'=>'2020-05-15 01:20:06',
'updated_at'=>'2020-06-21 00:08:23',
'deleted_at'=>NULL
] );


            
Post::create( [
'id'=>4806,
'cat_id'=>8,
'tag_id'=>7,
'title'=>'IV bags connected to IV lines  Should we measure the central venous pressure to guide fluid management Ten answers to 10 questions',
'text'=>'Critical Care201822:43',
'img'=>'file-1463591644-ZGN2DA.jpg',
'file'=>NULL,
'link'=>'https://ccforum.biomedcentral.com/articles/10.1186/s13054-018-1959-3?sap-outbound-id=869C5E42F62BDA46951BE7D0CDEA8C5821DE6A70',
'by'=>0,
'journal'=>'Daniel De Backer and Jean-Louis Vincent',
'type'=>0,
'month_article'=>0,
'famous'=>0,
'views'=>1101,
'show'=>0,
'date'=>'2018-03-20 22:00:00',
'active'=>'1',
'user'=>0,
'created_at'=>'2020-05-15 01:20:06',
'updated_at'=>'2020-06-21 00:08:23',
'deleted_at'=>NULL
] );


            
Post::create( [
'id'=>4807,
'cat_id'=>8,
'tag_id'=>7,
'title'=>'Flask of Methylene Blue  Vasoplegia treatments: the past, the present, and the future',
'text'=>'Critical Care201822:52',
'img'=>'file-1463591639-4YG8JY.jpg',
'file'=>NULL,
'link'=>'https://ccforum.biomedcentral.com/articles/10.1186/s13054-018-1967-3?sap-outbound-id=869C5E42F62BDA46951BE7D0CDEA8C5821DE6A70',
'by'=>0,
'journal'=>'Bruno Levy, Caroline Fritz, Elsa Tahon, Audrey Jacquot, Thomas Auchet and Antoine Kimmoun',
'type'=>0,
'month_article'=>0,
'famous'=>0,
'views'=>1101,
'show'=>0,
'date'=>'2018-03-20 22:00:00',
'active'=>'1',
'user'=>0,
'created_at'=>'2020-05-15 01:20:06',
'updated_at'=>'2020-06-21 00:08:23',
'deleted_at'=>NULL
] );


            
Post::create( [
'id'=>4808,
'cat_id'=>8,
'tag_id'=>3,
'title'=>'Total parenteral nutrition formula  Supplemental parenteral nutrition versus usual care in critically ill adults',
'text'=>'Critical Care201822:12',
'img'=>'file-1463591732-SM39NY.jpg',
'file'=>NULL,
'link'=>'https://ccforum.biomedcentral.com/articles/10.1186/s13054-018-1939-7?sap-outbound-id=869C5E42F62BDA46951BE7D0CDEA8C5821DE6A70',
'by'=>0,
'journal'=>'Emma J. Ridle and others',
'type'=>0,
'month_article'=>0,
'famous'=>0,
'views'=>1101,
'show'=>0,
'date'=>'2018-03-20 22:00:00',
'active'=>'1',
'user'=>0,
'created_at'=>'2020-05-15 01:20:06',
'updated_at'=>'2020-06-21 00:08:23',
'deleted_at'=>NULL
] );


            
Post::create( [
'id'=>4809,
'cat_id'=>8,
'tag_id'=>6,
'title'=>'A Systematic Review of the High-flow Nasal Cannula for Adult Patients',
'text'=>'Critical Care 2018 22:71. Published on: 20 March 2018',
'img'=>'im5.jpg',
'file'=>NULL,
'link'=>'https://ccforum.biomedcentral.com/articles/10.1186/s13054-018-1990-4',
'by'=>0,
'journal'=>'Yigal Helviz and Sharon Einav',
'type'=>0,
'month_article'=>0,
'famous'=>0,
'views'=>1101,
'show'=>0,
'date'=>'2018-03-20 22:00:00',
'active'=>'1',
'user'=>0,
'created_at'=>'2020-05-15 01:20:06',
'updated_at'=>'2020-06-21 00:08:23',
'deleted_at'=>NULL
] );


            
Post::create( [
'id'=>4810,
'cat_id'=>12,
'tag_id'=>3,
'title'=>'Making sense of early high-dose intravenous vitamin C in ischemia/reperfusion injury',
'text'=>'Critical Care 2018 22:70. Published on: 20 March 2018',
'img'=>'file-1463591725-9J88SF.jpg',
'file'=>NULL,
'link'=>'https://ccforum.biomedcentral.com/articles/10.1186/s13054-018-1996-y',
'by'=>0,
'journal'=>'Angelique M. E. Spoelstra-de Man, Paul W. G. Elbers and Heleen M. Oudemans-van Straaten',
'type'=>0,
'month_article'=>0,
'famous'=>0,
'views'=>1212,
'show'=>0,
'date'=>'2018-03-20 22:00:00',
'active'=>'1',
'user'=>0,
'created_at'=>'2020-05-15 01:20:06',
'updated_at'=>'2020-06-21 00:08:23',
'deleted_at'=>NULL
] );


            
Post::create( [
'id'=>4811,
'cat_id'=>8,
'tag_id'=>5,
'title'=>'Continuous Electroencephalography Monitoring in Adults in the Intensive Care Unit',
'text'=>'Critical Care 2018 22:75. Published on: 20 March 2018',
'img'=>'im2.jpg',
'file'=>NULL,
'link'=>'https://ccforum.biomedcentral.com/articles/10.1186/s13054-018-1997-x',
'by'=>0,
'journal'=>'Authors: Anselmo Caricato, Isabella Melchionda and Massimo Antonelli',
'type'=>0,
'month_article'=>0,
'famous'=>0,
'views'=>1114,
'show'=>0,
'date'=>'2018-03-20 22:00:00',
'active'=>'1',
'user'=>0,
'created_at'=>'2020-05-15 01:20:06',
'updated_at'=>'2020-06-21 00:08:23',
'deleted_at'=>NULL
] );


            
Post::create( [
'id'=>4812,
'cat_id'=>8,
'tag_id'=>8,
'title'=>'Renal autoregulation and blood pressure management in circulatory shock',
'text'=>'Critical Care 2018, 22:81  Published on: 22 March 2018',
'img'=>'file-1463591738-IZ3XFW.jpg',
'file'=>NULL,
'link'=>'https://ccforum.biomedcentral.com/articles/10.1186/s13054-018-1962-8',
'by'=>0,
'journal'=>'Emiel Hendrik Post and Jean-Louis Vincent',
'type'=>0,
'month_article'=>0,
'famous'=>0,
'views'=>1152,
'show'=>0,
'date'=>'2018-03-22 22:00:00',
'active'=>'1',
'user'=>0,
'created_at'=>'2020-05-15 01:20:06',
'updated_at'=>'2020-06-21 00:08:23',
'deleted_at'=>NULL
] );


            
Post::create( [
'id'=>4813,
'cat_id'=>8,
'tag_id'=>11,
'title'=>'Do we need new trials of procalcitonin-guided antibiotic therapy A response',
'text'=>'Critical Care 2018, 22:83  Published on: 23 March 2018',
'img'=>'file-1464106363-HWQX6B.jpg',
'file'=>NULL,
'link'=>'https://ccforum.biomedcentral.com/articles/10.1186/s13054-018-2008-y',
'by'=>0,
'journal'=>'Jos A. H. van Oers, Maarten W. Nijsten and Dylan W. de Lange',
'type'=>0,
'month_article'=>0,
'famous'=>0,
'views'=>1101,
'show'=>0,
'date'=>'2018-03-24 22:00:00',
'active'=>'1',
'user'=>0,
'created_at'=>'2020-05-15 01:20:06',
'updated_at'=>'2020-06-21 00:08:23',
'deleted_at'=>NULL
] );


            
Post::create( [
'id'=>4814,
'cat_id'=>8,
'tag_id'=>6,
'title'=>'Comparing fiberoptic bronchoscopy and a tracheal tube-mounted camera-guided percutaneous dilatational tracheostomy: authors reply',
'text'=>'Critical Care 2018, 22:84  Published on: 23 March 2018',
'img'=>'im4.jpg',
'file'=>NULL,
'link'=>'https://ccforum.biomedcentral.com/articles/10.1186/s13054-018-2004-2',
'by'=>0,
'journal'=>'Jörn Grensemann and others',
'type'=>0,
'month_article'=>0,
'famous'=>0,
'views'=>1101,
'show'=>0,
'date'=>'2018-03-24 22:00:00',
'active'=>'1',
'user'=>0,
'created_at'=>'2020-05-15 01:20:06',
'updated_at'=>'2020-06-21 00:08:23',
'deleted_at'=>NULL
] );


            
Post::create( [
'id'=>4815,
'cat_id'=>8,
'tag_id'=>7,
'title'=>'Angiotensin in Critical Care',
'text'=>'Critical Care 2018, 22:69  Published on: 20 March 2018',
'img'=>'file-1463591644-ZGN2DA.jpg',
'file'=>NULL,
'link'=>'https://ccforum.biomedcentral.com/articles/10.1186/s13054-018-1995-z',
'by'=>0,
'journal'=>'Anna Hall, Laurence W. Busse and Marlies Ostermann',
'type'=>0,
'month_article'=>0,
'famous'=>1,
'views'=>1196,
'show'=>0,
'date'=>'2018-03-24 22:00:00',
'active'=>'1',
'user'=>0,
'created_at'=>'2020-05-15 01:20:06',
'updated_at'=>'2020-06-21 00:08:23',
'deleted_at'=>NULL
] );


            
Post::create( [
'id'=>4816,
'cat_id'=>8,
'tag_id'=>1,
'title'=>'The emerging role of the microbiota in the ICU',
'text'=>'Critical Care 2018, 22:78  Published on: 20 March 2018',
'img'=>'file-1464165463-D3CBM9.jpg',
'file'=>NULL,
'link'=>'https://ccforum.biomedcentral.com/articles/10.1186/s13054-018-1999-8',
'by'=>0,
'journal'=>'Nora Suzanne Wolff, Floor Hugenholtz and Willem Joost Wiersinga',
'type'=>0,
'month_article'=>0,
'famous'=>0,
'views'=>1130,
'show'=>0,
'date'=>'2018-03-19 22:00:00',
'active'=>'1',
'user'=>0,
'created_at'=>'2020-05-15 01:20:06',
'updated_at'=>'2020-06-21 00:08:23',
'deleted_at'=>NULL
] );


            
Post::create( [
'id'=>4817,
'cat_id'=>8,
'tag_id'=>6,
'title'=>'Close down the lungs and keep them resting to minimize ventilator-induced lung injury',
'text'=>'Critical Care 2018, 22:72  Published on: 20 March 2018',
'img'=>'im6.jpg',
'file'=>NULL,
'link'=>'https://ccforum.biomedcentral.com/articles/10.1186/s13054-018-1991-3',
'by'=>0,
'journal'=>'Paolo Pelosi, Patricia Rieken Macedo Rocco and Marcelo Gama de Abreu',
'type'=>0,
'month_article'=>0,
'famous'=>0,
'views'=>1101,
'show'=>0,
'date'=>'2018-03-24 22:00:00',
'active'=>'1',
'user'=>0,
'created_at'=>'2020-05-15 01:20:06',
'updated_at'=>'2020-06-21 00:08:23',
'deleted_at'=>NULL
] );


            
Post::create( [
'id'=>4818,
'cat_id'=>8,
'tag_id'=>11,
'title'=>'Respiratory Management in Patients with Severe Brain Injury',
'text'=>'Critical Care 2018, 22:76  Published on: 20 March 2018',
'img'=>'im1.jpg',
'file'=>NULL,
'link'=>'https://ccforum.biomedcentral.com/articles/10.1186/s13054-018-1994-0',
'by'=>0,
'journal'=>'Karim Asehnoune, Antoine Roquilly and Raphaël Cinotti',
'type'=>0,
'month_article'=>0,
'famous'=>0,
'views'=>1101,
'show'=>0,
'date'=>'2018-03-24 22:00:00',
'active'=>'1',
'user'=>0,
'created_at'=>'2020-05-15 01:20:06',
'updated_at'=>'2020-06-21 00:08:23',
'deleted_at'=>NULL
] );


            
Post::create( [
'id'=>4819,
'cat_id'=>8,
'tag_id'=>6,
'title'=>'Diaphragm dysfunction during weaning from mechanical ventilation: an underestimated phenomenon with clinical implications',
'text'=>'Critical Care 2018, 22:73  Published on: 20 March 2018',
'img'=>'file-1463591752-XK911T.jpg',
'file'=>NULL,
'link'=>'https://ccforum.biomedcentral.com/articles/10.1186/s13054-018-1992-2',
'by'=>0,
'journal'=>'Martin Dres and Alexandre Demoule',
'type'=>0,
'month_article'=>0,
'famous'=>0,
'views'=>1101,
'show'=>0,
'date'=>'2018-03-19 22:00:00',
'active'=>'1',
'user'=>0,
'created_at'=>'2020-05-15 01:20:06',
'updated_at'=>'2020-06-21 00:08:23',
'deleted_at'=>NULL
] );


            
Post::create( [
'id'=>4820,
'cat_id'=>8,
'tag_id'=>11,
'title'=>'Early Mobilization of Patients in Intensive Care: Organization, Communication and Safety Factors that Influence Translation into Clinical Practice',
'text'=>'Critical Care 2018, 22:77  Published on: 20 March 2018',
'img'=>'file-1464165480-R231XR.jpg',
'file'=>NULL,
'link'=>'https://ccforum.biomedcentral.com/articles/10.1186/s13054-018-1998-9',
'by'=>0,
'journal'=>'Carol L. Hodgson, Elizabeth Capell and Claire J. Tipping',
'type'=>0,
'month_article'=>0,
'famous'=>0,
'views'=>1101,
'show'=>0,
'date'=>'2018-03-20 22:00:00',
'active'=>'1',
'user'=>0,
'created_at'=>'2020-05-15 01:20:06',
'updated_at'=>'2020-06-21 00:08:23',
'deleted_at'=>NULL
] );


            
Post::create( [
'id'=>4821,
'cat_id'=>8,
'tag_id'=>5,
'title'=>'Continuous Electroencephalography Monitoring in Adults in the Intensive Care Unit',
'text'=>'Critical Care 2018, 22:75  Published on: 20 March 2018',
'img'=>'im2.jpg',
'file'=>NULL,
'link'=>'https://ccforum.biomedcentral.com/articles/10.1186/s13054-018-1997-x',
'by'=>0,
'journal'=>'Anselmo Caricato, Isabella Melchionda and Massimo Antonelli',
'type'=>0,
'month_article'=>0,
'famous'=>0,
'views'=>1114,
'show'=>0,
'date'=>'2018-03-21 22:00:00',
'active'=>'1',
'user'=>0,
'created_at'=>'2020-05-15 01:20:06',
'updated_at'=>'2020-06-21 00:08:23',
'deleted_at'=>NULL
] );


            
Post::create( [
'id'=>4822,
'cat_id'=>8,
'tag_id'=>6,
'title'=>'A Systematic Review of the High-flow Nasal Cannula for Adult Patients',
'text'=>'Critical Care 2018, 22:71  Published on: 20 March 2018',
'img'=>'file-1470233162-2MW1RX.jpg',
'file'=>NULL,
'link'=>'https://ccforum.biomedcentral.com/articles/10.1186/s13054-018-1990-4',
'by'=>0,
'journal'=>'Yigal Helviz and Sharon Einav',
'type'=>0,
'month_article'=>0,
'famous'=>0,
'views'=>1101,
'show'=>0,
'date'=>'2018-03-21 22:00:00',
'active'=>'1',
'user'=>0,
'created_at'=>'2020-05-15 01:20:06',
'updated_at'=>'2020-06-21 00:08:23',
'deleted_at'=>NULL
] );


            
Post::create( [
'id'=>4823,
'cat_id'=>12,
'tag_id'=>5,
'title'=>'2018 ESC Guidelines for the diagnosis and management of syncope',
'text'=>'European Heart Journal, ehy037, https://doi.org/10.1093/eurheartj/ehy037\r\nPublished:\r\nEuropean Heart Journal, 19 March 2018',
'img'=>'im1.jpg',
'file'=>NULL,
'link'=>'https://academic.oup.com/eurheartj/advance-article/doi/10.1093/eurheartj/ehy037/4939241',
'by'=>0,
'journal'=>'Michele Brignole and others',
'type'=>0,
'month_article'=>0,
'famous'=>0,
'views'=>1225,
'show'=>0,
'date'=>'2018-03-28 22:00:00',
'active'=>'1',
'user'=>0,
'created_at'=>'2020-05-15 01:20:06',
'updated_at'=>'2020-06-21 00:08:23',
'deleted_at'=>NULL
] );


            
Post::create( [
'id'=>4824,
'cat_id'=>9,
'tag_id'=>5,
'title'=>'2018 Guidelines for the Early Management of Patients With Acute Ischemic Stroke: A Guideline for Healthcare Professionals From the American Heart Association/American Stroke Association',
'text'=>'Stroke. 2018, published January 24, 2018',
'img'=>'im2.jpg',
'file'=>NULL,
'link'=>'http://stroke.ahajournals.org/content/early/2018/01/23/STR.0000000000000158',
'by'=>0,
'journal'=>'William J. Powers, et al, on behalf of the American Heart Association Stroke Council',
'type'=>0,
'month_article'=>0,
'famous'=>0,
'views'=>1114,
'show'=>0,
'date'=>'2018-01-17 22:00:00',
'active'=>'1',
'user'=>0,
'created_at'=>'2020-05-15 01:20:06',
'updated_at'=>'2020-06-21 00:08:23',
'deleted_at'=>NULL
] );


            
Post::create( [
'id'=>4825,
'cat_id'=>12,
'tag_id'=>5,
'title'=>'2018 AHA/ASA Stroke Early Management Guidelines - Key Points',
'text'=>'American College of Cardiology',
'img'=>'im2.jpg',
'file'=>NULL,
'link'=>'http://www.acc.org/latest-in-cardiology/ten-points-to-remember/2018/01/29/12/45/2018-guidelines-for-the-early-management-of-stroke',
'by'=>0,
'journal'=>'Margaret Leslie McDermott, MD, MS',
'type'=>0,
'month_article'=>0,
'famous'=>0,
'views'=>1114,
'show'=>0,
'date'=>'2018-02-28 22:00:00',
'active'=>'1',
'user'=>0,
'created_at'=>'2020-05-15 01:20:06',
'updated_at'=>'2020-06-21 00:08:23',
'deleted_at'=>NULL
] );


            
Post::create( [
'id'=>4826,
'cat_id'=>8,
'tag_id'=>11,
'title'=>'38th International Symposium on Intensive Care and Emergency Medicine',
'text'=>'Critical Care 2018. 22 Suppl 1:82',
'img'=>'file-1464165480-R231XR.jpg',
'file'=>NULL,
'link'=>'https://ccforum.biomedcentral.com/articles/10.1186/s13054-018-1973-5',
'by'=>0,
'journal'=>'Brussels, Belgium. 20-23 March 2018',
'type'=>0,
'month_article'=>0,
'famous'=>0,
'views'=>1101,
'show'=>0,
'date'=>'2018-03-29 22:00:00',
'active'=>'1',
'user'=>0,
'created_at'=>'2020-05-15 01:20:06',
'updated_at'=>'2020-06-21 00:08:23',
'deleted_at'=>NULL
] );


            
Post::create( [
'id'=>4827,
'cat_id'=>8,
'tag_id'=>4,
'title'=>'Reverse shock index multiplied by Glasgow Coma Scale score rSIG is a simple measure with high discriminant ability for mortality risk in trauma patients: an analysis of the Japan Trauma Data Bank',
'text'=>'Critical Care 2018, 22:87  Published on: 11 April 2018',
'img'=>'im3.jpg',
'file'=>NULL,
'link'=>'https://ccforum.biomedcentral.com/articles/10.1186/s13054-018-2014-0',
'by'=>0,
'journal'=>'Akio Kimura and Noriko Tanaka',
'type'=>0,
'month_article'=>0,
'famous'=>0,
'views'=>1198,
'show'=>0,
'date'=>'2018-04-11 22:00:00',
'active'=>'1',
'user'=>0,
'created_at'=>'2020-05-15 01:20:06',
'updated_at'=>'2020-06-21 00:08:23',
'deleted_at'=>NULL
] );


            
Post::create( [
'id'=>4828,
'cat_id'=>8,
'tag_id'=>6,
'title'=>'Cardiac Arrest and Mortality Related to Intubation Procedure in Critically Ill Adult Patients: A Multicenter Cohort Study',
'text'=>'Critical Care Medicine. 464:532-539, April 2018',
'img'=>'file-1463591639-4YG8JY.jpg',
'file'=>NULL,
'link'=>'https://journals.lww.com/ccmjournal/Abstract/2018/04000/Cardiac_Arrest_and_Mortality_Related_to_Intubation.8.aspx',
'by'=>0,
'journal'=>'De Jong, Audrey and others',
'type'=>0,
'month_article'=>0,
'famous'=>0,
'views'=>1101,
'show'=>0,
'date'=>'2018-03-31 22:00:00',
'active'=>'1',
'user'=>0,
'created_at'=>'2020-05-15 01:20:06',
'updated_at'=>'2020-06-21 00:08:23',
'deleted_at'=>NULL
] );


            
Post::create( [
'id'=>4829,
'cat_id'=>8,
'tag_id'=>5,
'title'=>'Erythropoietin Does Not Alter Serum Profiles of Neuronal and Axonal Biomarkers After Traumatic Brain Injury: Findings From the Australian EPO-TBI Clinical Trial',
'text'=>'Critical Care Medicine. 464:554-561, April 2018',
'img'=>'im1.jpg',
'file'=>NULL,
'link'=>'https://journals.lww.com/ccmjournal/Abstract/2018/04000/Erythropoietin_Does_Not_Alter_Serum_Profiles_of.11.aspx',
'by'=>0,
'journal'=>'Hellewell, Sarah C and others',
'type'=>0,
'month_article'=>0,
'famous'=>1,
'views'=>1209,
'show'=>0,
'date'=>'2018-04-02 22:00:00',
'active'=>'1',
'user'=>0,
'created_at'=>'2020-05-15 01:20:06',
'updated_at'=>'2020-06-21 00:08:23',
'deleted_at'=>NULL
] );


            
Post::create( [
'id'=>4830,
'cat_id'=>8,
'tag_id'=>11,
'title'=>'Septic Cardiomyopathy',
'text'=>'Critical Care Medicine. 464:625-634, April 2018',
'img'=>'file-1463591639-4YG8JY.jpg',
'file'=>NULL,
'link'=>'https://journals.lww.com/ccmjournal/Abstract/2018/04000/Septic_Cardiomyopathy.20.aspx',
'by'=>0,
'journal'=>'Beesley, Sarah and others',
'type'=>0,
'month_article'=>0,
'famous'=>0,
'views'=>1101,
'show'=>0,
'date'=>'2018-04-01 22:00:00',
'active'=>'1',
'user'=>0,
'created_at'=>'2020-05-15 01:20:06',
'updated_at'=>'2020-06-21 00:08:23',
'deleted_at'=>NULL
] );


            
Post::create( [
'id'=>4831,
'cat_id'=>8,
'tag_id'=>7,
'title'=>'Cardiac output and CVP monitoring… to guide fluid removal',
'text'=>'Critical Care 2018, 22:89  Published on: 12 April 2018',
'img'=>'file-1463591644-ZGN2DA.jpg',
'file'=>NULL,
'link'=>'https://ccforum.biomedcentral.com/articles/10.1186/s13054-018-2016-y',
'by'=>0,
'journal'=>'Matthieu Legrand, Sabri Soussi and François Depret',
'type'=>0,
'month_article'=>0,
'famous'=>1,
'views'=>1196,
'show'=>0,
'date'=>'2018-04-12 22:00:00',
'active'=>'1',
'user'=>0,
'created_at'=>'2020-05-15 01:20:06',
'updated_at'=>'2020-06-21 00:08:23',
'deleted_at'=>NULL
] );


            
Post::create( [
'id'=>4832,
'cat_id'=>8,
'tag_id'=>5,
'title'=>'Variation in general supportive and preventive intensive care management of traumatic brain injury: a survey in 66 neurotrauma centers participating in the Collaborative European NeuroTrauma Effectiveness Research in Traumatic Brain Injury CENTER-TBI stud',
'text'=>'Critical Care 2018, 22:90  Published on: 13 April 2018',
'img'=>'im1.jpg',
'file'=>NULL,
'link'=>'https://ccforum.biomedcentral.com/articles/10.1186/s13054-018-2000-6',
'by'=>0,
'journal'=>'Jilske A. Huijben and others',
'type'=>0,
'month_article'=>0,
'famous'=>0,
'views'=>1114,
'show'=>0,
'date'=>'2018-04-13 22:00:00',
'active'=>'1',
'user'=>0,
'created_at'=>'2020-05-15 01:20:06',
'updated_at'=>'2020-06-21 00:08:23',
'deleted_at'=>NULL
] );


            
Post::create( [
'id'=>4833,
'cat_id'=>8,
'tag_id'=>3,
'title'=>'Route, early or energy … Protein improves protein balance in critically ill patients',
'text'=>'Critical Care 2018, 22:91  Published on: 14 April 2018',
'img'=>'file-1463591725-9J88SF.jpg',
'file'=>NULL,
'link'=>'https://ccforum.biomedcentral.com/articles/10.1186/s13054-018-2015-z',
'by'=>0,
'journal'=>'Peter J. M. Weijs',
'type'=>0,
'month_article'=>0,
'famous'=>0,
'views'=>1101,
'show'=>0,
'date'=>'2018-04-14 22:00:00',
'active'=>'1',
'user'=>0,
'created_at'=>'2020-05-15 01:20:06',
'updated_at'=>'2020-06-21 00:08:23',
'deleted_at'=>NULL
] );


            
Post::create( [
'id'=>4834,
'cat_id'=>8,
'tag_id'=>11,
'title'=>'Short-term amino acid infusion improves protein balance in critically ill patients',
'text'=>'Crit Care. 2015 Mar 1219:106',
'img'=>'file-1463591732-SM39NY.jpg',
'file'=>NULL,
'link'=>'https://ccforum.biomedcentral.com/articles/10.1186/s13054-015-0844-6',
'by'=>0,
'journal'=>'Liebau F and others',
'type'=>0,
'month_article'=>0,
'famous'=>1,
'views'=>1196,
'show'=>0,
'date'=>'2015-03-11 22:00:00',
'active'=>'1',
'user'=>0,
'created_at'=>'2020-05-15 01:20:06',
'updated_at'=>'2020-06-21 00:08:23',
'deleted_at'=>NULL
] );


            
Post::create( [
'id'=>4835,
'cat_id'=>8,
'tag_id'=>3,
'title'=>'Permissive underfeeding or standard enteral feeding in critically ill adults.',
'text'=>'N Engl J Med. 201537225:2398–408',
'img'=>'file-1463591725-9J88SF.jpg',
'file'=>NULL,
'link'=>'http://www.nejm.org/doi/full/10.1056/NEJMoa1502826',
'by'=>0,
'journal'=>'Arabi YM, Aldawood AS, Haddad SH, Al-Dorzi HM, Tamim HM, Jones G, Mehta S, McIntyre L, Solaiman O, Sakkijha MH, Sadat M, Afesh L, PermiT Trial Group',
'type'=>0,
'month_article'=>0,
'famous'=>1,
'views'=>1196,
'show'=>0,
'date'=>'2015-04-14 22:00:00',
'active'=>'1',
'user'=>0,
'created_at'=>'2020-05-15 01:20:06',
'updated_at'=>'2020-06-21 00:08:23',
'deleted_at'=>NULL
] );


            
Post::create( [
'id'=>4836,
'cat_id'=>8,
'tag_id'=>3,
'title'=>'Permissive Underfeeding or Standard Enteral Feeding in High– and Low–Nutritional-Risk Critically Ill Adults. Post Hoc Analysis of the PermiT Trial',
'text'=>'Am J Respir Crit Care Med. 2017 Mar 11955:652-662',
'img'=>'file-1463591732-SM39NY.jpg',
'file'=>NULL,
'link'=>'https://www.atsjournals.org/doi/abs/10.1164/rccm.201605-1012OC',
'by'=>0,
'journal'=>'Arabi YM and others',
'type'=>0,
'month_article'=>0,
'famous'=>1,
'views'=>1196,
'show'=>0,
'date'=>'2017-03-14 22:00:00',
'active'=>'1',
'user'=>0,
'created_at'=>'2020-05-15 01:20:06',
'updated_at'=>'2020-06-21 00:08:23',
'deleted_at'=>NULL
] );


            
Post::create( [
'id'=>4837,
'cat_id'=>8,
'tag_id'=>3,
'title'=>'Early goal-directed nutrition versus standard of care in adult intensive care patients: the single-centre, randomised, outcome assessor-blinded EAT-ICU trial',
'text'=>'Intensive Care Med. 2017 Nov4311:1637-1647',
'img'=>'file-1463591725-9J88SF.jpg',
'file'=>NULL,
'link'=>'https://www.ncbi.nlm.nih.gov/pubmed/28936712',
'by'=>0,
'journal'=>'Allingstrup MJ and others',
'type'=>0,
'month_article'=>0,
'famous'=>1,
'views'=>1196,
'show'=>0,
'date'=>'2017-11-14 22:00:00',
'active'=>'1',
'user'=>0,
'created_at'=>'2020-05-15 01:20:06',
'updated_at'=>'2020-06-21 00:08:23',
'deleted_at'=>NULL
] );


            
Post::create( [
'id'=>4838,
'cat_id'=>8,
'tag_id'=>3,
'title'=>'Optimisation of energy provision with supplemental parenteral nutrition in critically ill patients: a randomised controlled clinical trial',
'text'=>'Lancet. 2013 Feb 23819864:385-93',
'img'=>'file-1463591732-SM39NY.jpg',
'file'=>NULL,
'link'=>'http://www.thelancet.com/journals/lancet/article/PIIS0140-6736(12)61351-8/abstract',
'by'=>0,
'journal'=>'Heidegger CP and others',
'type'=>0,
'month_article'=>0,
'famous'=>1,
'views'=>1196,
'show'=>0,
'date'=>'2013-02-14 22:00:00',
'active'=>'1',
'user'=>0,
'created_at'=>'2020-05-15 01:20:06',
'updated_at'=>'2020-06-21 00:08:23',
'deleted_at'=>NULL
] );


            
Post::create( [
'id'=>4839,
'cat_id'=>8,
'tag_id'=>1,
'title'=>'Population pharmacokinetics/pharmacodynamics of micafungin against Candida species in obese, critically ill, and morbidly obese critically ill patients',
'text'=>'Critical Care. 2018 22:94',
'img'=>'file-1463591657-K8BRDF.jpg',
'file'=>NULL,
'link'=>'https://ccforum.biomedcentral.com/articles/10.1186/s13054-018-2019-8',
'by'=>0,
'journal'=>'Emilio Maseda and others',
'type'=>0,
'month_article'=>0,
'famous'=>0,
'views'=>1130,
'show'=>0,
'date'=>'2018-04-15 22:00:00',
'active'=>'1',
'user'=>0,
'created_at'=>'2020-05-15 01:20:06',
'updated_at'=>'2020-06-21 00:08:23',
'deleted_at'=>NULL
] );


            
Post::create( [
'id'=>4840,
'cat_id'=>8,
'tag_id'=>3,
'title'=>'Tailoring nutrition therapy to illness and recovery',
'text'=>'Critical Care201721Suppl 3:316',
'img'=>'file-1463591732-SM39NY.jpg',
'file'=>NULL,
'link'=>'https://ccforum.biomedcentral.com/articles/10.1186/s13054-017-1906-8',
'by'=>0,
'journal'=>'Paul E. Wischmeyer',
'type'=>0,
'month_article'=>0,
'famous'=>1,
'views'=>1196,
'show'=>0,
'date'=>'2018-04-01 22:00:00',
'active'=>'1',
'user'=>0,
'created_at'=>'2020-05-15 01:20:06',
'updated_at'=>'2020-06-21 00:08:23',
'deleted_at'=>NULL
] );


            
Post::create( [
'id'=>4841,
'cat_id'=>8,
'tag_id'=>11,
'title'=>'Seven unconfirmed ideas to improve future ICU practice',
'text'=>'Critical Care 2017 21Suppl 3:315',
'img'=>'file-1464165480-R231XR.jpg',
'file'=>NULL,
'link'=>'https://ccforum.biomedcentral.com/articles/10.1186/s13054-017-1904-xutm_sourceBMC_mailing&utm_mediumEmail_Internal&utm_contentAJs_Health_BMC-MultiJournal-Hybris_dynamic-Multidisciplinary-Usage_driving-Global&utm_campaignVicHung-BMC-AJH_USG_BMC_Q1_2018',
'by'=>0,
'journal'=>'John J. Marini, Daniel De Backer, Can Ince et al.',
'type'=>0,
'month_article'=>0,
'famous'=>1,
'views'=>1196,
'show'=>0,
'date'=>'2018-03-31 22:00:00',
'active'=>'1',
'user'=>0,
'created_at'=>'2020-05-15 01:20:06',
'updated_at'=>'2020-06-21 00:08:23',
'deleted_at'=>NULL
] );


            
Post::create( [
'id'=>4842,
'cat_id'=>8,
'tag_id'=>1,
'title'=>'Fungal infections in adult patients on extracorporeal life support',
'text'=>'Critical Care 2018, 22:98  Published on: 17 April 2018',
'img'=>'file-1464106363-HWQX6B.jpg',
'file'=>NULL,
'link'=>'https://ccforum.biomedcentral.com/articles/10.1186/s13054-018-2023-z',
'by'=>0,
'journal'=>'Yiorgos Alexandros Cavayas, Hakeem Yusuff and Richard Porter',
'type'=>0,
'month_article'=>0,
'famous'=>0,
'views'=>1130,
'show'=>0,
'date'=>'2018-04-18 22:00:00',
'active'=>'1',
'user'=>0,
'created_at'=>'2020-05-15 01:20:06',
'updated_at'=>'2020-06-21 00:08:23',
'deleted_at'=>NULL
] );


            
Post::create( [
'id'=>4843,
'cat_id'=>8,
'tag_id'=>7,
'title'=>'Angiotensin II for the treatment of vasodilatory shock: enough data to consider angiotensin II safe',
'text'=>'Critical Care 2018, 22:96  Published on: 16 April 2018',
'img'=>'file-1463591639-4YG8JY.jpg',
'file'=>NULL,
'link'=>'https://ccforum.biomedcentral.com/articles/10.1186/s13054-018-2006-0',
'by'=>0,
'journal'=>'Nina Buchtele, Michael Schwameis and Bernd Jilma',
'type'=>0,
'month_article'=>0,
'famous'=>0,
'views'=>1101,
'show'=>0,
'date'=>'2018-04-18 22:00:00',
'active'=>'1',
'user'=>0,
'created_at'=>'2020-05-15 01:20:06',
'updated_at'=>'2020-06-21 00:08:23',
'deleted_at'=>NULL
] );


            
Post::create( [
'id'=>4844,
'cat_id'=>8,
'tag_id'=>8,
'title'=>'Early versus standard initiation of renal replacement therapy in furosemide stress test non-responsive acute kidney injury patients the FST trial',
'text'=>'Critical Care 2018, 22:101  Published on: 19 April 2018',
'img'=>'file-1463591746-GEE3E7.jpg',
'file'=>NULL,
'link'=>'https://ccforum.biomedcentral.com/articles/10.1186/s13054-018-2021-1',
'by'=>0,
'journal'=>'Nuttha Lumlertgul and others',
'type'=>0,
'month_article'=>0,
'famous'=>0,
'views'=>1152,
'show'=>0,
'date'=>'2018-04-19 22:00:00',
'active'=>'1',
'user'=>0,
'created_at'=>'2020-05-15 01:20:06',
'updated_at'=>'2020-06-21 00:08:23',
'deleted_at'=>NULL
] );


            
Post::create( [
'id'=>4845,
'cat_id'=>8,
'tag_id'=>7,
'title'=>'Resuscitative endovascular balloon occlusion of the aorta performed by emergency physicians for traumatic hemorrhagic shock: a case series from Japanese emergency rooms',
'text'=>'Critical Care 2018, 22:103  Published on: 21 April 2018',
'img'=>'file-1463591644-ZGN2DA.jpg',
'file'=>NULL,
'link'=>'https://ccforum.biomedcentral.com/articles/10.1186/s13054-018-2032-y',
'by'=>0,
'journal'=>'Ryota Sato and others',
'type'=>0,
'month_article'=>0,
'famous'=>0,
'views'=>1101,
'show'=>0,
'date'=>'2018-04-22 22:00:00',
'active'=>'1',
'user'=>0,
'created_at'=>'2020-05-15 01:20:06',
'updated_at'=>'2020-06-21 00:08:23',
'deleted_at'=>NULL
] );


            
Post::create( [
'id'=>4846,
'cat_id'=>12,
'tag_id'=>11,
'title'=>'Tenecteplase versus Alteplase before Thrombectomy for Ischemic Stroke',
'text'=>'N Engl J Med 2018378:1573-1582',
'img'=>'im1.jpg',
'file'=>NULL,
'link'=>'http://www.nejm.org/doi/full/10.1056/NEJMoa1716405?query=TOC',
'by'=>0,
'journal'=>'B.C.V. Campbell and Others',
'type'=>0,
'month_article'=>0,
'famous'=>1,
'views'=>1196,
'show'=>0,
'date'=>'2018-04-25 22:00:00',
'active'=>'1',
'user'=>0,
'created_at'=>'2020-05-15 01:20:06',
'updated_at'=>'2020-06-21 00:08:23',
'deleted_at'=>NULL
] );


            
Post::create( [
'id'=>4847,
'cat_id'=>8,
'tag_id'=>6,
'title'=>'Best PEEP trials are dependent on tidal volume',
'text'=>'Critical Care 2018, 22:115  Published on: 2 May 2018',
'img'=>'im6.jpg',
'file'=>NULL,
'link'=>'https://ccforum.biomedcentral.com/articles/10.1186/s13054-018-2047-4',
'by'=>0,
'journal'=>'Andrew C. McKown, Matthew W. Semler and Todd W. Rice',
'type'=>0,
'month_article'=>0,
'famous'=>0,
'views'=>1101,
'show'=>0,
'date'=>'2018-05-02 22:00:00',
'active'=>'1',
'user'=>0,
'created_at'=>'2020-05-15 01:20:06',
'updated_at'=>'2020-06-21 00:08:23',
'deleted_at'=>NULL
] );


            
Post::create( [
'id'=>4848,
'cat_id'=>8,
'tag_id'=>8,
'title'=>'Gut–kidney crosstalk in septic acute kidney injury',
'text'=>'Critical Care 2018, 22:117  Published on: 3 May 2018',
'img'=>'file-1463591738-IZ3XFW.jpg',
'file'=>NULL,
'link'=>'https://ccforum.biomedcentral.com/articles/10.1186/s13054-018-2040-y',
'by'=>0,
'journal'=>'Jingxiao Zhang and others',
'type'=>0,
'month_article'=>0,
'famous'=>0,
'views'=>1152,
'show'=>0,
'date'=>'2018-05-02 22:00:00',
'active'=>'1',
'user'=>0,
'created_at'=>'2020-05-15 01:20:06',
'updated_at'=>'2020-06-21 00:08:23',
'deleted_at'=>NULL
] );


            
Post::create( [
'id'=>4849,
'cat_id'=>8,
'tag_id'=>10,
'title'=>'Haemoglobin concentration and volume of intravenous fluids in septic shock in the ARISE trial',
'text'=>'Critical Care 2018, 22:118  Published on: 3 May 2018',
'img'=>'file-1463591685-DDLMK9.jpg',
'file'=>NULL,
'link'=>'https://ccforum.biomedcentral.com/articles/10.1186/s13054-018-2029-6',
'by'=>0,
'journal'=>'Matthew J and others',
'type'=>0,
'month_article'=>0,
'famous'=>0,
'views'=>1101,
'show'=>0,
'date'=>'2018-05-01 22:00:00',
'active'=>'1',
'user'=>0,
'created_at'=>'2020-05-15 01:20:06',
'updated_at'=>'2020-06-21 00:08:23',
'deleted_at'=>NULL
] );


            
Post::create( [
'id'=>4850,
'cat_id'=>8,
'tag_id'=>7,
'title'=>'Pathophysiology, echocardiographic evaluation, biomarker findings, and prognostic implications of septic cardiomyopathy: a review of the literature',
'text'=>'Critical Care 2018, 22:112  Published on: 4 May 2018',
'img'=>'file-1463591644-ZGN2DA.jpg',
'file'=>NULL,
'link'=>'https://ccforum.biomedcentral.com/articles/10.1186/s13054-018-2043-8',
'by'=>0,
'journal'=>'Robert R. Ehrman and others',
'type'=>0,
'month_article'=>0,
'famous'=>0,
'views'=>1101,
'show'=>0,
'date'=>'2018-05-03 22:00:00',
'active'=>'1',
'user'=>0,
'created_at'=>'2020-05-15 01:20:06',
'updated_at'=>'2020-06-21 00:08:23',
'deleted_at'=>NULL
] );


            
Post::create( [
'id'=>4851,
'cat_id'=>8,
'tag_id'=>1,
'title'=>'Implications for paediatric shock management in resource-limited settings: a perspective from the FEAST trial',
'text'=>'Critical Care 2018, 22:119  Published on: 4 May 2018',
'img'=>'file-1464165509-QNPJY8.jpg',
'file'=>NULL,
'link'=>'https://ccforum.biomedcentral.com/articles/10.1186/s13054-018-1966-4',
'by'=>0,
'journal'=>'Kirsty Anne Houston, Elizabeth C. George and Kathryn Maitland',
'type'=>0,
'month_article'=>0,
'famous'=>0,
'views'=>1130,
'show'=>0,
'date'=>'2018-05-07 22:00:00',
'active'=>'1',
'user'=>0,
'created_at'=>'2020-05-15 01:20:06',
'updated_at'=>'2020-06-21 00:08:23',
'deleted_at'=>NULL
] );


            
Post::create( [
'id'=>4852,
'cat_id'=>8,
'tag_id'=>4,
'title'=>'Development and validation of a pre-hospital “Red Flag” alert for activation of intra-hospital haemorrhage control response in blunt trauma',
'text'=>'Critical Care 2018, 22:113  Published on: 5 May 2018',
'img'=>'im3.jpg',
'file'=>NULL,
'link'=>'https://ccforum.biomedcentral.com/articles/10.1186/s13054-018-2026-9',
'by'=>0,
'journal'=>'Sophie Rym Hamada and others',
'type'=>0,
'month_article'=>0,
'famous'=>1,
'views'=>1293,
'show'=>0,
'date'=>'2018-05-13 22:00:00',
'active'=>'1',
'user'=>0,
'created_at'=>'2020-05-15 01:20:06',
'updated_at'=>'2020-06-21 00:08:23',
'deleted_at'=>NULL
] );


            
Post::create( [
'id'=>4853,
'cat_id'=>8,
'tag_id'=>6,
'title'=>'Impact of flow and temperature on patient comfort during respiratory support by high-flow nasal cannula',
'text'=>'Critical Care 2018, 22:120  Published on: 9 May 2018',
'img'=>'im5.jpg',
'file'=>NULL,
'link'=>'https://ccforum.biomedcentral.com/articles/10.1186/s13054-018-2039-4',
'by'=>0,
'journal'=>'Tommaso Mauri and others',
'type'=>0,
'month_article'=>0,
'famous'=>0,
'views'=>1101,
'show'=>0,
'date'=>'2018-05-08 22:00:00',
'active'=>'1',
'user'=>0,
'created_at'=>'2020-05-15 01:20:06',
'updated_at'=>'2020-06-21 00:08:23',
'deleted_at'=>NULL
] );

Post::create( [
    'id'=>4883,
    'cat_id'=>8,
    'tag_id'=>5,
    'title'=>'Rivaroxaban for Stroke Prevention after Embolic Stroke of Undetermined Source',
    'text'=>'N Engl J Med 2018 378:2191-2201',
    'img'=>'im1.jpg',
    'file'=>NULL,
    'link'=>'https://www.nejm.org/doi/full/10.1056/NEJMoa1802686?query=TOC',
    'by'=>0,
    'journal'=>'Robert G. Hart and others for the NAVIGATE ESUS Investigators',
    'type'=>0,
    'month_article'=>0,
    'famous'=>0,
    'views'=>1114,
    'show'=>0,
    'date'=>'2018-05-31 22:00:00',
    'active'=>'1',
    'user'=>0,
    'created_at'=>'2020-05-15 01:20:06',
    'updated_at'=>'2020-06-21 00:08:23',
    'deleted_at'=>NULL
    ] );
    
    
                
    Post::create( [
    'id'=>4884,
    'cat_id'=>8,
    'tag_id'=>5,
    'title'=>'Antiepileptic drugs in critically ill patients',
    'text'=>'Critical Care 2018, 22:153  Published on: 7 June 2018',
    'img'=>'im1.jpg',
    'file'=>NULL,
    'link'=>'https://ccforum.biomedcentral.com/articles/10.1186/s13054-018-2066-1',
    'by'=>0,
    'journal'=>'Salia Farrokh, Pouya Tahsili-Fahadan, Eva K. Ritzl, John J. Lewin and Marek A. Mirski',
    'type'=>0,
    'month_article'=>0,
    'famous'=>0,
    'views'=>1114,
    'show'=>0,
    'date'=>'2018-06-07 22:00:00',
    'active'=>'1',
    'user'=>0,
    'created_at'=>'2020-05-15 01:20:06',
    'updated_at'=>'2020-06-21 00:08:23',
    'deleted_at'=>NULL
    ] );
    
    
                
    Post::create( [
    'id'=>4885,
    'cat_id'=>8,
    'tag_id'=>11,
    'title'=>'Including family in ICU rounds could improve communication',
    'text'=>'HEALTH NEWS. JUNE 6, 2018',
    'img'=>'file-1464165480-R231XR.jpg',
    'file'=>NULL,
    'link'=>'https://www.reuters.com/article/us-health-icu-families/including-family-in-icu-rounds-could-improve-communication-idUSKCN1J22G6',
    'by'=>0,
    'journal'=>'Carolyn Crist',
    'type'=>0,
    'month_article'=>0,
    'famous'=>0,
    'views'=>1101,
    'show'=>0,
    'date'=>'2018-06-08 22:00:00',
    'active'=>'1',
    'user'=>0,
    'created_at'=>'2020-05-15 01:20:06',
    'updated_at'=>'2020-06-21 00:08:23',
    'deleted_at'=>NULL
    ] );
    
    
                
    Post::create( [
    'id'=>4886,
    'cat_id'=>8,
    'tag_id'=>3,
    'title'=>'Can nutritional interventions change major clinical outcomes',
    'text'=>'Critical Care 2018, 22:155  Published on: 8 June 2018',
    'img'=>'file-1463591725-9J88SF.jpg',
    'file'=>NULL,
    'link'=>'https://ccforum.biomedcentral.com/articles/10.1186/s13054-018-2085-y',
    'by'=>0,
    'journal'=>'Sho Horikita, Masamitsu Sanui and Alan K. Lefor',
    'type'=>0,
    'month_article'=>0,
    'famous'=>0,
    'views'=>1101,
    'show'=>0,
    'date'=>'2018-06-08 22:00:00',
    'active'=>'1',
    'user'=>0,
    'created_at'=>'2020-05-15 01:20:06',
    'updated_at'=>'2020-06-21 00:08:23',
    'deleted_at'=>NULL
    ] );
    
    
                
    Post::create( [
    'id'=>4887,
    'cat_id'=>8,
    'tag_id'=>8,
    'title'=>'Acute kidney injury and mild therapeutic hypothermia in patients after cardiopulmonary resuscitation - a post hoc analysis of a prospective observational trial',
    'text'=>'Critical Care 2018, 22:154  Published on: 8 June 2018',
    'img'=>'file-1463591746-GEE3E7.jpg',
    'file'=>NULL,
    'link'=>'https://ccforum.biomedcentral.com/articles/10.1186/s13054-018-2061-6',
    'by'=>0,
    'journal'=>'Julia Hasslacher, Fabian Barbieri, Ulrich Harler, Hanno Ulmer, Lui G. Forni, Romuald Bellmann and Michael Joannidis',
    'type'=>0,
    'month_article'=>0,
    'famous'=>0,
    'views'=>1152,
    'show'=>0,
    'date'=>'2018-06-08 22:00:00',
    'active'=>'1',
    'user'=>0,
    'created_at'=>'2020-05-15 01:20:06',
    'updated_at'=>'2020-06-21 00:08:23',
    'deleted_at'=>NULL
    ] );
    
    
                
    Post::create( [
    'id'=>4888,
    'cat_id'=>8,
    'tag_id'=>11,
    'title'=>'A Randomized Trial of a Family-Support Intervention in Intensive Care Units',
    'text'=>'N Engl J Med 2018 378:2365-2375.June 21, 2018',
    'img'=>'file-1464165480-R231XR.jpg',
    'file'=>NULL,
    'link'=>'https://www.nejm.org/doi/full/10.1056/NEJMoa1802637?query=TOC',
    'by'=>0,
    'journal'=>'Douglas B. White and others. for the PARTNER Investigators',
    'type'=>0,
    'month_article'=>0,
    'famous'=>0,
    'views'=>1101,
    'show'=>0,
    'date'=>'2018-06-24 22:00:00',
    'active'=>'1',
    'user'=>0,
    'created_at'=>'2020-05-15 01:20:06',
    'updated_at'=>'2020-06-21 00:08:23',
    'deleted_at'=>NULL
    ] );
    
    
                
    Post::create( [
    'id'=>4889,
    'cat_id'=>12,
    'tag_id'=>9,
    'title'=>'Receptor blockers superior to proton pump inhibitors in preventing GI bleeding',
    'text'=>'',
    'img'=>'file-1463591650-J14MMU.jpg',
    'file'=>NULL,
    'link'=>'https://healthmanagement.org/c/icu/news/receptor-blockers-superior-to-proton-pump-inhibitors-in-preventing-gi-bleeding',
    'by'=>0,
    'journal'=>'ICU Management and Practice. June 2018',
    'type'=>0,
    'month_article'=>0,
    'famous'=>0,
    'views'=>1101,
    'show'=>0,
    'date'=>'2018-06-24 22:00:00',
    'active'=>'1',
    'user'=>0,
    'created_at'=>'2020-05-15 01:20:06',
    'updated_at'=>'2020-06-21 00:08:23',
    'deleted_at'=>NULL
    ] );
    
    
                
    Post::create( [
    'id'=>4890,
    'cat_id'=>8,
    'tag_id'=>9,
    'title'=>'Comparative Effectiveness of Proton Pump Inhibitors vs. Histamine type-2 receptors blockers for preventing Clinically Important Gastrointestinal Bleeding during Intensive Care: A population-based study',
    'text'=>'Chest 2018',
    'img'=>'file-1463591657-K8BRDF.jpg',
    'file'=>NULL,
    'link'=>'https://journal.chestnet.org/article/S0012-3692(18)30795-5/pdf',
    'by'=>0,
    'journal'=>'Craig M. Lilly and others',
    'type'=>0,
    'month_article'=>0,
    'famous'=>0,
    'views'=>1101,
    'show'=>0,
    'date'=>'2018-05-31 22:00:00',
    'active'=>'1',
    'user'=>0,
    'created_at'=>'2020-05-15 01:20:06',
    'updated_at'=>'2020-06-21 00:08:23',
    'deleted_at'=>NULL
    ] );
    
    
                
    Post::create( [
    'id'=>4891,
    'cat_id'=>8,
    'tag_id'=>10,
    'title'=>'Early fibrinogen concentrate therapy for major haemorrhage in trauma E-FIT 1: results from a UK multi-centre, randomised, double blind, placebo-controlled pilot trial',
    'text'=>'Critical Care 2018, 22:164  Published on: 18 June 2018',
    'img'=>'file-1463591639-4YG8JY.jpg',
    'file'=>NULL,
    'link'=>'https://ccforum.biomedcentral.com/articles/10.1186/s13054-018-2086-x',
    'by'=>0,
    'journal'=>'Nicola Curry, and others',
    'type'=>0,
    'month_article'=>0,
    'famous'=>0,
    'views'=>1101,
    'show'=>0,
    'date'=>'2018-06-19 22:00:00',
    'active'=>'1',
    'user'=>0,
    'created_at'=>'2020-05-15 01:20:06',
    'updated_at'=>'2020-06-21 00:08:23',
    'deleted_at'=>NULL
    ] );
    
    
                
    Post::create( [
    'id'=>4892,
    'cat_id'=>8,
    'tag_id'=>10,
    'title'=>'Thrombocytopenia in the ICU: disseminated intravascular coagulation and thrombotic microangiopathies—what intensivists need to know',
    'text'=>'Critical Care 2018, 22:158  Published on: 13 June 2018',
    'img'=>'file-1463591681-AGRM7H.jpg',
    'file'=>NULL,
    'link'=>'https://ccforum.biomedcentral.com/articles/10.1186/s13054-018-2073-2',
    'by'=>0,
    'journal'=>'Jean-Louis Vincent and others',
    'type'=>0,
    'month_article'=>0,
    'famous'=>0,
    'views'=>1101,
    'show'=>0,
    'date'=>'2018-06-22 22:00:00',
    'active'=>'1',
    'user'=>0,
    'created_at'=>'2020-05-15 01:20:06',
    'updated_at'=>'2020-06-21 00:08:23',
    'deleted_at'=>NULL
    ] );
    
    
                
    Post::create( [
    'id'=>4893,
    'cat_id'=>8,
    'tag_id'=>11,
    'title'=>'Predictors of In-Hospital Mortality After Rapid Response Team Calls in a 274 Hospital Nationwide Sample',
    'text'=>'Critical Care Medicine. 467:1041-1048, July 2018',
    'img'=>'file-1464165480-R231XR.jpg',
    'file'=>NULL,
    'link'=>'https://journals.lww.com/ccmjournal/Fulltext/2018/07000/Predictors_of_In_Hospital_Mortality_After_Rapid.1.aspx',
    'by'=>0,
    'journal'=>'Shappell, Claire Snyder, Ashley Edelson, Dana P. More',
    'type'=>0,
    'month_article'=>0,
    'famous'=>0,
    'views'=>1101,
    'show'=>0,
    'date'=>'2018-06-30 22:00:00',
    'active'=>'1',
    'user'=>0,
    'created_at'=>'2020-05-15 01:20:06',
    'updated_at'=>'2020-06-21 00:08:23',
    'deleted_at'=>NULL
    ] );
    
    
                
    Post::create( [
    'id'=>4894,
    'cat_id'=>8,
    'tag_id'=>11,
    'title'=>'Early Enteral Nutrition Provided Within 24 Hours of ICU Admission: A Meta-Analysis of Randomized Controlled Trials',
    'text'=>'Critical Care Medicine. 467:1049-1056, July 2018',
    'img'=>'file-1463591725-9J88SF.jpg',
    'file'=>NULL,
    'link'=>'https://journals.lww.com/ccmjournal/Fulltext/2018/07000/Early_Enteral_Nutrition_Provided_Within_24_Hours.2.aspx',
    'by'=>0,
    'journal'=>'Tian, Feng Heighes, Philippa T. Allingstrup, Matilde J. More',
    'type'=>0,
    'month_article'=>0,
    'famous'=>0,
    'views'=>1101,
    'show'=>0,
    'date'=>'2018-06-30 22:00:00',
    'active'=>'1',
    'user'=>0,
    'created_at'=>'2020-05-15 01:20:06',
    'updated_at'=>'2020-06-21 00:08:23',
    'deleted_at'=>NULL
    ] );
    
    
                
    Post::create( [
    'id'=>4895,
    'cat_id'=>8,
    'tag_id'=>11,
    'title'=>'Effect of Administration of Ramelteon, a Melatonin Receptor Agonist, on the Duration of Stay in the ICU: A Single-Center Randomized Placebo-Controlled Trial',
    'text'=>'Critical Care Medicine. 467:1099-1105, July 2018',
    'img'=>'im1.jpg',
    'file'=>NULL,
    'link'=>'https://journals.lww.com/ccmjournal/Fulltext/2018/07000/Effect_of_Administration_of_Ramelteon,_a_Melatonin.9.aspx',
    'by'=>0,
    'journal'=>'Nishikimi, Mitsuaki Numaguchi, Atsushi Takahashi, Kunihiko More',
    'type'=>0,
    'month_article'=>0,
    'famous'=>0,
    'views'=>1101,
    'show'=>0,
    'date'=>'2018-06-30 22:00:00',
    'active'=>'1',
    'user'=>0,
    'created_at'=>'2020-05-15 01:20:06',
    'updated_at'=>'2020-06-21 00:08:23',
    'deleted_at'=>NULL
    ] );
    
    
                
    Post::create( [
    'id'=>4896,
    'cat_id'=>8,
    'tag_id'=>11,
    'title'=>'Ventilator Bundle and Its Effects on Mortality Among ICU Patients: A Meta-Analysis',
    'text'=>'Critical Care Medicine. 467:1167-1174, July 2018',
    'img'=>'im6.jpg',
    'file'=>NULL,
    'link'=>'https://journals.lww.com/ccmjournal/Abstract/2018/07000/Ventilator_Bundle_and_Its_Effects_on_Mortality.17.aspx',
    'by'=>0,
    'journal'=>'Pileggi, Claudia Mascaro, Valentina Bianco, Aida More',
    'type'=>0,
    'month_article'=>0,
    'famous'=>0,
    'views'=>1101,
    'show'=>0,
    'date'=>'2018-07-09 22:00:00',
    'active'=>'1',
    'user'=>0,
    'created_at'=>'2020-05-15 01:20:06',
    'updated_at'=>'2020-06-21 00:08:23',
    'deleted_at'=>NULL
    ] );
    
    
                
    Post::create( [
    'id'=>4897,
    'cat_id'=>12,
    'tag_id'=>7,
    'title'=>'Definitions and pathophysiology of vasoplegic shock',
    'text'=>'Critical Care 2018, 22:174  Published on: 6 July 2018',
    'img'=>'file-1463591644-ZGN2DA.jpg',
    'file'=>NULL,
    'link'=>'https://ccforum.biomedcentral.com/articles/10.1186/s13054-018-2102-1',
    'by'=>0,
    'journal'=>'Simon Lambden, Ben C. Creagh-Brown, Julie Hunt, Charlotte Summers and Lui G. Forni',
    'type'=>0,
    'month_article'=>0,
    'famous'=>0,
    'views'=>1212,
    'show'=>0,
    'date'=>'2018-07-05 22:00:00',
    'active'=>'1',
    'user'=>0,
    'created_at'=>'2020-05-15 01:20:06',
    'updated_at'=>'2020-06-21 00:08:23',
    'deleted_at'=>NULL
    ] );
    
    
                
    Post::create( [
    'id'=>4898,
    'cat_id'=>8,
    'tag_id'=>11,
    'title'=>'Early application of continuous high-volume haemofiltration can reduce sepsis and improve the prognosis of patients with severe burns',
    'text'=>'Critical Care 2018, 22:173  Published on: 6 July 2018',
    'img'=>'file-1463591738-IZ3XFW.jpg',
    'file'=>NULL,
    'link'=>'https://ccforum.biomedcentral.com/articles/10.1186/s13054-018-2095-9',
    'by'=>0,
    'journal'=>'Bo You, Yu Long Zhang and others',
    'type'=>0,
    'month_article'=>0,
    'famous'=>0,
    'views'=>1101,
    'show'=>0,
    'date'=>'2018-07-05 22:00:00',
    'active'=>'1',
    'user'=>0,
    'created_at'=>'2020-05-15 01:20:06',
    'updated_at'=>'2020-06-21 00:08:23',
    'deleted_at'=>NULL
    ] );
    
    
                
    Post::create( [
    'id'=>4899,
    'cat_id'=>8,
    'tag_id'=>11,
    'title'=>'Life Lessons from Paul in the Face of Death',
    'text'=>'N Engl J Med. July 11, 2018',
    'img'=>'im3.jpg',
    'file'=>NULL,
    'link'=>'https://www.nejm.org/doi/full/10.1056/NEJMp1808695?query=TOC',
    'by'=>0,
    'journal'=>'Jeffrey M. Drazen, M.D.',
    'type'=>0,
    'month_article'=>0,
    'famous'=>0,
    'views'=>1101,
    'show'=>0,
    'date'=>'2018-07-11 22:00:00',
    'active'=>'1',
    'user'=>0,
    'created_at'=>'2020-05-15 01:20:06',
    'updated_at'=>'2020-06-21 00:08:23',
    'deleted_at'=>NULL
    ] );
    
    
                
    Post::create( [
    'id'=>4900,
    'cat_id'=>12,
    'tag_id'=>6,
    'title'=>'UK study: extubating ventilated patients on vasoactive infusions safe',
    'text'=>'Source: American Journal of Respiratory and Critical Care Medicine',
    'img'=>'im5.jpg',
    'file'=>NULL,
    'link'=>'https://healthmanagement.org/c/icu/news/uk-study-extubating-ventilated-patients-on-vasoactive-infusions-safe',
    'by'=>0,
    'journal'=>'ICU Management and Practice',
    'type'=>0,
    'month_article'=>0,
    'famous'=>0,
    'views'=>1212,
    'show'=>0,
    'date'=>'2018-07-13 22:00:00',
    'active'=>'1',
    'user'=>0,
    'created_at'=>'2020-05-15 01:20:06',
    'updated_at'=>'2020-06-21 00:08:23',
    'deleted_at'=>NULL
    ] );
    
    
                
    Post::create( [
    'id'=>4901,
    'cat_id'=>8,
    'tag_id'=>11,
    'title'=>'Safety of Extubating Mechanically Ventilated Patients on Vasoactive Infusions: A Retrospective Cohort Study',
    'text'=>'American Journal of Respiratory and Critical Care Medicine\r\nPublished Online: June 28, 2018',
    'img'=>'file-1463591639-4YG8JY.jpg',
    'file'=>NULL,
    'link'=>'https://www.atsjournals.org/doi/pdf/10.1164/rccm.201712-2492LE',
    'by'=>0,
    'journal'=>'Tara Quasim , Martin Shaw , Joanne McPeake , Martin Hughes , and Theodore J. Iwashyna',
    'type'=>0,
    'month_article'=>0,
    'famous'=>0,
    'views'=>1101,
    'show'=>0,
    'date'=>'2018-06-30 22:00:00',
    'active'=>'1',
    'user'=>0,
    'created_at'=>'2020-05-15 01:20:06',
    'updated_at'=>'2020-06-21 00:08:23',
    'deleted_at'=>NULL
    ] );
    
    
                
    Post::create( [
    'id'=>4902,
    'cat_id'=>8,
    'tag_id'=>7,
    'title'=>'Despite ECMO use, mortality remains high in patients with cardiogenic shock',
    'text'=>'Source: BMC Emergency Medicine',
    'img'=>'file-1463591644-ZGN2DA.jpg',
    'file'=>NULL,
    'link'=>'https://healthmanagement.org/c/icu/news/despite-ecmo-use-mortality-remains-high-in-patients-with-cardiogenic-shock',
    'by'=>0,
    'journal'=>'ICU Management and Practice',
    'type'=>0,
    'month_article'=>0,
    'famous'=>0,
    'views'=>1101,
    'show'=>0,
    'date'=>'2018-07-13 22:00:00',
    'active'=>'1',
    'user'=>0,
    'created_at'=>'2020-05-15 01:20:06',
    'updated_at'=>'2020-06-21 00:08:23',
    'deleted_at'=>NULL
    ] );
    
    
                
    Post::create( [
    'id'=>4903,
    'cat_id'=>8,
    'tag_id'=>11,
    'title'=>'Helping ICU clinicians provide high touch and high tech care',
    'text'=>'Source: Annals of the American Thoracic Society',
    'img'=>'file-1464165480-R231XR.jpg',
    'file'=>NULL,
    'link'=>'https://healthmanagement.org/c/icu/news/helping-icu-clinicians-provide-high-touch-and-high-tech-care',
    'by'=>0,
    'journal'=>'ICU Management and Practice',
    'type'=>0,
    'month_article'=>0,
    'famous'=>0,
    'views'=>1101,
    'show'=>0,
    'date'=>'2018-07-13 22:00:00',
    'active'=>'1',
    'user'=>0,
    'created_at'=>'2020-05-15 01:20:06',
    'updated_at'=>'2020-06-21 00:08:23',
    'deleted_at'=>NULL
    ] );
    
    
                
    Post::create( [
    'id'=>4904,
    'cat_id'=>8,
    'tag_id'=>11,
    'title'=>'The Cost of Caring: Emotion, Burnout, and Psychological Distress in Critical Care Clinicians',
    'text'=>'Ann Am Thorac Soc. 2018 Jul157:787-790',
    'img'=>'file-1464165463-D3CBM9.jpg',
    'file'=>NULL,
    'link'=>'https://www.atsjournals.org/doi/full/10.1513/AnnalsATS.201804-269PS',
    'by'=>0,
    'journal'=>'Costa DK, Moss M',
    'type'=>0,
    'month_article'=>0,
    'famous'=>1,
    'views'=>1196,
    'show'=>0,
    'date'=>'2018-07-10 22:00:00',
    'active'=>'1',
    'user'=>0,
    'created_at'=>'2020-05-15 01:20:06',
    'updated_at'=>'2020-06-21 00:08:23',
    'deleted_at'=>NULL
    ] );
    
    
                
    Post::create( [
    'id'=>4905,
    'cat_id'=>8,
    'tag_id'=>6,
    'title'=>'Use of ECMO in ARDS: does the EOLIA trial really help',
    'text'=>'Critical Care 2018, 22:171  Published on: 5 July 2018',
    'img'=>'im6.jpg',
    'file'=>NULL,
    'link'=>'https://ccforum.biomedcentral.com/articles/10.1186/s13054-018-2098-6',
    'by'=>0,
    'journal'=>'Luciano Gattinoni, Francesco Vasques and Michael Quintel',
    'type'=>0,
    'month_article'=>0,
    'famous'=>1,
    'views'=>1196,
    'show'=>0,
    'date'=>'2018-07-05 22:00:00',
    'active'=>'1',
    'user'=>0,
    'created_at'=>'2020-05-15 01:20:06',
    'updated_at'=>'2020-06-21 00:08:23',
    'deleted_at'=>NULL
    ] );
    
    
                
    Post::create( [
    'id'=>4906,
    'cat_id'=>8,
    'tag_id'=>6,
    'title'=>'Extracorporeal Membrane Oxygenation for Severe Acute Respiratory Distress Syndrome',
    'text'=>'N Engl J Med. 201837821:1965-1975',
    'img'=>'file-1463591644-ZGN2DA.jpg',
    'file'=>NULL,
    'link'=>'https://www.nejm.org/doi/full/10.1056/NEJMoa1800385',
    'by'=>0,
    'journal'=>'Combes A, Hajage D, and others, for the EOLIA Trial Group, REVA, and ECMONet',
    'type'=>0,
    'month_article'=>0,
    'famous'=>1,
    'views'=>1196,
    'show'=>0,
    'date'=>'2018-05-24 22:00:00',
    'active'=>'1',
    'user'=>0,
    'created_at'=>'2020-05-15 01:20:06',
    'updated_at'=>'2020-06-21 00:08:23',
    'deleted_at'=>NULL
    ] );
    
    
                
    Post::create( [
    'id'=>4907,
    'cat_id'=>9,
    'tag_id'=>7,
    'title'=>'New 2017 ACC/AHA High Blood Pressure Guidelines Lower Definition of Hypertension',
    'text'=>'Nov 13, 2017',
    'img'=>'file-1463591639-4YG8JY.jpg',
    'file'=>NULL,
    'link'=>'https://www.acc.org/latest-in-cardiology/articles/2017/11/08/11/47/mon-5pm-bp-guideline-aha-2017',
    'by'=>0,
    'journal'=>'Journal of the American College of Cardiology',
    'type'=>0,
    'month_article'=>0,
    'famous'=>0,
    'views'=>1101,
    'show'=>0,
    'date'=>'2017-10-31 22:00:00',
    'active'=>'1',
    'user'=>0,
    'created_at'=>'2020-05-15 01:20:06',
    'updated_at'=>'2020-06-21 00:08:23',
    'deleted_at'=>NULL
    ] );
    
    
                
    Post::create( [
    'id'=>4908,
    'cat_id'=>8,
    'tag_id'=>10,
    'title'=>'Thrombocytopenia in the ICU: disseminated intravascular coagulation and thrombotic microangiopathies—what intensivists need to know',
    'text'=>'Critical Care 2018 22:158',
    'img'=>'file-1463591681-AGRM7H.jpg',
    'file'=>NULL,
    'link'=>'https://ccforum.biomedcentral.com/articles/10.1186/s13054-018-2073-2sap-outbound-idE560029F7D5F6348F9738D6DF28D8BC6DBABAE0D',
    'by'=>0,
    'journal'=>'Jean-Louis Vincent and others',
    'type'=>0,
    'month_article'=>0,
    'famous'=>1,
    'views'=>1196,
    'show'=>0,
    'date'=>'2018-06-12 22:00:00',
    'active'=>'1',
    'user'=>0,
    'created_at'=>'2020-05-15 01:20:06',
    'updated_at'=>'2020-06-21 00:08:23',
    'deleted_at'=>NULL
    ] );
    
    
                
    Post::create( [
    'id'=>4909,
    'cat_id'=>8,
    'tag_id'=>10,
    'title'=>'A Phase 3 Trial of l-Glutamine in Sickle Cell Disease',
    'text'=>'N Engl J Med 2018 379:226-235. July 19, 2018',
    'img'=>'file-1463591685-DDLMK9.jpg',
    'file'=>NULL,
    'link'=>'https://www.nejm.org/doi/full/10.1056/NEJMoa1715971?query=TOC',
    'by'=>0,
    'journal'=>'Yutaka Niihara, and others',
    'type'=>0,
    'month_article'=>0,
    'famous'=>0,
    'views'=>1101,
    'show'=>0,
    'date'=>'2018-07-18 22:00:00',
    'active'=>'1',
    'user'=>0,
    'created_at'=>'2020-05-15 01:20:06',
    'updated_at'=>'2020-06-21 00:08:23',
    'deleted_at'=>NULL
    ] );
    
    
                
    Post::create( [
    'id'=>4910,
    'cat_id'=>8,
    'tag_id'=>1,
    'title'=>'Procalcitonin-Guided Use of Antibiotics for Lower Respiratory Tract Infection',
    'text'=>'N Engl J Med 2018379:236-249  Published Online May 20, 2018',
    'img'=>'file-1464106363-HWQX6B.jpg',
    'file'=>NULL,
    'link'=>'https://www.nejm.org/doi/full/10.1056/NEJMoa1802670?query=TOC',
    'by'=>0,
    'journal'=>'D.T. Huang and Others',
    'type'=>0,
    'month_article'=>0,
    'famous'=>0,
    'views'=>1130,
    'show'=>0,
    'date'=>'2018-07-18 22:00:00',
    'active'=>'1',
    'user'=>0,
    'created_at'=>'2020-05-15 01:20:06',
    'updated_at'=>'2020-06-21 00:08:23',
    'deleted_at'=>NULL
    ] );
    
    
                
    Post::create( [
    'id'=>4911,
    'cat_id'=>8,
    'tag_id'=>9,
    'title'=>'Effect of decompressive laparotomy on organ function in patients with abdominal compartment syndrome: a systematic review and meta-analysis',
    'text'=>'Critical Care 2018, 22:179  Published on: 25 July 2018',
    'img'=>'file-1463591650-J14MMU.jpg',
    'file'=>NULL,
    'link'=>'https://ccforum.biomedcentral.com/articles/10.1186/s13054-018-2103-0',
    'by'=>0,
    'journal'=>'Lana Van Damme and Jan J. De Waele',
    'type'=>0,
    'month_article'=>0,
    'famous'=>0,
    'views'=>1101,
    'show'=>0,
    'date'=>'2018-07-25 22:00:00',
    'active'=>'1',
    'user'=>0,
    'created_at'=>'2020-05-15 01:20:06',
    'updated_at'=>'2020-06-21 00:08:23',
    'deleted_at'=>NULL
    ] );
    
    
                
    Post::create( [
    'id'=>4912,
    'cat_id'=>8,
    'tag_id'=>3,
    'title'=>'The Effect of IV Amino Acid Supplementation on Mortality in ICU Patients May Be Dependent on Kidney Function: Post Hoc Subgroup Analyses of a Multicenter Randomized Trial',
    'text'=>'Critical Care Medicine. 468:1293-1301, August 2018',
    'img'=>'file-1463591725-9J88SF.jpg',
    'file'=>NULL,
    'link'=>'https://journals.lww.com/ccmjournal/Abstract/2018/08000/The_Effect_of_IV_Amino_Acid_Supplementation_on.12.aspx',
    'by'=>0,
    'journal'=>'Zhu, Ran Allingstrup, Matilde J. and others',
    'type'=>0,
    'month_article'=>0,
    'famous'=>0,
    'views'=>1101,
    'show'=>0,
    'date'=>'2018-07-31 22:00:00',
    'active'=>'1',
    'user'=>0,
    'created_at'=>'2020-05-15 01:20:06',
    'updated_at'=>'2020-06-21 00:08:23',
    'deleted_at'=>NULL
    ] );
    
    
                
    Post::create( [
    'id'=>4913,
    'cat_id'=>8,
    'tag_id'=>4,
    'title'=>'Early Interventions for the Prevention of Posttraumatic Stress Symptoms in Survivors of Critical Illness: A Qualitative Systematic Review',
    'text'=>'Critical Care Medicine. 468:1328-1333, August 2018',
    'img'=>'file-1463591639-4YG8JY.jpg',
    'file'=>NULL,
    'link'=>'https://journals.lww.com/ccmjournal/Abstract/2018/08000/Early_Interventions_for_the_Prevention_of.16.aspx',
    'by'=>0,
    'journal'=>'Roberts, Michael B and others',
    'type'=>0,
    'month_article'=>0,
    'famous'=>0,
    'views'=>1198,
    'show'=>0,
    'date'=>'2018-07-31 22:00:00',
    'active'=>'1',
    'user'=>0,
    'created_at'=>'2020-05-15 01:20:06',
    'updated_at'=>'2020-06-21 00:08:23',
    'deleted_at'=>NULL
    ] );
    
    
                
    Post::create( [
    'id'=>4914,
    'cat_id'=>8,
    'tag_id'=>6,
    'title'=>'Noninvasive Ventilation in Patients With Do-Not-Intubate and Comfort-Measures-Only Orders: A Systematic Review and Meta-Analysis',
    'text'=>'Critical Care Medicine. 468:1209-1216, August 2018',
    'img'=>'im6.jpg',
    'file'=>NULL,
    'link'=>'https://journals.lww.com/ccmjournal/Fulltext/2018/08000/Noninvasive_Ventilation_in_Patients_With.1.aspx',
    'by'=>0,
    'journal'=>'Wilson, Michael E. and others',
    'type'=>0,
    'month_article'=>0,
    'famous'=>0,
    'views'=>1101,
    'show'=>0,
    'date'=>'2018-07-31 22:00:00',
    'active'=>'1',
    'user'=>0,
    'created_at'=>'2020-05-15 01:20:06',
    'updated_at'=>'2020-06-21 00:08:23',
    'deleted_at'=>NULL
    ] );
    
    
                
    Post::create( [
    'id'=>4915,
    'cat_id'=>7,
    'tag_id'=>11,
    'title'=>'Near-Continuous Glucose Monitoring Makes Glycemic Control Safer in ICU Patients',
    'text'=>'Critical Care Medicine. 468:1224-1229, August 2018',
    'img'=>'file-1464106363-HWQX6B.jpg',
    'file'=>NULL,
    'link'=>'https://journals.lww.com/ccmjournal/Abstract/2018/08000/Near_Continuous_Glucose_Monitoring_Makes_Glycemic.3.aspx',
    'by'=>0,
    'journal'=>'Preiser, Jean-Charles and others',
    'type'=>0,
    'month_article'=>0,
    'famous'=>0,
    'views'=>1101,
    'show'=>0,
    'date'=>'2018-07-31 22:00:00',
    'active'=>'1',
    'user'=>0,
    'created_at'=>'2020-05-15 01:20:06',
    'updated_at'=>'2020-06-21 00:08:23',
    'deleted_at'=>NULL
    ] );
    
    
                
    Post::create( [
    'id'=>4916,
    'cat_id'=>8,
    'tag_id'=>6,
    'title'=>'High-flow nasal cannula oxygen therapy decreases postextubation neuroventilatory drive and work of breathing in patients with chronic obstructive pulmonary disease',
    'text'=>'Critical Care 2018, 22:180  Published on: 2 August 2018',
    'img'=>'im5.jpg',
    'file'=>NULL,
    'link'=>'https://ccforum.biomedcentral.com/articles/10.1186/s13054-018-2107-9',
    'by'=>0,
    'journal'=>'Rosa Di mussi and others',
    'type'=>0,
    'month_article'=>0,
    'famous'=>0,
    'views'=>1101,
    'show'=>0,
    'date'=>'2018-08-02 22:00:00',
    'active'=>'1',
    'user'=>0,
    'created_at'=>'2020-05-15 01:20:06',
    'updated_at'=>'2020-06-21 00:08:23',
    'deleted_at'=>NULL
    ] );
    
    
                
    Post::create( [
    'id'=>4917,
    'cat_id'=>8,
    'tag_id'=>3,
    'title'=>'Validation of carbon dioxide production VCO2 as a tool to calculate resting energy expenditure REE in mechanically ventilated critically ill patients: a retrospective observational study',
    'text'=>'Critical Care 2018, 22:186  Published on: 3 August 2018',
    'img'=>'file-1463591725-9J88SF.jpg',
    'file'=>NULL,
    'link'=>'https://ccforum.biomedcentral.com/articles/10.1186/s13054-018-2108-8',
    'by'=>0,
    'journal'=>'I. Kagan, O. Zusman, I. Bendavid, M. Theilla, J. Cohen and P. Singer',
    'type'=>0,
    'month_article'=>0,
    'famous'=>0,
    'views'=>1101,
    'show'=>0,
    'date'=>'2018-08-03 22:00:00',
    'active'=>'1',
    'user'=>0,
    'created_at'=>'2020-05-15 01:20:06',
    'updated_at'=>'2020-06-21 00:08:23',
    'deleted_at'=>NULL
    ] );
    
    
                
    Post::create( [
    'id'=>4918,
    'cat_id'=>8,
    'tag_id'=>11,
    'title'=>'Corticosteroid use and intensive care unit-acquired weakness: a systematic review and meta-analysis',
    'text'=>'Critical Care 2018, 22:187  Published on: 3 August 2018',
    'img'=>'file-1464165463-D3CBM9.jpg',
    'file'=>NULL,
    'link'=>'https://ccforum.biomedcentral.com/articles/10.1186/s13054-018-2111-0',
    'by'=>0,
    'journal'=>'Tao Yang, Zhiqiang Li, Li Jiang and Xiuming Xi',
    'type'=>0,
    'month_article'=>0,
    'famous'=>0,
    'views'=>1101,
    'show'=>0,
    'date'=>'2018-08-03 22:00:00',
    'active'=>'1',
    'user'=>0,
    'created_at'=>'2020-05-15 01:20:06',
    'updated_at'=>'2020-06-21 00:08:23',
    'deleted_at'=>NULL
    ] );
    
    
                
    Post::create( [
    'id'=>4919,
    'cat_id'=>9,
    'tag_id'=>5,
    'title'=>'Clinical Practice Guidelines for Healthcare-Associated Ventriculitis and Meningitis',
    'text'=>'Published: Clinical Infectious Diseases  2017  64 : 34 -65',
    'img'=>'im1.jpg',
    'file'=>NULL,
    'link'=>'https://www.idsociety.org/Guidelines/Patient_Care/IDSA_Practice_Guidelines/Infections_By_Organ_System-81567/Central_Nervous_System/Healthcare-Associated_Ventriculitis_and_Meningitis/#recommendations',
    'by'=>0,
    'journal'=>'Site Admin',
    'type'=>0,
    'month_article'=>0,
    'famous'=>0,
    'views'=>1114,
    'show'=>0,
    'date'=>'2017-11-30 22:00:00',
    'active'=>'1',
    'user'=>0,
    'created_at'=>'2020-05-15 01:20:06',
    'updated_at'=>'2020-06-21 00:08:23',
    'deleted_at'=>NULL
    ] );
    
    
                
    Post::create( [
    'id'=>4920,
    'cat_id'=>8,
    'tag_id'=>5,
    'title'=>'Acute Viral Encephalitis',
    'text'=>'N Engl J Med 2018379:557-566',
    'img'=>'file-1463591639-4YG8JY.jpg',
    'file'=>NULL,
    'link'=>'https://www.nejm.org/doi/full/10.1056/NEJMra1708714?query=TOC',
    'by'=>0,
    'journal'=>'K.L. Tyler',
    'type'=>0,
    'month_article'=>0,
    'famous'=>0,
    'views'=>1114,
    'show'=>0,
    'date'=>'2018-08-08 22:00:00',
    'active'=>'1',
    'user'=>0,
    'created_at'=>'2020-05-15 01:20:06',
    'updated_at'=>'2020-06-21 00:08:23',
    'deleted_at'=>NULL
    ] );
    
    
                
    Post::create( [
    'id'=>4921,
    'cat_id'=>9,
    'tag_id'=>5,
    'title'=>'Practice guideline update recommendations summary: Disorders of consciousness',
    'text'=>'Neurology 201800:1-11. d',
    'img'=>'im2.jpg',
    'file'=>NULL,
    'link'=>'http://n.neurology.org/content/neurology/early/2018/08/08/WNL.0000000000005926.full.pdf',
    'by'=>0,
    'journal'=>'Joseph T. Giacino, et al',
    'type'=>0,
    'month_article'=>0,
    'famous'=>0,
    'views'=>1114,
    'show'=>0,
    'date'=>'2018-08-10 22:00:00',
    'active'=>'1',
    'user'=>0,
    'created_at'=>'2020-05-15 01:20:06',
    'updated_at'=>'2020-06-21 00:08:23',
    'deleted_at'=>NULL
    ] );
    
    
                
    Post::create( [
    'id'=>4922,
    'cat_id'=>8,
    'tag_id'=>5,
    'title'=>'New guideline released for managing vegetative and minimally conscious states',
    'text'=>'August 8, 2018, American Academy of Neurology',
    'img'=>'im1.jpg',
    'file'=>NULL,
    'link'=>'https://medicalxpress.com/news/2018-08-guideline-vegetative-minimally-conscious-states.html',
    'by'=>0,
    'journal'=>'Site Admin',
    'type'=>0,
    'month_article'=>0,
    'famous'=>0,
    'views'=>1114,
    'show'=>0,
    'date'=>'2018-08-07 22:00:00',
    'active'=>'1',
    'user'=>0,
    'created_at'=>'2020-05-15 01:20:06',
    'updated_at'=>'2020-06-21 00:08:23',
    'deleted_at'=>NULL
    ] );
    
    
                
    Post::create( [
    'id'=>4923,
    'cat_id'=>8,
    'tag_id'=>2,
    'title'=>'Music helps prevent delirium in elderly critical care patients',
    'text'=>'Published on Intensive & Critical Care Nursing: Wed, 8 Aug 2018',
    'img'=>'file-1464165480-R231XR.jpg',
    'file'=>NULL,
    'link'=>'https://healthmanagement.org/c/icu/news/music-helps-prevent-delirium-in-elderly-critical-care-patients',
    'by'=>0,
    'journal'=>'ICU Management and Practice',
    'type'=>0,
    'month_article'=>0,
    'famous'=>0,
    'views'=>1172,
    'show'=>0,
    'date'=>'2018-08-11 22:00:00',
    'active'=>'1',
    'user'=>0,
    'created_at'=>'2020-05-15 01:20:06',
    'updated_at'=>'2020-06-21 00:08:23',
    'deleted_at'=>NULL
    ] );
    
    
                
    Post::create( [
    'id'=>4924,
    'cat_id'=>8,
    'tag_id'=>2,
    'title'=>'Music intervention to prevent delirium among older patients admitted to a trauma intensive care unit and a trauma orthopaedic unit',
    'text'=>'Intensive Crit Care Nurs 47: 7–14 August 2018',
    'img'=>'file-1464165463-D3CBM9.jpg',
    'file'=>NULL,
    'link'=>'https://www.sciencedirect.com/science/article/pii/S0964339716301112?via%3Dihub',
    'by'=>0,
    'journal'=>'Kari Johnson Julie Fleury Darya McClain',
    'type'=>0,
    'month_article'=>0,
    'famous'=>0,
    'views'=>1172,
    'show'=>0,
    'date'=>'2018-08-11 22:00:00',
    'active'=>'1',
    'user'=>0,
    'created_at'=>'2020-05-15 01:20:06',
    'updated_at'=>'2020-06-21 00:08:23',
    'deleted_at'=>NULL
    ] );
    
    
                
    Post::create( [
    'id'=>4925,
    'cat_id'=>8,
    'tag_id'=>11,
    'title'=>'New study confirms reliability of ICU-RESPECT index',
    'text'=>'Source: Journal of Critical Care',
    'img'=>'file-1463591639-4YG8JY.jpg',
    'file'=>NULL,
    'link'=>'https://healthmanagement.org/c/icu/news/new-study-confirms-reliability-of-icu-respect-index',
    'by'=>0,
    'journal'=>'ICU Management and Practice',
    'type'=>0,
    'month_article'=>0,
    'famous'=>0,
    'views'=>1101,
    'show'=>0,
    'date'=>'2018-08-11 22:00:00',
    'active'=>'1',
    'user'=>0,
    'created_at'=>'2020-05-15 01:20:06',
    'updated_at'=>'2020-06-21 00:08:23',
    'deleted_at'=>NULL
    ] );
    
    
                
    Post::create( [
    'id'=>4926,
    'cat_id'=>12,
    'tag_id'=>6,
    'title'=>'Many patients undergoing mechanical ventilation still receive daily chest x-rays, despite guidelines discouraging their use, study indicates',
    'text'=>'JAMA Network Open. 201814:e181119',
    'img'=>'im6.jpg',
    'file'=>NULL,
    'link'=>'https://jamanetwork.com/journals/jamanetworkopen/fullarticle/2696507',
    'by'=>0,
    'journal'=>'Hayley B. Gershengorn and other',
    'type'=>0,
    'month_article'=>0,
    'famous'=>0,
    'views'=>1212,
    'show'=>0,
    'date'=>'2018-08-12 22:00:00',
    'active'=>'1',
    'user'=>0,
    'created_at'=>'2020-05-15 01:20:06',
    'updated_at'=>'2020-06-21 00:08:23',
    'deleted_at'=>NULL
    ] );
    
    
                
    Post::create( [
    'id'=>4927,
    'cat_id'=>8,
    'tag_id'=>11,
    'title'=>'Association of Intensive Care Unit Patient-to-Intensivist Ratios With Hospital Mortality',
    'text'=>'JAMA Intern Med. 2017 1773:388-396',
    'img'=>'file-1464165480-R231XR.jpg',
    'file'=>NULL,
    'link'=>'https://jamanetwork.com/journals/jamainternalmedicine/fullarticle/2598511?resultClick=1',
    'by'=>0,
    'journal'=>'Hayley B. Gershengorn, MD David A. Harrison, PhD Allan Garland, MD, MA M. Elizabeth Wilcox, MD, MPH Kathryn M. Rowan, DPhil Hannah Wunsch, MD, MSc',
    'type'=>0,
    'month_article'=>0,
    'famous'=>1,
    'views'=>1196,
    'show'=>0,
    'date'=>'2017-10-31 22:00:00',
    'active'=>'1',
    'user'=>0,
    'created_at'=>'2020-05-15 01:20:06',
    'updated_at'=>'2020-06-21 00:08:23',
    'deleted_at'=>NULL
    ] );
    
    
                
    Post::create( [
    'id'=>4928,
    'cat_id'=>12,
    'tag_id'=>7,
    'title'=>'JAMA. 20183205:433-434',
    'text'=>'JAMA. 20183205:433-434',
    'img'=>'file-1463591644-ZGN2DA.jpg',
    'file'=>NULL,
    'link'=>'https://jamanetwork.com/journals/jama/article-abstract/2684931',
    'by'=>0,
    'journal'=>'Cian P. McCarthy and others',
    'type'=>0,
    'month_article'=>0,
    'famous'=>0,
    'views'=>1212,
    'show'=>0,
    'date'=>'2018-08-12 22:00:00',
    'active'=>'1',
    'user'=>0,
    'created_at'=>'2020-05-15 01:20:06',
    'updated_at'=>'2020-06-21 00:08:23',
    'deleted_at'=>NULL
    ] );
    
    
                
    Post::create( [
    'id'=>4929,
    'cat_id'=>8,
    'tag_id'=>9,
    'title'=>'Probiotics to Prevent Clostridium difficile Infection in Patients Receiving Antibiotics',
    'text'=>'JAMA. 20183205:499-500',
    'img'=>'file-1463591639-4YG8JY.jpg',
    'file'=>NULL,
    'link'=>'https://jamanetwork.com/journals/jama/article-abstract/2688768',
    'by'=>0,
    'journal'=>'Joshua Z. Goldenberg, ND Dominik Mertz, MD Bradley C. Johnston, PhD',
    'type'=>0,
    'month_article'=>0,
    'famous'=>0,
    'views'=>1101,
    'show'=>0,
    'date'=>'2018-08-12 22:00:00',
    'active'=>'1',
    'user'=>0,
    'created_at'=>'2020-05-15 01:20:06',
    'updated_at'=>'2020-06-21 00:08:23',
    'deleted_at'=>NULL
    ] );

    Post::create( [
        'id'=>4931,
        'cat_id'=>10,
        'tag_id'=>10,
        'title'=>'Recombinant Factor 7',
        'text'=>'',
        'img'=>'file-1463591685-DDLMK9.jpg',
        'file'=>NULL,
        'link'=>'https://www.novo-pi.com/novosevenrt.pdf',
        'by'=>0,
        'journal'=>'Full Prescribing Information',
        'type'=>0,
        'month_article'=>0,
        'famous'=>0,
        'views'=>1101,
        'show'=>0,
        'date'=>'2016-08-14 22:00:00',
        'active'=>'1',
        'user'=>0,
        'created_at'=>'2020-05-15 01:20:06',
        'updated_at'=>'2020-06-21 00:08:23',
        'deleted_at'=>NULL
        ] );
        
        
                    
        Post::create( [
        'id'=>4932,
        'cat_id'=>8,
        'tag_id'=>3,
        'title'=>'Effect of procalcitonin-guided antibiotic treatment on clinical outcomes in intensive care unit patients with infection and sepsis patients: a patient-level meta-analysis of randomized trials',
        'text'=>'Critical Care 2018, 22:191  Published on: 15 August 2018',
        'img'=>'file-1464106363-HWQX6B.jpg',
        'file'=>NULL,
        'link'=>'https://ccforum.biomedcentral.com/articles/10.1186/s13054-018-2125-7',
        'by'=>0,
        'journal'=>'Yannick Wirz and others',
        'type'=>0,
        'month_article'=>0,
        'famous'=>0,
        'views'=>1101,
        'show'=>0,
        'date'=>'2018-08-16 22:00:00',
        'active'=>'1',
        'user'=>0,
        'created_at'=>'2020-05-15 01:20:06',
        'updated_at'=>'2020-06-21 00:08:23',
        'deleted_at'=>NULL
        ] );
        
        
                    
        Post::create( [
        'id'=>4933,
        'cat_id'=>8,
        'tag_id'=>7,
        'title'=>'Bundle of care taking into account time to improve long-term outcome after cardiac arrest',
        'text'=>'Critical Care 2018, 22:192  Published on: 15 August 2018',
        'img'=>'file-1463591644-ZGN2DA.jpg',
        'file'=>NULL,
        'link'=>'https://ccforum.biomedcentral.com/articles/10.1186/s13054-018-2128-4',
        'by'=>0,
        'journal'=>'R. Jouffroy and B. Vivien',
        'type'=>0,
        'month_article'=>0,
        'famous'=>0,
        'views'=>1101,
        'show'=>0,
        'date'=>'2018-08-16 22:00:00',
        'active'=>'1',
        'user'=>0,
        'created_at'=>'2020-05-15 01:20:06',
        'updated_at'=>'2020-06-21 00:08:23',
        'deleted_at'=>NULL
        ] );
        
        
                    
        Post::create( [
        'id'=>4934,
        'cat_id'=>8,
        'tag_id'=>7,
        'title'=>'Airway and ventilation management during cardiopulmonary resuscitation and after successful resuscitation',
        'text'=>'Critical Care 2018, 22:190  Published on: 15 August 2018',
        'img'=>'file-1463591639-4YG8JY.jpg',
        'file'=>NULL,
        'link'=>'https://ccforum.biomedcentral.com/articles/10.1186/s13054-018-2121-y',
        'by'=>0,
        'journal'=>'Christopher Newell, Scott Grier and Jasmeet Soar',
        'type'=>0,
        'month_article'=>0,
        'famous'=>0,
        'views'=>1101,
        'show'=>0,
        'date'=>'2018-08-16 22:00:00',
        'active'=>'1',
        'user'=>0,
        'created_at'=>'2020-05-15 01:20:06',
        'updated_at'=>'2020-06-21 00:08:23',
        'deleted_at'=>NULL
        ] );
        
        
                    
        Post::create( [
        'id'=>4935,
        'cat_id'=>8,
        'tag_id'=>5,
        'title'=>'MRI-Guided Thrombolysis for Stroke with Unknown Time of Onset',
        'text'=>'N Engl J Med 2018379:611-622',
        'img'=>'im1.jpg',
        'file'=>NULL,
        'link'=>'https://www.nejm.org/doi/full/10.1056/NEJMoa1804355queryTOC',
        'by'=>0,
        'journal'=>'G. Thomalla and Others',
        'type'=>0,
        'month_article'=>0,
        'famous'=>1,
        'views'=>1209,
        'show'=>0,
        'date'=>'2018-08-15 22:00:00',
        'active'=>'1',
        'user'=>0,
        'created_at'=>'2020-05-15 01:20:06',
        'updated_at'=>'2020-06-21 00:08:23',
        'deleted_at'=>NULL
        ] );
        
        
                    
        Post::create( [
        'id'=>4936,
        'cat_id'=>8,
        'tag_id'=>11,
        'title'=>'Could ultrasound-guided internal jugular vein catheter insertion replace the use of chest X-ray',
        'text'=>'Critical Care 2018, 22:206  Published on: 16 August 2018',
        'img'=>'file-1470233162-2MW1RX.jpg',
        'file'=>NULL,
        'link'=>'https://ccforum.biomedcentral.com/articles/10.1186/s13054-018-2130-x',
        'by'=>0,
        'journal'=>'William F. Amaya-Zuñiga and Fernando Raffán-Sanabria',
        'type'=>0,
        'month_article'=>0,
        'famous'=>0,
        'views'=>1101,
        'show'=>0,
        'date'=>'2018-08-16 22:00:00',
        'active'=>'1',
        'user'=>0,
        'created_at'=>'2020-05-15 01:20:06',
        'updated_at'=>'2020-06-21 00:08:23',
        'deleted_at'=>NULL
        ] );
        
        
                    
        Post::create( [
        'id'=>4937,
        'cat_id'=>8,
        'tag_id'=>7,
        'title'=>'Adjuvant vitamin C in cardiac arrest patients undergoing renal replacement therapy: an appeal for a higher high-dose',
        'text'=>'Critical Care 2018. 22:207',
        'img'=>'file-1464165463-D3CBM9.jpg',
        'file'=>NULL,
        'link'=>'https://ccforum.biomedcentral.com/articles/10.1186/s13054-018-2115-9',
        'by'=>0,
        'journal'=>'Patrick M. Honore and others',
        'type'=>0,
        'month_article'=>0,
        'famous'=>0,
        'views'=>1101,
        'show'=>0,
        'date'=>'2018-08-16 22:00:00',
        'active'=>'1',
        'user'=>0,
        'created_at'=>'2020-05-15 01:20:06',
        'updated_at'=>'2020-06-21 00:08:23',
        'deleted_at'=>NULL
        ] );
        
        
                    
        Post::create( [
        'id'=>4938,
        'cat_id'=>8,
        'tag_id'=>8,
        'title'=>'Efficacy of polymyxin B hemoperfusion in and beyond septic shock: is an “endotoxin severity score” needed',
        'text'=>'Critical Care 2018, 22:205  Published on: 17 August 2018',
        'img'=>'file-1463591738-IZ3XFW.jpg',
        'file'=>NULL,
        'link'=>'https://ccforum.biomedcentral.com/articles/10.1186/s13054-018-2093-y',
        'by'=>0,
        'journal'=>'Patrick M. Honore, David De Bels, Thierry Preseau, Sebastien Redant and Herbert D. Spapen',
        'type'=>0,
        'month_article'=>0,
        'famous'=>0,
        'views'=>1152,
        'show'=>0,
        'date'=>'2018-08-18 22:00:00',
        'active'=>'1',
        'user'=>0,
        'created_at'=>'2020-05-15 01:20:06',
        'updated_at'=>'2020-06-21 00:08:23',
        'deleted_at'=>NULL
        ] );
        
        
                    
        Post::create( [
        'id'=>4939,
        'cat_id'=>8,
        'tag_id'=>7,
        'title'=>'Intravenous ivabradine versus placebo in patients with low cardiac output syndrome treated by dobutamine after elective coronary artery bypass surgery: a phase 2 exploratory randomized controlled trial',
        'text'=>'Critical Care 2018, 22:193  Published on: 17 August 2018',
        'img'=>'file-1463591644-ZGN2DA.jpg',
        'file'=>NULL,
        'link'=>'https://ccforum.biomedcentral.com/articles/10.1186/s13054-018-2124-8',
        'by'=>0,
        'journal'=>'Lee S. Nguyen and others',
        'type'=>0,
        'month_article'=>0,
        'famous'=>0,
        'views'=>1101,
        'show'=>0,
        'date'=>'2018-08-18 22:00:00',
        'active'=>'1',
        'user'=>0,
        'created_at'=>'2020-05-15 01:20:06',
        'updated_at'=>'2020-06-21 00:08:23',
        'deleted_at'=>NULL
        ] );
        
        
                    
        Post::create( [
        'id'=>4940,
        'cat_id'=>12,
        'tag_id'=>11,
        'title'=>'Corticosteroid use associated with ICU-acquired weakness',
        'text'=>'Source: Critical Care',
        'img'=>'file-1464106363-HWQX6B.jpg',
        'file'=>NULL,
        'link'=>'https://healthmanagement.org/c/icu/news/corticosteroid-use-associated-with-icu-acquired-weakness',
        'by'=>0,
        'journal'=>'ICU Management and Practice',
        'type'=>0,
        'month_article'=>0,
        'famous'=>0,
        'views'=>1212,
        'show'=>0,
        'date'=>'2018-08-18 22:00:00',
        'active'=>'1',
        'user'=>0,
        'created_at'=>'2020-05-15 01:20:06',
        'updated_at'=>'2020-06-21 00:08:23',
        'deleted_at'=>NULL
        ] );
        
        
                    
        Post::create( [
        'id'=>4941,
        'cat_id'=>8,
        'tag_id'=>3,
        'title'=>'Indirect calorimetry still the best REE assessment tool for ventilated patients',
        'text'=>'Source: Critical Care',
        'img'=>'file-1463591732-SM39NY.jpg',
        'file'=>NULL,
        'link'=>'https://healthmanagement.org/c/icu/news/indirect-calorimetry-still-the-best-ree-assessment-tool-for-ventilated-patients',
        'by'=>0,
        'journal'=>'ICU Management and Practice',
        'type'=>0,
        'month_article'=>0,
        'famous'=>0,
        'views'=>1101,
        'show'=>0,
        'date'=>'2018-08-18 22:00:00',
        'active'=>'1',
        'user'=>0,
        'created_at'=>'2020-05-15 01:20:06',
        'updated_at'=>'2020-06-21 00:08:23',
        'deleted_at'=>NULL
        ] );
        
        
                    
        Post::create( [
        'id'=>4942,
        'cat_id'=>8,
        'tag_id'=>7,
        'title'=>'Value of variation index of inferior vena cava diameter in predicting fluid responsiveness in patients with circulatory shock receiving mechanical ventilation: a systematic review and meta-analysis',
        'text'=>'Critical Care 2018, 22:204  Published on: 21 August 2018',
        'img'=>'file-1463591639-4YG8JY.jpg',
        'file'=>NULL,
        'link'=>'https://ccforum.biomedcentral.com/articles/10.1186/s13054-018-2063-4',
        'by'=>0,
        'journal'=>'Haijun Huang, Qinkang Shen, Yafen Liu, Hua Xu and Yixin Fang',
        'type'=>0,
        'month_article'=>0,
        'famous'=>0,
        'views'=>1101,
        'show'=>0,
        'date'=>'2018-08-21 22:00:00',
        'active'=>'1',
        'user'=>0,
        'created_at'=>'2020-05-15 01:20:06',
        'updated_at'=>'2020-06-21 00:08:23',
        'deleted_at'=>NULL
        ] );
        
        
                    
        Post::create( [
        'id'=>4943,
        'cat_id'=>12,
        'tag_id'=>7,
        'title'=>'A Randomized Trial of Epinephrine in Out-of-Hospital Cardiac Arrest',
        'text'=>'N Engl J Med 2018379:711-721  Published Online July 18, 2018',
        'img'=>'file-1463591657-K8BRDF.jpg',
        'file'=>NULL,
        'link'=>'https://www.nejm.org/doi/full/10.1056/NEJMoa1806842?query=TOC',
        'by'=>0,
        'journal'=>'G.D. Perkins and Others',
        'type'=>0,
        'month_article'=>0,
        'famous'=>0,
        'views'=>1212,
        'show'=>0,
        'date'=>'2018-08-22 22:00:00',
        'active'=>'1',
        'user'=>0,
        'created_at'=>'2020-05-15 01:20:06',
        'updated_at'=>'2020-06-21 00:08:23',
        'deleted_at'=>NULL
        ] );
        
        
                    
        Post::create( [
        'id'=>4944,
        'cat_id'=>8,
        'tag_id'=>6,
        'title'=>'Twice-Daily Tooth Brushing Regimen Cut Hospital-Acquired Pneumonia Rate By 92 Percent',
        'text'=>'The Roanoke Times luanne.rife@roanoke.com',
        'img'=>'file-1463591752-XK911T.jpg',
        'file'=>NULL,
        'link'=>'https://www.roanoke.com/news/local/salem/salem-va-leads-national-research-tooth-brushing-each-day-keeps/article_8a959bde-f3c4-11e7-a933-7b5029467621.html',
        'by'=>0,
        'journal'=>'By Luanne Rife',
        'type'=>0,
        'month_article'=>0,
        'famous'=>0,
        'views'=>1101,
        'show'=>0,
        'date'=>'2018-08-24 22:00:00',
        'active'=>'1',
        'user'=>0,
        'created_at'=>'2020-05-15 01:20:06',
        'updated_at'=>'2020-06-21 00:08:23',
        'deleted_at'=>NULL
        ] );
        
        
                    
        Post::create( [
        'id'=>4945,
        'cat_id'=>8,
        'tag_id'=>10,
        'title'=>'Rivaroxaban in Patients with Heart Failure, Sinus Rhythm, and Coronary Disease',
        'text'=>'NEJM, August 27, 2018',
        'img'=>'file-1463591681-AGRM7H.jpg',
        'file'=>NULL,
        'link'=>'https://www.nejm.org/doi/full/10.1056/NEJMoa1808848?query=RP',
        'by'=>0,
        'journal'=>'F. Zannad and Others, for the COMMANDER HF Investigators',
        'type'=>0,
        'month_article'=>0,
        'famous'=>0,
        'views'=>1101,
        'show'=>0,
        'date'=>'2018-08-26 22:00:00',
        'active'=>'1',
        'user'=>0,
        'created_at'=>'2020-05-15 01:20:06',
        'updated_at'=>'2020-06-21 00:08:23',
        'deleted_at'=>NULL
        ] );
        
        
                    
        Post::create( [
        'id'=>4946,
        'cat_id'=>8,
        'tag_id'=>11,
        'title'=>'Effects of n−3 Fatty Acid Supplements in Diabetes Mellitus',
        'text'=>'NEJM, August 26, 2018',
        'img'=>'file-1463591725-9J88SF.jpg',
        'file'=>NULL,
        'link'=>'https://www.nejm.org/doi/full/10.1056/NEJMoa1804989?query=featured_home',
        'by'=>0,
        'journal'=>'The ASCEND Study Collaborative Group',
        'type'=>0,
        'month_article'=>0,
        'famous'=>0,
        'views'=>1101,
        'show'=>0,
        'date'=>'2018-08-25 22:00:00',
        'active'=>'1',
        'user'=>0,
        'created_at'=>'2020-05-15 01:20:06',
        'updated_at'=>'2020-06-21 00:08:23',
        'deleted_at'=>NULL
        ] );
        
        
                    
        Post::create( [
        'id'=>4947,
        'cat_id'=>8,
        'tag_id'=>11,
        'title'=>'ECMO for Severe ARDS',
        'text'=>'N Engl J Med 2018379:884-887',
        'img'=>'file-1463591644-ZGN2DA.jpg',
        'file'=>NULL,
        'link'=>'https://www.nejm.org/doi/full/10.1056/NEJMclde1804601?query=TOC',
        'by'=>0,
        'journal'=>'M.Y. Mi, M.A. Matthay, and A.H. Morris',
        'type'=>0,
        'month_article'=>0,
        'famous'=>0,
        'views'=>1101,
        'show'=>0,
        'date'=>'2018-08-29 22:00:00',
        'active'=>'1',
        'user'=>0,
        'created_at'=>'2020-05-15 01:20:06',
        'updated_at'=>'2020-06-21 00:08:23',
        'deleted_at'=>NULL
        ] );
        
        
                    
        Post::create( [
        'id'=>4948,
        'cat_id'=>8,
        'tag_id'=>1,
        'title'=>'Corticosteroids in Sepsis: An Updated Systematic Review and Meta-Analysis',
        'text'=>'Critical Care Medicine. 469:1411-1420, September 2018.',
        'img'=>'file-1464165463-D3CBM9.jpg',
        'file'=>NULL,
        'link'=>'https://journals.lww.com/ccmjournal/Fulltext/2018/09000/Corticosteroids_in_Sepsis___An_Updated_Systematic.4.aspx',
        'by'=>0,
        'journal'=>'Rochwerg, Bram and others',
        'type'=>0,
        'month_article'=>0,
        'famous'=>0,
        'views'=>1130,
        'show'=>0,
        'date'=>'2018-09-02 22:00:00',
        'active'=>'1',
        'user'=>0,
        'created_at'=>'2020-05-15 01:20:06',
        'updated_at'=>'2020-06-21 00:08:23',
        'deleted_at'=>NULL
        ] );
        
        
                    
        Post::create( [
        'id'=>4949,
        'cat_id'=>8,
        'tag_id'=>11,
        'title'=>'Co-Occurrence of Post-Intensive Care Syndrome Problems Among 406 Survivors of Critical Illness',
        'text'=>'Critical Care Medicine. 469:1393-1401, September 2018',
        'img'=>'file-1470233162-2MW1RX.jpg',
        'file'=>NULL,
        'link'=>'https://journals.lww.com/ccmjournal/Fulltext/2018/09000/Co_Occurrence_of_Post_Intensive_Care_Syndrome.2.aspx',
        'by'=>0,
        'journal'=>'Marra, Annachiara and others',
        'type'=>0,
        'month_article'=>0,
        'famous'=>0,
        'views'=>1101,
        'show'=>0,
        'date'=>'2018-09-01 22:00:00',
        'active'=>'1',
        'user'=>0,
        'created_at'=>'2020-05-15 01:20:06',
        'updated_at'=>'2020-06-21 00:08:23',
        'deleted_at'=>NULL
        ] );
        
        
                    
        Post::create( [
        'id'=>4950,
        'cat_id'=>8,
        'tag_id'=>11,
        'title'=>'Economic Evaluation of a Patient-Directed Music Intervention for ICU Patients Receiving Mechanical Ventilatory Support',
        'text'=>'Critical Care Medicine. 469:1430-1435, September 2018',
        'img'=>'im2.jpg',
        'file'=>NULL,
        'link'=>'https://journals.lww.com/ccmjournal/Abstract/2018/09000/Economic_Evaluation_of_a_Patient_Directed_Music.6.aspx',
        'by'=>0,
        'journal'=>'Chlan, Linda L and others',
        'type'=>0,
        'month_article'=>0,
        'famous'=>0,
        'views'=>1101,
        'show'=>0,
        'date'=>'2018-09-01 22:00:00',
        'active'=>'1',
        'user'=>0,
        'created_at'=>'2020-05-15 01:20:06',
        'updated_at'=>'2020-06-21 00:08:23',
        'deleted_at'=>NULL
        ] );
        
        
                    
        Post::create( [
        'id'=>4951,
        'cat_id'=>8,
        'tag_id'=>6,
        'title'=>'E-cigarettes may reduce exacerbations, improve exercise capacity in COPD patients whose condition worsened when smoking conventional cigarettes, study suggests',
        'text'=>'AUGUST 31, 2018',
        'img'=>'file-1463591752-XK911T.jpg',
        'file'=>NULL,
        'link'=>'https://copdnewstoday.com/2018/08/31/switch-to-electronic-cigarettes-may-reduce-copd-progression-study-reports/',
        'by'=>0,
        'journal'=>'COPD News Today',
        'type'=>0,
        'month_article'=>0,
        'famous'=>0,
        'views'=>1101,
        'show'=>0,
        'date'=>'2018-09-03 22:00:00',
        'active'=>'1',
        'user'=>0,
        'created_at'=>'2020-05-15 01:20:06',
        'updated_at'=>'2020-06-21 00:08:23',
        'deleted_at'=>NULL
        ] );
        
        
                    
        Post::create( [
        'id'=>4952,
        'cat_id'=>12,
        'tag_id'=>7,
        'title'=>'2019 ESC Clinical Practice Guidelines',
        'text'=>'',
        'img'=>'file-1463591644-ZGN2DA.jpg',
        'file'=>NULL,
        'link'=>'https://www.escardio.org/Guidelines/Clinical-Practice-Guidelines',
        'by'=>0,
        'journal'=>'www.escardio.org',
        'type'=>0,
        'month_article'=>0,
        'famous'=>0,
        'views'=>1212,
        'show'=>0,
        'date'=>'2018-09-04 22:00:00',
        'active'=>'1',
        'user'=>0,
        'created_at'=>'2020-05-15 01:20:06',
        'updated_at'=>'2020-06-21 00:08:23',
        'deleted_at'=>NULL
        ] );
        
        
                    
        Post::create( [
        'id'=>4954,
        'cat_id'=>8,
        'tag_id'=>3,
        'title'=>'Assessment of perioperative nutrition practices and attitudes—A national survey of colorectal and GI surgical oncology programs',
        'text'=>'American Journal of Surgery',
        'img'=>'file-1463591732-SM39NY.jpg',
        'file'=>NULL,
        'link'=>'https://www.americanjournalofsurgery.com/article/S0002-9610(16)30867-4/pdf',
        'by'=>0,
        'journal'=>'J.D. Williams, Paul E. Wischmeyer',
        'type'=>0,
        'month_article'=>0,
        'famous'=>1,
        'views'=>1196,
        'show'=>0,
        'date'=>'2017-06-12 22:00:00',
        'active'=>'1',
        'user'=>0,
        'created_at'=>'2020-05-15 01:20:06',
        'updated_at'=>'2020-06-21 00:08:23',
        'deleted_at'=>NULL
        ] );
        
        
                    
        Post::create( [
        'id'=>4955,
        'cat_id'=>8,
        'tag_id'=>3,
        'title'=>'Nutrition‐Focused Quality Improvement Program Results in Significant Readmission and Length of Stay Reductions for Malnourished Surgical Patients',
        'text'=>'Journal of Enteral and Parenteral Nutrition. 02 February 2018',
        'img'=>'file-1463591732-SM39NY.jpg',
        'file'=>NULL,
        'link'=>'https://onlinelibrary.wiley.com/doi/full/10.1002/jpen.1040',
        'by'=>0,
        'journal'=>'Krishnan Sriram and others',
        'type'=>0,
        'month_article'=>0,
        'famous'=>1,
        'views'=>1196,
        'show'=>0,
        'date'=>'2018-02-12 22:00:00',
        'active'=>'1',
        'user'=>0,
        'created_at'=>'2020-05-15 01:20:06',
        'updated_at'=>'2020-06-21 00:08:23',
        'deleted_at'=>NULL
        ] );
        
        
                    
        Post::create( [
        'id'=>4956,
        'cat_id'=>9,
        'tag_id'=>3,
        'title'=>'American Society for Enhanced Recovery and Perioperative Quality Initiative Joint Consensus Statement on Nutrition Screening and Therapy Within a Surgical Enhanced Recovery Pathway',
        'text'=>'Anesth Analg. 2018 Jun1266:1883-1895',
        'img'=>'file-1463591725-9J88SF.jpg',
        'file'=>NULL,
        'link'=>'https://journals.lww.com/anesthesia-analgesia/Fulltext/2018/06000/American_Society_for_Enhanced_Recovery_and.19.aspx',
        'by'=>0,
        'journal'=>'Wischmeyer PE and others, Perioperative Quality Initiative POQI 2 Workgroup',
        'type'=>0,
        'month_article'=>0,
        'famous'=>0,
        'views'=>1101,
        'show'=>0,
        'date'=>'2018-06-12 22:00:00',
        'active'=>'1',
        'user'=>0,
        'created_at'=>'2020-05-15 01:20:06',
        'updated_at'=>'2020-06-21 00:08:23',
        'deleted_at'=>NULL
        ] );
        
        
                    
        Post::create( [
        'id'=>4957,
        'cat_id'=>8,
        'tag_id'=>7,
        'title'=>'Effect of Aspirin on All-Cause Mortality in the Healthy Elderly',
        'text'=>'N Eng J Med, September 16, 2018',
        'img'=>'file-1464165463-D3CBM9.jpg',
        'file'=>NULL,
        'link'=>'https://www.nejm.org/doi/full/10.1056/NEJMoa1803955?query=RP',
        'by'=>0,
        'journal'=>'J.J. McNeil and Others for the ASPREE Investigator Group',
        'type'=>0,
        'month_article'=>0,
        'famous'=>0,
        'views'=>1101,
        'show'=>0,
        'date'=>'2018-09-16 22:00:00',
        'active'=>'1',
        'user'=>0,
        'created_at'=>'2020-05-15 01:20:06',
        'updated_at'=>'2020-06-21 00:08:23',
        'deleted_at'=>NULL
        ] );
        
        
                    
        Post::create( [
        'id'=>4958,
        'cat_id'=>8,
        'tag_id'=>11,
        'title'=>'Vasopressors May Contribute to ICU-Related Weakness',
        'text'=>'Critical Care  General Critical Care',
        'img'=>'file-1463591639-4YG8JY.jpg',
        'file'=>NULL,
        'link'=>'https://www.medpagetoday.com/criticalcare/generalcriticalcare/75030',
        'by'=>0,
        'journal'=>'Salynn Boyles, Contributing Writer September 11, 2018',
        'type'=>0,
        'month_article'=>0,
        'famous'=>0,
        'views'=>1101,
        'show'=>0,
        'date'=>'2018-09-10 22:00:00',
        'active'=>'1',
        'user'=>0,
        'created_at'=>'2020-05-15 01:20:06',
        'updated_at'=>'2020-06-21 00:08:23',
        'deleted_at'=>NULL
        ] );
        
        
                    
        Post::create( [
        'id'=>4959,
        'cat_id'=>12,
        'tag_id'=>7,
        'title'=>'Sodium bicarbonate therapy for patients with severe metabolic acidaemia in the intensive care unit BICAR-ICU: a multicentre, open-label, randomised controlled, phase 3 trial.',
        'text'=>'Lancet. 2018 Jul 739210141:31-40',
        'img'=>'file-1463591644-ZGN2DA.jpg',
        'file'=>NULL,
        'link'=>'https://www.thelancet.com/journals/lancet/article/PIIS0140-6736%2818%2931080-8/fulltext',
        'by'=>0,
        'journal'=>'Jaber S, et al. BICAR-ICU Study Group',
        'type'=>0,
        'month_article'=>0,
        'famous'=>1,
        'views'=>1307,
        'show'=>0,
        'date'=>'2018-07-16 22:00:00',
        'active'=>'1',
        'user'=>0,
        'created_at'=>'2020-05-15 01:20:06',
        'updated_at'=>'2020-06-21 00:08:23',
        'deleted_at'=>NULL
        ] );
        
        
                    
        Post::create( [
        'id'=>4960,
        'cat_id'=>8,
        'tag_id'=>7,
        'title'=>'Effect of Sodium Bicarbonate Infusion on Patient Outcomes',
        'text'=>'',
        'img'=>'file-1463591644-ZGN2DA.jpg',
        'file'=>NULL,
        'link'=>'http://www.sccm.org/Blog/September-2018/Concise-Critical-Appraisal-Fluid-Administration-a?_zs=3THjd1&_zl=mP895',
        'by'=>0,
        'journal'=>'SCCM, The Intensive Care Professionals',
        'type'=>0,
        'month_article'=>0,
        'famous'=>0,
        'views'=>1101,
        'show'=>0,
        'date'=>'2018-09-16 22:00:00',
        'active'=>'1',
        'user'=>0,
        'created_at'=>'2020-05-15 01:20:06',
        'updated_at'=>'2020-06-21 00:08:23',
        'deleted_at'=>NULL
        ] );
        
        
                    
        Post::create( [
        'id'=>4961,
        'cat_id'=>8,
        'tag_id'=>10,
        'title'=>'FDA approves antidote for apixaban and rivaroxaban',
        'text'=>'',
        'img'=>'file-1463591681-AGRM7H.jpg',
        'file'=>NULL,
        'link'=>'https://www.andexxa.com/',
        'by'=>0,
        'journal'=>'Site Admin',
        'type'=>0,
        'month_article'=>0,
        'famous'=>0,
        'views'=>1101,
        'show'=>0,
        'date'=>'2018-09-12 22:00:00',
        'active'=>'1',
        'user'=>0,
        'created_at'=>'2020-05-15 01:20:06',
        'updated_at'=>'2020-06-21 00:08:23',
        'deleted_at'=>NULL
        ] );
        
        
                    
        Post::create( [
        'id'=>4962,
        'cat_id'=>8,
        'tag_id'=>11,
        'title'=>'We should avoid the term “fluid overload”',
        'text'=>'Critical Care 2018, 22:214  Published on: 11 September 2018',
        'img'=>'file-1463591644-ZGN2DA.jpg',
        'file'=>NULL,
        'link'=>'https://ccforum.biomedcentral.com/articles/10.1186/s13054-018-2141-7',
        'by'=>0,
        'journal'=>'Jean-Louis Vincent and Michael R. Pinsky',
        'type'=>0,
        'month_article'=>0,
        'famous'=>0,
        'views'=>1101,
        'show'=>0,
        'date'=>'2018-09-10 22:00:00',
        'active'=>'1',
        'user'=>0,
        'created_at'=>'2020-05-15 01:20:06',
        'updated_at'=>'2020-06-21 00:08:23',
        'deleted_at'=>NULL
        ] );
        
        
                    
        Post::create( [
        'id'=>4963,
        'cat_id'=>8,
        'tag_id'=>11,
        'title'=>'Single-dose influenza drug baloxavir similar to oseltamivir in efficacy',
        'text'=>'MDedge News. Publish date: September 5, 2018',
        'img'=>'file-1463591752-XK911T.jpg',
        'file'=>NULL,
        'link'=>'https://www.mdedge.com/chestphysician/article/174090/influenza/single-dose-influenza-drug-baloxavir-similar-oseltamivir?utm_campaign=NewsBrief&utm_source=hs_email&utm_medium=email&utm_content=65756783&_hsenc=p2ANqtz--z5oGw5Z9jFgKmpxBvoSuFXbFkUTOIAJSs6EJE8ilUi4-w1f2WnkyThcoyk-hatkLutOHuO7p9ZU058y27jc13LSom-w&_hsmi=65757195',
        'by'=>0,
        'journal'=>'Bianca Nogrady',
        'type'=>0,
        'month_article'=>0,
        'famous'=>0,
        'views'=>1101,
        'show'=>0,
        'date'=>'2018-09-04 22:00:00',
        'active'=>'1',
        'user'=>0,
        'created_at'=>'2020-05-15 01:20:06',
        'updated_at'=>'2020-06-21 00:08:23',
        'deleted_at'=>NULL
        ] );
        
        
                    
        Post::create( [
        'id'=>4964,
        'cat_id'=>8,
        'tag_id'=>3,
        'title'=>'Late parenteral nutrition beneficial to undernourished critically ill children',
        'text'=>'ICU Mangement and Practice. Source:  JAMA Network Open',
        'img'=>'file-1463591732-SM39NY.jpg',
        'file'=>NULL,
        'link'=>'https://healthmanagement.org/c/icu/news/late-parenteral-nutrition-beneficial-to-undernourished-critically-ill-children',
        'by'=>0,
        'journal'=>'Site Admin',
        'type'=>0,
        'month_article'=>0,
        'famous'=>0,
        'views'=>1101,
        'show'=>0,
        'date'=>'2018-09-18 22:00:00',
        'active'=>'1',
        'user'=>0,
        'created_at'=>'2020-05-15 01:20:06',
        'updated_at'=>'2020-06-21 00:08:23',
        'deleted_at'=>NULL
        ] );
        
        
                    
        Post::create( [
        'id'=>4965,
        'cat_id'=>8,
        'tag_id'=>3,
        'title'=>'Outcomes of Delaying Parenteral Nutrition for 1 Week vs Initiation Within 24 Hours Among Undernourished Children in Pediatric Intensive Care',
        'text'=>'JAMA Network',
        'img'=>'file-1463591725-9J88SF.jpg',
        'file'=>NULL,
        'link'=>'https://jamanetwork.com/journals/jamanetworkopen/fullarticle/2702217',
        'by'=>0,
        'journal'=>'Esther van Puffelen and others',
        'type'=>0,
        'month_article'=>0,
        'famous'=>0,
        'views'=>1101,
        'show'=>0,
        'date'=>'2018-09-18 22:00:00',
        'active'=>'1',
        'user'=>0,
        'created_at'=>'2020-05-15 01:20:06',
        'updated_at'=>'2020-06-21 00:08:23',
        'deleted_at'=>NULL
        ] );
        
        
                    
        Post::create( [
        'id'=>4966,
        'cat_id'=>12,
        'tag_id'=>6,
        'title'=>'Endotracheal tube-mounted camera-assisted intubation versus conventional intubation in intensive care: a prospective, randomised trial VivaITN',
        'text'=>'Critical Care 23 Sept. 201822:235',
        'img'=>'im6.jpg',
        'file'=>NULL,
        'link'=>'https://ccforum.biomedcentral.com/articles/10.1186/s13054-018-2152-4',
        'by'=>0,
        'journal'=>'Jörn Grensemann and others',
        'type'=>0,
        'month_article'=>0,
        'famous'=>0,
        'views'=>1212,
        'show'=>0,
        'date'=>'2018-09-22 22:00:00',
        'active'=>'1',
        'user'=>0,
        'created_at'=>'2020-05-15 01:20:06',
        'updated_at'=>'2020-06-21 00:08:23',
        'deleted_at'=>NULL
        ] );
        
        
                    
        Post::create( [
        'id'=>4967,
        'cat_id'=>8,
        'tag_id'=>5,
        'title'=>'Categorization of post-cardiac arrest patients according to the pattern of amplitude-integrated electroencephalography after return of spontaneous circulation',
        'text'=>'Critical Care 2018, 22:226  Published on: 20 September 2018',
        'img'=>'im2.jpg',
        'file'=>NULL,
        'link'=>'https://ccforum.biomedcentral.com/articles/10.1186/s13054-018-2138-2',
        'by'=>0,
        'journal'=>'Kazuhiro Sugiyama, Kazuki Miyazaki, Takuto Ishida, Takahiro Tanabe and Yuichi Hamabe',
        'type'=>0,
        'month_article'=>0,
        'famous'=>0,
        'views'=>1114,
        'show'=>0,
        'date'=>'2018-09-19 22:00:00',
        'active'=>'1',
        'user'=>0,
        'created_at'=>'2020-05-15 01:20:06',
        'updated_at'=>'2020-06-21 00:08:23',
        'deleted_at'=>NULL
        ] );
        
        
                    
        Post::create( [
        'id'=>4968,
        'cat_id'=>8,
        'tag_id'=>11,
        'title'=>'FOCUSED ACUTE MEDICINE ULTRASOUND FAMUS',
        'text'=>'SAM, Volume 17 Issue 3 Pages 164-167 2018',
        'img'=>'file-1464165463-D3CBM9.jpg',
        'file'=>NULL,
        'link'=>'https://acutemedjournal.co.uk/special-article/focused-acute-medicine-ultrasound-famus/',
        'by'=>0,
        'journal'=>'KF Alber, and others',
        'type'=>0,
        'month_article'=>0,
        'famous'=>0,
        'views'=>1101,
        'show'=>0,
        'date'=>'2018-09-23 22:00:00',
        'active'=>'1',
        'user'=>0,
        'created_at'=>'2020-05-15 01:20:06',
        'updated_at'=>'2020-06-21 00:08:23',
        'deleted_at'=>NULL
        ] );
        
        
                    
        Post::create( [
        'id'=>4969,
        'cat_id'=>8,
        'tag_id'=>11,
        'title'=>'Risk factors and outcomes for airway failure versus non-airway failure in the intensive care unit: a multicenter observational study of 1514 extubation procedures',
        'text'=>'Critical Care 2018 22:236. Published: 23 September 2018',
        'img'=>'im5.jpg',
        'file'=>NULL,
        'link'=>'https://ccforum.biomedcentral.com/articles/10.1186/s13054-018-2150-6',
        'by'=>0,
        'journal'=>'Samir Jaber and others',
        'type'=>0,
        'month_article'=>0,
        'famous'=>0,
        'views'=>1101,
        'show'=>0,
        'date'=>'2018-09-23 22:00:00',
        'active'=>'1',
        'user'=>0,
        'created_at'=>'2020-05-15 01:20:06',
        'updated_at'=>'2020-06-21 00:08:23',
        'deleted_at'=>NULL
        ] );
        
        
                    
        Post::create( [
        'id'=>4970,
        'cat_id'=>8,
        'tag_id'=>3,
        'title'=>'The role of bile acids in nutritional support',
        'text'=>'Critical Care 2018, 22:231  Published on: 30 September 2018',
        'img'=>'file-1463591725-9J88SF.jpg',
        'file'=>NULL,
        'link'=>'https://ccforum.biomedcentral.com/articles/10.1186/s13054-018-2160-4',
        'by'=>0,
        'journal'=>'Gustav van Niekerk, Tanja Davis, Willem de Villiers and Anna-Mart Engelbrecht',
        'type'=>0,
        'month_article'=>0,
        'famous'=>0,
        'views'=>1101,
        'show'=>0,
        'date'=>'2018-09-30 22:00:00',
        'active'=>'1',
        'user'=>0,
        'created_at'=>'2020-05-15 01:20:06',
        'updated_at'=>'2020-06-21 00:08:23',
        'deleted_at'=>NULL
        ] );
        
        
                    
        Post::create( [
        'id'=>4971,
        'cat_id'=>8,
        'tag_id'=>2,
        'title'=>'Hypoactive Delirium Is More Appropriately Named as “Acute Apathy Syndrome”',
        'text'=>'Critical Care Medicine. 4610:1561-1562, October 2018',
        'img'=>'im1.jpg',
        'file'=>NULL,
        'link'=>'https://journals.lww.com/ccmjournal/Fulltext/2018/10000/Hypoactive_Delirium_Is_More_Appropriately_Named_as.1.aspx',
        'by'=>0,
        'journal'=>'Schieveld, Jan N. M. Strik, Jacqueline J. M. H.',
        'type'=>0,
        'month_article'=>0,
        'famous'=>0,
        'views'=>1172,
        'show'=>0,
        'date'=>'2018-09-30 22:00:00',
        'active'=>'1',
        'user'=>0,
        'created_at'=>'2020-05-15 01:20:06',
        'updated_at'=>'2020-06-21 00:08:23',
        'deleted_at'=>NULL
        ] );
        
        
                    
        Post::create( [
        'id'=>4972,
        'cat_id'=>8,
        'tag_id'=>7,
        'title'=>'Deresuscitation of Patients With Iatrogenic Fluid Overload Is Associated With Reduced Mortality in Critical Illness',
        'text'=>'Critical Care Medicine. 4610:1600-1607, October 2018',
        'img'=>'file-1463591644-ZGN2DA.jpg',
        'file'=>NULL,
        'link'=>'https://journals.lww.com/ccmjournal/Abstract/2018/10000/Deresuscitation_of_Patients_With_Iatrogenic_Fluid.7.aspx',
        'by'=>0,
        'journal'=>'Silversides, Jonathan A and others',
        'type'=>0,
        'month_article'=>0,
        'famous'=>0,
        'views'=>1101,
        'show'=>0,
        'date'=>'2018-09-30 22:00:00',
        'active'=>'1',
        'user'=>0,
        'created_at'=>'2020-05-15 01:20:06',
        'updated_at'=>'2020-06-21 00:08:23',
        'deleted_at'=>NULL
        ] );
        
        
                    
        Post::create( [
        'id'=>4973,
        'cat_id'=>8,
        'tag_id'=>6,
        'title'=>'Moderate and Severe Acute Respiratory Distress Syndrome: Hemodynamic and Cardiac Effects of an Open Lung Strategy With Recruitment Maneuver Analyzed Using Echocardiography',
        'text'=>'Critical Care Medicine. 4610:1608-1616, October 2018.',
        'img'=>'im6.jpg',
        'file'=>NULL,
        'link'=>'https://journals.lww.com/ccmjournal/Abstract/2018/10000/Moderate_and_Severe_Acute_Respiratory_Distress.8.aspx',
        'by'=>0,
        'journal'=>'ercado, Pablo and others',
        'type'=>0,
        'month_article'=>0,
        'famous'=>0,
        'views'=>1101,
        'show'=>0,
        'date'=>'2018-09-30 22:00:00',
        'active'=>'1',
        'user'=>0,
        'created_at'=>'2020-05-15 01:20:06',
        'updated_at'=>'2020-06-21 00:08:23',
        'deleted_at'=>NULL
        ] );
        
        
                    
        Post::create( [
        'id'=>4974,
        'cat_id'=>12,
        'tag_id'=>6,
        'title'=>'Half-Dose Versus Full-Dose Alteplase for Treatment of Pulmonary Embolism',
        'text'=>'Critical Care Medicine. 4610:1617-1625, October 2018',
        'img'=>'file-1463591639-4YG8JY.jpg',
        'file'=>NULL,
        'link'=>'https://journals.lww.com/ccmjournal/Abstract/2018/10000/Half_Dose_Versus_Full_Dose_Alteplase_for_Treatment.9.aspx',
        'by'=>0,
        'journal'=>'Kiser, Tyree H and others',
        'type'=>0,
        'month_article'=>0,
        'famous'=>0,
        'views'=>1212,
        'show'=>0,
        'date'=>'2018-09-30 22:00:00',
        'active'=>'1',
        'user'=>0,
        'created_at'=>'2020-05-15 01:20:06',
        'updated_at'=>'2020-06-21 00:08:23',
        'deleted_at'=>NULL
        ] );
        
        
                    
        Post::create( [
        'id'=>4975,
        'cat_id'=>12,
        'tag_id'=>1,
        'title'=>'Survival of Patients With Vancomycin-Resistant Enterococcus faecium Bacteremia Treated With Conventional or High Doses of Daptomycin or Linezolid Is Associated With the Rate of Bacterial Clearance',
        'text'=>'Critical Care Medicine. 4610:1634-1642, October 2018',
        'img'=>'file-1463591657-K8BRDF.jpg',
        'file'=>NULL,
        'link'=>'https://journals.lww.com/ccmjournal/Abstract/2018/10000/Survival_of_Patients_With_Vancomycin_Resistant.11.aspx',
        'by'=>0,
        'journal'=>'Chuang, Yu-Chung and others',
        'type'=>0,
        'month_article'=>0,
        'famous'=>0,
        'views'=>1241,
        'show'=>0,
        'date'=>'2018-09-30 22:00:00',
        'active'=>'1',
        'user'=>0,
        'created_at'=>'2020-05-15 01:20:06',
        'updated_at'=>'2020-06-21 00:08:23',
        'deleted_at'=>NULL
        ] );
        
        
                    
        Post::create( [
        'id'=>4976,
        'cat_id'=>8,
        'tag_id'=>5,
        'title'=>'Hypertonic Lactate to Improve Cerebral Perfusion and Glucose Availability After Acute Brain Injury',
        'text'=>'Critical Care Medicine. 4610:1649-1655, October 2018',
        'img'=>'im2.jpg',
        'file'=>NULL,
        'link'=>'https://journals.lww.com/ccmjournal/Abstract/2018/10000/Hypertonic_Lactate_to_Improve_Cerebral_Perfusion.13.aspx',
        'by'=>0,
        'journal'=>'Carteron, Laurent and others',
        'type'=>0,
        'month_article'=>0,
        'famous'=>0,
        'views'=>1114,
        'show'=>0,
        'date'=>'2018-09-30 22:00:00',
        'active'=>'1',
        'user'=>0,
        'created_at'=>'2020-05-15 01:20:06',
        'updated_at'=>'2020-06-21 00:08:23',
        'deleted_at'=>NULL
        ] );
        
        
                    
        Post::create( [
        'id'=>4977,
        'cat_id'=>8,
        'tag_id'=>3,
        'title'=>'Evaluation of Effect of Probiotics on Cytokine Levels in Critically Ill Children With Severe Sepsis: A Double-Blind, Placebo-Controlled Trial',
        'text'=>'Critical Care Medicine. 4610:1656-1664, October 2018',
        'img'=>'file-1463591732-SM39NY.jpg',
        'file'=>NULL,
        'link'=>'https://journals.lww.com/ccmjournal/Abstract/2018/10000/Evaluation_of_Effect_of_Probiotics_on_Cytokine.14.aspx',
        'by'=>0,
        'journal'=>'Angurana, Suresh K and others',
        'type'=>0,
        'month_article'=>0,
        'famous'=>0,
        'views'=>1101,
        'show'=>0,
        'date'=>'2018-09-30 22:00:00',
        'active'=>'1',
        'user'=>0,
        'created_at'=>'2020-05-15 01:20:06',
        'updated_at'=>'2020-06-21 00:08:23',
        'deleted_at'=>NULL
        ] );
        
        
                    
        Post::create( [
        'id'=>4978,
        'cat_id'=>8,
        'tag_id'=>1,
        'title'=>'Daily bathing with 4% CHGwr helps prevent infection in ICU',
        'text'=>'Source: Clinical Microbiology and Infection',
        'img'=>'file-1464106363-HWQX6B.jpg',
        'file'=>NULL,
        'link'=>'https://healthmanagement.org/c/icu/news/daily-bathing-with-4-chgwr-helps-prevent-infection-in-icu',
        'by'=>0,
        'journal'=>'ICU Management and Practice, October 2018',
        'type'=>0,
        'month_article'=>0,
        'famous'=>0,
        'views'=>1130,
        'show'=>0,
        'date'=>'2018-10-04 22:00:00',
        'active'=>'1',
        'user'=>0,
        'created_at'=>'2020-05-15 01:20:06',
        'updated_at'=>'2020-06-21 00:08:23',
        'deleted_at'=>NULL
        ] );
        
        
                    
        Post::create( [
        'id'=>4979,
        'cat_id'=>8,
        'tag_id'=>1,
        'title'=>'Daily Bathing With 4% Chlorhexidine Gluconate In Intensive Care Settings: A Randomized Controlled Trial',
        'text'=>'Clinical Microbiology and Infection',
        'img'=>'file-1464165463-D3CBM9.jpg',
        'file'=>NULL,
        'link'=>'https://www.clinicalmicrobiologyandinfection.com/article/S1198-743X1830634-7/pdf',
        'by'=>0,
        'journal'=>'Pallotto C et al',
        'type'=>0,
        'month_article'=>0,
        'famous'=>0,
        'views'=>1130,
        'show'=>0,
        'date'=>'2018-09-30 22:00:00',
        'active'=>'1',
        'user'=>0,
        'created_at'=>'2020-05-15 01:20:06',
        'updated_at'=>'2020-06-21 00:08:23',
        'deleted_at'=>NULL
        ] );
        
        
                    
        Post::create( [
        'id'=>4980,
        'cat_id'=>8,
        'tag_id'=>1,
        'title'=>'Comparison between procalcitonin and C-reactive protein to predict blood culture results in ICU patients',
        'text'=>'Critical Care 2018, 22:252  Published on: 5 October 2018',
        'img'=>'file-1464106363-HWQX6B.jpg',
        'file'=>NULL,
        'link'=>'https://ccforum.biomedcentral.com/articles/10.1186/s13054-018-2183-x',
        'by'=>0,
        'journal'=>'Matteo Bassetti and others',
        'type'=>0,
        'month_article'=>0,
        'famous'=>0,
        'views'=>1130,
        'show'=>0,
        'date'=>'2018-10-05 22:00:00',
        'active'=>'1',
        'user'=>0,
        'created_at'=>'2020-05-15 01:20:06',
        'updated_at'=>'2020-06-21 00:08:23',
        'deleted_at'=>NULL
        ] );
        
        
                    
        Post::create( [
        'id'=>4981,
        'cat_id'=>8,
        'tag_id'=>11,
        'title'=>'Noise in the intensive care unit and its influence on sleep quality: a multicenter observational study in Dutch intensive care units',
        'text'=>'Critical Care 2018, 22:250  Published on: 5 October 2018',
        'img'=>'file-1464165480-R231XR.jpg',
        'file'=>NULL,
        'link'=>'https://ccforum.biomedcentral.com/articles/10.1186/s13054-018-2182-y',
        'by'=>0,
        'journal'=>'Koen S. Simons and others',
        'type'=>0,
        'month_article'=>0,
        'famous'=>0,
        'views'=>1101,
        'show'=>0,
        'date'=>'2018-10-04 22:00:00',
        'active'=>'1',
        'user'=>0,
        'created_at'=>'2020-05-15 01:20:06',
        'updated_at'=>'2020-06-21 00:08:23',
        'deleted_at'=>NULL
        ] );
        
        
                    
        Post::create( [
        'id'=>4982,
        'cat_id'=>8,
        'tag_id'=>1,
        'title'=>'Reusable temperature probes linked to ICU C. auris outbreak',
        'text'=>'',
        'img'=>'file-1464165509-QNPJY8.jpg',
        'file'=>NULL,
        'link'=>'https://www.medpagetoday.com/hospitalbasedmedicine/infectioncontrol/75469',
        'by'=>0,
        'journal'=>'Medpage today',
        'type'=>0,
        'month_article'=>0,
        'famous'=>0,
        'views'=>1130,
        'show'=>0,
        'date'=>'2018-10-06 22:00:00',
        'active'=>'1',
        'user'=>0,
        'created_at'=>'2020-05-15 01:20:06',
        'updated_at'=>'2020-06-21 00:08:23',
        'deleted_at'=>NULL
        ] );
        
        
                    
        Post::create( [
        'id'=>4983,
        'cat_id'=>8,
        'tag_id'=>1,
        'title'=>'A Candida auris Outbreak and Its Control in an Intensive Care Setting',
        'text'=>'N Engl J Med 2018379:1322-1331',
        'img'=>'im5.jpg',
        'file'=>NULL,
        'link'=>'https://www.nejm.org/doi/full/10.1056/NEJMoa1714373?query=TOC',
        'by'=>0,
        'journal'=>'D.W. Eyre and Others',
        'type'=>0,
        'month_article'=>0,
        'famous'=>0,
        'views'=>1130,
        'show'=>0,
        'date'=>'2018-10-03 22:00:00',
        'active'=>'1',
        'user'=>0,
        'created_at'=>'2020-05-15 01:20:06',
        'updated_at'=>'2020-06-21 00:08:23',
        'deleted_at'=>NULL
        ] );
        
        
                    
        Post::create( [
        'id'=>4984,
        'cat_id'=>8,
        'tag_id'=>10,
        'title'=>'Rivaroxaban in Patients with Heart Failure, Sinus Rhythm, and Coronary Disease',
        'text'=>'N Engl J Med 2018379:1332-1342',
        'img'=>'file-1463591681-AGRM7H.jpg',
        'file'=>NULL,
        'link'=>'https://www.nejm.org/doi/full/10.1056/NEJMoa1808848?query=TOC',
        'by'=>0,
        'journal'=>'F. Zannad and Others, for the COMMANDER HF Investigators',
        'type'=>0,
        'month_article'=>0,
        'famous'=>0,
        'views'=>1101,
        'show'=>0,
        'date'=>'2018-10-04 22:00:00',
        'active'=>'1',
        'user'=>0,
        'created_at'=>'2020-05-15 01:20:06',
        'updated_at'=>'2020-06-21 00:08:23',
        'deleted_at'=>NULL
        ] );
        
        
                    
        Post::create( [
        'id'=>4985,
        'cat_id'=>8,
        'tag_id'=>6,
        'title'=>'Bougie-in-channel intubation technique',
        'text'=>'Critical Care 2018, 22:253  Published on: 6 October 2018',
        'img'=>'file-1463591639-4YG8JY.jpg',
        'file'=>NULL,
        'link'=>'https://ccforum.biomedcentral.com/articles/10.1186/s13054-018-2184-9',
        'by'=>0,
        'journal'=>'Kay Choong See, Melanie Estaras, Rolando Capistrano, Sui Hua Wong, Juliet Sahagun and Juvel Taculod',
        'type'=>0,
        'month_article'=>0,
        'famous'=>0,
        'views'=>1101,
        'show'=>0,
        'date'=>'2018-09-30 22:00:00',
        'active'=>'1',
        'user'=>0,
        'created_at'=>'2020-05-15 01:20:06',
        'updated_at'=>'2020-06-21 00:08:23',
        'deleted_at'=>NULL
        ] );
        
        
                    
        Post::create( [
        'id'=>4986,
        'cat_id'=>8,
        'tag_id'=>1,
        'title'=>'Evidence is stronger than you think: a meta-analysis of vitamin C use in patients with sepsis',
        'text'=>'Critical Care 201822:258',
        'img'=>'file-1463591639-4YG8JY.jpg',
        'file'=>NULL,
        'link'=>'https://ccforum.biomedcentral.com/articles/10.1186/s13054-018-2191-x',
        'by'=>0,
        'journal'=>'Jing Li',
        'type'=>0,
        'month_article'=>0,
        'famous'=>0,
        'views'=>1130,
        'show'=>0,
        'date'=>'2018-10-12 22:00:00',
        'active'=>'1',
        'user'=>0,
        'created_at'=>'2020-05-15 01:20:06',
        'updated_at'=>'2020-06-21 00:08:23',
        'deleted_at'=>NULL
        ] );
        
        
                    
        Post::create( [
        'id'=>4987,
        'cat_id'=>8,
        'tag_id'=>8,
        'title'=>'Timing of Renal-Replacement Therapy in Patients with Acute Kidney Injury and Sepsis',
        'text'=>'N Engl J Med 2018379:1431-1442',
        'img'=>'file-1463591738-IZ3XFW.jpg',
        'file'=>NULL,
        'link'=>'https://www.nejm.org/doi/full/10.1056/NEJMoa1803213queryTOC',
        'by'=>0,
        'journal'=>'S.D. Barbar and Others. for the IDEAL-ICU Trial Investigators and the CRICS TRIGGERSEP Network',
        'type'=>0,
        'month_article'=>0,
        'famous'=>0,
        'views'=>1152,
        'show'=>0,
        'date'=>'2018-10-12 22:00:00',
        'active'=>'1',
        'user'=>0,
        'created_at'=>'2020-05-15 01:20:06',
        'updated_at'=>'2020-06-21 00:08:23',
        'deleted_at'=>NULL
        ] );
        
        
                    
        Post::create( [
        'id'=>4988,
        'cat_id'=>8,
        'tag_id'=>11,
        'title'=>'Quality of life after discharge from an ICU',
        'text'=>'Source: Anaesthesia',
        'img'=>'file-1464165480-R231XR.jpg',
        'file'=>NULL,
        'link'=>'https://healthmanagement.org/c/icu/news/quality-of-life-after-discharge-from-an-icu',
        'by'=>0,
        'journal'=>'ICU Management and Practice',
        'type'=>0,
        'month_article'=>0,
        'famous'=>0,
        'views'=>1101,
        'show'=>0,
        'date'=>'2018-10-12 22:00:00',
        'active'=>'1',
        'user'=>0,
        'created_at'=>'2020-05-15 01:20:06',
        'updated_at'=>'2020-06-21 00:08:23',
        'deleted_at'=>NULL
        ] );
        
        
                    
        Post::create( [
        'id'=>4989,
        'cat_id'=>8,
        'tag_id'=>11,
        'title'=>'Quality of life after discharge from an ICU',
        'text'=>'Source: Anaesthesia',
        'img'=>'',
        'file'=>NULL,
        'link'=>'https://healthmanagement.org/c/icu/news/quality-of-life-after-discharge-from-an-icu',
        'by'=>0,
        'journal'=>'ICU Management and Practice',
        'type'=>0,
        'month_article'=>0,
        'famous'=>0,
        'views'=>1101,
        'show'=>0,
        'date'=>'2018-09-30 22:00:00',
        'active'=>'1',
        'user'=>0,
        'created_at'=>'2020-05-15 01:20:06',
        'updated_at'=>'2020-06-21 00:08:23',
        'deleted_at'=>NULL
        ] );
        
        
                    
        Post::create( [
        'id'=>4990,
        'cat_id'=>8,
        'tag_id'=>1,
        'title'=>'Multidrug-Resistant Aspergillus fumigatus on the Rise',
        'text'=>'Infectious Disease Special Edition. October 17, 2018',
        'img'=>'file-1464106363-HWQX6B.jpg',
        'file'=>NULL,
        'link'=>'https://www.idse.net/Fungal-Infection/Article/10-18/Multidrug-Resistant-Aspergillus-fumigatus-on-the-Rise/53108?sub=28EDA9DC3752551BBE5674CAE4658C986AF62C6EC565A4B265B8A6B2A7D01F&enl=true',
        'by'=>0,
        'journal'=>'Ethan Covey',
        'type'=>0,
        'month_article'=>0,
        'famous'=>0,
        'views'=>1130,
        'show'=>0,
        'date'=>'2018-10-16 22:00:00',
        'active'=>'1',
        'user'=>0,
        'created_at'=>'2020-05-15 01:20:06',
        'updated_at'=>'2020-06-21 00:08:23',
        'deleted_at'=>NULL
        ] );
        
        
                    
        Post::create( [
        'id'=>4991,
        'cat_id'=>8,
        'tag_id'=>7,
        'title'=>'Fluids in Shock from physiology to bedside',
        'text'=>'',
        'img'=>'file-1463591644-ZGN2DA.jpg',
        'file'=>NULL,
        'link'=>'https://healthmanagement.org/c/icu/issuearticle/fluids-in-shock',
        'by'=>0,
        'journal'=>'ICU Management & Practice, Volume 18 - Issue 3, 2018',
        'type'=>0,
        'month_article'=>0,
        'famous'=>0,
        'views'=>1101,
        'show'=>0,
        'date'=>'2018-10-21 22:00:00',
        'active'=>'1',
        'user'=>0,
        'created_at'=>'2020-05-15 01:20:06',
        'updated_at'=>'2020-06-21 00:08:23',
        'deleted_at'=>NULL
        ] );
        
        
                    
        Post::create( [
        'id'=>4992,
        'cat_id'=>12,
        'tag_id'=>3,
        'title'=>'Energy-Dense versus Routine Enteral Nutrition in the Critically Ill',
        'text'=>'N Engl J Med. October 22, 2018',
        'img'=>'file-1463591725-9J88SF.jpg',
        'file'=>NULL,
        'link'=>'https://www.nejm.org/doi/full/10.1056/NEJMoa1811687?query=TOC',
        'by'=>0,
        'journal'=>'The TARGET Investigators, for the ANZICS Clinical Trials Group',
        'type'=>0,
        'month_article'=>0,
        'famous'=>0,
        'views'=>1212,
        'show'=>0,
        'date'=>'2018-10-21 22:00:00',
        'active'=>'1',
        'user'=>0,
        'created_at'=>'2020-05-15 01:20:06',
        'updated_at'=>'2020-06-21 00:08:23',
        'deleted_at'=>NULL
        ] );
        
        
                    
        Post::create( [
        'id'=>4993,
        'cat_id'=>8,
        'tag_id'=>2,
        'title'=>'Haloperidol and Ziprasidone for Treatment of Delirium in Critical Illness',
        'text'=>'N Eng J Med. October 22, 2018',
        'img'=>'im5.jpg',
        'file'=>NULL,
        'link'=>'https://www.nejm.org/doi/full/10.1056/NEJMoa1808217?query=TOC',
        'by'=>0,
        'journal'=>'T.D. Girard and Others. for the MIND-USA Investigators',
        'type'=>0,
        'month_article'=>0,
        'famous'=>0,
        'views'=>1172,
        'show'=>0,
        'date'=>'2018-10-24 22:00:00',
        'active'=>'1',
        'user'=>0,
        'created_at'=>'2020-05-15 01:20:06',
        'updated_at'=>'2020-06-21 00:08:23',
        'deleted_at'=>NULL
        ] );
        
        
                    
        Post::create( [
        'id'=>4994,
        'cat_id'=>8,
        'tag_id'=>9,
        'title'=>'Pantoprazole in Patients at Risk for Gastrointestinal Bleeding in the ICU',
        'text'=>'N Eng J Med. October 24, 2018',
        'img'=>'file-1463591650-J14MMU.jpg',
        'file'=>NULL,
        'link'=>'https://www.nejm.org/doi/full/10.1056/NEJMoa1714919?query=TOC',
        'by'=>0,
        'journal'=>'M. Krag and Others. for the SUP-ICU trial group',
        'type'=>0,
        'month_article'=>0,
        'famous'=>0,
        'views'=>1101,
        'show'=>0,
        'date'=>'2018-10-24 22:00:00',
        'active'=>'1',
        'user'=>0,
        'created_at'=>'2020-05-15 01:20:06',
        'updated_at'=>'2020-06-21 00:08:23',
        'deleted_at'=>NULL
        ] );
        
        
                    
        Post::create( [
        'id'=>4995,
        'cat_id'=>8,
        'tag_id'=>1,
        'title'=>'Nurse blogs about Project HAPPEN, designed to reduce HAP risk',
        'text'=>'MedPage Today',
        'img'=>'file-1464165480-R231XR.jpg',
        'file'=>NULL,
        'link'=>'https://www.medpagetoday.com/nursing/nursing/75682',
        'by'=>0,
        'journal'=>'Beth Abele, RN, MSN, CNL',
        'type'=>0,
        'month_article'=>0,
        'famous'=>0,
        'views'=>1130,
        'show'=>0,
        'date'=>'2018-10-26 22:00:00',
        'active'=>'1',
        'user'=>0,
        'created_at'=>'2020-05-15 01:20:06',
        'updated_at'=>'2020-06-21 00:08:23',
        'deleted_at'=>NULL
        ] );
        
        
                    
        Post::create( [
        'id'=>4996,
        'cat_id'=>8,
        'tag_id'=>1,
        'title'=>'Nurse blogs about Project HAPPEN, designed to reduce HAP risk',
        'text'=>'MedPage Today',
        'img'=>'',
        'file'=>NULL,
        'link'=>'https://www.medpagetoday.com/nursing/nursing/75682',
        'by'=>0,
        'journal'=>'Beth Abele, RN, MSN, CNL',
        'type'=>0,
        'month_article'=>0,
        'famous'=>0,
        'views'=>1130,
        'show'=>0,
        'date'=>'2018-10-26 22:00:00',
        'active'=>'1',
        'user'=>0,
        'created_at'=>'2020-05-15 01:20:06',
        'updated_at'=>'2020-06-21 00:08:23',
        'deleted_at'=>NULL
        ] );
        
        
                    
        Post::create( [
        'id'=>4997,
        'cat_id'=>8,
        'tag_id'=>11,
        'title'=>'Extracorporeal techniques for the treatment of critically ill patients with sepsis beyond conventional blood purification therapy: the promises and the pitfalls',
        'text'=>'Critical Care 2018, 22:262  Published on: 25 October 2018',
        'img'=>'file-1463591644-ZGN2DA.jpg',
        'file'=>NULL,
        'link'=>'https://ccforum.biomedcentral.com/articles/10.1186/s13054-018-2181-z',
        'by'=>0,
        'journal'=>'Ghada Ankawi and others',
        'type'=>0,
        'month_article'=>0,
        'famous'=>0,
        'views'=>1101,
        'show'=>0,
        'date'=>'2018-10-26 22:00:00',
        'active'=>'1',
        'user'=>0,
        'created_at'=>'2020-05-15 01:20:06',
        'updated_at'=>'2020-06-21 00:08:23',
        'deleted_at'=>NULL
        ] );
        
        
                    
        Post::create( [
        'id'=>4998,
        'cat_id'=>8,
        'tag_id'=>6,
        'title'=>'Volutrauma and atelectrauma: which is worse',
        'text'=>'Critical Care 2018, 22:264  Published on: 25 October 2018',
        'img'=>'file-1463591752-XK911T.jpg',
        'file'=>NULL,
        'link'=>'https://ccforum.biomedcentral.com/articles/10.1186/s13054-018-2199-2',
        'by'=>0,
        'journal'=>'Luciano Gattinoni, Michael Quintel and John J. Marini',
        'type'=>0,
        'month_article'=>0,
        'famous'=>0,
        'views'=>1101,
        'show'=>0,
        'date'=>'2018-10-26 22:00:00',
        'active'=>'1',
        'user'=>0,
        'created_at'=>'2020-05-15 01:20:06',
        'updated_at'=>'2020-06-21 00:08:23',
        'deleted_at'=>NULL
        ] );
        
        
                    
        Post::create( [
        'id'=>4999,
        'cat_id'=>8,
        'tag_id'=>3,
        'title'=>'High protein intake during the early phase of critical illness: yes or no',
        'text'=>'Critical Care 2018, 22:261  Published on: 25 October 2018',
        'img'=>'file-1463591732-SM39NY.jpg',
        'file'=>NULL,
        'link'=>'https://ccforum.biomedcentral.com/articles/10.1186/s13054-018-2196-5',
        'by'=>0,
        'journal'=>'Jean-Charles Preiser',
        'type'=>0,
        'month_article'=>0,
        'famous'=>0,
        'views'=>1101,
        'show'=>0,
        'date'=>'2018-10-26 22:00:00',
        'active'=>'1',
        'user'=>0,
        'created_at'=>'2020-05-15 01:20:06',
        'updated_at'=>'2020-06-21 00:08:23',
        'deleted_at'=>NULL
        ] );
        
        
                    
        Post::create( [
        'id'=>5000,
        'cat_id'=>8,
        'tag_id'=>3,
        'title'=>'Advanced Critical Care Nutrition Course',
        'text'=>'the course is very unique, as it is 4th time to be done in the whole world and the second time in Bahrain. It provides full comprehensive review from the basic to the very advanced level including all aspects of critical care nutrition in adults, pediatrics and neonates through answering all questions related to different categories of critically ill patients e.g. medical, surgical, cardiac, renal, oncology Both Enteral and Parenteral.\r\n\r\nThe course is composed of an updated textbook, 19 live lectures, 12 online lectures and 14 skill stations and workshops and mobile application..\r\n14 National and International experts are going to teach the course.\r\n\r\nBy the end of the course, the attendees will be able to prescribe easily and safely both enteral and parenteral nutrition and they can deal with all nutrition related complications.',
        'img'=>'file-1463591725-9J88SF.jpg',
        'file'=>NULL,
        'link'=>NULL,
        'by'=>0,
        'journal'=>'Wyndham Hotel in Bahrain',
        'type'=>0,
        'month_article'=>0,
        'famous'=>0,
        'views'=>89,
        'show'=>0,
        'date'=>'2018-10-31 22:00:00',
        'active'=>'1',
        'user'=>0,
        'created_at'=>'2020-05-15 01:20:06',
        'updated_at'=>'2020-06-21 00:08:23',
        'deleted_at'=>NULL
        ] );
        
        
                    
        Post::create( [
        'id'=>5001,
        'cat_id'=>8,
        'tag_id'=>1,
        'title'=>'Effect of Thiamine Administration on Lactate Clearance and Mortality in Patients With Septic Shock',
        'text'=>'Critical Care Medicine. 4611:1747-1752, November 2018',
        'img'=>'file-1464165463-D3CBM9.jpg',
        'file'=>NULL,
        'link'=>'https://journals.lww.com/ccmjournal/Abstract/2018/11000/Effect_of_Thiamine_Administration_on_Lactate.5.aspx',
        'by'=>0,
        'journal'=>'Woolum, Jordan A and others',
        'type'=>0,
        'month_article'=>0,
        'famous'=>0,
        'views'=>147,
        'show'=>0,
        'date'=>'2018-10-31 22:00:00',
        'active'=>'1',
        'user'=>0,
        'created_at'=>'2020-05-15 01:20:06',
        'updated_at'=>'2020-06-21 00:08:23',
        'deleted_at'=>NULL
        ] );
        
        
                    
        Post::create( [
        'id'=>5002,
        'cat_id'=>8,
        'tag_id'=>6,
        'title'=>'The Association Between Inhaled Nitric Oxide Treatment and ICU Mortality and 28-Day Ventilator-Free Days in Pediatric Acute Respiratory Distress Syndrome',
        'text'=>'Critical Care Medicine. 4611:1803-1810, November 2018',
        'img'=>'file-1464165509-QNPJY8.jpg',
        'file'=>NULL,
        'link'=>'https://journals.lww.com/ccmjournal/Abstract/2018/11000/The_Association_Between_Inhaled_Nitric_Oxide.12.aspx',
        'by'=>0,
        'journal'=>'Bhalla, Anoopindar K and other',
        'type'=>0,
        'month_article'=>0,
        'famous'=>0,
        'views'=>118,
        'show'=>0,
        'date'=>'2018-10-31 22:00:00',
        'active'=>'1',
        'user'=>0,
        'created_at'=>'2020-05-15 01:20:06',
        'updated_at'=>'2020-06-21 00:08:23',
        'deleted_at'=>NULL
        ] );
        
        
                    
        Post::create( [
        'id'=>5003,
        'cat_id'=>8,
        'tag_id'=>6,
        'title'=>'Beyond Low Tidal Volume Ventilation: Treatment Adjuncts for Severe Respiratory Failure in Acute Respiratory Distress Syndrome',
        'text'=>'Critical Care Medicine. 4611:1820-1831, November 2018.',
        'img'=>'file-1463591752-XK911T.jpg',
        'file'=>NULL,
        'link'=>'https://journals.lww.com/ccmjournal/Abstract/2018/11000/Beyond_Low_Tidal_Volume_Ventilation___Treatment.14.aspx',
        'by'=>0,
        'journal'=>'Fielding-Singh, Vikram Matthay, Michael A. Calfee, Carolyn S.',
        'type'=>0,
        'month_article'=>0,
        'famous'=>0,
        'views'=>118,
        'show'=>0,
        'date'=>'2018-10-31 22:00:00',
        'active'=>'1',
        'user'=>0,
        'created_at'=>'2020-05-15 01:20:06',
        'updated_at'=>'2020-06-21 00:08:23',
        'deleted_at'=>NULL
        ] );
        
        
                    
        Post::create( [
        'id'=>5004,
        'cat_id'=>8,
        'tag_id'=>2,
        'title'=>'Delirium Monitoring in Neurocritically Ill Patients: A Systematic Review',
        'text'=>'Critical Care Medicine. 4611:1832-1841, November 2018',
        'img'=>'file-1464165480-R231XR.jpg',
        'file'=>NULL,
        'link'=>'https://journals.lww.com/ccmjournal/Fulltext/2018/11000/Delirium_Monitoring_in_Neurocritically_Ill.15.aspx',
        'by'=>0,
        'journal'=>'Patel, Mayur B and others',
        'type'=>0,
        'month_article'=>0,
        'famous'=>0,
        'views'=>189,
        'show'=>0,
        'date'=>'2018-10-31 22:00:00',
        'active'=>'1',
        'user'=>0,
        'created_at'=>'2020-05-15 01:20:06',
        'updated_at'=>'2020-06-21 00:08:23',
        'deleted_at'=>NULL
        ] );
        
        
                    
        Post::create( [
        'id'=>5005,
        'cat_id'=>8,
        'tag_id'=>7,
        'title'=>'Extracorporeal techniques in sepsis treatment: benefits and risks',
        'text'=>'ICU Management and Practice',
        'img'=>'file-1463591644-ZGN2DA.jpg',
        'file'=>NULL,
        'link'=>'https://healthmanagement.org/c/icu/news/extracorporeal-techniques-in-sepsis-treatment-benefits-and-risks',
        'by'=>0,
        'journal'=>'Source: Critical Care. Ankawi G et al. 2018',
        'type'=>0,
        'month_article'=>0,
        'famous'=>0,
        'views'=>118,
        'show'=>0,
        'date'=>'2018-10-31 22:00:00',
        'active'=>'1',
        'user'=>0,
        'created_at'=>'2020-05-15 01:20:06',
        'updated_at'=>'2020-06-21 00:08:23',
        'deleted_at'=>NULL
        ] );
        
        
                    
        Post::create( [
        'id'=>5006,
        'cat_id'=>9,
        'tag_id'=>6,
        'title'=>'An Official American Thoracic Society/European Society of Intensive Care Medicine/Society of Critical Care Medicine Clinical Practice Guideline: Mechanical Ventilation in Adult Patients with Acute Respiratory Distress Syndrome',
        'text'=>'Am J Respir Crit Care Med. 2017 May 11959:1253-1263',
        'img'=>'file-1463591752-XK911T.jpg',
        'file'=>NULL,
        'link'=>'https://www.atsjournals.org/doi/full/10.1164/rccm.201703-0548ST',
        'by'=>0,
        'journal'=>'Fan E, Del Sorbo L and others',
        'type'=>0,
        'month_article'=>0,
        'famous'=>0,
        'views'=>118,
        'show'=>0,
        'date'=>'2017-05-04 22:00:00',
        'active'=>'1',
        'user'=>0,
        'created_at'=>'2020-05-15 01:20:06',
        'updated_at'=>'2020-06-21 00:08:23',
        'deleted_at'=>NULL
        ] );
        
        
                    
        Post::create( [
        'id'=>5007,
        'cat_id'=>8,
        'tag_id'=>1,
        'title'=>'Stethoscopes carry broad range of bacteria, even after cleaning',
        'text'=>'Clinical Leadership & Infection Control. December 13, 2018',
        'img'=>'file-1464165509-QNPJY8.jpg',
        'file'=>NULL,
        'link'=>'https://www.beckershospitalreview.com/quality/stethoscopes-carry-broad-range-of-bacteria-even-after-cleaning.html',
        'by'=>0,
        'journal'=>'Mackenzie Bean',
        'type'=>0,
        'month_article'=>0,
        'famous'=>0,
        'views'=>147,
        'show'=>0,
        'date'=>'2018-12-17 22:00:00',
        'active'=>'1',
        'user'=>0,
        'created_at'=>'2020-05-15 01:20:06',
        'updated_at'=>'2020-06-21 00:08:23',
        'deleted_at'=>NULL
        ] );
        
        
                    
        Post::create( [
        'id'=>5008,
        'cat_id'=>8,
        'tag_id'=>11,
        'title'=>'HIT-related stress linked to burnout among physicians',
        'text'=>'Dec 10, 2018',
        'img'=>'file-1464165480-R231XR.jpg',
        'file'=>NULL,
        'link'=>'https://www.physiciansweekly.com/hit-related-stress-linked-to-burnout-among-physicians/',
        'by'=>0,
        'journal'=>'Health Day',
        'type'=>0,
        'month_article'=>0,
        'famous'=>0,
        'views'=>118,
        'show'=>0,
        'date'=>'2018-12-09 22:00:00',
        'active'=>'1',
        'user'=>0,
        'created_at'=>'2020-05-15 01:20:06',
        'updated_at'=>'2020-06-21 00:08:23',
        'deleted_at'=>NULL
        ] );
        
        
                    
        Post::create( [
        'id'=>5009,
        'cat_id'=>8,
        'tag_id'=>6,
        'title'=>'Driving pressure in non-ARDS patients',
        'text'=>'Dec 13, 2018',
        'img'=>'im6.jpg',
        'file'=>NULL,
        'link'=>'https://www.physiciansweekly.com/driving-pressure-in-non-ards-patients/',
        'by'=>0,
        'journal'=>'Physicians Weekly',
        'type'=>0,
        'month_article'=>0,
        'famous'=>0,
        'views'=>118,
        'show'=>0,
        'date'=>'2018-12-12 22:00:00',
        'active'=>'1',
        'user'=>0,
        'created_at'=>'2020-05-15 01:20:06',
        'updated_at'=>'2020-06-21 00:08:23',
        'deleted_at'=>NULL
        ] );
        
        
                    
        Post::create( [
        'id'=>5010,
        'cat_id'=>8,
        'tag_id'=>11,
        'title'=>'Caring for Critically Ill Patients with the ABCDEF Bundle: Results of the ICU Liberation Collaborative in Over 15,000 Adults',
        'text'=>'Critical Care Medicine. 471:3-14, January 2019',
        'img'=>'file-1464165480-R231XR.jpg',
        'file'=>NULL,
        'link'=>'https://journals.lww.com/ccmjournal/Fulltext/2019/01000/Caring_for_Critically_Ill_Patients_with_the_ABCDEF.2.aspx',
        'by'=>0,
        'journal'=>'Pun, Brenda T. and others',
        'type'=>0,
        'month_article'=>0,
        'famous'=>0,
        'views'=>118,
        'show'=>0,
        'date'=>'2018-12-31 22:00:00',
        'active'=>'1',
        'user'=>0,
        'created_at'=>'2020-05-15 01:20:06',
        'updated_at'=>'2020-06-21 00:08:23',
        'deleted_at'=>NULL
        ] );
        
        
                    
        Post::create( [
        'id'=>5011,
        'cat_id'=>8,
        'tag_id'=>6,
        'title'=>'Extracorporeal CO2 Removal: The Minimally Invasive Approach, Theory, and Practice',
        'text'=>'Critical Care Medicine. 471:33-40, January 2019',
        'img'=>'im4.jpg',
        'file'=>NULL,
        'link'=>'https://journals.lww.com/ccmjournal/Fulltext/2019/01000/Extracorporeal_CO2_Removal__The_Minimally_Invasive.5.aspx',
        'by'=>0,
        'journal'=>'Duscio, Eleonora and others',
        'type'=>0,
        'month_article'=>0,
        'famous'=>0,
        'views'=>118,
        'show'=>0,
        'date'=>'2018-12-31 22:00:00',
        'active'=>'1',
        'user'=>0,
        'created_at'=>'2020-05-15 01:20:06',
        'updated_at'=>'2020-06-21 00:08:23',
        'deleted_at'=>NULL
        ] );
        
        
                    
        Post::create( [
        'id'=>5012,
        'cat_id'=>8,
        'tag_id'=>11,
        'title'=>'Estimating ICU Benefit: A Randomized Study of Physicians',
        'text'=>'Critical Care Medicine. 471:62-68, January 2019.',
        'img'=>'file-1464165463-D3CBM9.jpg',
        'file'=>NULL,
        'link'=>'https://journals.lww.com/ccmjournal/Abstract/2019/01000/Estimating_ICU_Benefit__A_Randomized_Study_of.9.aspx',
        'by'=>0,
        'journal'=>'Valley, Thomas S and others',
        'type'=>0,
        'month_article'=>0,
        'famous'=>0,
        'views'=>118,
        'show'=>0,
        'date'=>'2018-12-31 22:00:00',
        'active'=>'1',
        'user'=>0,
        'created_at'=>'2020-05-15 01:20:06',
        'updated_at'=>'2020-06-21 00:08:23',
        'deleted_at'=>NULL
        ] );
        
        
                    
        Post::create( [
        'id'=>5013,
        'cat_id'=>8,
        'tag_id'=>6,
        'title'=>'Extracorporeal Membrane Oxygenation for Acute Respiratory Distress Syndrome: EOLIA and Beyond',
        'text'=>'Critical Care Medicine. 471:114-117, January 2019',
        'img'=>'file-1463591752-XK911T.jpg',
        'file'=>NULL,
        'link'=>'https://journals.lww.com/ccmjournal/Citation/2019/01000/Extracorporeal_Membrane_Oxygenation_for_Acute.16.aspx',
        'by'=>0,
        'journal'=>'Bartlett, Robert H',
        'type'=>0,
        'month_article'=>0,
        'famous'=>0,
        'views'=>118,
        'show'=>0,
        'date'=>'2019-01-05 22:00:00',
        'active'=>'1',
        'user'=>0,
        'created_at'=>'2020-05-15 01:20:06',
        'updated_at'=>'2020-06-21 00:08:23',
        'deleted_at'=>NULL
        ] );
        
        
                    
        Post::create( [
        'id'=>5014,
        'cat_id'=>8,
        'tag_id'=>2,
        'title'=>'Enteral versus intravenous approach for the sedation of critically ill patients: a randomized and controlled trial',
        'text'=>'Critical Care 2019, 23:3  Published on: 7 January 2019',
        'img'=>'im5.jpg',
        'file'=>NULL,
        'link'=>'https://ccforum.biomedcentral.com/articles/10.1186/s13054-018-2280-x',
        'by'=>0,
        'journal'=>'Giovanni Mistraletti and others',
        'type'=>0,
        'month_article'=>0,
        'famous'=>0,
        'views'=>189,
        'show'=>0,
        'date'=>'2019-01-07 22:00:00',
        'active'=>'1',
        'user'=>0,
        'created_at'=>'2020-05-15 01:20:06',
        'updated_at'=>'2020-06-21 00:08:23',
        'deleted_at'=>NULL
        ] );
        
        
                    
        Post::create( [
        'id'=>5015,
        'cat_id'=>8,
        'tag_id'=>6,
        'title'=>'Neurally adjusted ventilatory assist NAVA versus pressure support ventilation: patient-ventilator interaction during invasive ventilation delivered by tracheostomy',
        'text'=>'Critical Care 2019, 23:2  Published on: 7 January 2019',
        'img'=>'im6.jpg',
        'file'=>NULL,
        'link'=>'https://ccforum.biomedcentral.com/articles/10.1186/s13054-018-2288-2',
        'by'=>0,
        'journal'=>'Olivier Lamouret and others',
        'type'=>0,
        'month_article'=>0,
        'famous'=>0,
        'views'=>118,
        'show'=>0,
        'date'=>'2019-01-07 22:00:00',
        'active'=>'1',
        'user'=>0,
        'created_at'=>'2020-05-15 01:20:06',
        'updated_at'=>'2020-06-21 00:08:23',
        'deleted_at'=>NULL
        ] );
        
        
                    
        Post::create( [
        'id'=>5016,
        'cat_id'=>8,
        'tag_id'=>7,
        'title'=>'Predicting mortality in patients undergoing VA-ECMO after coronary artery bypass grafting: the REMEMBER score',
        'text'=>'Critical Care 2019, 23:11  Published on: 11 January 2019',
        'img'=>'file-1463591644-ZGN2DA.jpg',
        'file'=>NULL,
        'link'=>'https://ccforum.biomedcentral.com/articles/10.1186/s13054-019-2307-y',
        'by'=>0,
        'journal'=>'Liangshan Wang and others',
        'type'=>0,
        'month_article'=>0,
        'famous'=>0,
        'views'=>118,
        'show'=>0,
        'date'=>'2019-01-12 22:00:00',
        'active'=>'1',
        'user'=>0,
        'created_at'=>'2020-05-15 01:20:06',
        'updated_at'=>'2020-06-21 00:08:23',
        'deleted_at'=>NULL
        ] );
        
        
                    
        Post::create( [
        'id'=>5017,
        'cat_id'=>8,
        'tag_id'=>7,
        'title'=>'Early diuretic use and mortality in critically ill patients with vasopressor support: a propensity score-matching analysis',
        'text'=>'Critical Care 2019, 23:9  Published on: 10 January 2019',
        'img'=>'file-1463591639-4YG8JY.jpg',
        'file'=>NULL,
        'link'=>'https://ccforum.biomedcentral.com/articles/10.1186/s13054-019-2309-9',
        'by'=>0,
        'journal'=>'Yanfei Shen, Weimin Zhang and Yong Shen',
        'type'=>0,
        'month_article'=>0,
        'famous'=>0,
        'views'=>118,
        'show'=>0,
        'date'=>'2019-01-09 22:00:00',
        'active'=>'1',
        'user'=>0,
        'created_at'=>'2020-05-15 01:20:06',
        'updated_at'=>'2020-06-21 00:08:23',
        'deleted_at'=>NULL
        ] );
        
        
                    
        Post::create( [
        'id'=>5018,
        'cat_id'=>8,
        'tag_id'=>11,
        'title'=>'Artificial intelligence in the intensive care unit',
        'text'=>'Critical Care 2019, 23:7  Published on: 10 January 2019',
        'img'=>'file-1464165463-D3CBM9.jpg',
        'file'=>NULL,
        'link'=>'https://ccforum.biomedcentral.com/articles/10.1186/s13054-018-2301-9',
        'by'=>0,
        'journal'=>'Christopher A. Lovejoy, Varun Buch and Mahiben Maruthappu',
        'type'=>0,
        'month_article'=>0,
        'famous'=>0,
        'views'=>118,
        'show'=>0,
        'date'=>'2019-01-08 22:00:00',
        'active'=>'1',
        'user'=>0,
        'created_at'=>'2020-05-15 01:20:06',
        'updated_at'=>'2020-06-21 00:08:23',
        'deleted_at'=>NULL
        ] );
        
        
                    
        Post::create( [
        'id'=>5019,
        'cat_id'=>8,
        'tag_id'=>11,
        'title'=>'Antipsychotic Medications for Delirium in the ICU',
        'text'=>'Society of Critical Care Medicine. Critical Connections',
        'img'=>'im1.jpg',
        'file'=>NULL,
        'link'=>'https://www.sccm.org/Blog/January-2019/Concise-Critical-Appraisal-Antipsychotic-Medicati?_zs=3THjd1&_zl=eJCR5',
        'by'=>0,
        'journal'=>'James H. Lantry III and Mohammed Nabeel',
        'type'=>0,
        'month_article'=>0,
        'famous'=>0,
        'views'=>118,
        'show'=>0,
        'date'=>'2019-01-07 22:00:00',
        'active'=>'1',
        'user'=>0,
        'created_at'=>'2020-05-15 01:20:06',
        'updated_at'=>'2020-06-21 00:08:23',
        'deleted_at'=>NULL
        ] );
        
        
                    
        Post::create( [
        'id'=>5020,
        'cat_id'=>8,
        'tag_id'=>11,
        'title'=>'Dosing adjuvant vitamin C in critically ill patients undergoing continuous renal replacement therapy: We are not there yet!',
        'text'=>'Critical Care 2019, 23:5  Published on: 9 January 2019',
        'img'=>'im4.jpg',
        'file'=>NULL,
        'link'=>'https://ccforum.biomedcentral.com/articles/10.1186/s13054-018-2297-1',
        'by'=>0,
        'journal'=>'Patrick M. Honore and others',
        'type'=>0,
        'month_article'=>0,
        'famous'=>0,
        'views'=>118,
        'show'=>0,
        'date'=>'2019-01-09 22:00:00',
        'active'=>'1',
        'user'=>0,
        'created_at'=>'2020-05-15 01:20:06',
        'updated_at'=>'2020-06-21 00:08:23',
        'deleted_at'=>NULL
        ] );
        
        
                    
        Post::create( [
        'id'=>5021,
        'cat_id'=>8,
        'tag_id'=>1,
        'title'=>'Vancomycin pharmacokinetics in critically ill obese patients: can the clinician sit back and relax',
        'text'=>'Critical Care 2019, 23:15  Published on: 17 January 2019',
        'img'=>'file-1464106363-HWQX6B.jpg',
        'file'=>NULL,
        'link'=>'https://ccforum.biomedcentral.com/articles/10.1186/s13054-019-2311-2',
        'by'=>0,
        'journal'=>'Patrick M. Honore and others',
        'type'=>0,
        'month_article'=>0,
        'famous'=>0,
        'views'=>147,
        'show'=>0,
        'date'=>'2019-01-16 22:00:00',
        'active'=>'1',
        'user'=>0,
        'created_at'=>'2020-05-15 01:20:06',
        'updated_at'=>'2020-06-21 00:08:23',
        'deleted_at'=>NULL
        ] );
        
        
                    
        Post::create( [
        'id'=>5022,
        'cat_id'=>8,
        'tag_id'=>8,
        'title'=>'To remove and replace—a role for plasma exchange in counterbalancing the host response in sepsis',
        'text'=>'Critical Care 2019, 23:14  Published on: 17 January 2019',
        'img'=>'file-1464165463-D3CBM9.jpg',
        'file'=>NULL,
        'link'=>'https://ccforum.biomedcentral.com/articles/10.1186/s13054-018-2289-1',
        'by'=>0,
        'journal'=>'S. David and K. Stahl',
        'type'=>0,
        'month_article'=>0,
        'famous'=>0,
        'views'=>169,
        'show'=>0,
        'date'=>'2019-01-16 22:00:00',
        'active'=>'1',
        'user'=>0,
        'created_at'=>'2020-05-15 01:20:06',
        'updated_at'=>'2020-06-21 00:08:23',
        'deleted_at'=>NULL
        ] );
        
        
                    
        Post::create( [
        'id'=>5023,
        'cat_id'=>8,
        'tag_id'=>11,
        'title'=>'Cerebrospinal fluid procalcitonin predicts Gram-negative bacterial meningitis in patients with empiric antibiotic pretreatment',
        'text'=>'Critical Care 2019, 23:21  Published on: 23 January 2019',
        'img'=>'im2.jpg',
        'file'=>NULL,
        'link'=>'https://ccforum.biomedcentral.com/articles/10.1186/s13054-019-2318-8',
        'by'=>0,
        'journal'=>'Wen Li, Fang Yuan, Xiaolong Sun, Zhihan Zhao, Yaoyao Zhang and Wen Jiang',
        'type'=>0,
        'month_article'=>0,
        'famous'=>0,
        'views'=>118,
        'show'=>0,
        'date'=>'2019-01-22 22:00:00',
        'active'=>'1',
        'user'=>0,
        'created_at'=>'2020-05-15 01:20:06',
        'updated_at'=>'2020-06-21 00:08:23',
        'deleted_at'=>NULL
        ] );
        
        
                    
        Post::create( [
        'id'=>5024,
        'cat_id'=>8,
        'tag_id'=>11,
        'title'=>'How to understand real net ultrafiltration and its association with low blood pressure in critically ill patients with renal replacement therapy',
        'text'=>'Critical Care 2019, 23:20  Published on: 23 January 2019',
        'img'=>'file-1463591738-IZ3XFW.jpg',
        'file'=>NULL,
        'link'=>'https://ccforum.biomedcentral.com/articles/10.1186/s13054-018-2298-0',
        'by'=>0,
        'journal'=>'Wei Li, Jinsong Zhang and Xufeng Chen',
        'type'=>0,
        'month_article'=>0,
        'famous'=>0,
        'views'=>118,
        'show'=>0,
        'date'=>'2019-01-23 22:00:00',
        'active'=>'1',
        'user'=>0,
        'created_at'=>'2020-05-15 01:20:06',
        'updated_at'=>'2020-06-21 00:08:23',
        'deleted_at'=>NULL
        ] );
        
        
                    
        Post::create( [
        'id'=>5025,
        'cat_id'=>8,
        'tag_id'=>1,
        'title'=>'Effect of Targeted Polymyxin B Hemoperfusion on 28-Day Mortality in Patients With Septic Shock and Elevated Endotoxin Level The EUPHRATES Randomized Clinical Trial',
        'text'=>'JAMA. 201832014:1455-1463',
        'img'=>'file-1463591738-IZ3XFW.jpg',
        'file'=>NULL,
        'link'=>'https://jamanetwork.com/journals/jama/article-abstract/2706139',
        'by'=>0,
        'journal'=>'R. Phillip Dellinger, and others',
        'type'=>0,
        'month_article'=>0,
        'famous'=>1,
        'views'=>242,
        'show'=>0,
        'date'=>'2018-02-01 22:00:00',
        'active'=>'1',
        'user'=>0,
        'created_at'=>'2020-05-15 01:20:06',
        'updated_at'=>'2020-06-21 00:08:23',
        'deleted_at'=>NULL
        ] );
        
        
                    
        Post::create( [
        'id'=>5026,
        'cat_id'=>12,
        'tag_id'=>1,
        'title'=>'How Best to Resuscitate Patients With Septic Shock',
        'text'=>'JAMA. Published online February 17, 2019',
        'img'=>'file-1463591657-K8BRDF.jpg',
        'file'=>NULL,
        'link'=>'https://jamanetwork.com/journals/jama/fullarticle/2724360',
        'by'=>0,
        'journal'=>'Derek C. Angus, MD, MPH',
        'type'=>0,
        'month_article'=>0,
        'famous'=>0,
        'views'=>258,
        'show'=>0,
        'date'=>'2019-02-16 22:00:00',
        'active'=>'1',
        'user'=>0,
        'created_at'=>'2020-05-15 01:20:06',
        'updated_at'=>'2020-06-21 00:08:23',
        'deleted_at'=>NULL
        ] );
        
        
                    
        Post::create( [
        'id'=>5027,
        'cat_id'=>12,
        'tag_id'=>1,
        'title'=>'Effect of a Resuscitation Strategy Targeting Peripheral Perfusion Status vs Serum Lactate Levels on 28-Day Mortality Among Patients With Septic Shock The ANDROMEDA-SHOCK Randomized Clinical Trial',
        'text'=>'JAMA. Published online February 17, 2019',
        'img'=>'file-1464106363-HWQX6B.jpg',
        'file'=>NULL,
        'link'=>'https://jamanetwork.com/journals/jama/fullarticle/2724361',
        'by'=>0,
        'journal'=>'Glenn Hernández and others',
        'type'=>0,
        'month_article'=>0,
        'famous'=>0,
        'views'=>258,
        'show'=>0,
        'date'=>'2019-02-17 22:00:00',
        'active'=>'1',
        'user'=>0,
        'created_at'=>'2020-05-15 01:20:06',
        'updated_at'=>'2020-06-21 00:08:23',
        'deleted_at'=>NULL
        ] );
        
        
                    
        Post::create( [
        'id'=>5028,
        'cat_id'=>12,
        'tag_id'=>11,
        'title'=>'Effect of Titrating Positive End-Expiratory Pressure PEEP With an Esophageal Pressure–Guided Strategy vs an Empirical High PEEP-Fio2 Strategy on Death and Days Free From Mechanical Ventilation Among Patients With Acute Respiratory Distress Syndrome A Rand',
        'text'=>'JAMA. Published online February 18, 2019',
        'img'=>'im6.jpg',
        'file'=>NULL,
        'link'=>'https://jamanetwork.com/journals/jama/fullarticle/2725206',
        'by'=>0,
        'journal'=>'Jeremy R. Beitler, and others',
        'type'=>0,
        'month_article'=>0,
        'famous'=>0,
        'views'=>229,
        'show'=>0,
        'date'=>'2019-02-18 22:00:00',
        'active'=>'1',
        'user'=>0,
        'created_at'=>'2020-05-15 01:20:06',
        'updated_at'=>'2020-06-21 00:08:23',
        'deleted_at'=>NULL
        ] );
        
        
                    
        Post::create( [
        'id'=>5029,
        'cat_id'=>8,
        'tag_id'=>10,
        'title'=>'Adjunctive Intermittent Pneumatic Compression for Venous Thromboprophylaxis',
        'text'=>'NEJ Med. February 18, 2019',
        'img'=>'file-1463591681-AGRM7H.jpg',
        'file'=>NULL,
        'link'=>'https://www.nejm.org/doi/full/10.1056/NEJMoa1816150?query=main_nav_lg',
        'by'=>0,
        'journal'=>'Yaseen M. Arabi, Fahad Al-Hameed and others',
        'type'=>0,
        'month_article'=>0,
        'famous'=>0,
        'views'=>118,
        'show'=>0,
        'date'=>'2019-02-17 22:00:00',
        'active'=>'1',
        'user'=>0,
        'created_at'=>'2020-05-15 01:20:06',
        'updated_at'=>'2020-06-21 00:08:23',
        'deleted_at'=>NULL
        ] );
        
        
                    
        Post::create( [
        'id'=>5030,
        'cat_id'=>8,
        'tag_id'=>6,
        'title'=>'Bag-Mask Ventilation during Tracheal Intubation of Critically Ill Adults',
        'text'=>'N Eng J Med. February 18, 2019',
        'img'=>'file-1463591752-XK911T.jpg',
        'file'=>NULL,
        'link'=>'https://www.nejm.org/doi/full/10.1056/NEJMoa1812405?query=featured_home',
        'by'=>0,
        'journal'=>'Jonathan D. Casey and others, for the PreVent Investigators and the Pragmatic Critical Care Research Group',
        'type'=>0,
        'month_article'=>0,
        'famous'=>0,
        'views'=>118,
        'show'=>0,
        'date'=>'2019-02-18 22:00:00',
        'active'=>'1',
        'user'=>0,
        'created_at'=>'2020-05-15 01:20:06',
        'updated_at'=>'2020-06-21 00:08:23',
        'deleted_at'=>NULL
        ] );
        
        
                    
        Post::create( [
        'id'=>5031,
        'cat_id'=>8,
        'tag_id'=>11,
        'title'=>'Intravascular versus surface cooling for targeted temperature management after out-of-hospital cardiac arrest: an analysis of the TTH48 trial',
        'text'=>'Critical Care 2019, 23:61  Published on 22 February 2019',
        'img'=>'file-1463591644-ZGN2DA.jpg',
        'file'=>NULL,
        'link'=>'https://ccforum.biomedcentral.com/articles/10.1186/s13054-019-2335-7',
        'by'=>0,
        'journal'=>'Chiara De Fazio and others',
        'type'=>0,
        'month_article'=>0,
        'famous'=>0,
        'views'=>118,
        'show'=>0,
        'date'=>'2019-02-24 22:00:00',
        'active'=>'1',
        'user'=>0,
        'created_at'=>'2020-05-15 01:20:06',
        'updated_at'=>'2020-06-21 00:08:23',
        'deleted_at'=>NULL
        ] );
        
        
                    
        Post::create( [
        'id'=>5032,
        'cat_id'=>8,
        'tag_id'=>11,
        'title'=>'Diagnostic and therapeutic approach in adult patients with traumatic brain injury receiving oral anticoagulant therapy: an Austrian interdisciplinary consensus statement',
        'text'=>'Critical Care 2019 23:62. Published on: 22 February 2019',
        'img'=>'im2.jpg',
        'file'=>NULL,
        'link'=>'https://ccforum.biomedcentral.com/articles',
        'by'=>0,
        'journal'=>'Marion Wiegele and others',
        'type'=>0,
        'month_article'=>0,
        'famous'=>0,
        'views'=>118,
        'show'=>0,
        'date'=>'2019-02-24 22:00:00',
        'active'=>'1',
        'user'=>0,
        'created_at'=>'2020-05-15 01:20:06',
        'updated_at'=>'2020-06-21 00:08:23',
        'deleted_at'=>NULL
        ] );
        
        
                    
        Post::create( [
        'id'=>5033,
        'cat_id'=>8,
        'tag_id'=>5,
        'title'=>'Response to “Are fluids resuscitation the “Keyser Soze” of acute kidney injury in trauma patients”',
        'text'=>'Critical Care 2019. 23:59.Published: 19 February 2019',
        'img'=>'im1.jpg',
        'file'=>NULL,
        'link'=>'https://ccforum.biomedcentral.com/articles/10.1186/s13054-019-2344-6',
        'by'=>0,
        'journal'=>'Anatole HarroisEmail authorView ORCID ID profile, Benjamin Soyer, Tobias Gauss, Sophie Hamada, Mathieu Raux, Jacques Duranteau and for the Traumabase Group',
        'type'=>0,
        'month_article'=>0,
        'famous'=>0,
        'views'=>131,
        'show'=>0,
        'date'=>'2019-02-19 22:00:00',
        'active'=>'1',
        'user'=>0,
        'created_at'=>'2020-05-15 01:20:06',
        'updated_at'=>'2020-06-21 00:08:23',
        'deleted_at'=>NULL
        ] );
        
        
                    
        Post::create( [
        'id'=>5034,
        'cat_id'=>8,
        'tag_id'=>11,
        'title'=>'Letter on “Synbiotics modulate gut microbiota and reduce enteritis and ventilator-associated pneumonia in patients with sepsis: a randomized controlled trial”',
        'text'=>'Critical Care 2019 23:56. Published on: 19 February 2019',
        'img'=>'file-1463591725-9J88SF.jpg',
        'file'=>NULL,
        'link'=>'https://ccforum.biomedcentral.com/articles/10.1186/s13054-019-2319-7',
        'by'=>0,
        'journal'=>'Alexander Reisinger and Vanessa Stadlbauer',
        'type'=>0,
        'month_article'=>0,
        'famous'=>0,
        'views'=>118,
        'show'=>0,
        'date'=>'2019-02-18 22:00:00',
        'active'=>'1',
        'user'=>0,
        'created_at'=>'2020-05-15 01:20:06',
        'updated_at'=>'2020-06-21 00:08:23',
        'deleted_at'=>NULL
        ] );


        Post::create( [
            'id'=>5035,
            'cat_id'=>8,
            'tag_id'=>11,
            'title'=>'Synbiotics modulate gut microbiota and reduce enteritis and ventilator-associated pneumonia in patients with sepsis: a randomized controlled trial',
            'text'=>'Crit Care. 2018 Sep 27221:239',
            'img'=>'file-1463591639-4YG8JY.jpg',
            'file'=>NULL,
            'link'=>'https://www.ncbi.nlm.nih.gov/pubmed/30261905',
            'by'=>0,
            'journal'=>'Shimizu K1 and others',
            'type'=>0,
            'month_article'=>0,
            'famous'=>0,
            'views'=>118,
            'show'=>0,
            'date'=>'2018-09-24 22:00:00',
            'active'=>'1',
            'user'=>0,
            'created_at'=>'2020-05-15 01:20:06',
            'updated_at'=>'2020-06-21 00:08:23',
            'deleted_at'=>NULL
            ] );
            
            
                        
            Post::create( [
            'id'=>5036,
            'cat_id'=>8,
            'tag_id'=>3,
            'title'=>'Effect of Hospital Use of Oral Nutritional Supplementation on Length of Stay, Hospital Cost, and 30-Day Readmissions Among Medicare Patients With COPD',
            'text'=>'Chest. 2015 Jun 1476: 1477–1484.Published online 2014',
            'img'=>'file-1463591725-9J88SF.jpg',
            'file'=>NULL,
            'link'=>'https://www.ncbi.nlm.nih.gov/pmc/articles/PMC4451705/',
            'by'=>0,
            'journal'=>'Julia Thornton Snider and others',
            'type'=>0,
            'month_article'=>0,
            'famous'=>1,
            'views'=>213,
            'show'=>0,
            'date'=>'2015-10-25 22:00:00',
            'active'=>'1',
            'user'=>0,
            'created_at'=>'2020-05-15 01:20:06',
            'updated_at'=>'2020-06-21 00:08:23',
            'deleted_at'=>NULL
            ] );
            
            
                        
            Post::create( [
            'id'=>5037,
            'cat_id'=>8,
            'tag_id'=>3,
            'title'=>'Adherence to the ERAS protocol is Associated with 5-Year Survival After Colorectal Cancer Surgery: A Retrospective Cohort Study',
            'text'=>'World Journal of Surgery\r\nJuly 2016, Volume 40, Issue 7, pp 1741–1747',
            'img'=>'file-1463591725-9J88SF.jpg',
            'file'=>NULL,
            'link'=>'https://link.springer.com/article/10.1007%2Fs00268-016-3460-y#dietitians',
            'by'=>0,
            'journal'=>'Ulf O. Gustafsson, Henrik Oppelstrup Anders Thorell Jonas, Nygren Olle Ljungqvist',
            'type'=>0,
            'month_article'=>0,
            'famous'=>1,
            'views'=>213,
            'show'=>0,
            'date'=>'2016-03-11 22:00:00',
            'active'=>'1',
            'user'=>0,
            'created_at'=>'2020-05-15 01:20:06',
            'updated_at'=>'2020-06-21 00:08:23',
            'deleted_at'=>NULL
            ] );
            
            
                        
            Post::create( [
            'id'=>5038,
            'cat_id'=>8,
            'tag_id'=>11,
            'title'=>'The Intensive Care Unit - Past, Present and Future',
            'text'=>'ICU Management & Practice, Volume 19 - Issue 1, 2019',
            'img'=>'file-1464165480-R231XR.jpg',
            'file'=>NULL,
            'link'=>'https://healthmanagement.org/c/icu/issuearticle/the-intensive-care-unit-past-present-and-future',
            'by'=>0,
            'journal'=>'Pierre Singer, Liron Elia',
            'type'=>0,
            'month_article'=>0,
            'famous'=>0,
            'views'=>118,
            'show'=>0,
            'date'=>'2019-03-17 22:00:00',
            'active'=>'1',
            'user'=>0,
            'created_at'=>'2020-05-15 01:20:06',
            'updated_at'=>'2020-06-21 00:08:23',
            'deleted_at'=>NULL
            ] );
            
            
                        
            Post::create( [
            'id'=>5039,
            'cat_id'=>8,
            'tag_id'=>3,
            'title'=>'Technology innovations in delivering accurate nutrition',
            'text'=>'ICU Management & Practice, Volume 19 - Issue 1, 2019',
            'img'=>'file-1463591732-SM39NY.jpg',
            'file'=>NULL,
            'link'=>'https://healthmanagement.org/c/icu/issuearticle/technology-innovations-in-delivering-accurate-nutrition',
            'by'=>0,
            'journal'=>'Pierre Singer, Liron Elia',
            'type'=>0,
            'month_article'=>0,
            'famous'=>0,
            'views'=>118,
            'show'=>0,
            'date'=>'2019-03-17 22:00:00',
            'active'=>'1',
            'user'=>0,
            'created_at'=>'2020-05-15 01:20:06',
            'updated_at'=>'2020-06-21 00:08:23',
            'deleted_at'=>NULL
            ] );
            
            
                        
            Post::create( [
            'id'=>5040,
            'cat_id'=>8,
            'tag_id'=>6,
            'title'=>'Innovations in ICU ventilation',
            'text'=>'ICU Management & Practice, Volume 19 - Issue 1, 2019',
            'img'=>'im6.jpg',
            'file'=>NULL,
            'link'=>'https://healthmanagement.org/c/icu/issuearticle/innovations-in-icu-ventilation',
            'by'=>0,
            'journal'=>'Prof. Federico Gordo',
            'type'=>0,
            'month_article'=>0,
            'famous'=>0,
            'views'=>118,
            'show'=>0,
            'date'=>'2019-03-17 22:00:00',
            'active'=>'1',
            'user'=>0,
            'created_at'=>'2020-05-15 01:20:06',
            'updated_at'=>'2020-06-21 00:08:23',
            'deleted_at'=>NULL
            ] );
            
            
                        
            Post::create( [
            'id'=>5041,
            'cat_id'=>8,
            'tag_id'=>2,
            'title'=>'How to manage sedation analgesia for patient-centred care in the ICU',
            'text'=>'ICU Management & Practice, Volume 19 - Issue 1, 2019',
            'img'=>'im5.jpg',
            'file'=>NULL,
            'link'=>'https://healthmanagement.org/c/icu/issuearticle/concluding-remarks',
            'by'=>0,
            'journal'=>'Jean-Louis Vincent',
            'type'=>0,
            'month_article'=>0,
            'famous'=>0,
            'views'=>189,
            'show'=>0,
            'date'=>'2019-03-17 22:00:00',
            'active'=>'1',
            'user'=>0,
            'created_at'=>'2020-05-15 01:20:06',
            'updated_at'=>'2020-06-21 00:08:23',
            'deleted_at'=>NULL
            ] );
            
            
                        
            Post::create( [
            'id'=>5042,
            'cat_id'=>8,
            'tag_id'=>1,
            'title'=>'Results of the ABATE infection trial',
            'text'=>'ICU Management & Practice, Volume 19 - Issue 1, 2019',
            'img'=>'file-1464106363-HWQX6B.jpg',
            'file'=>NULL,
            'link'=>'https://healthmanagement.org/c/icu/issuearticle/results-of-the-abate-infection-trial',
            'by'=>0,
            'journal'=>'Huang SS, Septimus E, Kleinman K et al.',
            'type'=>0,
            'month_article'=>0,
            'famous'=>0,
            'views'=>147,
            'show'=>0,
            'date'=>'2019-03-17 22:00:00',
            'active'=>'1',
            'user'=>0,
            'created_at'=>'2020-05-15 01:20:06',
            'updated_at'=>'2020-06-21 00:08:23',
            'deleted_at'=>NULL
            ] );
            
            
                        
            Post::create( [
            'id'=>5043,
            'cat_id'=>8,
            'tag_id'=>7,
            'title'=>'Coronary Angiography after Cardiac Arrest — The Right Timing or the Right Patients',
            'text'=>'N Engl J Med. March 18, 2019',
            'img'=>'file-1463591639-4YG8JY.jpg',
            'file'=>NULL,
            'link'=>'https://www.nejm.org/doi/full/10.1056/NEJMe1901651?query=RP',
            'by'=>0,
            'journal'=>'Benjamin S. Abella, M.D., M.Phil., and David F. Gaieski, M.D.',
            'type'=>0,
            'month_article'=>0,
            'famous'=>0,
            'views'=>118,
            'show'=>0,
            'date'=>'2019-03-17 22:00:00',
            'active'=>'1',
            'user'=>0,
            'created_at'=>'2020-05-15 01:20:06',
            'updated_at'=>'2020-06-21 00:08:23',
            'deleted_at'=>NULL
            ] );
            
            
                        
            Post::create( [
            'id'=>5044,
            'cat_id'=>8,
            'tag_id'=>7,
            'title'=>'Coronary Angiography after Cardiac Arrest without ST-Segment Elevation',
            'text'=>'N Engl J Med. March 18, 2019',
            'img'=>'file-1463591644-ZGN2DA.jpg',
            'file'=>NULL,
            'link'=>'https://www.nejm.org/doi/full/10.1056/NEJMoa1816897?query=RP',
            'by'=>0,
            'journal'=>'J.S. Lemkes and Others',
            'type'=>0,
            'month_article'=>0,
            'famous'=>1,
            'views'=>213,
            'show'=>0,
            'date'=>'2019-03-17 22:00:00',
            'active'=>'1',
            'user'=>0,
            'created_at'=>'2020-05-15 01:20:06',
            'updated_at'=>'2020-06-21 00:08:23',
            'deleted_at'=>NULL
            ] );
            
            
                        
            Post::create( [
            'id'=>5045,
            'cat_id'=>12,
            'tag_id'=>7,
            'title'=>'Pregnancy-Adapted YEARS Algorithm for Diagnosis of Suspected Pulmonary Embolism',
            'text'=>'Liselotte M. van der Pol and others, for the Artemis Study Investigators',
            'img'=>'file-1463591644-ZGN2DA.jpg',
            'file'=>NULL,
            'link'=>'https://www.nejm.org/doi/full/10.1056/NEJMoa1813865queryfeatured_home',
            'by'=>0,
            'journal'=>'N Engl J Med 2019 380:1139-1149. March 21, 2019',
            'type'=>0,
            'month_article'=>0,
            'famous'=>0,
            'views'=>229,
            'show'=>0,
            'date'=>'2019-03-20 22:00:00',
            'active'=>'1',
            'user'=>0,
            'created_at'=>'2020-05-15 01:20:06',
            'updated_at'=>'2020-06-21 00:08:23',
            'deleted_at'=>NULL
            ] );
            
            
                        
            Post::create( [
            'id'=>5046,
            'cat_id'=>8,
            'tag_id'=>7,
            'title'=>'In-Hospital Cardiac Arrest: A Review',
            'text'=>'AMA. 201932112:1200-1210',
            'img'=>'file-1463591639-4YG8JY.jpg',
            'file'=>NULL,
            'link'=>'https://jamanetwork.com/journals/jama/article-abstract/2728930',
            'by'=>0,
            'journal'=>'Lars W. Andersen, MD, MPH, PhD, DMSc et al.',
            'type'=>0,
            'month_article'=>0,
            'famous'=>0,
            'views'=>118,
            'show'=>0,
            'date'=>'2019-03-25 22:00:00',
            'active'=>'1',
            'user'=>0,
            'created_at'=>'2020-05-15 01:20:06',
            'updated_at'=>'2020-06-21 00:08:23',
            'deleted_at'=>NULL
            ] );
            
            
                        
            Post::create( [
            'id'=>5047,
            'cat_id'=>8,
            'tag_id'=>11,
            'title'=>'Emergency Medical Services Protocols for Generalized Convulsive Status Epilepticus',
            'text'=>'JAMA. 201932112:1216-1217',
            'img'=>'im1.jpg',
            'file'=>NULL,
            'link'=>'https://jamanetwork.com/journals/jama/article-abstract/2728910',
            'by'=>0,
            'journal'=>'John P. Betjemann, MD et al',
            'type'=>0,
            'month_article'=>0,
            'famous'=>0,
            'views'=>118,
            'show'=>0,
            'date'=>'2019-03-25 22:00:00',
            'active'=>'1',
            'user'=>0,
            'created_at'=>'2020-05-15 01:20:06',
            'updated_at'=>'2020-06-21 00:08:23',
            'deleted_at'=>NULL
            ] );
            
            
                        
            Post::create( [
            'id'=>5048,
            'cat_id'=>8,
            'tag_id'=>11,
            'title'=>'Chlorhexidine versus routine bathing to prevent multidrug-resistant organisms and all-cause bloodstream infections in general medical and surgical units ABATE Infection trial: a cluster-randomised trial',
            'text'=>'The Lancet.Published: March 05, 2019',
            'img'=>'file-1464165480-R231XR.jpg',
            'file'=>NULL,
            'link'=>'https://www.thelancet.com/journals/lancet/article/PIIS0140-6736(18)32593-5/fulltext',
            'by'=>0,
            'journal'=>'Huang et al',
            'type'=>0,
            'month_article'=>0,
            'famous'=>0,
            'views'=>118,
            'show'=>0,
            'date'=>'2019-03-25 22:00:00',
            'active'=>'1',
            'user'=>0,
            'created_at'=>'2020-05-15 01:20:06',
            'updated_at'=>'2020-06-21 00:08:23',
            'deleted_at'=>NULL
            ] );
            
            
                        
            Post::create( [
            'id'=>5049,
            'cat_id'=>8,
            'tag_id'=>11,
            'title'=>'β blockers to prevent decompensation of cirrhosis in patients with clinically significant portal hypertension PREDESCI: a randomised, double-blind, placebo-controlled, multicentre trial',
            'text'=>'The Lancet. \r\nThe relationships between democratic experience, adult health, and cause-specific mortality in 170 countries between 1980 and 2016: an observational analysis\r\nBollyky et al, March 13, 2019',
            'img'=>'file-1463591639-4YG8JY.jpg',
            'file'=>NULL,
            'link'=>'https://www.thelancet.com/journals/lancet/article/PIIS0140-6736(18)31875-0/fulltext',
            'by'=>0,
            'journal'=>'Villanueva et al.',
            'type'=>0,
            'month_article'=>0,
            'famous'=>0,
            'views'=>118,
            'show'=>0,
            'date'=>'2019-03-25 22:00:00',
            'active'=>'1',
            'user'=>0,
            'created_at'=>'2020-05-15 01:20:06',
            'updated_at'=>'2020-06-21 00:08:23',
            'deleted_at'=>NULL
            ] );
            
            
                        
            Post::create( [
            'id'=>5050,
            'cat_id'=>8,
            'tag_id'=>11,
            'title'=>'Incidence, Risk Factors, and Outcomes of Intra-Abdominal Hypertension in Critically Ill Patients—A Prospective Multicenter Study IROI Study',
            'text'=>'Critical Care Medicine. 474:535-542, April 2019',
            'img'=>'file-1463591657-K8BRDF.jpg',
            'file'=>NULL,
            'link'=>'https://journals.lww.com/ccmjournal/Fulltext/2019/04000/Incidence,_Risk_Factors,_and_Outcomes_of.6.aspx',
            'by'=>0,
            'journal'=>'Reintam Blaser, and others',
            'type'=>0,
            'month_article'=>0,
            'famous'=>0,
            'views'=>118,
            'show'=>0,
            'date'=>'2019-03-26 22:00:00',
            'active'=>'1',
            'user'=>0,
            'created_at'=>'2020-05-15 01:20:06',
            'updated_at'=>'2020-06-21 00:08:23',
            'deleted_at'=>NULL
            ] );
            
            
                        
            Post::create( [
            'id'=>5051,
            'cat_id'=>8,
            'tag_id'=>7,
            'title'=>'The effects and safety of vasopressin receptor agonists in patients with septic shock: a meta-analysis and trial sequential analysis',
            'text'=>'Critical Care 2019 23:91. Published on: 14 March 2019',
            'img'=>'file-1464165463-D3CBM9.jpg',
            'file'=>NULL,
            'link'=>'https://ccforum.biomedcentral.com/articles/10.1186/s13054-019-2362-4',
            'by'=>0,
            'journal'=>'Libing Jiang, Yi Sheng, Xia Feng and Jing Wu',
            'type'=>0,
            'month_article'=>0,
            'famous'=>0,
            'views'=>118,
            'show'=>0,
            'date'=>'2019-03-26 22:00:00',
            'active'=>'1',
            'user'=>0,
            'created_at'=>'2020-05-15 01:20:06',
            'updated_at'=>'2020-06-21 00:08:23',
            'deleted_at'=>NULL
            ] );
            
            
                        
            Post::create( [
            'id'=>5052,
            'cat_id'=>13,
            'tag_id'=>6,
            'title'=>'Bova Score for Pulmonary Embolism Complications',
            'text'=>'',
            'img'=>'file-1463591644-ZGN2DA.jpg',
            'file'=>NULL,
            'link'=>'https://www.mdcalc.com/bova-score-pulmonary-embolism-complications#next-steps',
            'by'=>0,
            'journal'=>'MedCalc',
            'type'=>0,
            'month_article'=>0,
            'famous'=>0,
            'views'=>118,
            'show'=>0,
            'date'=>'2019-04-02 22:00:00',
            'active'=>'1',
            'user'=>0,
            'created_at'=>'2020-05-15 01:20:06',
            'updated_at'=>'2020-06-21 00:08:23',
            'deleted_at'=>NULL
            ] );
            
            
                        
            Post::create( [
            'id'=>5053,
            'cat_id'=>12,
            'tag_id'=>5,
            'title'=>'The Effect of Goal-Directed Therapy on Patient Morbidity and Mortality After Traumatic Brain Injury: Results From the Progesterone for the Treatment of Traumatic Brain Injury III Clinical Trial',
            'text'=>'Critical Care Medicine. 475:623-631, May 2019',
            'img'=>'im1.jpg',
            'file'=>NULL,
            'link'=>'https://journals.lww.com/ccmjournal/Fulltext/2019/05000/The_Effect_of_Goal_Directed_Therapy_on_Patient.1.aspx',
            'by'=>0,
            'journal'=>'Merck, Lisa H and others',
            'type'=>0,
            'month_article'=>0,
            'famous'=>1,
            'views'=>337,
            'show'=>0,
            'date'=>'2019-04-13 22:00:00',
            'active'=>'1',
            'user'=>0,
            'created_at'=>'2020-05-15 01:20:06',
            'updated_at'=>'2020-06-21 00:08:23',
            'deleted_at'=>NULL
            ] );
            
            
                        
            Post::create( [
            'id'=>5054,
            'cat_id'=>8,
            'tag_id'=>1,
            'title'=>'Does Obesity Protect Against Death in Sepsis A Retrospective Cohort Study of 55,038 Adult Patients',
            'text'=>'Critical Care Medicine. 475:643-650, May 2019',
            'img'=>'file-1464106363-HWQX6B.jpg',
            'file'=>NULL,
            'link'=>'https://journals.lww.com/ccmjournal/Fulltext/2019/05000/Does_Obesity_Protect_Against_Death_in_Sepsis__A.3.aspx',
            'by'=>0,
            'journal'=>'Pepper, Dominique and others',
            'type'=>0,
            'month_article'=>0,
            'famous'=>0,
            'views'=>147,
            'show'=>0,
            'date'=>'2019-03-31 22:00:00',
            'active'=>'1',
            'user'=>0,
            'created_at'=>'2020-05-15 01:20:06',
            'updated_at'=>'2020-06-21 00:08:23',
            'deleted_at'=>NULL
            ] );
            
            
                        
            Post::create( [
            'id'=>5055,
            'cat_id'=>8,
            'tag_id'=>11,
            'title'=>'Central Venous Access Capability and Critical Care Telemedicine Decreases Inter-Hospital Transfer Among Severe Sepsis Patients: A Mixed Methods Design',
            'text'=>'Critical Care Medicine. 475:659-667, May 2019',
            'img'=>'file-1464165463-D3CBM9.jpg',
            'file'=>NULL,
            'link'=>'https://journals.lww.com/ccmjournal/Abstract/2019/05000/Central_Venous_Access_Capability_and_Critical_Care.5.aspx',
            'by'=>0,
            'journal'=>'Ilko, Steven A and others',
            'type'=>0,
            'month_article'=>0,
            'famous'=>0,
            'views'=>118,
            'show'=>0,
            'date'=>'2019-04-01 22:00:00',
            'active'=>'1',
            'user'=>0,
            'created_at'=>'2020-05-15 01:20:06',
            'updated_at'=>'2020-06-21 00:08:23',
            'deleted_at'=>NULL
            ] );
            
            
                        
            Post::create( [
            'id'=>5056,
            'cat_id'=>8,
            'tag_id'=>11,
            'title'=>'Moderate-Intensity Insulin Therapy Is Associated With Reduced Length of Stay in Critically Ill Patients With Diabetic Ketoacidosis and Hyperosmolar Hyperglycemic State',
            'text'=>'Critical Care Medicine. 475:700-705, May 2019',
            'img'=>'file-1463591639-4YG8JY.jpg',
            'file'=>NULL,
            'link'=>'https://journals.lww.com/ccmjournal/Abstract/2019/05000/Moderate_Intensity_Insulin_Therapy_Is_Associated.10.aspx',
            'by'=>0,
            'journal'=>'Firestone, Rachelle L and others',
            'type'=>0,
            'month_article'=>0,
            'famous'=>0,
            'views'=>118,
            'show'=>0,
            'date'=>'2019-04-03 22:00:00',
            'active'=>'1',
            'user'=>0,
            'created_at'=>'2020-05-15 01:20:06',
            'updated_at'=>'2020-06-21 00:08:23',
            'deleted_at'=>NULL
            ] );
            
            
                        
            Post::create( [
            'id'=>5057,
            'cat_id'=>8,
            'tag_id'=>8,
            'title'=>'Renal Replacement Therapy in the ICU',
            'text'=>'Rachoin, Jean-Sebastien and others',
            'img'=>'file-1463591738-IZ3XFW.jpg',
            'file'=>NULL,
            'link'=>'https://journals.lww.com/ccmjournal/Abstract/2019/05000/Renal_Replacement_Therapy_in_the_ICU.12.aspx',
            'by'=>0,
            'journal'=>'Critical Care Medicine. 475:715-721, May 2019',
            'type'=>0,
            'month_article'=>0,
            'famous'=>0,
            'views'=>169,
            'show'=>0,
            'date'=>'2019-04-13 22:00:00',
            'active'=>'1',
            'user'=>0,
            'created_at'=>'2020-05-15 01:20:06',
            'updated_at'=>'2020-06-21 00:08:23',
            'deleted_at'=>NULL
            ] );
            
            
                        
            Post::create( [
            'id'=>5058,
            'cat_id'=>8,
            'tag_id'=>5,
            'title'=>'Continuous Electroencephalography After Moderate to Severe Traumatic Brain Injury',
            'text'=>'Critical Care Medicine. 474:574-582, April 2019',
            'img'=>'im2.jpg',
            'file'=>NULL,
            'link'=>'https://journals.lww.com/ccmjournal/Abstract/2019/04000/Continuous_Electroencephalography_After_Moderate.11.aspx',
            'by'=>0,
            'journal'=>'Lee, Hyunjo and others',
            'type'=>0,
            'month_article'=>0,
            'famous'=>0,
            'views'=>131,
            'show'=>0,
            'date'=>'2019-03-31 22:00:00',
            'active'=>'1',
            'user'=>0,
            'created_at'=>'2020-05-15 01:20:06',
            'updated_at'=>'2020-06-21 00:08:23',
            'deleted_at'=>NULL
            ] );
            
            
                        
            Post::create( [
            'id'=>5059,
            'cat_id'=>9,
            'tag_id'=>5,
            'title'=>'Paracetamol may increase stroke risk in people with diabetes top',
            'text'=>'Wiley Online Library',
            'img'=>'file-1463591644-ZGN2DA.jpg',
            'file'=>NULL,
            'link'=>'https://onlinelibrary.wiley.com/doi/full/10.1111/jgs.15861',
            'by'=>0,
            'journal'=>'Philippe Girard MD  Sandrine Sourdet MD  Christelle Cantet MSc  Philipe de Souto Barreto PhD  Yves Rolland PhD',
            'type'=>0,
            'month_article'=>0,
            'famous'=>0,
            'views'=>131,
            'show'=>0,
            'date'=>'2019-03-31 22:00:00',
            'active'=>'1',
            'user'=>0,
            'created_at'=>'2020-05-15 01:20:06',
            'updated_at'=>'2020-06-21 00:08:23',
            'deleted_at'=>NULL
            ] );
            
            
                        
            Post::create( [
            'id'=>5061,
            'cat_id'=>8,
            'tag_id'=>11,
            'title'=>'Updated EULAR guidelines on systemic lupus erythematosus',
            'text'=>'Monday 08 April 2019',
            'img'=>'file-1464165480-R231XR.jpg',
            'file'=>NULL,
            'link'=>'https://www.univadis.md/medical-news/596/Updated-EULAR-guidelines-on-systemic-lupus-erythematosus?utm_source=newsletter+email&utm_medium=email&utm_campaign=medical+updates+-+weekly&utm_content=3250152&utm_term=automated_weekly',
            'by'=>0,
            'journal'=>'Univadis Medical News',
            'type'=>0,
            'month_article'=>0,
            'famous'=>0,
            'views'=>118,
            'show'=>0,
            'date'=>'2019-04-13 22:00:00',
            'active'=>'1',
            'user'=>0,
            'created_at'=>'2020-05-15 01:20:06',
            'updated_at'=>'2020-06-21 00:08:23',
            'deleted_at'=>NULL
            ] );
            
            
                        
            Post::create( [
            'id'=>5062,
            'cat_id'=>12,
            'tag_id'=>11,
            'title'=>'2019 update of the EULAR recommendations for the management of systemic lupus erythematosus',
            'text'=>'Annals of Rheumatological Diseases',
            'img'=>'file-1464165463-D3CBM9.jpg',
            'file'=>NULL,
            'link'=>'https://ard.bmj.com/content/early/2019/03/28/annrheumdis-2019-215089',
            'by'=>0,
            'journal'=>'Antonis Fanouriakis and others',
            'type'=>0,
            'month_article'=>0,
            'famous'=>0,
            'views'=>229,
            'show'=>0,
            'date'=>'2019-04-13 22:00:00',
            'active'=>'1',
            'user'=>0,
            'created_at'=>'2020-05-15 01:20:06',
            'updated_at'=>'2020-06-21 00:08:23',
            'deleted_at'=>NULL
            ] );
            
            
                        
            Post::create( [
            'id'=>5063,
            'cat_id'=>8,
            'tag_id'=>7,
            'title'=>'Early or Delayed Cardioversion in Recent-Onset Atrial Fibrillation',
            'text'=>'N Engl J Med 2019 380:1499-1508, April 18, 2019',
            'img'=>'file-1463591639-4YG8JY.jpg',
            'file'=>NULL,
            'link'=>'https://www.nejm.org/doi/full/10.1056/NEJMoa1900353?query=featured_home',
            'by'=>0,
            'journal'=>'Nikki A.H.A. Pluymaekers and others, for the RACE 7 ACWAS Investigators',
            'type'=>0,
            'month_article'=>0,
            'famous'=>1,
            'views'=>213,
            'show'=>0,
            'date'=>'2019-04-20 22:00:00',
            'active'=>'1',
            'user'=>0,
            'created_at'=>'2020-05-15 01:20:06',
            'updated_at'=>'2020-06-21 00:08:23',
            'deleted_at'=>NULL
            ] );
            
            
                        
            Post::create( [
            'id'=>5064,
            'cat_id'=>12,
            'tag_id'=>7,
            'title'=>'Antithrombotic Therapy after Acute Coronary Syndrome or PCI in Atrial Fibrillation',
            'text'=>'N Engl J Med 2019 380:1509-1524. April 18, 2019',
            'img'=>'file-1463591644-ZGN2DA.jpg',
            'file'=>NULL,
            'link'=>'https://www.nejm.org/doi/full/10.1056/NEJMoa1817083?query=featured_home',
            'by'=>0,
            'journal'=>'Renato D. Lopes and others, for the AUGUSTUS Investigators',
            'type'=>0,
            'month_article'=>0,
            'famous'=>1,
            'views'=>324,
            'show'=>0,
            'date'=>'2019-04-20 22:00:00',
            'active'=>'1',
            'user'=>0,
            'created_at'=>'2020-05-15 01:20:06',
            'updated_at'=>'2020-06-21 00:08:23',
            'deleted_at'=>NULL
            ] );
            
            
                        
            Post::create( [
            'id'=>5065,
            'cat_id'=>9,
            'tag_id'=>6,
            'title'=>'Therapy for Pulmonary Arterial Hypertension in Adults 2018: Update of the CHEST Guideline and Expert Panel Report Online First: January 2019',
            'text'=>'Chest, March 2019Volume 155, Issue 3, Pages 565–586',
            'img'=>'file-1463591752-XK911T.jpg',
            'file'=>NULL,
            'link'=>'https://journal.chestnet.org/article/S0012-3692(19)30002-9/fulltext',
            'by'=>0,
            'journal'=>'James R. Klinger and others',
            'type'=>0,
            'month_article'=>0,
            'famous'=>0,
            'views'=>118,
            'show'=>0,
            'date'=>'2019-03-23 22:00:00',
            'active'=>'1',
            'user'=>0,
            'created_at'=>'2020-05-15 01:20:06',
            'updated_at'=>'2020-06-21 00:08:23',
            'deleted_at'=>NULL
            ] );
            
            
                        
            Post::create( [
            'id'=>5066,
            'cat_id'=>9,
            'tag_id'=>7,
            'title'=>'Antithrombotic Therapy for Atrial Fibrillation: CHEST Guideline and Expert Panel Report Online First: August 2018',
            'text'=>'Chest, November 2018Volume 154, Issue 5, Pages 1121–1201',
            'img'=>'file-1463591639-4YG8JY.jpg',
            'file'=>NULL,
            'link'=>'https://journal.chestnet.org/article/S0012-3692(18)32244-X/fulltext',
            'by'=>0,
            'journal'=>'Gregory Y.H. Lip, and others',
            'type'=>0,
            'month_article'=>0,
            'famous'=>0,
            'views'=>118,
            'show'=>0,
            'date'=>'2018-11-23 22:00:00',
            'active'=>'1',
            'user'=>0,
            'created_at'=>'2020-05-15 01:20:06',
            'updated_at'=>'2020-06-21 00:08:23',
            'deleted_at'=>NULL
            ] );
            
            
                        
            Post::create( [
            'id'=>5067,
            'cat_id'=>9,
            'tag_id'=>10,
            'title'=>'2018 American Society of Haematology ASH Clinical Practice Guidelines on Venous Thromboembolism',
            'text'=>'',
            'img'=>'file-1463591681-AGRM7H.jpg',
            'file'=>NULL,
            'link'=>'https://www.hematology.org/VTE/',
            'by'=>0,
            'journal'=>'Site Admin',
            'type'=>0,
            'month_article'=>0,
            'famous'=>0,
            'views'=>118,
            'show'=>0,
            'date'=>'2019-01-15 22:00:00',
            'active'=>'1',
            'user'=>0,
            'created_at'=>'2020-05-15 01:20:06',
            'updated_at'=>'2020-06-21 00:08:23',
            'deleted_at'=>NULL
            ] );
            
            
                        
            Post::create( [
            'id'=>5068,
            'cat_id'=>12,
            'tag_id'=>1,
            'title'=>'The Effect of Vitamin C on Clinical Outcome in Critically Ill Patients: A Systematic Review With Meta-Analysis of Randomized Controlled Trials',
            'text'=>'Critical Care Medicine. 476:774-783, June 2019',
            'img'=>'file-1463591725-9J88SF.jpg',
            'file'=>NULL,
            'link'=>'https://journals.lww.com/ccmjournal/Abstract/2019/06000/The_Effect_of_Vitamin_C_on_Clinical_Outcome_in.5.aspx',
            'by'=>0,
            'journal'=>'Putzu, Alessandro and others',
            'type'=>0,
            'month_article'=>0,
            'famous'=>0,
            'views'=>147,
            'show'=>0,
            'date'=>'2019-06-05 22:00:00',
            'active'=>'1',
            'user'=>0,
            'created_at'=>'2020-05-15 01:20:06',
            'updated_at'=>'2020-06-21 00:08:23',
            'deleted_at'=>NULL
            ] );
            
            
                        
            Post::create( [
            'id'=>5070,
            'cat_id'=>8,
            'tag_id'=>6,
            'title'=>'Respiratory Mechanics, Lung Recruitability, and Gas Exchange in Pulmonary and Extrapulmonary Acute Respiratory Distress Syndrome',
            'text'=>'Critical Care Medicine. 476:792-799, June 2019',
            'img'=>'file-1463591639-4YG8JY.jpg',
            'file'=>NULL,
            'link'=>'https://journals.lww.com/ccmjournal/Abstract/2019/06000/Respiratory_Mechanics,_Lung_Recruitability,_and.7.aspx',
            'by'=>0,
            'journal'=>'Coppola, Silvia and others',
            'type'=>0,
            'month_article'=>0,
            'famous'=>0,
            'views'=>118,
            'show'=>0,
            'date'=>'2019-05-31 22:00:00',
            'active'=>'1',
            'user'=>0,
            'created_at'=>'2020-05-15 01:20:06',
            'updated_at'=>'2020-06-21 00:08:23',
            'deleted_at'=>NULL
            ] );
            
            
                        
            Post::create( [
            'id'=>5071,
            'cat_id'=>8,
            'tag_id'=>10,
            'title'=>'Thromboelastography Predicts Thromboembolism in Critically Ill Coagulopathic Patients',
            'text'=>'Critical Care Medicine. 476:826-832, June 2019',
            'img'=>'file-1463591685-DDLMK9.jpg',
            'file'=>NULL,
            'link'=>'https://journals.lww.com/ccmjournal/Abstract/2019/06000/Thromboelastography_Predicts_Thromboembolism_in.11.aspx',
            'by'=>0,
            'journal'=>'Harahsheh, Yusrah Duff, Oonagh C. Ho, Kwok M',
            'type'=>0,
            'month_article'=>0,
            'famous'=>0,
            'views'=>118,
            'show'=>0,
            'date'=>'2019-05-31 22:00:00',
            'active'=>'1',
            'user'=>0,
            'created_at'=>'2020-05-15 01:20:06',
            'updated_at'=>'2020-06-21 00:08:23',
            'deleted_at'=>NULL
            ] );
            
            
                        
            Post::create( [
            'id'=>5072,
            'cat_id'=>8,
            'tag_id'=>10,
            'title'=>'TWICE VERSUS THRICE DAILY HEPARIN FOR VENOUS THROMBOEMBOLISM PROPHYLAXIS IN CRITICALLY ILL PATIENTS',
            'text'=>'CRITICAL CARE MEDICINE, January 2019',
            'img'=>'file-1463591681-AGRM7H.jpg',
            'file'=>NULL,
            'link'=>'https://journals.lww.com/ccmjournal/Citation/2019/01001/502__TWICE_VERSUS_THRICE_DAILY_HEPARIN_FOR_VENOUS.465.aspx',
            'by'=>0,
            'journal'=>'Davis, Nicole Altshuler, Jerry Paris, Daryl Musallam, Nadine',
            'type'=>0,
            'month_article'=>0,
            'famous'=>1,
            'views'=>213,
            'show'=>0,
            'date'=>'2018-12-31 22:00:00',
            'active'=>'1',
            'user'=>0,
            'created_at'=>'2020-05-15 01:20:06',
            'updated_at'=>'2020-06-21 00:08:23',
            'deleted_at'=>NULL
            ] );
            
            
                        
            Post::create( [
            'id'=>5073,
            'cat_id'=>8,
            'tag_id'=>11,
            'title'=>'Trying to identify who may benefit most from future vitamin D intervention trials: a post hoc analysis from the VITDAL-ICU study excluding the early deaths',
            'text'=>'Critical Care 2019, 23:200  Published on: 4 June 2019',
            'img'=>'file-1463591732-SM39NY.jpg',
            'file'=>NULL,
            'link'=>'https://ccforum.biomedcentral.com/articles/10.1186/s13054-019-2472-z',
            'by'=>0,
            'journal'=>'Gennaro Martucci and others',
            'type'=>0,
            'month_article'=>0,
            'famous'=>0,
            'views'=>118,
            'show'=>0,
            'date'=>'2019-06-01 22:00:00',
            'active'=>'1',
            'user'=>0,
            'created_at'=>'2020-05-15 01:20:06',
            'updated_at'=>'2020-06-21 00:08:23',
            'deleted_at'=>NULL
            ] );
            
            
                        
            Post::create( [
            'id'=>5074,
            'cat_id'=>9,
            'tag_id'=>4,
            'title'=>'The European guideline on management of major bleeding and coagulopathy following trauma: fifth edition',
            'text'=>'Critical Care 2019 23:98. March 27,  2019',
            'img'=>'file-1463591681-AGRM7H.jpg',
            'file'=>NULL,
            'link'=>'https://ccforum.biomedcentral.com/articles/10.1186/s13054-019-2347-3',
            'by'=>0,
            'journal'=>'Donat R. Spahn, et al.',
            'type'=>0,
            'month_article'=>0,
            'famous'=>0,
            'views'=>215,
            'show'=>0,
            'date'=>'2019-03-26 22:00:00',
            'active'=>'1',
            'user'=>0,
            'created_at'=>'2020-05-15 01:20:06',
            'updated_at'=>'2020-06-21 00:08:23',
            'deleted_at'=>NULL
            ] );
            
            
                        
            Post::create( [
            'id'=>5075,
            'cat_id'=>8,
            'tag_id'=>11,
            'title'=>'Staged Implementation of Awakening and Breathing, Coordination, Delirium Monitoring and Management, and Early Mobilization Bundle Improves Patient Outcomes and Reduces Hospital Costs',
            'text'=>'Critical Care Medicine. 477:885-893, July 2019',
            'img'=>'file-1464165480-R231XR.jpg',
            'file'=>NULL,
            'link'=>'https://journals.lww.com/ccmjournal/Fulltext/2019/07000/Staged_Implementation_of_Awakening_and_Breathing,.1.aspx',
            'by'=>0,
            'journal'=>'Hsieh, S. Jean and others',
            'type'=>0,
            'month_article'=>0,
            'famous'=>0,
            'views'=>118,
            'show'=>0,
            'date'=>'2019-06-30 22:00:00',
            'active'=>'1',
            'user'=>0,
            'created_at'=>'2020-05-15 01:20:06',
            'updated_at'=>'2020-06-21 00:08:23',
            'deleted_at'=>NULL
            ] );
            
            
                        
            Post::create( [
            'id'=>5076,
            'cat_id'=>8,
            'tag_id'=>11,
            'title'=>'Sleep and Work in ICU Physicians During a Randomized Trial of Nighttime Intensivist Staffing',
            'text'=>'Critical Care Medicine. 477:894-902, July 2019',
            'img'=>'file-1463591639-4YG8JY.jpg',
            'file'=>NULL,
            'link'=>'https://journals.lww.com/ccmjournal/Fulltext/2019/07000/Sleep_and_Work_in_ICU_Physicians_During_a.2.aspx',
            'by'=>0,
            'journal'=>'Bakhru, Rita N and others',
            'type'=>0,
            'month_article'=>0,
            'famous'=>0,
            'views'=>118,
            'show'=>0,
            'date'=>'2019-06-30 22:00:00',
            'active'=>'1',
            'user'=>0,
            'created_at'=>'2020-05-15 01:20:06',
            'updated_at'=>'2020-06-21 00:08:23',
            'deleted_at'=>NULL
            ] );
            
            
                        
            Post::create( [
            'id'=>5077,
            'cat_id'=>8,
            'tag_id'=>11,
            'title'=>'Association Between Mean Arterial Pressure and Acute Kidney Injury and a Composite of Myocardial Injury and Mortality in Postoperative Critically Ill Patients: A Retrospective Cohort Analysis',
            'text'=>'Critical Care Medicine. 477:910-917, July 2019',
            'img'=>'file-1463591738-IZ3XFW.jpg',
            'file'=>NULL,
            'link'=>'https://journals.lww.com/ccmjournal/Abstract/2019/07000/Association_Between_Mean_Arterial_Pressure_and.4.aspx',
            'by'=>0,
            'journal'=>'Khanna, Ashish K and others',
            'type'=>0,
            'month_article'=>0,
            'famous'=>0,
            'views'=>118,
            'show'=>0,
            'date'=>'2019-06-30 22:00:00',
            'active'=>'1',
            'user'=>0,
            'created_at'=>'2020-05-15 01:20:06',
            'updated_at'=>'2020-06-21 00:08:23',
            'deleted_at'=>NULL
            ] );
            
            
                        
            Post::create( [
            'id'=>5078,
            'cat_id'=>8,
            'tag_id'=>11,
            'title'=>'Novel Risk Factors for Posttraumatic Stress Disorder Symptoms in Family Members of Acute Respiratory Distress Syndrome Survivors',
            'text'=>'Critical Care Medicine. 477:934-941, July 2019',
            'img'=>'file-1464165463-D3CBM9.jpg',
            'file'=>NULL,
            'link'=>'https://journals.lww.com/ccmjournal/Abstract/2019/07000/Novel_Risk_Factors_for_Posttraumatic_Stress.7.aspx',
            'by'=>0,
            'journal'=>'Lee, Robert Y and others',
            'type'=>0,
            'month_article'=>0,
            'famous'=>0,
            'views'=>118,
            'show'=>0,
            'date'=>'2019-06-30 22:00:00',
            'active'=>'1',
            'user'=>0,
            'created_at'=>'2020-05-15 01:20:06',
            'updated_at'=>'2020-06-21 00:08:23',
            'deleted_at'=>NULL
            ] );
            
            
                        
            Post::create( [
            'id'=>5079,
            'cat_id'=>8,
            'tag_id'=>11,
            'title'=>'The Restrictive IV Fluid Trial in Severe Sepsis and Septic Shock RIFTS: A Randomized Pilot Study',
            'text'=>'Critical Care Medicine. 477:951-959, July 2019',
            'img'=>'file-1463591644-ZGN2DA.jpg',
            'file'=>NULL,
            'link'=>'https://journals.lww.com/ccmjournal/Fulltext/2019/07000/The_Restrictive_IV_Fluid_Trial_in_Severe_Sepsis.9.aspx',
            'by'=>0,
            'journal'=>'Corl, Keith A and others',
            'type'=>0,
            'month_article'=>0,
            'famous'=>0,
            'views'=>118,
            'show'=>0,
            'date'=>'2019-07-01 22:00:00',
            'active'=>'1',
            'user'=>0,
            'created_at'=>'2020-05-15 01:20:06',
            'updated_at'=>'2020-06-21 00:08:23',
            'deleted_at'=>NULL
            ] );
            
            
                        
            Post::create( [
            'id'=>5080,
            'cat_id'=>8,
            'tag_id'=>11,
            'title'=>'The Burden of Brain Hypoxia and Optimal Mean Arterial Pressure in Patients With Hypoxic Ischemic Brain Injury After Cardiac Arrest',
            'text'=>'Critical Care Medicine. 477:960-969, July 2019',
            'img'=>'im1.jpg',
            'file'=>NULL,
            'link'=>'https://journals.lww.com/ccmjournal/Abstract/2019/07000/The_Burden_of_Brain_Hypoxia_and_Optimal_Mean.10.aspx',
            'by'=>0,
            'journal'=>'Sekhon, Mypinder S and others',
            'type'=>0,
            'month_article'=>0,
            'famous'=>0,
            'views'=>118,
            'show'=>0,
            'date'=>'2019-06-30 22:00:00',
            'active'=>'1',
            'user'=>0,
            'created_at'=>'2020-05-15 01:20:06',
            'updated_at'=>'2020-06-21 00:08:23',
            'deleted_at'=>NULL
            ] );
            
            
                        
            Post::create( [
            'id'=>5081,
            'cat_id'=>8,
            'tag_id'=>5,
            'title'=>'The Clinical Spectrum of New-Onset Status Epilepticus',
            'text'=>'Critical Care Medicine. 477:970-974, July 2019',
            'img'=>'im2.jpg',
            'file'=>NULL,
            'link'=>'https://journals.lww.com/ccmjournal/Abstract/2019/07000/The_Clinical_Spectrum_of_New_Onset_Status.11.aspx',
            'by'=>0,
            'journal'=>'Chakraborty, Tia Hocker, Sara',
            'type'=>0,
            'month_article'=>0,
            'famous'=>0,
            'views'=>131,
            'show'=>0,
            'date'=>'2019-06-30 22:00:00',
            'active'=>'1',
            'user'=>0,
            'created_at'=>'2020-05-15 01:20:06',
            'updated_at'=>'2020-06-21 00:08:23',
            'deleted_at'=>NULL
            ] );
            
            
                        
            Post::create( [
            'id'=>5082,
            'cat_id'=>8,
            'tag_id'=>1,
            'title'=>'Study shows catheters account for 25% of hospital infections',
            'text'=>'July 1 UPI',
            'img'=>'file-1464106363-HWQX6B.jpg',
            'file'=>NULL,
            'link'=>'https://www.upi.com/Health_News/2019/07/01/Study-Catheters-cause-25-percent-of-hospital-infections/5871562007191/',
            'by'=>0,
            'journal'=>'Tauren Dyson',
            'type'=>0,
            'month_article'=>0,
            'famous'=>0,
            'views'=>147,
            'show'=>0,
            'date'=>'2019-06-30 22:00:00',
            'active'=>'1',
            'user'=>0,
            'created_at'=>'2020-05-15 01:20:06',
            'updated_at'=>'2020-06-21 00:08:23',
            'deleted_at'=>NULL
            ] );
            
            
                        
            Post::create( [
            'id'=>5083,
            'cat_id'=>8,
            'tag_id'=>10,
            'title'=>'Coagulopathy Following Major Trauma: Review the Revised European Guidelines',
            'text'=>'Medscape Critical Care',
            'img'=>'file-1463591639-4YG8JY.jpg',
            'file'=>NULL,
            'link'=>'https://www.medscape.org/viewarticle/914810srcmkmcmr_driv_outcomes_mscpedu&impID2014381',
            'by'=>0,
            'journal'=>'Drs. Rossaint, Maegele, and Spahn',
            'type'=>0,
            'month_article'=>0,
            'famous'=>1,
            'views'=>213,
            'show'=>0,
            'date'=>'2019-06-30 22:00:00',
            'active'=>'1',
            'user'=>0,
            'created_at'=>'2020-05-15 01:20:06',
            'updated_at'=>'2020-06-21 00:08:23',
            'deleted_at'=>NULL
            ] );
            
            
                        
            Post::create( [
            'id'=>5084,
            'cat_id'=>8,
            'tag_id'=>4,
            'title'=>'Through the Looking Glass: Factors Predicting Outcomes In Trauma Patients',
            'text'=>'Medscape Critical Care',
            'img'=>'im3.jpg',
            'file'=>NULL,
            'link'=>'https://www.medscape.org/viewarticle/914769?src=mkmcmr_driv_stan_mscpedu&uac=102122PR&impID=2014204',
            'by'=>0,
            'journal'=>'Jerrold Levy, MD, FAHA, FCCM Dietmar Fries, MD Marc Maegele',
            'type'=>0,
            'month_article'=>0,
            'famous'=>1,
            'views'=>310,
            'show'=>0,
            'date'=>'2019-06-30 22:00:00',
            'active'=>'1',
            'user'=>0,
            'created_at'=>'2020-05-15 01:20:06',
            'updated_at'=>'2020-06-21 00:08:23',
            'deleted_at'=>NULL
            ] );
            
            
                        
            Post::create( [
            'id'=>5085,
            'cat_id'=>8,
            'tag_id'=>11,
            'title'=>'Endocrinologists challenge new treatment guidelines for sub-clinical hypothyroidism',
            'text'=>'',
            'img'=>'file-1463591639-4YG8JY.jpg',
            'file'=>NULL,
            'link'=>'https://www.univadis.md/medical-news/596/Endocrinologists-challenge-new-treatment-guidelines-for-sub-clinical-hypothyroidismutm_sourcenewsletter%20email&utm_mediumemail&utm_campaignmedical%20updates%20best%20of%20-%20month&utm_content3518661&utm_termautomated_bestof_monthly',
            'by'=>0,
            'journal'=>'Univadis Medical News',
            'type'=>0,
            'month_article'=>0,
            'famous'=>0,
            'views'=>118,
            'show'=>0,
            'date'=>'2019-06-30 22:00:00',
            'active'=>'1',
            'user'=>0,
            'created_at'=>'2020-05-15 01:20:06',
            'updated_at'=>'2020-06-21 00:08:23',
            'deleted_at'=>NULL
            ] );
            
            
                        
            Post::create( [
            'id'=>5086,
            'cat_id'=>8,
            'tag_id'=>1,
            'title'=>'FDA approves new treatment for hospital-acquired, ventilator-associated bacterial pneumonia',
            'text'=>'MDedge News',
            'img'=>'file-1464165463-D3CBM9.jpg',
            'file'=>NULL,
            'link'=>'https://www.mdedge.com/chestphysician/article/202251/critical-care/fda-approves-new-treatment-hospital-acquired-ventilator?utm_campaign=NewsBrief%20-%20Unengaged&utm_source=hs_email&utm_medium=email&utm_content=74178322&_hsenc=p2ANqtz-_PAaHSD5-UfgKkgxadrncxOmk6vPOJfj_KnoWIOO1JP7fPlORHlOwN8sCjekPYgOyBc4IMQorWLXUcIMYVw8qeIomiJg&_hsmi=74178951',
            'by'=>0,
            'journal'=>'Lucas Franki',
            'type'=>0,
            'month_article'=>0,
            'famous'=>0,
            'views'=>147,
            'show'=>0,
            'date'=>'2019-07-01 22:00:00',
            'active'=>'1',
            'user'=>0,
            'created_at'=>'2020-05-15 01:20:06',
            'updated_at'=>'2020-06-21 00:08:23',
            'deleted_at'=>NULL
            ] );
            
            
                        
            Post::create( [
            'id'=>5087,
            'cat_id'=>9,
            'tag_id'=>3,
            'title'=>'ESPEN guideline on clinical nutrition in the intensive care unit 2019',
            'text'=>'',
            'img'=>'file-1463591725-9J88SF.jpg',
            'file'=>NULL,
            'link'=>'https://www.espen.org/files/ESPEN-Guidelines/ESPEN_guideline-on-clinical-nutrition-in-the-intensive-care-unit.pdf',
            'by'=>0,
            'journal'=>'P. Singer et al. / Clinical Nutrition 38 2019 48e79',
            'type'=>0,
            'month_article'=>0,
            'famous'=>0,
            'views'=>118,
            'show'=>0,
            'date'=>'2019-01-24 22:00:00',
            'active'=>'1',
            'user'=>0,
            'created_at'=>'2020-05-15 01:20:06',
            'updated_at'=>'2020-06-21 00:08:23',
            'deleted_at'=>NULL
            ] );
            
            
                        
            Post::create( [
            'id'=>5088,
            'cat_id'=>8,
            'tag_id'=>1,
            'title'=>'Vitamin A deficiency in critically ill children with sepsis',
            'text'=>'Critical Care 2019, 23:267  Published on: 1 August 2019',
            'img'=>'file-1464165509-QNPJY8.jpg',
            'file'=>NULL,
            'link'=>'https://ccforum.biomedcentral.com/articles/10.1186/s13054-019-2548-9',
            'by'=>0,
            'journal'=>'Xuepeng Zhang, Kaiying Yang, Linwen Chen, Xuelian Liao, Liping Deng, Siyuan Chen and Yi Ji',
            'type'=>0,
            'month_article'=>0,
            'famous'=>0,
            'views'=>147,
            'show'=>0,
            'date'=>'2019-07-31 22:00:00',
            'active'=>'1',
            'user'=>0,
            'created_at'=>'2020-05-15 01:20:06',
            'updated_at'=>'2020-06-21 00:08:23',
            'deleted_at'=>NULL
            ] );
            
            
                        
            Post::create( [
            'id'=>5089,
            'cat_id'=>12,
            'tag_id'=>11,
            'title'=>'Thrombolysis Guided by Perfusion Imaging up to 9 Hours after Onset of Stroke',
            'text'=>'N Engl J Med 2019 380:1795-1803',
            'img'=>'file-1463591657-K8BRDF.jpg',
            'file'=>NULL,
            'link'=>'https://www.nejm.org/doi/full/10.1056/NEJMoa1813046?query=recirc_curatedRelated_article',
            'by'=>0,
            'journal'=>'Henry Ma, , et al., for the EXTEND Investigators',
            'type'=>0,
            'month_article'=>0,
            'famous'=>1,
            'views'=>324,
            'show'=>0,
            'date'=>'2019-05-08 22:00:00',
            'active'=>'1',
            'user'=>0,
            'created_at'=>'2020-05-15 01:20:06',
            'updated_at'=>'2020-06-21 00:08:23',
            'deleted_at'=>NULL
            ] );
            
            
                        
            Post::create( [
            'id'=>5090,
            'cat_id'=>8,
            'tag_id'=>7,
            'title'=>'Limb ischemia in peripheral veno-arterial extracorporeal membrane oxygenation: a narrative review of incidence, prevention, monitoring, and treatment',
            'text'=>'Critical Care 2019, 23:266',
            'img'=>'file-1463591644-ZGN2DA.jpg',
            'file'=>NULL,
            'link'=>'https://ccforum.biomedcentral.com/articles/10.1186/s13054-019-2541-3',
            'by'=>0,
            'journal'=>'Eleonora Bonicolini, Gennaro Martucci, Jorik Simons, Giuseppe M. Raffa, Cristina Spina, Valeria Lo Coco, Antonio Arcadipane, Michele Pilato and Roberto Lorusso',
            'type'=>0,
            'month_article'=>0,
            'famous'=>0,
            'views'=>118,
            'show'=>0,
            'date'=>'2019-07-31 22:00:00',
            'active'=>'1',
            'user'=>0,
            'created_at'=>'2020-05-15 01:20:06',
            'updated_at'=>'2020-06-21 00:08:23',
            'deleted_at'=>NULL
            ] );
            
            
                        
            Post::create( [
            'id'=>5091,
            'cat_id'=>8,
            'tag_id'=>3,
            'title'=>'Early Enteral Nutrition in Patients Undergoing Sustained Neuromuscular Blockade: A Propensity-Matched Analysis Using a Nationwide Inpatient Database',
            'text'=>'Critical Care Medicine. 478:1072-1080, August 2019',
            'img'=>'file-1463591725-9J88SF.jpg',
            'file'=>NULL,
            'link'=>'https://journals.lww.com/ccmjournal/Abstract/2019/08000/Early_Enteral_Nutrition_in_Patients_Undergoing.9.aspx',
            'by'=>0,
            'journal'=>'Ohbe, Hiroyuki Jo and others',
            'type'=>0,
            'month_article'=>0,
            'famous'=>0,
            'views'=>118,
            'show'=>0,
            'date'=>'2019-08-01 22:00:00',
            'active'=>'1',
            'user'=>0,
            'created_at'=>'2020-05-15 01:20:06',
            'updated_at'=>'2020-06-21 00:08:23',
            'deleted_at'=>NULL
            ] );
            
            
                        
            Post::create( [
            'id'=>5092,
            'cat_id'=>8,
            'tag_id'=>7,
            'title'=>'Extracorporeal Membrane Oxygenation for Septic Shock',
            'text'=>'Critical Care Medicine. 478:1097-1105, August 2019',
            'img'=>'file-1463591639-4YG8JY.jpg',
            'file'=>NULL,
            'link'=>'https://journals.lww.com/ccmjournal/Fulltext/2019/08000/Extracorporeal_Membrane_Oxygenation_for_Septic.12.aspx',
            'by'=>0,
            'journal'=>'Falk, Lars Hultman, Jan Broman, Lars Mikael',
            'type'=>0,
            'month_article'=>0,
            'famous'=>0,
            'views'=>118,
            'show'=>0,
            'date'=>'2019-08-01 22:00:00',
            'active'=>'1',
            'user'=>0,
            'created_at'=>'2020-05-15 01:20:06',
            'updated_at'=>'2020-06-21 00:08:23',
            'deleted_at'=>NULL
            ] );
            
            
                        
            Post::create( [
            'id'=>5093,
            'cat_id'=>8,
            'tag_id'=>10,
            'title'=>'Interventions to prevent iatrogenic anemia: a Laboratory Medicine Best Practices systematic review',
            'text'=>'Critical Care 2019, 23:278  Published on: 9 August 2019',
            'img'=>'file-1463591685-DDLMK9.jpg',
            'file'=>NULL,
            'link'=>'https://ccforum.biomedcentral.com/articles/10.1186/s13054-019-2511-9',
            'by'=>0,
            'journal'=>'Nedra S. Whitehead and others',
            'type'=>0,
            'month_article'=>0,
            'famous'=>0,
            'views'=>118,
            'show'=>0,
            'date'=>'2019-08-08 22:00:00',
            'active'=>'1',
            'user'=>0,
            'created_at'=>'2020-05-15 01:20:06',
            'updated_at'=>'2020-06-21 00:08:23',
            'deleted_at'=>NULL
            ] );
            
            
                        
            Post::create( [
            'id'=>5094,
            'cat_id'=>8,
            'tag_id'=>10,
            'title'=>'Thrombomodulin in disseminated intravascular coagulation and other critical conditions—a multi-faceted anticoagulant protein with therapeutic potential',
            'text'=>'Critical Care 2019 23:280. Published on: 15 August 2019',
            'img'=>'file-1463591681-AGRM7H.jpg',
            'file'=>NULL,
            'link'=>NULL,
            'by'=>0,
            'journal'=>'Takashi Ito, Jecko Thachil, Hidesaku Asakura, Jerrold H. Levy and Toshiaki Iba',
            'type'=>0,
            'month_article'=>0,
            'famous'=>0,
            'views'=>118,
            'show'=>0,
            'date'=>'2019-08-14 22:00:00',
            'active'=>'1',
            'user'=>0,
            'created_at'=>'2020-05-15 01:20:06',
            'updated_at'=>'2020-06-21 00:08:23',
            'deleted_at'=>NULL
            ] );
            
            
                        
            Post::create( [
            'id'=>5095,
            'cat_id'=>8,
            'tag_id'=>1,
            'title'=>'Combined assessment of ΔPCT and ΔCRP could increase the ability to differentiate candidemia from bacteremia',
            'text'=>'Critical Care 2019 23:271. Published on: 5 August 2019',
            'img'=>'file-1464106363-HWQX6B.jpg',
            'file'=>NULL,
            'link'=>'https://ccforum.biomedcentral.com/articles/10.1186/s13054-019-2557-8',
            'by'=>0,
            'journal'=>'Qin Wu, Hao Yang and Yan Kang',
            'type'=>0,
            'month_article'=>0,
            'famous'=>0,
            'views'=>147,
            'show'=>0,
            'date'=>'2019-08-04 22:00:00',
            'active'=>'1',
            'user'=>0,
            'created_at'=>'2020-05-15 01:20:06',
            'updated_at'=>'2020-06-21 00:08:23',
            'deleted_at'=>NULL
            ] );
            
            
                        
            Post::create( [
            'id'=>5096,
            'cat_id'=>8,
            'tag_id'=>1,
            'title'=>'Procalcitonin levels in candidemia versus bacteremia: a systematic review',
            'text'=>'Critical Carevolume 23, Article number: 190 2019',
            'img'=>'file-1464106363-HWQX6B.jpg',
            'file'=>NULL,
            'link'=>'https://ccforum.biomedcentral.com/articles/10.1186/s13054-019-2481-y',
            'by'=>0,
            'journal'=>'Andrea Cortegiani, Giovanni Misseri, Mariachiara Ippolito, Matteo Bassetti, Antonino Giarratano, Ignacio Martin-Loeches & Sharon Einav',
            'type'=>0,
            'month_article'=>0,
            'famous'=>1,
            'views'=>242,
            'show'=>0,
            'date'=>'2019-05-14 22:00:00',
            'active'=>'1',
            'user'=>0,
            'created_at'=>'2020-05-15 01:20:06',
            'updated_at'=>'2020-06-21 00:08:23',
            'deleted_at'=>NULL
            ] );
            
            
                        
            Post::create( [
            'id'=>5097,
            'cat_id'=>8,
            'tag_id'=>7,
            'title'=>'Veno-venous ECMO indications: more than respiratory support',
            'text'=>'Critical Care 2019 23:275. Published on: 7 August 2019',
            'img'=>'file-1463591644-ZGN2DA.jpg',
            'file'=>NULL,
            'link'=>'https://ccforum.biomedcentral.com/articles/10.1186/s13054-019-2555-x',
            'by'=>0,
            'journal'=>'Aaron Blandino Ortiz, David Cabestrero Alonso and Raúl De Pablo Sánchez',
            'type'=>0,
            'month_article'=>0,
            'famous'=>0,
            'views'=>118,
            'show'=>0,
            'date'=>'2019-08-06 22:00:00',
            'active'=>'1',
            'user'=>0,
            'created_at'=>'2020-05-15 01:20:06',
            'updated_at'=>'2020-06-21 00:08:23',
            'deleted_at'=>NULL
            ] );
            
            
                        
            Post::create( [
            'id'=>5098,
            'cat_id'=>12,
            'tag_id'=>7,
            'title'=>'Extracorporeal gas exchange: when to start and how to end',
            'text'=>'Critical Carevolume 23, Article number: 203 2019',
            'img'=>'file-1463591639-4YG8JY.jpg',
            'file'=>NULL,
            'link'=>'https://ccforum.biomedcentral.com/articles/10.1186/s13054-019-2437-2',
            'by'=>0,
            'journal'=>'L. Gattinoni, F. Vassalli, F. Romitti, F. Vasques, I. Pasticci, E. Duscio & M. Quintel',
            'type'=>0,
            'month_article'=>0,
            'famous'=>0,
            'views'=>229,
            'show'=>0,
            'date'=>'2019-08-14 22:00:00',
            'active'=>'1',
            'user'=>0,
            'created_at'=>'2020-05-15 01:20:06',
            'updated_at'=>'2020-06-21 00:08:23',
            'deleted_at'=>NULL
            ] );
            
            
                        
            Post::create( [
            'id'=>5099,
            'cat_id'=>12,
            'tag_id'=>6,
            'title'=>'The end-expiratory occlusion test: please, let me hold your breath!',
            'text'=>'Critical Carevolume 23, Article number: 274 2019',
            'img'=>'file-1463591657-K8BRDF.jpg',
            'file'=>NULL,
            'link'=>'https://ccforum.biomedcentral.com/articles/10.1186/s13054-019-2554-y',
            'by'=>0,
            'journal'=>'Francesco Gavelli, Jean-Louis Teboul & Xavier Monnet',
            'type'=>0,
            'month_article'=>0,
            'famous'=>0,
            'views'=>229,
            'show'=>0,
            'date'=>'2019-08-14 22:00:00',
            'active'=>'1',
            'user'=>0,
            'created_at'=>'2020-05-15 01:20:06',
            'updated_at'=>'2020-06-21 00:08:23',
            'deleted_at'=>NULL
            ] );
            
            
                        
            Post::create( [
            'id'=>5100,
            'cat_id'=>12,
            'tag_id'=>4,
            'title'=>'Earlier time to hemostasis is associated with decreased mortality and rate of complications: Results from the Pragmatic Randomized Optimal Platelet and Plasma Ratio trial',
            'text'=>'Journal of Trauma and Acute Care Surgery. 872:342-349, August 2019',
            'img'=>'im3.jpg',
            'file'=>NULL,
            'link'=>'https://journals.lww.com/jtrauma/Fulltext/2019/08000/Earlier_time_to_hemostasis_is_associated_with.11.aspx',
            'by'=>0,
            'journal'=>'Chang, Ronald and others',
            'type'=>0,
            'month_article'=>0,
            'famous'=>0,
            'views'=>326,
            'show'=>0,
            'date'=>'2019-08-14 22:00:00',
            'active'=>'1',
            'user'=>0,
            'created_at'=>'2020-05-15 01:20:06',
            'updated_at'=>'2020-06-21 00:08:23',
            'deleted_at'=>NULL
            ] );
            
            
                        
            Post::create( [
            'id'=>5101,
            'cat_id'=>8,
            'tag_id'=>4,
            'title'=>'The pragmatic randomized optimal platelet and plasma ratios trial: what does it mean for remote damage control resuscitation',
            'text'=>'ransfusion. 2016 Apr56 Suppl 2:S149-56',
            'img'=>'file-1463591639-4YG8JY.jpg',
            'file'=>NULL,
            'link'=>'https://www.ncbi.nlm.nih.gov/pubmed/27100751',
            'by'=>0,
            'journal'=>'Yonge JD1, Schreiber MA1.',
            'type'=>0,
            'month_article'=>0,
            'famous'=>1,
            'views'=>310,
            'show'=>0,
            'date'=>'2016-04-05 22:00:00',
            'active'=>'1',
            'user'=>0,
            'created_at'=>'2020-05-15 01:20:06',
            'updated_at'=>'2020-06-21 00:08:23',
            'deleted_at'=>NULL
            ] );
            
            
                        
            Post::create( [
            'id'=>5102,
            'cat_id'=>8,
            'tag_id'=>6,
            'title'=>'Lung Recruitability in Severe Acute Respiratory Distress Syndrome Requiring Extracorporeal Membrane Oxygenation',
            'text'=>'Critical Care Medicine. 479:1177-1183, September 2019',
            'img'=>'file-1463591752-XK911T.jpg',
            'file'=>NULL,
            'link'=>'https://journals.lww.com/ccmjournal/Fulltext/2019/09000/Lung_Recruitability_in_Severe_Acute_Respiratory.2.aspx',
            'by'=>0,
            'journal'=>'Camporota, Luigi and others',
            'type'=>0,
            'month_article'=>0,
            'famous'=>0,
            'views'=>118,
            'show'=>0,
            'date'=>'2019-09-01 22:00:00',
            'active'=>'1',
            'user'=>0,
            'created_at'=>'2020-05-15 01:20:06',
            'updated_at'=>'2020-06-21 00:08:23',
            'deleted_at'=>NULL
            ] );
            
            
                        
            Post::create( [
            'id'=>5103,
            'cat_id'=>8,
            'tag_id'=>11,
            'title'=>'Impact of Telemonitoring of Critically Ill Emergency Department Patients Awaiting ICU Transfer',
            'text'=>'Critical Care Medicine. 479:1201-1207, September 2019',
            'img'=>'file-1463591639-4YG8JY.jpg',
            'file'=>NULL,
            'link'=>'https://journals.lww.com/ccmjournal/Abstract/2019/09000/Impact_of_Telemonitoring_of_Critically_Ill.5.aspx',
            'by'=>0,
            'journal'=>'Kadar, Rachel B and others',
            'type'=>0,
            'month_article'=>0,
            'famous'=>0,
            'views'=>118,
            'show'=>0,
            'date'=>'2019-08-31 22:00:00',
            'active'=>'1',
            'user'=>0,
            'created_at'=>'2020-05-15 01:20:06',
            'updated_at'=>'2020-06-21 00:08:23',
            'deleted_at'=>NULL
            ] );
            
            
                        
            Post::create( [
            'id'=>5104,
            'cat_id'=>8,
            'tag_id'=>7,
            'title'=>'Management of Peripheral Venoarterial Extracorporeal Membrane Oxygenation in Cardiogenic Shock',
            'text'=>'Critical Care Medicine. 479:1235-1242, September 2019',
            'img'=>'im4.jpg',
            'file'=>NULL,
            'link'=>'https://journals.lww.com/ccmjournal/Abstract/2019/09000/Management_of_Peripheral_Venoarterial.10.aspx',
            'by'=>0,
            'journal'=>'Keller, Steven P.',
            'type'=>0,
            'month_article'=>0,
            'famous'=>0,
            'views'=>118,
            'show'=>0,
            'date'=>'2019-09-13 22:00:00',
            'active'=>'1',
            'user'=>0,
            'created_at'=>'2020-05-15 01:20:06',
            'updated_at'=>'2020-06-21 00:08:23',
            'deleted_at'=>NULL
            ] );
            
            
                        
            Post::create( [
            'id'=>5105,
            'cat_id'=>8,
            'tag_id'=>7,
            'title'=>'Comparison of echocardiographic indices of right ventricular systolic function and ejection fraction obtained with continuous thermodilution in critically ill patients',
            'text'=>'Critical Carevolume 23, Article number: 312 2019',
            'img'=>'file-1463591644-ZGN2DA.jpg',
            'file'=>NULL,
            'link'=>'https://ccforum.biomedcentral.com/articles/10.1186/s13054-019-2582-7',
            'by'=>0,
            'journal'=>'Romain Barthélémy, Xavier Roy, Tujia Javanainen, Alexandre Mebazaa and Benjamin Glenn Chousterman',
            'type'=>0,
            'month_article'=>0,
            'famous'=>0,
            'views'=>118,
            'show'=>0,
            'date'=>'2019-09-13 22:00:00',
            'active'=>'1',
            'user'=>0,
            'created_at'=>'2020-05-15 01:20:06',
            'updated_at'=>'2020-06-21 00:08:23',
            'deleted_at'=>NULL
            ] );
            
            
                        
            Post::create( [
            'id'=>5106,
            'cat_id'=>12,
            'tag_id'=>7,
            'title'=>'2019 ESC Guidelines for the diagnosis and management of acute pulmonary embolism',
            'text'=>'European Heart Journal, Published: 31 August 2019',
            'img'=>'file-1463591644-ZGN2DA.jpg',
            'file'=>NULL,
            'link'=>'https://academic.oup.com/eurheartj/advance-article/doi/10.1093/eurheartj/ehz405/5556136',
            'by'=>0,
            'journal'=>'Stavros V Konstantinides and others',
            'type'=>0,
            'month_article'=>0,
            'famous'=>0,
            'views'=>229,
            'show'=>0,
            'date'=>'2019-09-14 22:00:00',
            'active'=>'1',
            'user'=>0,
            'created_at'=>'2020-05-15 01:20:06',
            'updated_at'=>'2020-06-21 00:08:23',
            'deleted_at'=>NULL
            ] );
            
            
                        
            Post::create( [
            'id'=>5107,
            'cat_id'=>8,
            'tag_id'=>1,
            'title'=>'Intravenous fluid resuscitation is associated with septic endothelial glycocalyx degradation',
            'text'=>'Critical Carevolume 23, Article number: 259 2019',
            'img'=>'file-1463591650-J14MMU.jpg',
            'file'=>NULL,
            'link'=>'https://ccforum.biomedcentral.com/articles/10.1186/s13054-019-2534-2?sap-outbound-id=BCEC9BDAE0F87140C80DE3D66C954BA6C679E280&utm_source=hybris-campaign&utm_medium=email&utm_campaign=000_ASQ4576_0000014918_BSCH_CRIC_eTOC_Sept19_RW&utm_content=EN_internal_32958_20190917&mkt-key=005056B0331B1ED782F7E4462FD15FC0',
            'by'=>0,
            'journal'=>'Joseph A. Hippensteel, and others',
            'type'=>0,
            'month_article'=>0,
            'famous'=>0,
            'views'=>147,
            'show'=>0,
            'date'=>'2019-08-31 22:00:00',
            'active'=>'1',
            'user'=>0,
            'created_at'=>'2020-05-15 01:20:06',
            'updated_at'=>'2020-06-21 00:08:23',
            'deleted_at'=>NULL
            ] );
            
            
                        
            Post::create( [
            'id'=>5108,
            'cat_id'=>8,
            'tag_id'=>11,
            'title'=>'Characteristics of Rapid Response Calls in the United States: An Analysis of the First 402,023 Adult Cases From the Get With the Guidelines Resuscitation-Medical Emergency Team Registry',
            'text'=>'Critical Care Medicine. 4710:1283-1289, October 2019',
            'img'=>'file-1464165480-R231XR.jpg',
            'file'=>NULL,
            'link'=>'https://journals.lww.com/ccmjournal/Fulltext/2019/10000/Characteristics_of_Rapid_Response_Calls_in_the.1.aspx',
            'by'=>0,
            'journal'=>'Lyons, Patrick G and others',
            'type'=>0,
            'month_article'=>0,
            'famous'=>0,
            'views'=>118,
            'show'=>0,
            'date'=>'2019-09-30 22:00:00',
            'active'=>'1',
            'user'=>0,
            'created_at'=>'2020-05-15 01:20:06',
            'updated_at'=>'2020-06-21 00:08:23',
            'deleted_at'=>NULL
            ] );
            
            
                        
            Post::create( [
            'id'=>5109,
            'cat_id'=>8,
            'tag_id'=>1,
            'title'=>'Implementation of Sepsis Bundles: Just Do It!!',
            'text'=>'Critical Care Medicine. 4710:1450-1451, October 2019',
            'img'=>'file-1464165463-D3CBM9.jpg',
            'file'=>NULL,
            'link'=>'https://journals.lww.com/ccmjournal/Fulltext/2019/10000/Implementation_of_Sepsis_Bundles__Just_Do_It___.22.aspx',
            'by'=>0,
            'journal'=>'Levy, Mitchell M.',
            'type'=>0,
            'month_article'=>0,
            'famous'=>0,
            'views'=>147,
            'show'=>0,
            'date'=>'2019-10-01 22:00:00',
            'active'=>'1',
            'user'=>0,
            'created_at'=>'2020-05-15 01:20:06',
            'updated_at'=>'2020-06-21 00:08:23',
            'deleted_at'=>NULL
            ] );
            
                        
            Post::create( [
            'id'=>5111,
            'cat_id'=>8,
            'tag_id'=>7,
            'title'=>'Continuation of Newly Initiated Midodrine Therapy: Appropriate in Patients With Heart Failure',
            'text'=>'Critical Care Medicine. 4710:e845, October 2019',
            'img'=>'file-1463591639-4YG8JY.jpg',
            'file'=>NULL,
            'link'=>'https://journals.lww.com/ccmjournal/Citation/2019/10000/Continuation_of_Newly_Initiated_Midodrine_Therapy_.42.aspx',
            'by'=>0,
            'journal'=>'Reed, Brent N. Gale, Stormi E. Ramani, Gautam',
            'type'=>0,
            'month_article'=>0,
            'famous'=>0,
            'views'=>118,
            'show'=>0,
            'date'=>'2019-10-01 22:00:00',
            'active'=>'1',
            'user'=>0,
            'created_at'=>'2020-05-15 01:20:06',
            'updated_at'=>'2020-06-21 00:08:23',
            'deleted_at'=>NULL
            ] );
            
            
                        
            Post::create( [
            'id'=>5112,
            'cat_id'=>8,
            'tag_id'=>10,
            'title'=>'Thromboelastography Predicts Thromboembolism in Critically Ill Coagulopathic Patients: Erratum',
            'text'=>'',
            'img'=>'file-1463591681-AGRM7H.jpg',
            'file'=>NULL,
            'link'=>'https://journals.lww.com/ccmjournal/Citation/2019/10000/Thromboelastography_Predicts_Thromboembolism_in.51.aspx',
            'by'=>0,
            'journal'=>'Critical Care Medicine. 4710:e853, October 2019.',
            'type'=>0,
            'month_article'=>0,
            'famous'=>0,
            'views'=>118,
            'show'=>0,
            'date'=>'2019-09-30 22:00:00',
            'active'=>'1',
            'user'=>0,
            'created_at'=>'2020-05-15 01:20:06',
            'updated_at'=>'2020-06-21 00:08:23',
            'deleted_at'=>NULL
            ] );
            
            
                        
            Post::create( [
            'id'=>5113,
            'cat_id'=>12,
            'tag_id'=>1,
            'title'=>'Effect of Vitamin C Infusion on Organ Failure and Biomarkers of Inflammation and Vascular Injury in Patients With Sepsis and Severe Acute Respiratory Failure',
            'text'=>'Alpha A. Fowler III, and others. JAMA. 201932213:1261-1270',
            'img'=>'file-1463591685-DDLMK9.jpg',
            'file'=>NULL,
            'link'=>'https://jamanetwork.com/journals/jama/fullarticle/2752063',
            'by'=>0,
            'journal'=>'The CITRIS-ALI Randomized Clinical Trial',
            'type'=>0,
            'month_article'=>0,
            'famous'=>0,
            'views'=>147,
            'show'=>0,
            'date'=>'2019-10-01 22:00:00',
            'active'=>'1',
            'user'=>0,
            'created_at'=>'2020-05-15 01:20:06',
            'updated_at'=>'2020-06-21 00:08:23',
            'deleted_at'=>NULL
            ] );
            
            
                        
            Post::create( [
            'id'=>5115,
            'cat_id'=>12,
            'tag_id'=>7,
            'title'=>'Effect of Selepressin vs Placebo on Ventilator- and Vasopressor-Free Days in Patients With Septic Shock',
            'text'=>'Pierre-Francois Laterre and others. JAMA. Published online October 2, 2019',
            'img'=>'file-1463591644-ZGN2DA.jpg',
            'file'=>NULL,
            'link'=>'https://jamanetwork.com/journals/jama/fullarticle/2752580',
            'by'=>0,
            'journal'=>'The SEPSIS-ACT Randomized Clinical Trial',
            'type'=>0,
            'month_article'=>0,
            'famous'=>1,
            'views'=>324,
            'show'=>0,
            'date'=>'2019-10-01 22:00:00',
            'active'=>'1',
            'user'=>0,
            'created_at'=>'2020-05-15 01:20:06',
            'updated_at'=>'2020-06-21 00:08:23',
            'deleted_at'=>NULL
            ] );
            
            
                        
            Post::create( [
            'id'=>5116,
            'cat_id'=>12,
            'tag_id'=>11,
            'title'=>'Effect of Postextubation High-Flow Nasal Oxygen With Noninvasive Ventilation vs High-Flow Nasal Oxygen Alone on Reintubation Among Patients at High Risk of Extubation Failure',
            'text'=>'Arnaud W. Thille and others. JAMA. Published online October 2, 2019',
            'img'=>'im5.jpg',
            'file'=>NULL,
            'link'=>'https://jamanetwork.com/journals/jama/fullarticle/2752582',
            'by'=>0,
            'journal'=>'A Randomized Clinical Trial',
            'type'=>0,
            'month_article'=>0,
            'famous'=>1,
            'views'=>324,
            'show'=>0,
            'date'=>'2019-10-01 22:00:00',
            'active'=>'1',
            'user'=>0,
            'created_at'=>'2020-05-15 01:20:06',
            'updated_at'=>'2020-06-21 00:08:23',
            'deleted_at'=>NULL
            ] );
            
            
                        
            Post::create( [
            'id'=>5117,
            'cat_id'=>8,
            'tag_id'=>2,
            'title'=>'Non-sedation versus sedation with a daily wake-up trial in critically ill patients recieving mechanical ventilation - effects on long-term cognitive function: Study protocol for a randomized controlled trial, a substudy of the NONSEDA trial',
            'text'=>'BMC',
            'img'=>'im1.jpg',
            'file'=>NULL,
            'link'=>'https://trialsjournal.biomedcentral.com/articles/10.1186/s13063-016-1390-5',
            'by'=>0,
            'journal'=>'Helene Korvenius Nedergaard, Hanne Irene Jensen, Mette Stylsvig, Jørgen T. Lauridsen & Palle Toft',
            'type'=>0,
            'month_article'=>0,
            'famous'=>0,
            'views'=>189,
            'show'=>0,
            'date'=>'2019-09-30 22:00:00',
            'active'=>'1',
            'user'=>0,
            'created_at'=>'2020-05-15 01:20:06',
            'updated_at'=>'2020-06-21 00:08:23',
            'deleted_at'=>NULL
            ] );
            
            
                        
            Post::create( [
            'id'=>5118,
            'cat_id'=>8,
            'tag_id'=>11,
            'title'=>'LIVES2019: Findings from the TOPMAST Study',
            'text'=>'Published on: Fri, 4 Oct 2019',
            'img'=>'file-1463591650-J14MMU.jpg',
            'file'=>NULL,
            'link'=>'https://healthmanagement.org/c/icu/news/lives2019-findings-from-the-topmast-study',
            'by'=>0,
            'journal'=>'ICU Management',
            'type'=>0,
            'month_article'=>0,
            'famous'=>0,
            'views'=>118,
            'show'=>0,
            'date'=>'2019-10-03 22:00:00',
            'active'=>'1',
            'user'=>0,
            'created_at'=>'2020-05-15 01:20:06',
            'updated_at'=>'2020-06-21 00:08:23',
            'deleted_at'=>NULL
            ] );
            
            
                        
            Post::create( [
            'id'=>5119,
            'cat_id'=>8,
            'tag_id'=>1,
            'title'=>'LIVES2019: Findings from the DIANA Study',
            'text'=>'Published on: Tue, 1 Oct 2019',
            'img'=>'file-1464106363-HWQX6B.jpg',
            'file'=>NULL,
            'link'=>'https://healthmanagement.org/c/icu/news/lives2019-findings-from-the-diana-study',
            'by'=>0,
            'journal'=>'ICU Management and Practices',
            'type'=>0,
            'month_article'=>0,
            'famous'=>0,
            'views'=>147,
            'show'=>0,
            'date'=>'2019-10-02 22:00:00',
            'active'=>'1',
            'user'=>0,
            'created_at'=>'2020-05-15 01:20:06',
            'updated_at'=>'2020-06-21 00:08:23',
            'deleted_at'=>NULL
            ] );
            
            
                        
            Post::create( [
            'id'=>5120,
            'cat_id'=>8,
            'tag_id'=>5,
            'title'=>'LIVES2019: Should We Treat Fever in Critically Ill Patients Without Acute Brain Pathology',
            'text'=>'Published on: Tue, 1 Oct 2019',
            'img'=>'im2.jpg',
            'file'=>NULL,
            'link'=>'https://healthmanagement.org/c/icu/news/lives2019-findings-from-the-diana-study',
            'by'=>0,
            'journal'=>'ICU Management and Practices',
            'type'=>0,
            'month_article'=>0,
            'famous'=>0,
            'views'=>131,
            'show'=>0,
            'date'=>'2019-10-02 22:00:00',
            'active'=>'1',
            'user'=>0,
            'created_at'=>'2020-05-15 01:20:06',
            'updated_at'=>'2020-06-21 00:08:23',
            'deleted_at'=>NULL
            ] );
            
            
                        
            Post::create( [
            'id'=>5121,
            'cat_id'=>8,
            'tag_id'=>11,
            'title'=>'Procalcitonin levels in candidemia versus bacteremia: a systematic review',
            'text'=>'Critical Care volume 23, May 2019',
            'img'=>'file-1463591639-4YG8JY.jpg',
            'file'=>NULL,
            'link'=>'https://ccforum.biomedcentral.com/articles/10.1186/s13054-019-2481-y',
            'by'=>0,
            'journal'=>'Andrea Cortegiani, Giovanni Misseri, Mariachiara Ippolito, Matteo Bassetti, Antonino Giarratano, Ignacio Martin-Loeches & Sharon Einav',
            'type'=>0,
            'month_article'=>0,
            'famous'=>1,
            'views'=>213,
            'show'=>0,
            'date'=>'2019-05-11 22:00:00',
            'active'=>'1',
            'user'=>0,
            'created_at'=>'2020-05-15 01:20:06',
            'updated_at'=>'2020-06-21 00:08:23',
            'deleted_at'=>NULL
            ] );
            
            
                        
            Post::create( [
            'id'=>5122,
            'cat_id'=>8,
            'tag_id'=>6,
            'title'=>'Preoxygenation before intubation in adult patients with acute hypoxemic respiratory failure: a network meta-analysis of randomized trials',
            'text'=>'Critical Care,  18 September 2019',
            'img'=>'im6.jpg',
            'file'=>NULL,
            'link'=>'https://ccforum.biomedcentral.com/articles/10.1186/s13054-019-2596-1',
            'by'=>0,
            'journal'=>'Ka Man Fong, Shek Yin Au and George Wing Yiu Ng',
            'type'=>0,
            'month_article'=>0,
            'famous'=>0,
            'views'=>118,
            'show'=>0,
            'date'=>'2019-09-11 22:00:00',
            'active'=>'1',
            'user'=>0,
            'created_at'=>'2020-05-15 01:20:06',
            'updated_at'=>'2020-06-21 00:08:23',
            'deleted_at'=>NULL
            ] );
            
            
                        
            Post::create( [
            'id'=>5123,
            'cat_id'=>12,
            'tag_id'=>6,
            'title'=>'Conservative Oxygen Therapy during Mechanical Ventilation in the ICU',
            'text'=>'NEJ Med, October 14, 2019',
            'img'=>'im6.jpg',
            'file'=>NULL,
            'link'=>'https://www.nejm.org/doi/full/10.1056/NEJMoa1903297?query=RP',
            'by'=>0,
            'journal'=>'The ICU-ROX Investigators and the Australian and New Zealand Intensive Care Society Clinical Trials Group',
            'type'=>0,
            'month_article'=>0,
            'famous'=>0,
            'views'=>229,
            'show'=>0,
            'date'=>'2019-10-14 22:00:00',
            'active'=>'1',
            'user'=>0,
            'created_at'=>'2020-05-15 01:20:06',
            'updated_at'=>'2020-06-21 00:08:23',
            'deleted_at'=>NULL
            ] );
            
            
                        
            Post::create( [
            'id'=>5124,
            'cat_id'=>8,
            'tag_id'=>7,
            'title'=>'Angiotensin–Neprilysin Inhibition in Heart Failure with Preserved Ejection Fraction',
            'text'=>'N Engl J Med 2019381:1609-1620',
            'img'=>'file-1463591639-4YG8JY.jpg',
            'file'=>NULL,
            'link'=>'https://www.nejm.org/doi/full/10.1056/NEJMoa1908655?query=TOC',
            'by'=>0,
            'journal'=>'S.D. Solomon and Others',
            'type'=>0,
            'month_article'=>0,
            'famous'=>0,
            'views'=>118,
            'show'=>0,
            'date'=>'2019-10-25 22:00:00',
            'active'=>'1',
            'user'=>0,
            'created_at'=>'2020-05-15 01:20:06',
            'updated_at'=>'2020-06-21 00:08:23',
            'deleted_at'=>NULL
            ] );
            
            
                        
            Post::create( [
            'id'=>5125,
            'cat_id'=>8,
            'tag_id'=>6,
            'title'=>'Metoprolol for the Prevention of Acute Exacerbations of COPD',
            'text'=>'NEJMoa1908142  October 20, 2019',
            'img'=>'file-1463591752-XK911T.jpg',
            'file'=>NULL,
            'link'=>'https://www.nejm.org/doi/full/10.1056/NEJMoa1908142?query=TOC',
            'by'=>0,
            'journal'=>'M.T. Dransfield and Others',
            'type'=>0,
            'month_article'=>0,
            'famous'=>0,
            'views'=>118,
            'show'=>0,
            'date'=>'2019-10-19 22:00:00',
            'active'=>'1',
            'user'=>0,
            'created_at'=>'2020-05-15 01:20:06',
            'updated_at'=>'2020-06-21 00:08:23',
            'deleted_at'=>NULL
            ] );
            
            
                        
            Post::create( [
            'id'=>5126,
            'cat_id'=>8,
            'tag_id'=>11,
            'title'=>'A Machine Learning Algorithm to Predict Severe Sepsis and Septic Shock: Development, Implementation, and Impact on Clinical Practice',
            'text'=>'Critical Care Medicine. 4711:1485-1492, November 2019',
            'img'=>'file-1464106363-HWQX6B.jpg',
            'file'=>NULL,
            'link'=>'https://journals.lww.com/ccmjournal/Fulltext/2019/11000/A_Machine_Learning_Algorithm_to_Predict_Severe.2.aspx',
            'by'=>0,
            'journal'=>'Giannini, Heather M and others',
            'type'=>0,
            'month_article'=>0,
            'famous'=>0,
            'views'=>118,
            'show'=>0,
            'date'=>'2019-11-01 22:00:00',
            'active'=>'1',
            'user'=>0,
            'created_at'=>'2020-05-15 01:20:06',
            'updated_at'=>'2020-06-21 00:08:23',
            'deleted_at'=>NULL
            ] );
            
            
                        
            Post::create( [
            'id'=>5127,
            'cat_id'=>8,
            'tag_id'=>6,
            'title'=>'Ultra-Protective Ventilation Reduces Biotrauma in Patients on Venovenous Extracorporeal Membrane Oxygenation for Severe Acute Respiratory Distress Syndrome',
            'text'=>'Critical Care Medicine. 4711:1505-1512, November 2019',
            'img'=>'im4.jpg',
            'file'=>NULL,
            'link'=>'https://journals.lww.com/ccmjournal/Abstract/2019/11000/Ultra_Protective_Ventilation_Reduces_Biotrauma_in.5.aspx',
            'by'=>0,
            'journal'=>'ozencwajg, Sacha and others',
            'type'=>0,
            'month_article'=>0,
            'famous'=>0,
            'views'=>118,
            'show'=>0,
            'date'=>'2019-11-01 22:00:00',
            'active'=>'1',
            'user'=>0,
            'created_at'=>'2020-05-15 01:20:06',
            'updated_at'=>'2020-06-21 00:08:23',
            'deleted_at'=>NULL
            ] );
            
            
                        
            Post::create( [
            'id'=>5128,
            'cat_id'=>8,
            'tag_id'=>2,
            'title'=>'The ED-SED Study: A Multicenter, Prospective Cohort Study of Practice Patterns and Clinical Outcomes Associated With Emergency Department SEDation for Mechanically Ventilated Patients',
            'text'=>'Critical Care Medicine. 4711:1539-1548, November 2019',
            'img'=>'im5.jpg',
            'file'=>NULL,
            'link'=>'https://journals.lww.com/ccmjournal/Abstract/2019/11000/The_ED_SED_Study__A_Multicenter,_Prospective.9.aspx',
            'by'=>0,
            'journal'=>'Fuller, Brian M and others',
            'type'=>0,
            'month_article'=>0,
            'famous'=>0,
            'views'=>189,
            'show'=>0,
            'date'=>'2019-11-01 22:00:00',
            'active'=>'1',
            'user'=>0,
            'created_at'=>'2020-05-15 01:20:06',
            'updated_at'=>'2020-06-21 00:08:23',
            'deleted_at'=>NULL
            ] );
            
            
                        
            Post::create( [
            'id'=>5129,
            'cat_id'=>8,
            'tag_id'=>11,
            'title'=>'Emergency Department to ICU Time Is Associated With Hospital Mortality: A Registry Analysis of 14,788 Patients From Six University Hospitals in The Netherlands',
            'text'=>'Critical Care Medicine. 4711:1564-1571, November 2019',
            'img'=>'file-1464165480-R231XR.jpg',
            'file'=>NULL,
            'link'=>'https://journals.lww.com/ccmjournal/Fulltext/2019/11000/Emergency_Department_to_ICU_Time_Is_Associated.12.aspx',
            'by'=>0,
            'journal'=>'Groenland, Carline N. L and others',
            'type'=>0,
            'month_article'=>0,
            'famous'=>0,
            'views'=>118,
            'show'=>0,
            'date'=>'2019-11-01 22:00:00',
            'active'=>'1',
            'user'=>0,
            'created_at'=>'2020-05-15 01:20:06',
            'updated_at'=>'2020-06-21 00:08:23',
            'deleted_at'=>NULL
            ] );
            
            
                        
            Post::create( [
            'id'=>5130,
            'cat_id'=>12,
            'tag_id'=>2,
            'title'=>'Clinical Practice Guidelines and Consensus Statements About Pain Management in Critically Ill End-of-Life Patients: A Systematic Review',
            'text'=>'Critical Care Medicine. 4711:1619-1626, November 2019',
            'img'=>'file-1463591644-ZGN2DA.jpg',
            'file'=>NULL,
            'link'=>'https://journals.lww.com/ccmjournal/Abstract/2019/11000/Clinical_Practice_Guidelines_and_Consensus.18.aspx',
            'by'=>0,
            'journal'=>'Durán-Crane, Alejandro and others',
            'type'=>0,
            'month_article'=>0,
            'famous'=>0,
            'views'=>300,
            'show'=>0,
            'date'=>'2019-11-01 22:00:00',
            'active'=>'1',
            'user'=>0,
            'created_at'=>'2020-05-15 01:20:06',
            'updated_at'=>'2020-06-21 00:08:23',
            'deleted_at'=>NULL
            ] );
            
            
                        
            Post::create( [
            'id'=>5131,
            'cat_id'=>8,
            'tag_id'=>11,
            'title'=>'One-Year Outcomes Following Tracheostomy for Acute Respiratory Failure',
            'text'=>'Critical Care Medicine. 4711:1572-1581, November 2019',
            'img'=>'file-1463591639-4YG8JY.jpg',
            'file'=>NULL,
            'link'=>'https://journals.lww.com/ccmjournal/Abstract/2019/11000/One_Year_Outcomes_Following_Tracheostomy_for_Acute.13.aspx',
            'by'=>0,
            'journal'=>'Mehta, Anuj B and others',
            'type'=>0,
            'month_article'=>0,
            'famous'=>0,
            'views'=>118,
            'show'=>0,
            'date'=>'2019-11-01 22:00:00',
            'active'=>'1',
            'user'=>0,
            'created_at'=>'2020-05-15 01:20:06',
            'updated_at'=>'2020-06-21 00:08:23',
            'deleted_at'=>NULL
            ] );
            
            
                        
            Post::create( [
            'id'=>5132,
            'cat_id'=>8,
            'tag_id'=>6,
            'title'=>'Extracorporeal membrane oxygenation in Pneumocystis jirovecii pneumonia: outcome in HIV and non-HIV patients',
            'text'=>'Critical Care 2019, 23:356  Published on 14 November 2019',
            'img'=>'file-1463591752-XK911T.jpg',
            'file'=>NULL,
            'link'=>'https://ccforum.biomedcentral.com/articles/10.1186/s13054-019-2661-9',
            'by'=>0,
            'journal'=>'Jonathan Rilinger, Dawid L. Staudacher, Siegbert Rieg, Daniel Duerschmied, Christoph Bode and Tobias Wengenmayer',
            'type'=>0,
            'month_article'=>0,
            'famous'=>0,
            'views'=>118,
            'show'=>0,
            'date'=>'2019-11-14 22:00:00',
            'active'=>'1',
            'user'=>0,
            'created_at'=>'2020-05-15 01:20:06',
            'updated_at'=>'2020-06-21 00:08:23',
            'deleted_at'=>NULL
            ] );
            
            
                        
            Post::create( [
            'id'=>5133,
            'cat_id'=>8,
            'tag_id'=>8,
            'title'=>'A Phase 3 Trial of Difelikefalin in Hemodialysis Patients with Pruritus',
            'text'=>'N Eng J Med. November 8, 2019',
            'img'=>'file-1463591738-IZ3XFW.jpg',
            'file'=>NULL,
            'link'=>'https://www.nejm.org/doi/full/10.1056/NEJMoa1912770?query=TOC',
            'by'=>0,
            'journal'=>'S. Fishbane and Others',
            'type'=>0,
            'month_article'=>0,
            'famous'=>0,
            'views'=>169,
            'show'=>0,
            'date'=>'2019-11-07 22:00:00',
            'active'=>'1',
            'user'=>0,
            'created_at'=>'2020-05-15 01:20:06',
            'updated_at'=>'2020-06-21 00:08:23',
            'deleted_at'=>NULL
            ] );
            
            
                        
            Post::create( [
            'id'=>5134,
            'cat_id'=>8,
            'tag_id'=>6,
            'title'=>'Acute Upper Airway Obstruction',
            'text'=>'N Engl J Med 2019381:1940-1949',
            'img'=>'im4.jpg',
            'file'=>NULL,
            'link'=>'https://www.nejm.org/doi/full/10.1056/NEJMra1811697?query=TOC',
            'by'=>0,
            'journal'=>'A. Eskander, J.R. de Almeida, and J.C. Irish',
            'type'=>0,
            'month_article'=>0,
            'famous'=>0,
            'views'=>118,
            'show'=>0,
            'date'=>'2019-11-06 22:00:00',
            'active'=>'1',
            'user'=>0,
            'created_at'=>'2020-05-15 01:20:06',
            'updated_at'=>'2020-06-21 00:08:23',
            'deleted_at'=>NULL
            ] );
            
            
                        
            Post::create( [
            'id'=>5135,
            'cat_id'=>8,
            'tag_id'=>11,
            'title'=>'Study evaluates job loss after ICU discharge',
            'text'=>'Consumer Affairs, 11/13/2019',
            'img'=>'file-1464165480-R231XR.jpg',
            'file'=>NULL,
            'link'=>'consumeraffairs.com/news/job-loss-is-increasingly-common-among-patients-who-leave-the-icu-111319.html',
            'by'=>0,
            'journal'=>'By Kristen Dalli',
            'type'=>0,
            'month_article'=>0,
            'famous'=>0,
            'views'=>118,
            'show'=>0,
            'date'=>'2019-11-12 22:00:00',
            'active'=>'1',
            'user'=>0,
            'created_at'=>'2020-05-15 01:20:06',
            'updated_at'=>'2020-06-21 00:08:23',
            'deleted_at'=>NULL
            ] );
            
            
                        
            Post::create( [
            'id'=>5136,
            'cat_id'=>12,
            'tag_id'=>7,
            'title'=>'Efficacy and Safety of Low-Dose Colchicine after Myocardial Infarction',
            'text'=>'N Eng J. Med, November 16, 2019',
            'img'=>'file-1463591644-ZGN2DA.jpg',
            'file'=>NULL,
            'link'=>'https://www.nejm.org/doi/full/10.1056/NEJMoa1912388?query=RP',
            'by'=>0,
            'journal'=>'J.‑C. Tardif and Others',
            'type'=>0,
            'month_article'=>0,
            'famous'=>1,
            'views'=>324,
            'show'=>0,
            'date'=>'2019-11-16 22:00:00',
            'active'=>'1',
            'user'=>0,
            'created_at'=>'2020-05-15 01:20:06',
            'updated_at'=>'2020-06-21 00:08:23',
            'deleted_at'=>NULL
            ] );
            
            
                        
            Post::create( [
            'id'=>5137,
            'cat_id'=>8,
            'tag_id'=>11,
            'title'=>'Inflammation as a Treatment Target after Acute Myocardial Infarction',
            'text'=>'N Eng J Med, November 16, 2016',
            'img'=>'file-1463591639-4YG8JY.jpg',
            'file'=>NULL,
            'link'=>'https://www.nejm.org/doi/full/10.1056/NEJMe1914378?query=RP',
            'by'=>0,
            'journal'=>'L. Kristin Newby, M.D., M.H.S.',
            'type'=>0,
            'month_article'=>0,
            'famous'=>0,
            'views'=>118,
            'show'=>0,
            'date'=>'2019-11-16 22:00:00',
            'active'=>'1',
            'user'=>0,
            'created_at'=>'2020-05-15 01:20:06',
            'updated_at'=>'2020-06-21 00:08:23',
            'deleted_at'=>NULL
            ] );
            
            
                        
            Post::create( [
            'id'=>5138,
            'cat_id'=>8,
            'tag_id'=>1,
            'title'=>'Beware probiotics in ICU patients',
            'text'=>'',
            'img'=>'file-1463591725-9J88SF.jpg',
            'file'=>NULL,
            'link'=>'https://medicalxpress.com/news/2019-11-beware-probiotics-icu-patients.html',
            'by'=>0,
            'journal'=>'Childrens Hospital Boston. NOVEMBER 12, 2019',
            'type'=>0,
            'month_article'=>0,
            'famous'=>0,
            'views'=>147,
            'show'=>0,
            'date'=>'2019-11-17 22:00:00',
            'active'=>'1',
            'user'=>0,
            'created_at'=>'2020-05-15 01:20:06',
            'updated_at'=>'2020-06-21 00:08:23',
            'deleted_at'=>NULL
            ] );
            
            
                        
            Post::create( [
            'id'=>5139,
            'cat_id'=>8,
            'tag_id'=>11,
            'title'=>'A Comparison of Two LDL Cholesterol Targets after Ischemic Stroke',
            'text'=>'N Engl J Med. November 18, 2019',
            'img'=>'file-1463591639-4YG8JY.jpg',
            'file'=>NULL,
            'link'=>'https://www.nejm.org/doi/full/10.1056/NEJMoa1910355?query=RP',
            'by'=>0,
            'journal'=>'Pierre Amarenco, et al. for the Treat Stroke to Target Investigators',
            'type'=>0,
            'month_article'=>0,
            'famous'=>0,
            'views'=>118,
            'show'=>0,
            'date'=>'2019-11-17 22:00:00',
            'active'=>'1',
            'user'=>0,
            'created_at'=>'2020-05-15 01:20:06',
            'updated_at'=>'2020-06-21 00:08:23',
            'deleted_at'=>NULL
            ] );
            
            
                        
            Post::create( [
            'id'=>5140,
            'cat_id'=>9,
            'tag_id'=>5,
            'title'=>'2018 Clinical Practice Guidelines for the Prevention and Management of Pain, Agitation/Sedation, Delirium, Immobility, and Sleep Disruption in Adult Patients in the ICU',
            'text'=>'Critical Care Medicine: September 2018 - Volume 46 - Issue 9 - p 1532-1548',
            'img'=>'im1.jpg',
            'file'=>NULL,
            'link'=>'https://journals.lww.com/ccmjournal/Fulltext/2018/09000/Executive_Summary___Clinical_Practice_Guidelines.21.aspx',
            'by'=>0,
            'journal'=>'Devlin, John W and others',
            'type'=>0,
            'month_article'=>0,
            'famous'=>0,
            'views'=>131,
            'show'=>0,
            'date'=>'2018-09-17 22:00:00',
            'active'=>'1',
            'user'=>0,
            'created_at'=>'2020-05-15 01:20:06',
            'updated_at'=>'2020-06-21 00:08:23',
            'deleted_at'=>NULL
            ] );
            
            
                        
            Post::create( [
            'id'=>5141,
            'cat_id'=>8,
            'tag_id'=>6,
            'title'=>'Early Neuromuscular Blockade in the Acute Respiratory Distress Syndrome',
            'text'=>'N Engl J Med 2019 380:1997-2008',
            'img'=>'file-1463591752-XK911T.jpg',
            'file'=>NULL,
            'link'=>'https://www.nejm.org/doi/full/10.1056/NEJMoa1901686',
            'by'=>0,
            'journal'=>'ROSE ClinicalTrials',
            'type'=>0,
            'month_article'=>0,
            'famous'=>1,
            'views'=>213,
            'show'=>0,
            'date'=>'2019-05-19 22:00:00',
            'active'=>'1',
            'user'=>0,
            'created_at'=>'2020-05-15 01:20:06',
            'updated_at'=>'2020-06-21 00:08:23',
            'deleted_at'=>NULL
            ] );
            
            
                        
            Post::create( [
            'id'=>5142,
            'cat_id'=>8,
            'tag_id'=>1,
            'title'=>'Increased β-Lactams dosing regimens improve clinical outcome in critically ill patients with augmented renal clearance treated for a first episode of hospital or ventilator-acquired pneumonia: a before and after study',
            'text'=>'Critical Care 2019, 23:379  Published on: 27 November 2019',
            'img'=>'im6.jpg',
            'file'=>NULL,
            'link'=>'https://ccforum.biomedcentral.com/articles/10.1186/s13054-019-2621-4',
            'by'=>0,
            'journal'=>'Cédric Carrié and others',
            'type'=>0,
            'month_article'=>0,
            'famous'=>0,
            'views'=>147,
            'show'=>0,
            'date'=>'2019-11-27 22:00:00',
            'active'=>'1',
            'user'=>0,
            'created_at'=>'2020-05-15 01:20:06',
            'updated_at'=>'2020-06-21 00:08:23',
            'deleted_at'=>NULL
            ] );
            
            
                        
            Post::create( [
            'id'=>5143,
            'cat_id'=>8,
            'tag_id'=>11,
            'title'=>'Randomized Trial of Three Anticonvulsant Medications for Status Epilepticus',
            'text'=>'N Engl J Med 2019381:2103-2113',
            'img'=>'im1.jpg',
            'file'=>NULL,
            'link'=>'https://www.nejm.org/doi/full/10.1056/NEJMoa1905795?query=TOC',
            'by'=>0,
            'journal'=>'J. Kapur and Others',
            'type'=>0,
            'month_article'=>0,
            'famous'=>0,
            'views'=>118,
            'show'=>0,
            'date'=>'2019-11-27 22:00:00',
            'active'=>'1',
            'user'=>0,
            'created_at'=>'2020-05-15 01:20:06',
            'updated_at'=>'2020-06-21 00:08:23',
            'deleted_at'=>NULL
            ] );
            
            
                        
            Post::create( [
            'id'=>5144,
            'cat_id'=>12,
            'tag_id'=>11,
            'title'=>'Vitamin C Can Shorten the Length of Stay in the ICU: A Meta-Analysis',
            'text'=>'Nutrients 114:708 · March 2019',
            'img'=>'file-1463591681-AGRM7H.jpg',
            'file'=>NULL,
            'link'=>'https://www.researchgate.net/publication/332075264_Vitamin_C_Can_Shorten_the_Length_of_Stay_in_the_ICU_A_Meta-Analysis',
            'by'=>0,
            'journal'=>'Harri Hemilä & Elizabeth Chalker',
            'type'=>0,
            'month_article'=>0,
            'famous'=>1,
            'views'=>324,
            'show'=>0,
            'date'=>'2019-03-29 22:00:00',
            'active'=>'1',
            'user'=>0,
            'created_at'=>'2020-05-15 01:20:06',
            'updated_at'=>'2020-06-21 00:08:23',
            'deleted_at'=>NULL
            ] );
            
            
                        
            Post::create( [
            'id'=>5145,
            'cat_id'=>8,
            'tag_id'=>1,
            'title'=>'Too Many Definitions of Sepsis: Can Machine Learning Leverage the Electronic Health Record to Increase Accuracy and Bring Consensus',
            'text'=>'Critical Care Medicine. 482:137-141, February 2020',
            'img'=>'file-1464165463-D3CBM9.jpg',
            'file'=>NULL,
            'link'=>'https://journals.lww.com/ccmjournal/Fulltext/2020/02000/Too_Many_Definitions_of_Sepsis__Can_Machine.1.aspx',
            'by'=>0,
            'journal'=>'Saria, Suchi Henry, Katharine E.',
            'type'=>0,
            'month_article'=>0,
            'famous'=>0,
            'views'=>147,
            'show'=>0,
            'date'=>'2020-01-31 22:00:00',
            'active'=>'1',
            'user'=>0,
            'created_at'=>'2020-05-15 01:20:06',
            'updated_at'=>'2020-06-21 00:08:23',
            'deleted_at'=>NULL
            ] );
            
            
                        
            Post::create( [
            'id'=>5146,
            'cat_id'=>8,
            'tag_id'=>11,
            'title'=>'Epigenetic Profiling in Severe Sepsis: A Pilot Study of DNA Methylation Profiles in Critical Illness',
            'text'=>'Critical Care Medicine. 482:142-150, February 2020',
            'img'=>'file-1464106363-HWQX6B.jpg',
            'file'=>NULL,
            'link'=>'https://journals.lww.com/ccmjournal/Fulltext/2020/02000/Epigenetic_Profiling_in_Severe_Sepsis__A_Pilot.2.aspx',
            'by'=>0,
            'journal'=>'Binnie, Alexandra Walsh and others',
            'type'=>0,
            'month_article'=>0,
            'famous'=>0,
            'views'=>118,
            'show'=>0,
            'date'=>'2020-01-31 22:00:00',
            'active'=>'1',
            'user'=>0,
            'created_at'=>'2020-05-15 01:20:06',
            'updated_at'=>'2020-06-21 00:08:23',
            'deleted_at'=>NULL
            ] );
            
            
                        
            Post::create( [
            'id'=>5147,
            'cat_id'=>8,
            'tag_id'=>11,
            'title'=>'Comparative Performance of Pulmonary Ultrasound, Chest Radiograph, and CT Among Patients With Acute Respiratory Failure',
            'text'=>'Critical Care Medicine. 482:151-157, February 2020',
            'img'=>'file-1463591752-XK911T.jpg',
            'file'=>NULL,
            'link'=>'https://journals.lww.com/ccmjournal/Fulltext/2020/02000/Comparative_Performance_of_Pulmonary_Ultrasound,.3.aspx',
            'by'=>0,
            'journal'=>'Tierney, David M and others',
            'type'=>0,
            'month_article'=>0,
            'famous'=>0,
            'views'=>118,
            'show'=>0,
            'date'=>'2020-01-31 22:00:00',
            'active'=>'1',
            'user'=>0,
            'created_at'=>'2020-05-15 01:20:06',
            'updated_at'=>'2020-06-21 00:08:23',
            'deleted_at'=>NULL
            ] );
            
            
                        
            Post::create( [
            'id'=>5148,
            'cat_id'=>8,
            'tag_id'=>11,
            'title'=>'Family Care Rituals in the ICU to Reduce Symptoms of Post-Traumatic Stress Disorder in Family Members—A Multicenter, Multinational, Before-and-After Intervention Trial',
            'text'=>'Critical Care Medicine. 482:176-184, February 2020',
            'img'=>'file-1464165480-R231XR.jpg',
            'file'=>NULL,
            'link'=>'https://journals.lww.com/ccmjournal/Abstract/2020/02000/Family_Care_Rituals_in_the_ICU_to_Reduce_Symptoms.6.aspx',
            'by'=>0,
            'journal'=>'Amass, Timothy H and others',
            'type'=>0,
            'month_article'=>0,
            'famous'=>0,
            'views'=>118,
            'show'=>0,
            'date'=>'2020-01-31 22:00:00',
            'active'=>'1',
            'user'=>0,
            'created_at'=>'2020-05-15 01:20:06',
            'updated_at'=>'2020-06-21 00:08:23',
            'deleted_at'=>NULL
            ] );
            
            
                        
            Post::create( [
            'id'=>5149,
            'cat_id'=>8,
            'tag_id'=>11,
            'title'=>'Prevalence and Risk Factors for Thrombotic Complications Following Venovenous Extracorporeal Membrane Oxygenation: A CT Scan Study',
            'text'=>'Critical Care Medicine. 482:192-199, February 2020',
            'img'=>'file-1463591644-ZGN2DA.jpg',
            'file'=>NULL,
            'link'=>'https://journals.lww.com/ccmjournal/Abstract/2020/02000/Prevalence_and_Risk_Factors_for_Thrombotic.8.aspx',
            'by'=>0,
            'journal'=>'Parzy, Gabriel and others',
            'type'=>0,
            'month_article'=>0,
            'famous'=>0,
            'views'=>118,
            'show'=>0,
            'date'=>'2020-01-31 22:00:00',
            'active'=>'1',
            'user'=>0,
            'created_at'=>'2020-05-15 01:20:06',
            'updated_at'=>'2020-06-21 00:08:23',
            'deleted_at'=>NULL
            ] );
            
            
                        
            Post::create( [
            'id'=>5150,
            'cat_id'=>12,
            'tag_id'=>9,
            'title'=>'Correction and Control of Hyperammonemia in Acute Liver Failure: The Impact of Continuous Renal Replacement Timing, Intensity, and Duration',
            'text'=>'Critical Care Medicine. 482:218-224, February 2020',
            'img'=>'file-1463591650-J14MMU.jpg',
            'file'=>NULL,
            'link'=>'https://journals.lww.com/ccmjournal/Abstract/2020/02000/Correction_and_Control_of_Hyperammonemia_in_Acute.11.aspx',
            'by'=>0,
            'journal'=>'Warrillow, Stephen and others',
            'type'=>0,
            'month_article'=>0,
            'famous'=>0,
            'views'=>229,
            'show'=>0,
            'date'=>'2020-01-29 22:00:00',
            'active'=>'1',
            'user'=>0,
            'created_at'=>'2020-05-15 01:20:06',
            'updated_at'=>'2020-06-21 00:08:23',
            'deleted_at'=>NULL
            ] );
            
            
                        
            Post::create( [
            'id'=>5151,
            'cat_id'=>8,
            'tag_id'=>7,
            'title'=>'Epinephrine for Out-of-Hospital Cardiac Arrest: An Updated Systematic Review and Meta-Analysis',
            'text'=>'Critical Care Medicine. 482:225-229, February 2020',
            'img'=>'file-1463591639-4YG8JY.jpg',
            'file'=>NULL,
            'link'=>'https://journals.lww.com/ccmjournal/Abstract/2020/02000/Epinephrine_for_Out_of_Hospital_Cardiac_Arrest__An.12.aspx',
            'by'=>0,
            'journal'=>'Aves, Theres and others',
            'type'=>0,
            'month_article'=>0,
            'famous'=>0,
            'views'=>118,
            'show'=>0,
            'date'=>'2020-01-31 22:00:00',
            'active'=>'1',
            'user'=>0,
            'created_at'=>'2020-05-15 01:20:06',
            'updated_at'=>'2020-06-21 00:08:23',
            'deleted_at'=>NULL
            ] );
            
            
                        
            Post::create( [
            'id'=>5152,
            'cat_id'=>12,
            'tag_id'=>3,
            'title'=>'Effect of Vitamin C, Hydrocortisone, and Thiamine vs Hydrocortisone Alone on Time Alive and Free of Vasopressor Support Among Patients With Septic Shock The VITAMINS Randomized Clinical Trial',
            'text'=>'JAMA. Published online January 17, 2020',
            'img'=>'file-1463591725-9J88SF.jpg',
            'file'=>NULL,
            'link'=>'https://jamanetwork.com/journals/jama/article-abstract/2759414',
            'by'=>0,
            'journal'=>'Tomoko Fujii, And others',
            'type'=>0,
            'month_article'=>0,
            'famous'=>1,
            'views'=>324,
            'show'=>0,
            'date'=>'2020-01-29 22:00:00',
            'active'=>'1',
            'user'=>0,
            'created_at'=>'2020-05-15 01:20:06',
            'updated_at'=>'2020-06-21 00:08:23',
            'deleted_at'=>NULL
            ] );
            
            
                        
            Post::create( [
            'id'=>5153,
            'cat_id'=>12,
            'tag_id'=>9,
            'title'=>'Effect of Stress Ulcer Prophylaxis With Proton Pump Inhibitors vs Histamine-2 Receptor Blockers on In-Hospital Mortality Among ICU Patients Receiving Invasive Mechanical Ventilation The PEPTIC Randomized Clinical Trial',
            'text'=>'JAMA. Published online January 17, 2020',
            'img'=>'file-1463591657-K8BRDF.jpg',
            'file'=>NULL,
            'link'=>'https://jamanetwork.com/journals/jama/article-abstract/2759412',
            'by'=>0,
            'journal'=>'The PEPTIC Investigators',
            'type'=>0,
            'month_article'=>0,
            'famous'=>1,
            'views'=>324,
            'show'=>0,
            'date'=>'2020-02-01 22:00:00',
            'active'=>'1',
            'user'=>0,
            'created_at'=>'2020-05-15 01:20:06',
            'updated_at'=>'2020-06-21 00:08:23',
            'deleted_at'=>NULL
            ] );
            
            
                        
            Post::create( [
            'id'=>5154,
            'cat_id'=>8,
            'tag_id'=>11,
            'title'=>'Conservative versus Interventional Treatment for Spontaneous Pneumothorax',
            'text'=>'N Engl J Med 2020 382:405-415',
            'img'=>'file-1463591639-4YG8JY.jpg',
            'file'=>NULL,
            'link'=>'https://www.nejm.org/doi/full/10.1056/NEJMoa1910775',
            'by'=>0,
            'journal'=>'Simon G.A. Brown and others',
            'type'=>0,
            'month_article'=>0,
            'famous'=>0,
            'views'=>118,
            'show'=>0,
            'date'=>'2020-02-01 22:00:00',
            'active'=>'1',
            'user'=>0,
            'created_at'=>'2020-05-15 01:20:06',
            'updated_at'=>'2020-06-21 00:08:23',
            'deleted_at'=>NULL
            ] );
            
            
                        
            Post::create( [
            'id'=>5155,
            'cat_id'=>8,
            'tag_id'=>8,
            'title'=>'Soluble Urokinase Receptor and Acute Kidney Injury',
            'text'=>'N Engl J Med 2020 382:416-426',
            'img'=>'file-1463591738-IZ3XFW.jpg',
            'file'=>NULL,
            'link'=>'https://www.nejm.org/doi/full/10.1056/NEJMoa1911481',
            'by'=>0,
            'journal'=>'Salim S. Hayek and others',
            'type'=>0,
            'month_article'=>0,
            'famous'=>0,
            'views'=>169,
            'show'=>0,
            'date'=>'2020-01-01 22:00:00',
            'active'=>'1',
            'user'=>0,
            'created_at'=>'2020-05-15 01:20:06',
            'updated_at'=>'2020-06-21 00:08:23',
            'deleted_at'=>NULL
            ] );
            
            
                        
            Post::create( [
            'id'=>5156,
            'cat_id'=>8,
            'tag_id'=>1,
            'title'=>'A Novel Coronavirus from Patients with Pneumonia in China, 2019',
            'text'=>'N Eng J Med. January 24, 2020',
            'img'=>'file-1463591752-XK911T.jpg',
            'file'=>NULL,
            'link'=>'https://www.nejm.org/doi/full/10.1056/NEJMoa2001017?query=featured_home',
            'by'=>0,
            'journal'=>'Na Zhu and others  for the China Novel Coronavirus Investigating and Research Team',
            'type'=>0,
            'month_article'=>0,
            'famous'=>0,
            'views'=>147,
            'show'=>0,
            'date'=>'2020-02-01 22:00:00',
            'active'=>'1',
            'user'=>0,
            'created_at'=>'2020-05-15 01:20:06',
            'updated_at'=>'2020-06-21 00:08:23',
            'deleted_at'=>NULL
            ] );
            
            
                        
            Post::create( [
            'id'=>5157,
            'cat_id'=>12,
            'tag_id'=>6,
            'title'=>'Conservative Oxygen Therapy during Mechanical Ventilation in the ICU',
            'text'=>'N Engl J Med. March 2020382:989-998',
            'img'=>'im5.jpg',
            'file'=>NULL,
            'link'=>'https://www.nejm.org/doi/full/10.1056/NEJMoa1903297queryTOC',
            'by'=>0,
            'journal'=>'The ICU-ROX Investigators and the Australian and New Zealand Intensive Care Society Clinical Trials Group',
            'type'=>0,
            'month_article'=>0,
            'famous'=>0,
            'views'=>229,
            'show'=>0,
            'date'=>'2020-03-11 22:00:00',
            'active'=>'1',
            'user'=>0,
            'created_at'=>'2020-05-15 01:20:06',
            'updated_at'=>'2020-06-21 00:08:23',
            'deleted_at'=>NULL
            ] );
            
            
                        
            Post::create( [
            'id'=>5158,
            'cat_id'=>12,
            'tag_id'=>6,
            'title'=>'Liberal or Conservative Oxygen Therapy for Acute Respiratory Distress Syndrome',
            'text'=>'N Engl J Med. March 2020382:999-1008',
            'img'=>'file-1463591657-K8BRDF.jpg',
            'file'=>NULL,
            'link'=>'https://www.nejm.org/doi/full/10.1056/NEJMoa1916431?query=TOC',
            'by'=>0,
            'journal'=>'L. Barrot and Others',
            'type'=>0,
            'month_article'=>0,
            'famous'=>0,
            'views'=>229,
            'show'=>0,
            'date'=>'2020-03-10 22:00:00',
            'active'=>'1',
            'user'=>0,
            'created_at'=>'2020-05-15 01:20:06',
            'updated_at'=>'2020-06-21 00:08:23',
            'deleted_at'=>NULL
            ] );
            
            
                        
            Post::create( [
            'id'=>5159,
            'cat_id'=>8,
            'tag_id'=>9,
            'title'=>'Association of Aspirin with Hepatocellular Carcinoma and Liver-Related Mortality',
            'text'=>'N Engl J Med. March 2020382:1018-1028',
            'img'=>'file-1463591650-J14MMU.jpg',
            'file'=>NULL,
            'link'=>'https://www.nejm.org/doi/full/10.1056/NEJMoa1912035?query=TOC',
            'by'=>0,
            'journal'=>'T.G. Simon and Others',
            'type'=>0,
            'month_article'=>0,
            'famous'=>0,
            'views'=>118,
            'show'=>0,
            'date'=>'2020-02-29 22:00:00',
            'active'=>'1',
            'user'=>0,
            'created_at'=>'2020-05-15 01:20:06',
            'updated_at'=>'2020-06-21 00:08:23',
            'deleted_at'=>NULL
            ] );
            
            
                        
            Post::create( [
            'id'=>5160,
            'cat_id'=>8,
            'tag_id'=>1,
            'title'=>'Virtually Perfect Telemedicine for Covid-19',
            'text'=>'N Eng J Med. March 11, 2020',
            'img'=>'file-1464165480-R231XR.jpg',
            'file'=>NULL,
            'link'=>'https://www.nejm.org/doi/full/10.1056/NEJMp2003539?query=TOC',
            'by'=>0,
            'journal'=>'J.E. Hollander and B.G. Carr',
            'type'=>0,
            'month_article'=>0,
            'famous'=>0,
            'views'=>147,
            'show'=>0,
            'date'=>'2020-03-10 22:00:00',
            'active'=>'1',
            'user'=>0,
            'created_at'=>'2020-05-15 01:20:06',
            'updated_at'=>'2020-06-21 00:08:23',
            'deleted_at'=>NULL
            ] );
            
            
                        
            Post::create( [
            'id'=>5161,
            'cat_id'=>12,
            'tag_id'=>6,
            'title'=>'Dexamethasone Use in the Treatment of ARDs',
            'text'=>'SCCM, March 10, 2020',
            'img'=>'im6.jpg',
            'file'=>NULL,
            'link'=>'https://sccm.org/Blog/March-2020/Concise-Critical-Appraisal-Dexamethasone-Use-in-t?_zs=3THjd1&_zl=o6db6',
            'by'=>0,
            'journal'=>'Alexander Bracey and Brian J. Wright',
            'type'=>0,
            'month_article'=>0,
            'famous'=>0,
            'views'=>230,
            'show'=>0,
            'date'=>'2020-03-13 22:00:00',
            'active'=>'1',
            'user'=>0,
            'created_at'=>'2020-05-15 01:20:06',
            'updated_at'=>'2020-07-22 09:47:48',
            'deleted_at'=>NULL
            ] );
            
            
                        
            Post::create( [
            'id'=>5162,
            'cat_id'=>8,
            'tag_id'=>6,
            'title'=>'Dexamethasone treatment for the acute respiratory distress syndrome: a multicentre, randomised controlled trial',
            'text'=>'Lancet Respir Med. 2020 Mar83:267-276',
            'img'=>'file-1463591752-XK911T.jpg',
            'file'=>NULL,
            'link'=>'https://www.thelancet.com/journals/lanres/article/PIIS2213-2600(19)30417-5/fulltext',
            'by'=>0,
            'journal'=>'Villar J and others',
            'type'=>0,
            'month_article'=>0,
            'famous'=>1,
            'views'=>213,
            'show'=>0,
            'date'=>'2020-03-13 22:00:00',
            'active'=>'1',
            'user'=>0,
            'created_at'=>'2020-05-15 01:20:06',
            'updated_at'=>'2020-06-21 00:08:23',
            'deleted_at'=>NULL
            ] );
            
            
                        
            Post::create( [
            'id'=>5163,
            'cat_id'=>8,
            'tag_id'=>7,
            'title'=>'Effect of Continuous Epinephrine Infusion on Survival in Critically Ill Patients: A Meta-Analysis of Randomized Trials',
            'text'=>'Critical Care Medicine. 483:398-405, March 2020',
            'img'=>'file-1463591639-4YG8JY.jpg',
            'file'=>NULL,
            'link'=>'https://journals.lww.com/ccmjournal/Abstract/2020/03000/Effect_of_Continuous_Epinephrine_Infusion_on.15.aspx',
            'by'=>0,
            'journal'=>'Belletti, Alessandro and others',
            'type'=>0,
            'month_article'=>0,
            'famous'=>0,
            'views'=>118,
            'show'=>0,
            'date'=>'2020-02-29 22:00:00',
            'active'=>'1',
            'user'=>0,
            'created_at'=>'2020-05-15 01:20:06',
            'updated_at'=>'2020-06-21 00:08:23',
            'deleted_at'=>NULL
            ] );
            
            
                        
            Post::create( [
            'id'=>5164,
            'cat_id'=>12,
            'tag_id'=>9,
            'title'=>'Guidelines for the Management of Adult Acute and Acute-on-Chronic Liver Failure in the ICU: Cardiovascular, Endocrine, Hematologic, Pulmonary and Renal Considerations: Executive Summary',
            'text'=>'Critical Care Medicine. 483:415-419, March 2020',
            'img'=>'file-1463591644-ZGN2DA.jpg',
            'file'=>NULL,
            'link'=>'https://journals.lww.com/ccmjournal/Fulltext/2020/03000/Guidelines_for_the_Management_of_Adult_Acute_and.17.aspx',
            'by'=>0,
            'journal'=>'Nanchal, Rahul and others',
            'type'=>0,
            'month_article'=>0,
            'famous'=>0,
            'views'=>229,
            'show'=>0,
            'date'=>'2020-02-29 22:00:00',
            'active'=>'1',
            'user'=>0,
            'created_at'=>'2020-05-15 01:20:06',
            'updated_at'=>'2020-06-21 00:08:23',
            'deleted_at'=>NULL
            ] );
            
            
                        
            Post::create( [
            'id'=>5165,
            'cat_id'=>8,
            'tag_id'=>5,
            'title'=>'Relationship Between Duration of Targeted Temperature Management, Ischemic Interval, and Good Functional Outcome From Out-of-Hospital Cardiac Arrest',
            'text'=>'Critical Care Medicine. 483:370-377, March 2020',
            'img'=>'file-1464165480-R231XR.jpg',
            'file'=>NULL,
            'link'=>'https://journals.lww.com/ccmjournal/Abstract/2020/03000/Relationship_Between_Duration_of_Targeted.11.aspx',
            'by'=>0,
            'journal'=>'Sawyer, Kelly N and others',
            'type'=>0,
            'month_article'=>0,
            'famous'=>0,
            'views'=>131,
            'show'=>0,
            'date'=>'2020-03-07 22:00:00',
            'active'=>'1',
            'user'=>0,
            'created_at'=>'2020-05-15 01:20:06',
            'updated_at'=>'2020-06-21 00:08:23',
            'deleted_at'=>NULL
            ] );
            
            
                        
            Post::create( [
            'id'=>5166,
            'cat_id'=>12,
            'tag_id'=>10,
            'title'=>'Convalescent Plasma to Treat COVID-19 Possibilities and Challenges',
            'text'=>'JAMA. Published online March 27, 2020',
            'img'=>'file-1464106363-HWQX6B.jpg',
            'file'=>NULL,
            'link'=>'https://jamanetwork.com/journals/jama/fullarticle/2763982',
            'by'=>0,
            'journal'=>'John D. Roback and  Jeannette Guarner',
            'type'=>0,
            'month_article'=>0,
            'famous'=>0,
            'views'=>229,
            'show'=>0,
            'date'=>'2020-03-30 22:00:00',
            'active'=>'1',
            'user'=>0,
            'created_at'=>'2020-05-15 01:20:06',
            'updated_at'=>'2020-06-21 00:08:23',
            'deleted_at'=>NULL
            ] );
            
            
                        
            Post::create( [
            'id'=>5167,
            'cat_id'=>8,
            'tag_id'=>11,
            'title'=>'Association Between Cardiac Injury and Mortality in Hospitalized Patients Infected With Avian Influenza A H7N9 Virus',
            'text'=>'Critical Care Medicine. 484:451-458, April 2020',
            'img'=>'file-1463591639-4YG8JY.jpg',
            'file'=>NULL,
            'link'=>'https://journals.lww.com/ccmjournal/Fulltext/2020/04000/Association_Between_Cardiac_Injury_and_Mortality.2.aspx',
            'by'=>0,
            'journal'=>'Gao, Chang and others',
            'type'=>0,
            'month_article'=>0,
            'famous'=>0,
            'views'=>118,
            'show'=>0,
            'date'=>'2020-03-31 22:00:00',
            'active'=>'1',
            'user'=>0,
            'created_at'=>'2020-05-15 01:20:06',
            'updated_at'=>'2020-06-21 00:08:23',
            'deleted_at'=>NULL
            ] );
            
            
                        
            Post::create( [
            'id'=>5168,
            'cat_id'=>8,
            'tag_id'=>11,
            'title'=>'Hyperferritinemia in Critically Ill Patients',
            'text'=>'Critical Care Medicine. 484:459-465, April 2020',
            'img'=>'file-1463591639-4YG8JY.jpg',
            'file'=>NULL,
            'link'=>'https://journals.lww.com/ccmjournal/Fulltext/2020/04000/Hyperferritinemia_in_Critically_Ill_Patients_.3.aspx',
            'by'=>0,
            'journal'=>'Lachmann, Gunnar and others',
            'type'=>0,
            'month_article'=>0,
            'famous'=>0,
            'views'=>118,
            'show'=>0,
            'date'=>'2020-03-31 22:00:00',
            'active'=>'1',
            'user'=>0,
            'created_at'=>'2020-05-15 01:20:06',
            'updated_at'=>'2020-06-21 00:08:23',
            'deleted_at'=>NULL
            ] );
            
            
                        
            Post::create( [
            'id'=>5169,
            'cat_id'=>8,
            'tag_id'=>11,
            'title'=>'Tele-Critical Care: An Update From the Society of Critical Care Medicine Tele-ICU Committee',
            'text'=>'Critical Care Medicine. 484:553-561, April 2020',
            'img'=>'file-1464165463-D3CBM9.jpg',
            'file'=>NULL,
            'link'=>'https://journals.lww.com/ccmjournal/Fulltext/2020/04000/Tele_Critical_Care__An_Update_From_the_Society_of.14.aspx',
            'by'=>0,
            'journal'=>'Subramanian, Sanja and others',
            'type'=>0,
            'month_article'=>0,
            'famous'=>0,
            'views'=>118,
            'show'=>0,
            'date'=>'2020-03-31 22:00:00',
            'active'=>'1',
            'user'=>0,
            'created_at'=>'2020-05-15 01:20:06',
            'updated_at'=>'2020-06-21 00:08:23',
            'deleted_at'=>NULL
            ] );
            
            
                        
            Post::create( [
            'id'=>5170,
            'cat_id'=>8,
            'tag_id'=>11,
            'title'=>'Best Practices for Conducting Interprofessional Team Rounds to Facilitate Performance of the ICU Liberation ABCDEF Bundle',
            'text'=>'Critical Care Medicine. 484:562-570, April 2020',
            'img'=>'covid-2.jpg',
            'file'=>NULL,
            'link'=>'https://journals.lww.com/ccmjournal/Fulltext/2020/04000/Best_Practices_for_Conducting_Interprofessional.15.aspx',
            'by'=>0,
            'journal'=>'Stollings, Joanna L and others',
            'type'=>0,
            'month_article'=>0,
            'famous'=>0,
            'views'=>118,
            'show'=>0,
            'date'=>'2020-03-31 22:00:00',
            'active'=>'1',
            'user'=>0,
            'created_at'=>'2020-05-15 01:20:06',
            'updated_at'=>'2020-06-21 00:08:23',
            'deleted_at'=>NULL
            ] );
            
            
                        
            Post::create( [
            'id'=>5171,
            'cat_id'=>8,
            'tag_id'=>11,
            'title'=>'Moderate Certainty Evidence Suggests the Use of High-Flow Nasal Cannula Does Not Decrease Hypoxia When Compared With Conventional Oxygen Therapy in the Peri-Intubation Period: Results of a Systematic Review and Meta-Analysis',
            'text'=>'Critical Care Medicine. 484:571-578, April 2020',
            'img'=>'file-1463591639-4YG8JY.jpg',
            'file'=>NULL,
            'link'=>'https://journals.lww.com/ccmjournal/Abstract/2020/04000/Moderate_Certainty_Evidence_Suggests_the_Use_of.16.aspx',
            'by'=>0,
            'journal'=>'Chaudhuri, Dipayan and others',
            'type'=>0,
            'month_article'=>0,
            'famous'=>0,
            'views'=>118,
            'show'=>0,
            'date'=>'2020-03-31 22:00:00',
            'active'=>'1',
            'user'=>0,
            'created_at'=>'2020-05-15 01:20:06',
            'updated_at'=>'2020-06-21 00:08:23',
            'deleted_at'=>NULL
            ] );
            
            
                        
            Post::create( [
            'id'=>5172,
            'cat_id'=>12,
            'tag_id'=>1,
            'title'=>'Surviving Sepsis Campaign: Guidelines on the Management of Critically Ill Adults with Coronavirus Disease 2019 COVID-19',
            'text'=>'Updated: 3/20/2020',
            'img'=>'file-1464165480-R231XR.jpg',
            'file'=>NULL,
            'link'=>'https://www.sccm.org/getattachment/Disaster/SSC-COVID19-Critical-Care-Guidelines.pdflangen-US',
            'by'=>0,
            'journal'=>'Waleed Alhazzani1,2 , Morten Hylander Møller and others',
            'type'=>0,
            'month_article'=>0,
            'famous'=>0,
            'views'=>258,
            'show'=>0,
            'date'=>'2020-03-30 22:00:00',
            'active'=>'1',
            'user'=>0,
            'created_at'=>'2020-05-15 01:20:06',
            'updated_at'=>'2020-06-21 00:08:23',
            'deleted_at'=>NULL
            ] );
            
            
                        
            Post::create( [
            'id'=>5173,
            'cat_id'=>12,
            'tag_id'=>7,
            'title'=>'Renin–Angiotensin–Aldosterone System Inhibitors in Patients with Covid-19',
            'text'=>'N Eng J Me. March 30, 2020',
            'img'=>'file-1463591752-XK911T.jpg',
            'file'=>NULL,
            'link'=>'https://www.nejm.org/doi/full/10.1056/NEJMsr2005760?query=RP',
            'by'=>0,
            'journal'=>'M. Vaduganathan and Others',
            'type'=>0,
            'month_article'=>0,
            'famous'=>0,
            'views'=>229,
            'show'=>0,
            'date'=>'2020-03-31 22:00:00',
            'active'=>'1',
            'user'=>0,
            'created_at'=>'2020-05-15 01:20:06',
            'updated_at'=>'2020-06-21 00:08:23',
            'deleted_at'=>NULL
            ] );
            
            
                        
            Post::create( [
            'id'=>5174,
            'cat_id'=>8,
            'tag_id'=>6,
            'title'=>'Developing Covid-19 Vaccines at Pandemic Speed',
            'text'=>'N Eng J Med. March 30, 2020',
            'img'=>'file-1464106363-HWQX6B.jpg',
            'file'=>NULL,
            'link'=>'https://www.nejm.org/doi/full/10.1056/NEJMp2005630?query=RP',
            'by'=>0,
            'journal'=>'N. Lurie and Others',
            'type'=>0,
            'month_article'=>0,
            'famous'=>0,
            'views'=>118,
            'show'=>0,
            'date'=>'2020-03-30 22:00:00',
            'active'=>'1',
            'user'=>0,
            'created_at'=>'2020-05-15 01:20:06',
            'updated_at'=>'2020-06-21 00:08:23',
            'deleted_at'=>NULL
            ] );
            
            
                        
            Post::create( [
            'id'=>5175,
            'cat_id'=>8,
            'tag_id'=>7,
            'title'=>'Approach to Adult Extracorporeal Membrane Oxygenation Patient Selection',
            'text'=>'Bohman, J. Kyle and others.Critical Care Medicine. 48(5):618-622, May 2020.',
            'img'=>'file-1464105730-LQ3Z2Q.jpg',
            'file'=>NULL,
            'link'=>'https://journals.lww.com/ccmjournal/Citation/2020/05000/Approach_to_Adult_Extracorporeal_Membrane.2.aspx',
            'by'=>0,
            'journal'=>'Site Admin',
            'type'=>0,
            'month_article'=>0,
            'famous'=>0,
            'views'=>118,
            'show'=>0,
            'date'=>'2020-05-20 21:29:16',
            'active'=>'1',
            'user'=>0,
            'created_at'=>'2020-05-20 21:29:16',
            'updated_at'=>'2020-06-21 00:08:23',
            'deleted_at'=>NULL
            ] );
            
            
                        
            Post::create( [
            'id'=>5176,
            'cat_id'=>8,
            'tag_id'=>11,
            'title'=>'Framework to Support the Process of Decision-Making on Life-Sustaining Treatments in the ICU: Results of a Delphi Study',
            'text'=>'Kerckhoffs, Monika C and others.Critical Care Medicine. 48(5):645-653, May 2020.',
            'img'=>'im5.jpg',
            'file'=>NULL,
            'link'=>'https://journals.lww.com/ccmjournal/Fulltext/2020/05000/Framework_to_Support_the_Process_of.5.aspx',
            'by'=>0,
            'journal'=>'Site Admin',
            'type'=>0,
            'month_article'=>0,
            'famous'=>0,
            'views'=>118,
            'show'=>0,
            'date'=>'2020-05-20 21:31:44',
            'active'=>'1',
            'user'=>0,
            'created_at'=>'2020-05-20 21:31:44',
            'updated_at'=>'2020-06-21 00:08:23',
            'deleted_at'=>NULL
            ] );
            
            
                        
            Post::create( [
            'id'=>5177,
            'cat_id'=>8,
            'tag_id'=>9,
            'title'=>'Selective Digestive Decontamination Is Neither Safe Nor Efficacious for Critically Ill Patients',
            'text'=>'Hurley, James C. Critical Care Medicine. 48(5):732-735, May 2020.',
            'img'=>'file-1463591650-J14MMU.jpg',
            'file'=>NULL,
            'link'=>'https://journals.lww.com/ccmjournal/Citation/2020/05000/Selective_Digestive_Decontamination_Is_Neither.16.aspx',
            'by'=>0,
            'journal'=>'Site Admin',
            'type'=>0,
            'month_article'=>0,
            'famous'=>0,
            'views'=>118,
            'show'=>0,
            'date'=>'2020-05-20 21:33:25',
            'active'=>'1',
            'user'=>0,
            'created_at'=>'2020-05-20 21:33:25',
            'updated_at'=>'2020-06-21 00:08:23',
            'deleted_at'=>NULL
            ] );
            
            
                        
            Post::create( [
            'id'=>5178,
            'cat_id'=>12,
            'tag_id'=>5,
            'title'=>'Decompressive Craniectomy Is Associated With Good Quality of Life Up to 10 Years After Rehabilitation From Traumatic Brain Injury',
            'text'=>'Rauen, Katrin and others.Critical Care Medicine. ., Post Author Corrections: May 18, 2020.',
            'img'=>'im1.jpg',
            'file'=>NULL,
            'link'=>'https://journals.lww.com/ccmjournal/Abstract/9000/Decompressive_Craniectomy_Is_Associated_With_Good.95643.aspx',
            'by'=>0,
            'journal'=>'Site Admin',
            'type'=>0,
            'month_article'=>0,
            'famous'=>0,
            'views'=>242,
            'show'=>0,
            'date'=>'2020-05-20 21:35:39',
            'active'=>'1',
            'user'=>0,
            'created_at'=>'2020-05-20 21:35:39',
            'updated_at'=>'2020-06-21 00:08:23',
            'deleted_at'=>NULL
            ] );
            
            
                        
            Post::create( [
            'id'=>5179,
            'cat_id'=>8,
            'tag_id'=>5,
            'title'=>'Early Administration of Desmopressin and Platelet Transfusion for Reducing Hematoma Expansion in Patients With Acute Antiplatelet Therapy Associated Intracerebral Hemorrhage',
            'text'=>'Mengel, Annerose and others.Critical Care Medicine. ., Post Author Corrections: April 16, 2020',
            'img'=>'im2.jpg',
            'file'=>NULL,
            'link'=>'https://journals.lww.com/ccmjournal/Abstract/9000/Early_Administration_of_Desmopressin_and_Platelet.95691.aspx',
            'by'=>0,
            'journal'=>'Site Admin',
            'type'=>0,
            'month_article'=>0,
            'famous'=>0,
            'views'=>131,
            'show'=>0,
            'date'=>'2020-05-20 22:49:09',
            'active'=>'1',
            'user'=>0,
            'created_at'=>'2020-05-20 22:49:09',
            'updated_at'=>'2020-06-21 00:08:23',
            'deleted_at'=>NULL
            ] );
            
            
                        
            Post::create( [
            'id'=>5180,
            'cat_id'=>8,
            'tag_id'=>1,
            'title'=>'Saving Lives Versus Saving Dollars: The Acceptable Loss for Coronavirus Disease 2019',
            'text'=>'Ashkenazi, Isaac; Rapaport, CarmitCritical Care Medicine. ., Post Author Corrections: May 18, 2020',
            'img'=>'covid-9.jpg',
            'file'=>NULL,
            'link'=>'https://journals.lww.com/ccmjournal/Citation/9000/Saving_Lives_Versus_Saving_Dollars__The_Acceptable.95644.aspx',
            'by'=>0,
            'journal'=>'Site Admin',
            'type'=>0,
            'month_article'=>0,
            'famous'=>0,
            'views'=>147,
            'show'=>0,
            'date'=>'2020-05-20 22:52:21',
            'active'=>'1',
            'user'=>0,
            'created_at'=>'2020-05-20 22:52:21',
            'updated_at'=>'2020-06-21 00:08:23',
            'deleted_at'=>NULL
            ] );
            
            
                        
            Post::create( [
            'id'=>5181,
            'cat_id'=>8,
            'tag_id'=>1,
            'title'=>'Coronavirus Disease 2019 Triage Teams: Death by Numbers',
            'text'=>'Zivot, JoelCritical Care Medicine. ., Post Author Corrections: May 15, 2020',
            'img'=>'covid-10.jpg',
            'file'=>NULL,
            'link'=>'https://journals.lww.com/ccmjournal/Citation/9000/Coronavirus_Disease_2019_Triage_Teams__Death_by.95647.aspx',
            'by'=>0,
            'journal'=>'Site Admin',
            'type'=>0,
            'month_article'=>0,
            'famous'=>0,
            'views'=>147,
            'show'=>0,
            'date'=>'2020-05-20 22:54:40',
            'active'=>'1',
            'user'=>0,
            'created_at'=>'2020-05-20 22:54:40',
            'updated_at'=>'2020-06-21 00:08:23',
            'deleted_at'=>NULL
            ] );
            
            
                        
            Post::create( [
            'id'=>5182,
            'cat_id'=>8,
            'tag_id'=>1,
            'title'=>'Dealing With the CARDS of COVID-19',
            'text'=>'Marini, John J.Critical Care Medicine. ., Post Author Corrections: May 13, 2020',
            'img'=>'covid-1.jpg',
            'file'=>NULL,
            'link'=>'https://journals.lww.com/ccmjournal/Citation/9000/Dealing_With_the_CARDS_of_COVID_19.95649.aspx',
            'by'=>0,
            'journal'=>'Site Admin',
            'type'=>0,
            'month_article'=>0,
            'famous'=>0,
            'views'=>147,
            'show'=>0,
            'date'=>'2020-05-20 22:58:17',
            'active'=>'1',
            'user'=>0,
            'created_at'=>'2020-05-20 22:58:17',
            'updated_at'=>'2020-06-21 00:08:23',
            'deleted_at'=>NULL
            ] );
            
            
                        
            Post::create( [
            'id'=>5183,
            'cat_id'=>8,
            'tag_id'=>1,
            'title'=>'Capitalizing on Immune Responses to Covid-19',
            'text'=>'E.J. Rubin, L.R. Baden, and S. Morrissey',
            'img'=>'covid-4.jpg',
            'file'=>NULL,
            'link'=>'https://www.nejm.org/doi/full/10.1056/NEJMe2019020?query=featured_home',
            'by'=>0,
            'journal'=>'Site Admin',
            'type'=>0,
            'month_article'=>0,
            'famous'=>0,
            'views'=>147,
            'show'=>0,
            'date'=>'2020-05-20 23:03:15',
            'active'=>'1',
            'user'=>0,
            'created_at'=>'2020-05-20 23:03:15',
            'updated_at'=>'2020-06-21 00:08:23',
            'deleted_at'=>NULL
            ] );
            
            
                        
            Post::create( [
            'id'=>5184,
            'cat_id'=>18,
            'tag_id'=>1,
            'title'=>'Personal Protective Equipment and Covid-19',
            'text'=>'Rafael Ortega, M.D., Mauricio Gonzalez, M.D., Ala Nozari, M.D., Ph.D., and Robert Canelli, M.D.N Eng J Medicine, May 19, 2020',
            'img'=>'covid-5.jpg',
            'file'=>NULL,
            'link'=>'https://www.nejm.org/doi/full/10.1056/NEJMvcm2014809?query=featured_home',
            'by'=>0,
            'journal'=>'Site Admin',
            'type'=>0,
            'month_article'=>0,
            'famous'=>0,
            'views'=>147,
            'show'=>0,
            'date'=>'2020-05-20 23:06:27',
            'active'=>'1',
            'user'=>0,
            'created_at'=>'2020-05-20 23:06:27',
            'updated_at'=>'2020-06-21 00:08:23',
            'deleted_at'=>NULL
            ] );
            
            
                        
            Post::create( [
            'id'=>5185,
            'cat_id'=>8,
            'tag_id'=>1,
            'title'=>'Universal Masking in Hospitals in the Covid-19 Era',
            'text'=>'Michael Klompas and othersN Engl J Med 2020; 382:e63, May 21, 2020',
            'img'=>'covid-12.jpg',
            'file'=>NULL,
            'link'=>'https://www.nejm.org/doi/full/10.1056/NEJMp2006372?query=featured_home',
            'by'=>0,
            'journal'=>'Site Admin',
            'type'=>0,
            'month_article'=>0,
            'famous'=>0,
            'views'=>147,
            'show'=>0,
            'date'=>'2020-05-20 23:11:04',
            'active'=>'1',
            'user'=>0,
            'created_at'=>'2020-05-20 23:11:04',
            'updated_at'=>'2020-06-21 00:08:23',
            'deleted_at'=>NULL
            ] );
            
            
                        
            Post::create( [
            'id'=>5186,
            'cat_id'=>8,
            'tag_id'=>5,
            'title'=>'Endovascular Thrombectomy with or without Intravenous Alteplase in Acute Stroke',
            'text'=>'Pengfei Yang and others, for the DIRECT-MT Investigators.N Engl J Med 2020; 382:1981-1993, May 21, 2020',
            'img'=>'im3.jpg',
            'file'=>NULL,
            'link'=>'https://www.nejm.org/doi/full/10.1056/NEJMoa2001123?query=featured_home',
            'by'=>0,
            'journal'=>'Site Admin',
            'type'=>0,
            'month_article'=>0,
            'famous'=>1,
            'views'=>226,
            'show'=>0,
            'date'=>'2020-05-20 23:14:20',
            'active'=>'1',
            'user'=>0,
            'created_at'=>'2020-05-20 23:14:20',
            'updated_at'=>'2020-06-21 00:08:23',
            'deleted_at'=>NULL
            ] );
            
            
                        
            Post::create( [
            'id'=>5187,
            'cat_id'=>8,
            'tag_id'=>1,
            'title'=>'Developing Covid-19 Vaccines at Pandemic Speed',
            'text'=>'Nicole Lurie, and others.N Engl J Med 2020; 382:1969-1973.',
            'img'=>'covid-12.jpg',
            'file'=>NULL,
            'link'=>'https://www.nejm.org/doi/full/10.1056/NEJMp2005630',
            'by'=>0,
            'journal'=>'Site Admin',
            'type'=>0,
            'month_article'=>0,
            'famous'=>0,
            'views'=>118,
            'show'=>0,
            'date'=>'2020-05-20 23:17:50',
            'active'=>'1',
            'user'=>0,
            'created_at'=>'2020-05-20 23:17:50',
            'updated_at'=>'2020-06-21 00:08:23',
            'deleted_at'=>NULL
            ] );
            
            
                        
            Post::create( [
            'id'=>5188,
            'cat_id'=>8,
            'tag_id'=>1,
            'title'=>'Covid-19 in Critically Ill Patients in the Seattle Region — Case Series',
            'text'=>'Pavan K. Bhatraju, and others.N Engl J Med 2020; 382:2012-2022. May 21, 2020',
            'img'=>'covid-1.jpg',
            'file'=>NULL,
            'link'=>'https://www.nejm.org/doi/full/10.1056/NEJMoa2004500',
            'by'=>0,
            'journal'=>'Site Admin',
            'type'=>0,
            'month_article'=>0,
            'famous'=>0,
            'views'=>118,
            'show'=>0,
            'date'=>'2020-05-20 23:19:56',
            'active'=>'1',
            'user'=>0,
            'created_at'=>'2020-05-20 23:19:56',
            'updated_at'=>'2020-06-21 00:08:23',
            'deleted_at'=>NULL
            ] );
            
            
                        
            Post::create( [
            'id'=>5189,
            'cat_id'=>8,
            'tag_id'=>2,
            'title'=>'Factors Disrupting Melatonin Secretion Rhythms During Critical Illness',
            'text'=>'Maas, Matthew B, and othersCritical Care Medicine. 48(6):854-861, June 2020.',
            'img'=>'im1.jpg',
            'file'=>NULL,
            'link'=>'https://journals.lww.com/ccmjournal/Abstract/2020/06000/Factors_Disrupting_Melatonin_Secretion_Rhythms.11.aspx',
            'by'=>0,
            'journal'=>'Site Admin',
            'type'=>0,
            'month_article'=>0,
            'famous'=>0,
            'views'=>189,
            'show'=>0,
            'date'=>'2020-06-01 19:43:48',
            'active'=>'1',
            'user'=>0,
            'created_at'=>'2020-06-01 19:43:48',
            'updated_at'=>'2020-06-21 00:08:23',
            'deleted_at'=>NULL
            ] );
            
            
                        
            Post::create( [
            'id'=>5190,
            'cat_id'=>8,
            'tag_id'=>2,
            'title'=>'The Reemergence of Ketamine for Treatment in Critically Ill Adults',
            'text'=>'Hurth, Kimberly P. and others.Critical Care Medicine. 48(6):899-911, June 2020.',
            'img'=>'im2.jpg',
            'file'=>NULL,
            'link'=>'https://journals.lww.com/ccmjournal/Abstract/2020/06000/The_Reemergence_of_Ketamine_for_Treatment_in.17.aspx',
            'by'=>0,
            'journal'=>'Site Admin',
            'type'=>0,
            'month_article'=>0,
            'famous'=>0,
            'views'=>189,
            'show'=>0,
            'date'=>'2020-06-01 19:47:04',
            'active'=>'1',
            'user'=>0,
            'created_at'=>'2020-06-01 19:47:04',
            'updated_at'=>'2020-06-21 00:08:23',
            'deleted_at'=>NULL
            ] );
            
            
                        
            Post::create( [
            'id'=>5191,
            'cat_id'=>12,
            'tag_id'=>1,
            'title'=>'Continuous Versus Intermittent Infusion of Vancomycin and the Risk of Acute Kidney Injury in Critically Ill Adults: A Systematic Review and Meta-Analysis*',
            'text'=>'Flannery, Alexander H, and others.Critical Care Medicine. 48(6):912-918, June 2020.',
            'img'=>'file-1464106363-HWQX6B.jpg',
            'file'=>NULL,
            'link'=>'https://journals.lww.com/ccmjournal/Abstract/2020/06000/Continuous_Versus_Intermittent_Infusion_of.18.aspx',
            'by'=>0,
            'journal'=>'Site Admin',
            'type'=>0,
            'month_article'=>0,
            'famous'=>1,
            'views'=>353,
            'show'=>0,
            'date'=>'2020-06-01 19:50:22',
            'active'=>'1',
            'user'=>0,
            'created_at'=>'2020-06-01 19:50:22',
            'updated_at'=>'2020-06-21 00:08:23',
            'deleted_at'=>NULL
            ] );
            
            
                        
            Post::create( [
            'id'=>5192,
            'cat_id'=>8,
            'tag_id'=>7,
            'title'=>'Prediction of Symptomatic Venous Thromboembolism in Critically Ill Patients: The ICU-Venous Thromboembolism Score',
            'text'=>'Viarasilpa, Tanuwong, and others.Critical Care Medicine. 48(6):e470-e479, June 2020',
            'img'=>'file-1463591644-ZGN2DA.jpg',
            'file'=>NULL,
            'link'=>'https://journals.lww.com/ccmjournal/Abstract/2020/06000/Prediction_of_Symptomatic_Venous_Thromboembolism.30.aspx',
            'by'=>0,
            'journal'=>'Site Admin',
            'type'=>0,
            'month_article'=>0,
            'famous'=>1,
            'views'=>215,
            'show'=>0,
            'date'=>'2020-06-01 19:55:18',
            'active'=>'1',
            'user'=>0,
            'created_at'=>'2020-06-01 19:55:18',
            'updated_at'=>'2020-06-24 09:24:52',
            'deleted_at'=>NULL
            ] );
            
            
                        
            Post::create( [
            'id'=>5193,
            'cat_id'=>12,
            'tag_id'=>1,
            'title'=>'New Data on Remdesivir in Covid-19 ',
            'text'=>'Eric J. Rubin, M.D., Ph.D., Lindsey R. Baden, M.D., and Stephen Morrissey, Ph.D.N Engl J Med 2020; 382:e94',
            'img'=>'covid-3.jpg',
            'file'=>NULL,
            'link'=>'https://www.nejm.org/doi/full/10.1056/NEJMe2019975?query=featured_home',
            'by'=>0,
            'journal'=>'Site Admin',
            'type'=>0,
            'month_article'=>0,
            'famous'=>0,
            'views'=>258,
            'show'=>0,
            'date'=>'2020-06-01 20:34:20',
            'active'=>'1',
            'user'=>0,
            'created_at'=>'2020-06-01 20:34:20',
            'updated_at'=>'2020-06-21 00:08:23',
            'deleted_at'=>NULL
            ] );
            
            
                        
            Post::create( [
            'id'=>5194,
            'cat_id'=>8,
            'tag_id'=>1,
            'title'=>'Severe Covid-19',
            'text'=>'David A. Berlin, M.D., Roy M. Gulick, M.D., M.P.H., and Fernando J. Martinez, M.D.N Eng J Med. ',
            'img'=>'covid-2.jpg',
            'file'=>NULL,
            'link'=>'https://www.nejm.org/doi/full/10.1056/NEJMcp2009575?query=featured_home',
            'by'=>0,
            'journal'=>'Site Admin',
            'type'=>0,
            'month_article'=>0,
            'famous'=>0,
            'views'=>147,
            'show'=>0,
            'date'=>'2020-06-01 20:37:27',
            'active'=>'1',
            'user'=>0,
            'created_at'=>'2020-06-01 20:37:27',
            'updated_at'=>'2020-06-21 00:08:23',
            'deleted_at'=>NULL
            ] );
            
            
                        
            Post::create( [
            'id'=>5195,
            'cat_id'=>18,
            'tag_id'=>1,
            'title'=>'How to Obtain a Nasopharyngeal Swab Specimen',
            'text'=>'Francisco M. Marty, M.D., Kaiwen Chen, B.S., and Kelly A. Verrill, R.N.N Engl J Med 2020; 382:e76',
            'img'=>'file-1464165480-R231XR.jpg',
            'file'=>NULL,
            'link'=>'https://www.nejm.org/doi/full/10.1056/NEJMvcm2010260',
            'by'=>0,
            'journal'=>'Site Admin',
            'type'=>0,
            'month_article'=>0,
            'famous'=>0,
            'views'=>259,
            'show'=>0,
            'date'=>'2020-06-01 20:40:40',
            'active'=>'1',
            'user'=>0,
            'created_at'=>'2020-06-01 20:40:40',
            'updated_at'=>'2020-11-30 12:07:03',
            'deleted_at'=>NULL
            ] );
            
            
                        
            Post::create( [
            'id'=>5196,
            'cat_id'=>12,
            'tag_id'=>1,
            'title'=>'A Randomized Trial of Hydroxychloroquine as Postexposure Prophylaxis for Covid-19',
            'text'=>'David R. Boulware and others',
            'img'=>'file-1464165463-D3CBM9.jpg',
            'file'=>NULL,
            'link'=>'https://www.nejm.org/doi/full/10.1056/NEJMoa2016638',
            'by'=>0,
            'journal'=>'N Eng J Med; June 3, 2020',
            'type'=>0,
            'month_article'=>0,
            'famous'=>0,
            'views'=>230,
            'show'=>0,
            'date'=>'2020-06-09 21:27:20',
            'active'=>'1',
            'user'=>0,
            'created_at'=>'2020-06-09 21:27:20',
            'updated_at'=>'2020-06-24 09:25:02',
            'deleted_at'=>NULL
            ] );
            
            
                        
            Post::create( [
            'id'=>5197,
            'cat_id'=>12,
            'tag_id'=>6,
            'title'=>'Low-cost dexamethasone reduces death by up to one third in hospitalized patients with severe\r\nrespiratory complications of COVID-19',
            'text'=>'Oxford University News Release EMBARGOED UNTIL 16 June 2020, 13:00 (UK Time)',
            'img'=>'file-1464105730-LQ3Z2Q.jpg',
            'file'=>NULL,
            'link'=>'https://www.recoverytrial.net/files/recovery_dexamethasone_statement_160620_v2final.pdf',
            'by'=>0,
            'journal'=>'Site Admin',
            'type'=>0,
            'month_article'=>0,
            'famous'=>0,
            'views'=>203,
            'show'=>0,
            'date'=>'2020-06-17 11:45:49',
            'active'=>'1',
            'user'=>0,
            'created_at'=>'2020-06-17 11:45:49',
            'updated_at'=>'2020-08-01 16:03:16',
            'deleted_at'=>NULL
            ] );
            
            
                        
            Post::create( [
            'id'=>5198,
            'cat_id'=>8,
            'tag_id'=>1,
            'title'=>'Recovery trial for Covid-19 treatments: what we know so far',
            'text'=>'The biggest randomised controlled trial of drugs against Covid-19 in the world is already producing results',
            'img'=>'file-1464165480-R231XR.jpg',
            'file'=>NULL,
            'link'=>'https://www.theguardian.com/world/2020/jun/16/recovery-trial-for-covid-19-treatments-what-we-know-so-far',
            'by'=>0,
            'journal'=>'The Guardian',
            'type'=>0,
            'month_article'=>0,
            'famous'=>0,
            'views'=>120,
            'show'=>0,
            'date'=>'2020-06-17 11:48:05',
            'active'=>'1',
            'user'=>0,
            'created_at'=>'2020-06-17 11:48:05',
            'updated_at'=>'2020-06-23 19:25:16',
            'deleted_at'=>NULL
            ] );
            
            
                        
            Post::create( [
            'id'=>5199,
            'cat_id'=>8,
            'tag_id'=>1,
            'title'=>'Association Between Arterial Carbon Dioxide Tension and Clinical Outcomes in Venoarterial Extracorporeal Membrane Oxygenation',
            'text'=>'Diehl, Arne and others',
            'img'=>'file-1463591639-4YG8JY.jpg',
            'file'=>NULL,
            'link'=>'https://journals.lww.com/ccmjournal/Abstract/2020/07000/Association_Between_Arterial_Carbon_Dioxide.6.aspx',
            'by'=>0,
            'journal'=>'Critical Care Medicine. 48(7):977-984, July 2020',
            'type'=>0,
            'month_article'=>0,
            'famous'=>0,
            'views'=>0,
            'show'=>0,
            'date'=>'2020-07-05 12:16:10',
            'active'=>'1',
            'user'=>0,
            'created_at'=>'2020-07-05 12:16:10',
            'updated_at'=>'2020-07-05 12:16:10',
            'deleted_at'=>NULL
            ] );
            
            
                        
            Post::create( [
            'id'=>5200,
            'cat_id'=>8,
            'tag_id'=>10,
            'title'=>'Concomitant Aspirin and Anticoagulation Is Associated With Increased Risk for Major Bleeding in Surgical Patients Requiring Postoperative Intensive Care',
            'text'=>'Rayes, Hamza A and others',
            'img'=>'file-1463591685-DDLMK9.jpg',
            'file'=>NULL,
            'link'=>'https://journals.lww.com/ccmjournal/Abstract/2020/07000/Concomitant_Aspirin_and_Anticoagulation_Is.7.aspx',
            'by'=>0,
            'journal'=>'Critical Care Medicine. 48(7):985-992, July 2020',
            'type'=>0,
            'month_article'=>0,
            'famous'=>0,
            'views'=>0,
            'show'=>0,
            'date'=>'2020-07-05 12:18:23',
            'active'=>'1',
            'user'=>0,
            'created_at'=>'2020-07-05 12:18:23',
            'updated_at'=>'2020-07-05 12:16:10',
            'deleted_at'=>NULL
            ] );
            
            
                        
            Post::create( [
            'id'=>5201,
            'cat_id'=>8,
            'tag_id'=>1,
            'title'=>'Early Administration of Desmopressin and Platelet Transfusion for Reducing Hematoma Expansion in Patients With Acute Antiplatelet Therapy Associated Intracerebral Hemorrhage*',
            'text'=>'Mengel, Annerose and others',
            'img'=>'im2.jpg',
            'file'=>NULL,
            'link'=>'https://journals.lww.com/ccmjournal/Abstract/2020/07000/Early_Administration_of_Desmopressin_and_Platelet.10.aspx',
            'by'=>0,
            'journal'=>'Critical Care Medicine. 48(7):1009-1017, July 2020',
            'type'=>0,
            'month_article'=>0,
            'famous'=>0,
            'views'=>0,
            'show'=>0,
            'date'=>'2020-07-05 12:21:13',
            'active'=>'1',
            'user'=>0,
            'created_at'=>'2020-07-05 12:21:13',
            'updated_at'=>'2020-07-05 12:16:10',
            'deleted_at'=>NULL
            ] );
            
            
                        
            Post::create( [
            'id'=>5202,
            'cat_id'=>8,
            'tag_id'=>1,
            'title'=>'Nebulized Bacteriophages for Prophylaxis of Experimental Ventilator-Associated Pneumonia Due to Methicillin-Resistant Staphylococcus aureus',
            'text'=>'Prazak, Josef and others',
            'img'=>'im6.jpg',
            'file'=>NULL,
            'link'=>'https://journals.lww.com/ccmjournal/Fulltext/2020/07000/Nebulized_Bacteriophages_for_Prophylaxis_of.14.aspx',
            'by'=>0,
            'journal'=>'Critical Care Medicine. 48(7):1042-1046, July 2020',
            'type'=>0,
            'month_article'=>0,
            'famous'=>0,
            'views'=>0,
            'show'=>0,
            'date'=>'2020-07-05 12:25:06',
            'active'=>'1',
            'user'=>0,
            'created_at'=>'2020-07-05 12:25:06',
            'updated_at'=>'2020-07-05 12:16:10',
            'deleted_at'=>NULL
            ] );
            
            
                        
            Post::create( [
            'id'=>5203,
            'cat_id'=>8,
            'tag_id'=>1,
            'title'=>'Therapeutic Hypothermia in Critically Ill Patients: A Systematic Review and Meta-Analysis of High-Quality Randomized Trials',
            'text'=>'Kim, Jun Hyun, and others',
            'img'=>'covid-9.jpg',
            'file'=>NULL,
            'link'=>'https://journals.lww.com/ccmjournal/Abstract/2020/07000/Therapeutic_Hypothermia_in_Critically_Ill.15.aspx',
            'by'=>0,
            'journal'=>'Critical Care Medicine. 48(7):1047-1054, July 2020',
            'type'=>0,
            'month_article'=>0,
            'famous'=>0,
            'views'=>0,
            'show'=>0,
            'date'=>'2020-07-05 12:28:12',
            'active'=>'1',
            'user'=>0,
            'created_at'=>'2020-07-05 12:28:12',
            'updated_at'=>'2020-07-05 12:16:10',
            'deleted_at'=>NULL
            ] );
            
            
                        
            Post::create( [
            'id'=>5204,
            'cat_id'=>8,
            'tag_id'=>1,
            'title'=>'Iran records highest daily death toll from COVID-19',
            'text'=>'Iran recorded its highest number of deaths from COVID-19 within a 24-hour period, official health ministry figures showed on Sunday',
            'img'=>'covid-1.jpg',
            'file'=>NULL,
            'link'=>'https://www.reuters.com/article/us-health-coronavirus-iran/iran-records-highest-daily-death-toll-from-covid-19-idUSKBN2460FL',
            'by'=>0,
            'journal'=>'Reuters Health - July 5, 8:17 AM EDT',
            'type'=>0,
            'month_article'=>0,
            'famous'=>0,
            'views'=>0,
            'show'=>0,
            'date'=>'2020-07-05 12:31:17',
            'active'=>'1',
            'user'=>0,
            'created_at'=>'2020-07-05 12:31:17',
            'updated_at'=>'2020-07-05 12:16:10',
            'deleted_at'=>NULL
            ] );
            
            
                        
            Post::create( [
            'id'=>5205,
            'cat_id'=>8,
            'tag_id'=>1,
            'title'=>'Letter to the Editor in response to “COVID-19: desperate times call for desperate measures”\r\n',
            'text'=>'J. Geoffrey Chase, Yeong-Shiong Chiew, Bernard Lambermont, Philippe Morimont, Geoffrey M. Shaw and Thomas Desaive',
            'img'=>'im6.jpg',
            'file'=>NULL,
            'link'=>'https://ccforum.biomedcentral.com/articles/10.1186/s13054-020-03152-6',
            'by'=>0,
            'journal'=>'Critical Care 2020, 24:415 | Published on: 10 July 2020',
            'type'=>0,
            'month_article'=>0,
            'famous'=>0,
            'views'=>0,
            'show'=>0,
            'date'=>'2020-07-11 13:17:29',
            'active'=>'1',
            'user'=>0,
            'created_at'=>'2020-07-11 13:17:29',
            'updated_at'=>'2020-07-05 12:16:10',
            'deleted_at'=>NULL
            ] );
            
            
                        
            Post::create( [
            'id'=>5206,
            'cat_id'=>12,
            'tag_id'=>1,
            'title'=>'COVID-19 UPDATES',
            'text'=>'All most recent research updates on COVID-19 epidemics published in the journal of critical care',
            'img'=>'covid-1.jpg',
            'file'=>NULL,
            'link'=>'https://ccforum.biomedcentral.com/covid-19-updates',
            'by'=>0,
            'journal'=>'Site Admin',
            'type'=>0,
            'month_article'=>0,
            'famous'=>0,
            'views'=>1,
            'show'=>0,
            'date'=>'2020-07-11 13:20:22',
            'active'=>'1',
            'user'=>0,
            'created_at'=>'2020-07-11 13:20:22',
            'updated_at'=>'2020-07-22 09:46:41',
            'deleted_at'=>NULL
            ] );
            
            
                        
            Post::create( [
            'id'=>5207,
            'cat_id'=>8,
            'tag_id'=>1,
            'title'=>'Selected articles from the Annual Update in Intensive Care and Emergency Medicine 2020',
            'text'=>'This series of articles is published in Critical Care',
            'img'=>'file-1464165480-R231XR.jpg',
            'file'=>NULL,
            'link'=>'https://www.biomedcentral.com/collections/annualupdate2020',
            'by'=>0,
            'journal'=>'Site Admin',
            'type'=>0,
            'month_article'=>0,
            'famous'=>0,
            'views'=>0,
            'show'=>0,
            'date'=>'2020-07-11 13:22:20',
            'active'=>'1',
            'user'=>0,
            'created_at'=>'2020-07-11 13:22:20',
            'updated_at'=>'2020-07-05 12:16:10',
            'deleted_at'=>NULL
            ] );
            
            
                        
            Post::create( [
            'id'=>5208,
            'cat_id'=>8,
            'tag_id'=>1,
            'title'=>'Comparison of hydroxychloroquine, lopinavir/ritonavir, and standard of care in critically ill patients with SARS-CoV-2 pneumonia: an opportunistic retrospective analysis',
            'text'=>'Marie Lecronier and others',
            'img'=>'covid-2.jpg',
            'file'=>NULL,
            'link'=>'https://ccforum.biomedcentral.com/articles/10.1186/s13054-020-03117-9',
            'by'=>0,
            'journal'=>'Critical Care volume 24, Article number: 418 (2020)',
            'type'=>0,
            'month_article'=>0,
            'famous'=>0,
            'views'=>0,
            'show'=>0,
            'date'=>'2020-07-11 13:28:06',
            'active'=>'1',
            'user'=>0,
            'created_at'=>'2020-07-11 13:28:06',
            'updated_at'=>'2020-07-05 12:16:10',
            'deleted_at'=>NULL
            ] );
            
            
                        
            Post::create( [
            'id'=>5209,
            'cat_id'=>8,
            'tag_id'=>1,
            'title'=>'Audio Interview: Dexamethasone and Covid-19',
            'text'=>'Eric J. Rubin, M.D., Ph.D., Lindsey R. Baden, M.D., and Stephen Morrissey, Ph.D.',
            'img'=>'file-1464165463-D3CBM9.jpg',
            'file'=>NULL,
            'link'=>'https://www.nejm.org/doi/full/10.1056/NEJMe2025927?query=featured_home',
            'by'=>0,
            'journal'=>'N Engl J Med ; 383. July 23, 2020 ',
            'type'=>0,
            'month_article'=>0,
            'famous'=>0,
            'views'=>0,
            'show'=>0,
            'date'=>'2020-07-28 11:21:20',
            'active'=>'1',
            'user'=>0,
            'created_at'=>'2020-07-28 11:21:20',
            'updated_at'=>'2020-07-05 12:16:10',
            'deleted_at'=>NULL
            ] );
            
            
                        
            Post::create( [
            'id'=>5210,
            'cat_id'=>8,
            'tag_id'=>1,
            'title'=>'Hydroxychloroquine with or without Azithromycin in Mild-to-Moderate Covid-19',
            'text'=>'Alexandre B. Cavalcanti, et al, for the Coalition Covid-19 Brazil I Investigators',
            'img'=>'covid-1.jpg',
            'file'=>NULL,
            'link'=>'https://www.nejm.org/doi/full/10.1056/NEJMoa2019014?query=featured_home',
            'by'=>0,
            'journal'=>'N Engl J Med. July 23, 2020',
            'type'=>0,
            'month_article'=>0,
            'famous'=>1,
            'views'=>4,
            'show'=>0,
            'date'=>'2020-07-28 11:26:26',
            'active'=>'1',
            'user'=>0,
            'created_at'=>'2020-07-28 11:26:26',
            'updated_at'=>'2020-09-11 17:33:57',
            'deleted_at'=>NULL
            ] );
            
            
                        
            Post::create( [
            'id'=>5211,
            'cat_id'=>8,
            'tag_id'=>1,
            'title'=>'Dexamethasone in Hospitalized Patients with Covid-19 — Preliminary Report',
            'text'=>'The RECOVERY Collaborative Group',
            'img'=>'file-1464165480-R231XR.jpg',
            'file'=>NULL,
            'link'=>'https://www.nejm.org/doi/full/10.1056/NEJMoa2021436?query=featured_home',
            'by'=>0,
            'journal'=>'N Engl J Med. July 17, 2020',
            'type'=>0,
            'month_article'=>0,
            'famous'=>1,
            'views'=>0,
            'show'=>0,
            'date'=>'2020-07-28 11:32:07',
            'active'=>'1',
            'user'=>0,
            'created_at'=>'2020-07-28 11:32:07',
            'updated_at'=>'2020-07-05 12:16:10',
            'deleted_at'=>NULL
            ] );
            
            
                        
            Post::create( [
            'id'=>5212,
            'cat_id'=>12,
            'tag_id'=>1,
            'title'=>'Ticagrelor and Aspirin or Aspirin Alone in Acute Ischemic Stroke or TIA',
            'text'=>'S. Claiborne Johnston and others. for the THALES Investigators',
            'img'=>'im2.jpg',
            'file'=>NULL,
            'link'=>'https://www.nejm.org/doi/full/10.1056/NEJMoa1916870?query=featured_home',
            'by'=>0,
            'journal'=>'N Engl J Med; 383:207-217-July 16, 2020',
            'type'=>0,
            'month_article'=>0,
            'famous'=>0,
            'views'=>4,
            'show'=>0,
            'date'=>'2020-07-28 11:34:59',
            'active'=>'1',
            'user'=>0,
            'created_at'=>'2020-07-28 11:34:59',
            'updated_at'=>'2020-08-07 17:51:09',
            'deleted_at'=>NULL
            ] );
            
            
                        
            Post::create( [
            'id'=>5213,
            'cat_id'=>8,
            'tag_id'=>1,
            'title'=>'Coronavirus Disease 2019 Pandemic Measures: Reports From a National Survey of 9,120 ICU Clinicians',
            'text'=>'Kleinpell, Ruth, et al. ',
            'img'=>'covid-1.jpg',
            'file'=>NULL,
            'link'=>'https://journals.lww.com/ccmjournal/Fulltext/2020/10000/Coronavirus_Disease_2019_Pandemic_Measures_.27.aspx',
            'by'=>0,
            'journal'=>'Critical Care Medicine. 48(10):e846-e855, October 2020',
            'type'=>0,
            'month_article'=>0,
            'famous'=>1,
            'views'=>0,
            'show'=>0,
            'date'=>'2020-09-15 09:30:02',
            'active'=>'1',
            'user'=>0,
            'created_at'=>'2020-09-15 09:30:02',
            'updated_at'=>'2020-07-05 12:16:10',
            'deleted_at'=>NULL
            ] );
            
            
                        
            Post::create( [
            'id'=>5214,
            'cat_id'=>12,
            'tag_id'=>7,
            'title'=>'Beneficial Effects of Vasopressin Compared With Norepinephrine on Renal Perfusion, Oxygenation, and Function in Experimental Septic Acute Kidney Injury',
            'text'=>'Okazaki, Nobuki, and others',
            'img'=>'file-1463591639-4YG8JY.jpg',
            'file'=>NULL,
            'link'=>'https://journals.lww.com/ccmjournal/Abstract/2020/10000/Beneficial_Effects_of_Vasopressin_Compared_With.40.aspx',
            'by'=>0,
            'journal'=>'Critical Care Medicine. 48(10):e951-e958, October 2020.',
            'type'=>0,
            'month_article'=>0,
            'famous'=>1,
            'views'=>1,
            'show'=>0,
            'date'=>'2020-09-15 09:34:21',
            'active'=>'1',
            'user'=>0,
            'created_at'=>'2020-09-15 09:34:21',
            'updated_at'=>'2021-11-19 08:58:55',
            'deleted_at'=>NULL
            ] );
            
            
                        
            Post::create( [
            'id'=>5215,
            'cat_id'=>12,
            'tag_id'=>1,
            'title'=>'Practical strategies to reduce nosocomial transmission to healthcare professionals providing respiratory care to patients with COVID-19',
            'text'=>'Ramandeep Kaur and others',
            'img'=>'covid-10.jpg',
            'file'=>NULL,
            'link'=>'https://ccforum.biomedcentral.com/articles/10.1186/s13054-020-03231-8',
            'by'=>0,
            'journal'=>'Critical Care 2020, 24:571 | Published on: 23 September 2020',
            'type'=>0,
            'month_article'=>0,
            'famous'=>1,
            'views'=>1,
            'show'=>0,
            'date'=>'2020-09-24 10:03:57',
            'active'=>'1',
            'user'=>0,
            'created_at'=>'2020-09-24 10:03:57',
            'updated_at'=>'2021-11-19 08:59:07',
            'deleted_at'=>NULL
            ] );
            
            Post::create( [
            'id'=>5226,
            'cat_id'=>8,
            'tag_id'=>1,
            'title'=>'Surviving Sepsis Campaign: International Guidelines for Management of Sepsis and Septic Shock 2021\r\n',
            'text'=>'Evans, Laura1; Rhodes, Andrew, et al. ',
            'img'=>'file-1463591639-4YG8JY.jpg',
            'file'=>NULL,
            'link'=>'https://journals.lww.com/ccmjournal/Fulltext/2021/11000/Surviving_Sepsis_Campaign__International.21.aspx',
            'by'=>0,
            'journal'=>'Critical Care Medicine: November 2021 - Volume 49 - Issue 11 - p e1063-e1143',
            'type'=>0,
            'month_article'=>1,
            'famous'=>0,
            'views'=>0,
            'show'=>0,
            'date'=>'2021-12-01 05:00:00',
            'active'=>'1',
            'user'=>0,
            'created_at'=>'2021-12-10 20:02:32',
            'updated_at'=>'2020-07-05 12:16:10',
            'deleted_at'=>NULL
            ] );
            
            
                        
            Post::create( [
            'id'=>5227,
            'cat_id'=>8,
            'tag_id'=>1,
            'title'=>'21st Century Evidence: Randomized Controlled Trials Versus Systematic Reviews and Meta-Analyses\r\n',
            'text'=>'Agarwal, Ankita MD1; Rochwerg, Bram MD, MSc2; Sevransky, Jonathan E. MD, MHS, FCCM3',
            'img'=>'file-1464165480-R231XR.jpg',
            'file'=>NULL,
            'link'=>'https://journals.lww.com/ccmjournal/Fulltext/2021/12000/21st_Century_Evidence__Randomized_Controlled.1.aspx',
            'by'=>0,
            'journal'=>'Critical Care Medicine: December 2021 - Volume 49 - Issue 12 - p 2001-2002',
            'type'=>0,
            'month_article'=>1,
            'famous'=>0,
            'views'=>0,
            'show'=>0,
            'date'=>'2021-12-01 05:00:00',
            'active'=>'1',
            'user'=>0,
            'created_at'=>'2021-12-10 20:06:55',
            'updated_at'=>'2020-07-05 12:16:10',
            'deleted_at'=>NULL
            ] );
            
            Post::create( [
            'id'=>5228,
            'cat_id'=>8,
            'tag_id'=>1,
            'title'=>'Association Between an Increase in Serum Sodium and In-Hospital Mortality in Critically Ill Patients',
            'text'=>'Grim, Chloe C. A and others',
            'img'=>'file-1464106363-HWQX6B.jpg',
            'file'=>NULL,
            'link'=>'https://journals.lww.com/ccmjournal/Fulltext/2021/12000/Association_Between_an_Increase_in_Serum_Sodium.7.aspx',
            'by'=>0,
            'journal'=>'Critical Care Medicine. 49(12):2070-2079, December 2021',
            'type'=>0,
            'month_article'=>1,
            'famous'=>0,
            'views'=>0,
            'show'=>0,
            'date'=>'2021-12-10 05:00:00',
            'active'=>'1',
            'user'=>0,
            'created_at'=>'2021-12-10 20:23:14',
            'updated_at'=>'2021-12-10 20:26:53',
            'deleted_at'=>NULL
            ] );
            
            
                        
            Post::create( [
            'id'=>5229,
            'cat_id'=>8,
            'tag_id'=>1,
            'title'=>'Thiamine, Ascorbic Acid, and Hydrocortisone As a Metabolic Resuscitation Cocktail in Sepsis: A Meta-Analysis of Randomized Controlled Trials With Trial Sequential Analysis',
            'text'=>'Assouline, Benjamin and others',
            'img'=>'im5.jpg',
            'file'=>NULL,
            'link'=>'https://journals.lww.com/ccmjournal/Fulltext/2021/12000/Thiamine,_Ascorbic_Acid,_and_Hydrocortisone_As_a.11.aspx',
            'by'=>0,
            'journal'=>'Critical Care Medicine. 49(12):2112-2120, December 2021',
            'type'=>0,
            'month_article'=>1,
            'famous'=>0,
            'views'=>1,
            'show'=>0,
            'date'=>'2021-12-01 05:00:00',
            'active'=>'1',
            'user'=>0,
            'created_at'=>'2021-12-10 20:25:42',
            'updated_at'=>'2021-12-14 19:08:15',
            'deleted_at'=>NULL
            ] );
            
            
                        
            Post::create( [
            'id'=>5230,
            'cat_id'=>8,
            'tag_id'=>1,
            'title'=>'Effect of IV High-Dose Vitamin C on Mortality in Patients With Sepsis: A Systematic Review and Meta-Analysis of Randomized Controlled Trials',
            'text'=>'Sato, Ryota; Hasegawa and others',
            'img'=>'file-1463591725-9J88SF.jpg',
            'file'=>NULL,
            'link'=>'https://journals.lww.com/ccmjournal/Fulltext/2021/12000/Effect_of_IV_High_Dose_Vitamin_C_on_Mortality_in.12.aspx',
            'by'=>0,
            'journal'=>'Critical Care Medicine. 49(12):2121-2130, December 2021',
            'type'=>0,
            'month_article'=>1,
            'famous'=>0,
            'views'=>0,
            'show'=>0,
            'date'=>'2021-12-01 05:00:00',
            'active'=>'1',
            'user'=>0,
            'created_at'=>'2021-12-10 20:29:24',
            'updated_at'=>'2020-07-05 12:16:10',
            'deleted_at'=>NULL
            ] );
                                                        
                                                                     
                            
    }
}
