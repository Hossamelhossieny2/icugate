<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use App\Models\TeleConsultation;

class TeleConsultaionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        TeleConsultation::create([
            'id' => 1,
            'name' => 'Email',
            'price' => 50,
            'class' => 'fa-envelope-o'
        ]);

        TeleConsultation::create([
            'id' => 2,
            'name' => 'Zoom',
            'price' => 100,
            'class' => 'fa-phone'
        ]);
    }
}
