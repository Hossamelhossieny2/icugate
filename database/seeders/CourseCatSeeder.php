<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use App\Models\CourseCat;

class CourseCatSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        CourseCat::create([
            'id' => 1,
            'name' => 'Critical Care Educational Support Professionals',
            'playlist' => 'Critical+Care+Educational+Support+Professionals'
        ]);

        CourseCat::create([
            'id' => 2,
            'name' => 'Critical Care Ultrasound',
            'playlist' => 'Critical+Care+Ultrasound'
        ]);

        CourseCat::create([
            'id' => 3,
            'name' => 'Online Course',
            'playlist' => 'Online+Course'
        ]);
    }
}
