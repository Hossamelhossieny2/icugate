<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use App\Models\Critical;

class CriticalSeeder extends Seeder
{
    
    public function run()
    {
        Critical::create([
            'id' => 1,
            'date_from' => "2022-07-12",
            'date_to' => "2022-07-16",
            'event' => "CRITICAL CARE SOCIETY OF SOUTH AFRICA CONGRESS 2020",
            'country' => "SOUTH AFRICA",
            'city' => "Cape Town",
            'link' => "https://criticalcare.org.za/event/11th-congress-of-the-world-federation-of-pediatric-intensive-critical-care-societies/"
        ]);

        Critical::create([
            'id' => 2,
            'date_from' => "2022-11-23",
            'date_to' => "2022-11-25",
            'event' => "Critical Care Canada Forum 2020",
            'country' => "CANADA",
            'city' => "Toronto",
            'link' => "https://criticalcarecanada.com/"
        ]);

        Critical::create([
            'id' => 3,
            'date_from' => "2022-10-22",
            'date_to' => "2022-10-26",
            'event' => "35th ESICM Lives 2022",
            'country' => "France",
            'city' => "Paris",
            'link' => "https://www.esicm.org/events/35th-annual-congress/"
        ]);

        Critical::create([
            'id' => 4,
            'date_from' => "2022-05-13",
            'date_to' => "2022-05-15",
            'event' => "Emirates Critical Care Conference (ECCC 2022)",
            'country' => "United Arab of Emirates",
            'city' => "Dubai",
            'link' => "https://eccc-dubai.com/"
        ]);

        Critical::create([
            'id' => 5,
            'date_from' => "2022-03-22",
            'date_to' => "2022-03-25",
            'event' => "41ST INTERNATIONAL SYMPOSIUM ON INTENSIVE CARE & EMERGENCY MEDICINE",
            'country' => "Belgium",
            'city' => "Brussels",
            'link' => "https://www.isicem.org/1/Home.asp"
        ]);
    }
}
