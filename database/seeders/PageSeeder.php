<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use App\Models\Page;

class PageSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Page::create([
            'id' => 1,
            'name' => "Why ICU Gate ?",
            'content' => " It started as a discussion between two intensivists about the need of an easier way to follow the important literature and news in the field of critical care. The idea of departmental information sharing was soon upgraded to an intention of gathering these and similar tips into a website. Our chairman encouraged and supported us. A third and then a fourth colleague joined us.
            Before meeting with the website designer, a second important feature was decided. The critical care community needs also a webyard to share their efforts, experiences, comments, questions and concerns. Many times at the other end of the globe, a sincere enthusiast spends lengthy hours to prepare an updated beautiful power point that deserves sharing. It ends to be heard by his department staff only. Another has an important clinical tip he would like to put for discussion. And a third raises a clever question that may open the minds for new thinking or understanding. We added this second feature to our work. We strongly believe that the era of one way knowledge is of the past.We are more than open. We genuinely feel this is your website as it is ours. Your contributions are highly appreciated and your corrections and suggestions will be taken care of.
            Thank you.
            Khaled Sewify & Waleed Jasim ",
            'content2' => " Important Notes:  This website is a non-profit project.  During surfing on icugate, you may be redirected to freely available materials outside icugate including major critical care websites such as SCCM. We are not affiliated with any of these website owners and the content of these materials belong the owner of their original publisher.  Icugate is in no way responsible for any harm that may happen to the patients by following any of the materials seen on icugate. We also do not endorse the recommendations set by the websites we direct you to.   Any visitor to icugate is free to use any material on the website provided he/she mentions the source. For courtesy purposes, we ask you to mention also the source we used if we direct you outside icugate.",
            'img' => "file-1464106407-4C9NE9.jpg"
        ]);
    }
}
