<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use App\Models\PostType;

class PostTypeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
            PostType::create( [
            'id'=>1,
            'title'=>'link to other site',
            'type'=>0
            ] );
            
            PostType::create( [
                'id'=>2,
                'title'=>'internally news',
                'type'=>1
                ] );
            
                        
            PostType::create( [
            'id'=>3,
            'title'=>'research',
            'type'=>2
            ] );
            
            
                        
            PostType::create( [
            'id'=>4,
            'title'=>'presentation',
            'type'=>3
            ] );
    }
}
