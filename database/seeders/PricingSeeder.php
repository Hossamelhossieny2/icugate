<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use App\Models\Pricing;

class PricingSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Pricing::create([
            'id' => 1,
            'type' => 0,
            'title' => "Free Plan",
            'price' => 0,
            'popular' => 0,
            'course_percent' => 0.00,
            'ccesp_discount' => 0.00,
            'img' => "assets/images/free_member.png",
            'desc' => "Silver Member can view all the website ot but will not be able to view or download  presentations, videos, ICU procedures and protocols.",
            'brief' => "Great Follower and Reader",
            'videos' => 0,
            'protocols' => 0,
            'lectures' => 0,
            'contribution' => 0
        ]);

        Pricing::create([
            'id' => 2,
            'type' => 1,
            'title' => "Silver Plan",
            'price' => 20,
            'popular' => 1,
            'course_percent' => 2.50,
            'ccesp_discount' => 2.50,
            'img' => "assets/images/silver_member.png",
            'desc' => "Silver Member can view all the website as well as videos, presentations, ICU procedures and protocols and will get 2.5% discount in any CCESP course during the year of membership.",
            'brief' => "Advanced Specialist",
            'videos' => 1,
            'protocols' => 1,
            'lectures' => 0,
            'contribution' => 0
        ]);

        Pricing::create([
            'id' => 3,
            'type' => 2,
            'title' => "Gold Plan",
            'price' => 40,
            'popular' => 0,
            'course_percent' => 5.00,
            'ccesp_discount' => 5.00,
            'img' => "assets/images/gold_member.png",
            'desc' => "Gold Member may access all webpages, view and download presentations, videos, protocols, procedures as well as may publish articles or presentations. get 5 % discount in any CCESP course during the year of membership.",
            'brief' => "Contributor Partner",
            'videos' => 1,
            'protocols' => 1,
            'lectures' => 1,
            'contribution' => 1
        ]);
    }
}
