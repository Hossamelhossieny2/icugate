<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use App\Models\Tag;

class TagsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Tag::create([
            'id'=> 1,
            'title'=> 'SEPSIS',
            'image' => 'sepsis.jpg',
            'display' => 1
            ]);
        Tag::create([
            'id'=> 2,
            'title'=> 'SEDATION',
            'image' => 'sedation.jpg',
            'display' => 1
            ]);
        Tag::create([
            'id'=> 3,
            'title'=> 'NUTRITION',
            'image' => 'nutrition.jpg',
            'display' => 1
            ]);
        Tag::create([
            'id'=> 4,
            'title'=> 'TRAUAMA&SURGERY',
            'image' => 'surgery.jpg',
            'display' => 1
            ]);
        Tag::create([
            'id'=> 5,
            'title'=> 'NEURO-CRITICAL',
            'image' => 'neuro-critical.jpg',
            'display' => 1
            ]);
        Tag::create([
            'id'=> 6,
            'title'=> 'RESPIRATORY',
            'image' => 'respiratory.jpg',
            'display' => 1
            ]);
        Tag::create([
            'id'=> 7,
            'title'=> 'CARDIOVASCULAR',
            'image' => 'cardiovascular.jpg',
            'display' => 1
            ]);
        Tag::create([
            'id'=> 8,
            'title'=> 'RENAL',
            'image' => 'renal.jpg',
            'display' => 1
            ]);
        Tag::create([
            'id'=> 9,
            'title'=> 'GIT',
            'image' => '',
            'display' => 0
            ]);
        Tag::create([
            'id'=> 10,
            'title'=> 'HEMATOLGY',
            'image' => 'hematology.jpg',
            'display' => 1
            ]);
        Tag::create([
            'id'=> 11,
            'title'=> 'MISCELANEOUS',
            'image' => '',
            'display' => 0
            ]);
    }
}
