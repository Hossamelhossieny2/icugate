<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use App\Models\ContactInfo;

class ContactInfoSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        ContactInfo::create([
            'id'    => 1,
            'home_job_title' => "ICU GATE",
            'home_job_text' => "ICU Gate is a Specialized Website In Critical Care Medicine",
            'site_kw' => "Icugate,Medical,cources,News,Training",
            'site_des' => "ICU Gate is a Specialized Website In Medical Researches ,Live Medical News ,Medical cources ,More Training Hours ... amd discover More !!",
            'site_visit' => 12372,
            'phone' => "00966544802229",
            'address' => "ALDMMAM - KSA",
            'email' => "khaled@icugate.com",
            'site_email' => "info@icugate.com",
            'paypal_account' => "kmbsy@yahoo.com",
            'paypal_client_id' => "ATIXWmueNSzaPnysgY7-gxta-ay2zfDnMTGJ_X7VQhHIQoWGEXM6omE6cZZrZSyoVXu65-x1g0BcNTTj",
            'paypal_secret' => "EK8_P7xlxPTrpAn4K4PaISvwVzPQBZAELx4D5FXNJjh0RF-w0_ZiRwWSk5loG6Ba55WRbPk1jaHKjecS",
            'facebook' => "https://www.facebook.com/Icugate-1053612238037913/",
            'google' => "https://plus.google.com/112066228899366606119",
            'twitter' => "https://twitter.com/icugate",
            'instgram' => "https://www.instagram.com/",
            'youtube' => "https://www.youtube.com/channel/UCWnd9BLErb6EuiflehtpWTQ",
            'android' => "https://play.google.com/store/apps/details?id=com.icugate",
            'apple' => "https://apps.apple.com/us/app/id1526359370",
            'map' => '<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d457617.46591615706!2d49.71229092228524!3d26.3544482245112!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3e361d32276b3403%3A0xefd901ec7a5e5676!2sDammam+Saudi+Arabia!5e0!3m2!1sen!2seg!4v1464871691876" width="600" height="450" frameborder="0" allowfullscreen></iframe>'
        ]);
    }
}
