<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use App\Models\Team;

class TeamSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Team::create([
            'id'=> 1,
            'name'=> 'DR. KHLAED SEWIFY',
            'email'=> 'khaled@icugate.com',
            'jobtitle'=> 'Editor in Chief',
            'img'=> 'file-1544910516-13RUXD.jpg',
            'fb'=> '#',
            'tw'=> '#',
            'ins'=> '#',
            'skype'=> '#'
        ]);

        Team::create([
            'id'=> 2,
            'name'=> 'Dr. Colin Clinton',
            'email'=> 'colin@icugate.com',
            'jobtitle'=> 'Honorary President of ICUGATE',
            'img'=> 'file-1464710339-UV37DH.png',
            'fb'=> '#',
            'tw'=> '#',
            'ins'=> '#',
            'skype'=> '#'
        ]);

        Team::create([
            'id'=> 3,
            'name'=> 'Dr. Walid Shibl',
            'email'=> 'walid@icugate.com',
            'jobtitle'=> 'Moderator of The US Webpage',
            'img'=> 'file-1473155713-IWF9MI.png',
            'fb'=> '#',
            'tw'=> '#',
            'ins'=> '#',
            'skype'=> '#'
        ]);

        Team::create([
            'id'=> 4,
            'name'=> 'Dr. Waleed Jasim',
            'email'=> 'waleed@icugate.com',
            'jobtitle'=> 'ICUGATE Website Advisor',
            'img'=> 'file-1464710356-AGWLIV.png',
            'fb'=> '#',
            'tw'=> '#',
            'ins'=> '#',
            'skype'=> '#'
        ]);
    }
}
