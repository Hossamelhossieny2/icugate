<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Eloquent\Model; 
use Illuminate\Database\Seeder;
use App\Models\Cat;

class CatsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Cat::create([
            'id'=> "7",
            'title'=> "Resources",
            'img'=> "file-1464106363-HWQX6B.jpg",
            'main'=> "0",
            'order'=> "0",
            'shows'=> "0",
            'public'=> "0",
            'normal'=> "0",
            'silver'=> "1",
            'gold'=> "1",
            'deleted'=> "0",
        ]);

        Cat::create([
            'id'=> "8",
            'title'=> "Articles",
            'img'=> "file-1463591639-4YG8JY.jpg",
            'main'=> "7",
            'order'=> "0",
            'shows'=> "1",
            'public'=> "0",
            'normal'=> "0",
            'silver'=> "1",
            'gold'=> "1",
            'deleted'=> "0"
        ]);

        Cat::create([
            'id'=> "9",
            'title'=> "Guidelines",
            'img'=> "file-1463591644-ZGN2DA.jpg",
            'main'=> "7",
            'order'=> "0",
            'shows'=> "1",
            'public'=> "0",
            'normal'=> "0",
            'silver'=> "1",
            'gold'=> "1",
            'deleted'=> "0"
        ]);

        Cat::create([
            'id'=> "10",
            'title'=> "Protocols",
            'img'=> "file-1463591650-J14MMU.jpg",
            'main'=> "7",
            'order'=> "0",
            'shows'=> "1",
            'public'=> "0",
            'normal'=> "0",
            'silver'=> "1",
            'gold'=> "1",
            'deleted'=> "0"
        ]);

        Cat::create([
            'id'=> "11",
            'title'=> "CME",
            'img'=> "file-1464165463-D3CBM9.jpg",
            'main'=> "0",
            'order'=> "0",
            'shows'=> "0",
            'public'=> "0",
            'normal'=> "0",
            'silver'=> "1",
            'gold'=> "1",
            'deleted'=> "0"
        ]);

        Cat::create([
            'id'=> "12",
            'title'=> "Hot Topics",
            'img'=> "file-1463591657-K8BRDF.jpg",
            'main'=> "7",
            'order'=> "0",
            'shows'=> "1",
            'public'=> "0",
            'normal'=> "0",
            'silver'=> "1",
            'gold'=> "1",
            'deleted'=> "0"
        ]);

        $icuCat = Cat::create([
            'id'=> "13",
            'title'=> "ICU Scoring Calculators",
            'img'=> "file-1463591681-AGRM7H.jpg",
            'main'=> "7",
            'order'=> "0",
            'shows'=> "0",
            'public'=> "0",
            'normal'=> "0",
            'silver'=> "1",
            'gold'=> "1",
            'deleted'=> "0"
        ]);

        $icuCat->pricing()->attach(2);
        $icuCat->pricing()->attach(1);

        Cat::create([
            'id'=> "14",
            'title'=> "Drug Interactions Checker",
            'img'=> "file-1463591685-DDLMK9.jpg",
            'main'=> "7",
            'order'=> "0",
            'shows'=> "0",
            'public'=> "0",
            'normal'=> "0",
            'silver'=> "1",
            'gold'=> "1",
            'deleted'=> "0"
        ]);

        Cat::create([
            'id'=> "15",
            'title'=> "Residents Lecture",
            'img'=> "file-1463591725-9J88SF.jpg",
            'main'=> "11",
            'order'=> "0",
            'shows'=> "0",
            'public'=> "0",
            'normal'=> "0",
            'silver'=> "1",
            'gold'=> "1",
            'deleted'=> "0"
        ]);

        Cat::create([
            'id'=> "16",
            'title'=> "ICU Club Presentations",
            'img'=> "file-1463591732-SM39NY.jpg",
            'main'=> "11",
            'order'=> "0",
            'shows'=> "0",
            'public'=> "0",
            'normal'=> "0",
            'silver'=> "1",
            'gold'=> "1",
            'deleted'=> "0"
        ]);

        Cat::create([
            'id'=> "17",
            'title'=> "Members Contribution",
            'img'=> "file-1463591738-IZ3XFW.jpg",
            'main'=> "11",
            'order'=> "0",
            'shows'=> "0",
            'public'=> "0",
            'normal'=> "0",
            'silver'=> "1",
            'gold'=> "1",
            'deleted'=> "0"
        ]);

        Cat::create([
            'id'=> "18",
            'title'=> "Video",
            'img'=> "file-1463591746-GEE3E7.jpg",
            'main'=> "11",
            'order'=> "0",
            'shows'=> "0",
            'public'=> "0",
            'normal'=> "0",
            'silver'=> "1",
            'gold'=> "1",
            'deleted'=> "0"
        ]);

        Cat::create([
            'id'=> "19",
            'title'=> "ICU PROCEDURES",
            'img'=> "ile-1463591752-XK911T.jpg",
            'main'=> "11",
            'order'=> "0",
            'shows'=> "0",
            'public'=> "0",
            'normal'=> "0",
            'silver'=> "1",
            'gold'=> "1",
            'deleted'=> "0"
        ]);

        Cat::create([
            'id'=> "20",
            'title'=> "Ongoing Researches",
            'img'=> "file-1464165480-R231XR.jpg",
            'main'=> "0",
            'order'=> "0",
            'shows'=> "0",
            'public'=> "0",
            'normal'=> "0",
            'silver'=> "1",
            'gold'=> "1",
            'deleted'=> "0"
        ]);

        Cat::create([
            'id'=> "22",
            'title'=> "archive",
            'img'=> "",
            'main'=> "8",
            'order'=> "0",
            'shows'=> "1",
            'public'=> "0",
            'normal'=> "0",
            'silver'=> "1",
            'gold'=> "1",
            'deleted'=> "0"
        ]);
    }
}
