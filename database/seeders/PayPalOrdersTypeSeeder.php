<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use App\Models\PaypalOrderType;

class PayPalOrdersTypeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        PaypalOrderType::create([
            'id' => 1,
            'name' => "Upgrate",
            'created_at' => "2020-11-30 16:09:29",
        ]);

        PaypalOrderType::create([
            'id' => 2,
            'name' => "Course",
            'created_at' => "2020-11-30 16:09:29",
        ]);

        PaypalOrderType::create([
            'id' => 3,
            'name' => "Teleconsulting",
            'created_at' => "2020-11-30 16:09:29",
        ]);
    }
}
