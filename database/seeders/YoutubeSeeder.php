<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use App\Models\Youtube;

class YoutubeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Youtube::create( [
            'youtube_id'=>1,
            'title'=>'Goal Directed Bedside Cardiac Ultrasound',
            'playlist'=>'Critical Care Ultrasound',
            'playlist_id'=>'PLaH5e_8RLQBGMIflXgadUlPYhX4Pjdbe6',
            'embed'=>'https://www.youtube.com/embed/UD4fupZwJ4s',
            'img'=>'https://i.ytimg.com/vi/UD4fupZwJ4s/hqdefault.jpg',
            'video_id'=>'UD4fupZwJ4s',
            'date'=>'2016-09-29 13:15:02',
            'need_change'=>0
            ] );


                        
            Youtube::create( [
            'youtube_id'=>2,
            'title'=>'Bedside  Ultrasound for DVT',
            'playlist'=>'Critical Care Ultrasound',
            'playlist_id'=>'PLaH5e_8RLQBGMIflXgadUlPYhX4Pjdbe6',
            'embed'=>'https://www.youtube.com/embed/T4laNiWFdJM',
            'img'=>'https://i.ytimg.com/vi/T4laNiWFdJM/hqdefault.jpg',
            'video_id'=>'T4laNiWFdJM',
            'date'=>'2016-09-29 13:15:03',
            'need_change'=>0
            ] );


                        
            Youtube::create( [
            'youtube_id'=>3,
            'title'=>'Rapid Ultrasound in SHock',
            'playlist'=>'Critical Care Ultrasound',
            'playlist_id'=>'PLaH5e_8RLQBGMIflXgadUlPYhX4Pjdbe6',
            'embed'=>'https://www.youtube.com/embed/iVYr_W1_yV8',
            'img'=>'https://i.ytimg.com/vi/iVYr_W1_yV8/hqdefault.jpg',
            'video_id'=>'iVYr_W1_yV8',
            'date'=>'2016-09-29 13:15:03',
            'need_change'=>0
            ] );


                        
            Youtube::create( [
            'youtube_id'=>4,
            'title'=>'Emergency Early Goal Directed Therapy for Severe Sepsis',
            'playlist'=>'Critical Care Ultrasound',
            'playlist_id'=>'PLaH5e_8RLQBGMIflXgadUlPYhX4Pjdbe6',
            'embed'=>'https://www.youtube.com/embed/xwKjHWdXh_s',
            'img'=>'https://i.ytimg.com/vi/xwKjHWdXh_s/hqdefault.jpg',
            'video_id'=>'xwKjHWdXh_s',
            'date'=>'2016-09-29 13:15:03',
            'need_change'=>0
            ] );


                        
            Youtube::create( [
            'youtube_id'=>5,
            'title'=>'Cardiac Echocardiography Parasternal Views of the Heart',
            'playlist'=>'Critical Care Ultrasound',
            'playlist_id'=>'PLaH5e_8RLQBGMIflXgadUlPYhX4Pjdbe6',
            'embed'=>'https://www.youtube.com/embed/B731sgCuZU4',
            'img'=>'https://i.ytimg.com/vi/B731sgCuZU4/hqdefault.jpg',
            'video_id'=>'B731sgCuZU4',
            'date'=>'2016-09-29 13:15:03',
            'need_change'=>0
            ] );


                        
            Youtube::create( [
            'youtube_id'=>6,
            'title'=>'Scanning Technique Focused Echo  Apical View',
            'playlist'=>'Critical Care Ultrasound',
            'playlist_id'=>'PLaH5e_8RLQBGMIflXgadUlPYhX4Pjdbe6',
            'embed'=>'https://www.youtube.com/embed/5czv-c1uUnw',
            'img'=>'https://i.ytimg.com/vi/5czv-c1uUnw/hqdefault.jpg',
            'video_id'=>'5czv-c1uUnw',
            'date'=>'2016-09-29 13:15:03',
            'need_change'=>0
            ] );


                        
            Youtube::create( [
            'youtube_id'=>7,
            'title'=>'Scanning Technique Focused Echo Parasternall View',
            'playlist'=>'Critical Care Ultrasound',
            'playlist_id'=>'PLaH5e_8RLQBGMIflXgadUlPYhX4Pjdbe6',
            'embed'=>'https://www.youtube.com/embed/3mS3vTAEmS8',
            'img'=>'https://i.ytimg.com/vi/3mS3vTAEmS8/hqdefault.jpg',
            'video_id'=>'3mS3vTAEmS8',
            'date'=>'2016-09-29 13:15:03',
            'need_change'=>0
            ] );


                        
            Youtube::create( [
            'youtube_id'=>8,
            'title'=>'Which Probe Do I Use ?',
            'playlist'=>'Critical Care Ultrasound',
            'playlist_id'=>'PLaH5e_8RLQBGMIflXgadUlPYhX4Pjdbe6',
            'embed'=>'https://www.youtube.com/embed/gPPYIISnhUA',
            'img'=>'https://i.ytimg.com/vi/gPPYIISnhUA/hqdefault.jpg',
            'video_id'=>'gPPYIISnhUA',
            'date'=>'2016-09-29 13:15:03',
            'need_change'=>0
            ] );


                        
            Youtube::create( [
            'youtube_id'=>9,
            'title'=>'Measurement of Wedge Pressure',
            'playlist'=>'Critical Care Ultrasound',
            'playlist_id'=>'PLaH5e_8RLQBGMIflXgadUlPYhX4Pjdbe6',
            'embed'=>'https://www.youtube.com/embed/XQogjBTq7qA',
            'img'=>'https://i.ytimg.com/vi/XQogjBTq7qA/hqdefault.jpg',
            'video_id'=>'XQogjBTq7qA',
            'date'=>'2016-09-29 13:15:03',
            'need_change'=>0
            ] );


                        
            Youtube::create( [
            'youtube_id'=>10,
            'title'=>'WEAK DIAPHRAGM',
            'playlist'=>'Critical Care Ultrasound',
            'playlist_id'=>'PLaH5e_8RLQBGMIflXgadUlPYhX4Pjdbe6',
            'embed'=>'https://www.youtube.com/embed/kupg1g0qy9w',
            'img'=>'https://i.ytimg.com/vi/kupg1g0qy9w/hqdefault.jpg',
            'video_id'=>'kupg1g0qy9w',
            'date'=>'2016-09-29 13:15:03',
            'need_change'=>0
            ] );


                        
            Youtube::create( [
            'youtube_id'=>11,
            'title'=>'WEAK DIAPH',
            'playlist'=>'Critical Care Ultrasound',
            'playlist_id'=>'PLaH5e_8RLQBGMIflXgadUlPYhX4Pjdbe6',
            'embed'=>'https://www.youtube.com/embed/hq_Y5SobEQk',
            'img'=>'https://i.ytimg.com/vi/hq_Y5SobEQk/hqdefault.jpg',
            'video_id'=>'hq_Y5SobEQk',
            'date'=>'2016-09-29 13:15:03',
            'need_change'=>1
            ] );


                        
            Youtube::create( [
            'youtube_id'=>12,
            'title'=>'we beleive in GOD',
            'playlist'=>'Critical Care Ultrasound',
            'playlist_id'=>'PLaH5e_8RLQBGMIflXgadUlPYhX4Pjdbe6',
            'embed'=>'https://www.youtube.com/embed/wLZGGQbIleU',
            'img'=>'https://i.ytimg.com/vi/wLZGGQbIleU/hqdefault.jpg',
            'video_id'=>'wLZGGQbIleU',
            'date'=>'2016-09-29 13:15:03',
            'need_change'=>1
            ] );


                        
            Youtube::create( [
            'youtube_id'=>13,
            'title'=>'we beleive GOD is the treater',
            'playlist'=>'Critical Care Ultrasound',
            'playlist_id'=>'PLaH5e_8RLQBGMIflXgadUlPYhX4Pjdbe6',
            'embed'=>'https://www.youtube.com/embed/lcjO0qBzH0Q',
            'img'=>'https://i.ytimg.com/vi/lcjO0qBzH0Q/hqdefault.jpg',
            'video_id'=>'lcjO0qBzH0Q',
            'date'=>'2016-09-29 13:15:03',
            'need_change'=>1
            ] );


                        
            Youtube::create( [
            'youtube_id'=>14,
            'title'=>'we beleive  GOD is the treater',
            'playlist'=>'Critical Care Ultrasound',
            'playlist_id'=>'PLaH5e_8RLQBGMIflXgadUlPYhX4Pjdbe6',
            'embed'=>'https://www.youtube.com/embed/7C16oOzg0Dg',
            'img'=>'https://i.ytimg.com/vi/7C16oOzg0Dg/hqdefault.jpg',
            'video_id'=>'7C16oOzg0Dg',
            'date'=>'2016-09-29 13:15:03',
            'need_change'=>1
            ] );


                        
            Youtube::create( [
            'youtube_id'=>15,
            'title'=>'critical care U/S case study',
            'playlist'=>'Critical Care Ultrasound',
            'playlist_id'=>'PLaH5e_8RLQBGMIflXgadUlPYhX4Pjdbe6',
            'embed'=>'https://www.youtube.com/embed/W1pXolfcFQ8',
            'img'=>'https://i.ytimg.com/vi/W1pXolfcFQ8/hqdefault.jpg',
            'video_id'=>'W1pXolfcFQ8',
            'date'=>'2016-09-29 13:15:03',
            'need_change'=>0
            ] );


                        
            Youtube::create( [
            'youtube_id'=>16,
            'title'=>'TRIPLE',
            'playlist'=>'Critical Care Ultrasound',
            'playlist_id'=>'PLaH5e_8RLQBGMIflXgadUlPYhX4Pjdbe6',
            'embed'=>'https://www.youtube.com/embed/JDO_oHfYzck',
            'img'=>'https://i.ytimg.com/vi/JDO_oHfYzck/hqdefault.jpg',
            'video_id'=>'JDO_oHfYzck',
            'date'=>'2016-09-29 13:15:03',
            'need_change'=>1
            ] );


                        
            Youtube::create( [
            'youtube_id'=>17,
            'title'=>'the landmark',
            'playlist'=>'Critical Care Ultrasound',
            'playlist_id'=>'PLaH5e_8RLQBGMIflXgadUlPYhX4Pjdbe6',
            'embed'=>'https://www.youtube.com/embed/iaMzVmXHW4M',
            'img'=>'https://i.ytimg.com/vi/iaMzVmXHW4M/hqdefault.jpg',
            'video_id'=>'iaMzVmXHW4M',
            'date'=>'2016-09-29 13:15:03',
            'need_change'=>1
            ] );


                        
            Youtube::create( [
            'youtube_id'=>18,
            'title'=>'The complete story3 Recovered',
            'playlist'=>'Critical Care Ultrasound',
            'playlist_id'=>'PLaH5e_8RLQBGMIflXgadUlPYhX4Pjdbe6',
            'embed'=>'https://www.youtube.com/embed/ns7UZELQygA',
            'img'=>'https://i.ytimg.com/vi/ns7UZELQygA/hqdefault.jpg',
            'video_id'=>'ns7UZELQygA',
            'date'=>'2016-09-29 13:15:03',
            'need_change'=>1
            ] );


                        
            Youtube::create( [
            'youtube_id'=>19,
            'title'=>'the complete story still will remain difficult question lasix or IVF',
            'playlist'=>'Critical Care Ultrasound',
            'playlist_id'=>'PLaH5e_8RLQBGMIflXgadUlPYhX4Pjdbe6',
            'embed'=>'https://www.youtube.com/embed/bEZ-7-aU5dc',
            'img'=>'https://i.ytimg.com/vi/bEZ-7-aU5dc/hqdefault.jpg',
            'video_id'=>'bEZ-7-aU5dc',
            'date'=>'2016-09-29 13:15:03',
            'need_change'=>1
            ] );


                        
            Youtube::create( [
            'youtube_id'=>20,
            'title'=>'stroke in young',
            'playlist'=>'Critical Care Ultrasound',
            'playlist_id'=>'PLaH5e_8RLQBGMIflXgadUlPYhX4Pjdbe6',
            'embed'=>'https://www.youtube.com/embed/dVhra-cZG58',
            'img'=>'https://i.ytimg.com/vi/dVhra-cZG58/hqdefault.jpg',
            'video_id'=>'dVhra-cZG58',
            'date'=>'2016-09-29 13:15:03',
            'need_change'=>1
            ] );


                        
            Youtube::create( [
            'youtube_id'=>21,
            'title'=>'stroke in old the complete story',
            'playlist'=>'Critical Care Ultrasound',
            'playlist_id'=>'PLaH5e_8RLQBGMIflXgadUlPYhX4Pjdbe6',
            'embed'=>'https://www.youtube.com/embed/eE789tRDQG8',
            'img'=>'https://i.ytimg.com/vi/eE789tRDQG8/hqdefault.jpg',
            'video_id'=>'eE789tRDQG8',
            'date'=>'2016-09-29 13:15:03',
            'need_change'=>1
            ] );


                        
            Youtube::create( [
            'youtube_id'=>22,
            'title'=>'amazing splenic artery sickling by U/S',
            'playlist'=>'Critical Care Ultrasound',
            'playlist_id'=>'PLaH5e_8RLQBGMIflXgadUlPYhX4Pjdbe6',
            'embed'=>'https://www.youtube.com/embed/IqcEQOofsmI',
            'img'=>'https://i.ytimg.com/vi/IqcEQOofsmI/hqdefault.jpg',
            'video_id'=>'IqcEQOofsmI',
            'date'=>'2016-09-29 13:15:03',
            'need_change'=>0
            ] );


                        
            Youtube::create( [
            'youtube_id'=>23,
            'title'=>'sickler with stress myopathy after post partum hage',
            'playlist'=>'Critical Care Ultrasound',
            'playlist_id'=>'PLaH5e_8RLQBGMIflXgadUlPYhX4Pjdbe6',
            'embed'=>'https://www.youtube.com/embed/8saCqLdk3Fw',
            'img'=>'https://i.ytimg.com/vi/8saCqLdk3Fw/hqdefault.jpg',
            'video_id'=>'8saCqLdk3Fw',
            'date'=>'2016-09-29 13:15:03',
            'need_change'=>1
            ] );


                        
            Youtube::create( [
            'youtube_id'=>24,
            'title'=>'senior2',
            'playlist'=>'Critical Care Ultrasound',
            'playlist_id'=>'PLaH5e_8RLQBGMIflXgadUlPYhX4Pjdbe6',
            'embed'=>'https://www.youtube.com/embed/N5znmt1zbRs',
            'img'=>'https://i.ytimg.com/vi/N5znmt1zbRs/hqdefault.jpg',
            'video_id'=>'N5znmt1zbRs',
            'date'=>'2016-09-29 13:15:04',
            'need_change'=>1
            ] );


                        
            Youtube::create( [
            'youtube_id'=>25,
            'title'=>'senior 6',
            'playlist'=>'Critical Care Ultrasound',
            'playlist_id'=>'PLaH5e_8RLQBGMIflXgadUlPYhX4Pjdbe6',
            'embed'=>'https://www.youtube.com/embed/CpvS0UnDmPQ',
            'img'=>'https://i.ytimg.com/vi/CpvS0UnDmPQ/hqdefault.jpg',
            'video_id'=>'CpvS0UnDmPQ',
            'date'=>'2016-09-29 13:15:04',
            'need_change'=>1
            ] );


                        
            Youtube::create( [
            'youtube_id'=>26,
            'title'=>'senior 5',
            'playlist'=>'Critical Care Ultrasound',
            'playlist_id'=>'PLaH5e_8RLQBGMIflXgadUlPYhX4Pjdbe6',
            'embed'=>'https://www.youtube.com/embed/24DE_h42pRw',
            'img'=>'https://i.ytimg.com/vi/24DE_h42pRw/hqdefault.jpg',
            'video_id'=>'24DE_h42pRw',
            'date'=>'2016-09-29 13:15:04',
            'need_change'=>1
            ] );


                        
            Youtube::create( [
            'youtube_id'=>27,
            'title'=>'senior 4',
            'playlist'=>'Critical Care Ultrasound',
            'playlist_id'=>'PLaH5e_8RLQBGMIflXgadUlPYhX4Pjdbe6',
            'embed'=>'https://www.youtube.com/embed/3In9h95xw1I',
            'img'=>'https://i.ytimg.com/vi/3In9h95xw1I/hqdefault.jpg',
            'video_id'=>'3In9h95xw1I',
            'date'=>'2016-09-29 13:15:04',
            'need_change'=>1
            ] );


                        
            Youtube::create( [
            'youtube_id'=>28,
            'title'=>'senior 4 final',
            'playlist'=>'Critical Care Ultrasound',
            'playlist_id'=>'PLaH5e_8RLQBGMIflXgadUlPYhX4Pjdbe6',
            'embed'=>'https://www.youtube.com/embed/NaRCHoVYhgc',
            'img'=>'https://i.ytimg.com/vi/NaRCHoVYhgc/hqdefault.jpg',
            'video_id'=>'NaRCHoVYhgc',
            'date'=>'2016-09-29 13:15:04',
            'need_change'=>1
            ] );


                        
            Youtube::create( [
            'youtube_id'=>29,
            'title'=>'Approach to Dyspnea',
            'playlist'=>'Critical Care Ultrasound',
            'playlist_id'=>'PLaH5e_8RLQBGMIflXgadUlPYhX4Pjdbe6',
            'embed'=>'https://www.youtube.com/embed/0RVjAiAoH1c',
            'img'=>'https://i.ytimg.com/vi/0RVjAiAoH1c/hqdefault.jpg',
            'video_id'=>'0RVjAiAoH1c',
            'date'=>'2016-09-29 13:15:04',
            'need_change'=>0
            ] );


                        
            Youtube::create( [
            'youtube_id'=>30,
            'title'=>'real modified TCD live',
            'playlist'=>'Critical Care Ultrasound',
            'playlist_id'=>'PLaH5e_8RLQBGMIflXgadUlPYhX4Pjdbe6',
            'embed'=>'https://www.youtube.com/embed/kh7wD_Suty8',
            'img'=>'https://i.ytimg.com/vi/kh7wD_Suty8/hqdefault.jpg',
            'video_id'=>'kh7wD_Suty8',
            'date'=>'2016-09-29 13:15:04',
            'need_change'=>1
            ] );


                        
            Youtube::create( [
            'youtube_id'=>31,
            'title'=>'How to use the ultrasound probe',
            'playlist'=>'Critical Care Ultrasound',
            'playlist_id'=>'PLaH5e_8RLQBGMIflXgadUlPYhX4Pjdbe6',
            'embed'=>'https://www.youtube.com/embed/ABrHkFWcvUI',
            'img'=>'https://i.ytimg.com/vi/ABrHkFWcvUI/hqdefault.jpg',
            'video_id'=>'ABrHkFWcvUI',
            'date'=>'2016-09-29 13:15:04',
            'need_change'=>0
            ] );


                        
            Youtube::create( [
            'youtube_id'=>32,
            'title'=>'5 Orienting Your Probe',
            'playlist'=>'Critical Care Ultrasound',
            'playlist_id'=>'PLaH5e_8RLQBGMIflXgadUlPYhX4Pjdbe6',
            'embed'=>'https://www.youtube.com/embed/3MOooEUUccI',
            'img'=>'https://i.ytimg.com/vi/3MOooEUUccI/hqdefault.jpg',
            'video_id'=>'3MOooEUUccI',
            'date'=>'2016-09-29 13:15:04',
            'need_change'=>0
            ] );


                        
            Youtube::create( [
            'youtube_id'=>33,
            'title'=>'Ultrasound of the kidney 1 Normal anatomy of the kidney',
            'playlist'=>'Critical Care Ultrasound',
            'playlist_id'=>'PLaH5e_8RLQBGMIflXgadUlPYhX4Pjdbe6',
            'embed'=>'https://www.youtube.com/embed/dqnaHMuPW_Q',
            'img'=>'https://i.ytimg.com/vi/dqnaHMuPW_Q/hqdefault.jpg',
            'video_id'=>'dqnaHMuPW_Q',
            'date'=>'2016-09-29 13:15:04',
            'need_change'=>0
            ] );


                        
            Youtube::create( [
            'youtube_id'=>34,
            'title'=>'MCA complete study',
            'playlist'=>'Critical Care Ultrasound',
            'playlist_id'=>'PLaH5e_8RLQBGMIflXgadUlPYhX4Pjdbe6',
            'embed'=>'https://www.youtube.com/embed/93P0D5pzSWw',
            'img'=>'https://i.ytimg.com/vi/93P0D5pzSWw/hqdefault.jpg',
            'video_id'=>'93P0D5pzSWw',
            'date'=>'2016-09-29 13:15:04',
            'need_change'=>1
            ] );


                        
            Youtube::create( [
            'youtube_id'=>35,
            'title'=>'MCA 1',
            'playlist'=>'Critical Care Ultrasound',
            'playlist_id'=>'PLaH5e_8RLQBGMIflXgadUlPYhX4Pjdbe6',
            'embed'=>'https://www.youtube.com/embed/AnntKFgDmk8',
            'img'=>'https://i.ytimg.com/vi/AnntKFgDmk8/hqdefault.jpg',
            'video_id'=>'AnntKFgDmk8',
            'date'=>'2016-09-29 13:15:04',
            'need_change'=>1
            ] );


                        
            Youtube::create( [
            'youtube_id'=>36,
            'title'=>'lung grades',
            'playlist'=>'Critical Care Ultrasound',
            'playlist_id'=>'PLaH5e_8RLQBGMIflXgadUlPYhX4Pjdbe6',
            'embed'=>'https://www.youtube.com/embed/bxak7SJ1Os4',
            'img'=>'https://i.ytimg.com/vi/bxak7SJ1Os4/hqdefault.jpg',
            'video_id'=>'bxak7SJ1Os4',
            'date'=>'2016-09-29 13:15:04',
            'need_change'=>1
            ] );


                        
            Youtube::create( [
            'youtube_id'=>37,
            'title'=>'DVT Ultrasound/ deep vein thrombosis',
            'playlist'=>'Critical Care Ultrasound',
            'playlist_id'=>'PLaH5e_8RLQBGMIflXgadUlPYhX4Pjdbe6',
            'embed'=>'https://www.youtube.com/embed/cae0E0dzdww',
            'img'=>'https://i.ytimg.com/vi/cae0E0dzdww/hqdefault.jpg',
            'video_id'=>'cae0E0dzdww',
            'date'=>'2016-09-29 13:15:04',
            'need_change'=>0
            ] );


                        
            Youtube::create( [
            'youtube_id'=>38,
            'title'=>'Critical Care Educational Support Professionals courses',
            'playlist'=>'Critical Care Educational Support Professionals',
            'playlist_id'=>'PLaH5e_8RLQBGfdn1_2I9wCTXdP7uAyc_e',
            'embed'=>'https://www.youtube.com/embed/3ZVbCTv9S_4',
            'img'=>'https://i.ytimg.com/vi/3ZVbCTv9S_4/hqdefault.jpg',
            'video_id'=>'3ZVbCTv9S_4',
            'date'=>'2016-09-29 13:15:04',
            'need_change'=>1
            ] );


                        
            Youtube::create( [
            'youtube_id'=>39,
            'title'=>"Emory's Pulmonary & Critical Care Fellowship Program",
            'playlist'=>'Critical Care Educational Support Professionals',
            'playlist_id'=>'PLaH5e_8RLQBGfdn1_2I9wCTXdP7uAyc_e',
            'embed'=>'https://www.youtube.com/embed/3_QprNCQotk',
            'img'=>'https://i.ytimg.com/vi/3_QprNCQotk/hqdefault.jpg',
            'video_id'=>'3_QprNCQotk',
            'date'=>'2016-09-29 13:15:04',
            'need_change'=>0
            ] );


                        
            Youtube::create( [
            'youtube_id'=>40,
            'title'=>'The History of Critical Care & SCCM',
            'playlist'=>'Critical Care Educational Support Professionals',
            'playlist_id'=>'PLaH5e_8RLQBGfdn1_2I9wCTXdP7uAyc_e',
            'embed'=>'https://www.youtube.com/embed/dV7u9K35oIo',
            'img'=>'https://i.ytimg.com/vi/dV7u9K35oIo/hqdefault.jpg',
            'video_id'=>'dV7u9K35oIo',
            'date'=>'2016-09-29 13:15:04',
            'need_change'=>0
            ] );


                        
            Youtube::create( [
            'youtube_id'=>41,
            'title'=>'Introduction to Mechanical Ventilation',
            'playlist'=>'Critical Care Educational Support Professionals',
            'playlist_id'=>'PLaH5e_8RLQBGfdn1_2I9wCTXdP7uAyc_e',
            'embed'=>'https://www.youtube.com/embed/6JFHiiEkjlk',
            'img'=>'https://i.ytimg.com/vi/6JFHiiEkjlk/hqdefault.jpg',
            'video_id'=>'6JFHiiEkjlk',
            'date'=>'2016-09-29 13:15:04',
            'need_change'=>0
            ] );


                        
            Youtube::create( [
            'youtube_id'=>42,
            'title'=>'WEAK DIAPHRAGM2',
            'playlist'=>'uploads',
            'playlist_id'=>'UUiplEkp4yXoTg34HmV3M_pA',
            'embed'=>'https://www.youtube.com/embed/r6OshsJuhfo',
            'img'=>'https://i.ytimg.com/vi/r6OshsJuhfo/hqdefault.jpg',
            'video_id'=>'r6OshsJuhfo',
            'date'=>'2016-09-29 13:15:05',
            'need_change'=>1
            ] );


                        
            Youtube::create( [
            'youtube_id'=>43,
            'title'=>'which probe',
            'playlist'=>'uploads',
            'playlist_id'=>'UUiplEkp4yXoTg34HmV3M_pA',
            'embed'=>'https://www.youtube.com/embed/6ybjeu6suFQ',
            'img'=>'https://i.ytimg.com/vi/6ybjeu6suFQ/hqdefault.jpg',
            'video_id'=>'6ybjeu6suFQ',
            'date'=>'2016-09-29 13:15:05',
            'need_change'=>1
            ] );


                        
            Youtube::create( [
            'youtube_id'=>44,
            'title'=>'image dvt femoral',
            'playlist'=>'uploads',
            'playlist_id'=>'UUiplEkp4yXoTg34HmV3M_pA',
            'embed'=>'https://www.youtube.com/embed/D5-X3UYhO1Q',
            'img'=>'https://i.ytimg.com/vi/D5-X3UYhO1Q/hqdefault.jpg',
            'video_id'=>'D5-X3UYhO1Q',
            'date'=>'2016-09-29 13:15:05',
            'need_change'=>1
            ] );


                        
            Youtube::create( [
            'youtube_id'=>45,
            'title'=>'How to measure the optic nerve sheath diameter in a couple of minutes',
            'playlist'=>'uploads',
            'playlist_id'=>'UUiplEkp4yXoTg34HmV3M_pA',
            'embed'=>'https://www.youtube.com/embed/dQ70aYEPb-0',
            'img'=>'https://i.ytimg.com/vi/dQ70aYEPb-0/hqdefault.jpg',
            'video_id'=>'dQ70aYEPb-0',
            'date'=>'2016-09-29 13:15:05',
            'need_change'=>1
            ] );


                        
            Youtube::create( [
            'youtube_id'=>46,
            'title'=>'full study of recent stroke',
            'playlist'=>'uploads',
            'playlist_id'=>'UUiplEkp4yXoTg34HmV3M_pA',
            'embed'=>'https://www.youtube.com/embed/zFBGEVYHR50',
            'img'=>'https://i.ytimg.com/vi/zFBGEVYHR50/hqdefault.jpg',
            'video_id'=>'zFBGEVYHR50',
            'date'=>'2016-09-29 13:15:05',
            'need_change'=>1
            ] );


                        
            Youtube::create( [
            'youtube_id'=>47,
            'title'=>'full study of brain death by TCD',
            'playlist'=>'uploads',
            'playlist_id'=>'UUiplEkp4yXoTg34HmV3M_pA',
            'embed'=>'https://www.youtube.com/embed/kWs32a0hm44',
            'img'=>'https://i.ytimg.com/vi/kWs32a0hm44/hqdefault.jpg',
            'video_id'=>'kWs32a0hm44',
            'date'=>'2016-09-29 13:15:05',
            'need_change'=>1
            ] );


                        
            Youtube::create( [
            'youtube_id'=>48,
            'title'=>'femoral vein complete DVT study',
            'playlist'=>'uploads',
            'playlist_id'=>'UUiplEkp4yXoTg34HmV3M_pA',
            'embed'=>'https://www.youtube.com/embed/OBcr3xNnhUI',
            'img'=>'https://i.ytimg.com/vi/OBcr3xNnhUI/hqdefault.jpg',
            'video_id'=>'OBcr3xNnhUI',
            'date'=>'2016-09-29 13:15:05',
            'need_change'=>1
            ] );


                        
            Youtube::create( [
            'youtube_id'=>49,
            'title'=>'echo CPR',
            'playlist'=>'uploads',
            'playlist_id'=>'UUiplEkp4yXoTg34HmV3M_pA',
            'embed'=>'https://www.youtube.com/embed/gxUpLOLFf0A',
            'img'=>'https://i.ytimg.com/vi/gxUpLOLFf0A/hqdefault.jpg',
            'video_id'=>'gxUpLOLFf0A',
            'date'=>'2016-09-29 13:15:05',
            'need_change'=>1
            ] );


                        
            Youtube::create( [
            'youtube_id'=>50,
            'title'=>'ech cpr final',
            'playlist'=>'uploads',
            'playlist_id'=>'UUiplEkp4yXoTg34HmV3M_pA',
            'embed'=>'https://www.youtube.com/embed/PImegN-P6kM',
            'img'=>'https://i.ytimg.com/vi/PImegN-P6kM/hqdefault.jpg',
            'video_id'=>'PImegN-P6kM',
            'date'=>'2016-09-29 13:15:05',
            'need_change'=>1
            ] );


                        
            Youtube::create( [
            'youtube_id'=>51,
            'title'=>'dvt study',
            'playlist'=>'uploads',
            'playlist_id'=>'UUiplEkp4yXoTg34HmV3M_pA',
            'embed'=>'https://www.youtube.com/embed/meworuskLPA',
            'img'=>'https://i.ytimg.com/vi/meworuskLPA/hqdefault.jpg',
            'video_id'=>'meworuskLPA',
            'date'=>'2016-09-29 13:15:05',
            'need_change'=>1
            ] );


                        
            Youtube::create( [
            'youtube_id'=>52,
            'title'=>'duchenne myopathy',
            'playlist'=>'uploads',
            'playlist_id'=>'UUiplEkp4yXoTg34HmV3M_pA',
            'embed'=>'https://www.youtube.com/embed/jYeb7z4PSP4',
            'img'=>'https://i.ytimg.com/vi/jYeb7z4PSP4/hqdefault.jpg',
            'video_id'=>'jYeb7z4PSP4',
            'date'=>'2016-09-29 13:15:05',
            'need_change'=>1
            ] );


                        
            Youtube::create( [
            'youtube_id'=>53,
            'title'=>'diastolic dysfunction',
            'playlist'=>'uploads',
            'playlist_id'=>'UUiplEkp4yXoTg34HmV3M_pA',
            'embed'=>'https://www.youtube.com/embed/WMYnM0wf44c',
            'img'=>'https://i.ytimg.com/vi/WMYnM0wf44c/hqdefault.jpg',
            'video_id'=>'WMYnM0wf44c',
            'date'=>'2016-09-29 13:15:05',
            'need_change'=>1
            ] );


                        
            Youtube::create( [
            'youtube_id'=>54,
            'title'=>'diaphram',
            'playlist'=>'uploads',
            'playlist_id'=>'UUiplEkp4yXoTg34HmV3M_pA',
            'embed'=>'https://www.youtube.com/embed/5WnKcqNqvUE',
            'img'=>'https://i.ytimg.com/vi/5WnKcqNqvUE/hqdefault.jpg',
            'video_id'=>'5WnKcqNqvUE',
            'date'=>'2016-09-29 13:15:05',
            'need_change'=>1
            ] );


                        
            Youtube::create( [
            'youtube_id'=>55,
            'title'=>'diaphragm ASSESSEMENT',
            'playlist'=>'uploads',
            'playlist_id'=>'UUiplEkp4yXoTg34HmV3M_pA',
            'embed'=>'https://www.youtube.com/embed/zeEO1oHngSQ',
            'img'=>'https://i.ytimg.com/vi/zeEO1oHngSQ/hqdefault.jpg',
            'video_id'=>'zeEO1oHngSQ',
            'date'=>'2016-09-29 13:15:05',
            'need_change'=>1
            ] );


                        
            Youtube::create( [
            'youtube_id'=>56,
            'title'=>'complete TCD in a SCD patient with haemolytic crisis and hyperemia  all intracranial arteries',
            'playlist'=>'uploads',
            'playlist_id'=>'UUiplEkp4yXoTg34HmV3M_pA',
            'embed'=>'https://www.youtube.com/embed/NOkbtpCiQfE',
            'img'=>'https://i.ytimg.com/vi/NOkbtpCiQfE/hqdefault.jpg',
            'video_id'=>'NOkbtpCiQfE',
            'date'=>'2016-09-29 13:15:06',
            'need_change'=>1
            ] );


                        
            Youtube::create( [
            'youtube_id'=>57,
            'title'=>'complete diaphragmatic paralysis',
            'playlist'=>'uploads',
            'playlist_id'=>'UUiplEkp4yXoTg34HmV3M_pA',
            'embed'=>'https://www.youtube.com/embed/jN3e8k4Vaws',
            'img'=>'https://i.ytimg.com/vi/jN3e8k4Vaws/hqdefault.jpg',
            'video_id'=>'jN3e8k4Vaws',
            'date'=>'2016-09-29 13:15:06',
            'need_change'=>1
            ] );


                        
            Youtube::create( [
            'youtube_id'=>58,
            'title'=>'complete diaphragmatic paralysis 2',
            'playlist'=>'uploads',
            'playlist_id'=>'UUiplEkp4yXoTg34HmV3M_pA',
            'embed'=>'https://www.youtube.com/embed/9MXrYUAcDQQ',
            'img'=>'https://i.ytimg.com/vi/9MXrYUAcDQQ/hqdefault.jpg',
            'video_id'=>'9MXrYUAcDQQ',
            'date'=>'2016-09-29 13:15:06',
            'need_change'=>1
            ] );


                        
            Youtube::create( [
            'youtube_id'=>59,
            'title'=>'COMLETE TCD STUDY',
            'playlist'=>'uploads',
            'playlist_id'=>'UUiplEkp4yXoTg34HmV3M_pA',
            'embed'=>'https://www.youtube.com/embed/NjtPeB52ipU',
            'img'=>'https://i.ytimg.com/vi/NjtPeB52ipU/hqdefault.jpg',
            'video_id'=>'NjtPeB52ipU',
            'date'=>'2016-09-29 13:15:06',
            'need_change'=>1
            ] );


                        
            Youtube::create( [
            'youtube_id'=>60,
            'title'=>'circle of willis',
            'playlist'=>'uploads',
            'playlist_id'=>'UUiplEkp4yXoTg34HmV3M_pA',
            'embed'=>'https://www.youtube.com/embed/s3oqcMKr9fo',
            'img'=>'https://i.ytimg.com/vi/s3oqcMKr9fo/hqdefault.jpg',
            'video_id'=>'s3oqcMKr9fo',
            'date'=>'2016-09-29 13:15:06',
            'need_change'=>1
            ] );


                        
            Youtube::create( [
            'youtube_id'=>61,
            'title'=>'cardiogenic dyspnea',
            'playlist'=>'uploads',
            'playlist_id'=>'UUiplEkp4yXoTg34HmV3M_pA',
            'embed'=>'https://www.youtube.com/embed/_gwEHXkGDc8',
            'img'=>'https://i.ytimg.com/vi/_gwEHXkGDc8/hqdefault.jpg',
            'video_id'=>'_gwEHXkGDc8',
            'date'=>'2016-09-29 13:15:06',
            'need_change'=>1
            ] );


                        
            Youtube::create( [
            'youtube_id'=>62,
            'title'=>'camatasia AAA',
            'playlist'=>'uploads',
            'playlist_id'=>'UUiplEkp4yXoTg34HmV3M_pA',
            'embed'=>'https://www.youtube.com/embed/9U4q3aTDT8E',
            'img'=>'https://i.ytimg.com/vi/9U4q3aTDT8E/hqdefault.jpg',
            'video_id'=>'9U4q3aTDT8E',
            'date'=>'2016-09-29 13:15:06',
            'need_change'=>1
            ] );


                        
            Youtube::create( [
            'youtube_id'=>63,
            'title'=>'cachexia',
            'playlist'=>'uploads',
            'playlist_id'=>'UUiplEkp4yXoTg34HmV3M_pA',
            'embed'=>'https://www.youtube.com/embed/BjOEb8Acayw',
            'img'=>'https://i.ytimg.com/vi/BjOEb8Acayw/hqdefault.jpg',
            'video_id'=>'BjOEb8Acayw',
            'date'=>'2016-09-29 13:15:06',
            'need_change'=>1
            ] );


                        
            Youtube::create( [
            'youtube_id'=>64,
            'title'=>'basillar bifurcation',
            'playlist'=>'uploads',
            'playlist_id'=>'UUiplEkp4yXoTg34HmV3M_pA',
            'embed'=>'https://www.youtube.com/embed/9xIyI0zYyRc',
            'img'=>'https://i.ytimg.com/vi/9xIyI0zYyRc/hqdefault.jpg',
            'video_id'=>'9xIyI0zYyRc',
            'date'=>'2016-09-29 13:15:06',
            'need_change'=>1
            ] );


                        
            Youtube::create( [
            'youtube_id'=>65,
            'title'=>'assessement of normal diaphragm by all modes',
            'playlist'=>'uploads',
            'playlist_id'=>'UUiplEkp4yXoTg34HmV3M_pA',
            'embed'=>'https://www.youtube.com/embed/iQuH0j2dyPU',
            'img'=>'https://i.ytimg.com/vi/iQuH0j2dyPU/hqdefault.jpg',
            'video_id'=>'iQuH0j2dyPU',
            'date'=>'2016-09-29 13:15:06',
            'need_change'=>1
            ] );


                        
            Youtube::create( [
            'youtube_id'=>66,
            'title'=>'Art of Hemodynamics',
            'playlist'=>'uploads',
            'playlist_id'=>'UUiplEkp4yXoTg34HmV3M_pA',
            'embed'=>'https://www.youtube.com/embed/3NE0Oot0Aoc',
            'img'=>'https://i.ytimg.com/vi/3NE0Oot0Aoc/hqdefault.jpg',
            'video_id'=>'3NE0Oot0Aoc',
            'date'=>'2016-09-29 13:15:06',
            'need_change'=>1
            ] );


                        
            Youtube::create( [
            'youtube_id'=>67,
            'title'=>'aortic regurge6',
            'playlist'=>'uploads',
            'playlist_id'=>'UUiplEkp4yXoTg34HmV3M_pA',
            'embed'=>'https://www.youtube.com/embed/8crsuhbXXxI',
            'img'=>'https://i.ytimg.com/vi/8crsuhbXXxI/hqdefault.jpg',
            'video_id'=>'8crsuhbXXxI',
            'date'=>'2016-09-29 13:15:06',
            'need_change'=>1
            ] );


                        
            Youtube::create( [
            'youtube_id'=>68,
            'title'=>'aortic regurge5',
            'playlist'=>'uploads',
            'playlist_id'=>'UUiplEkp4yXoTg34HmV3M_pA',
            'embed'=>'https://www.youtube.com/embed/gsEAlTkyufA',
            'img'=>'https://i.ytimg.com/vi/gsEAlTkyufA/hqdefault.jpg',
            'video_id'=>'gsEAlTkyufA',
            'date'=>'2016-09-29 13:15:06',
            'need_change'=>1
            ] );


                        
            Youtube::create( [
            'youtube_id'=>69,
            'title'=>'aortic regurge4',
            'playlist'=>'uploads',
            'playlist_id'=>'UUiplEkp4yXoTg34HmV3M_pA',
            'embed'=>'https://www.youtube.com/embed/jTuzTDiE8kQ',
            'img'=>'https://i.ytimg.com/vi/jTuzTDiE8kQ/hqdefault.jpg',
            'video_id'=>'jTuzTDiE8kQ',
            'date'=>'2016-09-29 13:15:06',
            'need_change'=>1
            ] );


                        
            Youtube::create( [
            'youtube_id'=>70,
            'title'=>'aortic regurge 3',
            'playlist'=>'uploads',
            'playlist_id'=>'UUiplEkp4yXoTg34HmV3M_pA',
            'embed'=>'https://www.youtube.com/embed/78VA8cCWQbo',
            'img'=>'https://i.ytimg.com/vi/78VA8cCWQbo/hqdefault.jpg',
            'video_id'=>'78VA8cCWQbo',
            'date'=>'2016-09-29 13:15:06',
            'need_change'=>1
            ] );


                        
            Youtube::create( [
            'youtube_id'=>71,
            'title'=>'aortic regurge 2',
            'playlist'=>'uploads',
            'playlist_id'=>'UUiplEkp4yXoTg34HmV3M_pA',
            'embed'=>'https://www.youtube.com/embed/Om7LEf9JqpQ',
            'img'=>'https://i.ytimg.com/vi/Om7LEf9JqpQ/hqdefault.jpg',
            'video_id'=>'Om7LEf9JqpQ',
            'date'=>'2016-09-29 13:15:06',
            'need_change'=>1
            ] );


                        
            Youtube::create( [
            'youtube_id'=>72,
            'title'=>'aortic regurge 1',
            'playlist'=>'uploads',
            'playlist_id'=>'UUiplEkp4yXoTg34HmV3M_pA',
            'embed'=>'https://www.youtube.com/embed/byn_MKLJSIM',
            'img'=>'https://i.ytimg.com/vi/byn_MKLJSIM/hqdefault.jpg',
            'video_id'=>'byn_MKLJSIM',
            'date'=>'2016-09-29 13:15:06',
            'need_change'=>1
            ] );


                        
            Youtube::create( [
            'youtube_id'=>73,
            'title'=>'all causes of aortic regurge',
            'playlist'=>'uploads',
            'playlist_id'=>'UUiplEkp4yXoTg34HmV3M_pA',
            'embed'=>'https://www.youtube.com/embed/S5XWRGzwrZM',
            'img'=>'https://i.ytimg.com/vi/S5XWRGzwrZM/hqdefault.jpg',
            'video_id'=>'S5XWRGzwrZM',
            'date'=>'2016-09-29 13:15:06',
            'need_change'=>1
            ] );
    }
}
