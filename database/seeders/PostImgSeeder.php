<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use App\Models\PostImg;

class PostImgSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        PostImg::create( [
        'id'=>1,
        'pic'=>'file-1463591639-4YG8JY.jpg'
        ] );
                    
        PostImg::create( [
        'id'=>2,
        'pic'=>'file-1464165463-D3CBM9.jpg'
        ] );
                    
        PostImg::create( [
        'id'=>3,
        'pic'=>'file-1464106363-HWQX6B.jpg'
        ] );
                    
        PostImg::create( [
        'id'=>4,
        'pic'=>'file-1464165480-R231XR.jpg'
        ] );
                    
        PostImg::create( [
        'id'=>5,
        'pic'=>'im6.jpg'
        ] );
                    
        PostImg::create( [
        'id'=>6,
        'pic'=>'im5.jpg'
        ] );
                    
        PostImg::create( [
        'id'=>7,
        'pic'=>'im4.jpg'
        ] );
                    
        PostImg::create( [
        'id'=>8,
        'pic'=>'im3.jpg'
        ] );
                    
        PostImg::create( [
        'id'=>9,
        'pic'=>'im2.jpg'
        ] );
                    
        PostImg::create( [
        'id'=>10,
        'pic'=>'im1.jpg'
        ] );
                    
        PostImg::create( [
        'id'=>11,
        'pic'=>'file-1463591746-GEE3E7.jpg'
        ] );
                    
        PostImg::create( [
        'id'=>12,
        'pic'=>'file-1463591685-DDLMK9.jpg'
        ] );
                    
        PostImg::create( [
        'id'=>13,
        'pic'=>'file-1463591644-ZGN2DA.jpg'
        ] );
                    
        PostImg::create( [
        'id'=>14,
        'pic'=>'file-1463591650-J14MMU.jpg'
        ] );
                    
        PostImg::create( [
        'id'=>15,
        'pic'=>'file-1463591725-9J88SF.jpg'
        ] );
                    
        PostImg::create( [
        'id'=>16,
        'pic'=>'file-1463591738-IZ3XFW.jpg'
        ] );
                    
        PostImg::create( [
        'id'=>17,
        'pic'=>'file-1463591752-XK911T.jpg'
        ] );
                    
        PostImg::create( [
        'id'=>18,
        'pic'=>'file-1464105730-LQ3Z2Q.jpg'
        ] );
                    
        PostImg::create( [
        'id'=>19,
        'pic'=>'covid-1.jpg'
        ] );
                    
        PostImg::create( [
        'id'=>20,
        'pic'=>'covid-2.jpg'
        ] );
                    
        PostImg::create( [
        'id'=>21,
        'pic'=>'covid-3.jpg'
        ] );
                    
        PostImg::create( [
        'id'=>22,
        'pic'=>'covid-4.jpg'
        ] );
                    
        PostImg::create( [
        'id'=>23,
        'pic'=>'covid-5.jpg'
        ] );
                    
        PostImg::create( [
        'id'=>24,
        'pic'=>'covid-6.jpg'
        ] );
                    
        PostImg::create( [
        'id'=>25,
        'pic'=>'covid-7.jpg'
        ] );
                    
        PostImg::create( [
        'id'=>26,
        'pic'=>'covid-8.jpg'
        ] );
                    
        PostImg::create( [
        'id'=>27,
        'pic'=>'covid-9.jpg'
        ] );
                    
        PostImg::create( [
        'id'=>28,
        'pic'=>'covid-10.jpg'
        ] );
                    
        PostImg::create( [
        'id'=>29,
        'pic'=>'covid-11.jpg'
        ] );
                    
        PostImg::create( [
        'id'=>30,
        'pic'=>'covid-12.jpg'
        ] );
    }
}
