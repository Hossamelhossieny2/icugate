<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use App\Models\Config;

class ConfigSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Config::create([
            'id' => 1,
            'conf_title' => "Agrofod",
            'conf_desc' => "Agrofod Description",
            'conf_username' => "admin",
            'conf_password' => "c20ad4d76fe97759aa27a0c99bff6710",
            'conf_mail' => "logistics@agrofood.org",
            'conf_welcome' => "",
            'conf_address' => "",
            'conf_phone' => "002 010 0240 6728",
            'conf_fax' => "",
            'conf_facebook' => "",
            'conf_twitter' => "",
            'conf_google_plus' => "",
            'conf_inquire_image' => "",
            'conf_inquire_title' => ""

        ]);
    }
}
