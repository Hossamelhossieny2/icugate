<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use App\Models\Post;

class Posts3Seeder extends Seeder
{
   
    public function run()
    {

                                                                                    
        Post::create( [
        'id'=>3800,
        'cat_id'=>8,
        'tag_id'=>8,
        'title'=>'Vancomycin-Associated Nephrotoxicity in the Critically Ill: A Retrospective Multivariate Regression Analysis',
        'text'=>'Hanrahan, Timothy P and others\r\nCritical Care Medicine. December 2014 42 12:2527â€“2536',
        'img'=>'file-1463591657-K8BRDF.jpg',
        'file'=>NULL,
        'link'=>'http://journals.lww.com/ccmjournal/Abstract/2014/12000/Vancomycin_Associated_Nephrotoxicity_in_the.8.aspx',
        'by'=>0,
        'journal'=>'Site Admin',
        'type'=>0,
        'month_article'=>0,
        'famous'=>0,
        'views'=>1550,
        'show'=>0,
        'date'=>'2014-12-01 17:53:16',
        'active'=>'1',
        'user'=>0,
        'created_at'=>'2020-05-15 01:20:06',
        'updated_at'=>'2020-06-21 00:08:23',
        'deleted_at'=>NULL
        ] );
        
        
                    
        Post::create( [
        'id'=>3801,
        'cat_id'=>8,
        'tag_id'=>7,
        'title'=>'Safety, Feasibility, and Outcomes of Induced Hypothermia Therapy Following In-Hospital Cardiac Arrestâ€”Evaluation of a Large Prospective Registry',
        'text'=>'Dankiewicz, Josef and others\r\nCritical Care Medicine. December 2014 42 12:2537â€“2545',
        'img'=>'file-1463591681-AGRM7H.jpg',
        'file'=>NULL,
        'link'=>'http://journals.lww.com/ccmjournal/Abstract/2014/12000/Safety,_Feasibility,_and_Outcomes_of_Induced.9.aspx',
        'by'=>0,
        'journal'=>'Site Admin',
        'type'=>0,
        'month_article'=>0,
        'famous'=>0,
        'views'=>1499,
        'show'=>0,
        'date'=>'2014-12-01 17:54:36',
        'active'=>'1',
        'user'=>0,
        'created_at'=>'2020-05-15 01:20:06',
        'updated_at'=>'2020-06-21 00:08:23',
        'deleted_at'=>NULL
        ] );
        
        
                    
        Post::create( [
        'id'=>3802,
        'cat_id'=>8,
        'tag_id'=>9,
        'title'=>'Critical Illnessâ€“Related Corticosteroid Insufficiency in Cirrhotic Patients With Acute Gastroesophageal Variceal Bleeding: Risk Factors and Association With Outcome',
        'text'=>'Tsai, Ming-Hung and others\r\nCritical Care Medicine. December 2014 42 12:2546â€“2555',
        'img'=>'file-1463591639-4YG8JY.jpg',
        'file'=>NULL,
        'link'=>'http://journals.lww.com/ccmjournal/Abstract/2014/12000/Critical_Illness_Related_Corticosteroid.10.aspx',
        'by'=>0,
        'journal'=>'Site Admin',
        'type'=>0,
        'month_article'=>0,
        'famous'=>0,
        'views'=>1499,
        'show'=>0,
        'date'=>'2014-12-01 17:57:29',
        'active'=>'1',
        'user'=>0,
        'created_at'=>'2020-05-15 01:20:06',
        'updated_at'=>'2020-06-21 00:08:23',
        'deleted_at'=>NULL
        ] );
        
        
                    
        Post::create( [
        'id'=>3803,
        'cat_id'=>8,
        'tag_id'=>3,
        'title'=>'Feeding the Critically Ill Patient',
        'text'=>'McClave, Stephen A and others\r\nCritical Care Medicine. December 2014 42 12:2600â€“2610',
        'img'=>'file-1463591657-K8BRDF.jpg',
        'file'=>NULL,
        'link'=>'http://journals.lww.com/ccmjournal/Abstract/2014/12000/Feeding_the_Critically_Ill_Patient.16.aspx',
        'by'=>0,
        'journal'=>'Site Admin',
        'type'=>0,
        'month_article'=>0,
        'famous'=>0,
        'views'=>1499,
        'show'=>0,
        'date'=>'2014-12-01 18:00:27',
        'active'=>'1',
        'user'=>0,
        'created_at'=>'2020-05-15 01:20:06',
        'updated_at'=>'2020-06-21 00:08:23',
        'deleted_at'=>NULL
        ] );
        
        
                    
        Post::create( [
        'id'=>3804,
        'cat_id'=>8,
        'tag_id'=>7,
        'title'=>'Transfusion Triggers for Guiding RBC Transfusion for Cardiovascular Surgery: A Systematic Review and Meta-Analysis',
        'text'=>'Curley, Gerard F and others\r\nCritical Care Medicine. December 2014 42 12:2611â€“2624',
        'img'=>'file-1463591746-GEE3E7.jpg',
        'file'=>NULL,
        'link'=>'http://journals.lww.com/ccmjournal/Abstract/2014/12000/Transfusion_Triggers_for_Guiding_RBC_Transfusion.17.aspx',
        'by'=>0,
        'journal'=>'Site Admin',
        'type'=>0,
        'month_article'=>0,
        'famous'=>0,
        'views'=>1499,
        'show'=>0,
        'date'=>'2014-12-01 18:02:45',
        'active'=>'1',
        'user'=>0,
        'created_at'=>'2020-05-15 01:20:06',
        'updated_at'=>'2020-06-21 00:08:23',
        'deleted_at'=>NULL
        ] );
        
        
                    
        Post::create( [
        'id'=>3805,
        'cat_id'=>9,
        'tag_id'=>1,
        'title'=>'Ebola Virus Resources',
        'text'=>'Centers for Disease Control and Prevention 2014',
        'img'=>'file-1463591644-ZGN2DA.jpg',
        'file'=>NULL,
        'link'=>'http://www.cdc.gov/vhf/ebola/index.html',
        'by'=>0,
        'journal'=>'Site Admin',
        'type'=>0,
        'month_article'=>0,
        'famous'=>0,
        'views'=>1528,
        'show'=>0,
        'date'=>'2014-11-01 17:00:17',
        'active'=>'1',
        'user'=>0,
        'created_at'=>'2020-05-15 01:20:06',
        'updated_at'=>'2020-06-21 00:08:23',
        'deleted_at'=>NULL
        ] );
        
        
                    
        Post::create( [
        'id'=>3806,
        'cat_id'=>12,
        'tag_id'=>1,
        'title'=>'Evaluating Ebola Therapies â€” The Case for RCTs  Online First',
        'text'=>'E. Cox, L. Borio, and R. Temple\r\nN Eng J Med. December 3, 2014',
        'img'=>'file-1463591644-ZGN2DA.jpg',
        'file'=>NULL,
        'link'=>'http://www.nejm.org/doi/full/10.1056/NEJMp1414145?query=TOC',
        'by'=>0,
        'journal'=>'Site Admin',
        'type'=>0,
        'month_article'=>0,
        'famous'=>0,
        'views'=>1639,
        'show'=>0,
        'date'=>'2014-12-03 17:03:44',
        'active'=>'1',
        'user'=>0,
        'created_at'=>'2020-05-15 01:20:06',
        'updated_at'=>'2020-06-21 00:08:23',
        'deleted_at'=>NULL
        ] );
        
        
                    
        Post::create( [
        'id'=>3807,
        'cat_id'=>10,
        'tag_id'=>5,
        'title'=>'Proposed algorithm in the use of brain oximetry.Near-infrared spectroscopy as an index of brain and tissue oxygenation',
        'text'=>'J. M. Murkin and M. Arango\r\nBr. J. Anaesth. 2009 103 suppl 1: i3-i13.',
        'img'=>'file-1463591725-9J88SF.jpg',
        'file'=>NULL,
        'link'=>'http://bja.oxfordjournals.org/content/103/suppl_1/i3/F2.large.jpg',
        'by'=>0,
        'journal'=>'Site Admin',
        'type'=>0,
        'month_article'=>0,
        'famous'=>1,
        'views'=>1607,
        'show'=>0,
        'date'=>'2009-04-01 15:51:56',
        'active'=>'1',
        'user'=>0,
        'created_at'=>'2020-05-15 01:20:06',
        'updated_at'=>'2020-06-21 00:08:23',
        'deleted_at'=>NULL
        ] );
        
        
                    
        Post::create( [
        'id'=>3808,
        'cat_id'=>10,
        'tag_id'=>5,
        'title'=>'Proposed algorithm in the use of brain oximetry.Near-infrared spectroscopy as an index of brain and tissue oxygenation',
        'text'=>'J. M. Murkin and M. Arango\r\nBr. J. Anaesth. 2009 103 suppl 1: i3-i13',
        'img'=>'file-1463591746-GEE3E7.jpg',
        'file'=>NULL,
        'link'=>'http://bja.oxfordjournals.org/content/103/suppl_1/i3/F2.large.jpg',
        'by'=>0,
        'journal'=>'Site Admin',
        'type'=>0,
        'month_article'=>0,
        'famous'=>0,
        'views'=>1512,
        'show'=>0,
        'date'=>'2009-04-01 15:54:14',
        'active'=>'1',
        'user'=>0,
        'created_at'=>'2020-05-15 01:20:06',
        'updated_at'=>'2020-06-21 00:08:23',
        'deleted_at'=>NULL
        ] );
        
        
                    
        Post::create( [
        'id'=>3809,
        'cat_id'=>8,
        'tag_id'=>2,
        'title'=>'urrent practices and barriers impairing physiciansâ€™ and nursesâ€™ adherence to analgo-sedation recommendations in the intensive care unit - a national survey',
        'text'=>'Sneyers B, Laterre P, Perreault MM, Wouters D, Spinewine A\r\nCritical Care 2014, 18 :655 5 December 2014',
        'img'=>'file-1463591650-J14MMU.jpg',
        'file'=>NULL,
        'link'=>'http://ccforum.com/content/18/6/655/abstract',
        'by'=>0,
        'journal'=>'Site Admin',
        'type'=>0,
        'month_article'=>0,
        'famous'=>0,
        'views'=>1570,
        'show'=>0,
        'date'=>'2014-12-05 17:47:27',
        'active'=>'1',
        'user'=>0,
        'created_at'=>'2020-05-15 01:20:06',
        'updated_at'=>'2020-06-21 00:08:23',
        'deleted_at'=>NULL
        ] );
        
        
                    
        Post::create( [
        'id'=>3810,
        'cat_id'=>8,
        'tag_id'=>1,
        'title'=>'Decreasing the time to achieve therapeutic vancomycin concentrations in critically Ill patients: developing and testing of a dosing nomogram',
        'text'=>'Baptista J, Roberts JA, Sousa E, Freitas R, Deveza N, Pimentel J\r\nCritical Care 2014, 18 :654 5 December 2014',
        'img'=>'file-1463591725-9J88SF.jpg',
        'file'=>NULL,
        'link'=>'http://ccforum.com/content/18/6/654/abstract',
        'by'=>0,
        'journal'=>'Site Admin',
        'type'=>0,
        'month_article'=>0,
        'famous'=>0,
        'views'=>1528,
        'show'=>0,
        'date'=>'2014-12-05 17:48:23',
        'active'=>'1',
        'user'=>0,
        'created_at'=>'2020-05-15 01:20:06',
        'updated_at'=>'2020-06-21 00:08:23',
        'deleted_at'=>NULL
        ] );
        
        
                    
        Post::create( [
        'id'=>3811,
        'cat_id'=>8,
        'tag_id'=>7,
        'title'=>'Jugular vein distensibility predicts fluid responsiveness in septic patients',
        'text'=>'Guarracino F, Ferro B, Forfori F, Bertini P, Magliacane L, Pinsky MR\r\nCritical Care 2014, 18 :647 5 December 2014',
        'img'=>'file-1463591685-DDLMK9.jpg',
        'file'=>NULL,
        'link'=>'http://ccforum.com/content/18/6/647/abstract',
        'by'=>0,
        'journal'=>'Site Admin',
        'type'=>0,
        'month_article'=>0,
        'famous'=>0,
        'views'=>1499,
        'show'=>0,
        'date'=>'2014-12-05 17:49:31',
        'active'=>'1',
        'user'=>0,
        'created_at'=>'2020-05-15 01:20:06',
        'updated_at'=>'2020-06-21 00:08:23',
        'deleted_at'=>NULL
        ] );
        
        
                    
        Post::create( [
        'id'=>3812,
        'cat_id'=>8,
        'tag_id'=>7,
        'title'=>'The protective effects of phosphodiesterase-5 inhibitor, sildenafil on post-resuscitation cardiac dysfunction of cardiac arrest: metabolic evidence from microdialysis',
        'text'=>'Zhang Q, Yuan W, Wang G, Wu J, Wang M, Li C\r\nCritical Care 2014, 18 :641 5 December 2014',
        'img'=>'file-1463591639-4YG8JY.jpg',
        'file'=>NULL,
        'link'=>'http://ccforum.com/content/18/6/641/abstract',
        'by'=>0,
        'journal'=>'Site Admin',
        'type'=>0,
        'month_article'=>0,
        'famous'=>0,
        'views'=>1499,
        'show'=>0,
        'date'=>'2014-12-05 17:50:28',
        'active'=>'1',
        'user'=>0,
        'created_at'=>'2020-05-15 01:20:06',
        'updated_at'=>'2020-06-21 00:08:23',
        'deleted_at'=>NULL
        ] );
        
        
                    
        Post::create( [
        'id'=>3813,
        'cat_id'=>12,
        'tag_id'=>1,
        'title'=>'Vitamin D deficiency as a risk factor for infection, sepsis and mortality in the critically ill: systematic review and meta-analysis',
        'text'=>'de Haan K, Groeneveld A, de Geus H, Egal M, Struijs A\r\nCritical Care 2014, 18 :660 5 December 2014',
        'img'=>'file-1463591732-SM39NY.jpg',
        'file'=>NULL,
        'link'=>'http://ccforum.com/content/18/6/660/abstract',
        'by'=>0,
        'journal'=>'Site Admin',
        'type'=>0,
        'month_article'=>0,
        'famous'=>0,
        'views'=>1639,
        'show'=>0,
        'date'=>'2014-12-05 17:56:05',
        'active'=>'1',
        'user'=>0,
        'created_at'=>'2020-05-15 01:20:06',
        'updated_at'=>'2020-06-21 00:08:23',
        'deleted_at'=>NULL
        ] );
        
        
                    
        Post::create( [
        'id'=>3814,
        'cat_id'=>8,
        'tag_id'=>7,
        'title'=>'Temporal trends in cardiac arrest incidence and outcome in Finnish intensive care units from 2003 to 2013',
        'text'=>'I. Efendijev, R. Raj, M. Reinikainen, S. Hoppu, M. B. Skrifvars\r\nIntensive Care Med J. December 2014 4012:1853 - 1861',
        'img'=>'file-1463591644-ZGN2DA.jpg',
        'file'=>NULL,
        'link'=>'http://icmjournal.esicm.org/journals/abstract.html?v=40&j=134&i=12&a=3509_10.1007_s00134-014-3509-z&doi=',
        'by'=>0,
        'journal'=>'Site Admin',
        'type'=>0,
        'month_article'=>0,
        'famous'=>0,
        'views'=>1499,
        'show'=>0,
        'date'=>'2014-12-02 18:36:54',
        'active'=>'1',
        'user'=>0,
        'created_at'=>'2020-05-15 01:20:06',
        'updated_at'=>'2020-06-21 00:08:23',
        'deleted_at'=>NULL
        ] );
        
        
                    
        Post::create( [
        'id'=>3815,
        'cat_id'=>8,
        'tag_id'=>6,
        'title'=>'Efficiency of gas transfer in venovenous extracorporeal membrane oxygenation: analysis of 317 cases with four different ECMO systems',
        'text'=>'Karla Lehle and others\r\nIntensive Care Med J. December 2014 4012:1870 - 1877',
        'img'=>'file-1463591738-IZ3XFW.jpg',
        'file'=>NULL,
        'link'=>'http://icmjournal.esicm.org/journals/abstract.html?v=40&j=134&i=12&a=3489_10.1007_s00134-014-3489-z&doi=',
        'by'=>0,
        'journal'=>'Site Admin',
        'type'=>0,
        'month_article'=>0,
        'famous'=>0,
        'views'=>1499,
        'show'=>0,
        'date'=>'2014-12-02 18:43:20',
        'active'=>'1',
        'user'=>0,
        'created_at'=>'2020-05-15 01:20:06',
        'updated_at'=>'2020-06-21 00:08:23',
        'deleted_at'=>NULL
        ] );
        
        
                    
        Post::create( [
        'id'=>3816,
        'cat_id'=>8,
        'tag_id'=>1,
        'title'=>'Association between intravenous chloride load during resuscitation and in-hospital mortality among patients with SIRSOpen access',
        'text'=>'Andrew D. Shaw and others\r\nIntensive Care Med J. December 2014 4012:1897 - 1905',
        'img'=>'file-1463591752-XK911T.jpg',
        'file'=>NULL,
        'link'=>'http://icmjournal.esicm.org/journals/abstract.html?v=40&j=134&i=12&a=3505_10.1007_s00134-014-3505-3&doi=',
        'by'=>0,
        'journal'=>'Site Admin',
        'type'=>0,
        'month_article'=>0,
        'famous'=>0,
        'views'=>1528,
        'show'=>0,
        'date'=>'2014-12-02 18:46:34',
        'active'=>'1',
        'user'=>0,
        'created_at'=>'2020-05-15 01:20:06',
        'updated_at'=>'2020-06-21 00:08:23',
        'deleted_at'=>NULL
        ] );
        
        
                    
        Post::create( [
        'id'=>3817,
        'cat_id'=>8,
        'tag_id'=>1,
        'title'=>'Temporal trends in critical events complicating HIV infection: 1999â€“2010 multicentre cohort study in France',
        'text'=>'FranÃ§ois Barbier and others\r\nIntensive Care Med J. December 2014 4012:1906 - 1915',
        'img'=>'file-1463591681-AGRM7H.jpg',
        'file'=>NULL,
        'link'=>'http://icmjournal.esicm.org/journals/abstract.html?v=40&j=134&i=12&a=3481_10.1007_s00134-014-3481-7&doi=',
        'by'=>0,
        'journal'=>'Site Admin',
        'type'=>0,
        'month_article'=>0,
        'famous'=>0,
        'views'=>1528,
        'show'=>0,
        'date'=>'2014-12-02 18:50:10',
        'active'=>'1',
        'user'=>0,
        'created_at'=>'2020-05-15 01:20:06',
        'updated_at'=>'2020-06-21 00:08:23',
        'deleted_at'=>NULL
        ] );
        
        
                    
        Post::create( [
        'id'=>3818,
        'cat_id'=>12,
        'tag_id'=>1,
        'title'=>'Early therapy with IgM-enriched polyclonal immunoglobulin in patients with septic shock',
        'text'=>'Ilaria Cavazzuti and others\r\nIntensive Care Med J. December 2014 4012:1888-1896',
        'img'=>'file-1463591650-J14MMU.jpg',
        'file'=>NULL,
        'link'=>'http://icmjournal.esicm.org/journals/abstract.html?v=40&j=134&i=12&a=3474_10.1007_s00134-014-3474-6&doi=',
        'by'=>0,
        'journal'=>'Site Admin',
        'type'=>0,
        'month_article'=>0,
        'famous'=>0,
        'views'=>1639,
        'show'=>0,
        'date'=>'2014-12-02 16:44:03',
        'active'=>'1',
        'user'=>0,
        'created_at'=>'2020-05-15 01:20:06',
        'updated_at'=>'2020-06-21 00:08:23',
        'deleted_at'=>NULL
        ] );
        
        
                    
        Post::create( [
        'id'=>3819,
        'cat_id'=>8,
        'tag_id'=>7,
        'title'=>'The impact of hydroxyethyl starches in cardiac surgery â€“ a meta-analysis',
        'text'=>'Jacob M, Fellahi J, Chappell D, Kurz A \r\nCritical Care 2014, 18 :656 4 December 2014',
        'img'=>'file-1463591639-4YG8JY.jpg',
        'file'=>NULL,
        'link'=>'http://ccforum.com/content/18/6/656/abstract',
        'by'=>0,
        'journal'=>'Site Admin',
        'type'=>0,
        'month_article'=>0,
        'famous'=>0,
        'views'=>1499,
        'show'=>0,
        'date'=>'2014-12-03 21:38:46',
        'active'=>'1',
        'user'=>0,
        'created_at'=>'2020-05-15 01:20:06',
        'updated_at'=>'2020-06-21 00:08:23',
        'deleted_at'=>NULL
        ] );
        
        
                    
        Post::create( [
        'id'=>3820,
        'cat_id'=>9,
        'tag_id'=>11,
        'title'=>'Expert consensus and recommendations on safety criteria for active mobilization of mechanically ventilated critically ill adults',
        'text'=>'Hodgson CL and others\r\nCritical Care 2014, 18 :658 4 December 2014',
        'img'=>'file-1463591650-J14MMU.jpg',
        'file'=>NULL,
        'link'=>'http://ccforum.com/content/pdf/s13054-014-0658-y.pdf',
        'by'=>0,
        'journal'=>'Site Admin',
        'type'=>0,
        'month_article'=>0,
        'famous'=>0,
        'views'=>1499,
        'show'=>0,
        'date'=>'2014-12-03 21:40:45',
        'active'=>'1',
        'user'=>0,
        'created_at'=>'2020-05-15 01:20:06',
        'updated_at'=>'2020-06-21 00:08:23',
        'deleted_at'=>NULL
        ] );
        
        
                    
        Post::create( [
        'id'=>3821,
        'cat_id'=>8,
        'tag_id'=>1,
        'title'=>'Ebola Vaccine â€” An Urgent International Priority',
        'text'=>'R. Kanapathipillai and Others \r\nN Engl J Med 2014371:2249-2251',
        'img'=>'file-1463591738-IZ3XFW.jpg',
        'file'=>NULL,
        'link'=>'http://www.nejm.org/doi/full/10.1056/NEJMp1412166',
        'by'=>0,
        'journal'=>'Site Admin',
        'type'=>0,
        'month_article'=>0,
        'famous'=>0,
        'views'=>1528,
        'show'=>0,
        'date'=>'2014-12-01 06:25:26',
        'active'=>'1',
        'user'=>0,
        'created_at'=>'2020-05-15 01:20:06',
        'updated_at'=>'2020-06-21 00:08:23',
        'deleted_at'=>NULL
        ] );
        
        
                    
        Post::create( [
        'id'=>3822,
        'cat_id'=>8,
        'tag_id'=>5,
        'title'=>'Very Early Administration of Progesterone for Acute Traumatic Brain Injury',
        'text'=>'D.W. Wright and Others\r\nN Engl J Med. December 10, 2014',
        'img'=>'file-1463591732-SM39NY.jpg',
        'file'=>NULL,
        'link'=>'http://www.nejm.org/doi/full/10.1056/NEJMoa1404304?query=TOC',
        'by'=>0,
        'journal'=>'Site Admin',
        'type'=>0,
        'month_article'=>0,
        'famous'=>0,
        'views'=>1512,
        'show'=>0,
        'date'=>'2014-12-10 06:27:16',
        'active'=>'1',
        'user'=>0,
        'created_at'=>'2020-05-15 01:20:06',
        'updated_at'=>'2020-06-21 00:08:23',
        'deleted_at'=>NULL
        ] );
        
        
                    
        Post::create( [
        'id'=>3823,
        'cat_id'=>8,
        'tag_id'=>8,
        'title'=>'Disorders of Fluids and Electrolytes: Lactic Acidosis',
        'text'=>'J.A. Kraut and N.E. Madias\r\nN Engl J Med 2014371:2309-2319',
        'img'=>'file-1463591732-SM39NY.jpg',
        'file'=>NULL,
        'link'=>'http://www.nejm.org/doi/full/10.1056/NEJMra1309483?query=TOC',
        'by'=>0,
        'journal'=>'Site Admin',
        'type'=>0,
        'month_article'=>0,
        'famous'=>0,
        'views'=>1550,
        'show'=>0,
        'date'=>'2014-12-09 06:28:46',
        'active'=>'1',
        'user'=>0,
        'created_at'=>'2020-05-15 01:20:06',
        'updated_at'=>'2020-06-21 00:08:23',
        'deleted_at'=>NULL
        ] );
        
        
                    
        Post::create( [
        'id'=>3824,
        'cat_id'=>8,
        'tag_id'=>1,
        'title'=>'Impact of ertapenem on antimicrobial resistance in a sentinel group of Gram-negative bacilli: a 6 year antimicrobial resistance surveillance study',
        'text'=>'Carlos A. Rodriguez-Osorio and others\r\nJ. Antimicrob. Chemother. published 4 December 2014',
        'img'=>'file-1463591752-XK911T.jpg',
        'file'=>NULL,
        'link'=>'http://jac.oxfordjournals.org/content/early/2014/12/04/jac.dku471.abstract?papetoc',
        'by'=>0,
        'journal'=>'Site Admin',
        'type'=>0,
        'month_article'=>0,
        'famous'=>0,
        'views'=>1528,
        'show'=>0,
        'date'=>'2014-12-04 06:34:54',
        'active'=>'1',
        'user'=>0,
        'created_at'=>'2020-05-15 01:20:06',
        'updated_at'=>'2020-06-21 00:08:23',
        'deleted_at'=>NULL
        ] );
        
        
                    
        Post::create( [
        'id'=>3825,
        'cat_id'=>8,
        'tag_id'=>1,
        'title'=>'Antifungal susceptibilities of Candida glabrata species complex, Candida krusei, Candida parapsilosis species complex and Candida tropicalis causing invasive candidiasis in China: 3 year national surveillance',
        'text'=>'Meng Xiao and others\r\nJ. Antimicrob. Chemother. published 3 December 2014',
        'img'=>'file-1463591681-AGRM7H.jpg',
        'file'=>NULL,
        'link'=>'http://jac.oxfordjournals.org/content/early/2014/12/02/jac.dku460.abstract?papetoc',
        'by'=>0,
        'journal'=>'Site Admin',
        'type'=>0,
        'month_article'=>0,
        'famous'=>0,
        'views'=>1528,
        'show'=>0,
        'date'=>'2014-12-03 06:38:21',
        'active'=>'1',
        'user'=>0,
        'created_at'=>'2020-05-15 01:20:06',
        'updated_at'=>'2020-06-21 00:08:23',
        'deleted_at'=>NULL
        ] );
        
        
                    
        Post::create( [
        'id'=>3826,
        'cat_id'=>8,
        'tag_id'=>1,
        'title'=>'Safety and clinical outcomes of carbapenem de-escalation as part of an antimicrobial stewardship programme in an ESBL-endemic setting',
        'text'=>'Kaung Yuan Lew, Tat Ming Ng, Michelle Tan, Sock Hoon Tan, Ee Ling Lew, Li Min Ling, Brenda Ang, David Lye, and Christine B. Teng\r\nJ. Antimicrob. Chemother. published 3 December 2014',
        'img'=>'file-1463591752-XK911T.jpg',
        'file'=>NULL,
        'link'=>'http://jac.oxfordjournals.org/content/early/2014/12/02/jac.dku479.abstract?papetoc',
        'by'=>0,
        'journal'=>'Site Admin',
        'type'=>0,
        'month_article'=>0,
        'famous'=>0,
        'views'=>1528,
        'show'=>0,
        'date'=>'2014-12-03 06:40:12',
        'active'=>'1',
        'user'=>0,
        'created_at'=>'2020-05-15 01:20:06',
        'updated_at'=>'2020-06-21 00:08:23',
        'deleted_at'=>NULL
        ] );
        
        
                    
        Post::create( [
        'id'=>3827,
        'cat_id'=>9,
        'tag_id'=>5,
        'title'=>'AHA/ASA Guidelines for the Primary Prevention of Stroke',
        'text'=>'Stroke. 2014 45: 3754-3832 Published online before print October 28, 2014,',
        'img'=>'file-1463591746-GEE3E7.jpg',
        'file'=>NULL,
        'link'=>'http://my.americanheart.org/professional/ScienceNews/New-Guidelines-for-the-Primary-Stroke-Prevention-A-Closer-Step-Toward_UCM_469086_Article.jsp',
        'by'=>0,
        'journal'=>'Site Admin',
        'type'=>0,
        'month_article'=>0,
        'famous'=>0,
        'views'=>1512,
        'show'=>0,
        'date'=>'2014-10-28 06:47:29',
        'active'=>'1',
        'user'=>0,
        'created_at'=>'2020-05-15 01:20:06',
        'updated_at'=>'2020-06-21 00:08:23',
        'deleted_at'=>NULL
        ] );
        
        
                    
        Post::create( [
        'id'=>3828,
        'cat_id'=>13,
        'tag_id'=>5,
        'title'=>'ACC/ AHA CV Risk assessment Tool Calculator',
        'text'=>'This downloadable spreadsheet is a companion tool to the 2013 ACC/AHA Guideline on the Assessment of Cardiovascular Risk',
        'img'=>'file-1463591752-XK911T.jpg',
        'file'=>NULL,
        'link'=>'http://my.americanheart.org/professional/StatementsGuidelines/PreventionGuidelines/Prevention-Guidelines_UCM_457698_SubHomePage.jsp',
        'by'=>0,
        'journal'=>'Site Admin',
        'type'=>0,
        'month_article'=>0,
        'famous'=>0,
        'views'=>1512,
        'show'=>0,
        'date'=>'2014-10-28 06:53:36',
        'active'=>'1',
        'user'=>0,
        'created_at'=>'2020-05-15 01:20:06',
        'updated_at'=>'2020-06-21 00:08:23',
        'deleted_at'=>NULL
        ] );
        
        
                    
        Post::create( [
        'id'=>3829,
        'cat_id'=>8,
        'tag_id'=>5,
        'title'=>'ASA/AHA Updated Guidelines for Primary Prevention of Stroke',
        'text'=>'Medscape Education Clinical Briefs\r\nNews Author: Pauline Anderson\r\nCME Author: Charles P. Vega, MD',
        'img'=>'file-1463591685-DDLMK9.jpg',
        'file'=>NULL,
        'link'=>'http://www.medscape.org/viewarticle/834329?nlid=71508_2808&src=cmemp',
        'by'=>0,
        'journal'=>'Site Admin',
        'type'=>0,
        'month_article'=>0,
        'famous'=>0,
        'views'=>1512,
        'show'=>0,
        'date'=>'2014-12-05 06:58:44',
        'active'=>'1',
        'user'=>0,
        'created_at'=>'2020-05-15 01:20:06',
        'updated_at'=>'2020-06-21 00:08:23',
        'deleted_at'=>NULL
        ] );
        
        
                    
        Post::create( [
        'id'=>3830,
        'cat_id'=>8,
        'tag_id'=>5,
        'title'=>'Clinical neurophysiological assessment of sepsis-associated brain dysfunction: a systematic review',
        'text'=>'Hosokawa K, Gaspard N, Su F, Oddo M, Vincent J, Taccone F\r\nCritical Care 2014, 18 :674 8 December 2014',
        'img'=>'file-1463591725-9J88SF.jpg',
        'file'=>NULL,
        'link'=>'http://ccforum.com/content/18/6/674/abstract',
        'by'=>0,
        'journal'=>'Site Admin',
        'type'=>0,
        'month_article'=>0,
        'famous'=>0,
        'views'=>1512,
        'show'=>0,
        'date'=>'2014-12-08 07:35:07',
        'active'=>'1',
        'user'=>0,
        'created_at'=>'2020-05-15 01:20:06',
        'updated_at'=>'2020-06-21 00:08:23',
        'deleted_at'=>NULL
        ] );
        
        
                    
        Post::create( [
        'id'=>3831,
        'cat_id'=>8,
        'tag_id'=>1,
        'title'=>'Intravenous immunoglobulin for severe sepsis and septic shock: clinical effectiveness, cost effectiveness and value of a further randomised controlled trial',
        'text'=>'Soares MO, Welton NJ, Harrison DA, Peura P, Shankar-Hari M, Harvey SE, Madan J, Ades AE, Rowan KM, Palmer SJ\r\nCritical Care 2014, 18 :649 1 December 2014',
        'img'=>'file-1463591650-J14MMU.jpg',
        'file'=>NULL,
        'link'=>'http://ccforum.com/content/18/6/649/abstract',
        'by'=>0,
        'journal'=>'Site Admin',
        'type'=>0,
        'month_article'=>0,
        'famous'=>0,
        'views'=>1528,
        'show'=>0,
        'date'=>'2014-12-01 07:37:49',
        'active'=>'1',
        'user'=>0,
        'created_at'=>'2020-05-15 01:20:06',
        'updated_at'=>'2020-06-21 00:08:23',
        'deleted_at'=>NULL
        ] );
        
        
                    
        Post::create( [
        'id'=>3832,
        'cat_id'=>8,
        'tag_id'=>7,
        'title'=>'A practical approach to goal-directed echocardiography in the critical care setting',
        'text'=>'Walley PE, Walley KR, Goodgame B, Punjabi V, Sirounis D\r\nCritical Care 2014, 18 :681 1 December 2014',
        'img'=>'file-1463591738-IZ3XFW.jpg',
        'file'=>NULL,
        'link'=>'http://ccforum.com/content/18/6/681/abstract',
        'by'=>0,
        'journal'=>'Site Admin',
        'type'=>0,
        'month_article'=>0,
        'famous'=>0,
        'views'=>1499,
        'show'=>0,
        'date'=>'2014-12-01 07:39:19',
        'active'=>'1',
        'user'=>0,
        'created_at'=>'2020-05-15 01:20:06',
        'updated_at'=>'2020-06-21 00:08:23',
        'deleted_at'=>NULL
        ] );
        
        
                    
        Post::create( [
        'id'=>3833,
        'cat_id'=>8,
        'tag_id'=>7,
        'title'=>'Does pulse pressure variation predict fluid responsiveness in critically ill patients A systematic review and meta-analysis',
        'text'=>'Yang X, Du B\r\n\r\nCritical Care 2014, 18 :650 27 November 2014',
        'img'=>'file-1463591752-XK911T.jpg',
        'file'=>NULL,
        'link'=>'http://ccforum.com/content/18/6/650/abstract',
        'by'=>0,
        'journal'=>'Site Admin',
        'type'=>0,
        'month_article'=>0,
        'famous'=>0,
        'views'=>1499,
        'show'=>0,
        'date'=>'2014-12-01 07:40:22',
        'active'=>'1',
        'user'=>0,
        'created_at'=>'2020-05-15 01:20:06',
        'updated_at'=>'2020-06-21 00:08:23',
        'deleted_at'=>NULL
        ] );
        
        
                    
        Post::create( [
        'id'=>3834,
        'cat_id'=>8,
        'tag_id'=>6,
        'title'=>'Effectiveness of inhaled furosemide for acute asthma exacerbation: a meta-analysis',
        'text'=>'Inokuchi R, Aoki A, Aoki Y, Yahagi N\r\nCritical Care 2014, 18 :621 24 November 2014',
        'img'=>'file-1463591738-IZ3XFW.jpg',
        'file'=>NULL,
        'link'=>'http://ccforum.com/content/18/6/621',
        'by'=>0,
        'journal'=>'Site Admin',
        'type'=>0,
        'month_article'=>0,
        'famous'=>1,
        'views'=>1594,
        'show'=>0,
        'date'=>'2014-11-24 07:44:08',
        'active'=>'1',
        'user'=>0,
        'created_at'=>'2020-05-15 01:20:06',
        'updated_at'=>'2020-06-21 00:08:23',
        'deleted_at'=>NULL
        ] );
        
        
                    
        Post::create( [
        'id'=>3835,
        'cat_id'=>8,
        'tag_id'=>1,
        'title'=>'Clinical relevance of procalcitonin and C-reactive protein as infection markers in renal impairment: a cross sectional study',
        'text'=>'Park J, Kim D, Jang H, Kim M, Jung S, Lee J, Huh W, Kim Y, Kim D, Oh H\r\nCritical Care 2014, 18 :640 19 November 2014',
        'img'=>'file-1463591685-DDLMK9.jpg',
        'file'=>NULL,
        'link'=>'http://ccforum.com/content/18/6/640/abstract',
        'by'=>0,
        'journal'=>'Site Admin',
        'type'=>0,
        'month_article'=>0,
        'famous'=>0,
        'views'=>1528,
        'show'=>0,
        'date'=>'2014-11-19 07:46:23',
        'active'=>'1',
        'user'=>0,
        'created_at'=>'2020-05-15 01:20:06',
        'updated_at'=>'2020-06-21 00:08:23',
        'deleted_at'=>NULL
        ] );
        
        
                    
        Post::create( [
        'id'=>3836,
        'cat_id'=>12,
        'tag_id'=>6,
        'title'=>'Effectiveness of inhaled furosemide for acute asthma exacerbation: a meta-analysis',
        'text'=>'Inokuchi R, Aoki A, Aoki Y, Yahagi N\r\nCritical Care 2014, 18 :621 24 November 2014',
        'img'=>'file-1463591746-GEE3E7.jpg',
        'file'=>NULL,
        'link'=>'http://ccforum.com/content/18/6/621',
        'by'=>0,
        'journal'=>'Site Admin',
        'type'=>0,
        'month_article'=>0,
        'famous'=>0,
        'views'=>1610,
        'show'=>0,
        'date'=>'2014-11-24 16:44:16',
        'active'=>'1',
        'user'=>0,
        'created_at'=>'2020-05-15 01:20:06',
        'updated_at'=>'2020-06-21 00:08:23',
        'deleted_at'=>NULL
        ] );
        
        
                    
        Post::create( [
        'id'=>3837,
        'cat_id'=>8,
        'tag_id'=>5,
        'title'=>'Assessment of brain midline shift using sonography in neurosurgical ICU patients',
        'text'=>'Motuel J, Biette I, Srairi M, Mrozek S, Kurrek MM, Chaynes P, Cognard C, Fourcade O, Geeraerts T\r\nCritical Care 2014, 18 :676 9 December 2014',
        'img'=>'file-1463591685-DDLMK9.jpg',
        'file'=>NULL,
        'link'=>'http://ccforum.com/content/18/6/676/abstract',
        'by'=>0,
        'journal'=>'Site Admin',
        'type'=>0,
        'month_article'=>0,
        'famous'=>0,
        'views'=>1512,
        'show'=>0,
        'date'=>'2014-12-09 16:47:44',
        'active'=>'1',
        'user'=>0,
        'created_at'=>'2020-05-15 01:20:06',
        'updated_at'=>'2020-06-21 00:08:23',
        'deleted_at'=>NULL
        ] );
        
        
                    
        Post::create( [
        'id'=>3838,
        'cat_id'=>8,
        'tag_id'=>10,
        'title'=>'Role of the massive transfusion protocol in the management of haemorrhagic shock',
        'text'=>'J. H. Waters\r\nBr. J. Anaesth. 2014 113: ii3-ii8',
        'img'=>'file-1463591685-DDLMK9.jpg',
        'file'=>NULL,
        'link'=>'http://bja.oxfordjournals.org/content/113/suppl_2/ii3.abstract?etoc',
        'by'=>0,
        'journal'=>'Site Admin',
        'type'=>0,
        'month_article'=>0,
        'famous'=>0,
        'views'=>1499,
        'show'=>0,
        'date'=>'2014-12-13 06:25:19',
        'active'=>'1',
        'user'=>0,
        'created_at'=>'2020-05-15 01:20:06',
        'updated_at'=>'2020-06-21 00:08:23',
        'deleted_at'=>NULL
        ] );
        
        
                    
        Post::create( [
        'id'=>3839,
        'cat_id'=>8,
        'tag_id'=>1,
        'title'=>'Methylglyoxal as a new biomarker in patients with septic shock: an observational clinical study',
        'text'=>'Brenner T, Fleming T, Uhle F, Silaff S, Schmitt F, Salgado E, Ulrich A, Zimmermann S, Bruckner T, Martin E, Bierhaus A, Nawroth PP, Weigand MA, Hofer S \r\nCritical Care 2014, 18 :683 12 December 2014',
        'img'=>'file-1463591746-GEE3E7.jpg',
        'file'=>NULL,
        'link'=>'http://ccforum.com/content/18/6/683/abstract',
        'by'=>0,
        'journal'=>'Site Admin',
        'type'=>0,
        'month_article'=>0,
        'famous'=>0,
        'views'=>1528,
        'show'=>0,
        'date'=>'2014-12-12 06:59:28',
        'active'=>'1',
        'user'=>0,
        'created_at'=>'2020-05-15 01:20:06',
        'updated_at'=>'2020-06-21 00:08:23',
        'deleted_at'=>NULL
        ] );
        
        
                    
        Post::create( [
        'id'=>3840,
        'cat_id'=>8,
        'tag_id'=>3,
        'title'=>'Early high protein intake is associated with low mortality and energy overfeeding with high mortality in non-septic mechanically ventilated critically ill patients',
        'text'=>'Weijs P, Looijaard W, Beishuizen A, Girbes A, Oudemans-van Straaten HM \r\nCritical Care 2014, 18 :701 14 December 2014',
        'img'=>'file-1463591681-AGRM7H.jpg',
        'file'=>NULL,
        'link'=>'http://ccforum.com/content/18/6/701/abstract',
        'by'=>0,
        'journal'=>'Site Admin',
        'type'=>0,
        'month_article'=>0,
        'famous'=>0,
        'views'=>1499,
        'show'=>0,
        'date'=>'2014-12-14 14:34:01',
        'active'=>'1',
        'user'=>0,
        'created_at'=>'2020-05-15 01:20:06',
        'updated_at'=>'2020-06-21 00:08:23',
        'deleted_at'=>NULL
        ] );
        
        
                    
        Post::create( [
        'id'=>3841,
        'cat_id'=>8,
        'tag_id'=>4,
        'title'=>'Mechanism of action of tranexamic acid in bleeding trauma patients: an exploratory analysis of data from the CRASH-2 trial',
        'text'=>'Roberts I, Prieto-Merino D, Manno D \r\nCritical Care 2014, 18 :685 13 December 2014',
        'img'=>'file-1463591738-IZ3XFW.jpg',
        'file'=>NULL,
        'link'=>'http://ccforum.com/content/18/6/685/abstract',
        'by'=>0,
        'journal'=>'Site Admin',
        'type'=>0,
        'month_article'=>0,
        'famous'=>0,
        'views'=>1596,
        'show'=>0,
        'date'=>'2014-12-13 14:34:45',
        'active'=>'1',
        'user'=>0,
        'created_at'=>'2020-05-15 01:20:06',
        'updated_at'=>'2020-06-21 00:08:23',
        'deleted_at'=>NULL
        ] );
        
        
                    
        Post::create( [
        'id'=>3842,
        'cat_id'=>8,
        'tag_id'=>1,
        'title'=>'Association between Pseudomonas aeruginosa type III secretion, antibiotic resistance, and clinical outcome: a review',
        'text'=>'Sawa T, Shimizu M, Moriyama K, Wiener-Kronish JP \r\nCritical Care 2014, 18 :668 13 December 2014',
        'img'=>'file-1463591746-GEE3E7.jpg',
        'file'=>NULL,
        'link'=>'http://ccforum.com/content/18/6/668/abstract',
        'by'=>0,
        'journal'=>'Site Admin',
        'type'=>0,
        'month_article'=>0,
        'famous'=>0,
        'views'=>1528,
        'show'=>0,
        'date'=>'2014-12-13 14:35:31',
        'active'=>'1',
        'user'=>0,
        'created_at'=>'2020-05-15 01:20:06',
        'updated_at'=>'2020-06-21 00:08:23',
        'deleted_at'=>NULL
        ] );
        
        
                    
        Post::create( [
        'id'=>3843,
        'cat_id'=>8,
        'tag_id'=>1,
        'title'=>'Antifungal drugs during pregnancy: an updated review',
        'text'=>'BenoÃ®t Pilmis, Vincent Jullien, Jack Sobel, Marc Lecuit, Olivier Lortholary, and Caroline Charlier\r\nJ. Antimicrob. Chemother. 2014 70: 14-22',
        'img'=>'file-1463591644-ZGN2DA.jpg',
        'file'=>NULL,
        'link'=>'http://jac.oxfordjournals.org/content/70/1/14.abstract?etoc',
        'by'=>0,
        'journal'=>'Site Admin',
        'type'=>0,
        'month_article'=>0,
        'famous'=>0,
        'views'=>1528,
        'show'=>0,
        'date'=>'2014-12-16 16:53:18',
        'active'=>'1',
        'user'=>0,
        'created_at'=>'2020-05-15 01:20:06',
        'updated_at'=>'2020-06-21 00:08:23',
        'deleted_at'=>NULL
        ] );
        
        
                    
        Post::create( [
        'id'=>3844,
        'cat_id'=>8,
        'tag_id'=>1,
        'title'=>'The spread of carbapenemase-producing bacteria in Africa: a systematic review',
        'text'=>'Rendani I. Manenzhe, Heather J. Zar, Mark P. Nicol, and Mamadou Kaba\r\nJ. Antimicrob. Chemother. 2014 70: 23-40',
        'img'=>'file-1463591738-IZ3XFW.jpg',
        'file'=>NULL,
        'link'=>'http://jac.oxfordjournals.org/content/70/1/23.abstract?etoc',
        'by'=>0,
        'journal'=>'Site Admin',
        'type'=>0,
        'month_article'=>0,
        'famous'=>0,
        'views'=>1528,
        'show'=>0,
        'date'=>'2014-12-16 16:55:16',
        'active'=>'1',
        'user'=>0,
        'created_at'=>'2020-05-15 01:20:06',
        'updated_at'=>'2020-06-21 00:08:23',
        'deleted_at'=>NULL
        ] );
        
        
                    
        Post::create( [
        'id'=>3845,
        'cat_id'=>8,
        'tag_id'=>1,
        'title'=>'Î²-Lactam/Î²-lactamase inhibitors versus carbapenems for the treatment of sepsis: systematic review and meta-analysis of randomized controlled trials',
        'text'=>'Shachaf Shiber, Dafna Yahav, Tomer Avni, Leonard Leibovici, and Mical Paul\r\nJ. Antimicrob. Chemother. 2014 70: 41-47',
        'img'=>'file-1463591725-9J88SF.jpg',
        'file'=>NULL,
        'link'=>'http://jac.oxfordjournals.org/content/70/1/41.abstract?etoc',
        'by'=>0,
        'journal'=>'Site Admin',
        'type'=>0,
        'month_article'=>0,
        'famous'=>0,
        'views'=>1528,
        'show'=>0,
        'date'=>'2014-12-16 16:56:07',
        'active'=>'1',
        'user'=>0,
        'created_at'=>'2020-05-15 01:20:06',
        'updated_at'=>'2020-06-21 00:08:23',
        'deleted_at'=>NULL
        ] );
        
        
                    
        Post::create( [
        'id'=>3846,
        'cat_id'=>9,
        'tag_id'=>11,
        'title'=>'Critical Care Protocol Toolkit',
        'text'=>'SOCIETY OF CRITICAL CARE MEDICINE\r\nCLINICAL PHARMACY & PHARMACOLOGY SECTION\r\nEDITORS: WILLIAM R. VINCENT III, PHARMD\r\nRUSSEL J. ROBERTS, PHARMD\r\nNICOLE T. REARDON, PHARMD\r\nCreated January 2012, updated Sept 2014',
        'img'=>'file-1463591644-ZGN2DA.jpg',
        'file'=>NULL,
        'link'=>'http://sccmmedia.sccm.org/documents/LearnICU/CPP-Protocol-Toolkit-2014.pdf',
        'by'=>0,
        'journal'=>'Site Admin',
        'type'=>0,
        'month_article'=>0,
        'famous'=>0,
        'views'=>1499,
        'show'=>0,
        'date'=>'2014-09-01 20:25:27',
        'active'=>'1',
        'user'=>0,
        'created_at'=>'2020-05-15 01:20:06',
        'updated_at'=>'2020-06-21 00:08:23',
        'deleted_at'=>NULL
        ] );
        
        
                    
        Post::create( [
        'id'=>3847,
        'cat_id'=>10,
        'tag_id'=>11,
        'title'=>'Critical Care Protocol Toolkit \r\nSOCIETY OF CRITICAL CARE MEDICINE\r\nCLINICAL PHARMACY & PHARMACOLOGY SECTION',
        'text'=>'EDITORS: WILLIAM R. VINCENT III, PHARMD\r\nRUSSEL J. ROBERTS, PHARMD\r\nNICOLE T. REARDON, PHARMD\r\nCreated January 2012, updated Sept 2014',
        'img'=>'file-1463591639-4YG8JY.jpg',
        'file'=>NULL,
        'link'=>'http://sccmmedia.sccm.org/documents/LearnICU/CPP-Protocol-Toolkit-2014.pdf',
        'by'=>0,
        'journal'=>'Site Admin',
        'type'=>0,
        'month_article'=>0,
        'famous'=>0,
        'views'=>1499,
        'show'=>0,
        'date'=>'2014-09-01 20:27:10',
        'active'=>'1',
        'user'=>0,
        'created_at'=>'2020-05-15 01:20:06',
        'updated_at'=>'2020-06-21 00:08:23',
        'deleted_at'=>NULL
        ] );
        
        
                    
        Post::create( [
        'id'=>3848,
        'cat_id'=>8,
        'tag_id'=>11,
        'title'=>'Cytisine versus Nicotine for Smoking Cessation',
        'text'=>'N. Walker and Others\r\nN Engl J Med. December 18, 2014371:2353-2362',
        'img'=>'file-1463591650-J14MMU.jpg',
        'file'=>NULL,
        'link'=>'http://www.nejm.org/doi/full/10.1056/NEJMoa1407764?query=TOC',
        'by'=>0,
        'journal'=>'Site Admin',
        'type'=>0,
        'month_article'=>0,
        'famous'=>0,
        'views'=>1499,
        'show'=>0,
        'date'=>'2014-12-18 19:17:04',
        'active'=>'1',
        'user'=>0,
        'created_at'=>'2020-05-15 01:20:06',
        'updated_at'=>'2020-06-21 00:08:23',
        'deleted_at'=>NULL
        ] );
        
        
                    
        Post::create( [
        'id'=>3849,
        'cat_id'=>8,
        'tag_id'=>5,
        'title'=>'A Randomized Trial of Intraarterial Treatment for Acute Ischemic Stroke',
        'text'=>'O.A. Berkhemer and Others\r\nN Eng J Med. December 17, 2014',
        'img'=>'file-1463591650-J14MMU.jpg',
        'file'=>NULL,
        'link'=>'http://www.nejm.org/doi/full/10.1056/NEJMoa1411587?query=TOC',
        'by'=>0,
        'journal'=>'Site Admin',
        'type'=>0,
        'month_article'=>0,
        'famous'=>1,
        'views'=>1607,
        'show'=>0,
        'date'=>'2014-12-17 19:20:40',
        'active'=>'1',
        'user'=>0,
        'created_at'=>'2020-05-15 01:20:06',
        'updated_at'=>'2020-06-21 00:08:23',
        'deleted_at'=>NULL
        ] );
        
        
                    
        Post::create( [
        'id'=>3850,
        'cat_id'=>12,
        'tag_id'=>1,
        'title'=>'The efficacy and safety of plasma exchange in patients with sepsis and septic shock: a systematic review and meta-analysis',
        'text'=>'Rimmer E and others\r\nCritical Care 2014, 18 :69920 December 2014',
        'img'=>'file-1463591732-SM39NY.jpg',
        'file'=>NULL,
        'link'=>'http://ccforum.com/content/18/6/699/abstract',
        'by'=>0,
        'journal'=>'Site Admin',
        'type'=>0,
        'month_article'=>0,
        'famous'=>0,
        'views'=>1639,
        'show'=>0,
        'date'=>'2014-12-21 18:24:40',
        'active'=>'1',
        'user'=>0,
        'created_at'=>'2020-05-15 01:20:06',
        'updated_at'=>'2020-06-21 00:08:23',
        'deleted_at'=>NULL
        ] );
        
        
                    
        Post::create( [
        'id'=>3851,
        'cat_id'=>8,
        'tag_id'=>7,
        'title'=>'The arterial blood pressure associated with terminal cardiovascular collapse in critically ill patients: a retrospective cohort study',
        'text'=>'Brunauer A, KokÃ¶fer A, Bataar O, Gradwohl-Matis I, Dankl D, DÃ¼nser MW\r\nCritical Care 2014, 18 :719 19 December 2014',
        'img'=>'file-1463591644-ZGN2DA.jpg',
        'file'=>NULL,
        'link'=>'http://ccforum.com/content/18/6/719/abstract',
        'by'=>0,
        'journal'=>'Site Admin',
        'type'=>0,
        'month_article'=>0,
        'famous'=>0,
        'views'=>1499,
        'show'=>0,
        'date'=>'2014-12-19 12:00:00',
        'active'=>'1',
        'user'=>0,
        'created_at'=>'2020-05-15 01:20:06',
        'updated_at'=>'2020-06-21 00:08:23',
        'deleted_at'=>NULL
        ] );
        
        
                    
        Post::create( [
        'id'=>3852,
        'cat_id'=>8,
        'tag_id'=>6,
        'title'=>'Percutaneous and surgical tracheostomy in critically ill adult patients: a meta-analysis',
        'text'=>'Putensen C, Theuerkauf NU, Guenther U, Vargas M, Pelosi P\r\nCritical Care, 19 December 2014',
        'img'=>'file-1463591746-GEE3E7.jpg',
        'file'=>NULL,
        'link'=>'http://ccforum.com/content/18/6/544/abstract',
        'by'=>0,
        'journal'=>'Site Admin',
        'type'=>0,
        'month_article'=>0,
        'famous'=>0,
        'views'=>1499,
        'show'=>0,
        'date'=>'2014-12-19 12:02:44',
        'active'=>'1',
        'user'=>0,
        'created_at'=>'2020-05-15 01:20:06',
        'updated_at'=>'2020-06-21 00:08:23',
        'deleted_at'=>NULL
        ] );
        
        
                    
        Post::create( [
        'id'=>3853,
        'cat_id'=>8,
        'tag_id'=>5,
        'title'=>'Propranolol, post-traumatic stress disorder, and intensive care: incorporating new advances in psychiatry into the ICU',
        'text'=>'Gardner A, Griffiths J\r\nCritical Care 2014, 18:69819 December 2014',
        'img'=>'file-1463591681-AGRM7H.jpg',
        'file'=>NULL,
        'link'=>'http://ccforum.com/content/18/6/698/abstract',
        'by'=>0,
        'journal'=>'Site Admin',
        'type'=>0,
        'month_article'=>0,
        'famous'=>0,
        'views'=>1512,
        'show'=>0,
        'date'=>'2014-12-19 12:05:03',
        'active'=>'1',
        'user'=>0,
        'created_at'=>'2020-05-15 01:20:06',
        'updated_at'=>'2020-06-21 00:08:23',
        'deleted_at'=>NULL
        ] );
        
        
                    
        Post::create( [
        'id'=>3854,
        'cat_id'=>8,
        'tag_id'=>5,
        'title'=>'Very Early Administration of Progesterone for Acute Traumatic Brain Injury',
        'text'=>'David W. Wright and otherrs\r\nN Engl J Med 2014 371:2457-2466, December 25, 2014',
        'img'=>'file-1463591738-IZ3XFW.jpg',
        'file'=>NULL,
        'link'=>'http://www.nejm.org/doi/full/10.1056/NEJMoa1404304?query=TOC',
        'by'=>0,
        'journal'=>'Site Admin',
        'type'=>0,
        'month_article'=>0,
        'famous'=>1,
        'views'=>1607,
        'show'=>0,
        'date'=>'2014-12-25 05:34:40',
        'active'=>'1',
        'user'=>0,
        'created_at'=>'2020-05-15 01:20:06',
        'updated_at'=>'2020-06-21 00:08:23',
        'deleted_at'=>NULL
        ] );
        
        
                    
        Post::create( [
        'id'=>3855,
        'cat_id'=>8,
        'tag_id'=>5,
        'title'=>'Very Early Administration of Progesterone for Acute Traumatic Brain Injury',
        'text'=>'David W. Wright and otherrs\r\nN Engl J Med 2014 371:2457-2466, December 25, 2014',
        'img'=>'file-1463591644-ZGN2DA.jpg',
        'file'=>NULL,
        'link'=>'http://www.nejm.org/doi/full/10.1056/NEJMoa1404304?query=TOC',
        'by'=>0,
        'journal'=>'Site Admin',
        'type'=>0,
        'month_article'=>0,
        'famous'=>0,
        'views'=>1512,
        'show'=>0,
        'date'=>'0000-00-00 00:00:00',
        'active'=>'1',
        'user'=>0,
        'created_at'=>'2020-05-15 01:20:06',
        'updated_at'=>'2020-06-21 00:08:23',
        'deleted_at'=>NULL
        ] );
        
        
                    
        Post::create( [
        'id'=>3856,
        'cat_id'=>8,
        'tag_id'=>5,
        'title'=>'Very Early Administration of Progesterone for Acute Traumatic Brain Injury',
        'text'=>'David W. Wright and otherrs\r\nN Engl J Med 2014 371:2457-2466, December 25, 2014',
        'img'=>'file-1463591685-DDLMK9.jpg',
        'file'=>NULL,
        'link'=>'http://www.nejm.org/doi/full/10.1056/NEJMoa1404304?query=TOC',
        'by'=>0,
        'journal'=>'Site Admin',
        'type'=>0,
        'month_article'=>0,
        'famous'=>0,
        'views'=>1512,
        'show'=>0,
        'date'=>'2014-12-24 05:35:33',
        'active'=>'1',
        'user'=>0,
        'created_at'=>'2020-05-15 01:20:06',
        'updated_at'=>'2020-06-21 00:08:23',
        'deleted_at'=>NULL
        ] );
        
        
                    
        Post::create( [
        'id'=>3857,
        'cat_id'=>9,
        'tag_id'=>6,
        'title'=>'Difficult Airway Society DAS Extubation Guidelines',
        'text'=>'Difficult Airway Society Algorithm 2011',
        'img'=>'file-1463591681-AGRM7H.jpg',
        'file'=>NULL,
        'link'=>'http://www.das.uk.com/guidelines/das-extubation-guidelines1',
        'by'=>0,
        'journal'=>'Site Admin',
        'type'=>0,
        'month_article'=>0,
        'famous'=>0,
        'views'=>1499,
        'show'=>0,
        'date'=>'2011-12-01 09:39:47',
        'active'=>'1',
        'user'=>0,
        'created_at'=>'2020-05-15 01:20:06',
        'updated_at'=>'2020-06-21 00:08:23',
        'deleted_at'=>NULL
        ] );
        
        
                    
        Post::create( [
        'id'=>3858,
        'cat_id'=>9,
        'tag_id'=>6,
        'title'=>'Fibreoptic guided tracheal intubation through SAD using Aintree intubation catheter',
        'text'=>'Difficult Intubation Society\r\nIllustrations produced by Andy Morris, Medical Illustration Department, NHS Lanarkshire. 26/01/2011',
        'img'=>'file-1463591681-AGRM7H.jpg',
        'file'=>NULL,
        'link'=>'http://www.das.uk.com/files/AIC_abbreviated_Guide_Final_for_DAS.pdf',
        'by'=>0,
        'journal'=>'Site Admin',
        'type'=>0,
        'month_article'=>0,
        'famous'=>0,
        'views'=>1499,
        'show'=>0,
        'date'=>'2011-12-26 09:45:05',
        'active'=>'1',
        'user'=>0,
        'created_at'=>'2020-05-15 01:20:06',
        'updated_at'=>'2020-06-21 00:08:23',
        'deleted_at'=>NULL
        ] );
        
        
                    
        Post::create( [
        'id'=>3859,
        'cat_id'=>9,
        'tag_id'=>6,
        'title'=>'Intubation guidelines - Cannot Intubate, Cannot Ventilate',
        'text'=>'Difficult Airway Society 2Â­007',
        'img'=>'file-1463591650-J14MMU.jpg',
        'file'=>NULL,
        'link'=>'http://www.das.uk.com/guidelines/cvci.html',
        'by'=>0,
        'journal'=>'Site Admin',
        'type'=>0,
        'month_article'=>0,
        'famous'=>0,
        'views'=>1499,
        'show'=>0,
        'date'=>'2007-01-01 09:48:00',
        'active'=>'1',
        'user'=>0,
        'created_at'=>'2020-05-15 01:20:06',
        'updated_at'=>'2020-06-21 00:08:23',
        'deleted_at'=>NULL
        ] );
        
        
                    
        Post::create( [
        'id'=>3860,
        'cat_id'=>9,
        'tag_id'=>6,
        'title'=>'Rapid sequence induction, non-pregnant adult patient, no predicted difficulty',
        'text'=>'Difficult Intubation Society 2006',
        'img'=>'file-1463591657-K8BRDF.jpg',
        'file'=>NULL,
        'link'=>'http://www.das.uk.com/guidelines/rsi.html',
        'by'=>0,
        'journal'=>'Site Admin',
        'type'=>0,
        'month_article'=>0,
        'famous'=>0,
        'views'=>1499,
        'show'=>0,
        'date'=>'2006-12-01 09:49:55',
        'active'=>'1',
        'user'=>0,
        'created_at'=>'2020-05-15 01:20:06',
        'updated_at'=>'2020-06-21 00:08:23',
        'deleted_at'=>NULL
        ] );
        
        
                    
        Post::create( [
        'id'=>3861,
        'cat_id'=>9,
        'tag_id'=>6,
        'title'=>'Strategy for intubation byÂ­ direct laryngoscopy, no predictedÂ­ airway problem, no risk of regurgitation',
        'text'=>'Difficult Intubation Society',
        'img'=>'file-1463591650-J14MMU.jpg',
        'file'=>NULL,
        'link'=>'http://www.das.uk.com/guidelines/ddl.html',
        'by'=>0,
        'journal'=>'Site Admin',
        'type'=>0,
        'month_article'=>0,
        'famous'=>0,
        'views'=>1499,
        'show'=>0,
        'date'=>'2006-01-01 09:51:39',
        'active'=>'1',
        'user'=>0,
        'created_at'=>'2020-05-15 01:20:06',
        'updated_at'=>'2020-06-21 00:08:23',
        'deleted_at'=>NULL
        ] );
        
        
                    
        Post::create( [
        'id'=>3862,
        'cat_id'=>9,
        'tag_id'=>6,
        'title'=>'Difficult Airway Society DAS Guidelines 2015: Some of the main changes',
        'text'=>'Difficult Airway Society\r\nFor your views, you may contact the society via email: intubation@das.uk.com',
        'img'=>'file-1463591738-IZ3XFW.jpg',
        'file'=>NULL,
        'link'=>'http://www.das.uk.com/files/DAS_intubation_guidelines_2015_update1.pdf',
        'by'=>0,
        'journal'=>'Site Admin',
        'type'=>0,
        'month_article'=>0,
        'famous'=>0,
        'views'=>1499,
        'show'=>0,
        'date'=>'2014-12-25 09:56:41',
        'active'=>'1',
        'user'=>0,
        'created_at'=>'2020-05-15 01:20:06',
        'updated_at'=>'2020-06-21 00:08:23',
        'deleted_at'=>NULL
        ] );
        
        
                    
        Post::create( [
        'id'=>3863,
        'cat_id'=>12,
        'tag_id'=>6,
        'title'=>'Difficult Airway Society DAS Guidelines 2015: Some of the main changes',
        'text'=>'Difficult Airway Society\r\nFor your views, you may contact the society via email: intubation@das.uk.com',
        'img'=>'file-1463591746-GEE3E7.jpg',
        'file'=>NULL,
        'link'=>'http://www.das.uk.com/files/DAS_intubation_guidelines_2015_update1.pdf',
        'by'=>0,
        'journal'=>'Site Admin',
        'type'=>0,
        'month_article'=>0,
        'famous'=>0,
        'views'=>1610,
        'show'=>0,
        'date'=>'2014-12-25 09:59:13',
        'active'=>'1',
        'user'=>0,
        'created_at'=>'2020-05-15 01:20:06',
        'updated_at'=>'2020-06-21 00:08:23',
        'deleted_at'=>NULL
        ] );
        
        
                    
        Post::create( [
        'id'=>3864,
        'cat_id'=>12,
        'tag_id'=>1,
        'title'=>'Proton pump inhibitors increase the risk for hospital-acquired Clostridium difficile infection in critically ill patients',
        'text'=>'Barletta JF, Sclar DA\r\nCritical Care 2014, 18 :714 24 December 2014',
        'img'=>'file-1463591732-SM39NY.jpg',
        'file'=>NULL,
        'link'=>'http://ccforum.com/content/18/6/714/abstract',
        'by'=>0,
        'journal'=>'Site Admin',
        'type'=>0,
        'month_article'=>0,
        'famous'=>0,
        'views'=>1639,
        'show'=>0,
        'date'=>'2014-12-25 17:36:45',
        'active'=>'1',
        'user'=>0,
        'created_at'=>'2020-05-15 01:20:06',
        'updated_at'=>'2020-06-21 00:08:23',
        'deleted_at'=>NULL
        ] );
        
        
                    
        Post::create( [
        'id'=>3865,
        'cat_id'=>8,
        'tag_id'=>6,
        'title'=>'Arterial hyperoxia and mortality in critically ill patients: a systematic review and meta-analysis',
        'text'=>'Damiani E, Adrario E, Girardis M, Romano R, Pelaia P, Singer M, Donati A\r\nCritical Care 2014, 18 :711 23 December 2014',
        'img'=>'file-1463591639-4YG8JY.jpg',
        'file'=>NULL,
        'link'=>'http://ccforum.com/content/18/6/711/abstract',
        'by'=>0,
        'journal'=>'Site Admin',
        'type'=>0,
        'month_article'=>0,
        'famous'=>0,
        'views'=>1499,
        'show'=>0,
        'date'=>'2014-12-23 19:04:17',
        'active'=>'1',
        'user'=>0,
        'created_at'=>'2020-05-15 01:20:06',
        'updated_at'=>'2020-06-21 00:08:23',
        'deleted_at'=>NULL
        ] );
        
        
                    
        Post::create( [
        'id'=>3866,
        'cat_id'=>8,
        'tag_id'=>11,
        'title'=>'EVERY ICU nurse and doctor should watch this film',
        'text'=>'Kathys story',
        'img'=>'file-1463591725-9J88SF.jpg',
        'file'=>NULL,
        'link'=>'http://www.youtube.com/watch?v=TFHP7WbICro&feature=youtu.be&goback=%2Egde_1836804_member_5862999898630164483',
        'by'=>0,
        'journal'=>'Site Admin',
        'type'=>0,
        'month_article'=>0,
        'famous'=>0,
        'views'=>1499,
        'show'=>0,
        'date'=>'2014-12-24 22:04:21',
        'active'=>'1',
        'user'=>0,
        'created_at'=>'2020-05-15 01:20:06',
        'updated_at'=>'2020-06-21 00:08:23',
        'deleted_at'=>NULL
        ] );
        
        
                    
        Post::create( [
        'id'=>3867,
        'cat_id'=>8,
        'tag_id'=>4,
        'title'=>'Thromboelastometry and organ failure in trauma patients: a prospective cohort study',
        'text'=>'MÃ¼ller M, Balvers K, Binnekade JM, Curry N, Stanworth S, Gaarder C, Kolstadbraaten KM, Rourke C, Brohi K, Goslings J, Juffermans NP\r\nCritical Care 2014, 18 :687 25 December 2014',
        'img'=>'file-1463591639-4YG8JY.jpg',
        'file'=>NULL,
        'link'=>'http://ccforum.com/content/18/6/687/abstract',
        'by'=>0,
        'journal'=>'Site Admin',
        'type'=>0,
        'month_article'=>0,
        'famous'=>0,
        'views'=>1596,
        'show'=>0,
        'date'=>'2014-12-25 19:10:16',
        'active'=>'1',
        'user'=>0,
        'created_at'=>'2020-05-15 01:20:06',
        'updated_at'=>'2020-06-21 00:08:23',
        'deleted_at'=>NULL
        ] );
        
        
                    
        Post::create( [
        'id'=>3868,
        'cat_id'=>8,
        'tag_id'=>1,
        'title'=>'Physiological changes after fluid bolus therapy in sepsis: a systematic review of contemporary data',
        'text'=>'Glassford NJ, Eastwood GM, Bellomo R\r\nCritical Care 2014, 18 :2557 27 December 2014',
        'img'=>'file-1463591644-ZGN2DA.jpg',
        'file'=>NULL,
        'link'=>'http://ccforum.com/content/18/6/2557/abstract',
        'by'=>0,
        'journal'=>'Site Admin',
        'type'=>0,
        'month_article'=>0,
        'famous'=>0,
        'views'=>1528,
        'show'=>0,
        'date'=>'2014-12-27 19:16:13',
        'active'=>'1',
        'user'=>0,
        'created_at'=>'2020-05-15 01:20:06',
        'updated_at'=>'2020-06-21 00:08:23',
        'deleted_at'=>NULL
        ] );
        
        
                    
        Post::create( [
        'id'=>3869,
        'cat_id'=>8,
        'tag_id'=>6,
        'title'=>'High flow nasal cannula oxygen versus non-invasive ventilation in patients with acute hypoxaemic respiratory failure undergoing flexible bronchoscopy - a prospective randomised trial',
        'text'=>'Simon M, Braune S, Frings D, Wiontzek A, Klose H, Kluge S\r\nCritical Care 2014, 18 :712 22 December 2014',
        'img'=>'file-1463591685-DDLMK9.jpg',
        'file'=>NULL,
        'link'=>'http://ccforum.com/content/18/6/712/abstract',
        'by'=>0,
        'journal'=>'Site Admin',
        'type'=>0,
        'month_article'=>0,
        'famous'=>0,
        'views'=>1499,
        'show'=>0,
        'date'=>'2014-12-22 19:22:29',
        'active'=>'1',
        'user'=>0,
        'created_at'=>'2020-05-15 01:20:06',
        'updated_at'=>'2020-06-21 00:08:23',
        'deleted_at'=>NULL
        ] );
        
        
                    
        Post::create( [
        'id'=>3870,
        'cat_id'=>12,
        'tag_id'=>8,
        'title'=>'Risk factors for acute kidney injury AKI in patients treated with polymyxin B and influence of AKI on mortality: a multicentre prospective cohort study',
        'text'=>'Maria Helena Rigatto and others\r\nJ. Antimicrob. Chemother. published 20 January 2015',
        'img'=>'file-1463591650-J14MMU.jpg',
        'file'=>NULL,
        'link'=>'http://jac.oxfordjournals.org/content/early/2015/01/20/jac.dku561.abstract?papetoc',
        'by'=>0,
        'journal'=>'Site Admin',
        'type'=>0,
        'month_article'=>0,
        'famous'=>0,
        'views'=>1661,
        'show'=>0,
        'date'=>'2015-01-20 08:55:13',
        'active'=>'1',
        'user'=>0,
        'created_at'=>'2020-05-15 01:20:06',
        'updated_at'=>'2020-06-21 00:08:23',
        'deleted_at'=>NULL
        ] );
        
        
                    
        Post::create( [
        'id'=>3871,
        'cat_id'=>8,
        'tag_id'=>6,
        'title'=>'Prevention of Ventilator-Associated Pneumonia and Ventilator-Associated Conditions: A Randomized Controlled Trial With Subglottic Secretion Suctioning',
        'text'=>'Damas, Pierre and others\r\nCritical Care Medicine:\r\nJanuary 2015 - Volume 43 - Issue 1 - p 22â€“30',
        'img'=>'file-1463591725-9J88SF.jpg',
        'file'=>NULL,
        'link'=>'http://journals.lww.com/ccmjournal/Abstract/2015/01000/Prevention_of_Ventilator_Associated_Pneumonia_and.4.aspx',
        'by'=>0,
        'journal'=>'Site Admin',
        'type'=>0,
        'month_article'=>0,
        'famous'=>0,
        'views'=>1499,
        'show'=>0,
        'date'=>'2015-01-01 19:56:08',
        'active'=>'1',
        'user'=>0,
        'created_at'=>'2020-05-15 01:20:06',
        'updated_at'=>'2020-06-21 00:08:23',
        'deleted_at'=>NULL
        ] );
        
        
                    
        Post::create( [
        'id'=>3872,
        'cat_id'=>8,
        'tag_id'=>1,
        'title'=>'Influence of n-3 Polyunsaturated Fatty Acids Enriched Lipid Emulsions on Nosocomial Infections and Clinical Outcomes in Critically Ill Patients: ICU Lipids Study',
        'text'=>'Grau-Carmona and others\r\nCritical Care Medicine:\r\nJanuary 2015 - Volume 43 - Issue 1 - p 31â€“39',
        'img'=>'file-1463591681-AGRM7H.jpg',
        'file'=>NULL,
        'link'=>'http://journals.lww.com/ccmjournal/Abstract/2015/01000/Influence_of_n_3_Polyunsaturated_Fatty_Acids.5.aspx',
        'by'=>0,
        'journal'=>'Site Admin',
        'type'=>0,
        'month_article'=>0,
        'famous'=>0,
        'views'=>1528,
        'show'=>0,
        'date'=>'2015-01-01 20:01:24',
        'active'=>'1',
        'user'=>0,
        'created_at'=>'2020-05-15 01:20:06',
        'updated_at'=>'2020-06-21 00:08:23',
        'deleted_at'=>NULL
        ] );
        
        
                    
        Post::create( [
        'id'=>3873,
        'cat_id'=>8,
        'tag_id'=>5,
        'title'=>'A Systematic Review of Risk Factors for Delirium in the ICU',
        'text'=>'Zaal, Irene J and others',
        'img'=>'file-1463591746-GEE3E7.jpg',
        'file'=>NULL,
        'link'=>'http://journals.lww.com/ccmjournal/Abstract/2015/01000/A_Systematic_Review_of_Risk_Factors_for_Delirium.6.aspx',
        'by'=>0,
        'journal'=>'Site Admin',
        'type'=>0,
        'month_article'=>0,
        'famous'=>0,
        'views'=>1512,
        'show'=>0,
        'date'=>'2015-01-01 20:02:35',
        'active'=>'1',
        'user'=>0,
        'created_at'=>'2020-05-15 01:20:06',
        'updated_at'=>'2020-06-21 00:08:23',
        'deleted_at'=>NULL
        ] );
        
        
                    
        Post::create( [
        'id'=>3874,
        'cat_id'=>8,
        'tag_id'=>10,
        'title'=>'Multicenter, Randomized, Placebo-Controlled Phase III Study of Pyridoxalated Hemoglobin Polyoxyethylene in Distributive Shock PHOENIX',
        'text'=>'Vincent, Jean-Louis and others\r\nCritical Care Medicine:\r\nJanuary 2015 - Volume 43 - Issue 1 - p 57â€“64',
        'img'=>'file-1463591752-XK911T.jpg',
        'file'=>NULL,
        'link'=>'http://journals.lww.com/ccmjournal/Abstract/2015/01000/Multicenter,_Randomized,_Placebo_Controlled_Phase.8.aspx',
        'by'=>0,
        'journal'=>'Site Admin',
        'type'=>0,
        'month_article'=>0,
        'famous'=>0,
        'views'=>1499,
        'show'=>0,
        'date'=>'2015-01-01 20:04:10',
        'active'=>'1',
        'user'=>0,
        'created_at'=>'2020-05-15 01:20:06',
        'updated_at'=>'2020-06-21 00:08:23',
        'deleted_at'=>NULL
        ] );
        
        
                    
        Post::create( [
        'id'=>3875,
        'cat_id'=>8,
        'tag_id'=>9,
        'title'=>'Abdominal Paracentesis Drainage Ahead of Percutaneous Catheter Drainage Benefits Patients Attacked by Acute Pancreatitis With Fluid Collections: A Retrospective Clinical Cohort',
        'text'=>'Liu, Wei-hui and others\r\nCritical Care Medicine:\r\nJanuary 2015 - Volume 43 - Issue 1 - p 109â€“119',
        'img'=>'file-1463591732-SM39NY.jpg',
        'file'=>NULL,
        'link'=>'http://journals.lww.com/ccmjournal/Abstract/2015/01000/Abdominal_Paracentesis_Drainage_Ahead_of.13.aspx',
        'by'=>0,
        'journal'=>'Site Admin',
        'type'=>0,
        'month_article'=>0,
        'famous'=>0,
        'views'=>1499,
        'show'=>0,
        'date'=>'2015-01-01 20:06:29',
        'active'=>'1',
        'user'=>0,
        'created_at'=>'2020-05-15 01:20:06',
        'updated_at'=>'2020-06-21 00:08:23',
        'deleted_at'=>NULL
        ] );
        
        
                    
        Post::create( [
        'id'=>3876,
        'cat_id'=>8,
        'tag_id'=>6,
        'title'=>'Extracorporeal Co2 Removal in Hypercapnic Patients At Risk of Noninvasive Ventilation Failure: A Matched Cohort Study With Historical Control',
        'text'=>'Del Sorbo, Lorenzo and others\r\nCritical Care Medicine:\r\nJanuary 2015 - Volume 43 - Issue 1 - p 120â€“127',
        'img'=>'file-1463591752-XK911T.jpg',
        'file'=>NULL,
        'link'=>'http://journals.lww.com/ccmjournal/Abstract/2015/01000/Extracorporeal_Co2_Removal_in_Hypercapnic_Patients.14.aspx',
        'by'=>0,
        'journal'=>'Site Admin',
        'type'=>0,
        'month_article'=>0,
        'famous'=>0,
        'views'=>1499,
        'show'=>0,
        'date'=>'2015-01-01 20:07:58',
        'active'=>'1',
        'user'=>0,
        'created_at'=>'2020-05-15 01:20:06',
        'updated_at'=>'2020-06-21 00:08:23',
        'deleted_at'=>NULL
        ] );
        
        
                    
        Post::create( [
        'id'=>3877,
        'cat_id'=>8,
        'tag_id'=>6,
        'title'=>'Type III procollagen is a reliable marker of ARDS-associated lung fibroproliferation',
        'text'=>'Jean-Marie Forel and others\r\nIntensive Care Med J. 411:1-11Jan. 2015',
        'img'=>'file-1463591657-K8BRDF.jpg',
        'file'=>NULL,
        'link'=>'http://icmjournal.esicm.org/journals/abstract.html?v=41&j=134&i=1&a=3524_10.1007_s00134-014-3524-0&doi=',
        'by'=>0,
        'journal'=>'Site Admin',
        'type'=>0,
        'month_article'=>0,
        'famous'=>0,
        'views'=>1499,
        'show'=>0,
        'date'=>'2015-01-01 19:58:14',
        'active'=>'1',
        'user'=>0,
        'created_at'=>'2020-05-15 01:20:06',
        'updated_at'=>'2020-06-21 00:08:23',
        'deleted_at'=>NULL
        ] );
        
        
                    
        Post::create( [
        'id'=>3878,
        'cat_id'=>8,
        'tag_id'=>1,
        'title'=>'Diagnostic accuracy of SeptiFast multi-pathogen real-time PCR in the setting of suspected healthcare-associated bloodstream infection',
        'text'=>'Geoffrey Warhurst and others\r\nIntensive Care Med J. 41186 - 93. Jan. 2015',
        'img'=>'file-1463591650-J14MMU.jpg',
        'file'=>NULL,
        'link'=>'http://icmjournal.esicm.org/journals/abstract.html?v=41&j=134&i=1&a=3551_10.1007_s00134-014-3551-x&doi=',
        'by'=>0,
        'journal'=>'Site Admin',
        'type'=>0,
        'month_article'=>0,
        'famous'=>0,
        'views'=>1528,
        'show'=>0,
        'date'=>'2015-01-01 20:12:45',
        'active'=>'1',
        'user'=>0,
        'created_at'=>'2020-05-15 01:20:06',
        'updated_at'=>'2020-06-21 00:08:23',
        'deleted_at'=>NULL
        ] );
        
        
                    
        Post::create( [
        'id'=>3879,
        'cat_id'=>8,
        'tag_id'=>1,
        'title'=>'Linezolid plasma and intrapulmonary concentrations in critically ill obese patients with ventilator-associated pneumonia: intermittent vs continuous administration',
        'text'=>'Gennaro De Pascale and others\r\nIntensive Care Med J. 41186-93. Jan. 2015',
        'img'=>'file-1463591738-IZ3XFW.jpg',
        'file'=>NULL,
        'link'=>'http://icmjournal.esicm.org/journals/abstract.html?v=41&j=134&i=1&a=3550_10.1007_s00134-014-3550-y&doi=',
        'by'=>0,
        'journal'=>'Site Admin',
        'type'=>0,
        'month_article'=>0,
        'famous'=>0,
        'views'=>1528,
        'show'=>0,
        'date'=>'2015-01-01 20:15:11',
        'active'=>'1',
        'user'=>0,
        'created_at'=>'2020-05-15 01:20:06',
        'updated_at'=>'2020-06-21 00:08:23',
        'deleted_at'=>NULL
        ] );
        
        
                    
        Post::create( [
        'id'=>3880,
        'cat_id'=>8,
        'tag_id'=>4,
        'title'=>'Acute cholecystitis: When to operate and how to do it safely',
        'text'=>'Peitzman, Andrew B and others\r\nJournal of Trauma and Acute Care Surgery, 7811-12: Jan. 2015',
        'img'=>'file-1463591725-9J88SF.jpg',
        'file'=>NULL,
        'link'=>'http://journals.lww.com/jtrauma/Fulltext/2015/01000/Acute_cholecystitis___When_to_operate_and_how_to.1.aspx',
        'by'=>0,
        'journal'=>'Site Admin',
        'type'=>0,
        'month_article'=>0,
        'famous'=>0,
        'views'=>1596,
        'show'=>0,
        'date'=>'2015-01-01 20:23:27',
        'active'=>'1',
        'user'=>0,
        'created_at'=>'2020-05-15 01:20:06',
        'updated_at'=>'2020-06-21 00:08:23',
        'deleted_at'=>NULL
        ] );
        
        
                    
        Post::create( [
        'id'=>3881,
        'cat_id'=>8,
        'tag_id'=>4,
        'title'=>'A protocol for the management of adhesive small bowel obstruction',
        'text'=>'Loftus, Tyler and others\r\nJournal of Trauma and Acute Care Surgery, 78113-21: Jan. 2015',
        'img'=>'file-1463591746-GEE3E7.jpg',
        'file'=>NULL,
        'link'=>'http://journals.lww.com/jtrauma/Fulltext/2015/01000/A_protocol_for_the_management_of_adhesive_small.2.aspx',
        'by'=>0,
        'journal'=>'Site Admin',
        'type'=>0,
        'month_article'=>0,
        'famous'=>0,
        'views'=>1596,
        'show'=>0,
        'date'=>'2015-01-01 20:26:43',
        'active'=>'1',
        'user'=>0,
        'created_at'=>'2020-05-15 01:20:06',
        'updated_at'=>'2020-06-21 00:08:23',
        'deleted_at'=>NULL
        ] );
        
        
                    
        Post::create( [
        'id'=>3882,
        'cat_id'=>8,
        'tag_id'=>4,
        'title'=>'Nonoperative management of hemodynamically unstable abdominal trauma patients with angioembolization and resuscitative endovascular balloon occlusion of the aorta',
        'text'=>'Ogura, Takayuki and others\r\nJournal of Trauma and Acute Care Surgery, 781132-135: Jan. 2015',
        'img'=>'file-1463591752-XK911T.jpg',
        'file'=>NULL,
        'link'=>'http://journals.lww.com/jtrauma/Abstract/2015/01000/Nonoperative_management_of_hemodynamically.18.aspx',
        'by'=>0,
        'journal'=>'Site Admin',
        'type'=>0,
        'month_article'=>0,
        'famous'=>0,
        'views'=>1596,
        'show'=>0,
        'date'=>'2015-01-01 20:29:58',
        'active'=>'1',
        'user'=>0,
        'created_at'=>'2020-05-15 01:20:06',
        'updated_at'=>'2020-06-21 00:08:23',
        'deleted_at'=>NULL
        ] );
        
        
                    
        Post::create( [
        'id'=>3883,
        'cat_id'=>8,
        'tag_id'=>4,
        'title'=>'Evaluation and management of blunt traumatic aortic injury: A practice management guideline from the Eastern Association for the Surgery of Trauma',
        'text'=>'Fox, Nicole and others\r\nJournal of Trauma and Acute Care Surgery, 781136-146: Jan. 2015',
        'img'=>'file-1463591746-GEE3E7.jpg',
        'file'=>NULL,
        'link'=>'http://journals.lww.com/jtrauma/Fulltext/2015/01000/Evaluation_and_management_of_blunt_traumatic.19.aspx',
        'by'=>0,
        'journal'=>'Site Admin',
        'type'=>0,
        'month_article'=>0,
        'famous'=>0,
        'views'=>1596,
        'show'=>0,
        'date'=>'2015-01-01 20:31:41',
        'active'=>'1',
        'user'=>0,
        'created_at'=>'2020-05-15 01:20:06',
        'updated_at'=>'2020-06-21 00:08:23',
        'deleted_at'=>NULL
        ] );
        
        
                    
        Post::create( [
        'id'=>3884,
        'cat_id'=>8,
        'tag_id'=>4,
        'title'=>'Traumatic intra-abdominal hemorrhage control: Has current technology tipped the balance toward a role for prehospital intervention',
        'text'=>'Chaudery, Muzzafer and others\r\nJournal of Trauma and Acute Care Surgery, 781153-163: Jan. 2015',
        'img'=>'file-1463591650-J14MMU.jpg',
        'file'=>NULL,
        'link'=>'http://journals.lww.com/jtrauma/Abstract/2015/01000/Traumatic_intra_abdominal_hemorrhage_control___Has.21.aspx',
        'by'=>0,
        'journal'=>'Site Admin',
        'type'=>0,
        'month_article'=>0,
        'famous'=>0,
        'views'=>1596,
        'show'=>0,
        'date'=>'2015-01-01 20:33:15',
        'active'=>'1',
        'user'=>0,
        'created_at'=>'2020-05-15 01:20:06',
        'updated_at'=>'2020-06-21 00:08:23',
        'deleted_at'=>NULL
        ] );
        
        
                    
        Post::create( [
        'id'=>3885,
        'cat_id'=>9,
        'tag_id'=>4,
        'title'=>'Evaluation and management of blunt traumatic aortic injury: A practice management guideline from the Eastern Association for the Surgery of Trauma',
        'text'=>'Fox Nicole and others\r\nJournal of Trauma and Acute Care Surgery, 781136-146: Jan. 2015',
        'img'=>'file-1463591681-AGRM7H.jpg',
        'file'=>NULL,
        'link'=>'http://journals.lww.com/jtrauma/Fulltext/2015/01000/Evaluation_and_management_of_blunt_traumatic.19.aspx',
        'by'=>0,
        'journal'=>'Site Admin',
        'type'=>0,
        'month_article'=>0,
        'famous'=>0,
        'views'=>1596,
        'show'=>0,
        'date'=>'2014-12-31 21:18:18',
        'active'=>'1',
        'user'=>0,
        'created_at'=>'2020-05-15 01:20:06',
        'updated_at'=>'2020-06-21 00:08:23',
        'deleted_at'=>NULL
        ] );
        
        
                    
        Post::create( [
        'id'=>3886,
        'cat_id'=>8,
        'tag_id'=>11,
        'title'=>'A meta-analysis to derive literature-based benchmarks for readmission and hospital mortality after patient discharge from intensive care',
        'text'=>'Hosein F, Roberts DJ, Turin T, Zygun D, Ghali WA, Stelfox HT \r\nCritical Care 2014, 18 :2558 31 December 2014',
        'img'=>'file-1463591681-AGRM7H.jpg',
        'file'=>NULL,
        'link'=>'http://ccforum.com/content/18/6/2558/abstract',
        'by'=>0,
        'journal'=>'Site Admin',
        'type'=>0,
        'month_article'=>0,
        'famous'=>0,
        'views'=>1499,
        'show'=>0,
        'date'=>'2014-12-31 21:40:45',
        'active'=>'1',
        'user'=>0,
        'created_at'=>'2020-05-15 01:20:06',
        'updated_at'=>'2020-06-21 00:08:23',
        'deleted_at'=>NULL
        ] );
        
        
                    
        Post::create( [
        'id'=>3887,
        'cat_id'=>8,
        'tag_id'=>1,
        'title'=>'Ebola Lessons for Critical Care',
        'text'=>'Munro CL, Savel RH 2015 Viral outbreaks in an age of global citizenship. American Journal of Critical Care, 241: 4-6.',
        'img'=>'file-1463591725-9J88SF.jpg',
        'file'=>NULL,
        'link'=>'http://healthmanagement.org/c/icu/news/ebola-lessons-for-critical-care?utm_campaign=Content&utm_medium=Email&utm_source=Newsletter',
        'by'=>0,
        'journal'=>'Site Admin',
        'type'=>0,
        'month_article'=>0,
        'famous'=>0,
        'views'=>1528,
        'show'=>0,
        'date'=>'2014-12-31 21:42:58',
        'active'=>'1',
        'user'=>0,
        'created_at'=>'2020-05-15 01:20:06',
        'updated_at'=>'2020-06-21 00:08:23',
        'deleted_at'=>NULL
        ] );
        
        
                    
        Post::create( [
        'id'=>3888,
        'cat_id'=>8,
        'tag_id'=>7,
        'title'=>'Passive leg raising: five rules, not a drop of fluid!',
        'text'=>'Monnet X, Teboul J\r\nCritical Care 2015, 19 :18 14 January 2015',
        'img'=>'file-1463591725-9J88SF.jpg',
        'file'=>NULL,
        'link'=>'http://link.springer.com/article/10.1186/s13054-014-0708-5',
        'by'=>0,
        'journal'=>'Site Admin',
        'type'=>0,
        'month_article'=>0,
        'famous'=>0,
        'views'=>1499,
        'show'=>0,
        'date'=>'2015-01-14 18:46:57',
        'active'=>'1',
        'user'=>0,
        'created_at'=>'2020-05-15 01:20:06',
        'updated_at'=>'2020-06-21 00:08:23',
        'deleted_at'=>NULL
        ] );
        
        
                    
        Post::create( [
        'id'=>3889,
        'cat_id'=>8,
        'tag_id'=>7,
        'title'=>'Preoperative intra aortic balloon pump to reduce mortality in coronary artery bypass graft: a meta-analysis of randomized controlled trials',
        'text'=>'Zangrillo A, Pappalardo F, Dossi R, Di Prima A, Sassone M, Greco T, Monaco F, Musu M, Finco G, Landoni G\r\nCritical Care 2015, 19 :10 14 January 2015',
        'img'=>'file-1463591732-SM39NY.jpg',
        'file'=>NULL,
        'link'=>'http://ccforum.com/content/19/1/10/abstract',
        'by'=>0,
        'journal'=>'Site Admin',
        'type'=>0,
        'month_article'=>0,
        'famous'=>0,
        'views'=>1499,
        'show'=>0,
        'date'=>'2015-01-14 18:52:00',
        'active'=>'1',
        'user'=>0,
        'created_at'=>'2020-05-15 01:20:06',
        'updated_at'=>'2020-06-21 00:08:23',
        'deleted_at'=>NULL
        ] );
        
        
                    
        Post::create( [
        'id'=>3890,
        'cat_id'=>12,
        'tag_id'=>6,
        'title'=>'Mechanical ventilation during extracorporeal membrane oxygenation',
        'text'=>'Matthieu Schmidt and others\r\nCritical Care 2014, 18:203 21 January 2014',
        'img'=>'file-1463591685-DDLMK9.jpg',
        'file'=>NULL,
        'link'=>'http://ccforum.com/content/18/1/203/abstract',
        'by'=>0,
        'journal'=>'Site Admin',
        'type'=>0,
        'month_article'=>0,
        'famous'=>1,
        'views'=>1705,
        'show'=>0,
        'date'=>'2014-01-21 18:56:28',
        'active'=>'1',
        'user'=>0,
        'created_at'=>'2020-05-15 01:20:06',
        'updated_at'=>'2020-06-21 00:08:23',
        'deleted_at'=>NULL
        ] );
        
        
                    
        Post::create( [
        'id'=>3891,
        'cat_id'=>8,
        'tag_id'=>6,
        'title'=>'Adjusting tidal volume to stress index in an open lung condition optimizes ventilation and prevents overdistension in an experimental model of lung injury and reduced chest wall compliance',
        'text'=>'errando C, SuÃ¡rez-Sipmann F, Gutierrez A, Tusman G, Carbonell J, GarcÃ­a M, Piqueras L, CompaÃ± D et al.\r\nCritical Care 2015, 19:9 13 January 2015',
        'img'=>'file-1463591746-GEE3E7.jpg',
        'file'=>NULL,
        'link'=>'http://ccforum.com/content/19/1/9/abstract',
        'by'=>0,
        'journal'=>'Site Admin',
        'type'=>0,
        'month_article'=>0,
        'famous'=>0,
        'views'=>1499,
        'show'=>0,
        'date'=>'2015-01-13 18:59:14',
        'active'=>'1',
        'user'=>0,
        'created_at'=>'2020-05-15 01:20:06',
        'updated_at'=>'2020-06-21 00:08:23',
        'deleted_at'=>NULL
        ] );
        
        
                    
        Post::create( [
        'id'=>3892,
        'cat_id'=>8,
        'tag_id'=>6,
        'title'=>'Evaluation of lung recruitment maneuvers in acute respiratory distress syndrome using computer simulation',
        'text'=>'Das A, Cole O, Chikhani M, Wang W, Ali T, Haque M, Bates DG and Hardman JG\r\nCritical Care 2015, 19:8 12 January 2015',
        'img'=>'file-1463591752-XK911T.jpg',
        'file'=>NULL,
        'link'=>'http://ccforum.com/content/19/1/8/abstract',
        'by'=>0,
        'journal'=>'Site Admin',
        'type'=>0,
        'month_article'=>0,
        'famous'=>0,
        'views'=>1499,
        'show'=>0,
        'date'=>'2015-01-12 19:00:58',
        'active'=>'1',
        'user'=>0,
        'created_at'=>'2020-05-15 01:20:06',
        'updated_at'=>'2020-06-21 00:08:23',
        'deleted_at'=>NULL
        ] );
        
        
                    
        Post::create( [
        'id'=>3893,
        'cat_id'=>8,
        'tag_id'=>6,
        'title'=>'Preload dependence indices to titrate volume expansion during septic shock: a randomized controlled trial',
        'text'=>'Richard JC, Bayle F, Bourdin G, Leray V, Debord S, Delannoy B, Stoian AC, Wallet F et al.\r\nCritical Care 2015, 19:5 8 January 2015',
        'img'=>'file-1463591732-SM39NY.jpg',
        'file'=>NULL,
        'link'=>'http://ccforum.com/content/19/1/5/abstract',
        'by'=>0,
        'journal'=>'Site Admin',
        'type'=>0,
        'month_article'=>0,
        'famous'=>0,
        'views'=>1499,
        'show'=>0,
        'date'=>'2015-01-08 19:03:31',
        'active'=>'1',
        'user'=>0,
        'created_at'=>'2020-05-15 01:20:06',
        'updated_at'=>'2020-06-21 00:08:23',
        'deleted_at'=>NULL
        ] );
        
        
                    
        Post::create( [
        'id'=>3894,
        'cat_id'=>8,
        'tag_id'=>6,
        'title'=>'Ventilator-associated pneumonia in the ICU',
        'text'=>'Kalanuria AA, Zai W and Mirski M\r\nCritical Care 2014, 18:208 18 March 2014',
        'img'=>'file-1463591639-4YG8JY.jpg',
        'file'=>NULL,
        'link'=>'http://ccforum.com/content/18/2/208?utm_campaign=BMC13438N&utm_medium=BMCemail&utm_source=Teradata',
        'by'=>0,
        'journal'=>'Site Admin',
        'type'=>0,
        'month_article'=>0,
        'famous'=>1,
        'views'=>1594,
        'show'=>0,
        'date'=>'2014-03-18 19:10:30',
        'active'=>'1',
        'user'=>0,
        'created_at'=>'2020-05-15 01:20:06',
        'updated_at'=>'2020-06-21 00:08:23',
        'deleted_at'=>NULL
        ] );
        
        
                    
        Post::create( [
        'id'=>3895,
        'cat_id'=>9,
        'tag_id'=>7,
        'title'=>'Guidelines for the diagnosis, prevention and management of implantable cardiac electronic device infection. Report of a joint Working Party project on behalf of the British Society for Antimicrobial Chemotherapy BSAC, host organization, British Heart Rhyt',
        'text'=>'Jonathan A. T. Sandoe, and others\r\nJ. Antimicrob. Chemother. 2015 70: 325-359',
        'img'=>'file-1463591644-ZGN2DA.jpg',
        'file'=>NULL,
        'link'=>'http://jac.oxfordjournals.org/content/70/2/325.full?etoc',
        'by'=>0,
        'journal'=>'Site Admin',
        'type'=>0,
        'month_article'=>0,
        'famous'=>0,
        'views'=>1499,
        'show'=>0,
        'date'=>'2015-01-15 19:19:41',
        'active'=>'1',
        'user'=>0,
        'created_at'=>'2020-05-15 01:20:06',
        'updated_at'=>'2020-06-21 00:08:23',
        'deleted_at'=>NULL
        ] );
        
        
                    
        Post::create( [
        'id'=>3896,
        'cat_id'=>12,
        'tag_id'=>1,
        'title'=>'Bacteriostatic versus bactericidal antibiotics for patients with serious bacterial infections: systematic review and meta-analysis',
        'text'=>'Johannes Nemeth, and others\r\nJ. Antimicrob. Chemother. 2015 70: 382-395',
        'img'=>'file-1463591732-SM39NY.jpg',
        'file'=>NULL,
        'link'=>'http://jac.oxfordjournals.org/content/70/2/382.abstract?etoc',
        'by'=>0,
        'journal'=>'Site Admin',
        'type'=>0,
        'month_article'=>0,
        'famous'=>0,
        'views'=>1639,
        'show'=>0,
        'date'=>'2015-01-15 17:36:22',
        'active'=>'1',
        'user'=>0,
        'created_at'=>'2020-05-15 01:20:06',
        'updated_at'=>'2020-06-21 00:08:23',
        'deleted_at'=>NULL
        ] );
        
        
                    
        Post::create( [
        'id'=>3897,
        'cat_id'=>8,
        'tag_id'=>1,
        'title'=>'Are interstitial fluid concentrations of meropenem equivalent to plasma concentrations in critically ill patients receiving continuous renal replacement therapy',
        'text'=>'Julie M. Varghese, Paul Jarrett, Steven C. Wallis, Robert J. Boots, Carl M. J. Kirkpatrick, Jeffrey Lipman, and Jason A. Roberts\r\nJ. Antimicrob. Chemother. 2015 70: 528-533',
        'img'=>'file-1463591738-IZ3XFW.jpg',
        'file'=>NULL,
        'link'=>'http://jac.oxfordjournals.org/content/70/2/528.abstract?etoc',
        'by'=>0,
        'journal'=>'Site Admin',
        'type'=>0,
        'month_article'=>0,
        'famous'=>0,
        'views'=>1528,
        'show'=>0,
        'date'=>'2015-01-15 19:25:08',
        'active'=>'1',
        'user'=>0,
        'created_at'=>'2020-05-15 01:20:06',
        'updated_at'=>'2020-06-21 00:08:23',
        'deleted_at'=>NULL
        ] );
        
        
                    
        Post::create( [
        'id'=>3898,
        'cat_id'=>8,
        'tag_id'=>1,
        'title'=>'Oral versus parenteral antimicrobials for the treatment of cellulitis: a randomized non-inferiority trial',
        'text'=>'Craig A. Aboltins, Anastasia F. Hutchinson, Rabindra N. Sinnappu, Damian Cresp, Chrissie Risteski, Rajasutharsan Kathirgamanathan, Mark A. Tacey, Herman Chiu, and Kwang Lim\r\nJ. Antimicrob. Chemother. 2015 70: 581-586',
        'img'=>'file-1463591746-GEE3E7.jpg',
        'file'=>NULL,
        'link'=>'http://jac.oxfordjournals.org/content/70/2/581.abstract?etoc',
        'by'=>0,
        'journal'=>'Site Admin',
        'type'=>0,
        'month_article'=>0,
        'famous'=>0,
        'views'=>1528,
        'show'=>0,
        'date'=>'2015-01-01 19:27:06',
        'active'=>'1',
        'user'=>0,
        'created_at'=>'2020-05-15 01:20:06',
        'updated_at'=>'2020-06-21 00:08:23',
        'deleted_at'=>NULL
        ] );
        
        
                    
        Post::create( [
        'id'=>3899,
        'cat_id'=>12,
        'tag_id'=>6,
        'title'=>'Reintubation in critically ill patients: procedural complications and implications for care',
        'text'=>'Elmer J, Lee S, Rittenberger JC, Dargin J, Winger D, Emlet L \r\nCritical Care 2015, 19 :12 16 January 2015',
        'img'=>'file-1463591681-AGRM7H.jpg',
        'file'=>NULL,
        'link'=>'http://ccforum.com/content/19/1/12/abstract',
        'by'=>0,
        'journal'=>'Site Admin',
        'type'=>0,
        'month_article'=>0,
        'famous'=>0,
        'views'=>1610,
        'show'=>0,
        'date'=>'2015-01-16 15:36:31',
        'active'=>'1',
        'user'=>0,
        'created_at'=>'2020-05-15 01:20:06',
        'updated_at'=>'2020-06-21 00:08:23',
        'deleted_at'=>NULL
        ] );
        
        
                    
        Post::create( [
        'id'=>3900,
        'cat_id'=>8,
        'tag_id'=>5,
        'title'=>'Efficacy and safety of quetiapine in critically ill patients with delirium: a prospective, multicenter, randomized, double-blind, placebo-controlled pilot study',
        'text'=>'Devlin JW1, Roberts RJ, Fong JJ, Skrobik Y, Riker RR, Hill NS, Robbins T, Garpestad E.\r\nCrit Care Med. 2010 Feb382:419-27',
        'img'=>'file-1463591650-J14MMU.jpg',
        'file'=>NULL,
        'link'=>'http://www.ncbi.nlm.nih.gov/pubmed/19915454',
        'by'=>0,
        'journal'=>'Site Admin',
        'type'=>0,
        'month_article'=>0,
        'famous'=>1,
        'views'=>1607,
        'show'=>0,
        'date'=>'2010-02-06 05:42:58',
        'active'=>'1',
        'user'=>0,
        'created_at'=>'2020-05-15 01:20:06',
        'updated_at'=>'2020-06-21 00:08:23',
        'deleted_at'=>NULL
        ] );
        
        
                    
        Post::create( [
        'id'=>3901,
        'cat_id'=>8,
        'tag_id'=>5,
        'title'=>'Clearing up the confusion: The results of two pilot studies of antipsychotics for ICU delirium',
        'text'=>'Zaher Qassem and Eric B Milbrandt\r\nCritical Care 2010, 14:316',
        'img'=>'file-1463591685-DDLMK9.jpg',
        'file'=>NULL,
        'link'=>'http://ccforum.com/content/14/4/316',
        'by'=>0,
        'journal'=>'Site Admin',
        'type'=>0,
        'month_article'=>0,
        'famous'=>0,
        'views'=>1512,
        'show'=>0,
        'date'=>'2010-08-11 05:44:26',
        'active'=>'1',
        'user'=>0,
        'created_at'=>'2020-05-15 01:20:06',
        'updated_at'=>'2020-06-21 00:08:23',
        'deleted_at'=>NULL
        ] );
        
        
                    
        Post::create( [
        'id'=>3902,
        'cat_id'=>8,
        'tag_id'=>1,
        'title'=>'Are Anesthesiologists Finally Recognizing the Importance of Infection Control',
        'text'=>'Infectious Disease Special Edition\r\nJanuary 2105',
        'img'=>'file-1463591746-GEE3E7.jpg',
        'file'=>NULL,
        'link'=>'http://www.idse.net//ViewArticle.aspx?ses=ogst&d=Hepatitis&d_id=213&i=January+2015&i_id=1144&a_id=29256',
        'by'=>0,
        'journal'=>'Site Admin',
        'type'=>0,
        'month_article'=>0,
        'famous'=>0,
        'views'=>1528,
        'show'=>0,
        'date'=>'2015-01-22 17:45:36',
        'active'=>'1',
        'user'=>0,
        'created_at'=>'2020-05-15 01:20:06',
        'updated_at'=>'2020-06-21 00:08:23',
        'deleted_at'=>NULL
        ] );
        
        
                    
        Post::create( [
        'id'=>3903,
        'cat_id'=>8,
        'tag_id'=>1,
        'title'=>'CDC: Some Hospital Super Bugs Losing Their Power',
        'text'=>'Infectious Disease Special Edition\r\nJanuary 2105',
        'img'=>'file-1463591644-ZGN2DA.jpg',
        'file'=>NULL,
        'link'=>'http://www.idse.net//ViewArticle.aspx?ses=ogst&d=Public+Health&d_id=212&i=January+2015&i_id=1144&a_id=29254',
        'by'=>0,
        'journal'=>'Site Admin',
        'type'=>0,
        'month_article'=>0,
        'famous'=>0,
        'views'=>1528,
        'show'=>0,
        'date'=>'2015-01-22 17:47:17',
        'active'=>'1',
        'user'=>0,
        'created_at'=>'2020-05-15 01:20:06',
        'updated_at'=>'2020-06-21 00:08:23',
        'deleted_at'=>NULL
        ] );
        
        
                    
        Post::create( [
        'id'=>3904,
        'cat_id'=>12,
        'tag_id'=>1,
        'title'=>'Chlorhexidine Bathing and Health Careâ€“Associated Infections',
        'text'=>'Michael J. Noto and others\r\nJAMA. Published online January 20, 2015',
        'img'=>'file-1463591738-IZ3XFW.jpg',
        'file'=>NULL,
        'link'=>'http://jama.jamanetwork.com/article.aspx?articleid=2091544',
        'by'=>0,
        'journal'=>'Site Admin',
        'type'=>0,
        'month_article'=>0,
        'famous'=>0,
        'views'=>1639,
        'show'=>0,
        'date'=>'2015-01-22 17:57:06',
        'active'=>'1',
        'user'=>0,
        'created_at'=>'2020-05-15 01:20:06',
        'updated_at'=>'2020-06-21 00:08:23',
        'deleted_at'=>NULL
        ] );
        
        
                    
        Post::create( [
        'id'=>3905,
        'cat_id'=>8,
        'tag_id'=>11,
        'title'=>'The Impact of Hospital and ICU Organizational Factors on Outcome in Critically Ill Patients: Results From the Extended Prevalence of Infection in Intensive Care Study',
        'text'=>'Sakr, Yasser MD, PhD Moreira, Cora L. MD Rhodes, Andrew MBBS Ferguson, Niall D. MD Kleinpell, Ruth PhD, RN, FCCM Pickkers, Peter MD Kuiper, Michael A. MD, PhD, FCCM Lipman, Jeffrey MD Vincent, Jean-Louis MD, PhD, FCCM on behalf of the Extended Prevalence of Infection in Intensive Care Study Investigators\r\nCritical Care Medicine',
        'img'=>'file-1463591650-J14MMU.jpg',
        'file'=>NULL,
        'link'=>'http://journals.lww.com/ccmjournal/Abstract/publishahead/The_Impact_of_Hospital_and_ICU_Organizational.97396.aspx',
        'by'=>0,
        'journal'=>'Site Admin',
        'type'=>0,
        'month_article'=>0,
        'famous'=>0,
        'views'=>1499,
        'show'=>0,
        'date'=>'2015-01-01 18:03:14',
        'active'=>'1',
        'user'=>0,
        'created_at'=>'2020-05-15 01:20:06',
        'updated_at'=>'2020-06-21 00:08:23',
        'deleted_at'=>NULL
        ] );
        
        
                    
        Post::create( [
        'id'=>3906,
        'cat_id'=>8,
        'tag_id'=>10,
        'title'=>'Retrospective analysis of the incidence of epidural haematoma in patients with epidural catheters and abnormal coagulation parameters',
        'text'=>'P. Gulur, B. Tsui, R. Pathak, K. M. Koury, and H. Lee\r\nBr. J. Anaesth. published 22 January 2015',
        'img'=>'file-1463591650-J14MMU.jpg',
        'file'=>NULL,
        'link'=>'http://bja.oxfordjournals.org/content/early/2015/01/22/bja.aeu461.abstract?papetoc',
        'by'=>0,
        'journal'=>'Site Admin',
        'type'=>0,
        'month_article'=>0,
        'famous'=>0,
        'views'=>1499,
        'show'=>0,
        'date'=>'2015-01-22 10:01:25',
        'active'=>'1',
        'user'=>0,
        'created_at'=>'2020-05-15 01:20:06',
        'updated_at'=>'2020-06-21 00:08:23',
        'deleted_at'=>NULL
        ] );
        
        
                    
        Post::create( [
        'id'=>3907,
        'cat_id'=>8,
        'tag_id'=>4,
        'title'=>'Effect of high-dose dexamethasone on perioperative lactate levels and glucose control: a randomized controlled trial',
        'text'=>'Thomas H Ottens, Maarten Nijsten, Jan Hofland, Jan M Dieleman, Miriam Hoekstra, Diederik van Dijk, Joost van der Maaten\r\nCritical Care 2015, 19:41 13 February 2015',
        'img'=>'file-1463591738-IZ3XFW.jpg',
        'file'=>NULL,
        'link'=>'http://ccforum.com/content/19/1/41/abstract',
        'by'=>0,
        'journal'=>'Site Admin',
        'type'=>0,
        'month_article'=>0,
        'famous'=>0,
        'views'=>1596,
        'show'=>0,
        'date'=>'2015-02-13 09:03:40',
        'active'=>'1',
        'user'=>0,
        'created_at'=>'2020-05-15 01:20:06',
        'updated_at'=>'2020-06-21 00:08:23',
        'deleted_at'=>NULL
        ] );
        
        
                    
        Post::create( [
        'id'=>3908,
        'cat_id'=>8,
        'tag_id'=>6,
        'title'=>'Impact of imipenem and amikacin pharmacokinetic/pharmacodynamic parameters on microbiological outcome of Gram-negative bacilli ventilator-associated pneumonia',
        'text'=>'O. Pajot, C. Burdet, C. Couffignal, L. Massias, L. Armand-Lefevre, A. Foucrier, D. Da Silva, S. Lasocki, C. LaouÃ©nan, H. Mentec, F. MentrÃ©, and M. Wolff\r\nJ. Antimicrob. Chemother. published 27 January 2015',
        'img'=>'file-1463591650-J14MMU.jpg',
        'file'=>NULL,
        'link'=>'http://jac.oxfordjournals.org/content/early/2015/01/27/jac.dku569.abstract?papetoc',
        'by'=>0,
        'journal'=>'Site Admin',
        'type'=>0,
        'month_article'=>0,
        'famous'=>0,
        'views'=>1499,
        'show'=>0,
        'date'=>'2015-01-27 07:23:06',
        'active'=>'1',
        'user'=>0,
        'created_at'=>'2020-05-15 01:20:06',
        'updated_at'=>'2020-06-21 00:08:23',
        'deleted_at'=>NULL
        ] );
        
        
                    
        Post::create( [
        'id'=>3909,
        'cat_id'=>8,
        'tag_id'=>1,
        'title'=>'Comparative effectiveness of cefazolin versus cloxacillin as definitive antibiotic therapy for MSSA bacteraemia: results from a large multicentre cohort study',
        'text'=>'Anthony D. Bai and others\r\nJ. Antimicrob. Chemother. published 21 January 2015',
        'img'=>'file-1463591746-GEE3E7.jpg',
        'file'=>NULL,
        'link'=>'http://jac.oxfordjournals.org/content/early/2015/01/21/jac.dku560.abstract?papetoc',
        'by'=>0,
        'journal'=>'Site Admin',
        'type'=>0,
        'month_article'=>0,
        'famous'=>0,
        'views'=>1528,
        'show'=>0,
        'date'=>'2015-01-21 07:29:19',
        'active'=>'1',
        'user'=>0,
        'created_at'=>'2020-05-15 01:20:06',
        'updated_at'=>'2020-06-21 00:08:23',
        'deleted_at'=>NULL
        ] );
        
        
                    
        Post::create( [
        'id'=>3910,
        'cat_id'=>8,
        'tag_id'=>7,
        'title'=>'Inverted-Takotsubo cardiomyopathy: severe refractory heart failure in poly-trauma patients saved by emergency extracorporeal life support',
        'text'=>'Massimo Bonacchi, Andrea Vannini, Guy Harmelin, Stefano Batacchi, Marco Bugetti, Guido Sani, Adriano Peris\r\nInteract Cardiovasc Thorac Surg. 2014 Dec 21',
        'img'=>'file-1463591681-AGRM7H.jpg',
        'file'=>NULL,
        'link'=>'http://www.pubfacts.com/detail/25535176/Inverted-Takotsubo-cardiomyopathy:-severe-refractory-heart-failure-in-poly-trauma-patients-saved-by-',
        'by'=>0,
        'journal'=>'Site Admin',
        'type'=>0,
        'month_article'=>0,
        'famous'=>0,
        'views'=>1499,
        'show'=>0,
        'date'=>'2015-12-21 07:47:50',
        'active'=>'1',
        'user'=>0,
        'created_at'=>'2020-05-15 01:20:06',
        'updated_at'=>'2020-06-21 00:08:23',
        'deleted_at'=>NULL
        ] );
        
        
                    
        Post::create( [
        'id'=>3911,
        'cat_id'=>8,
        'tag_id'=>10,
        'title'=>'Red blood cell transfusion is a determinant of neurological complications after cardiac surgery',
        'text'=>'Giovanni Mariscalco,and others\r\nInteract CardioVasc Thorac Surg 2015 20 2: 166-171',
        'img'=>'file-1463591732-SM39NY.jpg',
        'file'=>NULL,
        'link'=>'http://icvts.oxfordjournals.org/content/20/2/166.abstract',
        'by'=>0,
        'journal'=>'Site Admin',
        'type'=>0,
        'month_article'=>0,
        'famous'=>0,
        'views'=>1499,
        'show'=>0,
        'date'=>'2015-02-01 07:49:53',
        'active'=>'1',
        'user'=>0,
        'created_at'=>'2020-05-15 01:20:06',
        'updated_at'=>'2020-06-21 00:08:23',
        'deleted_at'=>NULL
        ] );
        
        
                    
        Post::create( [
        'id'=>3912,
        'cat_id'=>8,
        'tag_id'=>6,
        'title'=>'Surgical embolectomy for intermediate-risk acute pulmonary embolism',
        'text'=>'SÃ©bastien Champion and Eric Braunberger\r\nInteract CardioVasc Thorac Surg 2015 20 2: 274-275',
        'img'=>'file-1463591657-K8BRDF.jpg',
        'file'=>NULL,
        'link'=>'http://icvts.oxfordjournals.org/content/20/2/274.full',
        'by'=>0,
        'journal'=>'Site Admin',
        'type'=>0,
        'month_article'=>0,
        'famous'=>0,
        'views'=>1499,
        'show'=>0,
        'date'=>'2015-02-01 07:53:03',
        'active'=>'1',
        'user'=>0,
        'created_at'=>'2020-05-15 01:20:06',
        'updated_at'=>'2020-06-21 00:08:23',
        'deleted_at'=>NULL
        ] );
        
        
                    
        Post::create( [
        'id'=>3913,
        'cat_id'=>8,
        'tag_id'=>7,
        'title'=>'Prophylactic treatment with coenzyme Q10 in patients undergoing cardiac surgery: could an antioxidant reduce complications A systematic review and meta-analysis',
        'text'=>'Fernando de Frutos, Alfredo Gea, Rafael Hernandez-Estefania, and Gregorio Rabago\r\nInteract CardioVasc Thorac Surg 2015 20 2: 254-259',
        'img'=>'file-1463591685-DDLMK9.jpg',
        'file'=>NULL,
        'link'=>'http://icvts.oxfordjournals.org/content/20/2/254.full',
        'by'=>0,
        'journal'=>'Site Admin',
        'type'=>0,
        'month_article'=>0,
        'famous'=>0,
        'views'=>1499,
        'show'=>0,
        'date'=>'2015-02-01 07:54:27',
        'active'=>'1',
        'user'=>0,
        'created_at'=>'2020-05-15 01:20:06',
        'updated_at'=>'2020-06-21 00:08:23',
        'deleted_at'=>NULL
        ] );
        
        
                    
        Post::create( [
        'id'=>3914,
        'cat_id'=>8,
        'tag_id'=>6,
        'title'=>'Fluid Management With a Simplified Conservative Protocol for the Acute Respiratory Distress Syndrome',
        'text'=>'pGrissom, Colin K and others Critical Care Medicine: February 2015 - Volume 43 - Issue 2 - p 288â€“295/p',
        'img'=>'file-1463591644-ZGN2DA.jpg',
        'file'=>NULL,
        'link'=>'18d3aaf1b6d7defc5261d7bf0696c440_test.php',
        'by'=>0,
        'journal'=>'Site Admin',
        'type'=>0,
        'month_article'=>0,
        'famous'=>0,
        'views'=>1499,
        'show'=>0,
        'date'=>'2015-02-01 19:16:16',
        'active'=>'1',
        'user'=>0,
        'created_at'=>'2020-05-15 01:20:06',
        'updated_at'=>'2020-06-21 00:08:23',
        'deleted_at'=>NULL
        ] );
        
        
                    
        Post::create( [
        'id'=>3915,
        'cat_id'=>8,
        'tag_id'=>6,
        'title'=>'Hypercapnia Without Acidemia, Compared To Hypercapnia With Acidemia, Is Associated With Reduced Mortality In ARDS: Results From The NHLBI ARDS Network Fluid And Catheter Treatment FACT Trial',
        'text'=>'Jeremy R. Beitler and others\r\nC55. ACUTE RESPIRATORY DISTRESS SYNDROME, 2013, pp. A6088',
        'img'=>'file-1463591752-XK911T.jpg',
        'file'=>NULL,
        'link'=>'http://www.atsjournals.org/doi/abs/10.1164/ajrccm-conference.2013.187.1_MeetingAbstracts.A6088?src=recsys',
        'by'=>0,
        'journal'=>'Site Admin',
        'type'=>0,
        'month_article'=>0,
        'famous'=>1,
        'views'=>1594,
        'show'=>0,
        'date'=>'2013-05-01 08:17:37',
        'active'=>'1',
        'user'=>0,
        'created_at'=>'2020-05-15 01:20:06',
        'updated_at'=>'2020-06-21 00:08:23',
        'deleted_at'=>NULL
        ] );
        
        
                    
        Post::create( [
        'id'=>3916,
        'cat_id'=>8,
        'tag_id'=>6,
        'title'=>'Development and Validation of Severe Hypoxemia Associated Risk Prediction Model in 1,000 Mechanically Ventilated Patients',
        'text'=>'Pannu, Sonal R and others\r\nCritical Care Medicine:\r\nFebruary 2015 - Volume 43 - Issue 2 - p 308â€“317',
        'img'=>'file-1463591732-SM39NY.jpg',
        'file'=>NULL,
        'link'=>'http://journals.lww.com/ccmjournal/Abstract/2015/02000/Development_and_Validation_of_Severe_Hypoxemia.7.aspx',
        'by'=>0,
        'journal'=>'Site Admin',
        'type'=>0,
        'month_article'=>0,
        'famous'=>0,
        'views'=>1499,
        'show'=>0,
        'date'=>'2015-02-01 08:19:52',
        'active'=>'1',
        'user'=>0,
        'created_at'=>'2020-05-15 01:20:06',
        'updated_at'=>'2020-06-21 00:08:23',
        'deleted_at'=>NULL
        ] );
        
        
                    
        Post::create( [
        'id'=>3917,
        'cat_id'=>8,
        'tag_id'=>7,
        'title'=>'Hemodynamics and Vasopressor Support During Targeted Temperature Management at 33Â°C Versus 36Â°C After Out-of-Hospital Cardiac Arrest: A Post Hoc Study of the Target Temperature',
        'text'=>'Bro-Jeppesen, John and others\r\nCritical Care Medicine:\r\nFebruary 2015 - Volume 43 - Issue 2 - p 318â€“327',
        'img'=>'file-1463591746-GEE3E7.jpg',
        'file'=>NULL,
        'link'=>'http://journals.lww.com/ccmjournal/Abstract/2015/02000/Hemodynamics_and_Vasopressor_Support_During.8.aspx',
        'by'=>0,
        'journal'=>'Site Admin',
        'type'=>0,
        'month_article'=>0,
        'famous'=>0,
        'views'=>1499,
        'show'=>0,
        'date'=>'2015-02-01 08:22:30',
        'active'=>'1',
        'user'=>0,
        'created_at'=>'2020-05-15 01:20:06',
        'updated_at'=>'2020-06-21 00:08:23',
        'deleted_at'=>NULL
        ] );
        
        
                    
        Post::create( [
        'id'=>3918,
        'cat_id'=>8,
        'tag_id'=>6,
        'title'=>'Coenrollment in a Randomized Trial of High-Frequency Oscillation: Prevalence, Patterns, Predictors, and Outcomes',
        'text'=>'Cook, Deborah J and others\r\nCritical Care Medicine:\r\nFebruary 2015 - Volume 43 - Issue 2 - p 328â€“338',
        'img'=>'file-1463591738-IZ3XFW.jpg',
        'file'=>NULL,
        'link'=>'http://journals.lww.com/ccmjournal/Abstract/2015/02000/Coenrollment_in_a_Randomized_Trial_of.9.aspx',
        'by'=>0,
        'journal'=>'Site Admin',
        'type'=>0,
        'month_article'=>0,
        'famous'=>0,
        'views'=>1499,
        'show'=>0,
        'date'=>'2015-02-01 08:24:24',
        'active'=>'1',
        'user'=>0,
        'created_at'=>'2020-05-15 01:20:06',
        'updated_at'=>'2020-06-21 00:08:23',
        'deleted_at'=>NULL
        ] );
        
        
                    
        Post::create( [
        'id'=>3919,
        'cat_id'=>8,
        'tag_id'=>6,
        'title'=>'A Clinical Classification of the Acute Respiratory Distress Syndrome for Predicting Outcome and Guiding Medical Therapy',
        'text'=>'Villar, JesÃºs and others\r\nCritical Care Medicine:\r\nFebruary 2015 - Volume 43 - Issue 2 - p 346â€“353',
        'img'=>'file-1463591644-ZGN2DA.jpg',
        'file'=>NULL,
        'link'=>'http://journals.lww.com/ccmjournal/Abstract/2015/02000/A_Clinical_Classification_of_the_Acute_Respiratory.11.aspx',
        'by'=>0,
        'journal'=>'Site Admin',
        'type'=>0,
        'month_article'=>0,
        'famous'=>0,
        'views'=>1499,
        'show'=>0,
        'date'=>'2015-02-01 08:26:43',
        'active'=>'1',
        'user'=>0,
        'created_at'=>'2020-05-15 01:20:06',
        'updated_at'=>'2020-06-21 00:08:23',
        'deleted_at'=>NULL
        ] );
        
        
                    
        Post::create( [
        'id'=>3920,
        'cat_id'=>8,
        'tag_id'=>9,
        'title'=>'Dysphagiaâ€”A Common, Transient Symptom in Critical Illness Polyneuropathy: A Fiberoptic Endoscopic Evaluation of Swallowing Study',
        'text'=>'Ponfick, Matthias and others\r\nCritical Care Medicine:\r\nFebruary 2015 - Volume 43 - Issue 2 - p 365â€“372',
        'img'=>'file-1463591752-XK911T.jpg',
        'file'=>NULL,
        'link'=>'http://journals.lww.com/ccmjournal/Abstract/2015/02000/Dysphagia_A_Common,_Transient_Symptom_in_Critical.13.aspx',
        'by'=>0,
        'journal'=>'Site Admin',
        'type'=>0,
        'month_article'=>0,
        'famous'=>0,
        'views'=>1499,
        'show'=>0,
        'date'=>'2015-02-01 08:29:50',
        'active'=>'1',
        'user'=>0,
        'created_at'=>'2020-05-15 01:20:06',
        'updated_at'=>'2020-06-21 00:08:23',
        'deleted_at'=>NULL
        ] );
        
        
                    
        Post::create( [
        'id'=>3921,
        'cat_id'=>8,
        'tag_id'=>6,
        'title'=>'Postoperative Pro-Adrenomedullin Levels Predict Mortality in Thoracic Surgery Patients: Comparison With Acute Physiology and Chronic Health Evaluation IV Score',
        'text'=>'Schoe, Abraham and others\r\nCritical Care Medicine:\r\nFebruary 2015 - Volume 43 - Issue 2 - p 373â€“381',
        'img'=>'file-1463591752-XK911T.jpg',
        'file'=>NULL,
        'link'=>'http://journals.lww.com/ccmjournal/Abstract/2015/02000/Postoperative_Pro_Adrenomedullin_Levels_Predict.14.aspx',
        'by'=>0,
        'journal'=>'Site Admin',
        'type'=>0,
        'month_article'=>0,
        'famous'=>0,
        'views'=>1499,
        'show'=>0,
        'date'=>'2015-02-01 08:31:49',
        'active'=>'1',
        'user'=>0,
        'created_at'=>'2020-05-15 01:20:06',
        'updated_at'=>'2020-06-21 00:08:23',
        'deleted_at'=>NULL
        ] );
        
        
                    
        Post::create( [
        'id'=>3922,
        'cat_id'=>8,
        'tag_id'=>6,
        'title'=>'Cytomegalovirus Seroprevalence as a Risk Factor for Poor Outcome in Acute Respiratory Distress Syndrome',
        'text'=>'Ong, David S. and others\r\nCritical Care Medicine:\r\nFebruary 2015 - Volume 43 - Issue 2 - p 394â€“400',
        'img'=>'file-1463591725-9J88SF.jpg',
        'file'=>NULL,
        'link'=>'http://journals.lww.com/ccmjournal/Abstract/2015/02000/Cytomegalovirus_Seroprevalence_as_a_Risk_Factor.16.aspx',
        'by'=>0,
        'journal'=>'Site Admin',
        'type'=>0,
        'month_article'=>0,
        'famous'=>0,
        'views'=>1499,
        'show'=>0,
        'date'=>'2015-02-01 08:34:28',
        'active'=>'1',
        'user'=>0,
        'created_at'=>'2020-05-15 01:20:06',
        'updated_at'=>'2020-06-21 00:08:23',
        'deleted_at'=>NULL
        ] );
        
        
                    
        Post::create( [
        'id'=>3923,
        'cat_id'=>8,
        'tag_id'=>7,
        'title'=>'Failure of Anticoagulant Thromboprophylaxis: Risk Factors in Medical-Surgical Critically Ill Patients',
        'text'=>'Lim, Wendy and others\r\nCritical Care Medicine:\r\nFebruary 2015 - Volume 43 - Issue 2 - p 401â€“410',
        'img'=>'file-1463591639-4YG8JY.jpg',
        'file'=>NULL,
        'link'=>'http://journals.lww.com/ccmjournal/Abstract/2015/02000/Failure_of_Anticoagulant_Thromboprophylaxis___Risk.17.aspx',
        'by'=>0,
        'journal'=>'Site Admin',
        'type'=>0,
        'month_article'=>0,
        'famous'=>0,
        'views'=>1499,
        'show'=>0,
        'date'=>'2015-02-01 08:36:27',
        'active'=>'1',
        'user'=>0,
        'created_at'=>'2020-05-15 01:20:06',
        'updated_at'=>'2020-06-21 00:08:23',
        'deleted_at'=>NULL
        ] );
        
        
                    
        Post::create( [
        'id'=>3924,
        'cat_id'=>8,
        'tag_id'=>5,
        'title'=>'Comparison of the Full Outline of UnResponsiveness Score and the Glasgow Coma Scale in Predicting Mortality in Critically Ill Patients',
        'text'=>'Wijdicks, Eelco F.M and others\r\nCritical Care Medicine:\r\nFebruary 2015 - Volume 43 - Issue 2 - p 439â€“444',
        'img'=>'file-1463591746-GEE3E7.jpg',
        'file'=>NULL,
        'link'=>'http://journals.lww.com/ccmjournal/Abstract/2015/02000/Comparison_of_the_Full_Outline_of_UnResponsiveness.21.aspx',
        'by'=>0,
        'journal'=>'Site Admin',
        'type'=>0,
        'month_article'=>0,
        'famous'=>1,
        'views'=>1607,
        'show'=>0,
        'date'=>'2015-02-01 08:45:28',
        'active'=>'1',
        'user'=>0,
        'created_at'=>'2020-05-15 01:20:06',
        'updated_at'=>'2020-06-21 00:08:23',
        'deleted_at'=>NULL
        ] );
        
        
                    
        Post::create( [
        'id'=>3925,
        'cat_id'=>8,
        'tag_id'=>5,
        'title'=>'Accuracy of Brain Multimodal Monitoring to Detect Cerebral Hypoperfusion After Traumatic Brain Injury',
        'text'=>'Bouzat, Pierre and others\r\nCritical Care Medicine:\r\nFebruary 2015 - Volume 43 - Issue 2 - p 445â€“452',
        'img'=>'file-1463591650-J14MMU.jpg',
        'file'=>NULL,
        'link'=>'http://journals.lww.com/ccmjournal/Abstract/2015/02000/Accuracy_of_Brain_Multimodal_Monitoring_to_Detect.22.aspx',
        'by'=>0,
        'journal'=>'Site Admin',
        'type'=>0,
        'month_article'=>0,
        'famous'=>0,
        'views'=>1512,
        'show'=>0,
        'date'=>'2015-02-01 08:47:13',
        'active'=>'1',
        'user'=>0,
        'created_at'=>'2020-05-15 01:20:06',
        'updated_at'=>'2020-06-21 00:08:23',
        'deleted_at'=>NULL
        ] );
        
        
                    
        Post::create( [
        'id'=>3926,
        'cat_id'=>8,
        'tag_id'=>8,
        'title'=>'Recommendations for the Role of Extracorporeal Treatments in the Management of Acute Methanol Poisoning: A Systematic Review and Consensus Statement',
        'text'=>'Roberts, Darren M and others\r\nCritical Care Medicine:\r\nFebruary 2015 - Volume 43 - Issue 2 - p 461â€“472',
        'img'=>'file-1463591681-AGRM7H.jpg',
        'file'=>NULL,
        'link'=>'http://journals.lww.com/ccmjournal/Abstract/2015/02000/Recommendations_for_the_Role_of_Extracorporeal.24.aspx',
        'by'=>0,
        'journal'=>'Site Admin',
        'type'=>0,
        'month_article'=>0,
        'famous'=>0,
        'views'=>1550,
        'show'=>0,
        'date'=>'2015-02-01 08:49:03',
        'active'=>'1',
        'user'=>0,
        'created_at'=>'2020-05-15 01:20:06',
        'updated_at'=>'2020-06-21 00:08:23',
        'deleted_at'=>NULL
        ] );
        
        
                    
        Post::create( [
        'id'=>3927,
        'cat_id'=>8,
        'tag_id'=>9,
        'title'=>'Diagnosis and Treatment of Clostridium difficile in Adults\r\nA Systematic Review',
        'text'=>'Bagdasarian N, Rao K, Malani PN.\r\nJAMA. 2015 Jan 273134:398-408',
        'img'=>'file-1463591725-9J88SF.jpg',
        'file'=>NULL,
        'link'=>'http://jama.jamanetwork.com/article.aspx?articleid=2091993',
        'by'=>0,
        'journal'=>'Site Admin',
        'type'=>0,
        'month_article'=>0,
        'famous'=>0,
        'views'=>1499,
        'show'=>0,
        'date'=>'2015-01-31 06:28:52',
        'active'=>'1',
        'user'=>0,
        'created_at'=>'2020-05-15 01:20:06',
        'updated_at'=>'2020-06-21 00:08:23',
        'deleted_at'=>NULL
        ] );
        
        
                    
        Post::create( [
        'id'=>3928,
        'cat_id'=>8,
        'tag_id'=>10,
        'title'=>'Venous Thromboembolism Prophylaxis in Critically Ill Patients',
        'text'=>'Boonyawat K, Crowther MA\r\nSemin Thromb Hemost. 2015 Jan 16',
        'img'=>'file-1463591650-J14MMU.jpg',
        'file'=>NULL,
        'link'=>'http://www.ntkinstitute.org/news/content.nsf/PaperFrameSet?OpenForm&pp=1&id=DF5950353CA4263E85256FDB00692E0A&refid=4504&specid=106&newsid=5319FA7B7748EC5685257DDD005BA54B&locref=ntkwatch&u=http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&db=PubMed&dopt=Abstract&list_uids=25594495',
        'by'=>0,
        'journal'=>'Site Admin',
        'type'=>0,
        'month_article'=>0,
        'famous'=>0,
        'views'=>1499,
        'show'=>0,
        'date'=>'2015-01-16 06:33:04',
        'active'=>'1',
        'user'=>0,
        'created_at'=>'2020-05-15 01:20:06',
        'updated_at'=>'2020-06-21 00:08:23',
        'deleted_at'=>NULL
        ] );
        
        
                    
        Post::create( [
        'id'=>3929,
        'cat_id'=>8,
        'tag_id'=>5,
        'title'=>'FDA Clears First Neck-Access Neuroprotection for Carotid Stenting',
        'text'=>'U.S. Food and Drug Administration\r\n10 February 2015',
        'img'=>'file-1463591650-J14MMU.jpg',
        'file'=>NULL,
        'link'=>'http://www.fda.gov/NewsEvents/Newsroom/PressAnnouncements/ucm433482.htm',
        'by'=>0,
        'journal'=>'Site Admin',
        'type'=>0,
        'month_article'=>0,
        'famous'=>1,
        'views'=>1607,
        'show'=>0,
        'date'=>'2015-02-10 08:07:42',
        'active'=>'1',
        'user'=>0,
        'created_at'=>'2020-05-15 01:20:06',
        'updated_at'=>'2020-06-21 00:08:23',
        'deleted_at'=>NULL
        ] );
        
        
                    
        Post::create( [
        'id'=>3930,
        'cat_id'=>8,
        'tag_id'=>7,
        'title'=>'Icatibant Quickly Curtails ACE-Inhibitor-Induced Angioedema',
        'text'=>'Jenni Laidman\r\nMedscape Medical News\r\nFebruary 05, 2015',
        'img'=>'file-1463591738-IZ3XFW.jpg',
        'file'=>NULL,
        'link'=>'http://www.medscape.com/viewarticle/839297?src=rss',
        'by'=>0,
        'journal'=>'Site Admin',
        'type'=>0,
        'month_article'=>0,
        'famous'=>0,
        'views'=>1499,
        'show'=>0,
        'date'=>'2015-02-05 08:51:03',
        'active'=>'1',
        'user'=>0,
        'created_at'=>'2020-05-15 01:20:06',
        'updated_at'=>'2020-06-21 00:08:23',
        'deleted_at'=>NULL
        ] );
        
        
                    
        Post::create( [
        'id'=>3931,
        'cat_id'=>8,
        'tag_id'=>5,
        'title'=>'Endovascular Treatment Shows Dramatic Benefits for Large Strokes in 3 Trials',
        'text'=>'Todd Neale \r\nDailynewsleaks.com\r\nWednesday, February 11, 2015',
        'img'=>'file-1463591685-DDLMK9.jpg',
        'file'=>NULL,
        'link'=>'http://www.tctmd.com/show.aspx?id=127973',
        'by'=>0,
        'journal'=>'Site Admin',
        'type'=>0,
        'month_article'=>0,
        'famous'=>0,
        'views'=>1512,
        'show'=>0,
        'date'=>'2015-02-11 08:30:16',
        'active'=>'1',
        'user'=>0,
        'created_at'=>'2020-05-15 01:20:06',
        'updated_at'=>'2020-06-21 00:08:23',
        'deleted_at'=>NULL
        ] );
        
        
                    
        Post::create( [
        'id'=>3932,
        'cat_id'=>12,
        'tag_id'=>5,
        'title'=>'ESCAPE: Endovascular Therapy Reduces Mortality in Ischemic Stroke',
        'text'=>'Mayank Goyal and others  for the ESCAPE Trial Investigators\r\nNew Eng. J Med. February 11, 2015',
        'img'=>'file-1463591657-K8BRDF.jpg',
        'file'=>NULL,
        'link'=>'http://www.nejm.org/doi/full/10.1056/NEJMoa1414905',
        'by'=>0,
        'journal'=>'Site Admin',
        'type'=>0,
        'month_article'=>0,
        'famous'=>1,
        'views'=>1718,
        'show'=>0,
        'date'=>'2015-02-11 16:12:41',
        'active'=>'1',
        'user'=>0,
        'created_at'=>'2020-05-15 01:20:06',
        'updated_at'=>'2020-06-21 00:08:23',
        'deleted_at'=>NULL
        ] );
        
        
                    
        Post::create( [
        'id'=>3933,
        'cat_id'=>8,
        'tag_id'=>10,
        'title'=>'Hemodynamic Resuscitation: What is PROPPR',
        'text'=>'Richard P. Dutton\r\nChief Quality Officer, American Society of Anesthesiologists\r\nExecutive Director, Anesthesia Quality Institute\r\nClinical Associate, University of Chicago Department of Anesthesia and Critical Care',
        'img'=>'file-1463591681-AGRM7H.jpg',
        'file'=>NULL,
        'link'=>'https://tas.ucsf.edu/doc/HemostaticResuscitationPROPPR.pdf',
        'by'=>0,
        'journal'=>'Site Admin',
        'type'=>0,
        'month_article'=>0,
        'famous'=>0,
        'views'=>1499,
        'show'=>0,
        'date'=>'2015-02-01 08:50:49',
        'active'=>'1',
        'user'=>0,
        'created_at'=>'2020-05-15 01:20:06',
        'updated_at'=>'2020-06-21 00:08:23',
        'deleted_at'=>NULL
        ] );
        
        
                    
        Post::create( [
        'id'=>3934,
        'cat_id'=>12,
        'tag_id'=>10,
        'title'=>'Transfusion of Plasma, Platelets, and Red Blood Cells in a 1:1:1 vs a 1:1:2 Ratio and Mortality in Patients With Severe Trauma: The PROPPR Randomized Clinical Trial',
        'text'=>'John B. Holcomb and others for the PROPPR Study Group\r\nJAMA. 2015 3135:471-482',
        'img'=>'file-1463591639-4YG8JY.jpg',
        'file'=>NULL,
        'link'=>'http://jama.jamanetwork.com/article.aspx?articleid=2107789&resultClick=3',
        'by'=>0,
        'journal'=>'Site Admin',
        'type'=>0,
        'month_article'=>0,
        'famous'=>0,
        'views'=>1610,
        'show'=>0,
        'date'=>'2015-02-03 08:54:40',
        'active'=>'1',
        'user'=>0,
        'created_at'=>'2020-05-15 01:20:06',
        'updated_at'=>'2020-06-21 00:08:23',
        'deleted_at'=>NULL
        ] );
        
        
                    
        Post::create( [
        'id'=>3935,
        'cat_id'=>12,
        'tag_id'=>6,
        'title'=>'Simplified lung ultrasound protocol shows excellent prediction of extravascular lung water in ventilated intensive care patients',
        'text'=>'Philipp Enghard and others\r\nCritical Care 2015, 19:36 6 February 2015',
        'img'=>'file-1463591738-IZ3XFW.jpg',
        'file'=>NULL,
        'link'=>'http://ccforum.com/content/19/1/36/abstract',
        'by'=>0,
        'journal'=>'Site Admin',
        'type'=>0,
        'month_article'=>0,
        'famous'=>0,
        'views'=>1610,
        'show'=>0,
        'date'=>'2015-02-06 09:06:56',
        'active'=>'1',
        'user'=>0,
        'created_at'=>'2020-05-15 01:20:06',
        'updated_at'=>'2020-06-21 00:08:23',
        'deleted_at'=>NULL
        ] );
        
        
                    
        Post::create( [
        'id'=>3936,
        'cat_id'=>8,
        'tag_id'=>1,
        'title'=>'Pharmacokinetic variability and exposures of fluconazole, anidulafungin and caspofungin in intensive care unit patients: Data from multinational Defining Antibiotic Levels in Intensive care unit DALI patients Study',
        'text'=>'Mahipal G Sinnollaredd and others, on behalf of the DALI Study Authors\r\nCritical Care 2015, 19:33 4 February 2015',
        'img'=>'file-1463591738-IZ3XFW.jpg',
        'file'=>NULL,
        'link'=>'http://ccforum.com/content/19/1/33/abstract',
        'by'=>0,
        'journal'=>'Site Admin',
        'type'=>0,
        'month_article'=>0,
        'famous'=>0,
        'views'=>1528,
        'show'=>0,
        'date'=>'2015-02-04 09:08:42',
        'active'=>'1',
        'user'=>0,
        'created_at'=>'2020-05-15 01:20:06',
        'updated_at'=>'2020-06-21 00:08:23',
        'deleted_at'=>NULL
        ] );
        
        
                    
        Post::create( [
        'id'=>3937,
        'cat_id'=>8,
        'tag_id'=>1,
        'title'=>'Efficiency of hydrogen peroxide in improving disinfection of ICU rooms',
        'text'=>'Caroline Blazejewski, FrÃ©dÃ©ric Wallet, Anahita RouzÃ©, RÃ©mi Le Guern, Sylvie Ponthieux, Julia Salleron, Saad Nseir\r\nCritical Care 2015, 19:30 2 February 2015',
        'img'=>'file-1463591752-XK911T.jpg',
        'file'=>NULL,
        'link'=>'Caroline Blazejewski, FrÃ©dÃ©ric Wallet, Anahita RouzÃ©, RÃ©mi Le Guern, Sylvie Ponthieux, Julia Salleron, Saad Nseir Critical Care 2015, 19:30 (2 February 2015)',
        'by'=>0,
        'journal'=>'Site Admin',
        'type'=>0,
        'month_article'=>0,
        'famous'=>0,
        'views'=>1528,
        'show'=>0,
        'date'=>'2015-02-02 09:10:51',
        'active'=>'1',
        'user'=>0,
        'created_at'=>'2020-05-15 01:20:06',
        'updated_at'=>'2020-06-21 00:08:23',
        'deleted_at'=>NULL
        ] );
        
        
                    
        Post::create( [
        'id'=>3938,
        'cat_id'=>8,
        'tag_id'=>7,
        'title'=>'Low-dose hydrocortisone reduces norepinephrine duration in severe burn patients: a randomized clinical trial',
        'text'=>'Fabienne Venet, Jonathan Plassais, Julien Textoris, Marie-AngÃ©lique Cazalis, Alexandre Pachot, Marc Bertin-Maghit, Christophe Magnin, Thomas RimmelÃ©, Guillaume Monneret, Sylvie Tissot\r\nCritical Care 2015, 19:21 26 January 2015',
        'img'=>'file-1463591657-K8BRDF.jpg',
        'file'=>NULL,
        'link'=>'http://ccforum.com/content/19/1/21/abstract',
        'by'=>0,
        'journal'=>'Site Admin',
        'type'=>0,
        'month_article'=>0,
        'famous'=>0,
        'views'=>1499,
        'show'=>0,
        'date'=>'2015-01-26 09:13:15',
        'active'=>'1',
        'user'=>0,
        'created_at'=>'2020-05-15 01:20:06',
        'updated_at'=>'2020-06-21 00:08:23',
        'deleted_at'=>NULL
        ] );
        
        
                    
        Post::create( [
        'id'=>3939,
        'cat_id'=>8,
        'tag_id'=>1,
        'title'=>'Antifungal step-down therapy based on hospital intravenous to oral switch policy and susceptibility testing in adult patients with candidaemia: a single centre experience',
        'text'=>'Bal AM1, Shankland GS, Scott G, Imtiaz T, Macaulay R, McGill M.\r\nInt J Clin Pract. 2014 Jan681:20-7',
        'img'=>'file-1463591725-9J88SF.jpg',
        'file'=>NULL,
        'link'=>'http://www.ncbi.nlm.nih.gov/pubmed/24341299',
        'by'=>0,
        'journal'=>'Site Admin',
        'type'=>0,
        'month_article'=>0,
        'famous'=>0,
        'views'=>1528,
        'show'=>0,
        'date'=>'2014-01-01 16:36:41',
        'active'=>'1',
        'user'=>0,
        'created_at'=>'2020-05-15 01:20:06',
        'updated_at'=>'2020-06-21 00:08:23',
        'deleted_at'=>NULL
        ] );
        
        
                    
        Post::create( [
        'id'=>3940,
        'cat_id'=>8,
        'tag_id'=>6,
        'title'=>'Driving Pressure and Survival in the Acute Respiratory Distress Syndrome',
        'text'=>'M.B.P. Amato and Others\r\nN Engl J Med 2015372:747-755',
        'img'=>'file-1463591725-9J88SF.jpg',
        'file'=>NULL,
        'link'=>'http://www.nejm.org/doi/full/10.1056/NEJMsa1410639?query=TOC',
        'by'=>0,
        'journal'=>'Site Admin',
        'type'=>0,
        'month_article'=>0,
        'famous'=>0,
        'views'=>1499,
        'show'=>0,
        'date'=>'2015-02-18 12:47:53',
        'active'=>'1',
        'user'=>0,
        'created_at'=>'2020-05-15 01:20:06',
        'updated_at'=>'2020-06-21 00:08:23',
        'deleted_at'=>NULL
        ] );
        
        
                    
        Post::create( [
        'id'=>3941,
        'cat_id'=>8,
        'tag_id'=>6,
        'title'=>'2014 MERS-CoV Outbreak in Jeddah â€” A Link to Health Care Facilities',
        'text'=>'I.K. Oboho and Others\r\nN Engl J Med 2015372:846-854',
        'img'=>'file-1463591738-IZ3XFW.jpg',
        'file'=>NULL,
        'link'=>'http://www.nejm.org/doi/full/10.1056/NEJMoa1408636?query=TOC',
        'by'=>0,
        'journal'=>'Site Admin',
        'type'=>0,
        'month_article'=>0,
        'famous'=>0,
        'views'=>1499,
        'show'=>0,
        'date'=>'2015-02-25 04:30:35',
        'active'=>'1',
        'user'=>0,
        'created_at'=>'2020-05-15 01:20:06',
        'updated_at'=>'2020-06-21 00:08:23',
        'deleted_at'=>NULL
        ] );
        
        
                    
        Post::create( [
        'id'=>3942,
        'cat_id'=>8,
        'tag_id'=>1,
        'title'=>'Having and Fighting Ebola â€” Public Health Lessons from a Clinician Turned Patient',
        'text'=>'C. Spencer\r\nN EGL J Med. February 25, 2015',
        'img'=>'file-1463591650-J14MMU.jpg',
        'file'=>NULL,
        'link'=>'http://www.nejm.org/doi/full/10.1056/NEJMp1501355?query=TOC',
        'by'=>0,
        'journal'=>'Site Admin',
        'type'=>0,
        'month_article'=>0,
        'famous'=>0,
        'views'=>1528,
        'show'=>0,
        'date'=>'2015-02-24 04:32:24',
        'active'=>'1',
        'user'=>0,
        'created_at'=>'2020-05-15 01:20:06',
        'updated_at'=>'2020-06-21 00:08:23',
        'deleted_at'=>NULL
        ] );
        
        
                    
        Post::create( [
        'id'=>3943,
        'cat_id'=>8,
        'tag_id'=>11,
        'title'=>'A Scoping Review of Patient Discharge From Intensive Care: Opportunities and Tools to Improve Care',
        'text'=>'Henry T. Stelfox and others\r\nChest. February 20151472:317-327',
        'img'=>'file-1463591681-AGRM7H.jpg',
        'file'=>NULL,
        'link'=>'http://journal.publications.chestnet.org/article.aspx?articleID=1905079',
        'by'=>0,
        'journal'=>'Site Admin',
        'type'=>0,
        'month_article'=>0,
        'famous'=>0,
        'views'=>1499,
        'show'=>0,
        'date'=>'2015-02-28 17:03:52',
        'active'=>'1',
        'user'=>0,
        'created_at'=>'2020-05-15 01:20:06',
        'updated_at'=>'2020-06-21 00:08:23',
        'deleted_at'=>NULL
        ] );
        
        
                    
        Post::create( [
        'id'=>3944,
        'cat_id'=>8,
        'tag_id'=>1,
        'title'=>'The Efficacy and Safety of Heparin in Patients With Sepsis: A Systematic Review and Metaanalysis',
        'text'=>'Zarychanski, Ryan and others\r\nCritical Care Medicine:\r\nMarch 2015 - Volume 43 - Issue 3 - p 511â€“518',
        'img'=>'file-1463591738-IZ3XFW.jpg',
        'file'=>NULL,
        'link'=>'http://journals.lww.com/ccmjournal/Abstract/2015/03000/The_Efficacy_and_Safety_of_Heparin_in_Patients.1.aspx',
        'by'=>0,
        'journal'=>'Site Admin',
        'type'=>0,
        'month_article'=>0,
        'famous'=>0,
        'views'=>1528,
        'show'=>0,
        'date'=>'2015-03-01 17:06:02',
        'active'=>'1',
        'user'=>0,
        'created_at'=>'2020-05-15 01:20:06',
        'updated_at'=>'2020-06-21 00:08:23',
        'deleted_at'=>NULL
        ] );
        
        
                    
        Post::create( [
        'id'=>3945,
        'cat_id'=>8,
        'tag_id'=>11,
        'title'=>'The Impact of Hospital and ICU Organizational Factors on Outcome in Critically Ill Patients: Results From the Extended Prevalence of Infection in Intensive Care Study',
        'text'=>'Sakr, Yasser and others\r\nCritical Care Medicine:\r\nMarch 2015 - Volume 43 - Issue 3 - p 519â€“526',
        'img'=>'file-1463591746-GEE3E7.jpg',
        'file'=>NULL,
        'link'=>'http://journals.lww.com/ccmjournal/Abstract/2015/03000/The_Impact_of_Hospital_and_ICU_Organizational.2.aspx',
        'by'=>0,
        'journal'=>'Site Admin',
        'type'=>0,
        'month_article'=>0,
        'famous'=>0,
        'views'=>1499,
        'show'=>0,
        'date'=>'2015-03-01 17:07:40',
        'active'=>'1',
        'user'=>0,
        'created_at'=>'2020-05-15 01:20:06',
        'updated_at'=>'2020-06-21 00:08:23',
        'deleted_at'=>NULL
        ] );
        
        
                    
        Post::create( [
        'id'=>3946,
        'cat_id'=>8,
        'tag_id'=>6,
        'title'=>'The Role of Aerosolized Colistin in the Treatment of Ventilator-Associated Pneumonia: A Systematic Review and Metaanalysis',
        'text'=>'Valachis, Antonis and others\r\nCritical Care Medicine:\r\nMarch 2015 - Volume 43 - Issue 3 - p 527â€“533',
        'img'=>'file-1463591746-GEE3E7.jpg',
        'file'=>NULL,
        'link'=>'http://journals.lww.com/ccmjournal/Abstract/2015/03000/The_Role_of_Aerosolized_Colistin_in_the_Treatment.3.aspx',
        'by'=>0,
        'journal'=>'Site Admin',
        'type'=>0,
        'month_article'=>0,
        'famous'=>0,
        'views'=>1499,
        'show'=>0,
        'date'=>'2015-03-01 17:09:48',
        'active'=>'1',
        'user'=>0,
        'created_at'=>'2020-05-15 01:20:06',
        'updated_at'=>'2020-06-21 00:08:23',
        'deleted_at'=>NULL
        ] );
        
        
                    
        Post::create( [
        'id'=>3947,
        'cat_id'=>8,
        'tag_id'=>1,
        'title'=>'Randomized, Placebo-Controlled Trial of Acetaminophen for the Reduction of Oxidative Injury in Severe Sepsis',
        'text'=>'Janz, David R and others\r\nCritical Care Medicine:\r\nMarch 2015 - Volume 43 - Issue 3 - p 534â€“541',
        'img'=>'file-1463591650-J14MMU.jpg',
        'file'=>NULL,
        'link'=>'http://journals.lww.com/ccmjournal/Abstract/2015/03000/Randomized,_Placebo_Controlled_Trial_of.4.aspx',
        'by'=>0,
        'journal'=>'Site Admin',
        'type'=>0,
        'month_article'=>0,
        'famous'=>0,
        'views'=>1528,
        'show'=>0,
        'date'=>'2015-03-01 17:12:48',
        'active'=>'1',
        'user'=>0,
        'created_at'=>'2020-05-15 01:20:06',
        'updated_at'=>'2020-06-21 00:08:23',
        'deleted_at'=>NULL
        ] );
        
        
                    
        Post::create( [
        'id'=>3948,
        'cat_id'=>8,
        'tag_id'=>6,
        'title'=>'Use of High-Flow Nasal Cannula Oxygen Therapy to Prevent Desaturation During Tracheal Intubation of Intensive Care Patients With Mild-to-Moderate Hypoxemia',
        'text'=>'Miguel-Montanes, Romain and others\r\nCritical Care Medicine:\r\nMarch 2015 - Volume 43 - Issue 3 - p 574â€“583',
        'img'=>'file-1463591657-K8BRDF.jpg',
        'file'=>NULL,
        'link'=>'http://journals.lww.com/ccmjournal/Abstract/2015/03000/Use_of_High_Flow_Nasal_Cannula_Oxygen_Therapy_to.9.aspx',
        'by'=>0,
        'journal'=>'Site Admin',
        'type'=>0,
        'month_article'=>0,
        'famous'=>0,
        'views'=>1499,
        'show'=>0,
        'date'=>'2015-03-01 17:15:49',
        'active'=>'1',
        'user'=>0,
        'created_at'=>'2020-05-15 01:20:06',
        'updated_at'=>'2020-06-21 00:08:23',
        'deleted_at'=>NULL
        ] );
        
        
                    
        Post::create( [
        'id'=>3949,
        'cat_id'=>8,
        'tag_id'=>1,
        'title'=>'Invasive Candida Infections and the Harm From Antibacterial Drugs in Critically Ill Patients: Data From a Randomized, Controlled Trial to Determine the Role of Ciprofloxacin, Piperacillin-Tazobactam, Meropenem, and Cefuroxime',
        'text'=>'Jensen, Jens-Ulrik S and others\r\nCritical Care Medicine:\r\nMarch 2015 - Volume 43 - Issue 3 - p 594â€“602',
        'img'=>'file-1463591657-K8BRDF.jpg',
        'file'=>NULL,
        'link'=>'http://journals.lww.com/ccmjournal/Abstract/2015/03000/Invasive_Candida_Infections_and_the_Harm_From.11.aspx',
        'by'=>0,
        'journal'=>'Site Admin',
        'type'=>0,
        'month_article'=>0,
        'famous'=>0,
        'views'=>1528,
        'show'=>0,
        'date'=>'2015-03-01 17:17:41',
        'active'=>'1',
        'user'=>0,
        'created_at'=>'2020-05-15 01:20:06',
        'updated_at'=>'2020-06-21 00:08:23',
        'deleted_at'=>NULL
        ] );
        
        
                    
        Post::create( [
        'id'=>3950,
        'cat_id'=>8,
        'tag_id'=>6,
        'title'=>'Comparison of Video Laryngoscopy Versus Direct Laryngoscopy During Urgent Endotracheal Intubation: A Randomized Controlled Trial',
        'text'=>'Silverberg, Michael J and others\r\nCritical Care Medicine:\r\nMarch 2015 - Volume 43 - Issue 3 - p 636â€“641',
        'img'=>'file-1463591657-K8BRDF.jpg',
        'file'=>NULL,
        'link'=>'http://journals.lww.com/ccmjournal/Abstract/2015/03000/Comparison_of_Video_Laryngoscopy_Versus_Direct.16.aspx',
        'by'=>0,
        'journal'=>'Site Admin',
        'type'=>0,
        'month_article'=>0,
        'famous'=>0,
        'views'=>1499,
        'show'=>0,
        'date'=>'2015-03-01 17:23:52',
        'active'=>'1',
        'user'=>0,
        'created_at'=>'2020-05-15 01:20:06',
        'updated_at'=>'2020-06-21 00:08:23',
        'deleted_at'=>NULL
        ] );
        
        
                    
        Post::create( [
        'id'=>3951,
        'cat_id'=>8,
        'tag_id'=>6,
        'title'=>'Mechanical Ventilation Management During Extracorporeal Membrane Oxygenation for Acute Respiratory Distress Syndrome: A Retrospective International Multicenter Study',
        'text'=>'Schmidt, Matthieu and others\r\nCritical Care Medicine:\r\nMarch 2015 - Volume 43 - Issue 3 - p 654â€“664',
        'img'=>'file-1463591738-IZ3XFW.jpg',
        'file'=>NULL,
        'link'=>'http://journals.lww.com/ccmjournal/Abstract/2015/03000/Mechanical_Ventilation_Management_During.18.aspx',
        'by'=>0,
        'journal'=>'Site Admin',
        'type'=>0,
        'month_article'=>0,
        'famous'=>0,
        'views'=>1499,
        'show'=>0,
        'date'=>'2015-03-01 17:36:48',
        'active'=>'1',
        'user'=>0,
        'created_at'=>'2020-05-15 01:20:06',
        'updated_at'=>'2020-06-21 00:08:23',
        'deleted_at'=>NULL
        ] );
        
        
                    
        Post::create( [
        'id'=>3952,
        'cat_id'=>8,
        'tag_id'=>7,
        'title'=>'Stress-Induced Cardiomyopathy',
        'text'=>'Boland, Torrey A. and others\r\nCritical Care Medicine:\r\nMarch 2015 - Volume 43 - Issue 3 - p 686â€“693',
        'img'=>'file-1463591650-J14MMU.jpg',
        'file'=>NULL,
        'link'=>'http://journals.lww.com/ccmjournal/Abstract/2015/03000/Stress_Induced_Cardiomyopathy.21.aspx',
        'by'=>0,
        'journal'=>'Site Admin',
        'type'=>0,
        'month_article'=>0,
        'famous'=>0,
        'views'=>1499,
        'show'=>0,
        'date'=>'2015-03-01 17:39:03',
        'active'=>'1',
        'user'=>0,
        'created_at'=>'2020-05-15 01:20:06',
        'updated_at'=>'2020-06-21 00:08:23',
        'deleted_at'=>NULL
        ] );
        
        
                    
        Post::create( [
        'id'=>3953,
        'cat_id'=>9,
        'tag_id'=>1,
        'title'=>'Clinical practice guidelines for the management of invasive Candida infections in adults in the Middle East region: Expert panel recommendations',
        'text'=>'Alothman AF and others\r\nJ Infect Public Health. 2014 Feb71:6-19',
        'img'=>'file-1463591732-SM39NY.jpg',
        'file'=>NULL,
        'link'=>'http://www.jiph.org/article/S1876-0341(13)00105-6/fulltext',
        'by'=>0,
        'journal'=>'Site Admin',
        'type'=>0,
        'month_article'=>0,
        'famous'=>0,
        'views'=>1528,
        'show'=>0,
        'date'=>'2014-03-07 16:23:19',
        'active'=>'1',
        'user'=>0,
        'created_at'=>'2020-05-15 01:20:06',
        'updated_at'=>'2020-06-21 00:08:23',
        'deleted_at'=>NULL
        ] );
        
        
                    
        Post::create( [
        'id'=>3954,
        'cat_id'=>8,
        'tag_id'=>10,
        'title'=>'Fibrinogen concentrates for post-partum haemorrhage Do not miss the most relevant population!',
        'text'=>'B. Ickx and C. M. Samama\r\nBr. J. Anaesth. published 3 March 2015',
        'img'=>'file-1463591681-AGRM7H.jpg',
        'file'=>NULL,
        'link'=>'http://bja.oxfordjournals.org/content/early/2015/03/03/bja.aev033.extract?papetoc',
        'by'=>0,
        'journal'=>'Site Admin',
        'type'=>0,
        'month_article'=>0,
        'famous'=>0,
        'views'=>1499,
        'show'=>0,
        'date'=>'2015-03-03 16:11:24',
        'active'=>'1',
        'user'=>0,
        'created_at'=>'2020-05-15 01:20:06',
        'updated_at'=>'2020-06-21 00:08:23',
        'deleted_at'=>NULL
        ] );
        
        
                    
        Post::create( [
        'id'=>3955,
        'cat_id'=>8,
        'tag_id'=>6,
        'title'=>'ISICEM 2015: European launch of the advanced Proxima System',
        'text'=>'Discover how easy it is to run a blood gas with this in-line arterial blood gas monitoring system',
        'img'=>'file-1463591644-ZGN2DA.jpg',
        'file'=>NULL,
        'link'=>'http://www.spheremedical.com/products/proxima?utm_source=Newsletter&utm_medium=Email&utm_campaign=Content',
        'by'=>0,
        'journal'=>'Site Admin',
        'type'=>0,
        'month_article'=>0,
        'famous'=>0,
        'views'=>1499,
        'show'=>0,
        'date'=>'2015-03-03 05:20:58',
        'active'=>'1',
        'user'=>0,
        'created_at'=>'2020-05-15 01:20:06',
        'updated_at'=>'2020-06-21 00:08:23',
        'deleted_at'=>NULL
        ] );
        
        
                    
        Post::create( [
        'id'=>3956,
        'cat_id'=>8,
        'tag_id'=>10,
        'title'=>'Frequency of Blood Tests in Heart Surgery Patients May Lead to Anemia, Transfusions',
        'text'=>'Patients should ask their doctors whether a specific test is necessary\r\nAnnals of Thoracic Surgery, Feb. 24, 2015',
        'img'=>'file-1463591738-IZ3XFW.jpg',
        'file'=>NULL,
        'link'=>'http://www.newswise.com/articles/frequency-of-blood-tests-in-heart-surgery-patients-may-lead-to-anemia-transfusions',
        'by'=>0,
        'journal'=>'Site Admin',
        'type'=>0,
        'month_article'=>0,
        'famous'=>0,
        'views'=>1499,
        'show'=>0,
        'date'=>'2015-02-24 05:42:15',
        'active'=>'1',
        'user'=>0,
        'created_at'=>'2020-05-15 01:20:06',
        'updated_at'=>'2020-06-21 00:08:23',
        'deleted_at'=>NULL
        ] );
        
        
                    
        Post::create( [
        'id'=>3957,
        'cat_id'=>8,
        'tag_id'=>1,
        'title'=>'Caspofungin versus liposomal amphotericin B for empirical antifungal therapy in patients with persistent fever and neutropenia.',
        'text'=>'Walsh TJ and others\r\nN Engl J Med. 2004 Sep 3035114:1391-402',
        'img'=>'file-1463591644-ZGN2DA.jpg',
        'file'=>NULL,
        'link'=>'http://www.nejm.org/doi/full/10.1056/NEJMoa040446',
        'by'=>0,
        'journal'=>'Site Admin',
        'type'=>0,
        'month_article'=>0,
        'famous'=>1,
        'views'=>1623,
        'show'=>0,
        'date'=>'2005-09-30 12:23:22',
        'active'=>'1',
        'user'=>0,
        'created_at'=>'2020-05-15 01:20:06',
        'updated_at'=>'2020-06-21 00:08:23',
        'deleted_at'=>NULL
        ] );
        
        
                    
        Post::create( [
        'id'=>3958,
        'cat_id'=>12,
        'tag_id'=>1,
        'title'=>'Trial of Early, Goal-Directed Resuscitation for Septic Shock',
        'text'=>'P.R. Mouncey and Others for the ProMISe Trial Investigators\r\nN Engl J Med. March 17, 2015',
        'img'=>'file-1463591752-XK911T.jpg',
        'file'=>NULL,
        'link'=>'http://www.nejm.org/doi/full/10.1056/NEJMoa1500896#t=abstract',
        'by'=>0,
        'journal'=>'Site Admin',
        'type'=>0,
        'month_article'=>0,
        'famous'=>1,
        'views'=>1734,
        'show'=>0,
        'date'=>'2015-03-18 16:08:52',
        'active'=>'1',
        'user'=>0,
        'created_at'=>'2020-05-15 01:20:06',
        'updated_at'=>'2020-06-21 00:08:23',
        'deleted_at'=>NULL
        ] );
        
        
                    
        Post::create( [
        'id'=>3959,
        'cat_id'=>12,
        'tag_id'=>10,
        'title'=>'Age of Transfused Blood in Critically Ill Patients',
        'text'=>'J. Lacroix and Others for the ABLE Investigators and the Canadian Critical Care Trials Group \r\nN Engl J Med. March 17, 2015',
        'img'=>'file-1463591752-XK911T.jpg',
        'file'=>NULL,
        'link'=>'http://www.nejm.org/doi/full/10.1056/NEJMoa1500704?query=featured_home',
        'by'=>0,
        'journal'=>'Site Admin',
        'type'=>0,
        'month_article'=>0,
        'famous'=>1,
        'views'=>1705,
        'show'=>0,
        'date'=>'2015-03-17 16:12:17',
        'active'=>'1',
        'user'=>0,
        'created_at'=>'2020-05-15 01:20:06',
        'updated_at'=>'2020-06-21 00:08:23',
        'deleted_at'=>NULL
        ] );
        
        
                    
        Post::create( [
        'id'=>3960,
        'cat_id'=>12,
        'tag_id'=>1,
        'title'=>'SIRS Criteria in Defining Severe Sepsis',
        'text'=>'K.-M. Kaukonen and Others\r\n N Engl J Med. March 17, 2015',
        'img'=>'file-1463591650-J14MMU.jpg',
        'file'=>NULL,
        'link'=>'http://www.nejm.org/doi/full/10.1056/NEJMoa1415236?query=featured_home',
        'by'=>0,
        'journal'=>'Site Admin',
        'type'=>0,
        'month_article'=>0,
        'famous'=>1,
        'views'=>1734,
        'show'=>0,
        'date'=>'2015-03-18 06:25:28',
        'active'=>'1',
        'user'=>0,
        'created_at'=>'2020-05-15 01:20:06',
        'updated_at'=>'2020-06-21 00:08:23',
        'deleted_at'=>NULL
        ] );
        
        
                    
        Post::create( [
        'id'=>3961,
        'cat_id'=>8,
        'tag_id'=>1,
        'title'=>'The Next Epidemic â€” Lessons from Ebola',
        'text'=>'B. Gates \r\nN Engl J Med. March 18, 2015',
        'img'=>'file-1463591738-IZ3XFW.jpg',
        'file'=>NULL,
        'link'=>'http://www.nejm.org/doi/full/10.1056/NEJMp1502918?query=featured_home',
        'by'=>0,
        'journal'=>'Site Admin',
        'type'=>0,
        'month_article'=>0,
        'famous'=>0,
        'views'=>1528,
        'show'=>0,
        'date'=>'2015-03-18 16:03:23',
        'active'=>'1',
        'user'=>0,
        'created_at'=>'2020-05-15 01:20:06',
        'updated_at'=>'2020-06-21 00:08:23',
        'deleted_at'=>NULL
        ] );
        
        
                    
        Post::create( [
        'id'=>3962,
        'cat_id'=>12,
        'tag_id'=>10,
        'title'=>'Liberal or Restrictive Transfusion after Cardiac Surgery',
        'text'=>'Gavin J. Murphy and others for the TITRe2 Investigators\r\nN Engl J Med. March 12, 2015',
        'img'=>'file-1463591732-SM39NY.jpg',
        'file'=>NULL,
        'link'=>'http://www.nejm.org/doi/full/10.1056/NEJMoa1403612',
        'by'=>0,
        'journal'=>'Site Admin',
        'type'=>0,
        'month_article'=>0,
        'famous'=>1,
        'views'=>1705,
        'show'=>0,
        'date'=>'2015-03-12 16:12:29',
        'active'=>'1',
        'user'=>0,
        'created_at'=>'2020-05-15 01:20:06',
        'updated_at'=>'2020-06-21 00:08:23',
        'deleted_at'=>NULL
        ] );
        
        
                    
        Post::create( [
        'id'=>3963,
        'cat_id'=>8,
        'tag_id'=>7,
        'title'=>'Trial of Everolimus-Eluting Stents or Bypass Surgery for Coronary Disease',
        'text'=>'S.-J. Park and Others for the BEST Trial Investigators\r\nN Engl J Med. March 16, 2015',
        'img'=>'file-1463591644-ZGN2DA.jpg',
        'file'=>NULL,
        'link'=>'http://www.nejm.org/doi/full/10.1056/NEJMoa1415447',
        'by'=>0,
        'journal'=>'Site Admin',
        'type'=>0,
        'month_article'=>0,
        'famous'=>1,
        'views'=>1594,
        'show'=>0,
        'date'=>'2015-03-18 16:11:24',
        'active'=>'1',
        'user'=>0,
        'created_at'=>'2020-05-15 01:20:06',
        'updated_at'=>'2020-06-21 00:08:23',
        'deleted_at'=>NULL
        ] );
        
        
                    
        Post::create( [
        'id'=>3964,
        'cat_id'=>9,
        'tag_id'=>1,
        'title'=>'Surviving Sepsis Campaign Bundles - Revised April 2015',
        'text'=>'Updated Bundles in Response to New Evidence\r\nwww.survivingsepsis.org/Bundles',
        'img'=>'file-1463591644-ZGN2DA.jpg',
        'file'=>NULL,
        'link'=>'http://www.survivingsepsis.org/SiteCollectionDocuments/SSC_Bundle.pdf',
        'by'=>0,
        'journal'=>'Site Admin',
        'type'=>0,
        'month_article'=>0,
        'famous'=>0,
        'views'=>1528,
        'show'=>0,
        'date'=>'2015-04-27 06:30:52',
        'active'=>'1',
        'user'=>0,
        'created_at'=>'2020-05-15 01:20:06',
        'updated_at'=>'2020-06-21 00:08:23',
        'deleted_at'=>NULL
        ] );
        
        
                    
        Post::create( [
        'id'=>3965,
        'cat_id'=>8,
        'tag_id'=>7,
        'title'=>'Bivalirudin vs Heparin With or Without Tirofiban During Primary Percutaneous Coronary Intervention in Acute Myocardial Infarction: The BRIGHT Randomized Clinical Trial',
        'text'=>'Han Y and others, for the BRIGHT Investigators\r\nJAMA. 2015 Mar 16',
        'img'=>'file-1463591732-SM39NY.jpg',
        'file'=>NULL,
        'link'=>'http://jama.jamanetwork.com/article.aspx?articleid=2205187',
        'by'=>0,
        'journal'=>'Site Admin',
        'type'=>0,
        'month_article'=>0,
        'famous'=>1,
        'views'=>1594,
        'show'=>0,
        'date'=>'2015-03-16 20:40:00',
        'active'=>'1',
        'user'=>0,
        'created_at'=>'2020-05-15 01:20:06',
        'updated_at'=>'2020-06-21 00:08:23',
        'deleted_at'=>NULL
        ] );
        
        
                    
        Post::create( [
        'id'=>3966,
        'cat_id'=>8,
        'tag_id'=>1,
        'title'=>'Azithromycin for acute lower respiratory tract infections',
        'text'=>'Laopaiboon M, Panpanich R, Swa Mya K\r\nCochrane Database Syst Rev. 2015 Mar 83',
        'img'=>'file-1463591657-K8BRDF.jpg',
        'file'=>NULL,
        'link'=>'http://www.ntkinstitute.org/news/content.nsf/NTKPaperFrameSet?OpenForm&pp=1&id=DF5950353CA4263E85256FDB00692E0A&newsid=14132D8207C20DE585257E0A005D05DE&locref=ntkwatch&u=http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&db=PubMed&dopt=Abstract&list_uids=25749735',
        'by'=>0,
        'journal'=>'Site Admin',
        'type'=>0,
        'month_article'=>0,
        'famous'=>0,
        'views'=>1528,
        'show'=>0,
        'date'=>'2015-03-26 10:43:52',
        'active'=>'1',
        'user'=>0,
        'created_at'=>'2020-05-15 01:20:06',
        'updated_at'=>'2020-06-21 00:08:23',
        'deleted_at'=>NULL
        ] );
        
        
                    
        Post::create( [
        'id'=>3967,
        'cat_id'=>8,
        'tag_id'=>8,
        'title'=>'Renal effects of atorvastatin and rosuvastatin in patients with diabetes who have progressive renal disease PLANET I: a randomised clinical trial',
        'text'=>'de Zeeuw D and others\r\nLancet Diabetes Endocrinol. 2015 Mar33:181-90',
        'img'=>'file-1463591746-GEE3E7.jpg',
        'file'=>NULL,
        'link'=>'http://www.ntkinstitute.org/news/content.nsf/NTKPaperFrameSet?OpenForm&pp=1&id=DF5950353CA4263E85256FDB00692E0A&newsid=1085618E8AADFDA085257E0300486D8B&locref=ntkwatch&u=http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&db=PubMed&dopt=Abstract&list_uids=25660356',
        'by'=>0,
        'journal'=>'Site Admin',
        'type'=>0,
        'month_article'=>0,
        'famous'=>0,
        'views'=>1550,
        'show'=>0,
        'date'=>'2015-03-03 10:49:04',
        'active'=>'1',
        'user'=>0,
        'created_at'=>'2020-05-15 01:20:06',
        'updated_at'=>'2020-06-21 00:08:23',
        'deleted_at'=>NULL
        ] );
        
        
                    
        Post::create( [
        'id'=>3968,
        'cat_id'=>8,
        'tag_id'=>11,
        'title'=>'Characteristics and Outcomes of Patients Admitted to ICU Following Activation of the Medical Emergency Team: Impact of Introducing a Two-Tier Response System',
        'text'=>'Aneman, Anders and others\r\nCritical Care Medicine: \r\nApril 2015 - Volume 43 - Issue 4 -p 765â€“773',
        'img'=>'file-1463591732-SM39NY.jpg',
        'file'=>NULL,
        'link'=>'http://journals.lww.com/ccmjournal/Abstract/2015/04000/Characteristics_and_Outcomes_of_Patients_Admitted.6.aspx',
        'by'=>0,
        'journal'=>'Site Admin',
        'type'=>0,
        'month_article'=>0,
        'famous'=>0,
        'views'=>1499,
        'show'=>0,
        'date'=>'2015-04-01 10:55:03',
        'active'=>'1',
        'user'=>0,
        'created_at'=>'2020-05-15 01:20:06',
        'updated_at'=>'2020-06-21 00:08:23',
        'deleted_at'=>NULL
        ] );
        
        
                    
        Post::create( [
        'id'=>3969,
        'cat_id'=>8,
        'tag_id'=>1,
        'title'=>'The Role of Systemic Antibiotics in Acquiring Respiratory Tract Colonization With Gram-Negative Bacteria in Intensive Care Patients: A Nested Cohort Study',
        'text'=>'Jongerden, Irene P and others',
        'img'=>'file-1463591725-9J88SF.jpg',
        'file'=>NULL,
        'link'=>'http://journals.lww.com/ccmjournal/Abstract/2015/04000/The_Role_of_Systemic_Antibiotics_in_Acquiring.7.aspx',
        'by'=>0,
        'journal'=>'Site Admin',
        'type'=>0,
        'month_article'=>0,
        'famous'=>0,
        'views'=>1528,
        'show'=>0,
        'date'=>'2015-04-01 10:54:51',
        'active'=>'1',
        'user'=>0,
        'created_at'=>'2020-05-15 01:20:06',
        'updated_at'=>'2020-06-21 00:08:23',
        'deleted_at'=>NULL
        ] );
        
        
                    
        Post::create( [
        'id'=>3970,
        'cat_id'=>8,
        'tag_id'=>6,
        'title'=>'Lung Recruitability Is Better Estimated According to the Berlin Definition of Acute Respiratory Distress Syndrome at Standard 5 cm H2O Rather Than Higher Positive End-Expiratory...',
        'text'=>'Caironi, Pietro Carlesso and others\r\nCritical Care Medicine: \r\nApril 2015 - Volume 43 - Issue 4 - p 781â€“790',
        'img'=>'file-1463591650-J14MMU.jpg',
        'file'=>NULL,
        'link'=>'http://journals.lww.com/ccmjournal/Abstract/2015/04000/Lung_Recruitability_Is_Better_Estimated_According.8.aspx',
        'by'=>0,
        'journal'=>'Site Admin',
        'type'=>0,
        'month_article'=>0,
        'famous'=>0,
        'views'=>1499,
        'show'=>0,
        'date'=>'2015-04-01 10:59:12',
        'active'=>'1',
        'user'=>0,
        'created_at'=>'2020-05-15 01:20:06',
        'updated_at'=>'2020-06-21 00:08:23',
        'deleted_at'=>NULL
        ] );
        
        
                    
        Post::create( [
        'id'=>3971,
        'cat_id'=>8,
        'tag_id'=>6,
        'title'=>'Prehospital Aspirin Use Is Associated With Reduced Risk of Acute Respiratory Distress Syndrome in Critically Ill Patients: A Propensity-Adjusted Analysis',
        'text'=>'Chen, Wei and others\r\nCritical Care Medicine: \r\nApril 2015 - Volume 43 - Issue 4 - p 801â€“807',
        'img'=>'file-1463591752-XK911T.jpg',
        'file'=>NULL,
        'link'=>'http://journals.lww.com/ccmjournal/Abstract/2015/04000/Prehospital_Aspirin_Use_Is_Associated_With_Reduced.10.aspx',
        'by'=>0,
        'journal'=>'Site Admin',
        'type'=>0,
        'month_article'=>0,
        'famous'=>0,
        'views'=>1499,
        'show'=>0,
        'date'=>'2015-04-01 11:01:31',
        'active'=>'1',
        'user'=>0,
        'created_at'=>'2020-05-15 01:20:06',
        'updated_at'=>'2020-06-21 00:08:23',
        'deleted_at'=>NULL
        ] );
        
        
                    
        Post::create( [
        'id'=>3972,
        'cat_id'=>8,
        'tag_id'=>7,
        'title'=>'Is Long-Axis View Superior to Short-Axis View in Ultrasound-Guided Central Venous Catheterization',
        'text'=>'Vogel, Jody A and others\r\nCritical Care Medicine: \r\nApril 2015 - Volume 43 - Issue 4 - p 832â€“839',
        'img'=>'file-1463591738-IZ3XFW.jpg',
        'file'=>NULL,
        'link'=>'http://journals.lww.com/ccmjournal/Abstract/2015/04000/Is_Long_Axis_View_Superior_to_Short_Axis_View_in.14.aspx',
        'by'=>0,
        'journal'=>'Site Admin',
        'type'=>0,
        'month_article'=>0,
        'famous'=>0,
        'views'=>1499,
        'show'=>0,
        'date'=>'2015-04-01 11:04:45',
        'active'=>'1',
        'user'=>0,
        'created_at'=>'2020-05-15 01:20:06',
        'updated_at'=>'2020-06-21 00:08:23',
        'deleted_at'=>NULL
        ] );
        
        
                    
        Post::create( [
        'id'=>3973,
        'cat_id'=>8,
        'tag_id'=>6,
        'title'=>'Noninvasive Ventilation and Survival in Acute Care Settings: A Comprehensive Systematic Review and Metaanalysis of Randomized Controlled Trials',
        'text'=>'Cabrini, Luca and others\r\nCritical Care Medicine: \r\nApril 2015 - Volume 43 - Issue 4 - p 880â€“888',
        'img'=>'file-1463591639-4YG8JY.jpg',
        'file'=>NULL,
        'link'=>'http://journals.lww.com/ccmjournal/Abstract/2015/04000/Noninvasive_Ventilation_and_Survival_in_Acute_Care.20.aspx',
        'by'=>0,
        'journal'=>'Site Admin',
        'type'=>0,
        'month_article'=>0,
        'famous'=>0,
        'views'=>1499,
        'show'=>0,
        'date'=>'2015-04-01 11:08:14',
        'active'=>'1',
        'user'=>0,
        'created_at'=>'2020-05-15 01:20:06',
        'updated_at'=>'2020-06-21 00:08:23',
        'deleted_at'=>NULL
        ] );
        
        
                    
        Post::create( [
        'id'=>3974,
        'cat_id'=>8,
        'tag_id'=>7,
        'title'=>'Active Compression-Decompression Resuscitation and Impedance Threshold Device for Out-of-Hospital Cardiac Arrest: A Systematic Review and Metaanalysis of Randomized Controlled...',
        'text'=>'Wang, Chih-Hung and others\r\nCritical Care Medicine: \r\nApril 2015 - Volume 43 - Issue 4 - p 889â€“896',
        'img'=>'file-1463591725-9J88SF.jpg',
        'file'=>NULL,
        'link'=>'http://journals.lww.com/ccmjournal/Abstract/2015/04000/Active_Compression_Decompression_Resuscitation_and.21.aspx',
        'by'=>0,
        'journal'=>'Site Admin',
        'type'=>0,
        'month_article'=>0,
        'famous'=>0,
        'views'=>1499,
        'show'=>0,
        'date'=>'2015-04-01 11:11:14',
        'active'=>'1',
        'user'=>0,
        'created_at'=>'2020-05-15 01:20:06',
        'updated_at'=>'2020-06-21 00:08:23',
        'deleted_at'=>NULL
        ] );
        
        
                    
        Post::create( [
        'id'=>3975,
        'cat_id'=>8,
        'tag_id'=>7,
        'title'=>'Passive leg raising: five rules, not a drop of fluid!',
        'text'=>'Xavier Monnet and Jean-Louis Tebou\r\nCritical Care 2015, 19:18',
        'img'=>'file-1463591650-J14MMU.jpg',
        'file'=>NULL,
        'link'=>'http://ccforum.com/content/19/1/18?utm_campaign=BMC15614C&utm_medium=BMCemail&utm_source=Teradata',
        'by'=>0,
        'journal'=>'Site Admin',
        'type'=>0,
        'month_article'=>0,
        'famous'=>1,
        'views'=>1594,
        'show'=>0,
        'date'=>'2015-01-01 16:26:54',
        'active'=>'1',
        'user'=>0,
        'created_at'=>'2020-05-15 01:20:06',
        'updated_at'=>'2020-06-21 00:08:23',
        'deleted_at'=>NULL
        ] );
        
        
                    
        Post::create( [
        'id'=>3976,
        'cat_id'=>8,
        'tag_id'=>7,
        'title'=>'Starling curves and central venous pressure',
        'text'=>'David A Berlin and Jan Bakker\r\nCritical Care 2015, 19:55',
        'img'=>'file-1463591685-DDLMK9.jpg',
        'file'=>NULL,
        'link'=>'http://ccforum.com/content/19/1/55?utm_campaign=BMC15614C&utm_medium=BMCemail&utm_source=Teradata',
        'by'=>0,
        'journal'=>'Site Admin',
        'type'=>0,
        'month_article'=>0,
        'famous'=>1,
        'views'=>1594,
        'show'=>0,
        'date'=>'2015-02-16 16:28:41',
        'active'=>'1',
        'user'=>0,
        'created_at'=>'2020-05-15 01:20:06',
        'updated_at'=>'2020-06-21 00:08:23',
        'deleted_at'=>NULL
        ] );
        
        
                    
        Post::create( [
        'id'=>3977,
        'cat_id'=>8,
        'tag_id'=>8,
        'title'=>'Renal angina: concept and development of pretest probability assessment in acute kidney injury',
        'text'=>'Lakhmir S Chawla, Stuart L Goldstein, John A Kellum and Claudio Ronco\r\nCritical Care 2015, 19:93',
        'img'=>'file-1463591650-J14MMU.jpg',
        'file'=>NULL,
        'link'=>'http://ccforum.com/content/19/1/93?utm_campaign=BMC15775&utm_medium=BMCemail&utm_source=Teradata',
        'by'=>0,
        'journal'=>'Site Admin',
        'type'=>0,
        'month_article'=>0,
        'famous'=>1,
        'views'=>1645,
        'show'=>0,
        'date'=>'2015-02-27 07:44:16',
        'active'=>'1',
        'user'=>0,
        'created_at'=>'2020-05-15 01:20:06',
        'updated_at'=>'2020-06-21 00:08:23',
        'deleted_at'=>NULL
        ] );
        
        
                    
        Post::create( [
        'id'=>3978,
        'cat_id'=>8,
        'tag_id'=>6,
        'title'=>'Impact of closed versus open tracheal suctioning systems for mechanically ventilated adults: a systematic review and meta-analysis',
        'text'=>'Akira Kuriyama, Noriyuki Umakoshi, Jun Fujinaga, Tadaaki Takada\r\nIntensive Care Med J. March 2105 413:402-411',
        'img'=>'file-1463591657-K8BRDF.jpg',
        'file'=>NULL,
        'link'=>'http://icmjournal.esicm.org/journals/abstract.html?v=41&j=134&i=3&a=3565_10.1007_s00134-014-3565-4&doi=',
        'by'=>0,
        'journal'=>'Site Admin',
        'type'=>0,
        'month_article'=>0,
        'famous'=>0,
        'views'=>1499,
        'show'=>0,
        'date'=>'2015-03-30 07:50:24',
        'active'=>'1',
        'user'=>0,
        'created_at'=>'2020-05-15 01:20:06',
        'updated_at'=>'2020-06-21 00:08:23',
        'deleted_at'=>NULL
        ] );
        
        
                    
        Post::create( [
        'id'=>3979,
        'cat_id'=>8,
        'tag_id'=>8,
        'title'=>'Comparison of different equations to assess glomerular filtration in critically ill patients',
        'text'=>'Mieke Carlier and others\r\nIntensive Care Med J. March 2105 413:427-435',
        'img'=>'file-1463591732-SM39NY.jpg',
        'file'=>NULL,
        'link'=>'http://icmjournal.esicm.org/journals/abstract.html?v=41&j=134&i=3&a=3641_10.1007_s00134-014-3641-9&doi=',
        'by'=>0,
        'journal'=>'Site Admin',
        'type'=>0,
        'month_article'=>0,
        'famous'=>0,
        'views'=>1550,
        'show'=>0,
        'date'=>'2015-03-30 07:51:42',
        'active'=>'1',
        'user'=>0,
        'created_at'=>'2020-05-15 01:20:06',
        'updated_at'=>'2020-06-21 00:08:23',
        'deleted_at'=>NULL
        ] );

        Post::create( [
            'id'=>3980,
            'cat_id'=>8,
            'tag_id'=>2,
            'title'=>'Volatile-Based Short-Term Sedation in Cardiac Surgical Patients: A Prospective Randomized Controlled Trial',
            'text'=>'Jerath A1, Beattie SW, Chandy T, Karski J, Djaiani G, Rao V, Yau T, Wasowicz M Perioperative Anesthesia Clinical Trials Group\r\nCrit Care Med. March 2015',
            'img'=>'file-1463591657-K8BRDF.jpg',
            'file'=>NULL,
            'link'=>'http://journals.lww.com/ccmjournal/Abstract/publishahead/Volatile_Based_Short_Term_Sedation_in_Cardiac.97326.aspx',
            'by'=>0,
            'journal'=>'Site Admin',
            'type'=>0,
            'month_article'=>0,
            'famous'=>0,
            'views'=>1570,
            'show'=>0,
            'date'=>'2015-03-31 14:55:59',
            'active'=>'1',
            'user'=>0,
            'created_at'=>'2020-05-15 01:20:06',
            'updated_at'=>'2020-06-21 00:08:23',
            'deleted_at'=>NULL
            ] );
            
            
                        
            Post::create( [
            'id'=>3981,
            'cat_id'=>8,
            'tag_id'=>8,
            'title'=>'A multicenter study on the effect of continuous hemodiafiltration intensity on antibiotic pharmacokinetics',
            'text'=>'Roberts DM1, Liu X, Roberts JA, Nair P, Cole L, Roberts MS, Lipman J, Bellomo R RENAL Replacement Therapy Study Investigators\r\nCrit Care. 2015 March191:818',
            'img'=>'file-1463591732-SM39NY.jpg',
            'file'=>NULL,
            'link'=>'http://ccforum.com/content/19/1/84/abstract',
            'by'=>0,
            'journal'=>'Site Admin',
            'type'=>0,
            'month_article'=>0,
            'famous'=>0,
            'views'=>1550,
            'show'=>0,
            'date'=>'2015-03-31 14:59:33',
            'active'=>'1',
            'user'=>0,
            'created_at'=>'2020-05-15 01:20:06',
            'updated_at'=>'2020-06-21 00:08:23',
            'deleted_at'=>NULL
            ] );
            
            
                        
            Post::create( [
            'id'=>3982,
            'cat_id'=>8,
            'tag_id'=>6,
            'title'=>'Antibiotic Treatment Strategies for Community-Acquired Pneumonia in Adults',
            'text'=>'Douwe F. Postma and others, for the CAP-START Study Group\r\nN Engl J Med 2015 372:1312-1323April 2, 2015',
            'img'=>'file-1463591732-SM39NY.jpg',
            'file'=>NULL,
            'link'=>'http://www.nejm.org/doi/full/10.1056/NEJMoa1406330',
            'by'=>0,
            'journal'=>'Site Admin',
            'type'=>0,
            'month_article'=>0,
            'famous'=>1,
            'views'=>1594,
            'show'=>0,
            'date'=>'2015-04-01 21:33:21',
            'active'=>'1',
            'user'=>0,
            'created_at'=>'2020-05-15 01:20:06',
            'updated_at'=>'2020-06-21 00:08:23',
            'deleted_at'=>NULL
            ] );
            
            
                        
            Post::create( [
            'id'=>3983,
            'cat_id'=>8,
            'tag_id'=>4,
            'title'=>'A controlled resuscitation strategy is feasible and safe in hypotensive trauma patients: Results of a prospective randomized pilot trial',
            'text'=>'Schreiber, Martin A and others\r\nJournal of Trauma and Acute Care Surgery:\r\nApril 2015 - Volume 78 - Issue 4 - p 687â€“697',
            'img'=>'file-1463591650-J14MMU.jpg',
            'file'=>NULL,
            'link'=>'http://journals.lww.com/jtrauma/Abstract/2015/04000/A_controlled_resuscitation_strategy_is_feasible.3.aspx',
            'by'=>0,
            'journal'=>'Site Admin',
            'type'=>0,
            'month_article'=>0,
            'famous'=>0,
            'views'=>1596,
            'show'=>0,
            'date'=>'2015-04-03 21:42:52',
            'active'=>'1',
            'user'=>0,
            'created_at'=>'2020-05-15 01:20:06',
            'updated_at'=>'2020-06-21 00:08:23',
            'deleted_at'=>NULL
            ] );
            
            
                        
            Post::create( [
            'id'=>3984,
            'cat_id'=>8,
            'tag_id'=>5,
            'title'=>'Secondary brain injury in trauma patients: The effects of remote ischemic conditioning',
            'text'=>'Joseph, Bellal and others\r\nJournal of Trauma and Acute Care Surgery:\r\nApril 2015 - Volume 78 - Issue 4 - p 698â€“705',
            'img'=>'file-1463591685-DDLMK9.jpg',
            'file'=>NULL,
            'link'=>'http://journals.lww.com/jtrauma/Abstract/2015/04000/Secondary_brain_injury_in_trauma_patients___The.4.aspx',
            'by'=>0,
            'journal'=>'Site Admin',
            'type'=>0,
            'month_article'=>0,
            'famous'=>0,
            'views'=>1512,
            'show'=>0,
            'date'=>'2015-04-03 21:37:01',
            'active'=>'1',
            'user'=>0,
            'created_at'=>'2020-05-15 01:20:06',
            'updated_at'=>'2020-06-21 00:08:23',
            'deleted_at'=>NULL
            ] );
            
            
                        
            Post::create( [
            'id'=>3985,
            'cat_id'=>8,
            'tag_id'=>4,
            'title'=>'Early autologous fresh whole blood transfusion leads to less allogeneic transfusions and is safe',
            'text'=>'Rhee, Peter and others\r\nJournal of Trauma and Acute Care Surgery:\r\nApril 2015 - Volume 78 - Issue 4 - p 729â€“734',
            'img'=>'file-1463591681-AGRM7H.jpg',
            'file'=>NULL,
            'link'=>'http://journals.lww.com/jtrauma/Fulltext/2015/04000/Early_autologous_fresh_whole_blood_transfusion.8.aspx',
            'by'=>0,
            'journal'=>'Site Admin',
            'type'=>0,
            'month_article'=>0,
            'famous'=>0,
            'views'=>1596,
            'show'=>0,
            'date'=>'2015-04-03 21:38:32',
            'active'=>'1',
            'user'=>0,
            'created_at'=>'2020-05-15 01:20:06',
            'updated_at'=>'2020-06-21 00:08:23',
            'deleted_at'=>NULL
            ] );
            
            
                        
            Post::create( [
            'id'=>3986,
            'cat_id'=>8,
            'tag_id'=>4,
            'title'=>'Hypotensive resuscitation in combination with arginine vasopressin may prolong the hypotensive resuscitation time in uncontrolled hemorrhagic shock rats',
            'text'=>'Yang, Guangming and others\r\nJournal of Trauma and Acute Care Surgery:\r\nApril 2015 - Volume 78 - Issue 4 - p 760â€“766',
            'img'=>'file-1463591657-K8BRDF.jpg',
            'file'=>NULL,
            'link'=>'http://journals.lww.com/jtrauma/Abstract/2015/04000/Hypotensive_resuscitation_in_combination_with.12.aspx',
            'by'=>0,
            'journal'=>'Site Admin',
            'type'=>0,
            'month_article'=>0,
            'famous'=>0,
            'views'=>1596,
            'show'=>0,
            'date'=>'2015-04-03 21:42:35',
            'active'=>'1',
            'user'=>0,
            'created_at'=>'2020-05-15 01:20:06',
            'updated_at'=>'2020-06-21 00:08:23',
            'deleted_at'=>NULL
            ] );
            
            
                        
            Post::create( [
            'id'=>3987,
            'cat_id'=>8,
            'tag_id'=>4,
            'title'=>'Correction of acute traumatic coagulopathy with small-volume 7.5% NaCl adenosine, lidocaine, and Mg2 occurs within 5 minutes: A ROTEM analysis',
            'text'=>'Letson, Hayley L and thers\r\nJournal of Trauma and Acute Care Surgery:\r\nApril 2015 - Volume 78 - Issue 4 - p 773â€“783',
            'img'=>'file-1463591650-J14MMU.jpg',
            'file'=>NULL,
            'link'=>'http://journals.lww.com/jtrauma/Abstract/2015/04000/Correction_of_acute_traumatic_coagulopathy_with.14.aspx',
            'by'=>0,
            'journal'=>'Site Admin',
            'type'=>0,
            'month_article'=>0,
            'famous'=>0,
            'views'=>1596,
            'show'=>0,
            'date'=>'2015-04-03 21:41:31',
            'active'=>'1',
            'user'=>0,
            'created_at'=>'2020-05-15 01:20:06',
            'updated_at'=>'2020-06-21 00:08:23',
            'deleted_at'=>NULL
            ] );
            
            
                        
            Post::create( [
            'id'=>3988,
            'cat_id'=>8,
            'tag_id'=>8,
            'title'=>'Renal tubular acidosis is highly prevalent in critically ill patients',
            'text'=>'Brunner R, Drolz A, Scherzer T, Staufer K, Fuhrmann V, Zauner C, Holzinger U, SchneeweiÃŸ B\r\nCritical Care 2015, 19 :148 6 April 2015',
            'img'=>'file-1463591639-4YG8JY.jpg',
            'file'=>NULL,
            'link'=>'http://ccforum.com/content/pdf/s13054-015-0890-0.pdf',
            'by'=>0,
            'journal'=>'Site Admin',
            'type'=>0,
            'month_article'=>0,
            'famous'=>0,
            'views'=>1550,
            'show'=>0,
            'date'=>'2015-04-06 18:04:52',
            'active'=>'1',
            'user'=>0,
            'created_at'=>'2020-05-15 01:20:06',
            'updated_at'=>'2020-06-21 00:08:23',
            'deleted_at'=>NULL
            ] );
            
            
                        
            Post::create( [
            'id'=>3989,
            'cat_id'=>8,
            'tag_id'=>8,
            'title'=>'Inhaled nitric oxide therapy and risk of renal dysfunction: a systematic review and meta-analysis of randomized trials',
            'text'=>'Ruan S, Huang T, Wu H, Wu H, Yu C, Lai M\r\nCritical Care 2015, 19 :137 3 April 2015',
            'img'=>'file-1463591685-DDLMK9.jpg',
            'file'=>NULL,
            'link'=>'http://ccforum.com/content/19/1/137/abstract',
            'by'=>0,
            'journal'=>'Site Admin',
            'type'=>0,
            'month_article'=>0,
            'famous'=>0,
            'views'=>1550,
            'show'=>0,
            'date'=>'2015-04-03 18:06:16',
            'active'=>'1',
            'user'=>0,
            'created_at'=>'2020-05-15 01:20:06',
            'updated_at'=>'2020-06-21 00:08:23',
            'deleted_at'=>NULL
            ] );
            
            
                        
            Post::create( [
            'id'=>3990,
            'cat_id'=>8,
            'tag_id'=>1,
            'title'=>'Does gastric tonometry-guided therapy reduce total mortality in critically ill patients',
            'text'=>'Mythen MG\r\nCritical Care 2015, 19 :172 2 April 2015',
            'img'=>'file-1463591738-IZ3XFW.jpg',
            'file'=>NULL,
            'link'=>'http://ccforum.com/content/19/1/172',
            'by'=>0,
            'journal'=>'Site Admin',
            'type'=>0,
            'month_article'=>0,
            'famous'=>0,
            'views'=>1528,
            'show'=>0,
            'date'=>'2015-04-06 18:07:52',
            'active'=>'1',
            'user'=>0,
            'created_at'=>'2020-05-15 01:20:06',
            'updated_at'=>'2020-06-21 00:08:23',
            'deleted_at'=>NULL
            ] );
            
            
                        
            Post::create( [
            'id'=>3991,
            'cat_id'=>8,
            'tag_id'=>4,
            'title'=>'Significant modification of traditional rapid sequence induction improves safety and effectiveness of pre-hospital trauma anaesthesia',
            'text'=>'Lyon RM, Perkins ZB, Chatterjee D, Lockey DJ, Russell MQ, on behalf of Kent, Surrey & Sussex Air Ambulance Trust\r\nCritical Care 2015, 19 :134 1 April 2015',
            'img'=>'file-1463591681-AGRM7H.jpg',
            'file'=>NULL,
            'link'=>'http://ccforum.com/content/19/1/134/abstract',
            'by'=>0,
            'journal'=>'Site Admin',
            'type'=>0,
            'month_article'=>0,
            'famous'=>0,
            'views'=>1596,
            'show'=>0,
            'date'=>'2015-04-05 18:11:27',
            'active'=>'1',
            'user'=>0,
            'created_at'=>'2020-05-15 01:20:06',
            'updated_at'=>'2020-06-21 00:08:23',
            'deleted_at'=>NULL
            ] );
            
            
                        
            Post::create( [
            'id'=>3992,
            'cat_id'=>8,
            'tag_id'=>4,
            'title'=>'Resuscitation strategies with different arterial pressure targets after surgical management of traumatic shock',
            'text'=>'Bai X, Yu W, Ji W, Duan K, Tan S, Lin Z, Xu L, Li N \r\nCritical Care 2015, 19 :170 20 April 2015',
            'img'=>'file-1463591644-ZGN2DA.jpg',
            'file'=>NULL,
            'link'=>'http://ccforum.com/content/19/1/170/abstract',
            'by'=>0,
            'journal'=>'Site Admin',
            'type'=>0,
            'month_article'=>0,
            'famous'=>0,
            'views'=>1596,
            'show'=>0,
            'date'=>'2015-04-20 12:24:07',
            'active'=>'1',
            'user'=>0,
            'created_at'=>'2020-05-15 01:20:06',
            'updated_at'=>'2020-06-21 00:08:23',
            'deleted_at'=>NULL
            ] );
            
            
                        
            Post::create( [
            'id'=>3993,
            'cat_id'=>8,
            'tag_id'=>8,
            'title'=>'Does sodium bicarbonate infusion really have no effect on the incidence of acute kidney injury after cardiac surgery A prospective observational trial',
            'text'=>'Wetz AJ, BrÃ¤uer A, Quintel M, Heise D \r\nCritical Care 2015, 19 :183 22 April 2015',
            'img'=>'file-1463591752-XK911T.jpg',
            'file'=>NULL,
            'link'=>'http://ccforum.com/content/19/1/183/abstract',
            'by'=>0,
            'journal'=>'Site Admin',
            'type'=>0,
            'month_article'=>0,
            'famous'=>0,
            'views'=>1550,
            'show'=>0,
            'date'=>'2015-04-22 07:17:37',
            'active'=>'1',
            'user'=>0,
            'created_at'=>'2020-05-15 01:20:06',
            'updated_at'=>'2020-06-21 00:08:23',
            'deleted_at'=>NULL
            ] );
            
            
                        
            Post::create( [
            'id'=>3994,
            'cat_id'=>8,
            'tag_id'=>5,
            'title'=>'Neuroprotection in acute brain injury: an up-to-date review',
            'text'=>'Stocchetti N, Taccone FS, Citerio G, Pepe PE, Le Roux PD, Oddo M, Polderman KH, Stevens RD, Barsan W, Maas A, Meyfroidt G, Bell MJ, Silbergleit R, Vespa PM, Faden AI, Helbok R, Tisherman S, Zanier ER, Valenzuela T, Wendon J, Menon DK, Vincent J \r\nCritical Care 2015, 19 :186 21 April 2015',
            'img'=>'file-1463591732-SM39NY.jpg',
            'file'=>NULL,
            'link'=>'http://ccforum.com/content/19/1/186',
            'by'=>0,
            'journal'=>'Site Admin',
            'type'=>0,
            'month_article'=>0,
            'famous'=>0,
            'views'=>1512,
            'show'=>0,
            'date'=>'2015-04-21 07:18:47',
            'active'=>'1',
            'user'=>0,
            'created_at'=>'2020-05-15 01:20:06',
            'updated_at'=>'2020-06-21 00:08:23',
            'deleted_at'=>NULL
            ] );
            
            
                        
            Post::create( [
            'id'=>3995,
            'cat_id'=>8,
            'tag_id'=>5,
            'title'=>'The increasing need for biomarkers in intensive care unit-acquired weakness - are microRNAs the solution',
            'text'=>'Lugg ST, Howells PA, Thickett DR \r\nCritical Care 2015, 19 :189 22 April 2015',
            'img'=>'file-1463591685-DDLMK9.jpg',
            'file'=>NULL,
            'link'=>'http://ccforum.com/content/19/1/189/abstract',
            'by'=>0,
            'journal'=>'Site Admin',
            'type'=>0,
            'month_article'=>0,
            'famous'=>0,
            'views'=>1512,
            'show'=>0,
            'date'=>'2015-04-22 07:19:49',
            'active'=>'1',
            'user'=>0,
            'created_at'=>'2020-05-15 01:20:06',
            'updated_at'=>'2020-06-21 00:08:23',
            'deleted_at'=>NULL
            ] );
            
            
                        
            Post::create( [
            'id'=>3996,
            'cat_id'=>8,
            'tag_id'=>9,
            'title'=>'Prednisolone or Pentoxifylline for Alcoholic Hepatitis',
            'text'=>'M.R. Thursz and Others,for the STOPAH Trial\r\nN Engl J Med 2015372:1619-1628',
            'img'=>'file-1463591639-4YG8JY.jpg',
            'file'=>NULL,
            'link'=>'http://www.nejm.org/doi/full/10.1056/NEJMoa1412278?query=TOC',
            'by'=>0,
            'journal'=>'Site Admin',
            'type'=>0,
            'month_article'=>0,
            'famous'=>1,
            'views'=>1594,
            'show'=>0,
            'date'=>'2015-04-23 07:22:21',
            'active'=>'1',
            'user'=>0,
            'created_at'=>'2020-05-15 01:20:06',
            'updated_at'=>'2020-06-21 00:08:23',
            'deleted_at'=>NULL
            ] );
            
            
                        
            Post::create( [
            'id'=>3997,
            'cat_id'=>8,
            'tag_id'=>5,
            'title'=>'Thrombectomy within 8 Hours after Symptom Onset in Ischemic Stroke',
            'text'=>'T.G. Jovin and Others,  for the REVASCAT Trial Investigators\r\nN Engl J Med. April 17, 2015',
            'img'=>'file-1463591644-ZGN2DA.jpg',
            'file'=>NULL,
            'link'=>'http://www.nejm.org/doi/full/10.1056/NEJMoa1503780?query=TOC',
            'by'=>0,
            'journal'=>'Site Admin',
            'type'=>0,
            'month_article'=>0,
            'famous'=>0,
            'views'=>1512,
            'show'=>0,
            'date'=>'2015-04-18 07:24:04',
            'active'=>'1',
            'user'=>0,
            'created_at'=>'2020-05-15 01:20:06',
            'updated_at'=>'2020-06-21 00:08:23',
            'deleted_at'=>NULL
            ] );
            
            
                        
            Post::create( [
            'id'=>3998,
            'cat_id'=>12,
            'tag_id'=>1,
            'title'=>'Surviving Sepsis Campaign Bundles - Revised April 2015',
            'text'=>'Updated Bundles in Response to New Evidence\r\nwww.survivingsepsis.org/Bundles',
            'img'=>'file-1463591746-GEE3E7.jpg',
            'file'=>NULL,
            'link'=>'http://www.survivingsepsis.org/Guidelines/Pages/default.aspx',
            'by'=>0,
            'journal'=>'Site Admin',
            'type'=>0,
            'month_article'=>0,
            'famous'=>1,
            'views'=>1734,
            'show'=>0,
            'date'=>'0000-00-00 00:00:00',
            'active'=>'1',
            'user'=>0,
            'created_at'=>'2020-05-15 01:20:06',
            'updated_at'=>'2020-06-21 00:08:23',
            'deleted_at'=>NULL
            ] );
            
            
                        
            Post::create( [
            'id'=>3999,
            'cat_id'=>8,
            'tag_id'=>4,
            'title'=>'Early autologous fresh whole blood transfusion leads to less allogeneic transfusions and is safe',
            'text'=>'Rhee, Peter and others\r\nJournal of Trauma and Acute Care Surgery:\r\nApril 2015 - Volume 78 - Issue 4 - p 729â€“734',
            'img'=>'file-1463591650-J14MMU.jpg',
            'file'=>NULL,
            'link'=>'http://journals.lww.com/jtrauma/Fulltext/2015/04000/Early_autologous_fresh_whole_blood_transfusion.8.aspx',
            'by'=>0,
            'journal'=>'Site Admin',
            'type'=>0,
            'month_article'=>0,
            'famous'=>0,
            'views'=>1596,
            'show'=>0,
            'date'=>'2015-04-27 06:33:48',
            'active'=>'1',
            'user'=>0,
            'created_at'=>'2020-05-15 01:20:06',
            'updated_at'=>'2020-06-21 00:08:23',
            'deleted_at'=>NULL
            ] );
            
            
                        
            Post::create( [
            'id'=>4000,
            'cat_id'=>8,
            'tag_id'=>4,
            'title'=>'Hypotensive resuscitation in combination with arginine vasopressin may prolong the hypotensive resuscitation time in uncontrolled hemorrhagic shock rats',
            'text'=>'Yang, Guangming and others\r\nJournal of Trauma and Acute Care Surgery:\r\nApril 2015 - Volume 78 - Issue 4 - p 760â€“766',
            'img'=>'file-1463591681-AGRM7H.jpg',
            'file'=>NULL,
            'link'=>'http://journals.lww.com/jtrauma/Abstract/2015/04000/Hypotensive_resuscitation_in_combination_with.12.aspx',
            'by'=>0,
            'journal'=>'Site Admin',
            'type'=>0,
            'month_article'=>0,
            'famous'=>0,
            'views'=>1198,
            'show'=>0,
            'date'=>'2015-04-27 06:35:37',
            'active'=>'1',
            'user'=>0,
            'created_at'=>'2020-05-15 01:20:06',
            'updated_at'=>'2020-06-21 00:08:23',
            'deleted_at'=>NULL
            ] );
            
            
                        
            Post::create( [
            'id'=>4001,
            'cat_id'=>8,
            'tag_id'=>4,
            'title'=>'Correction of acute traumatic coagulopathy with small-volume 7.5% NaCl adenosine, lidocaine, and Mg2 occurs within 5 minutes: A ROTEM analysis',
            'text'=>'Letson, Hayley L and others\r\nJournal of Trauma and Acute Care Surgery:\r\nApril 2015 - Volume 78 - Issue 4 - p 773â€“783',
            'img'=>'file-1463591681-AGRM7H.jpg',
            'file'=>NULL,
            'link'=>'http://journals.lww.com/jtrauma/Abstract/2015/04000/Correction_of_acute_traumatic_coagulopathy_with.14.aspx',
            'by'=>0,
            'journal'=>'Site Admin',
            'type'=>0,
            'month_article'=>0,
            'famous'=>0,
            'views'=>1198,
            'show'=>0,
            'date'=>'2015-04-27 06:40:10',
            'active'=>'1',
            'user'=>0,
            'created_at'=>'2020-05-15 01:20:06',
            'updated_at'=>'2020-06-21 00:08:23',
            'deleted_at'=>NULL
            ] );
            
            
                        
            Post::create( [
            'id'=>4002,
            'cat_id'=>8,
            'tag_id'=>7,
            'title'=>'Improving Use of Targeted Temperature Management After Out-of-Hospital Cardiac Arrest: A Stepped Wedge Cluster Randomized Controlled Trial',
            'text'=>'Morrison, Laurie J and others\r\nCritical Care Medicine:\r\nMay 2015 - Volume 43 - Issue 5 - p 954â€“964',
            'img'=>'file-1463591725-9J88SF.jpg',
            'file'=>NULL,
            'link'=>'http://journals.lww.com/ccmjournal/Abstract/2015/05000/Improving_Use_of_Targeted_Temperature_Management.5.aspx',
            'by'=>0,
            'journal'=>'Site Admin',
            'type'=>0,
            'month_article'=>0,
            'famous'=>0,
            'views'=>1101,
            'show'=>0,
            'date'=>'2015-05-01 05:11:28',
            'active'=>'1',
            'user'=>0,
            'created_at'=>'2020-05-15 01:20:06',
            'updated_at'=>'2020-06-21 00:08:23',
            'deleted_at'=>NULL
            ] );
            
            
                        
            Post::create( [
            'id'=>4003,
            'cat_id'=>8,
            'tag_id'=>4,
            'title'=>'Neurologic Outcomes and Postresuscitation Care of Patients With Myoclonus Following Cardiac Arrest',
            'text'=>'Seder, David B and others\r\nCritical Care Medicine:\r\nMay 2015 - Volume 43 - Issue 5 - p 965â€“972',
            'img'=>'file-1463591644-ZGN2DA.jpg',
            'file'=>NULL,
            'link'=>'http://journals.lww.com/ccmjournal/Fulltext/2015/05000/Neurologic_Outcomes_and_Postresuscitation_Care_of.6.aspx',
            'by'=>0,
            'journal'=>'Site Admin',
            'type'=>0,
            'month_article'=>0,
            'famous'=>0,
            'views'=>1198,
            'show'=>0,
            'date'=>'2015-05-01 05:18:09',
            'active'=>'1',
            'user'=>0,
            'created_at'=>'2020-05-15 01:20:06',
            'updated_at'=>'2020-06-21 00:08:23',
            'deleted_at'=>NULL
            ] );
            
            
                        
            Post::create( [
            'id'=>4004,
            'cat_id'=>8,
            'tag_id'=>7,
            'title'=>'Percutaneous Cannulation for Extracorporeal Membrane Oxygenation by Intensivists: A Retrospective Single-Institution Case Series',
            'text'=>'Conrad, Steven A and others\r\nCritical Care Medicine:\r\nMay 2015 - Volume 43 - Issue 5 - p 1010â€“1015',
            'img'=>'file-1463591738-IZ3XFW.jpg',
            'file'=>NULL,
            'link'=>'http://journals.lww.com/ccmjournal/Abstract/2015/05000/Percutaneous_Cannulation_for_Extracorporeal.12.aspx',
            'by'=>0,
            'journal'=>'Site Admin',
            'type'=>0,
            'month_article'=>0,
            'famous'=>0,
            'views'=>1101,
            'show'=>0,
            'date'=>'2015-05-01 05:16:55',
            'active'=>'1',
            'user'=>0,
            'created_at'=>'2020-05-15 01:20:06',
            'updated_at'=>'2020-06-21 00:08:23',
            'deleted_at'=>NULL
            ] );
            
            
                        
            Post::create( [
            'id'=>4005,
            'cat_id'=>8,
            'tag_id'=>7,
            'title'=>'Extracorporeal Membrane Oxygenation for the Support of Adults With Acute Myocarditis',
            'text'=>'Diddle, J. Wesley and others\r\nCritical Care Medicine:\r\nMay 2015 - Volume 43 - Issue 5 - p 1016â€“1025',
            'img'=>'file-1463591752-XK911T.jpg',
            'file'=>NULL,
            'link'=>'http://journals.lww.com/ccmjournal/Abstract/2015/05000/Extracorporeal_Membrane_Oxygenation_for_the.13.aspx',
            'by'=>0,
            'journal'=>'Site Admin',
            'type'=>0,
            'month_article'=>0,
            'famous'=>0,
            'views'=>1101,
            'show'=>0,
            'date'=>'2015-05-01 05:19:40',
            'active'=>'1',
            'user'=>0,
            'created_at'=>'2020-05-15 01:20:06',
            'updated_at'=>'2020-06-21 00:08:23',
            'deleted_at'=>NULL
            ] );
            
            
                        
            Post::create( [
            'id'=>4006,
            'cat_id'=>8,
            'tag_id'=>2,
            'title'=>'Volatile-Based Short-Term Sedation in Cardiac Surgical Patients: A Prospective Randomized Controlled Trial',
            'text'=>'Jerath, Angela and others\r\nCritical Care Medicine:\r\nMay 2015 - Volume 43 - Issue 5 - p 1062â€“1069',
            'img'=>'file-1463591657-K8BRDF.jpg',
            'file'=>NULL,
            'link'=>'http://journals.lww.com/ccmjournal/Abstract/2015/05000/Volatile_Based_Short_Term_Sedation_in_Cardiac.18.aspx',
            'by'=>0,
            'journal'=>'Site Admin',
            'type'=>0,
            'month_article'=>0,
            'famous'=>0,
            'views'=>1172,
            'show'=>0,
            'date'=>'2015-05-01 06:16:35',
            'active'=>'1',
            'user'=>0,
            'created_at'=>'2020-05-15 01:20:06',
            'updated_at'=>'2020-06-21 00:08:23',
            'deleted_at'=>NULL
            ] );
            
            
                        
            Post::create( [
            'id'=>4007,
            'cat_id'=>8,
            'tag_id'=>7,
            'title'=>'Enhanced Perfusion During Advanced Life Support Improves Survival With Favorable Neurologic Function in a Porcine Model of Refractory Cardiac Arrest',
            'text'=>'Debaty, Guillaume and others\r\nCritical Care Medicine:\r\nMay 2015 - Volume 43 - Issue 5 - p 1087â€“1095',
            'img'=>'file-1463591746-GEE3E7.jpg',
            'file'=>NULL,
            'link'=>'http://journals.lww.com/ccmjournal/Abstract/2015/05000/Enhanced_Perfusion_During_Advanced_Life_Support.21.aspx',
            'by'=>0,
            'journal'=>'Site Admin',
            'type'=>0,
            'month_article'=>0,
            'famous'=>0,
            'views'=>1101,
            'show'=>0,
            'date'=>'2015-05-01 05:24:23',
            'active'=>'1',
            'user'=>0,
            'created_at'=>'2020-05-15 01:20:06',
            'updated_at'=>'2020-06-21 00:08:23',
            'deleted_at'=>NULL
            ] );
            
            
                        
            Post::create( [
            'id'=>4008,
            'cat_id'=>8,
            'tag_id'=>11,
            'title'=>'Estimating the Effect of Palliative Care Interventions and Advance Care Planning on ICU Utilization: A Systematic Review',
            'text'=>'Khandelwal, Nita  and others\r\nCritical Care Medicine:\r\nMay 2015 - Volume 43 - Issue 5 - p 1102â€“1111',
            'img'=>'file-1463591639-4YG8JY.jpg',
            'file'=>NULL,
            'link'=>'http://journals.lww.com/ccmjournal/Abstract/2015/05000/Estimating_the_Effect_of_Palliative_Care.23.aspx',
            'by'=>0,
            'journal'=>'Site Admin',
            'type'=>0,
            'month_article'=>0,
            'famous'=>0,
            'views'=>1101,
            'show'=>0,
            'date'=>'2015-05-01 05:25:41',
            'active'=>'1',
            'user'=>0,
            'created_at'=>'2020-05-15 01:20:06',
            'updated_at'=>'2020-06-21 00:08:23',
            'deleted_at'=>NULL
            ] );
            
            
                        
            Post::create( [
            'id'=>4009,
            'cat_id'=>8,
            'tag_id'=>4,
            'title'=>'Posttraumatic Stress Disorder in Critical Illness Survivors: A Metaanalysis',
            'text'=>'Parker, Ann M and others\r\nCritical Care Medicine:\r\nMay 2015 - Volume 43 - Issue 5 - p 1121â€“1129',
            'img'=>'file-1463591752-XK911T.jpg',
            'file'=>NULL,
            'link'=>'http://journals.lww.com/ccmjournal/Abstract/2015/05000/Posttraumatic_Stress_Disorder_in_Critical_Illness.25.aspx',
            'by'=>0,
            'journal'=>'Site Admin',
            'type'=>0,
            'month_article'=>0,
            'famous'=>0,
            'views'=>1198,
            'show'=>0,
            'date'=>'2015-05-01 05:27:02',
            'active'=>'1',
            'user'=>0,
            'created_at'=>'2020-05-15 01:20:06',
            'updated_at'=>'2020-06-21 00:08:23',
            'deleted_at'=>NULL
            ] );
            
            
                        
            Post::create( [
            'id'=>4010,
            'cat_id'=>12,
            'tag_id'=>7,
            'title'=>'Effect of a Retrievable Inferior Vena Cava Filter Plus Anticoagulation vs Anticoagulation Alone on Risk of Recurrent Pulmonary Embolism: A Randomized Clinical Trial',
            'text'=>'Patrick Mismetti and others\r\nJAMA. 201531316:1627-1635',
            'img'=>'file-1463591657-K8BRDF.jpg',
            'file'=>NULL,
            'link'=>'http://jama.jamanetwork.com/article.aspx?articleid=2279714',
            'by'=>0,
            'journal'=>'Site Admin',
            'type'=>0,
            'month_article'=>0,
            'famous'=>0,
            'views'=>1212,
            'show'=>0,
            'date'=>'2015-04-01 05:32:40',
            'active'=>'1',
            'user'=>0,
            'created_at'=>'2020-05-15 01:20:06',
            'updated_at'=>'2020-06-21 00:08:23',
            'deleted_at'=>NULL
            ] );
            
            
                        
            Post::create( [
            'id'=>4011,
            'cat_id'=>8,
            'tag_id'=>11,
            'title'=>'Impact of follow-up consultations for ICU survivors on post-ICU syndrome: a systematic review and meta-analysis',
            'text'=>'J. F. Jensen and others\r\nIntensive care Med, 415 763 - 775',
            'img'=>'file-1463591738-IZ3XFW.jpg',
            'file'=>NULL,
            'link'=>'http://icmjournal.esicm.org/journals/abstract.html?v=41&j=134&i=5&a=3689_10.1007_s00134-015-3689-1&doi=',
            'by'=>0,
            'journal'=>'Site Admin',
            'type'=>0,
            'month_article'=>0,
            'famous'=>0,
            'views'=>1101,
            'show'=>0,
            'date'=>'2015-05-01 05:42:19',
            'active'=>'1',
            'user'=>0,
            'created_at'=>'2020-05-15 01:20:06',
            'updated_at'=>'2020-06-21 00:08:23',
            'deleted_at'=>NULL
            ] );
            
            
                        
            Post::create( [
            'id'=>4012,
            'cat_id'=>8,
            'tag_id'=>9,
            'title'=>'Prevalence and outcome of gastrointestinal bleeding and use of acid suppressants in acutely ill adult intensive care patients',
            'text'=>'Mette Krag and others\r\nIntensive care Med, 415 833 - 845',
            'img'=>'file-1463591639-4YG8JY.jpg',
            'file'=>NULL,
            'link'=>'http://icmjournal.esicm.org/journals/abstract.html?v=41&j=134&i=5&a=3725_10.1007_s00134-015-3725-1&doi=',
            'by'=>0,
            'journal'=>'Site Admin',
            'type'=>0,
            'month_article'=>0,
            'famous'=>0,
            'views'=>1101,
            'show'=>0,
            'date'=>'2015-05-01 05:55:10',
            'active'=>'1',
            'user'=>0,
            'created_at'=>'2020-05-15 01:20:06',
            'updated_at'=>'2020-06-21 00:08:23',
            'deleted_at'=>NULL
            ] );
            
            
                        
            Post::create( [
            'id'=>4013,
            'cat_id'=>8,
            'tag_id'=>6,
            'title'=>'Effects of a recruitment maneuver on plasma levels of soluble RAGE in patients with diffuse acute respiratory distress syndrome: a prospective randomized crossover study',
            'text'=>'Matthieu Jabaudon and others\r\nIntensive care Med, 415 846 - 855',
            'img'=>'file-1463591732-SM39NY.jpg',
            'file'=>NULL,
            'link'=>'http://icmjournal.esicm.org/journals/abstract.html?v=41&j=134&i=5&a=3726_10.1007_s00134-015-3726-0&doi=',
            'by'=>0,
            'journal'=>'Site Admin',
            'type'=>0,
            'month_article'=>0,
            'famous'=>0,
            'views'=>1101,
            'show'=>0,
            'date'=>'2015-05-01 06:02:36',
            'active'=>'1',
            'user'=>0,
            'created_at'=>'2020-05-15 01:20:06',
            'updated_at'=>'2020-06-21 00:08:23',
            'deleted_at'=>NULL
            ] );
            
            
                        
            Post::create( [
            'id'=>4014,
            'cat_id'=>8,
            'tag_id'=>6,
            'title'=>'High-frequency oscillation ventilation for hypercapnic failure of conventional ventilation in pulmonary acute respiratory distress syndrome',
            'text'=>'Friesecke S, Stecher S, Abel P\r\nCritical Care 2015, 19 :201 1 May 2015',
            'img'=>'file-1463591738-IZ3XFW.jpg',
            'file'=>NULL,
            'link'=>'http://ccforum.com/content/19/1/201/abstract',
            'by'=>0,
            'journal'=>'Site Admin',
            'type'=>0,
            'month_article'=>0,
            'famous'=>0,
            'views'=>1101,
            'show'=>0,
            'date'=>'2015-05-02 17:17:36',
            'active'=>'1',
            'user'=>0,
            'created_at'=>'2020-05-15 01:20:06',
            'updated_at'=>'2020-06-21 00:08:23',
            'deleted_at'=>NULL
            ] );
            
            
                        
            Post::create( [
            'id'=>4015,
            'cat_id'=>8,
            'tag_id'=>9,
            'title'=>'The abdominal compartment syndrome: evolving concepts and future directions',
            'text'=>'De Waele JJ, Malbrain M, Kirkpatrick AW\r\nCritical Care 2015, 19 :211 6 May 2015',
            'img'=>'file-1463591725-9J88SF.jpg',
            'file'=>NULL,
            'link'=>'http://ccforum.com/content/19/1/211',
            'by'=>0,
            'journal'=>'Site Admin',
            'type'=>0,
            'month_article'=>0,
            'famous'=>0,
            'views'=>1101,
            'show'=>0,
            'date'=>'2015-05-06 17:17:28',
            'active'=>'1',
            'user'=>0,
            'created_at'=>'2020-05-15 01:20:06',
            'updated_at'=>'2020-06-21 00:08:23',
            'deleted_at'=>NULL
            ] );
            
            
                        
            Post::create( [
            'id'=>4016,
            'cat_id'=>8,
            'tag_id'=>10,
            'title'=>'Restrictive and liberal red cell transfusion strategies in adult patients: reconciling clinical data with best practice',
            'text'=>'Mirski MA, Frank SM, Kor DJ, Vincent J, Holmes DR\r\nCritical Care 2015, 19 :202 5 May 2015',
            'img'=>'file-1463591681-AGRM7H.jpg',
            'file'=>NULL,
            'link'=>'http://ccforum.com/content/19/1/202',
            'by'=>0,
            'journal'=>'Site Admin',
            'type'=>0,
            'month_article'=>0,
            'famous'=>0,
            'views'=>1101,
            'show'=>0,
            'date'=>'2015-05-06 17:17:17',
            'active'=>'1',
            'user'=>0,
            'created_at'=>'2020-05-15 01:20:06',
            'updated_at'=>'2020-06-21 00:08:23',
            'deleted_at'=>NULL
            ] );
            
            
                        
            Post::create( [
            'id'=>4017,
            'cat_id'=>9,
            'tag_id'=>6,
            'title'=>'Extracorprial Membrane Oxygenation ECMO for Refractory ARDS',
            'text'=>'Alain Combs\r\nCritical Care Canada.com',
            'img'=>'file-1463591685-DDLMK9.jpg',
            'file'=>NULL,
            'link'=>'http://www.criticalcarecanada.com/presentations/2012/vv-ecmo_for_refractory_ards.pdf',
            'by'=>0,
            'journal'=>'Site Admin',
            'type'=>0,
            'month_article'=>0,
            'famous'=>0,
            'views'=>1101,
            'show'=>0,
            'date'=>'2015-01-01 19:37:36',
            'active'=>'1',
            'user'=>0,
            'created_at'=>'2020-05-15 01:20:06',
            'updated_at'=>'2020-06-21 00:08:23',
            'deleted_at'=>NULL
            ] );
            
            
                        
            Post::create( [
            'id'=>4018,
            'cat_id'=>8,
            'tag_id'=>5,
            'title'=>'Simvastatin in aneurysmal subarachnoid haemorrhage STASH: a multicentre randomised phase 3 trial',
            'text'=>'Peter J Kirkpatrick and others, for STASH\r\nThe Lancet, May 2014',
            'img'=>'file-1463591752-XK911T.jpg',
            'file'=>NULL,
            'link'=>'http://www.thelancet.com/journals/laneur/article/PIIS1474-4422%2814%2970084-5/abstract',
            'by'=>0,
            'journal'=>'Site Admin',
            'type'=>0,
            'month_article'=>0,
            'famous'=>1,
            'views'=>1209,
            'show'=>0,
            'date'=>'2014-05-01 17:29:29',
            'active'=>'1',
            'user'=>0,
            'created_at'=>'2020-05-15 01:20:06',
            'updated_at'=>'2020-06-21 00:08:23',
            'deleted_at'=>NULL
            ] );
            
            
                        
            Post::create( [
            'id'=>4019,
            'cat_id'=>9,
            'tag_id'=>5,
            'title'=>'European Stroke Organization Guidelines for the Management of Intracranial Aneurysms and Subarachnoid Haemorrhage',
            'text'=>'Thorsten Steiner and others. \r\nCerebrovasc Dis 201335:93â€“112',
            'img'=>'file-1463591685-DDLMK9.jpg',
            'file'=>NULL,
            'link'=>'http://www.congrex-switzerland.com/fileadmin/files/2013/eso-stroke/pdf/ESO_SAH_Guideline_CVD.pdf',
            'by'=>0,
            'journal'=>'Site Admin',
            'type'=>0,
            'month_article'=>0,
            'famous'=>0,
            'views'=>1114,
            'show'=>0,
            'date'=>'2013-12-04 19:09:41',
            'active'=>'1',
            'user'=>0,
            'created_at'=>'2020-05-15 01:20:06',
            'updated_at'=>'2020-06-21 00:08:23',
            'deleted_at'=>NULL
            ] );
            
            
                        
            Post::create( [
            'id'=>4020,
            'cat_id'=>8,
            'tag_id'=>1,
            'title'=>'Effect of eritoran, an antagonist of MD2-TLR4, on mortality in patients with severe sepsis: the ACCESS randomized trial',
            'text'=>'Opal SM, and others\r\nJAMA. 2013 Mar 2030911:1154-62',
            'img'=>'file-1463591657-K8BRDF.jpg',
            'file'=>NULL,
            'link'=>'http://www.ncbi.nlm.nih.gov/pubmed/23512062',
            'by'=>0,
            'journal'=>'Site Admin',
            'type'=>0,
            'month_article'=>0,
            'famous'=>0,
            'views'=>1130,
            'show'=>0,
            'date'=>'2013-02-28 21:45:37',
            'active'=>'1',
            'user'=>0,
            'created_at'=>'2020-05-15 01:20:06',
            'updated_at'=>'2020-06-21 00:08:23',
            'deleted_at'=>NULL
            ] );
            
            
                        
            Post::create( [
            'id'=>4021,
            'cat_id'=>8,
            'tag_id'=>1,
            'title'=>'Effect of eritoran, an antagonist of MD2-TLR4, on mortality in patients with severe sepsis: the ACCESS randomized trial',
            'text'=>'Opal SM, and others\r\nJAMA. 2013 Mar 2030911:1154-62',
            'img'=>'file-1463591725-9J88SF.jpg',
            'file'=>NULL,
            'link'=>'http://www.ncbi.nlm.nih.gov/pubmed/23512062',
            'by'=>0,
            'journal'=>'Site Admin',
            'type'=>0,
            'month_article'=>0,
            'famous'=>1,
            'views'=>1225,
            'show'=>0,
            'date'=>'0000-00-00 00:00:00',
            'active'=>'1',
            'user'=>0,
            'created_at'=>'2020-05-15 01:20:06',
            'updated_at'=>'2020-06-21 00:08:23',
            'deleted_at'=>NULL
            ] );
            
            
                        
            Post::create( [
            'id'=>4022,
            'cat_id'=>10,
            'tag_id'=>4,
            'title'=>'Perioperative Goal-Directed Therapy Protocol Summary',
            'text'=>'Edwards Critical Care Education',
            'img'=>'file-1463591738-IZ3XFW.jpg',
            'file'=>NULL,
            'link'=>'http://www.edwards.com/_layouts/Edwards.moss.web.webapp/ESRPage/Resources/AR09235-PGDT_ProtocolSummary_Bro_1LRiP.pdf',
            'by'=>0,
            'journal'=>'Site Admin',
            'type'=>0,
            'month_article'=>0,
            'famous'=>0,
            'views'=>1198,
            'show'=>0,
            'date'=>'2014-04-30 22:52:02',
            'active'=>'1',
            'user'=>0,
            'created_at'=>'2020-05-15 01:20:06',
            'updated_at'=>'2020-06-21 00:08:23',
            'deleted_at'=>NULL
            ] );
            
            
                        
            Post::create( [
            'id'=>4023,
            'cat_id'=>8,
            'tag_id'=>10,
            'title'=>'Acute phase treatment of VTE: Anticoagulation, including non-vitamin K antagonist oral anticoagulants',
            'text'=>'Hillis CM, Crowther MA\r\nThromb Haemost. 2015 May 71136',
            'img'=>'file-1463591738-IZ3XFW.jpg',
            'file'=>NULL,
            'link'=>'http://www.ntkinstitute.org/news/content.nsf/PaperFrameSet?OpenForm&pp=1&id=DF5950353CA4263E85256FDB00692E0A&refid=4576&specid=106&newsid=18A5CE138CCD622085257E4300578705&locref=ntkwatch&u=http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&db=PubMed&dopt=Abstract&list_uids=25948149',
            'by'=>0,
            'journal'=>'Site Admin',
            'type'=>0,
            'month_article'=>0,
            'famous'=>0,
            'views'=>1101,
            'show'=>0,
            'date'=>'2015-05-01 05:38:11',
            'active'=>'1',
            'user'=>0,
            'created_at'=>'2020-05-15 01:20:06',
            'updated_at'=>'2020-06-21 00:08:23',
            'deleted_at'=>NULL
            ] );
            
            
                        
            Post::create( [
            'id'=>4024,
            'cat_id'=>8,
            'tag_id'=>10,
            'title'=>'Oral Apixaban for the Treatment of Acute Venous Thromboembolism',
            'text'=>'Giancarlo Agnelli and others\r\nN Engl J Med 2013 369:799-808. August 29, 2013',
            'img'=>'file-1463591738-IZ3XFW.jpg',
            'file'=>NULL,
            'link'=>'http://www.nejm.org/doi/full/10.1056/NEJMoa1302507',
            'by'=>0,
            'journal'=>'Site Admin',
            'type'=>0,
            'month_article'=>0,
            'famous'=>1,
            'views'=>1196,
            'show'=>0,
            'date'=>'2013-08-29 05:40:53',
            'active'=>'1',
            'user'=>0,
            'created_at'=>'2020-05-15 01:20:06',
            'updated_at'=>'2020-06-21 00:08:23',
            'deleted_at'=>NULL
            ] );
            
            
                        
            Post::create( [
            'id'=>4025,
            'cat_id'=>8,
            'tag_id'=>10,
            'title'=>'European Commission Approves Eliquis apixaban for the Treatment of Deep Vein Thrombosis DVT and Pulmonary Embolism PE, and Prevention of Recurrent DVT and PE',
            'text'=>'Tuesday, July 29, 2014 7:00 am EDT',
            'img'=>'file-1463591746-GEE3E7.jpg',
            'file'=>NULL,
            'link'=>'http://news.bms.com/press-release/european-commission-approves-eliquis-apixaban-treatment-deep-vein-thrombosis-dvt-and-p',
            'by'=>0,
            'journal'=>'Site Admin',
            'type'=>0,
            'month_article'=>0,
            'famous'=>1,
            'views'=>1196,
            'show'=>0,
            'date'=>'2014-07-29 05:50:20',
            'active'=>'1',
            'user'=>0,
            'created_at'=>'2020-05-15 01:20:06',
            'updated_at'=>'2020-06-21 00:08:23',
            'deleted_at'=>NULL
            ] );
            
            
                        
            Post::create( [
            'id'=>4026,
            'cat_id'=>8,
            'tag_id'=>1,
            'title'=>'Lessons Learned: Critical Care Management of Patients With Ebola in the United States',
            'text'=>'Johnson, Daniel W and others\r\nCritical Care Medicine:\r\nJune 2015 - Volume 43 - Issue 6 - p 1157â€“1164',
            'img'=>'file-1463591639-4YG8JY.jpg',
            'file'=>NULL,
            'link'=>'http://journals.lww.com/ccmjournal/Abstract/2015/06000/Lessons_Learned___Critical_Care_Management_of.3.aspx',
            'by'=>0,
            'journal'=>'Site Admin',
            'type'=>0,
            'month_article'=>0,
            'famous'=>0,
            'views'=>1130,
            'show'=>0,
            'date'=>'2015-06-01 07:16:43',
            'active'=>'1',
            'user'=>0,
            'created_at'=>'2020-05-15 01:20:06',
            'updated_at'=>'2020-06-21 00:08:23',
            'deleted_at'=>NULL
            ] );
            
            
                        
            Post::create( [
            'id'=>4027,
            'cat_id'=>8,
            'tag_id'=>1,
            'title'=>'The Association Between Colonization With Carbapenemase-Producing Enterobacteriaceae and Overall ICU Mortality: An Observational Cohort Study',
            'text'=>'Dautzenberg, Mirjam J. D and others\r\nCritical Care Medicine:\r\nJune 2015 - Volume 43 - Issue 6 - p 1170â€“1177',
            'img'=>'file-1463591657-K8BRDF.jpg',
            'file'=>NULL,
            'link'=>'http://journals.lww.com/ccmjournal/Fulltext/2015/06000/The_Association_Between_Colonization_With.5.aspx',
            'by'=>0,
            'journal'=>'Site Admin',
            'type'=>0,
            'month_article'=>0,
            'famous'=>0,
            'views'=>1130,
            'show'=>0,
            'date'=>'2015-06-01 07:20:07',
            'active'=>'1',
            'user'=>0,
            'created_at'=>'2020-05-15 01:20:06',
            'updated_at'=>'2020-06-21 00:08:23',
            'deleted_at'=>NULL
            ] );
            
            
                        
            Post::create( [
            'id'=>4028,
            'cat_id'=>8,
            'tag_id'=>8,
            'title'=>'Association Between Colistin Dose and Development of Nephrotoxicity',
            'text'=>'Lee, Yu-Ji Wi and others\r\nCritical Care Medicine:\r\nJune 2015 - Volume 43 - Issue 6 - p 1187â€“1193',
            'img'=>'file-1463591738-IZ3XFW.jpg',
            'file'=>NULL,
            'link'=>'http://journals.lww.com/ccmjournal/Abstract/2015/06000/Association_Between_Colistin_Dose_and_Development.7.aspx',
            'by'=>0,
            'journal'=>'Site Admin',
            'type'=>0,
            'month_article'=>0,
            'famous'=>0,
            'views'=>1152,
            'show'=>0,
            'date'=>'2015-06-01 07:22:51',
            'active'=>'1',
            'user'=>0,
            'created_at'=>'2020-05-15 01:20:06',
            'updated_at'=>'2020-06-21 00:08:23',
            'deleted_at'=>NULL
            ] );
            
            
                        
            Post::create( [
            'id'=>4029,
            'cat_id'=>12,
            'tag_id'=>1,
            'title'=>'Excess Mortality Associated With Colistin-Tigecycline Compared With Colistin-Carbapenem Combination Therapy for Extensively Drug-Resistant Acinetobacter baumannii Bacteremia: A Multicenter Prospective Observational Study',
            'text'=>'Cheng Aristine  and others\r\nCritical Care Medicine:\r\nJune 2015 - Volume 43 - Issue 6 - p 1194â€“1204',
            'img'=>'file-1463591657-K8BRDF.jpg',
            'file'=>NULL,
            'link'=>'http://journals.lww.com/ccmjournal/Abstract/2015/06000/Excess_Mortality_Associated_With.8.aspx',
            'by'=>0,
            'journal'=>'Site Admin',
            'type'=>0,
            'month_article'=>0,
            'famous'=>0,
            'views'=>1241,
            'show'=>0,
            'date'=>'2015-06-01 07:28:41',
            'active'=>'1',
            'user'=>0,
            'created_at'=>'2020-05-15 01:20:06',
            'updated_at'=>'2020-06-21 00:08:23',
            'deleted_at'=>NULL
            ] );
            
            
                        
            Post::create( [
            'id'=>4030,
            'cat_id'=>8,
            'tag_id'=>2,
            'title'=>'Stress Disorders Following Prolonged Critical Illness in Survivors of Severe Sepsis',
            'text'=>'Wintermann, Gloria-Beatrice and others\r\nCritical Care Medicine:\r\nJune 2015 - Volume 43 - Issue 6 - p 1213â€“1222',
            'img'=>'file-1463591746-GEE3E7.jpg',
            'file'=>NULL,
            'link'=>'http://journals.lww.com/ccmjournal/Abstract/2015/06000/Stress_Disorders_Following_Prolonged_Critical.10.aspx',
            'by'=>0,
            'journal'=>'Site Admin',
            'type'=>0,
            'month_article'=>0,
            'famous'=>0,
            'views'=>1172,
            'show'=>0,
            'date'=>'2015-06-01 07:30:55',
            'active'=>'1',
            'user'=>0,
            'created_at'=>'2020-05-15 01:20:06',
            'updated_at'=>'2020-06-21 00:08:23',
            'deleted_at'=>NULL
            ] );
            
            
                        
            Post::create( [
            'id'=>4031,
            'cat_id'=>12,
            'tag_id'=>1,
            'title'=>'Intraosseous Versus Central Venous Catheter Utilization and Performance During Inpatient Medical Emergencies',
            'text'=>'Lee, Peter M. J. and others\r\nCritical Care Medicine:\r\nJune 2015 - Volume 43 - Issue 6 - p 1233â€“1238',
            'img'=>'file-1463591644-ZGN2DA.jpg',
            'file'=>NULL,
            'link'=>'http://journals.lww.com/ccmjournal/Abstract/2015/06000/Intraosseous_Versus_Central_Venous_Catheter.12.aspx',
            'by'=>0,
            'journal'=>'Site Admin',
            'type'=>0,
            'month_article'=>0,
            'famous'=>0,
            'views'=>1241,
            'show'=>0,
            'date'=>'2015-06-01 07:33:51',
            'active'=>'1',
            'user'=>0,
            'created_at'=>'2020-05-15 01:20:06',
            'updated_at'=>'2020-06-21 00:08:23',
            'deleted_at'=>NULL
            ] );
            
            
                        
            Post::create( [
            'id'=>4032,
            'cat_id'=>8,
            'tag_id'=>4,
            'title'=>'Perioperative goal directed therapy and postoperative outcomes in patients undergoing high-risk abdominal surgery: a historical-prospective, comparative effectiveness study',
            'text'=>'Cannesson M, Ramsingh D, Rinehart J, Demirjian A, Vu T, Vakharia S, Imagawa D, Yu Z, Greenfield S, Kain Z\r\nCritical Care 2015, 19 :261 19 June 2015',
            'img'=>'file-1463591657-K8BRDF.jpg',
            'file'=>NULL,
            'link'=>'http://www.ccforum.com/content/19/1/261/abstract',
            'by'=>0,
            'journal'=>'Site Admin',
            'type'=>0,
            'month_article'=>0,
            'famous'=>0,
            'views'=>1198,
            'show'=>0,
            'date'=>'2015-06-19 11:46:53',
            'active'=>'1',
            'user'=>0,
            'created_at'=>'2020-05-15 01:20:06',
            'updated_at'=>'2020-06-21 00:08:23',
            'deleted_at'=>NULL
            ] );
            
            
                        
            Post::create( [
            'id'=>4033,
            'cat_id'=>8,
            'tag_id'=>3,
            'title'=>'Permissive Underfeeding or Standard Enteral Feeding in Critically Ill Adults',
            'text'=>'Y.M. Arabi and Others\r\nN Engl J Med 2015372:2398-2408',
            'img'=>'file-1463591644-ZGN2DA.jpg',
            'file'=>NULL,
            'link'=>'http://www.nejm.org/doi/full/10.1056/NEJMoa1502826?query=TOC',
            'by'=>0,
            'journal'=>'Site Admin',
            'type'=>0,
            'month_article'=>0,
            'famous'=>0,
            'views'=>1101,
            'show'=>0,
            'date'=>'2015-06-18 11:52:29',
            'active'=>'1',
            'user'=>0,
            'created_at'=>'2020-05-15 01:20:06',
            'updated_at'=>'2020-06-21 00:08:23',
            'deleted_at'=>NULL
            ] );
            
            
                        
            Post::create( [
            'id'=>4034,
            'cat_id'=>8,
            'tag_id'=>7,
            'title'=>'Rapid response systems: a systematic review and meta-analysis',
            'text'=>'Maharaj R, Raffaele I, Wendon J\r\nCritical Care 2015, 19 :254 12 June 2015',
            'img'=>'file-1463591752-XK911T.jpg',
            'file'=>NULL,
            'link'=>'http://www.ccforum.com/content/pdf/s13054-015-0973-y.pdf',
            'by'=>0,
            'journal'=>'Site Admin',
            'type'=>0,
            'month_article'=>0,
            'famous'=>0,
            'views'=>1101,
            'show'=>0,
            'date'=>'2015-06-12 12:02:06',
            'active'=>'1',
            'user'=>0,
            'created_at'=>'2020-05-15 01:20:06',
            'updated_at'=>'2020-06-21 00:08:23',
            'deleted_at'=>NULL
            ] );
            
            
                        
            Post::create( [
            'id'=>4035,
            'cat_id'=>8,
            'tag_id'=>8,
            'title'=>'Effects of fluid administration on renal perfusion in critically ill patients',
            'text'=>'Moussa M, Scolletta S, Fagnoul D, Pasquier P, Brasseur A, Taccone F, Vincent J, De Backer D\r\nCritical Care 2015, 19 :250 12 June 2015',
            'img'=>'file-1463591681-AGRM7H.jpg',
            'file'=>NULL,
            'link'=>'http://www.ccforum.com/content/pdf/s13054-015-0963-0.pdf',
            'by'=>0,
            'journal'=>'Site Admin',
            'type'=>0,
            'month_article'=>0,
            'famous'=>0,
            'views'=>1152,
            'show'=>0,
            'date'=>'2015-06-12 12:03:03',
            'active'=>'1',
            'user'=>0,
            'created_at'=>'2020-05-15 01:20:06',
            'updated_at'=>'2020-06-21 00:08:23',
            'deleted_at'=>NULL
            ] );
            
            
                        
            Post::create( [
            'id'=>4036,
            'cat_id'=>8,
            'tag_id'=>4,
            'title'=>'Burn wound healing and treatment: review and advancements',
            'text'=>'Rowan M, Cancio L, Elster E, Burmeister D, Rose L, Natesan S, Chan R, Christy R, Chung K\r\nCritical Care 2015, 19 :243 12 June 2015',
            'img'=>'file-1463591650-J14MMU.jpg',
            'file'=>NULL,
            'link'=>'http://www.ccforum.com/content/19/1/243',
            'by'=>0,
            'journal'=>'Site Admin',
            'type'=>0,
            'month_article'=>0,
            'famous'=>0,
            'views'=>1198,
            'show'=>0,
            'date'=>'2015-06-12 12:04:16',
            'active'=>'1',
            'user'=>0,
            'created_at'=>'2020-05-15 01:20:06',
            'updated_at'=>'2020-06-21 00:08:23',
            'deleted_at'=>NULL
            ] );
            
            
                        
            Post::create( [
            'id'=>4037,
            'cat_id'=>8,
            'tag_id'=>7,
            'title'=>'Incidence of and risk factors for severe cardiovascular collapse after endotracheal intubation in the ICU: a multicenter observational study',
            'text'=>'Perbet S, De Jong A, Delmas J, Futier E, Pereira B, Jaber S, Constantin J\r\nCritical Care 2015, 19 :257 18 June 2015',
            'img'=>'file-1463591732-SM39NY.jpg',
            'file'=>NULL,
            'link'=>'http://www.ccforum.com/content/pdf/s13054-015-0975-9.pdf',
            'by'=>0,
            'journal'=>'Site Admin',
            'type'=>0,
            'month_article'=>0,
            'famous'=>0,
            'views'=>1101,
            'show'=>0,
            'date'=>'2015-06-18 12:10:59',
            'active'=>'1',
            'user'=>0,
            'created_at'=>'2020-05-15 01:20:06',
            'updated_at'=>'2020-06-21 00:08:23',
            'deleted_at'=>NULL
            ] );
            
            
                        
            Post::create( [
            'id'=>4038,
            'cat_id'=>8,
            'tag_id'=>7,
            'title'=>'Early dynamic left intraventricular obstruction is associated with hypovolemia and high mortality in septic shock patients',
            'text'=>'Chauvet J, El-Dash S, Delastre O, Bouffandeau B, Jusserand D, Michot J, Bauer F, Maizel J, Slama M\r\nCritical Care 2015, 19 :262 17 June 2015',
            'img'=>'file-1463591685-DDLMK9.jpg',
            'file'=>NULL,
            'link'=>'http://www.ccforum.com/content/pdf/s13054-015-0975-9.pdf',
            'by'=>0,
            'journal'=>'Site Admin',
            'type'=>0,
            'month_article'=>0,
            'famous'=>0,
            'views'=>1101,
            'show'=>0,
            'date'=>'2015-06-17 12:12:27',
            'active'=>'1',
            'user'=>0,
            'created_at'=>'2020-05-15 01:20:06',
            'updated_at'=>'2020-06-21 00:08:23',
            'deleted_at'=>NULL
            ] );
            
            
                        
            Post::create( [
            'id'=>4039,
            'cat_id'=>8,
            'tag_id'=>6,
            'title'=>'Neural versus pneumatic control of pressure support in patients with chronic obstructive pulmonary diseases at different levels of positive end expiratory pressure: a physiological study',
            'text'=>'Liu L, Xia F, Yang Y, Longhini F, Navalesi P, Beck J, Sinderby C, Qiu H\r\nCritical Care 2015, 19 :244 9 June 2015',
            'img'=>'file-1463591639-4YG8JY.jpg',
            'file'=>NULL,
            'link'=>'http://www.ccforum.com/content/pdf/s13054-015-0971-0.pdf',
            'by'=>0,
            'journal'=>'Site Admin',
            'type'=>0,
            'month_article'=>0,
            'famous'=>0,
            'views'=>1101,
            'show'=>0,
            'date'=>'2015-06-09 12:14:37',
            'active'=>'1',
            'user'=>0,
            'created_at'=>'2020-05-15 01:20:06',
            'updated_at'=>'2020-06-21 00:08:23',
            'deleted_at'=>NULL
            ] );
            
            
                        
            Post::create( [
            'id'=>4040,
            'cat_id'=>8,
            'tag_id'=>11,
            'title'=>'FDA Approves Another Weekly Injectable Drug for Type 2 Diabetes',
            'text'=>'Serena Gordon\r\nWebMD News from HealthDay',
            'img'=>'file-1463591685-DDLMK9.jpg',
            'file'=>NULL,
            'link'=>'http://www.webmd.com/diabetes/news/20140919/fda-approves-another-weekly-injectable-drug-for-type-2-diabetes',
            'by'=>0,
            'journal'=>'Site Admin',
            'type'=>0,
            'month_article'=>0,
            'famous'=>0,
            'views'=>1101,
            'show'=>0,
            'date'=>'2014-09-19 20:07:28',
            'active'=>'1',
            'user'=>0,
            'created_at'=>'2020-05-15 01:20:06',
            'updated_at'=>'2020-06-21 00:08:23',
            'deleted_at'=>NULL
            ] );
            
            
                        
            Post::create( [
            'id'=>4041,
            'cat_id'=>8,
            'tag_id'=>11,
            'title'=>'Selenide Targets to Reperfusing Tissue and Protects It From Injury',
            'text'=>'Iwata, Akiko and others\r\nCritical Care Medicine:\r\nJuly 2015 - Volume 43 - Issue 7 - p 1361â€“1367',
            'img'=>'file-1463591725-9J88SF.jpg',
            'file'=>NULL,
            'link'=>'http://journals.lww.com/ccmjournal/Abstract/2015/07000/Selenide_Targets_to_Reperfusing_Tissue_and.3.aspx',
            'by'=>0,
            'journal'=>'Site Admin',
            'type'=>0,
            'month_article'=>0,
            'famous'=>0,
            'views'=>1101,
            'show'=>0,
            'date'=>'2015-07-01 10:59:40',
            'active'=>'1',
            'user'=>0,
            'created_at'=>'2020-05-15 01:20:06',
            'updated_at'=>'2020-06-21 00:08:23',
            'deleted_at'=>NULL
            ] );
            
            
                        
            Post::create( [
            'id'=>4042,
            'cat_id'=>8,
            'tag_id'=>6,
            'title'=>'Comparative Effectiveness of Noninvasive and Invasive Ventilation in Critically Ill Patients With Acute Exacerbation of Chronic Obstructive Pulmonary Disease',
            'text'=>'Stefan, Mihaela S and others\r\nCritical Care Medicine:\r\nJuly 2015 - Volume 43 - Issue 7 - p 1386â€“1394',
            'img'=>'file-1463591657-K8BRDF.jpg',
            'file'=>NULL,
            'link'=>'http://journals.lww.com/ccmjournal/Abstract/2015/07000/Comparative_Effectiveness_of_Noninvasive_and.6.aspx',
            'by'=>0,
            'journal'=>'Site Admin',
            'type'=>0,
            'month_article'=>0,
            'famous'=>0,
            'views'=>1101,
            'show'=>0,
            'date'=>'2015-07-01 10:59:35',
            'active'=>'1',
            'user'=>0,
            'created_at'=>'2020-05-15 01:20:06',
            'updated_at'=>'2020-06-21 00:08:23',
            'deleted_at'=>NULL
            ] );
            
            
                        
            Post::create( [
            'id'=>4043,
            'cat_id'=>8,
            'tag_id'=>5,
            'title'=>'Effects and Clinical Characteristics of Intracranial Pressure Monitoringâ€“Targeted Management for Subsets of Traumatic Brain Injury: An Observational Multicenter Study',
            'text'=>'Yuan, Qiang and others\r\nCritical Care Medicine:\r\nJuly 2015 - Volume 43 - Issue 7 - p 1405â€“1414',
            'img'=>'file-1463591732-SM39NY.jpg',
            'file'=>NULL,
            'link'=>'http://journals.lww.com/ccmjournal/Abstract/2015/07000/Effects_and_Clinical_Characteristics_of.8.aspx',
            'by'=>0,
            'journal'=>'Site Admin',
            'type'=>0,
            'month_article'=>0,
            'famous'=>0,
            'views'=>1114,
            'show'=>0,
            'date'=>'0000-00-00 00:00:00',
            'active'=>'1',
            'user'=>0,
            'created_at'=>'2020-05-15 01:20:06',
            'updated_at'=>'2020-06-21 00:08:23',
            'deleted_at'=>NULL
            ] );
            
            
                        
            Post::create( [
            'id'=>4044,
            'cat_id'=>8,
            'tag_id'=>5,
            'title'=>'Hypothermia for Traumatic Brain Injury in Childrenâ€”A Phase II Randomized Controlled Trial',
            'text'=>'Beca, John and others\r\nCritical Care Medicine:\r\nJuly 2015 - Volume 43 - Issue 7 - p 1458â€“1466',
            'img'=>'file-1463591681-AGRM7H.jpg',
            'file'=>NULL,
            'link'=>'http://journals.lww.com/ccmjournal/Abstract/2015/07000/Hypothermia_for_Traumatic_Brain_Injury_in.14.aspx',
            'by'=>0,
            'journal'=>'Site Admin',
            'type'=>0,
            'month_article'=>0,
            'famous'=>0,
            'views'=>1114,
            'show'=>0,
            'date'=>'2015-07-01 10:59:17',
            'active'=>'1',
            'user'=>0,
            'created_at'=>'2020-05-15 01:20:06',
            'updated_at'=>'2020-06-21 00:08:23',
            'deleted_at'=>NULL
            ] );
            
            
                        
            Post::create( [
            'id'=>4045,
            'cat_id'=>8,
            'tag_id'=>4,
            'title'=>'Postoperative Critical Care of the Adult Cardiac Surgical Patient. Part I: Routine Postoperative Care',
            'text'=>'Stephens, R. Scott Whitman, Glenn J. R.\r\nCritical Care Medicine:\r\nJuly 2015 - Volume 43 - Issue 7 - p 1477â€“1497',
            'img'=>'file-1463591681-AGRM7H.jpg',
            'file'=>NULL,
            'link'=>'http://journals.lww.com/ccmjournal/Abstract/2015/07000/Postoperative_Critical_Care_of_the_Adult_Cardiac.16.aspx',
            'by'=>0,
            'journal'=>'Site Admin',
            'type'=>0,
            'month_article'=>0,
            'famous'=>0,
            'views'=>1198,
            'show'=>0,
            'date'=>'2015-07-01 10:59:12',
            'active'=>'1',
            'user'=>0,
            'created_at'=>'2020-05-15 01:20:06',
            'updated_at'=>'2020-06-21 00:08:23',
            'deleted_at'=>NULL
            ] );
            
            
                        
            Post::create( [
            'id'=>4046,
            'cat_id'=>12,
            'tag_id'=>1,
            'title'=>'Ultrasound-Guided Subclavian Vein Catheterization: A Systematic Review and Meta-Analysis',
            'text'=>'Lalu, Manoj M and others\r\nCritical Care Medicine. July 2015:4371498â€“1507',
            'img'=>'file-1463591657-K8BRDF.jpg',
            'file'=>NULL,
            'link'=>'http://journals.lww.com/ccmjournal/Abstract/2015/07000/Ultrasound_Guided_Subclavian_Vein_Catheterization_.17.aspx',
            'by'=>0,
            'journal'=>'Site Admin',
            'type'=>0,
            'month_article'=>0,
            'famous'=>0,
            'views'=>1241,
            'show'=>0,
            'date'=>'2015-07-01 09:53:40',
            'active'=>'1',
            'user'=>0,
            'created_at'=>'2020-05-15 01:20:06',
            'updated_at'=>'2020-06-21 00:08:23',
            'deleted_at'=>NULL
            ] );
            
            
                        
            Post::create( [
            'id'=>4047,
            'cat_id'=>8,
            'tag_id'=>6,
            'title'=>'Association Between Arterial Hyperoxia and Outcome in Subsets of Critical Illness: A Systematic Review, Meta-Analysis, and Meta-Regression of Cohort Studies',
            'text'=>'Helmerhorst, Hendrik J. F. and others\r\nCritical Care Medicine:\r\nJuly 2015 - Volume 43 - Issue 7 - p 1508â€“1519',
            'img'=>'file-1463591725-9J88SF.jpg',
            'file'=>NULL,
            'link'=>'http://journals.lww.com/ccmjournal/Abstract/2015/07000/Association_Between_Arterial_Hyperoxia_and_Outcome.18.aspx',
            'by'=>0,
            'journal'=>'Site Admin',
            'type'=>0,
            'month_article'=>0,
            'famous'=>0,
            'views'=>1101,
            'show'=>0,
            'date'=>'2015-07-01 10:59:08',
            'active'=>'1',
            'user'=>0,
            'created_at'=>'2020-05-15 01:20:06',
            'updated_at'=>'2020-06-21 00:08:23',
            'deleted_at'=>NULL
            ] );
            
            
                        
            Post::create( [
            'id'=>4048,
            'cat_id'=>12,
            'tag_id'=>4,
            'title'=>'Injectable hemostatic adjuncts\r\nFIinTIC-Study\r\nFibrinogen in Trauma Induced Coagulopathy',
            'text'=>'Marc Maegele\r\nDepartment for Trauma and Orthopedic Surgery\r\nCologne-Merheim Medical Center CMMC\r\nInstitute for Research in Operative Medicine\r\nIFOM',
            'img'=>'file-1463591639-4YG8JY.jpg',
            'file'=>NULL,
            'link'=>'http://rdcr.org/rdcrpresentations/2014-maegele-injectable-hemostatic-ajuncts.pdf',
            'by'=>0,
            'journal'=>'Site Admin',
            'type'=>0,
            'month_article'=>0,
            'famous'=>0,
            'views'=>1309,
            'show'=>0,
            'date'=>'2015-07-01 10:54:05',
            'active'=>'1',
            'user'=>0,
            'created_at'=>'2020-05-15 01:20:06',
            'updated_at'=>'2020-06-21 00:08:23',
            'deleted_at'=>NULL
            ] );
            
            
                        
            Post::create( [
            'id'=>4049,
            'cat_id'=>8,
            'tag_id'=>4,
            'title'=>'Remote Damage Control Resuscitation Symposium',
            'text'=>'Journal of Trauma and Acute Care Surgery:June 2015 - Volume 78 - Supplement 1, pp: S1-S86',
            'img'=>'file-1463591681-AGRM7H.jpg',
            'file'=>NULL,
            'link'=>'http://journals.lww.com/jtrauma/toc/2015/06001',
            'by'=>0,
            'journal'=>'Site Admin',
            'type'=>0,
            'month_article'=>0,
            'famous'=>0,
            'views'=>1198,
            'show'=>0,
            'date'=>'2015-07-01 10:58:55',
            'active'=>'1',
            'user'=>0,
            'created_at'=>'2020-05-15 01:20:06',
            'updated_at'=>'2020-06-21 00:08:23',
            'deleted_at'=>NULL
            ] );
            
            
                        
            Post::create( [
            'id'=>4050,
            'cat_id'=>9,
            'tag_id'=>4,
            'title'=>'An evidence-based approach to patient selection for emergency department thoracotomy: A practice management guideline from the Eastern Association for the Surgery of Trauma',
            'text'=>'Seamon, Mark J. and others\r\nJournal of Trauma and Acute Care Surgery:\r\nJuly 2015 - Volume 79 - Issue 1 - p 159â€“173',
            'img'=>'file-1463591650-J14MMU.jpg',
            'file'=>NULL,
            'link'=>'http://journals.lww.com/jtrauma/Fulltext/2015/07000/An_evidence_based_approach_to_patient_selection.24.aspx',
            'by'=>0,
            'journal'=>'Site Admin',
            'type'=>0,
            'month_article'=>0,
            'famous'=>0,
            'views'=>1198,
            'show'=>0,
            'date'=>'2015-07-01 11:06:49',
            'active'=>'1',
            'user'=>0,
            'created_at'=>'2020-05-15 01:20:06',
            'updated_at'=>'2020-06-21 00:08:23',
            'deleted_at'=>NULL
            ] );
            
            
                        
            Post::create( [
            'id'=>4051,
            'cat_id'=>9,
            'tag_id'=>4,
            'title'=>'REBOA and catheter-based technology in trauma',
            'text'=>'Brenner, Megan MD\r\nJournal of Trauma and Acute Care Surgery:\r\nJuly 2015 - Volume 79 - Issue 1 - p 174â€“175',
            'img'=>'file-1463591725-9J88SF.jpg',
            'file'=>NULL,
            'link'=>'http://journals.lww.com/jtrauma/Citation/2015/07000/REBOA_and_catheter_based_technology_in_trauma.25.aspx',
            'by'=>0,
            'journal'=>'Site Admin',
            'type'=>0,
            'month_article'=>0,
            'famous'=>0,
            'views'=>1198,
            'show'=>0,
            'date'=>'2015-07-01 11:13:23',
            'active'=>'1',
            'user'=>0,
            'created_at'=>'2020-05-15 01:20:06',
            'updated_at'=>'2020-06-21 00:08:23',
            'deleted_at'=>NULL
            ] );
            
            
                        
            Post::create( [
            'id'=>4052,
            'cat_id'=>8,
            'tag_id'=>5,
            'title'=>'Enoxaparin ameliorates postâ€“traumatic brain injury edema and neurologic recovery, reducing cerebral leukocyte endothelial interactions and vessel permeability in vivo',
            'text'=>'Li, Shengjie and others\r\nJournal of Trauma and Acute Care Surgery:\r\nJuly 2015 - Volume 79 - Issue 1 - p 78â€“84',
            'img'=>'file-1463591746-GEE3E7.jpg',
            'file'=>NULL,
            'link'=>'http://journals.lww.com/jtrauma/Abstract/2015/07000/Enoxaparin_ameliorates_post_traumatic_brain_injury.12.aspx',
            'by'=>0,
            'journal'=>'Site Admin',
            'type'=>0,
            'month_article'=>0,
            'famous'=>0,
            'views'=>1114,
            'show'=>0,
            'date'=>'2015-07-01 12:01:13',
            'active'=>'1',
            'user'=>0,
            'created_at'=>'2020-05-15 01:20:06',
            'updated_at'=>'2020-06-21 00:08:23',
            'deleted_at'=>NULL
            ] );
            
            
                        
            Post::create( [
            'id'=>4053,
            'cat_id'=>9,
            'tag_id'=>6,
            'title'=>'Guidelines for Percutaneous Dilatational\r\nTracheostomy PDT from the Danish Society of\r\nIntensive Care Medicine DSIT and the Danish\r\nSociety of Anesthesiology and Intensive Care\r\nMedicine DASAIM',
            'text'=>'Kristian RÃ¸rbÃ¦k Madsen and others\r\nDANISH MEDICAL BULLETIN, Oct 14 2011',
            'img'=>'file-1463591752-XK911T.jpg',
            'file'=>NULL,
            'link'=>'http://www.danmedj.dk/portal/pls/portal/!PORTAL.wwpob_page.show?_docname=9104900.PDF',
            'by'=>0,
            'journal'=>'Site Admin',
            'type'=>0,
            'month_article'=>0,
            'famous'=>0,
            'views'=>1101,
            'show'=>0,
            'date'=>'2011-10-01 07:53:20',
            'active'=>'1',
            'user'=>0,
            'created_at'=>'2020-05-15 01:20:06',
            'updated_at'=>'2020-06-21 00:08:23',
            'deleted_at'=>NULL
            ] );
            
            
                        
            Post::create( [
            'id'=>4054,
            'cat_id'=>8,
            'tag_id'=>1,
            'title'=>'Reviving old antibiotics',
            'text'=>'Ursula Theuretzbacher and others\r\nJ. Antimicrob. Chemother. 2015 70 8: 2177-2181',
            'img'=>'file-1463591746-GEE3E7.jpg',
            'file'=>NULL,
            'link'=>'http://jac.oxfordjournals.org/content/70/8/2177.full',
            'by'=>0,
            'journal'=>'Site Admin',
            'type'=>0,
            'month_article'=>0,
            'famous'=>0,
            'views'=>1130,
            'show'=>0,
            'date'=>'2015-07-14 08:13:31',
            'active'=>'1',
            'user'=>0,
            'created_at'=>'2020-05-15 01:20:06',
            'updated_at'=>'2020-06-21 00:08:23',
            'deleted_at'=>NULL
            ] );
            
            
                        
            Post::create( [
            'id'=>4055,
            'cat_id'=>8,
            'tag_id'=>1,
            'title'=>'Candida biomarkers in patients with candidaemia and bacteraemia',
            'text'=>'Ursula Theuretzbacher and others\r\nJ. Antimicrob. Chemother. 2015 70 8: 2354-2361.',
            'img'=>'file-1463591650-J14MMU.jpg',
            'file'=>NULL,
            'link'=>'http://jac.oxfordjournals.org/content/70/8/2354.abstract?etoc',
            'by'=>0,
            'journal'=>'Site Admin',
            'type'=>0,
            'month_article'=>0,
            'famous'=>0,
            'views'=>1130,
            'show'=>0,
            'date'=>'2015-07-14 08:18:03',
            'active'=>'1',
            'user'=>0,
            'created_at'=>'2020-05-15 01:20:06',
            'updated_at'=>'2020-06-21 00:08:23',
            'deleted_at'=>NULL
            ] );
            
            
                        
            Post::create( [
            'id'=>4056,
            'cat_id'=>12,
            'tag_id'=>1,
            'title'=>'Endogenous IgG hypogammaglobulinaemia in critically ill adults with sepsis: systematic review and meta-analysis',
            'text'=>'Manu Shankar-Hari and others\r\nIntensive Care Med. J. August 2015418: 1393 - 1401',
            'img'=>'file-1463591681-AGRM7H.jpg',
            'file'=>NULL,
            'link'=>'http://icmjournal.esicm.org/journals/abstract.html?v=41&j=134&i=8&a=3845_10.1007_s00134-015-3845-7&doi=',
            'by'=>0,
            'journal'=>'Site Admin',
            'type'=>0,
            'month_article'=>0,
            'famous'=>0,
            'views'=>1241,
            'show'=>0,
            'date'=>'2015-07-24 12:13:44',
            'active'=>'1',
            'user'=>0,
            'created_at'=>'2020-05-15 01:20:06',
            'updated_at'=>'2020-06-21 00:08:23',
            'deleted_at'=>NULL
            ] );
            
            
                        
            Post::create( [
            'id'=>4057,
            'cat_id'=>8,
            'tag_id'=>8,
            'title'=>'Epidemiology of acute kidney injury in critically ill patients: the multinational AKI-EPI study',
            'text'=>'1411 - 1423 and others\r\nIntensive Care Med. J. August 2015418: 1393 - 1401',
            'img'=>'file-1463591681-AGRM7H.jpg',
            'file'=>NULL,
            'link'=>'http://icmjournal.esicm.org/journals/abstract.html?v=41&j=134&i=8&a=3934_10.1007_s00134-015-3934-7&doi=',
            'by'=>0,
            'journal'=>'Site Admin',
            'type'=>0,
            'month_article'=>0,
            'famous'=>0,
            'views'=>1152,
            'show'=>0,
            'date'=>'2015-08-01 12:16:34',
            'active'=>'1',
            'user'=>0,
            'created_at'=>'2020-05-15 01:20:06',
            'updated_at'=>'2020-06-21 00:08:23',
            'deleted_at'=>NULL
            ] );
            
            
                        
            Post::create( [
            'id'=>4058,
            'cat_id'=>8,
            'tag_id'=>11,
            'title'=>'Ten major priorities for intensive care in India',
            'text'=>'J. V. Divatia, Shivakumar Iyer\r\nIntensive Care Med. J. August 2015418: 1468 - 1471',
            'img'=>'file-1463591738-IZ3XFW.jpg',
            'file'=>NULL,
            'link'=>'http://icmjournal.esicm.org/journals/abstract.html?v=41&j=134&i=8&a=3618_10.1007_s00134-014-3618-8&doi=',
            'by'=>0,
            'journal'=>'Site Admin',
            'type'=>0,
            'month_article'=>0,
            'famous'=>0,
            'views'=>1101,
            'show'=>0,
            'date'=>'2015-08-01 12:20:19',
            'active'=>'1',
            'user'=>0,
            'created_at'=>'2020-05-15 01:20:06',
            'updated_at'=>'2020-06-21 00:08:23',
            'deleted_at'=>NULL
            ] );
            
            
                        
            Post::create( [
            'id'=>4059,
            'cat_id'=>12,
            'tag_id'=>1,
            'title'=>'Î²-d-Glucan and Candida albicans germ tube antibody in ICU patients with invasive candidiasis',
            'text'=>'Estrella MartÃ­n-Mazuelos and others\r\nIntensive Care Med. J. August 2015418: 1424 - 1432',
            'img'=>'file-1463591685-DDLMK9.jpg',
            'file'=>NULL,
            'link'=>'http://icmjournal.esicm.org/journals/abstract.html?v=41&j=134&i=8&a=3922_10.1007_s00134-015-3922-y&doi=',
            'by'=>0,
            'journal'=>'Site Admin',
            'type'=>0,
            'month_article'=>0,
            'famous'=>1,
            'views'=>1336,
            'show'=>0,
            'date'=>'2015-07-24 12:26:39',
            'active'=>'1',
            'user'=>0,
            'created_at'=>'2020-05-15 01:20:06',
            'updated_at'=>'2020-06-21 00:08:23',
            'deleted_at'=>NULL
            ] );
            
            
                        
            Post::create( [
            'id'=>4060,
            'cat_id'=>8,
            'tag_id'=>1,
            'title'=>'Strategies to reduce curative antibiotic therapy in intensive care units adult and paediatric',
            'text'=>'CÃ©dric BretonniÃ¨re and others\r\nIntensive Care Med. J. August 2015417: 1181 - 1196',
            'img'=>'file-1463591746-GEE3E7.jpg',
            'file'=>NULL,
            'link'=>'http://icmjournal.esicm.org/journals/abstract.html?v=41&j=134&i=7&a=3853_10.1007_s00134-015-3853-7&doi=',
            'by'=>0,
            'journal'=>'Site Admin',
            'type'=>0,
            'month_article'=>0,
            'famous'=>0,
            'views'=>1130,
            'show'=>0,
            'date'=>'2015-07-24 12:30:49',
            'active'=>'1',
            'user'=>0,
            'created_at'=>'2020-05-15 01:20:06',
            'updated_at'=>'2020-06-21 00:08:23',
            'deleted_at'=>NULL
            ] );
            
            
                        
            Post::create( [
            'id'=>4061,
            'cat_id'=>8,
            'tag_id'=>3,
            'title'=>'Intravenous amino acid therapy for kidney function in critically ill patients: a randomized controlled trial',
            'text'=>'Gordon S. Doig and others\r\nIntensive Care Med. J. August 2015417: 1197 - 1208',
            'img'=>'file-1463591657-K8BRDF.jpg',
            'file'=>NULL,
            'link'=>'http://icmjournal.esicm.org/journals/abstract.html?v=41&j=134&i=7&a=3827_10.1007_s00134-015-3827-9&doi=',
            'by'=>0,
            'journal'=>'Site Admin',
            'type'=>0,
            'month_article'=>0,
            'famous'=>0,
            'views'=>1101,
            'show'=>0,
            'date'=>'2015-07-24 12:34:00',
            'active'=>'1',
            'user'=>0,
            'created_at'=>'2020-05-15 01:20:06',
            'updated_at'=>'2020-06-21 00:08:23',
            'deleted_at'=>NULL
            ] );
            
            
                        
            Post::create( [
            'id'=>4062,
            'cat_id'=>8,
            'tag_id'=>10,
            'title'=>'Thromboprophylaxis with low molecular weight heparin versus unfractionated heparin in intensive care patients: a systematic review with meta-analysis and trial sequential analysis',
            'text'=>'Sigrid Beitland and others\r\nIntensive Care Med. J. August 2015417: 1209 - 1219',
            'img'=>'file-1463591746-GEE3E7.jpg',
            'file'=>NULL,
            'link'=>'http://icmjournal.esicm.org/journals/abstract.html?v=41&j=134&i=7&a=3840_10.1007_s00134-015-3840-z&doi=',
            'by'=>0,
            'journal'=>'Site Admin',
            'type'=>0,
            'month_article'=>0,
            'famous'=>0,
            'views'=>1101,
            'show'=>0,
            'date'=>'2015-07-24 12:35:51',
            'active'=>'1',
            'user'=>0,
            'created_at'=>'2020-05-15 01:20:06',
            'updated_at'=>'2020-06-21 00:08:23',
            'deleted_at'=>NULL
            ] );
            
            
                        
            Post::create( [
            'id'=>4063,
            'cat_id'=>8,
            'tag_id'=>6,
            'title'=>'Plasma suPAR as a prognostic biological marker for ICU mortality in ARDS patients',
            'text'=>'Diederik G. P. J. Geboers, and others\r\nIntensive Care Med. J. August 2015417: 1281 - 1290',
            'img'=>'file-1463591644-ZGN2DA.jpg',
            'file'=>NULL,
            'link'=>'http://icmjournal.esicm.org/journals/abstract.html?v=41&j=134&i=7&a=3924_10.1007_s00134-015-3924-9&doi=',
            'by'=>0,
            'journal'=>'Site Admin',
            'type'=>0,
            'month_article'=>0,
            'famous'=>0,
            'views'=>1101,
            'show'=>0,
            'date'=>'2015-07-24 12:38:18',
            'active'=>'1',
            'user'=>0,
            'created_at'=>'2020-05-15 01:20:06',
            'updated_at'=>'2020-06-21 00:08:23',
            'deleted_at'=>NULL
            ] );
            
            
                        
            Post::create( [
            'id'=>4064,
            'cat_id'=>8,
            'tag_id'=>3,
            'title'=>'The Association Between Nutritional Adequacy and Long-Term Outcomes in Critically Ill Patients Requiring Prolonged Mechanical Ventilation: A Multicenter Cohort Study',
            'text'=>'Wei, Xuejiao and others\nCritical Care Medicine:\nAugust 2015 - Volume 43 - Issue 8 - p 1569â€“1579',
            'img'=>'file-1463591650-J14MMU.jpg',
            'file'=>NULL,
            'link'=>'http://journals.lww.com/ccmjournal/Abstract/2015/08000/The_Association_Between_Nutritional_Adequacy_and.3.aspx',
            'by'=>0,
            'journal'=>'Site Admin',
            'type'=>0,
            'month_article'=>0,
            'famous'=>0,
            'views'=>1101,
            'show'=>0,
            'date'=>'2015-08-01 05:47:09',
            'active'=>'1',
            'user'=>0,
            'created_at'=>'2020-05-15 01:20:06',
            'updated_at'=>'2020-06-21 00:08:23',
            'deleted_at'=>NULL
            ] );
            
            
                        
            Post::create( [
            'id'=>4065,
            'cat_id'=>8,
            'tag_id'=>11,
            'title'=>'Patient Mortality Is Associated With Staff Resources and Workload in the ICU: A Multicenter Observational Study',
            'text'=>'Neuraz, Antoine and others\nCritical Care Medicine:\nAugust 2015 - Volume 43 - Issue 8 - p 1587â€“1594',
            'img'=>'file-1463591639-4YG8JY.jpg',
            'file'=>NULL,
            'link'=>'http://journals.lww.com/ccmjournal/Abstract/2015/08000/Patient_Mortality_Is_Associated_With_Staff.5.aspx',
            'by'=>0,
            'journal'=>'Site Admin',
            'type'=>0,
            'month_article'=>0,
            'famous'=>0,
            'views'=>1101,
            'show'=>0,
            'date'=>'2015-08-01 12:45:27',
            'active'=>'1',
            'user'=>0,
            'created_at'=>'2020-05-15 01:20:06',
            'updated_at'=>'2020-06-21 00:08:23',
            'deleted_at'=>NULL
            ] );
            
            
                        
            Post::create( [
            'id'=>4066,
            'cat_id'=>12,
            'tag_id'=>8,
            'title'=>'A Randomized Controlled Trial of Regional Citrate Versus Regional Heparin Anticoagulation for Continuous Renal Replacement Therapy in Critically Ill Adults',
            'text'=>'Gattas, David J and others\nCritical Care Med J. August 2015438:1622â€“1629',
            'img'=>'file-1463591738-IZ3XFW.jpg',
            'file'=>NULL,
            'link'=>'http://journals.lww.com/ccmjournal/Abstract/2015/08000/A_Randomized_Controlled_Trial_of_Regional_Citrate.9.aspx',
            'by'=>0,
            'journal'=>'Site Admin',
            'type'=>0,
            'month_article'=>0,
            'famous'=>0,
            'views'=>1263,
            'show'=>0,
            'date'=>'2015-07-24 12:50:56',
            'active'=>'1',
            'user'=>0,
            'created_at'=>'2020-05-15 01:20:06',
            'updated_at'=>'2020-06-21 00:08:23',
            'deleted_at'=>NULL
            ] );
            
            
                        
            Post::create( [
            'id'=>4067,
            'cat_id'=>8,
            'tag_id'=>5,
            'title'=>'External Ventricular and Lumbar Drain Device Infections in ICU Patients: A Prospective Multicenter Italian Study',
            'text'=>'Citerio, Giuseppe and others\r\nCritical Care Medicine:\r\nAugust 2015 - Volume 43 - Issue 8 - p 1630â€“1637',
            'img'=>'file-1463591685-DDLMK9.jpg',
            'file'=>NULL,
            'link'=>'http://journals.lww.com/ccmjournal/Abstract/2015/08000/External_Ventricular_and_Lumbar_Drain_Device.10.aspx',
            'by'=>0,
            'journal'=>'Site Admin',
            'type'=>0,
            'month_article'=>0,
            'famous'=>0,
            'views'=>1114,
            'show'=>0,
            'date'=>'2015-08-01 12:53:53',
            'active'=>'1',
            'user'=>0,
            'created_at'=>'2020-05-15 01:20:06',
            'updated_at'=>'2020-06-21 00:08:23',
            'deleted_at'=>NULL
            ] );
            
            
                        
            Post::create( [
            'id'=>4068,
            'cat_id'=>8,
            'tag_id'=>5,
            'title'=>'Neurogenic Pulmonary Edema\r\nConcise Definitive Review',
            'text'=>'Busl, Katharina and Bleck, Thomas\r\nCritical Care Medicine:\r\nAugust 2015 - Volume 43 - Issue 8 - p 1710â€“1715',
            'img'=>'file-1463591752-XK911T.jpg',
            'file'=>NULL,
            'link'=>'http://journals.lww.com/ccmjournal/Abstract/2015/08000/Neurogenic_Pulmonary_Edema.20.aspx',
            'by'=>0,
            'journal'=>'Site Admin',
            'type'=>0,
            'month_article'=>0,
            'famous'=>0,
            'views'=>1114,
            'show'=>0,
            'date'=>'2015-08-01 12:57:43',
            'active'=>'1',
            'user'=>0,
            'created_at'=>'2020-05-15 01:20:06',
            'updated_at'=>'2020-06-21 00:08:23',
            'deleted_at'=>NULL
            ] );
            
            
                        
            Post::create( [
            'id'=>4069,
            'cat_id'=>12,
            'tag_id'=>10,
            'title'=>'Management of Anticoagulation in Patients With Atrial Fibrillation\r\nJAMA Clinical Guidelines Synopsis',
            'text'=>'Joshua D. Moss and  Adam S. Cifu\r\nJAMA. July 21,20153143:291-292',
            'img'=>'file-1463591732-SM39NY.jpg',
            'file'=>NULL,
            'link'=>'http://jama.jamanetwork.com/article.aspx?articleid=2397821',
            'by'=>0,
            'journal'=>'Site Admin',
            'type'=>0,
            'month_article'=>0,
            'famous'=>0,
            'views'=>1212,
            'show'=>0,
            'date'=>'2015-07-21 13:04:54',
            'active'=>'1',
            'user'=>0,
            'created_at'=>'2020-05-15 01:20:06',
            'updated_at'=>'2020-06-21 00:08:23',
            'deleted_at'=>NULL
            ] );
            
            
                        
            Post::create( [
            'id'=>4070,
            'cat_id'=>9,
            'tag_id'=>10,
            'title'=>'Management of Anticoagulation in Patients With Atrial Fibrillation: \r\nJAMA Clinical Guidelines Synopsis',
            'text'=>'Joshua D. Moss and Adam S. Cifu\r\nJAMA. July 21,20153143:291-292',
            'img'=>'file-1463591657-K8BRDF.jpg',
            'file'=>NULL,
            'link'=>'http://jama.jamanetwork.com/article.aspx?articleid=2397821',
            'by'=>0,
            'journal'=>'Site Admin',
            'type'=>0,
            'month_article'=>0,
            'famous'=>0,
            'views'=>1101,
            'show'=>0,
            'date'=>'2015-07-21 13:07:47',
            'active'=>'1',
            'user'=>0,
            'created_at'=>'2020-05-15 01:20:06',
            'updated_at'=>'2020-06-21 00:08:23',
            'deleted_at'=>NULL
            ] );
            
            
                        
            Post::create( [
            'id'=>4071,
            'cat_id'=>8,
            'tag_id'=>7,
            'title'=>'Adverse events associated with poor neurological outcome during targeted temperature management and advanced critical care after out-of-hospital cardiac arrest',
            'text'=>'Kim Y, Youn C, Kim S, Lee B, Cho I, Cho G, Jeung K, Oh S, Choi S, Shin J, Cha K, Oh J, Yim H, Park K, on behalf of the Korean Hypothermia Network Investigators\r\nCritical Care 2015, 19 :283 22 July 2015',
            'img'=>'file-1463591732-SM39NY.jpg',
            'file'=>NULL,
            'link'=>'http://www.ccforum.com/content/19/1/283/abstract',
            'by'=>0,
            'journal'=>'Site Admin',
            'type'=>0,
            'month_article'=>0,
            'famous'=>0,
            'views'=>1101,
            'show'=>0,
            'date'=>'2015-07-22 20:12:25',
            'active'=>'1',
            'user'=>0,
            'created_at'=>'2020-05-15 01:20:06',
            'updated_at'=>'2020-06-21 00:08:23',
            'deleted_at'=>NULL
            ] );
            
            
                        
            Post::create( [
            'id'=>4072,
            'cat_id'=>8,
            'tag_id'=>6,
            'title'=>'Use of ultrasound guidance to improve the safety of percutaneous dilatational tracheostomy: a literature review',
            'text'=>'Mariam Alansari, Hadil Alotair, Zohair Al Aseri, Mohammed A Elhoseny\r\nCritical Care 2015, 19:229 18 May 2015',
            'img'=>'file-1463591650-J14MMU.jpg',
            'file'=>NULL,
            'link'=>'http://www.ccforum.com/content/19/1/229?utm_campaign=BMC20186C&utm_medium=BMCemail&utm_source=Teradata',
            'by'=>0,
            'journal'=>'Site Admin',
            'type'=>0,
            'month_article'=>0,
            'famous'=>0,
            'views'=>1101,
            'show'=>0,
            'date'=>'2015-05-18 05:48:24',
            'active'=>'1',
            'user'=>0,
            'created_at'=>'2020-05-15 01:20:06',
            'updated_at'=>'2020-06-21 00:08:23',
            'deleted_at'=>NULL
            ] );
            
            
                        
            Post::create( [
            'id'=>4073,
            'cat_id'=>8,
            'tag_id'=>7,
            'title'=>'Perioperative cardiovascular monitoring of high-risk patients: a consensus of 12',
            'text'=>'Jean-Louis Vincent, Paolo Pelosi, Rupert Pearse, Didier Payen, Azriel Perel, Andreas Hoeft, et al.\r\nCritical Care 2015, 19:224 8 May 2015',
            'img'=>'file-1463591650-J14MMU.jpg',
            'file'=>NULL,
            'link'=>'http://www.ccforum.com/content/19/1/224?utm_campaign=BMC20186C&utm_medium=BMCemail&utm_source=Teradata',
            'by'=>0,
            'journal'=>'Site Admin',
            'type'=>0,
            'month_article'=>0,
            'famous'=>0,
            'views'=>1101,
            'show'=>0,
            'date'=>'2015-05-08 05:49:25',
            'active'=>'1',
            'user'=>0,
            'created_at'=>'2020-05-15 01:20:06',
            'updated_at'=>'2020-06-21 00:08:23',
            'deleted_at'=>NULL
            ] );
            
            
                        
            Post::create( [
            'id'=>4074,
            'cat_id'=>8,
            'tag_id'=>10,
            'title'=>'Restrictive and liberal red cell transfusion strategies in adult patients: reconciling clinical data with best practice',
            'text'=>'Marek A Mirski, Steven M Frank, Daryl J Kor, Jean-Louis Vincent, David R Holmes\r\nCritical Care 2015, 19:202 5 May 2015',
            'img'=>'file-1463591685-DDLMK9.jpg',
            'file'=>NULL,
            'link'=>'http://news.springer.com/re?l=D0In5x6tlI6hx2p5vIq&req=utm_campaign%3DBMC20186C',
            'by'=>0,
            'journal'=>'Site Admin',
            'type'=>0,
            'month_article'=>0,
            'famous'=>0,
            'views'=>1101,
            'show'=>0,
            'date'=>'2015-05-05 05:51:31',
            'active'=>'1',
            'user'=>0,
            'created_at'=>'2020-05-15 01:20:06',
            'updated_at'=>'2020-06-21 00:08:23',
            'deleted_at'=>NULL
            ] );
            
            
                        
            Post::create( [
            'id'=>4075,
            'cat_id'=>8,
            'tag_id'=>4,
            'title'=>'Perioperative goal directed therapy and postoperative outcomes in patients undergoing high-risk abdominal surgery: a historical-prospective, comparative effectiveness study',
            'text'=>'Maxime Cannesson, Davinder Ramsingh, Joseph Rinehart, Aram Demirjian, Trung Vu, Shermeen Vakharia, et al.\r\nCritical Care 2015, 19:261 19 June 2015',
            'img'=>'file-1463591752-XK911T.jpg',
            'file'=>NULL,
            'link'=>'http://www.ccforum.com/content/19/1/261?utm_campaign=BMC20186C&utm_medium=BMCemail&utm_source=Teradata',
            'by'=>0,
            'journal'=>'Site Admin',
            'type'=>0,
            'month_article'=>0,
            'famous'=>0,
            'views'=>1198,
            'show'=>0,
            'date'=>'2015-06-19 05:52:09',
            'active'=>'1',
            'user'=>0,
            'created_at'=>'2020-05-15 01:20:06',
            'updated_at'=>'2020-06-21 00:08:23',
            'deleted_at'=>NULL
            ] );
            
            
                        
            Post::create( [
            'id'=>4076,
            'cat_id'=>8,
            'tag_id'=>6,
            'title'=>'Incidence of and risk factors for severe cardiovascular collapse after endotracheal intubation in the ICU: a multicenter observational study',
            'text'=>'Sebastien Perbet, Audrey De Jong, Julie Delmas, Emmanuel Futier, Bruno Pereira, Samir Jaber, Jean-Michel Constantin\r\nCritical Care 2015, 19:257 18 June 2015',
            'img'=>'file-1463591685-DDLMK9.jpg',
            'file'=>NULL,
            'link'=>'http://www.ccforum.com/content/19/1/257?utm_campaign=BMC20186C&utm_medium=BMCemail&utm_source=Teradata',
            'by'=>0,
            'journal'=>'Site Admin',
            'type'=>0,
            'month_article'=>0,
            'famous'=>0,
            'views'=>1101,
            'show'=>0,
            'date'=>'2015-06-18 05:53:07',
            'active'=>'1',
            'user'=>0,
            'created_at'=>'2020-05-15 01:20:06',
            'updated_at'=>'2020-06-21 00:08:23',
            'deleted_at'=>NULL
            ] );
            
            
                        
            Post::create( [
            'id'=>4077,
            'cat_id'=>8,
            'tag_id'=>7,
            'title'=>'Early dynamic left intraventricular obstruction is associated with hypovolemia and high mortality in septic shock patients',
            'text'=>'Jean-Louis Chauvet, Shari El-Dash, Olivier Delastre, Bernard Bouffandeau, Dominique Jusserand, Jean-Baptiste Michot, et al.\r\nCritical Care 2015, 19:262 17 June 2015',
            'img'=>'file-1463591725-9J88SF.jpg',
            'file'=>NULL,
            'link'=>'http://www.ccforum.com/content/19/1/262?utm_campaign=BMC20186C&utm_medium=BMCemail&utm_source=Teradata',
            'by'=>0,
            'journal'=>'Site Admin',
            'type'=>0,
            'month_article'=>0,
            'famous'=>0,
            'views'=>1101,
            'show'=>0,
            'date'=>'2015-06-17 05:53:58',
            'active'=>'1',
            'user'=>0,
            'created_at'=>'2020-05-15 01:20:06',
            'updated_at'=>'2020-06-21 00:08:23',
            'deleted_at'=>NULL
            ] );
            
            
                        
            Post::create( [
            'id'=>4078,
            'cat_id'=>8,
            'tag_id'=>7,
            'title'=>'Rapid response systems: a systematic review and meta-analysis',
            'text'=>'Ritesh Maharaj, Ivan Raffaele, Julia Wendon\r\nCritical Care 2015, 19:254 12 June 2015',
            'img'=>'file-1463591650-J14MMU.jpg',
            'file'=>NULL,
            'link'=>'http://www.ccforum.com/content/19/1/254?utm_campaign=BMC20186C&utm_medium=BMCemail&utm_source=Teradata',
            'by'=>0,
            'journal'=>'Site Admin',
            'type'=>0,
            'month_article'=>0,
            'famous'=>0,
            'views'=>1101,
            'show'=>0,
            'date'=>'2015-06-12 05:54:58',
            'active'=>'1',
            'user'=>0,
            'created_at'=>'2020-05-15 01:20:06',
            'updated_at'=>'2020-06-21 00:08:23',
            'deleted_at'=>NULL
            ] );
            
            
                        
            Post::create( [
            'id'=>4079,
            'cat_id'=>8,
            'tag_id'=>6,
            'title'=>'Immunomodulation by lipid emulsions in pulmonary inflammation: a randomized controlled trial',
            'text'=>'Matthias Hecker, Tomke Linder, Juliane Ott, Hans-Dieter Walmrath, JÃ¼rgen Lohmeyer, IstvÃ¡n VadÃ¡sz, et al.\r\nCritical Care 2015, 19:226 12 May 2015',
            'img'=>'file-1463591657-K8BRDF.jpg',
            'file'=>NULL,
            'link'=>'http://www.ccforum.com/content/19/1/226?utm_campaign=BMC20186C&utm_medium=BMCemail&utm_source=Teradata',
            'by'=>0,
            'journal'=>'Site Admin',
            'type'=>0,
            'month_article'=>0,
            'famous'=>0,
            'views'=>1101,
            'show'=>0,
            'date'=>'2015-05-12 05:56:42',
            'active'=>'1',
            'user'=>0,
            'created_at'=>'2020-05-15 01:20:06',
            'updated_at'=>'2020-06-21 00:08:23',
            'deleted_at'=>NULL
            ] );
            
            
                        
            Post::create( [
            'id'=>4080,
            'cat_id'=>8,
            'tag_id'=>11,
            'title'=>'A comprehensive method to develop a checklist to increase safety of intra-hospital transport of critically ill patients',
            'text'=>'Anja H Brunsveld-Reinders, M Arbous, Sander G Kuiper, Evert de Jonge\r\nCritical Care 2015, 19:214 7 May 2015',
            'img'=>'file-1463591746-GEE3E7.jpg',
            'file'=>NULL,
            'link'=>'http://news.springer.com/re?l=D0In5x6tlI6hx2p5vI1i&req=utm_campaign%3DBMC20186C',
            'by'=>0,
            'journal'=>'Site Admin',
            'type'=>0,
            'month_article'=>0,
            'famous'=>0,
            'views'=>1101,
            'show'=>0,
            'date'=>'2015-05-07 05:58:09',
            'active'=>'1',
            'user'=>0,
            'created_at'=>'2020-05-15 01:20:06',
            'updated_at'=>'2020-06-21 00:08:23',
            'deleted_at'=>NULL
            ] );
            
            
                        
            Post::create( [
            'id'=>4081,
            'cat_id'=>8,
            'tag_id'=>6,
            'title'=>'Importance of a registered and structured protocol when conducting systematic reviews: comments about nebulized antibiotics for ventilator-associated pneumonia',
            'text'=>'Zampieri F, Nassar A, Gusmao-Flores D, Taniguchi L, Torres A, Ranzani O \r\nCritical Care 2015, 19 :298 20 August 2015',
            'img'=>'file-1463591738-IZ3XFW.jpg',
            'file'=>NULL,
            'link'=>'http://www.ccforum.com/content/19/1/298/abstract',
            'by'=>0,
            'journal'=>'Site Admin',
            'type'=>0,
            'month_article'=>0,
            'famous'=>0,
            'views'=>1101,
            'show'=>0,
            'date'=>'2015-08-20 10:13:47',
            'active'=>'1',
            'user'=>0,
            'created_at'=>'2020-05-15 01:20:06',
            'updated_at'=>'2020-06-21 00:08:23',
            'deleted_at'=>NULL
            ] );
            
            
                        
            Post::create( [
            'id'=>4082,
            'cat_id'=>8,
            'tag_id'=>7,
            'title'=>'Pre-existing atrial fibrillation and risk of arterial thromboembolism and death in intensive care unit patients: a population-based cohort study',
            'text'=>'Gamst J, Christiansen C, Rasmussen B, Rasmussen L, Thomsen R \r\nCritical Care 2015, 19 :299 19 August 2015',
            'img'=>'file-1463591657-K8BRDF.jpg',
            'file'=>NULL,
            'link'=>'http://www.ccforum.com/content/19/1/299/abstract',
            'by'=>0,
            'journal'=>'Site Admin',
            'type'=>0,
            'month_article'=>0,
            'famous'=>0,
            'views'=>1101,
            'show'=>0,
            'date'=>'2015-08-19 10:16:25',
            'active'=>'1',
            'user'=>0,
            'created_at'=>'2020-05-15 01:20:06',
            'updated_at'=>'2020-06-21 00:08:23',
            'deleted_at'=>NULL
            ] );
            
            
                        
            Post::create( [
            'id'=>4083,
            'cat_id'=>8,
            'tag_id'=>3,
            'title'=>'Enteral glutamine supplementation in critically ill patients: a systematic review and meta-analysis',
            'text'=>'van Zanten A, Dhaliwal R, Garrel D, Heyland D \r\nCritical Care 2015, 19 :294 18 August 2015',
            'img'=>'file-1463591657-K8BRDF.jpg',
            'file'=>NULL,
            'link'=>'http://www.ccforum.com/content/19/1/294/abstract',
            'by'=>0,
            'journal'=>'Site Admin',
            'type'=>0,
            'month_article'=>0,
            'famous'=>0,
            'views'=>1101,
            'show'=>0,
            'date'=>'2015-08-19 10:17:27',
            'active'=>'1',
            'user'=>0,
            'created_at'=>'2020-05-15 01:20:06',
            'updated_at'=>'2020-06-21 00:08:23',
            'deleted_at'=>NULL
            ] );
            
            
                        
            Post::create( [
            'id'=>4084,
            'cat_id'=>8,
            'tag_id'=>10,
            'title'=>'Venous thromboembolism in the ICU: main characteristics, diagnosis and thromboprophylaxis',
            'text'=>'Minet C, Potton L, Bonadona A, Hamidfar-Roy R, Somohano C, Lugosi M, Cartier J, Ferretti G, Schwebel C, Timsit J \r\nCritical Care 2015, 19 :287 18 August 2015',
            'img'=>'file-1463591644-ZGN2DA.jpg',
            'file'=>NULL,
            'link'=>'http://www.ccforum.com/content/19/1/287/abstract',
            'by'=>0,
            'journal'=>'Site Admin',
            'type'=>0,
            'month_article'=>0,
            'famous'=>0,
            'views'=>1101,
            'show'=>0,
            'date'=>'2015-08-19 10:18:16',
            'active'=>'1',
            'user'=>0,
            'created_at'=>'2020-05-15 01:20:06',
            'updated_at'=>'2020-06-21 00:08:23',
            'deleted_at'=>NULL
            ] );
            
            
                        
            Post::create( [
            'id'=>4085,
            'cat_id'=>8,
            'tag_id'=>4,
            'title'=>'Characteristics of chest wall injuries that predict postrecovery pulmonary symptoms: A secondary analysis of data from a randomized trial',
            'text'=>'Dhillon, Tejveer and others\r\nJournal of Trauma and Acute Care Surgery: \r\nAugust 2015 - Volume 79 - Issue 2 - p 179â€“187',
            'img'=>'file-1463591685-DDLMK9.jpg',
            'file'=>NULL,
            'link'=>'http://journals.lww.com/jtrauma/Abstract/2015/08000/Characteristics_of_chest_wall_injuries_that.1.aspx',
            'by'=>0,
            'journal'=>'Site Admin',
            'type'=>0,
            'month_article'=>0,
            'famous'=>0,
            'views'=>1198,
            'show'=>0,
            'date'=>'2015-08-23 10:20:00',
            'active'=>'1',
            'user'=>0,
            'created_at'=>'2020-05-15 01:20:06',
            'updated_at'=>'2020-06-21 00:08:23',
            'deleted_at'=>NULL
            ] );
            
            
                        
            Post::create( [
            'id'=>4086,
            'cat_id'=>8,
            'tag_id'=>4,
            'title'=>'Mechanisms of early trauma-induced coagulopathy: The clot thickens or not',
            'text'=>'Dobson, Geoffrey P and others\r\nJournal of Trauma and Acute Care Surgery: \r\nAugust 2015 - Volume 79 - Issue 2 - p 301â€“309',
            'img'=>'file-1463591746-GEE3E7.jpg',
            'file'=>NULL,
            'link'=>'http://journals.lww.com/jtrauma/Abstract/2015/08000/Mechanisms_of_early_trauma_induced_coagulopathy__.19.aspx',
            'by'=>0,
            'journal'=>'Site Admin',
            'type'=>0,
            'month_article'=>0,
            'famous'=>0,
            'views'=>1198,
            'show'=>0,
            'date'=>'2015-08-23 10:21:52',
            'active'=>'1',
            'user'=>0,
            'created_at'=>'2020-05-15 01:20:06',
            'updated_at'=>'2020-06-21 00:08:23',
            'deleted_at'=>NULL
            ] );
            
            
                        
            Post::create( [
            'id'=>4087,
            'cat_id'=>8,
            'tag_id'=>10,
            'title'=>'Tinzaparin vs Warfarin for Acute VTE in Patients With Active Cancer',
            'text'=>'Agnes Y. Y. Lee and others\r\nJAMA. 18 August 20153147:677-686.',
            'img'=>'file-1463591738-IZ3XFW.jpg',
            'file'=>NULL,
            'link'=>'http://jama.jamanetwork.com/article.aspx?articleid=2428955',
            'by'=>0,
            'journal'=>'Site Admin',
            'type'=>0,
            'month_article'=>0,
            'famous'=>0,
            'views'=>1101,
            'show'=>0,
            'date'=>'2015-08-19 10:26:39',
            'active'=>'1',
            'user'=>0,
            'created_at'=>'2020-05-15 01:20:06',
            'updated_at'=>'2020-06-21 00:08:23',
            'deleted_at'=>NULL
            ] );
            
            
                        
            Post::create( [
            'id'=>4088,
            'cat_id'=>8,
            'tag_id'=>1,
            'title'=>'Septic Shock: Advances in Diagnosis and Treatment',
            'text'=>'Christopher W. Seymour Matthew R. Rosengart\r\nJAMA. 18 August 20153147:708-717',
            'img'=>'file-1463591639-4YG8JY.jpg',
            'file'=>NULL,
            'link'=>'http://jama.jamanetwork.com/article.aspx?articleid=2428960',
            'by'=>0,
            'journal'=>'Site Admin',
            'type'=>0,
            'month_article'=>0,
            'famous'=>0,
            'views'=>1130,
            'show'=>0,
            'date'=>'2015-08-18 10:26:25',
            'active'=>'1',
            'user'=>0,
            'created_at'=>'2020-05-15 01:20:06',
            'updated_at'=>'2020-06-21 00:08:23',
            'deleted_at'=>NULL
            ] );
            
            
                        
            Post::create( [
            'id'=>4089,
            'cat_id'=>9,
            'tag_id'=>7,
            'title'=>'Pulmonary Embolism',
            'text'=>'Jill M. Merrigan, BA Gregory Piazza, MD, MS Cassio Lynm, MA Edward H. Livingston, MD \r\nJAMA. 6 Feb. 20133095:504',
            'img'=>'file-1463591685-DDLMK9.jpg',
            'file'=>NULL,
            'link'=>'http://jama.jamanetwork.com/article.aspx?articleid=1568253',
            'by'=>0,
            'journal'=>'Site Admin',
            'type'=>0,
            'month_article'=>0,
            'famous'=>0,
            'views'=>1101,
            'show'=>0,
            'date'=>'2013-02-06 10:29:37',
            'active'=>'1',
            'user'=>0,
            'created_at'=>'2020-05-15 01:20:06',
            'updated_at'=>'2020-06-21 00:08:23',
            'deleted_at'=>NULL
            ] );
            
            
                        
            Post::create( [
            'id'=>4090,
            'cat_id'=>9,
            'tag_id'=>7,
            'title'=>'2014 Evidence-Based Guideline for the Management of High Blood Pressure in AdultsReport From the Panel Members Appointed to the Eighth Joint National Committee JNC 8',
            'text'=>'Paul A. James and others\r\nJAMA. 5 Feb. 20143115:507-520',
            'img'=>'file-1463591644-ZGN2DA.jpg',
            'file'=>NULL,
            'link'=>'http://jama.jamanetwork.com/article.aspx?articleid=1791497',
            'by'=>0,
            'journal'=>'Site Admin',
            'type'=>0,
            'month_article'=>0,
            'famous'=>0,
            'views'=>1101,
            'show'=>0,
            'date'=>'2014-02-05 10:31:15',
            'active'=>'1',
            'user'=>0,
            'created_at'=>'2020-05-15 01:20:06',
            'updated_at'=>'2020-06-21 00:08:23',
            'deleted_at'=>NULL
            ] );
            
            
                        
            Post::create( [
            'id'=>4091,
            'cat_id'=>8,
            'tag_id'=>6,
            'title'=>'Association of Electronic Cigarette Use With Initiation of Combustible Tobacco Product Smoking in Early Adolescence',
            'text'=>'Adam M. Leventhal and others\r\nJAMA. 18 August 20153147:700-707',
            'img'=>'file-1463591738-IZ3XFW.jpg',
            'file'=>NULL,
            'link'=>'http://jama.jamanetwork.com/article.aspx?articleid=2428954',
            'by'=>0,
            'journal'=>'Site Admin',
            'type'=>0,
            'month_article'=>0,
            'famous'=>0,
            'views'=>1101,
            'show'=>0,
            'date'=>'2015-08-18 10:33:00',
            'active'=>'1',
            'user'=>0,
            'created_at'=>'2020-05-15 01:20:06',
            'updated_at'=>'2020-06-21 00:08:23',
            'deleted_at'=>NULL
            ] );
            
            
                        
            Post::create( [
            'id'=>4092,
            'cat_id'=>8,
            'tag_id'=>11,
            'title'=>'Female viagraâ€™ gets FDA approval despite severe side effects',
            'text'=>'Published time: 19 Aug, 2015 05:12',
            'img'=>'file-1463591746-GEE3E7.jpg',
            'file'=>NULL,
            'link'=>'http://www.rt.com/news/312793-female-viagra-fda-approves/',
            'by'=>0,
            'journal'=>'Site Admin',
            'type'=>0,
            'month_article'=>0,
            'famous'=>0,
            'views'=>1101,
            'show'=>0,
            'date'=>'2015-08-19 10:37:40',
            'active'=>'1',
            'user'=>0,
            'created_at'=>'2020-05-15 01:20:06',
            'updated_at'=>'2020-06-21 00:08:23',
            'deleted_at'=>NULL
            ] );
            
            
                        
            Post::create( [
            'id'=>4093,
            'cat_id'=>8,
            'tag_id'=>1,
            'title'=>'The Future of Sepsis Performance Improvement',
            'text'=>'Dellinger, R. Phillip\r\nCritical Care Medicine:\r\nSeptember 2015 - Volume 43 - Issue 9 - p 1787â€“1789',
            'img'=>'file-1463591639-4YG8JY.jpg',
            'file'=>NULL,
            'link'=>'http://journals.lww.com/ccmjournal/Fulltext/2015/09000/The_Future_of_Sepsis_Performance_Improvement.1.aspx',
            'by'=>0,
            'journal'=>'Site Admin',
            'type'=>0,
            'month_article'=>0,
            'famous'=>0,
            'views'=>1130,
            'show'=>0,
            'date'=>'2015-09-01 06:58:27',
            'active'=>'1',
            'user'=>0,
            'created_at'=>'2020-05-15 01:20:06',
            'updated_at'=>'2020-06-21 00:08:23',
            'deleted_at'=>NULL
            ] );
            
            
                        
            Post::create( [
            'id'=>4094,
            'cat_id'=>8,
            'tag_id'=>6,
            'title'=>'Cigarette Smoke Exposure and the Acute Respiratory Distress Syndrome',
            'text'=>'Calfee, Carolyn S and others\r\nCritical Care Medicine:\r\nSeptember 2015 - Volume 43 - Issue 9 - p 1790â€“1797',
            'img'=>'file-1463591639-4YG8JY.jpg',
            'file'=>NULL,
            'link'=>'http://journals.lww.com/ccmjournal/Abstract/2015/09000/Cigarette_Smoke_Exposure_and_the_Acute_Respiratory.2.aspx',
            'by'=>0,
            'journal'=>'Site Admin',
            'type'=>0,
            'month_article'=>0,
            'famous'=>0,
            'views'=>1101,
            'show'=>0,
            'date'=>'2015-09-01 06:59:50',
            'active'=>'1',
            'user'=>0,
            'created_at'=>'2020-05-15 01:20:06',
            'updated_at'=>'2020-06-21 00:08:23',
            'deleted_at'=>NULL
            ] );
            
            
                        
            Post::create( [
            'id'=>4095,
            'cat_id'=>8,
            'tag_id'=>7,
            'title'=>'Postoperative Critical Care of the Adult Cardiac Surgical Patient: Part II: Procedure-Specific Considerations, Management of Complications, and Quality Improvement',
            'text'=>'Stephens, R. Scott and others\r\nCritical Care Medicine:\r\nSeptember 2015 - Volume 43 - Issue 9 - p 1995â€“2014',
            'img'=>'file-1463591639-4YG8JY.jpg',
            'file'=>NULL,
            'link'=>'http://journals.lww.com/ccmjournal/Abstract/2015/09000/Postoperative_Critical_Care_of_the_Adult_Cardiac.25.aspx',
            'by'=>0,
            'journal'=>'Site Admin',
            'type'=>0,
            'month_article'=>0,
            'famous'=>0,
            'views'=>1101,
            'show'=>0,
            'date'=>'2015-09-01 07:01:59',
            'active'=>'1',
            'user'=>0,
            'created_at'=>'2020-05-15 01:20:06',
            'updated_at'=>'2020-06-21 00:08:23',
            'deleted_at'=>NULL
            ] );
            
            
                        
            Post::create( [
            'id'=>4096,
            'cat_id'=>8,
            'tag_id'=>1,
            'title'=>'Î²1-Adrenergic Inhibition Improves Cardiac and Vascular Function in Experimental Septic Shock',
            'text'=>'Kimmoun, Antoine and others\r\nCritical Care Medicine:\r\nSeptember 2015 - Volume 43 - Issue 9 - p e332â€“e340',
            'img'=>'file-1463591725-9J88SF.jpg',
            'file'=>NULL,
            'link'=>'http://journals.lww.com/ccmjournal/Abstract/2015/09000/_1_Adrenergic_Inhibition_Improves_Cardiac_and.44.aspx',
            'by'=>0,
            'journal'=>'Site Admin',
            'type'=>0,
            'month_article'=>0,
            'famous'=>0,
            'views'=>1130,
            'show'=>0,
            'date'=>'2015-09-01 07:04:42',
            'active'=>'1',
            'user'=>0,
            'created_at'=>'2020-05-15 01:20:06',
            'updated_at'=>'2020-06-21 00:08:23',
            'deleted_at'=>NULL
            ] );
            
            
                        
            Post::create( [
            'id'=>4097,
            'cat_id'=>12,
            'tag_id'=>8,
            'title'=>'Consensus statement from the 2014 International Microdialysis Forum',
            'text'=>'Peter J. Hutchinson and others\r\nIntensive Care Med J. Sept. 2015 419:1517-1528',
            'img'=>'file-1463591650-J14MMU.jpg',
            'file'=>NULL,
            'link'=>'http://icmjournal.esicm.org/journals/abstract.html?v=41&j=134&i=9&a=3930_10.1007_s00134-015-3930-y&doi=',
            'by'=>0,
            'journal'=>'Site Admin',
            'type'=>0,
            'month_article'=>0,
            'famous'=>0,
            'views'=>1263,
            'show'=>0,
            'date'=>'2015-09-01 07:09:39',
            'active'=>'1',
            'user'=>0,
            'created_at'=>'2020-05-15 01:20:06',
            'updated_at'=>'2020-06-21 00:08:23',
            'deleted_at'=>NULL
            ] );
            
            
                        
            Post::create( [
            'id'=>4098,
            'cat_id'=>12,
            'tag_id'=>7,
            'title'=>'Fluid challenges in intensive care: the FENICE study',
            'text'=>'Maurizio Cecconi and others\r\nIntensive Care Med J. Sept. 2015419:1529-1537',
            'img'=>'file-1463591752-XK911T.jpg',
            'file'=>NULL,
            'link'=>'http://icmjournal.esicm.org/journals/abstract.html?v=41&j=134&i=9&a=3850_10.1007_s00134-015-3850-x&doi=',
            'by'=>0,
            'journal'=>'Site Admin',
            'type'=>0,
            'month_article'=>0,
            'famous'=>0,
            'views'=>1212,
            'show'=>0,
            'date'=>'2015-09-01 07:12:11',
            'active'=>'1',
            'user'=>0,
            'created_at'=>'2020-05-15 01:20:06',
            'updated_at'=>'2020-06-21 00:08:23',
            'deleted_at'=>NULL
            ] );
            
            
                        
            Post::create( [
            'id'=>4099,
            'cat_id'=>12,
            'tag_id'=>6,
            'title'=>'High-flow nasal cannula oxygen during endotracheal intubation in hypoxemic patients: a randomized controlled clinical trial',
            'text'=>'MickaÃ«l Vourcâ€™h and others\r\nIntensive Care Med J. Sept. 2015419:1538-1548',
            'img'=>'file-1463591639-4YG8JY.jpg',
            'file'=>NULL,
            'link'=>'http://icmjournal.esicm.org/journals/abstract.html?v=41&j=134&i=9&a=3796_10.1007_s00134-015-3796-z&doi=',
            'by'=>0,
            'journal'=>'Site Admin',
            'type'=>0,
            'month_article'=>0,
            'famous'=>0,
            'views'=>1212,
            'show'=>0,
            'date'=>'2015-09-01 07:14:14',
            'active'=>'1',
            'user'=>0,
            'created_at'=>'2020-05-15 01:20:06',
            'updated_at'=>'2020-06-21 00:08:23',
            'deleted_at'=>NULL
            ] );
            
            
                        
            Post::create( [
            'id'=>4100,
            'cat_id'=>12,
            'tag_id'=>1,
            'title'=>'A systematic review and meta-analysis of early goal-directed therapy for septic shock: the ARISE, ProCESS and ProMISe Investigators',
            'text'=>'D. C. Angus and others\r\nIntensive Care Med J. Sept. 2015419:1549- 1560',
            'img'=>'file-1463591746-GEE3E7.jpg',
            'file'=>NULL,
            'link'=>'http://icmjournal.esicm.org/journals/abstract.html?v=41&j=134&i=9&a=3822_10.1007_s00134-015-3822-1&doi=',
            'by'=>0,
            'journal'=>'Site Admin',
            'type'=>0,
            'month_article'=>0,
            'famous'=>0,
            'views'=>1241,
            'show'=>0,
            'date'=>'2015-09-01 07:15:51',
            'active'=>'1',
            'user'=>0,
            'created_at'=>'2020-05-15 01:20:06',
            'updated_at'=>'2020-06-21 00:08:23',
            'deleted_at'=>NULL
            ] );
            
            
                        
            Post::create( [
            'id'=>4101,
            'cat_id'=>8,
            'tag_id'=>8,
            'title'=>'Fluid type and the use of renal replacement therapy in sepsis: a systematic review and network meta-analysis',
            'text'=>'B. Rochwerg and others\r\nIntensive Care Med J. Sept. 2015419:1561- 1571',
            'img'=>'file-1463591732-SM39NY.jpg',
            'file'=>NULL,
            'link'=>'http://icmjournal.esicm.org/journals/abstract.html?v=41&j=134&i=9&a=3794_10.1007_s00134-015-3794-1&doi=',
            'by'=>0,
            'journal'=>'Site Admin',
            'type'=>0,
            'month_article'=>0,
            'famous'=>0,
            'views'=>1152,
            'show'=>0,
            'date'=>'2015-09-01 07:17:45',
            'active'=>'1',
            'user'=>0,
            'created_at'=>'2020-05-15 01:20:06',
            'updated_at'=>'2020-06-21 00:08:23',
            'deleted_at'=>NULL
            ] );
            
            
                        
            Post::create( [
            'id'=>4102,
            'cat_id'=>8,
            'tag_id'=>11,
            'title'=>'Global variability in withholding and withdrawal of life-sustaining treatment in the intensive care unit: a systematic review',
            'text'=>'N. M. Mark, S. G. Rayner, N. J. Lee, J. R. Curtis\r\nIntensive Care Med J. Sept. 2015419:1572-1585',
            'img'=>'file-1463591650-J14MMU.jpg',
            'file'=>NULL,
            'link'=>'http://icmjournal.esicm.org/journals/abstract.html?v=41&j=134&i=9&a=3810_10.1007_s00134-015-3810-5&doi=',
            'by'=>0,
            'journal'=>'Site Admin',
            'type'=>0,
            'month_article'=>0,
            'famous'=>0,
            'views'=>1101,
            'show'=>0,
            'date'=>'2015-09-01 07:19:11',
            'active'=>'1',
            'user'=>0,
            'created_at'=>'2020-05-15 01:20:06',
            'updated_at'=>'2020-06-21 00:08:23',
            'deleted_at'=>NULL
            ] );
            
            
                        
            Post::create( [
            'id'=>4103,
            'cat_id'=>8,
            'tag_id'=>1,
            'title'=>'The Surviving Sepsis Campaign bundles and outcome: results from the International Multicentre Prevalence Study on Sepsis the IMPreSS study',
            'text'=>'Andrew Rhodes and others\r\nIntensive Care Med J. Sept. 2015419:1620- 1628',
            'img'=>'file-1463591644-ZGN2DA.jpg',
            'file'=>NULL,
            'link'=>'http://icmjournal.esicm.org/journals/abstract.html?v=41&j=134&i=9&a=3906_10.1007_s00134-015-3906-y&doi=',
            'by'=>0,
            'journal'=>'Site Admin',
            'type'=>0,
            'month_article'=>0,
            'famous'=>0,
            'views'=>1130,
            'show'=>0,
            'date'=>'2015-09-01 07:21:27',
            'active'=>'1',
            'user'=>0,
            'created_at'=>'2020-05-15 01:20:06',
            'updated_at'=>'2020-06-21 00:08:23',
            'deleted_at'=>NULL
            ] );
            
            
                        
            Post::create( [
            'id'=>4104,
            'cat_id'=>8,
            'tag_id'=>4,
            'title'=>'The splenic injury outcomes trial: An American Association for the Surgery of Trauma multi-institutional study',
            'text'=>'Zarzaur, Ben L. and others\r\nJournal of Trauma and Acute Care Surgery:\r\nSeptember 2015 - Volume 79 - Issue 3 - p 335â€“342',
            'img'=>'file-1463591644-ZGN2DA.jpg',
            'file'=>NULL,
            'link'=>'http://journals.lww.com/jtrauma/Abstract/2015/09000/The_splenic_injury_outcomes_trial___An_American.1.aspx',
            'by'=>0,
            'journal'=>'Site Admin',
            'type'=>0,
            'month_article'=>0,
            'famous'=>0,
            'views'=>1198,
            'show'=>0,
            'date'=>'2015-09-05 18:41:37',
            'active'=>'1',
            'user'=>0,
            'created_at'=>'2020-05-15 01:20:06',
            'updated_at'=>'2020-06-21 00:08:23',
            'deleted_at'=>NULL
            ] );
            
            
                        
            Post::create( [
            'id'=>4105,
            'cat_id'=>8,
            'tag_id'=>8,
            'title'=>'Acute kidney injury following severe trauma: Risk factors and long-term outcome',
            'text'=>'Eriksson, Mikael and others\r\nJournal of Trauma and Acute Care Surgery:\r\nSeptember 2015 - Volume 79 - Issue 3 - p 407â€“412',
            'img'=>'file-1463591657-K8BRDF.jpg',
            'file'=>NULL,
            'link'=>'http://journals.lww.com/jtrauma/Abstract/2015/09000/Acute_kidney_injury_following_severe_trauma___Risk.11.aspx',
            'by'=>0,
            'journal'=>'Site Admin',
            'type'=>0,
            'month_article'=>0,
            'famous'=>0,
            'views'=>1152,
            'show'=>0,
            'date'=>'2015-09-05 18:44:33',
            'active'=>'1',
            'user'=>0,
            'created_at'=>'2020-05-15 01:20:06',
            'updated_at'=>'2020-06-21 00:08:23',
            'deleted_at'=>NULL
            ] );
            
            
                        
            Post::create( [
            'id'=>4106,
            'cat_id'=>8,
            'tag_id'=>6,
            'title'=>'Misclassification of acute respiratory distress syndrome after traumatic injury: The cost of less rigorous approaches',
            'text'=>'Hendrickson, Carolyn M and others\r\nJournal of Trauma and Acute Care Surgery:\r\nSeptember 2015 - Volume 79 - Issue 3 - p 417â€“424',
            'img'=>'file-1463591639-4YG8JY.jpg',
            'file'=>NULL,
            'link'=>'http://journals.lww.com/jtrauma/Abstract/2015/09000/Misclassification_of_acute_respiratory_distress.13.aspx',
            'by'=>0,
            'journal'=>'Site Admin',
            'type'=>0,
            'month_article'=>0,
            'famous'=>0,
            'views'=>1101,
            'show'=>0,
            'date'=>'2015-09-05 18:46:40',
            'active'=>'1',
            'user'=>0,
            'created_at'=>'2020-05-15 01:20:06',
            'updated_at'=>'2020-06-21 00:08:23',
            'deleted_at'=>NULL
            ] );
            
            
                        
            Post::create( [
            'id'=>4107,
            'cat_id'=>12,
            'tag_id'=>7,
            'title'=>'A new device for the prevention of pulmonary embolism in critically ill patients: Results of the European Angel Catheter Registry',
            'text'=>'Taccone, Fabio S and others\r\nJournal of Trauma and Acute Care Surgery:\r\nSeptember 2015793:456â€“462',
            'img'=>'file-1463591752-XK911T.jpg',
            'file'=>NULL,
            'link'=>'http://journals.lww.com/jtrauma/Abstract/2015/09000/A_new_device_for_the_prevention_of_pulmonary.19.aspx',
            'by'=>0,
            'journal'=>'Site Admin',
            'type'=>0,
            'month_article'=>0,
            'famous'=>0,
            'views'=>1212,
            'show'=>0,
            'date'=>'2015-09-30 09:22:53',
            'active'=>'1',
            'user'=>0,
            'created_at'=>'2020-05-15 01:20:06',
            'updated_at'=>'2020-06-21 00:08:23',
            'deleted_at'=>NULL
            ] );
            
            
                        
            Post::create( [
            'id'=>4108,
            'cat_id'=>12,
            'tag_id'=>4,
            'title'=>'RETIC Trial: Reversal of Trauma Induced Coagulopathy Using Coagulation Factor Concentrates or Fresh Frozen Plasma',
            'text'=>'Principal Investigator: Univ.-Doz. Dr. Petra Innerhofer, Principal Investigator â€“ Medical University Innsbruck',
            'img'=>'file-1463591681-AGRM7H.jpg',
            'file'=>NULL,
            'link'=>'https://trialbulletin.com/lib/entry/ct-01545635',
            'by'=>0,
            'journal'=>'Site Admin',
            'type'=>0,
            'month_article'=>0,
            'famous'=>0,
            'views'=>1309,
            'show'=>0,
            'date'=>'2015-09-05 19:02:52',
            'active'=>'1',
            'user'=>0,
            'created_at'=>'2020-05-15 01:20:06',
            'updated_at'=>'2020-06-21 00:08:23',
            'deleted_at'=>NULL
            ] );
            
            
                        
            Post::create( [
            'id'=>4109,
            'cat_id'=>8,
            'tag_id'=>7,
            'title'=>'Stopping antiplatelet medication before coronary artery bypass graft surgery: is there an optimal timing to minimize bleeding',
            'text'=>'Gielen CL and others\r\nEur J Cardiothorac Surg. 2015 Aug 6',
            'img'=>'file-1463591685-DDLMK9.jpg',
            'file'=>NULL,
            'link'=>'http://ejcts.oxfordjournals.org/content/early/2015/08/06/ejcts.ezv269.abstract',
            'by'=>0,
            'journal'=>'Site Admin',
            'type'=>0,
            'month_article'=>0,
            'famous'=>1,
            'views'=>1196,
            'show'=>0,
            'date'=>'2015-08-06 19:12:33',
            'active'=>'1',
            'user'=>0,
            'created_at'=>'2020-05-15 01:20:06',
            'updated_at'=>'2020-06-21 00:08:23',
            'deleted_at'=>NULL
            ] );
            
            
                        
            Post::create( [
            'id'=>4110,
            'cat_id'=>8,
            'tag_id'=>1,
            'title'=>'Task force on management and prevention of Acinetobacter baumannii infections in the ICU',
            'text'=>'JosÃ© Garnacho-Montero and others\r\nIntensive Care Med. December 20154112:2057 - 2075',
            'img'=>'file-1463591732-SM39NY.jpg',
            'file'=>NULL,
            'link'=>'http://icmjournal.esicm.org/journals/abstract.html?v=41&j=134&i=12&a=4079_10.1007_s00134-015-4079-4&doi=',
            'by'=>0,
            'journal'=>'Site Admin',
            'type'=>0,
            'month_article'=>0,
            'famous'=>0,
            'views'=>1130,
            'show'=>0,
            'date'=>'2015-12-01 09:04:47',
            'active'=>'1',
            'user'=>0,
            'created_at'=>'2020-05-15 01:20:06',
            'updated_at'=>'2020-06-21 00:08:23',
            'deleted_at'=>NULL
            ] );
            
            
                        
            Post::create( [
            'id'=>4111,
            'cat_id'=>12,
            'tag_id'=>7,
            'title'=>'Bivalirudin or Unfractionated Heparin in Acute Coronary Syndromes',
            'text'=>'M. Valgimigli and Others\r\nN Engl J Med September1,2015373:997-1009',
            'img'=>'file-1463591725-9J88SF.jpg',
            'file'=>NULL,
            'link'=>'http://www.nejm.org/doi/full/10.1056/NEJMoa1507854?query=TOC',
            'by'=>0,
            'journal'=>'Site Admin',
            'type'=>0,
            'month_article'=>0,
            'famous'=>1,
            'views'=>1307,
            'show'=>0,
            'date'=>'2015-09-10 06:44:10',
            'active'=>'1',
            'user'=>0,
            'created_at'=>'2020-05-15 01:20:06',
            'updated_at'=>'2020-06-21 00:08:23',
            'deleted_at'=>NULL
            ] );
            
            
                        
            Post::create( [
            'id'=>4112,
            'cat_id'=>8,
            'tag_id'=>7,
            'title'=>'Cyclosporine before PCI in Patients with Acute Myocardial Infarction',
            'text'=>'T.-T. Cung and Others\r\nN Engl J Med 2015373:1021-1031',
            'img'=>'file-1463591639-4YG8JY.jpg',
            'file'=>NULL,
            'link'=>'http://www.nejm.org/doi/full/10.1056/NEJMoa1505489?query=TOC',
            'by'=>0,
            'journal'=>'Site Admin',
            'type'=>0,
            'month_article'=>0,
            'famous'=>0,
            'views'=>1101,
            'show'=>0,
            'date'=>'2015-09-10 07:08:49',
            'active'=>'1',
            'user'=>0,
            'created_at'=>'2020-05-15 01:20:06',
            'updated_at'=>'2020-06-21 00:08:23',
            'deleted_at'=>NULL
            ] );
            
            
                        
            Post::create( [
            'id'=>4113,
            'cat_id'=>8,
            'tag_id'=>5,
            'title'=>'Cannabinoids in the Treatment of Epilepsy',
            'text'=>'D. Friedman and O. Devinsky\r\nN Engl J Med 2015373:1048-1058',
            'img'=>'file-1463591685-DDLMK9.jpg',
            'file'=>NULL,
            'link'=>'http://www.nejm.org/doi/full/10.1056/NEJMra1407304?query=TOC',
            'by'=>0,
            'journal'=>'Site Admin',
            'type'=>0,
            'month_article'=>0,
            'famous'=>0,
            'views'=>1114,
            'show'=>0,
            'date'=>'2015-09-10 07:14:45',
            'active'=>'1',
            'user'=>0,
            'created_at'=>'2020-05-15 01:20:06',
            'updated_at'=>'2020-06-21 00:08:23',
            'deleted_at'=>NULL
            ] );
            
            
                        
            Post::create( [
            'id'=>4114,
            'cat_id'=>8,
            'tag_id'=>5,
            'title'=>'Effect of clopidogrel with aspirin on functional outcome in TIA or minor stroke: CHANCE substudy',
            'text'=>'Wang X and others\r\nNeurology. 2015 Aug 18857:573-9',
            'img'=>'file-1463591752-XK911T.jpg',
            'file'=>NULL,
            'link'=>'http://www.ntkinstitute.org/news/content.nsf/NTKPaperFrameSet?OpenForm&pp=1&id=DF5950353CA4263E85256FDB00692E0A&newsid=6299375CF729858885257EB40050AA0A&locref=ntkwatch&u=http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&db=PubMed&dopt=Abstract&list_uids=26283758',
            'by'=>0,
            'journal'=>'Site Admin',
            'type'=>0,
            'month_article'=>0,
            'famous'=>0,
            'views'=>1114,
            'show'=>0,
            'date'=>'2015-09-01 18:21:20',
            'active'=>'1',
            'user'=>0,
            'created_at'=>'2020-05-15 01:20:06',
            'updated_at'=>'2020-06-21 00:08:23',
            'deleted_at'=>NULL
            ] );
            
            
                        
            Post::create( [
            'id'=>4115,
            'cat_id'=>8,
            'tag_id'=>7,
            'title'=>'Among antithrombotic agents, prasugrel, but not ticagrelor, is associated with reduced 30 day mortality in patients with ST-elevated myocardial infarction',
            'text'=>'Serebruany VL, Cherepanov V, Tomek A, Kim MH.\r\nInt J Cardiol. 2015 Sep 15195:104-10',
            'img'=>'file-1463591725-9J88SF.jpg',
            'file'=>NULL,
            'link'=>'http://www.internationaljournalofcardiology.com/article/S0167-5273(15)01108-0/abstract',
            'by'=>0,
            'journal'=>'Site Admin',
            'type'=>0,
            'month_article'=>0,
            'famous'=>0,
            'views'=>1101,
            'show'=>0,
            'date'=>'2015-09-21 05:44:32',
            'active'=>'1',
            'user'=>0,
            'created_at'=>'2020-05-15 01:20:06',
            'updated_at'=>'2020-06-21 00:08:23',
            'deleted_at'=>NULL
            ] );
            
            
                        
            Post::create( [
            'id'=>4116,
            'cat_id'=>8,
            'tag_id'=>11,
            'title'=>'Integrated Critical Care Organizations: A Personal View',
            'text'=>'Fink, Mitchell P. \r\nCritical Care Medicine:\r\nOctober 2015 - Volume 43 - Issue 10 - p 2047â€“2048',
            'img'=>'file-1463591639-4YG8JY.jpg',
            'file'=>NULL,
            'link'=>'http://journals.lww.com/ccmjournal/Fulltext/2015/10000/Integrated_Critical_Care_Organizations___A.1.aspx',
            'by'=>0,
            'journal'=>'Site Admin',
            'type'=>0,
            'month_article'=>0,
            'famous'=>0,
            'views'=>1101,
            'show'=>0,
            'date'=>'2015-10-01 08:46:53',
            'active'=>'1',
            'user'=>0,
            'created_at'=>'2020-05-15 01:20:06',
            'updated_at'=>'2020-06-21 00:08:23',
            'deleted_at'=>NULL
            ] );
            
            
                        
            Post::create( [
            'id'=>4117,
            'cat_id'=>8,
            'tag_id'=>11,
            'title'=>'Delayed Emergency Team Calls and Associated Hospital Mortality: A Multicenter Study',
            'text'=>'Chen, Jack and others\r\nCritical Care Medicine:\r\nOctober 2015 - Volume 43 - Issue 10 - p 2059â€“2065',
            'img'=>'file-1463591650-J14MMU.jpg',
            'file'=>NULL,
            'link'=>'http://journals.lww.com/ccmjournal/Abstract/2015/10000/Delayed_Emergency_Team_Calls_and_Associated.3.aspx',
            'by'=>0,
            'journal'=>'Site Admin',
            'type'=>0,
            'month_article'=>0,
            'famous'=>0,
            'views'=>1101,
            'show'=>0,
            'date'=>'2015-10-01 08:49:26',
            'active'=>'1',
            'user'=>0,
            'created_at'=>'2020-05-15 01:20:06',
            'updated_at'=>'2020-06-21 00:08:23',
            'deleted_at'=>NULL
            ] );
            
            
                        
            Post::create( [
            'id'=>4118,
            'cat_id'=>8,
            'tag_id'=>11,
            'title'=>'Protocols and Hospital Mortality in Critically Ill Patients: The United States Critical Illness and Injury Trials Group Critical Illness Outcomes Study',
            'text'=>'Sevransky, Jonathan E and others\r\nCritical Care Medicine:\r\nOctober 2015 - Volume 43 - Issue 10 - p 2076â€“2084',
            'img'=>'file-1463591681-AGRM7H.jpg',
            'file'=>NULL,
            'link'=>'http://journals.lww.com/ccmjournal/Abstract/2015/10000/Protocols_and_Hospital_Mortality_in_Critically_Ill.5.aspx',
            'by'=>0,
            'journal'=>'Site Admin',
            'type'=>0,
            'month_article'=>0,
            'famous'=>0,
            'views'=>1101,
            'show'=>0,
            'date'=>'2015-10-01 08:54:30',
            'active'=>'1',
            'user'=>0,
            'created_at'=>'2020-05-15 01:20:06',
            'updated_at'=>'2020-06-21 00:08:23',
            'deleted_at'=>NULL
            ] );
            
            
                        
            Post::create( [
            'id'=>4119,
            'cat_id'=>8,
            'tag_id'=>7,
            'title'=>'Atrial Fibrillation Is an Independent Predictor of Mortality in Critically Ill Patients',
            'text'=>'Shaver, Ciara M and others\r\nCritical Care Medicine:\r\nOctober 2015 - Volume 43 - Issue 10 - p 2104â€“2111',
            'img'=>'file-1463591681-AGRM7H.jpg',
            'file'=>NULL,
            'link'=>'http://journals.lww.com/ccmjournal/Abstract/2015/10000/Atrial_Fibrillation_Is_an_Independent_Predictor_of.8.aspx',
            'by'=>0,
            'journal'=>'Site Admin',
            'type'=>0,
            'month_article'=>0,
            'famous'=>0,
            'views'=>1101,
            'show'=>0,
            'date'=>'2015-10-01 08:56:20',
            'active'=>'1',
            'user'=>0,
            'created_at'=>'2020-05-15 01:20:06',
            'updated_at'=>'2020-06-21 00:08:23',
            'deleted_at'=>NULL
            ] );
            
            
                        
            Post::create( [
            'id'=>4120,
            'cat_id'=>8,
            'tag_id'=>7,
            'title'=>'Comparison of Needle Insertion and Guidewire Placement Techniques During Internal Jugular Vein Catheterization: The Thin-Wall Introducer Needle Technique Versus the Cannula-Over-Needle Technique',
            'text'=>'Lee, Yong Hun and others\r\nCritical Care Medicine:\r\nOctober 2015 - Volume 43 - Issue 10 - p 2112â€“2116',
            'img'=>'file-1463591657-K8BRDF.jpg',
            'file'=>NULL,
            'link'=>'http://journals.lww.com/ccmjournal/Abstract/2015/10000/Comparison_of_Needle_Insertion_and_Guidewire.9.aspx',
            'by'=>0,
            'journal'=>'Site Admin',
            'type'=>0,
            'month_article'=>0,
            'famous'=>0,
            'views'=>1101,
            'show'=>0,
            'date'=>'2015-10-01 08:58:22',
            'active'=>'1',
            'user'=>0,
            'created_at'=>'2020-05-15 01:20:06',
            'updated_at'=>'2020-06-21 00:08:23',
            'deleted_at'=>NULL
            ] );
            
            
                        
            Post::create( [
            'id'=>4121,
            'cat_id'=>8,
            'tag_id'=>1,
            'title'=>'Time to Appropriate Antibiotic Therapy Is an Independent Determinant of Postinfection ICU and Hospital Lengths of Stay in Patients With Sepsis',
            'text'=>'Zhang, David and others\r\nCritical Care Medicine:\r\nOctober 2015 - Volume 43 - Issue 10 - p 2133â€“2140',
            'img'=>'file-1463591657-K8BRDF.jpg',
            'file'=>NULL,
            'link'=>'http://journals.lww.com/ccmjournal/Abstract/2015/10000/Time_to_Appropriate_Antibiotic_Therapy_Is_an.11.aspx',
            'by'=>0,
            'journal'=>'Site Admin',
            'type'=>0,
            'month_article'=>0,
            'famous'=>0,
            'views'=>1130,
            'show'=>0,
            'date'=>'2015-10-01 09:00:01',
            'active'=>'1',
            'user'=>0,
            'created_at'=>'2020-05-15 01:20:06',
            'updated_at'=>'2020-06-21 00:08:23',
            'deleted_at'=>NULL
            ] );
            
            
                        
            Post::create( [
            'id'=>4122,
            'cat_id'=>8,
            'tag_id'=>7,
            'title'=>'Practice Patterns and Outcomes Associated With Choice of Initial Vasopressor Therapy for Septic Shock',
            'text'=>'Fawzy, Ashraf and others\r\nCritical Care Medicine:\r\nOctober 2015 - Volume 43 - Issue 10 - p 2141â€“2146',
            'img'=>'file-1463591746-GEE3E7.jpg',
            'file'=>NULL,
            'link'=>'http://journals.lww.com/ccmjournal/Abstract/2015/10000/Practice_Patterns_and_Outcomes_Associated_With.12.aspx',
            'by'=>0,
            'journal'=>'Site Admin',
            'type'=>0,
            'month_article'=>0,
            'famous'=>0,
            'views'=>1101,
            'show'=>0,
            'date'=>'2015-10-01 09:02:14',
            'active'=>'1',
            'user'=>0,
            'created_at'=>'2020-05-15 01:20:06',
            'updated_at'=>'2020-06-21 00:08:23',
            'deleted_at'=>NULL
            ] );
            
            
                        
            Post::create( [
            'id'=>4123,
            'cat_id'=>8,
            'tag_id'=>11,
            'title'=>'â€œItâ€™s Parallel Universesâ€: An Analysis of Communication Between Surgeons and Intensivists',
            'text'=>'Haas, Barbara and others\r\nCritical Care Medicine:\r\nOctober 2015 - Volume 43 - Issue 10 - p 2147â€“2154',
            'img'=>'file-1463591657-K8BRDF.jpg',
            'file'=>NULL,
            'link'=>'http://journals.lww.com/ccmjournal/Abstract/2015/10000/_It_s_Parallel_Universes____An_Analysis_of.13.aspx',
            'by'=>0,
            'journal'=>'Site Admin',
            'type'=>0,
            'month_article'=>0,
            'famous'=>0,
            'views'=>1101,
            'show'=>0,
            'date'=>'2015-10-01 09:03:51',
            'active'=>'1',
            'user'=>0,
            'created_at'=>'2020-05-15 01:20:06',
            'updated_at'=>'2020-06-21 00:08:23',
            'deleted_at'=>NULL
            ] );
            
            
                        
            Post::create( [
            'id'=>4124,
            'cat_id'=>8,
            'tag_id'=>6,
            'title'=>'Lung-Protective Ventilation With Low Tidal Volumes and the Occurrence of Pulmonary Complications in Patients Without Acute Respiratory Distress Syndrome: A Systematic Review and Individual Patient Data Analysis',
            'text'=>'Neto, Ary Serpa and others\r\nCritical Care Medicine:\r\nOctober 2015 - Volume 43 - Issue 10 - p 2155â€“2163',
            'img'=>'file-1463591746-GEE3E7.jpg',
            'file'=>NULL,
            'link'=>'http://journals.lww.com/ccmjournal/Abstract/2015/10000/Lung_Protective_Ventilation_With_Low_Tidal_Volumes.14.aspx',
            'by'=>0,
            'journal'=>'Site Admin',
            'type'=>0,
            'month_article'=>0,
            'famous'=>0,
            'views'=>1101,
            'show'=>0,
            'date'=>'2015-10-01 09:07:02',
            'active'=>'1',
            'user'=>0,
            'created_at'=>'2020-05-15 01:20:06',
            'updated_at'=>'2020-06-21 00:08:23',
            'deleted_at'=>NULL
            ] );
            
            
                        
            Post::create( [
            'id'=>4125,
            'cat_id'=>8,
            'tag_id'=>7,
            'title'=>'Prevalence and Risk Factors of Stress Cardiomyopathy After Convulsive Status Epilepticus in ICU Patients',
            'text'=>'Belcour, Dominique and others\r\nCritical Care Medicine:\r\nOctober 2015 - Volume 43 - Issue 10 - p 2164â€“2170',
            'img'=>'file-1463591738-IZ3XFW.jpg',
            'file'=>NULL,
            'link'=>'http://journals.lww.com/ccmjournal/Abstract/2015/10000/Prevalence_and_Risk_Factors_of_Stress.15.aspx',
            'by'=>0,
            'journal'=>'Site Admin',
            'type'=>0,
            'month_article'=>0,
            'famous'=>0,
            'views'=>1101,
            'show'=>0,
            'date'=>'2015-10-01 09:08:24',
            'active'=>'1',
            'user'=>0,
            'created_at'=>'2020-05-15 01:20:06',
            'updated_at'=>'2020-06-21 00:08:23',
            'deleted_at'=>NULL
            ] );
            
            
                        
            Post::create( [
            'id'=>4126,
            'cat_id'=>8,
            'tag_id'=>2,
            'title'=>'Recall of ICU Stay in Patients Managed With a Sedation Protocol or a Sedation Protocol With Daily Interruption',
            'text'=>'Burry, Lisa and others\r\nCritical Care Medicine:\r\nOctober 2015 - Volume 43 - Issue 10 - p 2180â€“2190',
            'img'=>'file-1463591746-GEE3E7.jpg',
            'file'=>NULL,
            'link'=>'http://journals.lww.com/ccmjournal/Abstract/2015/10000/Recall_of_ICU_Stay_in_Patients_Managed_With_a.17.aspx',
            'by'=>0,
            'journal'=>'Site Admin',
            'type'=>0,
            'month_article'=>0,
            'famous'=>0,
            'views'=>1172,
            'show'=>0,
            'date'=>'2015-10-01 09:16:16',
            'active'=>'1',
            'user'=>0,
            'created_at'=>'2020-05-15 01:20:06',
            'updated_at'=>'2020-06-21 00:08:23',
            'deleted_at'=>NULL
            ] );
            
            
                        
            Post::create( [
            'id'=>4127,
            'cat_id'=>8,
            'tag_id'=>5,
            'title'=>'Mannitol Improves Brain Tissue Oxygenation in a Model of Diffuse Traumatic Brain Injury',
            'text'=>'Schilte, Clotilde and others\r\nCritical Care Medicine:\r\nOctober 2015 - Volume 43 - Issue 10 - p 2212â€“2218',
            'img'=>'file-1463591738-IZ3XFW.jpg',
            'file'=>NULL,
            'link'=>'http://journals.lww.com/ccmjournal/Abstract/2015/10000/Mannitol_Improves_Brain_Tissue_Oxygenation_in_a.20.aspx',
            'by'=>0,
            'journal'=>'Site Admin',
            'type'=>0,
            'month_article'=>0,
            'famous'=>0,
            'views'=>1114,
            'show'=>0,
            'date'=>'2015-10-01 09:21:20',
            'active'=>'1',
            'user'=>0,
            'created_at'=>'2020-05-15 01:20:06',
            'updated_at'=>'2020-06-21 00:08:23',
            'deleted_at'=>NULL
            ] );
            
            
                        
            Post::create( [
            'id'=>4128,
            'cat_id'=>8,
            'tag_id'=>6,
            'title'=>'A novel pump-driven veno-venous gas exchange system during extracorporeal CO2-removal',
            'text'=>'Alexander Hermann and others\r\nIntensive Care Med. October, 2015 4110:1773-1780',
            'img'=>'file-1463591738-IZ3XFW.jpg',
            'file'=>NULL,
            'link'=>'http://icmjournal.esicm.org/journals/abstract.html?v=41&j=134&i=10&a=3957_10.1007_s00134-015-3957-0&doi=',
            'by'=>0,
            'journal'=>'Site Admin',
            'type'=>0,
            'month_article'=>0,
            'famous'=>0,
            'views'=>1101,
            'show'=>0,
            'date'=>'2015-10-01 09:25:49',
            'active'=>'1',
            'user'=>0,
            'created_at'=>'2020-05-15 01:20:06',
            'updated_at'=>'2020-06-21 00:08:23',
            'deleted_at'=>NULL
            ] );
            
            
                        
            Post::create( [
            'id'=>4129,
            'cat_id'=>8,
            'tag_id'=>1,
            'title'=>'Respective impact of lowering body temperature and heart rate on mortality in septic shock: mediation analysis of a randomized trial',
            'text'=>'FrÃ©dÃ©rique Schortgen and others\r\nIntensive Care Med. October, 2015 4110:1800 - 1808',
            'img'=>'file-1463591752-XK911T.jpg',
            'file'=>NULL,
            'link'=>'http://icmjournal.esicm.org/journals/abstract.html?v=41&j=134&i=10&a=3987_10.1007_s00134-015-3987-7&doi=',
            'by'=>0,
            'journal'=>'Site Admin',
            'type'=>0,
            'month_article'=>0,
            'famous'=>0,
            'views'=>1130,
            'show'=>0,
            'date'=>'2015-10-01 09:28:46',
            'active'=>'1',
            'user'=>0,
            'created_at'=>'2020-05-15 01:20:06',
            'updated_at'=>'2020-06-21 00:08:23',
            'deleted_at'=>NULL
            ] );
            
            
                        
            Post::create( [
            'id'=>4130,
            'cat_id'=>8,
            'tag_id'=>1,
            'title'=>'Pros and cons of using biomarkers versus clinical decisions in start and stop decisions for antibiotics in the critical care setting',
            'text'=>'Werner C. Albrich, Stephan Harbarth\r\nIntensive Care Med. October, 2015 4110:1739 - 1751',
            'img'=>'file-1463591639-4YG8JY.jpg',
            'file'=>NULL,
            'link'=>'http://icmjournal.esicm.org/journals/abstract.html?v=41&j=134&i=10&a=3978_10.1007_s00134-015-3978-8&doi=',
            'by'=>0,
            'journal'=>'Site Admin',
            'type'=>0,
            'month_article'=>0,
            'famous'=>0,
            'views'=>1130,
            'show'=>0,
            'date'=>'2015-10-01 09:30:14',
            'active'=>'1',
            'user'=>0,
            'created_at'=>'2020-05-15 01:20:06',
            'updated_at'=>'2020-06-21 00:08:23',
            'deleted_at'=>NULL
            ] );
            
            
                        
            Post::create( [
            'id'=>4131,
            'cat_id'=>8,
            'tag_id'=>6,
            'title'=>'Extracorporeal carbon dioxide removal in patients with chronic obstructive pulmonary disease: a systematic review',
            'text'=>'Michael C. Sklar and others\r\nIntensive Care Med. October, 2015 4110:1752 - 1762',
            'img'=>'file-1463591657-K8BRDF.jpg',
            'file'=>NULL,
            'link'=>'http://icmjournal.esicm.org/journals/abstract.html?v=41&j=134&i=10&a=3921_10.1007_s00134-015-3921-z&doi=',
            'by'=>0,
            'journal'=>'Site Admin',
            'type'=>0,
            'month_article'=>0,
            'famous'=>0,
            'views'=>1101,
            'show'=>0,
            'date'=>'2015-10-01 09:32:06',
            'active'=>'1',
            'user'=>0,
            'created_at'=>'2020-05-15 01:20:06',
            'updated_at'=>'2020-06-21 00:08:23',
            'deleted_at'=>NULL
            ] );
            
            
                        
            Post::create( [
            'id'=>4132,
            'cat_id'=>8,
            'tag_id'=>4,
            'title'=>'In-hospital outcomes and costs of surgical stabilization versus nonoperative management of severe rib fractures',
            'text'=>'Majercik, Sarah and others\r\nJournal of Trauma and Acute Care Surgery:\r\nOctober 2015 - Volume 79 - Issue 4 - p 533â€“539',
            'img'=>'file-1463591639-4YG8JY.jpg',
            'file'=>NULL,
            'link'=>'http://journals.lww.com/jtrauma/Abstract/2015/10000/In_hospital_outcomes_and_costs_of_surgical.2.aspx',
            'by'=>0,
            'journal'=>'Site Admin',
            'type'=>0,
            'month_article'=>0,
            'famous'=>0,
            'views'=>1198,
            'show'=>0,
            'date'=>'2015-10-01 09:36:33',
            'active'=>'1',
            'user'=>0,
            'created_at'=>'2020-05-15 01:20:06',
            'updated_at'=>'2020-06-21 00:08:23',
            'deleted_at'=>NULL
            ] );
            
            
                        
            Post::create( [
            'id'=>4133,
            'cat_id'=>8,
            'tag_id'=>4,
            'title'=>'Causes of death differ between elderly and adult falls',
            'text'=>'Allen, Casey J and others\r\nJournal of Trauma and Acute Care Surgery:\r\nOctober 2015 - Volume 79 - Issue 4 - p 617â€“621',
            'img'=>'file-1463591639-4YG8JY.jpg',
            'file'=>NULL,
            'link'=>'http://journals.lww.com/jtrauma/Abstract/2015/10000/Causes_of_death_differ_between_elderly_and_adult.13.aspx',
            'by'=>0,
            'journal'=>'Site Admin',
            'type'=>0,
            'month_article'=>0,
            'famous'=>0,
            'views'=>1198,
            'show'=>0,
            'date'=>'2015-10-01 09:39:17',
            'active'=>'1',
            'user'=>0,
            'created_at'=>'2020-05-15 01:20:06',
            'updated_at'=>'2020-06-21 00:08:23',
            'deleted_at'=>NULL
            ] );
            
            
                        
            Post::create( [
            'id'=>4134,
            'cat_id'=>8,
            'tag_id'=>4,
            'title'=>'Nonoperative management of blunt hepatic trauma: A systematic review',
            'text'=>'Boese, Christoph Kolja and others\r\nJournal of Trauma and Acute Care Surgery:\r\nOctober 2015 - Volume 79 - Issue 4 - p 654â€“660',
            'img'=>'file-1463591732-SM39NY.jpg',
            'file'=>NULL,
            'link'=>'http://journals.lww.com/jtrauma/Abstract/2015/10000/Nonoperative_management_of_blunt_hepatic_trauma__.19.aspx',
            'by'=>0,
            'journal'=>'Site Admin',
            'type'=>0,
            'month_article'=>0,
            'famous'=>0,
            'views'=>1198,
            'show'=>0,
            'date'=>'2015-10-01 09:41:59',
            'active'=>'1',
            'user'=>0,
            'created_at'=>'2020-05-15 01:20:06',
            'updated_at'=>'2020-06-21 00:08:23',
            'deleted_at'=>NULL
            ] );
            
            
                        
            Post::create( [
            'id'=>4135,
            'cat_id'=>8,
            'tag_id'=>7,
            'title'=>'Successful Resuscitation From In-Hospital Cardiac Arrestâ€”What Happens Next',
            'text'=>'Derek C. Angus, MD, MPH\r\nJAMA. 201531412:1238-1239',
            'img'=>'file-1463591657-K8BRDF.jpg',
            'file'=>NULL,
            'link'=>'http://jama.jamanetwork.com/article.aspx?articleid=2442916',
            'by'=>0,
            'journal'=>'Site Admin',
            'type'=>0,
            'month_article'=>0,
            'famous'=>0,
            'views'=>1101,
            'show'=>0,
            'date'=>'2015-10-01 09:45:52',
            'active'=>'1',
            'user'=>0,
            'created_at'=>'2020-05-15 01:20:06',
            'updated_at'=>'2020-06-21 00:08:23',
            'deleted_at'=>NULL
            ] );
            
            
                        
            Post::create( [
            'id'=>4136,
            'cat_id'=>8,
            'tag_id'=>1,
            'title'=>'Single Dose of Ebola Vaccine May Be Protective',
            'text'=>'Anita Slomski, MA\r\nJAMA. 201531412:1217',
            'img'=>'file-1463591732-SM39NY.jpg',
            'file'=>NULL,
            'link'=>'http://jama.jamanetwork.com/article.aspx?articleid=2442934',
            'by'=>0,
            'journal'=>'Site Admin',
            'type'=>0,
            'month_article'=>0,
            'famous'=>0,
            'views'=>1130,
            'show'=>0,
            'date'=>'2015-10-01 10:20:28',
            'active'=>'1',
            'user'=>0,
            'created_at'=>'2020-05-15 01:20:06',
            'updated_at'=>'2020-06-21 00:08:23',
            'deleted_at'=>NULL
            ] );
            
            
                        
            Post::create( [
            'id'=>4137,
            'cat_id'=>8,
            'tag_id'=>8,
            'title'=>'Major bleeding in hemodialysis patients using unfractionated or low molecular weight heparin: a single-center study',
            'text'=>'Nadarajah L, Fan S, Forbes S, Ashman N.\r\nClin Nephrol. 2015 Sep 14',
            'img'=>'file-1463591681-AGRM7H.jpg',
            'file'=>NULL,
            'link'=>'http://www.ntkinstitute.org/news/content.nsf/PaperFrameSet?OpenForm&pp=1&id=DF5950353CA4263E85256FDB00692E0A&refid=4677&specid=77&newsid=D7A77F7A388ABABD85257ED0004C8B99&locref=ntkwatch&u=http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&db=PubMed&dopt=Abstract&list_uids=26365216',
            'by'=>0,
            'journal'=>'Site Admin',
            'type'=>0,
            'month_article'=>0,
            'famous'=>0,
            'views'=>1152,
            'show'=>0,
            'date'=>'2015-09-14 11:17:25',
            'active'=>'1',
            'user'=>0,
            'created_at'=>'2020-05-15 01:20:06',
            'updated_at'=>'2020-06-21 00:08:23',
            'deleted_at'=>NULL
            ] );
            
            
                        
            Post::create( [
            'id'=>4138,
            'cat_id'=>8,
            'tag_id'=>7,
            'title'=>'Echocardiography for adult patients supported with extracorporeal membrane oxygenation',
            'text'=>'DouflÃ© G, Roscoe A, Billia F and Fan E\r\nCritical Care 2015, 19:326 2 October 2015',
            'img'=>'file-1463591650-J14MMU.jpg',
            'file'=>NULL,
            'link'=>'http://www.ccforum.com/content/19/1/326',
            'by'=>0,
            'journal'=>'Site Admin',
            'type'=>0,
            'month_article'=>0,
            'famous'=>0,
            'views'=>1101,
            'show'=>0,
            'date'=>'2015-10-01 22:07:28',
            'active'=>'1',
            'user'=>0,
            'created_at'=>'2020-05-15 01:20:06',
            'updated_at'=>'2020-06-21 00:08:23',
            'deleted_at'=>NULL
            ] );
            
            
                        
            Post::create( [
            'id'=>4139,
            'cat_id'=>8,
            'tag_id'=>6,
            'title'=>'Postextubation laryngeal edema and stridor resulting in respiratory failure in critically ill adult patients: updated review',
            'text'=>'Wouter Pluijms, Walther van Mook, Bastiaan Wittekamp, Dennis Bergmans\r\nCritical Care 2015, 19:295 23 September 2015',
            'img'=>'file-1463591746-GEE3E7.jpg',
            'file'=>NULL,
            'link'=>'http://www.ccforum.com/content/19/1/295',
            'by'=>0,
            'journal'=>'Site Admin',
            'type'=>0,
            'month_article'=>0,
            'famous'=>0,
            'views'=>1101,
            'show'=>0,
            'date'=>'2015-09-22 22:09:37',
            'active'=>'1',
            'user'=>0,
            'created_at'=>'2020-05-15 01:20:06',
            'updated_at'=>'2020-06-21 00:08:23',
            'deleted_at'=>NULL
            ] );
            
            
                        
            Post::create( [
            'id'=>4140,
            'cat_id'=>8,
            'tag_id'=>1,
            'title'=>'Treatment for infections with carbapenem-resistant Enterobacteriaceae: what options do we still have',
            'text'=>'Michele Yamamoto and Aurora E Pop-Vicas\r\nCritical Care 2014, 18:229 27 June 2014',
            'img'=>'file-1463591738-IZ3XFW.jpg',
            'file'=>NULL,
            'link'=>'http://www.ccforum.com/content/18/3/229',
            'by'=>0,
            'journal'=>'Site Admin',
            'type'=>0,
            'month_article'=>0,
            'famous'=>1,
            'views'=>1225,
            'show'=>0,
            'date'=>'2015-06-26 22:21:02',
            'active'=>'1',
            'user'=>0,
            'created_at'=>'2020-05-15 01:20:06',
            'updated_at'=>'2020-06-21 00:08:23',
            'deleted_at'=>NULL
            ] );
            
            
                        
            Post::create( [
            'id'=>4141,
            'cat_id'=>8,
            'tag_id'=>11,
            'title'=>'Toward Evidence-Based End-of-Life Care  Online First',
            'text'=>'S.D. Halpern \r\nN Engl J Med October 14, 2015',
            'img'=>'file-1463591752-XK911T.jpg',
            'file'=>NULL,
            'link'=>'http://www.nejm.org/doi/full/10.1056/NEJMp1509664',
            'by'=>0,
            'journal'=>'Site Admin',
            'type'=>0,
            'month_article'=>0,
            'famous'=>0,
            'views'=>1101,
            'show'=>0,
            'date'=>'2015-10-14 08:46:49',
            'active'=>'1',
            'user'=>0,
            'created_at'=>'2020-05-15 01:20:06',
            'updated_at'=>'2020-06-21 00:08:23',
            'deleted_at'=>NULL
            ] );
            
            
                        
            Post::create( [
            'id'=>4142,
            'cat_id'=>9,
            'tag_id'=>6,
            'title'=>'OPTIMAL PEEP DETERMINATION',
            'text'=>'Kevin T. Martin \r\nC Educational Consulting Services, Inc. 16781 Van Buren Blvd, Suite B,Riverside, CA 92504-5798 800 441-LUNG / 877 367-NURS \r\nwww.RCECS.com',
            'img'=>'file-1463591725-9J88SF.jpg',
            'file'=>NULL,
            'link'=>'http://www.rcecs.com/MyCE/PDFDocs/course/V7022.pdf',
            'by'=>0,
            'journal'=>'Site Admin',
            'type'=>0,
            'month_article'=>0,
            'famous'=>0,
            'views'=>1101,
            'show'=>0,
            'date'=>'2009-10-01 13:06:27',
            'active'=>'1',
            'user'=>0,
            'created_at'=>'2020-05-15 01:20:06',
            'updated_at'=>'2020-06-21 00:08:23',
            'deleted_at'=>NULL
            ] );
            
            
                        
            Post::create( [
            'id'=>4143,
            'cat_id'=>12,
            'tag_id'=>1,
            'title'=>'Acetaminophen for Fever in Critically Ill Patients with Suspected Infection',
            'text'=>'P. Young and Others for the HEAT Investigators\r\nNEJM October 5, 2015',
            'img'=>'file-1463591685-DDLMK9.jpg',
            'file'=>NULL,
            'link'=>'http://www.nejm.org/doi/full/10.1056/NEJMoa1508375',
            'by'=>0,
            'journal'=>'Site Admin',
            'type'=>0,
            'month_article'=>0,
            'famous'=>0,
            'views'=>1241,
            'show'=>0,
            'date'=>'2015-10-05 09:01:22',
            'active'=>'1',
            'user'=>0,
            'created_at'=>'2020-05-15 01:20:06',
            'updated_at'=>'2020-06-21 00:08:23',
            'deleted_at'=>NULL
            ] );
            
            
                        
            Post::create( [
            'id'=>4144,
            'cat_id'=>12,
            'tag_id'=>1,
            'title'=>'Is prolonged infusion of piperacillin/tazobactam and meropenem in critically ill patients associated with improved pharmacokinetic/pharmacodynamic and patient outcomes An observation from the Defining Antibiotic Levels in Intensive care unit patients DALI',
            'text'=>'Mohd H. Abdul-Aziz and others on behalf of the DALI Study Group\r\nJ. Antimicrob. Chemother. published 3 October 2015',
            'img'=>'file-1463591738-IZ3XFW.jpg',
            'file'=>NULL,
            'link'=>'http://jac.oxfordjournals.org/content/early/2015/10/02/jac.dkv288.abstract?papetoc',
            'by'=>0,
            'journal'=>'Site Admin',
            'type'=>0,
            'month_article'=>0,
            'famous'=>0,
            'views'=>1241,
            'show'=>0,
            'date'=>'2015-10-04 09:01:32',
            'active'=>'1',
            'user'=>0,
            'created_at'=>'2020-05-15 01:20:06',
            'updated_at'=>'2020-06-21 00:08:23',
            'deleted_at'=>NULL
            ] );
            
            
                        
            Post::create( [
            'id'=>4145,
            'cat_id'=>8,
            'tag_id'=>1,
            'title'=>'Impact of 30 mg/kg amikacin and 8 mg/kg gentamicin on serum concentrations in critically ill patients with severe sepsis',
            'text'=>'Claire Roger and others\r\nJ. Antimicrob. Chemother. published 1 October 2015',
            'img'=>'file-1463591681-AGRM7H.jpg',
            'file'=>NULL,
            'link'=>'http://jac.oxfordjournals.org/content/early/2015/09/30/jac.dkv291.abstract?papetoc',
            'by'=>0,
            'journal'=>'Site Admin',
            'type'=>0,
            'month_article'=>0,
            'famous'=>0,
            'views'=>1130,
            'show'=>0,
            'date'=>'2015-10-01 08:33:07',
            'active'=>'1',
            'user'=>0,
            'created_at'=>'2020-05-15 01:20:06',
            'updated_at'=>'2020-06-21 00:08:23',
            'deleted_at'=>NULL
            ] );
            
            
                        
            Post::create( [
            'id'=>4146,
            'cat_id'=>12,
            'tag_id'=>1,
            'title'=>'Acetaminophen for Fever in Critically Ill Patients with Suspected Infection',
            'text'=>'P. Young and Others for the HEAT Investigators\r\nNEJM October 5, 2015',
            'img'=>'file-1463591725-9J88SF.jpg',
            'file'=>NULL,
            'link'=>'http://www.nejm.org/doi/full/10.1056/NEJMoa1508375',
            'by'=>0,
            'journal'=>'Site Admin',
            'type'=>0,
            'month_article'=>0,
            'famous'=>0,
            'views'=>1241,
            'show'=>0,
            'date'=>'0000-00-00 00:00:00',
            'active'=>'1',
            'user'=>0,
            'created_at'=>'2020-05-15 01:20:06',
            'updated_at'=>'2020-06-21 00:08:23',
            'deleted_at'=>NULL
            ] );
            
            
                        
            Post::create( [
            'id'=>4147,
            'cat_id'=>12,
            'tag_id'=>5,
            'title'=>'Hypothermia for Intracranial Hypertension after Traumatic Brain Injury',
            'text'=>'P.J.D. Andrews and Others,  for the Eurotherm3235 Trial Collaborators\r\nN Engl J Med October 7, 2015',
            'img'=>'file-1463591650-J14MMU.jpg',
            'file'=>NULL,
            'link'=>'http://www.nejm.org/doi/full/10.1056/NEJMoa1507581',
            'by'=>0,
            'journal'=>'Site Admin',
            'type'=>0,
            'month_article'=>0,
            'famous'=>1,
            'views'=>1320,
            'show'=>0,
            'date'=>'2015-10-10 06:46:42',
            'active'=>'1',
            'user'=>0,
            'created_at'=>'2020-05-15 01:20:06',
            'updated_at'=>'2020-06-21 00:08:23',
            'deleted_at'=>NULL
            ] );
            
            
                        
            Post::create( [
            'id'=>4148,
            'cat_id'=>8,
            'tag_id'=>1,
            'title'=>'Invasive Candidiasis',
            'text'=>'Bart Jan Kullberg, and Maiken C. Arendrup\r\n\r\nN Engl J Med 373:1445-1456. October 8, 2015',
            'img'=>'file-1463591657-K8BRDF.jpg',
            'file'=>NULL,
            'link'=>'http://www.nejm.org/doi/full/10.1056/NEJMra1315399',
            'by'=>0,
            'journal'=>'Site Admin',
            'type'=>0,
            'month_article'=>0,
            'famous'=>0,
            'views'=>1130,
            'show'=>0,
            'date'=>'2015-10-06 06:46:04',
            'active'=>'1',
            'user'=>0,
            'created_at'=>'2020-05-15 01:20:06',
            'updated_at'=>'2020-06-21 00:08:23',
            'deleted_at'=>NULL
            ] );
            
            
                        
            Post::create( [
            'id'=>4149,
            'cat_id'=>8,
            'tag_id'=>7,
            'title'=>'A Multicenter Trial of Remote Ischemic Preconditioning for Heart Surgery',
            'text'=>'P. Meybohm and Others, or the RIPHeart Study Collaborators\r\nN Engl J Med 2015 373:1397-1407, October 8, 2015',
            'img'=>'file-1463591644-ZGN2DA.jpg',
            'file'=>NULL,
            'link'=>'http://www.nejm.org/doi/full/10.1056/NEJMoa1413579',
            'by'=>0,
            'journal'=>'Site Admin',
            'type'=>0,
            'month_article'=>0,
            'famous'=>0,
            'views'=>1101,
            'show'=>0,
            'date'=>'2015-10-08 06:49:14',
            'active'=>'1',
            'user'=>0,
            'created_at'=>'2020-05-15 01:20:06',
            'updated_at'=>'2020-06-21 00:08:23',
            'deleted_at'=>NULL
            ] );
            
            
                        
            Post::create( [
            'id'=>4150,
            'cat_id'=>8,
            'tag_id'=>5,
            'title'=>'Getting Warmer on Critical Care for Head Injury',
            'text'=>'C.S. Robertson and A.H. Ropper\r\nN Engl J Med, October 7, 2015',
            'img'=>'file-1463591650-J14MMU.jpg',
            'file'=>NULL,
            'link'=>'http://www.nejm.org/doi/full/10.1056/NEJMe1511174',
            'by'=>0,
            'journal'=>'Site Admin',
            'type'=>0,
            'month_article'=>0,
            'famous'=>0,
            'views'=>1114,
            'show'=>0,
            'date'=>'2015-10-07 06:55:01',
            'active'=>'1',
            'user'=>0,
            'created_at'=>'2020-05-15 01:20:06',
            'updated_at'=>'2020-06-21 00:08:23',
            'deleted_at'=>NULL
            ] );
            
            
                        
            Post::create( [
            'id'=>4151,
            'cat_id'=>10,
            'tag_id'=>6,
            'title'=>'A PROTOCOL FOR PRONING',
            'text'=>'Martin Cieslak \r\nCriticalcarelondon.ca',
            'img'=>'file-1463591752-XK911T.jpg',
            'file'=>NULL,
            'link'=>'http://www.icugate.com/admin/index.php?cmd=icugate__add_link&article_id=12&action=protocols',
            'by'=>0,
            'journal'=>'Site Admin',
            'type'=>0,
            'month_article'=>0,
            'famous'=>0,
            'views'=>1101,
            'show'=>0,
            'date'=>'2014-10-01 18:14:04',
            'active'=>'1',
            'user'=>0,
            'created_at'=>'2020-05-15 01:20:06',
            'updated_at'=>'2020-06-21 00:08:23',
            'deleted_at'=>NULL
            ] );
            
            
                        
            Post::create( [
            'id'=>4152,
            'cat_id'=>8,
            'tag_id'=>1,
            'title'=>'Effects of tolvaptan in the early postoperative stage after heart valve surgery: results of the STAR Study of Tolvaptan for fluid retention AfteR valve surgery trial.',
            'text'=>'Nishi H and others\r\nSurg Today. 2015 Sep 28',
            'img'=>'file-1463591681-AGRM7H.jpg',
            'file'=>NULL,
            'link'=>'http://link.springer.com/article/10.1007/s00595-015-1251-y',
            'by'=>0,
            'journal'=>'Site Admin',
            'type'=>0,
            'month_article'=>0,
            'famous'=>1,
            'views'=>1225,
            'show'=>0,
            'date'=>'2015-09-25 05:58:24',
            'active'=>'1',
            'user'=>0,
            'created_at'=>'2020-05-15 01:20:06',
            'updated_at'=>'2020-06-21 00:08:23',
            'deleted_at'=>NULL
            ] );
            
            
                        
            Post::create( [
            'id'=>4153,
            'cat_id'=>12,
            'tag_id'=>7,
            'title'=>'Executive Summary: 2015 International Consensus on Cardiopulmonary Resuscitation and Emergency Cardiovascular Care Science With Treatment Recommendations',
            'text'=>'Mary Fran Hazinski and others\r\nCirculation. 2015132:S2-S39',
            'img'=>'file-1463591681-AGRM7H.jpg',
            'file'=>NULL,
            'link'=>'http://circ.ahajournals.org/content/132/16_suppl_1/S2.full',
            'by'=>0,
            'journal'=>'Site Admin',
            'type'=>0,
            'month_article'=>0,
            'famous'=>0,
            'views'=>1212,
            'show'=>0,
            'date'=>'2015-10-15 07:50:20',
            'active'=>'1',
            'user'=>0,
            'created_at'=>'2020-05-15 01:20:06',
            'updated_at'=>'2020-06-21 00:08:23',
            'deleted_at'=>NULL
            ] );
            
            
                        
            Post::create( [
            'id'=>4154,
            'cat_id'=>9,
            'tag_id'=>7,
            'title'=>'2015 International Consensus on Cardiopulmonary Resuscitation and Emergency Cardiovascular Care Science With Treatment Recommendations',
            'text'=>'Vinay M. Nadkarni and others\r\nCirculation. 2015132:S1',
            'img'=>'file-1463591681-AGRM7H.jpg',
            'file'=>NULL,
            'link'=>'http://circ.ahajournals.org/content/132/16_suppl_1/S1.full.pdf+html',
            'by'=>0,
            'journal'=>'Site Admin',
            'type'=>0,
            'month_article'=>0,
            'famous'=>0,
            'views'=>1101,
            'show'=>0,
            'date'=>'2015-10-15 13:01:09',
            'active'=>'1',
            'user'=>0,
            'created_at'=>'2020-05-15 01:20:06',
            'updated_at'=>'2020-06-21 00:08:23',
            'deleted_at'=>NULL
            ] );
            
            
                        
            Post::create( [
            'id'=>4155,
            'cat_id'=>8,
            'tag_id'=>7,
            'title'=>'Off-Label Use of Agents for Management of Serious or Life-threatening Angiotensin Converting Enzyme Inhibitor-Induced Angioedema.',
            'text'=>'Culley CM, DiBridge JN, Wilson GL Jr\r\nAnn Pharmacother. 2015 Sep 28',
            'img'=>'file-1463591681-AGRM7H.jpg',
            'file'=>NULL,
            'link'=>'http://aop.sagepub.com/content/early/2015/09/25/1060028015607037.abstract',
            'by'=>0,
            'journal'=>'Site Admin',
            'type'=>0,
            'month_article'=>0,
            'famous'=>1,
            'views'=>1196,
            'show'=>0,
            'date'=>'2015-10-01 07:42:01',
            'active'=>'1',
            'user'=>0,
            'created_at'=>'2020-05-15 01:20:06',
            'updated_at'=>'2020-06-21 00:08:23',
            'deleted_at'=>NULL
            ] );
            
            
                        
            Post::create( [
            'id'=>4156,
            'cat_id'=>8,
            'tag_id'=>5,
            'title'=>'Nimodipine in aneurysmal subarachnoid hemorrhage: a randomized study of intravenous or peroral administration',
            'text'=>'Kronvall E1, UndrÃ©n P, Romner B, SÃ¤veland H, Cronqvist M, Nilsson OG\r\nJ Neurosurg. 2009 Jan1101:58-63',
            'img'=>'file-1463591732-SM39NY.jpg',
            'file'=>NULL,
            'link'=>'http://www.ntkinstitute.org/news/content.nsf/PaperFrameSet?OpenForm&pp=1&id=DF5950353CA4263E85256FDB00692E0A&refid=4688&specid=106&newsid=837523331CF77C7F85257EDF0049CBCA&locref=ntkwatch&u=http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=Retrieve&db=PubMed&dopt=Abstract&list_uids=26416949',
            'by'=>0,
            'journal'=>'Site Admin',
            'type'=>0,
            'month_article'=>0,
            'famous'=>1,
            'views'=>1209,
            'show'=>0,
            'date'=>'2009-01-01 07:45:02',
            'active'=>'1',
            'user'=>0,
            'created_at'=>'2020-05-15 01:20:06',
            'updated_at'=>'2020-06-21 00:08:23',
            'deleted_at'=>NULL
            ] );
            
            
                        
            Post::create( [
            'id'=>4157,
            'cat_id'=>8,
            'tag_id'=>1,
            'title'=>'Prognostic significance of the angiopoietin-2/angiopoietin-1 and angiopoietin-1/Tie-2 ratios for early sepsis in an emergency department',
            'text'=>'Fang Y, Li C, Shao R, Yu H, Zhang Q, Zhao L\r\nCritical Care 2015, 19 :367 14 October 2015',
            'img'=>'file-1463591639-4YG8JY.jpg',
            'file'=>NULL,
            'link'=>'http://www.ccforum.com/content/19/1/367/abstract',
            'by'=>0,
            'journal'=>'Site Admin',
            'type'=>0,
            'month_article'=>0,
            'famous'=>0,
            'views'=>1130,
            'show'=>0,
            'date'=>'2015-10-14 07:47:04',
            'active'=>'1',
            'user'=>0,
            'created_at'=>'2020-05-15 01:20:06',
            'updated_at'=>'2020-06-21 00:08:23',
            'deleted_at'=>NULL
            ] );
            
            
                        
            Post::create( [
            'id'=>4158,
            'cat_id'=>8,
            'tag_id'=>10,
            'title'=>'Inhaled nitric oxide for acute chest syndrome in adult sickle cell patients: a randomized controlled study',
            'text'=>'B. Maitre and others\r\nIntensive Care Med. December 20154112:2121-2129',
            'img'=>'file-1463591685-DDLMK9.jpg',
            'file'=>NULL,
            'link'=>'http://icmjournal.esicm.org/journals/abstract.html?v=41&j=134&i=12&a=4060_10.1007_s00134-015-4060-2&doi=',
            'by'=>0,
            'journal'=>'Site Admin',
            'type'=>0,
            'month_article'=>0,
            'famous'=>0,
            'views'=>1101,
            'show'=>0,
            'date'=>'2015-12-01 07:52:51',
            'active'=>'1',
            'user'=>0,
            'created_at'=>'2020-05-15 01:20:06',
            'updated_at'=>'2020-06-21 00:08:23',
            'deleted_at'=>NULL
            ] );
            
            
                        
            Post::create( [
            'id'=>4159,
            'cat_id'=>8,
            'tag_id'=>6,
            'title'=>'Diagnostic accuracy of C-reactive protein and procalcitonin in suspected community-acquired pneumonia adults visiting emergency department and having a systematic thoracic CT scan',
            'text'=>'Le Bel J and others\r\nCritical Care 2015, 19 :366 16 October 2015',
            'img'=>'file-1463591644-ZGN2DA.jpg',
            'file'=>NULL,
            'link'=>'http://www.ccforum.com/content/19/1/366/abstract',
            'by'=>0,
            'journal'=>'Site Admin',
            'type'=>0,
            'month_article'=>0,
            'famous'=>0,
            'views'=>1101,
            'show'=>0,
            'date'=>'2015-10-16 06:31:58',
            'active'=>'1',
            'user'=>0,
            'created_at'=>'2020-05-15 01:20:06',
            'updated_at'=>'2020-06-21 00:08:23',
            'deleted_at'=>NULL
            ] );
            
            
                        
            Post::create( [
            'id'=>4160,
            'cat_id'=>8,
            'tag_id'=>7,
            'title'=>'Updated European Resuscitation Council guidelines published',
            'text'=>'www.medicalnewstoday.com\r\nOctober 16, 2015',
            'img'=>'file-1463591752-XK911T.jpg',
            'file'=>NULL,
            'link'=>'http://www.medicalnewstoday.com/releases/301074.php',
            'by'=>0,
            'journal'=>'Site Admin',
            'type'=>0,
            'month_article'=>0,
            'famous'=>0,
            'views'=>1101,
            'show'=>0,
            'date'=>'2015-10-16 06:37:15',
            'active'=>'1',
            'user'=>0,
            'created_at'=>'2020-05-15 01:20:06',
            'updated_at'=>'2020-06-21 00:08:23',
            'deleted_at'=>NULL
            ] );
            
            
                        
            Post::create( [
            'id'=>4161,
            'cat_id'=>9,
            'tag_id'=>7,
            'title'=>'European Resuscitation Council Guidelines for Resuscitation 2015',
            'text'=>'Koenraad G and others on behalf of the ERC Guidelines 2015 Writing Group\r\nResuscitation, October 2015, 95:1â€“80',
            'img'=>'file-1463591657-K8BRDF.jpg',
            'file'=>NULL,
            'link'=>'http://www.resuscitationjournal.com/article/S0300-9572%2815%2900350-0/pdf',
            'by'=>0,
            'journal'=>'Site Admin',
            'type'=>0,
            'month_article'=>0,
            'famous'=>0,
            'views'=>1101,
            'show'=>0,
            'date'=>'2015-10-18 06:49:05',
            'active'=>'1',
            'user'=>0,
            'created_at'=>'2020-05-15 01:20:06',
            'updated_at'=>'2020-06-21 00:08:23',
            'deleted_at'=>NULL
            ] );
            
            
                        
            Post::create( [
            'id'=>4162,
            'cat_id'=>8,
            'tag_id'=>10,
            'title'=>'The efficacy of recombinant human soluble thrombomodulin for obstetric disseminated intravascular coagulation: a retrospective study',
            'text'=>'Yoshihara M, Uno K, Tano S, Mayama M, Ukai M, Kondo S, Kokabu T, Kishigami Y, Oguchi H\r\nCritical Care 2015, 19 :369 20 October 2015',
            'img'=>'file-1463591681-AGRM7H.jpg',
            'file'=>NULL,
            'link'=>'http://www.ccforum.com/content/19/1/369/abstract',
            'by'=>0,
            'journal'=>'Site Admin',
            'type'=>0,
            'month_article'=>0,
            'famous'=>0,
            'views'=>1101,
            'show'=>0,
            'date'=>'2015-10-20 08:32:29',
            'active'=>'1',
            'user'=>0,
            'created_at'=>'2020-05-15 01:20:06',
            'updated_at'=>'2020-06-21 00:08:23',
            'deleted_at'=>NULL
            ] );
            
            
                        
            Post::create( [
            'id'=>4163,
            'cat_id'=>8,
            'tag_id'=>10,
            'title'=>'Efficacy of recombinant human soluble thrombomodulin in patients with sepsis and disseminated intravascular coagulation in the gastroenterology field',
            'text'=>'Ito T and others\r\nBiomed Rep. 2015 Jul34:457-460',
            'img'=>'file-1463591738-IZ3XFW.jpg',
            'file'=>NULL,
            'link'=>'http://www.ncbi.nlm.nih.gov/pubmed/26171148',
            'by'=>0,
            'journal'=>'Site Admin',
            'type'=>0,
            'month_article'=>0,
            'famous'=>0,
            'views'=>1101,
            'show'=>0,
            'date'=>'2015-07-03 08:52:55',
            'active'=>'1',
            'user'=>0,
            'created_at'=>'2020-05-15 01:20:06',
            'updated_at'=>'2020-06-21 00:08:23',
            'deleted_at'=>NULL
            ] );
            
            
                        
            Post::create( [
            'id'=>4164,
            'cat_id'=>8,
            'tag_id'=>9,
            'title'=>'Efficacy of recombinant human soluble thrombomodulin in preventing walled-off necrosis in severe acute pancreatitis patients',
            'text'=>'Takaaki Eguch and others\r\nPancreatology\r\nVolume 15, Issue 5, Septemberâ€“October 2015, Pages 485â€“490',
            'img'=>'file-1463591752-XK911T.jpg',
            'file'=>NULL,
            'link'=>'http://www.sciencedirect.com/science/article/pii/S1424390315006274',
            'by'=>0,
            'journal'=>'Site Admin',
            'type'=>0,
            'month_article'=>0,
            'famous'=>0,
            'views'=>1101,
            'show'=>0,
            'date'=>'2015-10-01 08:56:55',
            'active'=>'1',
            'user'=>0,
            'created_at'=>'2020-05-15 01:20:06',
            'updated_at'=>'2020-06-21 00:08:23',
            'deleted_at'=>NULL
            ] );
            
            
                        
            Post::create( [
            'id'=>4165,
            'cat_id'=>8,
            'tag_id'=>10,
            'title'=>'Use of Recombinant Human Soluble Thrombomodulin in Patients with Sepsis-Induced Disseminated Intravascular Coagulation after Intestinal Perforation',
            'text'=>'Takashi Tagam and others\r\nFront Med Lausanne. 2015 2: 7.\r\nPublished online 2015 Feb 26',
            'img'=>'file-1463591725-9J88SF.jpg',
            'file'=>NULL,
            'link'=>'http://www.ncbi.nlm.nih.gov/pmc/articles/PMC4341430/',
            'by'=>0,
            'journal'=>'Site Admin',
            'type'=>0,
            'month_article'=>0,
            'famous'=>0,
            'views'=>1101,
            'show'=>0,
            'date'=>'2015-02-26 09:00:34',
            'active'=>'1',
            'user'=>0,
            'created_at'=>'2020-05-15 01:20:06',
            'updated_at'=>'2020-06-21 00:08:23',
            'deleted_at'=>NULL
            ] );
            
            
                        
            Post::create( [
            'id'=>4166,
            'cat_id'=>8,
            'tag_id'=>10,
            'title'=>'Administration of recombinant thrombomodulin before progression of disease causing disseminated intravascular coagulation might be better compared with administration after progression of disease',
            'text'=>'Tomoaki Yatabe1a and others\r\nJournal of Intensive Care 2014, 2:49',
            'img'=>'file-1463591746-GEE3E7.jpg',
            'file'=>NULL,
            'link'=>'http://www.jintensivecare.com/content/2/1/49',
            'by'=>0,
            'journal'=>'Site Admin',
            'type'=>0,
            'month_article'=>0,
            'famous'=>0,
            'views'=>1101,
            'show'=>0,
            'date'=>'2015-08-22 09:04:10',
            'active'=>'1',
            'user'=>0,
            'created_at'=>'2020-05-15 01:20:06',
            'updated_at'=>'2020-06-21 00:08:23',
            'deleted_at'=>NULL
            ] );
            
            
                        
            Post::create( [
            'id'=>4167,
            'cat_id'=>8,
            'tag_id'=>10,
            'title'=>'Efficacy and safety of recombinant human soluble thrombomodulin ART-123 in disseminated intravascular coagulation: results of a phase III, randomized, double-blind clinical trial',
            'text'=>'Saito H and others\r\nJ Thromb Haemost. 2007 Jan51:31-41',
            'img'=>'file-1463591639-4YG8JY.jpg',
            'file'=>NULL,
            'link'=>'http://www.ncbi.nlm.nih.gov/pubmed/17059423',
            'by'=>0,
            'journal'=>'Site Admin',
            'type'=>0,
            'month_article'=>0,
            'famous'=>1,
            'views'=>1196,
            'show'=>0,
            'date'=>'2007-01-01 09:06:26',
            'active'=>'1',
            'user'=>0,
            'created_at'=>'2020-05-15 01:20:06',
            'updated_at'=>'2020-06-21 00:08:23',
            'deleted_at'=>NULL
            ] );
            
            
                        
            Post::create( [
            'id'=>4168,
            'cat_id'=>9,
            'tag_id'=>9,
            'title'=>'European Society of Clinical Microbiology and Infectious Diseases: 2014 update of the treatment guidance document for Clostridium difficile infection',
            'text'=>'S. B. Debast, M. P. Bauer, E. J. Kuijper, andon behalf of the Committeeâ€ \r\nClin Microbiol Infect 2014 Vol 20 Suppl s2, pp 1â€“26',
            'img'=>'file-1463591681-AGRM7H.jpg',
            'file'=>NULL,
            'link'=>'http://onlinelibrary.wiley.com/doi/10.1111/1469-0691.12418/full',
            'by'=>0,
            'journal'=>'Site Admin',
            'type'=>0,
            'month_article'=>0,
            'famous'=>0,
            'views'=>1101,
            'show'=>0,
            'date'=>'2014-10-01 10:10:41',
            'active'=>'1',
            'user'=>0,
            'created_at'=>'2020-05-15 01:20:06',
            'updated_at'=>'2020-06-21 00:08:23',
            'deleted_at'=>NULL
            ] );
            
            
                        
            Post::create( [
            'id'=>4169,
            'cat_id'=>9,
            'tag_id'=>1,
            'title'=>'ESCMID guidelines for the management of the infection control measures to reduce transmission of multidrug-resistant Gram-negative bacteria in hospitalized patients',
            'text'=>'E. Tacconelli and others\r\nClin Microbiol Infect 2014 Vol 20 Suppl s1, pp 1â€“55',
            'img'=>'file-1463591639-4YG8JY.jpg',
            'file'=>NULL,
            'link'=>'http://onlinelibrary.wiley.com/doi/10.1111/1469-0691.12427/full',
            'by'=>0,
            'journal'=>'Site Admin',
            'type'=>0,
            'month_article'=>0,
            'famous'=>0,
            'views'=>1130,
            'show'=>0,
            'date'=>'2014-10-01 10:14:42',
            'active'=>'1',
            'user'=>0,
            'created_at'=>'2020-05-15 01:20:06',
            'updated_at'=>'2020-06-21 00:08:23',
            'deleted_at'=>NULL
            ] );
            
            
                        
            Post::create( [
            'id'=>4170,
            'cat_id'=>9,
            'tag_id'=>1,
            'title'=>'European guidelines for empirical antibacterial therapy for febrile neutropenic patients in the era of growing resistance: summary of the 2011 4th European Conference on Infections in Leukemia',
            'text'=>'Diana Averbuch, Christina Orasch, Catherine Cordonnier, David M. Livermore, MaÅ‚gorzata Mikulska, Claudio Viscoli, Inge C. Gyssens, Winfried V. Kern, Galina Klyasova, Oscar Marchetti, Dan Engelhard, Murat Akova\r\nHaematologica December 2013 98: 1826-1835',
            'img'=>'file-1463591657-K8BRDF.jpg',
            'file'=>NULL,
            'link'=>'http://www.haematologica.org/content/98/12/1826.full',
            'by'=>0,
            'journal'=>'Site Admin',
            'type'=>0,
            'month_article'=>0,
            'famous'=>0,
            'views'=>1130,
            'show'=>0,
            'date'=>'2013-12-01 10:18:13',
            'active'=>'1',
            'user'=>0,
            'created_at'=>'2020-05-15 01:20:06',
            'updated_at'=>'2020-06-21 00:08:23',
            'deleted_at'=>NULL
            ] );
            
            
                        
            Post::create( [
            'id'=>4171,
            'cat_id'=>9,
            'tag_id'=>1,
            'title'=>'ESCMID Guideline for the Diagnosis and Treatment of Biofilm Infections',
            'text'=>'N. HÃ¸iby for the ESCMID Study Group for Biofilms ESGB and Consulting External Expert Werner Zimmerli14\r\nClin Microbiol Infect, May 1, 2015 Volume 21, Supplement 1, Pages S1â€“S25',
            'img'=>'file-1463591752-XK911T.jpg',
            'file'=>NULL,
            'link'=>'http://www.clinicalmicrobiologyandinfection.com/article/S1198-743X%2814%2900090-1/pdf',
            'by'=>0,
            'journal'=>'Site Admin',
            'type'=>0,
            'month_article'=>0,
            'famous'=>0,
            'views'=>1130,
            'show'=>0,
            'date'=>'2015-05-01 10:25:14',
            'active'=>'1',
            'user'=>0,
            'created_at'=>'2020-05-15 01:20:06',
            'updated_at'=>'2020-06-21 00:08:23',
            'deleted_at'=>NULL
            ] );
            
            
                        
            Post::create( [
            'id'=>4172,
            'cat_id'=>8,
            'tag_id'=>1,
            'title'=>'Aspirin as a potential treatment in sepsis or acute respiratory distress syndrome',
            'text'=>'Toner P, McAuley D, Shyamsundar M\r\nCritical Care 2015, 19 :374 23 October 2015',
            'img'=>'file-1463591738-IZ3XFW.jpg',
            'file'=>NULL,
            'link'=>'http://www.ccforum.com/content/19/1/374/abstract',
            'by'=>0,
            'journal'=>'Site Admin',
            'type'=>0,
            'month_article'=>0,
            'famous'=>0,
            'views'=>1130,
            'show'=>0,
            'date'=>'2015-10-23 09:47:41',
            'active'=>'1',
            'user'=>0,
            'created_at'=>'2020-05-15 01:20:06',
            'updated_at'=>'2020-06-21 00:08:23',
            'deleted_at'=>NULL
            ] );
            
            
                        
            Post::create( [
            'id'=>4173,
            'cat_id'=>8,
            'tag_id'=>8,
            'title'=>'Fluid balance and mortality in critically ill patients with acute kidney injury: a multicenter prospective epidemiological study',
            'text'=>'Wang N, Jiang L, Zhu B, Wen Y, Xi X, The Beijing Acute Kidney Injury Trial BAKIT Workgroup\r\nCritical Care 2015, 19 :371 23 October 2015',
            'img'=>'file-1463591738-IZ3XFW.jpg',
            'file'=>NULL,
            'link'=>'http://www.ccforum.com/content/19/1/371/abstract',
            'by'=>0,
            'journal'=>'Site Admin',
            'type'=>0,
            'month_article'=>0,
            'famous'=>0,
            'views'=>1152,
            'show'=>0,
            'date'=>'2015-10-23 09:54:35',
            'active'=>'1',
            'user'=>0,
            'created_at'=>'2020-05-15 01:20:06',
            'updated_at'=>'2020-06-21 00:08:23',
            'deleted_at'=>NULL
            ] );
            
            
                        
            Post::create( [
            'id'=>4174,
            'cat_id'=>8,
            'tag_id'=>11,
            'title'=>'The Association Between Daytime Intensivist Physician Staffing and Mortality in the Context of Other ICU Organizational Practices: A Multicenter Cohort Study',
            'text'=>'Costa, Deena Kelly and others\r\nCritical Care Medicine:\r\nNovember 2015 - Volume 43 - Issue 11 - p 2275â€“2282',
            'img'=>'file-1463591685-DDLMK9.jpg',
            'file'=>NULL,
            'link'=>'http://journals.lww.com/ccmjournal/Fulltext/2015/11000/The_Association_Between_Daytime_Intensivist.2.aspx',
            'by'=>0,
            'journal'=>'Site Admin',
            'type'=>0,
            'month_article'=>0,
            'famous'=>0,
            'views'=>1101,
            'show'=>0,
            'date'=>'2015-11-01 08:21:03',
            'active'=>'1',
            'user'=>0,
            'created_at'=>'2020-05-15 01:20:06',
            'updated_at'=>'2020-06-21 00:08:23',
            'deleted_at'=>NULL
            ] );
            
            
                        
            Post::create( [
            'id'=>4175,
            'cat_id'=>8,
            'tag_id'=>1,
            'title'=>'Rapid Diagnosis of Infection in the Critically Ill, a Multicenter Study of Molecular Detection in Bloodstream Infections, Pneumonia, and Sterile Site Infections',
            'text'=>'Vincent, Jean-Louis and others\r\nCritical Care Medicine:\r\nNovember 2015 - Volume 43 - Issue 11 - p 2283â€“2291',
            'img'=>'file-1463591725-9J88SF.jpg',
            'file'=>NULL,
            'link'=>'http://journals.lww.com/ccmjournal/Fulltext/2015/11000/Rapid_Diagnosis_of_Infection_in_the_Critically.3.aspx',
            'by'=>0,
            'journal'=>'Site Admin',
            'type'=>0,
            'month_article'=>0,
            'famous'=>0,
            'views'=>1130,
            'show'=>0,
            'date'=>'2015-11-02 08:23:01',
            'active'=>'1',
            'user'=>0,
            'created_at'=>'2020-05-15 01:20:06',
            'updated_at'=>'2020-06-21 00:08:23',
            'deleted_at'=>NULL
            ] );
            
            
                        
            Post::create( [
            'id'=>4176,
            'cat_id'=>8,
            'tag_id'=>7,
            'title'=>'Double-Blind Prospective Randomized Controlled Trial of Dopamine Versus Epinephrine as First-Line Vasoactive Drugs in Pediatric Septic Shock',
            'text'=>'Ventura, AndrÃ©a M. C. and others\r\nCritical Care Medicine:\r\nNovember 2015 - Volume 43 - Issue 11 - p 2292â€“2302',
            'img'=>'file-1463591650-J14MMU.jpg',
            'file'=>NULL,
            'link'=>'http://journals.lww.com/ccmjournal/Abstract/2015/11000/Double_Blind_Prospective_Randomized_Controlled.4.aspx',
            'by'=>0,
            'journal'=>'Site Admin',
            'type'=>0,
            'month_article'=>0,
            'famous'=>0,
            'views'=>1101,
            'show'=>0,
            'date'=>'2015-11-01 08:25:35',
            'active'=>'1',
            'user'=>0,
            'created_at'=>'2020-05-15 01:20:06',
            'updated_at'=>'2020-06-21 00:08:23',
            'deleted_at'=>NULL
            ] );
            
            
                        
            Post::create( [
            'id'=>4177,
            'cat_id'=>8,
            'tag_id'=>11,
            'title'=>'A Randomized Study of a Single Dose of Intramuscular Cholecalciferol in Critically Ill Adults',
            'text'=>'Nair, Priya and others\r\nCritical Care Medicine:\r\nNovember 2015 - Volume 43 - Issue 11 - p 2313â€“2320',
            'img'=>'file-1463591657-K8BRDF.jpg',
            'file'=>NULL,
            'link'=>'http://journals.lww.com/ccmjournal/Abstract/2015/11000/A_Randomized_Study_of_a_Single_Dose_of.6.aspx',
            'by'=>0,
            'journal'=>'Site Admin',
            'type'=>0,
            'month_article'=>0,
            'famous'=>0,
            'views'=>1101,
            'show'=>0,
            'date'=>'2015-11-01 08:29:44',
            'active'=>'1',
            'user'=>0,
            'created_at'=>'2020-05-15 01:20:06',
            'updated_at'=>'2020-06-21 00:08:23',
            'deleted_at'=>NULL
            ] );
            
            
                        
            Post::create( [
            'id'=>4178,
            'cat_id'=>8,
            'tag_id'=>1,
            'title'=>'Arterial Catheter Use in the ICU: A National Survey of Antiseptic Technique and Perceived Infectious Risk',
            'text'=>'Cohen, David M and others\r\nCritical Care Medicine:\r\nNovember 2015 - Volume 43 - Issue 11 - p 2346â€“2353',
            'img'=>'file-1463591746-GEE3E7.jpg',
            'file'=>NULL,
            'link'=>'http://journals.lww.com/ccmjournal/Abstract/2015/11000/Arterial_Catheter_Use_in_the_ICU___A_National.10.aspx',
            'by'=>0,
            'journal'=>'Site Admin',
            'type'=>0,
            'month_article'=>0,
            'famous'=>0,
            'views'=>1130,
            'show'=>0,
            'date'=>'2015-11-01 08:31:21',
            'active'=>'1',
            'user'=>0,
            'created_at'=>'2020-05-15 01:20:06',
            'updated_at'=>'2020-06-21 00:08:23',
            'deleted_at'=>NULL
            ] );
            
            
                        
            Post::create( [
            'id'=>4179,
            'cat_id'=>8,
            'tag_id'=>7,
            'title'=>'Predicting the Occurrence of Hypotension in Stable Patients With Nonvariceal Upper Gastrointestinal Bleeding: Point-of-Care Lactate Testing',
            'text'=>'Ko, Byuk Sung and others\r\nCritical Care Medicine:\r\nNovember 2015 - Volume 43 - Issue 11 - p 2409â€“2415',
            'img'=>'file-1463591725-9J88SF.jpg',
            'file'=>NULL,
            'link'=>'http://journals.lww.com/ccmjournal/Abstract/2015/11000/Predicting_the_Occurrence_of_Hypotension_in_Stable.18.aspx',
            'by'=>0,
            'journal'=>'Site Admin',
            'type'=>0,
            'month_article'=>0,
            'famous'=>0,
            'views'=>1101,
            'show'=>0,
            'date'=>'2015-11-01 08:33:54',
            'active'=>'1',
            'user'=>0,
            'created_at'=>'2020-05-15 01:20:06',
            'updated_at'=>'2020-06-21 00:08:23',
            'deleted_at'=>NULL
            ] );
            
            
    }
}
