<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->integer('id', true);
            $table->string('email', 50);
            $table->string('facebook', 100)->nullable();
            $table->string('twitter', 100)->nullable();
            $table->string('password');
            $table->string('date')->nullable();
            $table->string('f_name')->nullable();
            $table->string('l_name')->nullable();
            $table->string('hospital')->nullable();
            $table->string('profession')->nullable();
            $table->string('mobile', 20)->nullable();
            $table->string('country', 100)->nullable();
            $table->string('city', 100)->nullable();
            $table->string('street', 200)->nullable();
            $table->text('img')->nullable();
            $table->integer('active')->default(0);
            $table->integer('type')->comment('0 Normal / 1 Silver / 2 Gold / 3 admin');
            $table->boolean('paid')->default(false);
            $table->boolean('activated')->default(false);
            $table->rememberToken();
            $table->string('app_token')->nullable();
            $table->timestamp('email_verfied_at')->nullable();
            $table->timestamp('created_at')->default(DB::raw('CURRENT_TIMESTAMP'));
            $table->timestamp('updated_at')->default(DB::raw('CURRENT_TIMESTAMP on update CURRENT_TIMESTAMP'));
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
