<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCoursesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('courses', function (Blueprint $table) {
            $table->integer('id', true);
            $table->string('title', 100);
            $table->date('from');
            $table->date('to');
            $table->date('exam_time');
            $table->decimal('price', 11);
            $table->integer('exam_limit')->comment('in hours');
            $table->string('img', 100);
            $table->text('description');
            $table->boolean('type')->comment('1 = critical care edu / 2= critical care us / 3=online course');
            $table->integer('deleted')->default(0);
            $table->integer('views')->default(0);
            $table->timestamp('created_at')->default(DB::raw('CURRENT_TIMESTAMP'));
            $table->timestamp('updated_at')->default(DB::raw('CURRENT_TIMESTAMP on update CURRENT_TIMESTAMP'));
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('courses');
    }
}
