<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCatsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cats', function (Blueprint $table) {
            $table->integer('id', true);
            $table->string('title', 100);
            $table->string('img', 50);
            $table->integer('main')->comment('show in site links');
            $table->integer('order');
            $table->integer('shows')->default(0);
            $table->boolean('public');
            $table->tinyInteger('normal');
            $table->tinyInteger('silver');
            $table->tinyInteger('gold');
            $table->integer('deleted');
            $table->timestamp('created_at')->default(DB::raw('CURRENT_TIMESTAMP'));
            $table->timestamp('updated_at')->default(DB::raw('CURRENT_TIMESTAMP on update CURRENT_TIMESTAMP'));
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('cats');
    }
}
