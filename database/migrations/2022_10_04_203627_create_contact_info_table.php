<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateContactInfoTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('contact_info', function (Blueprint $table) {
            $table->integer('id', true);
            $table->string('home_job_title');
            $table->text('home_job_text');
            $table->string('site_kw');
            $table->text('site_des');
            $table->integer('site_visit');
            $table->string('phone', 15);
            $table->text('address');
            $table->string('email', 50);
            $table->string('site_email');
            $table->string('paypal_account', 100);
            $table->string('paypal_client_id')->nullable();
            $table->string('paypal_secret')->nullable();
            $table->string('facebook');
            $table->string('google');
            $table->string('twitter');
            $table->string('instgram');
            $table->string('youtube');
            $table->string('android');
            $table->string('apple');
            $table->text('map');
            $table->timestamp('created_at')->default(DB::raw('CURRENT_TIMESTAMP'));
            $table->timestamp('updated_at')->default(DB::raw('CURRENT_TIMESTAMP on update CURRENT_TIMESTAMP'));
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('contact_info');
    }
}
