<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCcatsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ccats', function (Blueprint $table) {
            $table->integer('id', true);
            $table->string('name');
            $table->string('playlist');
            $table->text('link1');
            $table->text('link2');
            $table->text('link3');
            $table->text('who_we_are');
            $table->text('what_we_do');
            $table->text('ongoing_researches');
            $table->timestamp('created_at')->default(DB::raw('CURRENT_TIMESTAMP'));
            $table->timestamp('updated_at')->default(DB::raw('CURRENT_TIMESTAMP on update CURRENT_TIMESTAMP'));
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ccats');
    }
}
