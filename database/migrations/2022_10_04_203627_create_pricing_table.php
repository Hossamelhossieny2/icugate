<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePricingTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pricing', function (Blueprint $table) {
            $table->integer('id', true);
            $table->boolean('type');
            $table->string('title', 100);
            $table->integer('price');
            $table->integer('popular');
            $table->decimal('course_percent', 11);
            $table->decimal('ccesp_discount', 11);
            $table->string('img');
            $table->text('desc');
            $table->text('brief');
            $table->integer('videos');
            $table->integer('protocols');
            $table->integer('lectures');
            $table->integer('contribution');
            $table->timestamp('created_at')->default(DB::raw('CURRENT_TIMESTAMP'));
            $table->timestamp('updated_at')->default(DB::raw('CURRENT_TIMESTAMP on update CURRENT_TIMESTAMP'));
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pricing');
    }
}
