<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUserCourseTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_course', function (Blueprint $table) {
            $table->integer('id', true);
            $table->integer('user_id');
            $table->integer('course_id');
            $table->integer('quiz')->nullable();
            $table->integer('ready_exam')->default(0);
            $table->integer('test')->nullable()->default(0);
            $table->boolean('status')->default(false)->comment('0 = Didn\'t finish quiz / 1 = finished Quiz But no Exam / 2 Finished Exam');
            $table->timestamp('date')->useCurrent();
            $table->boolean('paid')->default(false);
            $table->decimal('price', 6)->default(0);
            $table->string('certificate')->nullable();
            $table->string('paypal_order_id')->nullable();
            $table->timestamp('created_at')->default(DB::raw('CURRENT_TIMESTAMP'));
            $table->timestamp('updated_at')->default(DB::raw('CURRENT_TIMESTAMP on update CURRENT_TIMESTAMP'));
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('user_course');
    }
}
