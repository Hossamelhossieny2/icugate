<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUserOnlineStudyTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_online_study', function (Blueprint $table) {
            $table->integer('id', true);
            $table->integer('course_id');
            $table->integer('chapter');
            $table->integer('exam')->nullable();
            $table->timestamp('start')->useCurrentOnUpdate()->default(DB::raw('CURRENT_TIMESTAMP(0)'));
            $table->timestamp('visit')->nullable();
            $table->integer('video_id');
            $table->integer('user');
            $table->timestamp('created_at')->default(DB::raw('CURRENT_TIMESTAMP'));
            $table->timestamp('updated_at')->default(DB::raw('CURRENT_TIMESTAMP on update CURRENT_TIMESTAMP'));
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('user_online_study');
    }
}
