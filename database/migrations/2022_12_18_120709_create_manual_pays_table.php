<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('manual_pays', function (Blueprint $table) {
            $table->id();
            $table->integer('user_id');
            $table->integer('pay_user');
            $table->integer('payment_type')->comment('1 sub / 2 cor /3 con');
            $table->integer('item_id')->comment('item ID to pay');
            $table->string('amount')->comment('total amout to pay');
            $table->string('payment_title')->comment('reson for this payment');
            $table->timestamp('created_at')->default(DB::raw('CURRENT_TIMESTAMP'));
            $table->timestamp('updated_at')->default(DB::raw('CURRENT_TIMESTAMP on update CURRENT_TIMESTAMP'));
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('manual_pays');
    }
};
