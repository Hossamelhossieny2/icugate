<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateConfigTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('config', function (Blueprint $table) {
            $table->integer('id');
            $table->string('conf_title');
            $table->text('conf_desc');
            $table->string('conf_username');
            $table->string('conf_password');
            $table->string('conf_mail');
            $table->text('conf_welcome');
            $table->string('conf_address', 300);
            $table->string('conf_phone', 200);
            $table->string('conf_fax', 200);
            $table->string('conf_facebook', 250);
            $table->string('conf_twitter', 250);
            $table->string('conf_google_plus', 250);
            $table->string('conf_inquire_image');
            $table->string('conf_inquire_title');
            $table->timestamp('created_at')->default(DB::raw('CURRENT_TIMESTAMP'));
            $table->timestamp('updated_at')->default(DB::raw('CURRENT_TIMESTAMP on update CURRENT_TIMESTAMP'));
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('config');
    }
}
