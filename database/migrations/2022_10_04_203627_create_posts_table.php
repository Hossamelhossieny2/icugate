<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePostsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('posts', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('cat_id')->nullable();
            $table->integer('tag_id')->nullable();
            $table->text('title');
            $table->text('text');
            $table->string('img', 50);
            $table->string('file', 100)->nullable();
            $table->text('link')->nullable();
            $table->integer('by')->nullable();
            $table->string('journal')->default('Site Admin');
            $table->tinyInteger('type')->comment('0 = news / 1 = link on other site / 2 = research / 3 presentation');
            $table->integer('month_article')->default(0);
            $table->boolean('famous')->default(0);
            $table->integer('views')->nullable()->default(0);
            $table->boolean('show')->nullable();
            $table->string('date')->nullable();
            $table->enum('active', ['1', '0'])->default('1');
            $table->integer('user')->default(0);
            $table->timestamp('created_at')->default(DB::raw('CURRENT_TIMESTAMP'));
            $table->timestamp('updated_at')->default(DB::raw('CURRENT_TIMESTAMP on update CURRENT_TIMESTAMP'));
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('posts');
    }
}
