<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePayPalOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pay_pal_orders', function (Blueprint $table) {
            $table->integer('id', true);
            $table->integer('user_id');
            $table->integer('item_id');
            $table->integer('order_type_id')->comment('1 cor /2 sub /3 con');
            $table->string('order_id')->comment('this local payment id');
            $table->string('order_title')->comment('reson for this payment');
            $table->string('total')->comment('total amout to pay');
            $table->text('response')->nullable()->comment('all paypal response');
            $table->string('response_code')->nullable();
            $table->string('paypal_transaction_id')->nullable();
            $table->integer('paid')->default(0)->comment('0 not paid /1 paid');
            $table->timestamp('created_at')->default(DB::raw('CURRENT_TIMESTAMP'));
            $table->timestamp('updated_at')->default(DB::raw('CURRENT_TIMESTAMP on update CURRENT_TIMESTAMP'));
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pay_pal_orders');
    }
}
